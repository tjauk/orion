#!/bin/bash

# set all folders
declare -a folders=(events_croatia events_france events_poland)

# loop through folders and execute
for folder in "${folders[@]}"
do
	find /var/www/orion/$folder/cache/globals/*.same.* -type f -delete
	find /var/www/orion/$folder/cache/globals/city.* -type f -delete
done

echo "Cache cleared on 3NIUM server"
