<?php

class ArticleController extends Controller
{

    /**
     * @return mixed
     */
    function index()
    {
        return Article::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    function show($id)
    {
        return Article::findOrFail($id);
    }

    function latest()
    {
        return Article::where('publish_at', '>', time())->get();
    }

}
