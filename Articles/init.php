<?php
/**
 * Articles module
 *
 * @package  Orion
 * @author   Tihomir Jauk <tihomirjauk@gmail.com>
 */

/*
|--------------------------------------------------------------------------
| Register module
|--------------------------------------------------------------------------
|
| Simple definition to quickly start your module
|
*/
Module::init([
	'name'=>"Articles management",
	'author'=>"Test",
	'basedir'=>__DIR__,
	'autoload'=>['controllers','models','views']
]);
