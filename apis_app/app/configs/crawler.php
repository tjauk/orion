<?php

return [

	'report_url'	=> "http://netapis.com/api/report",

	/*
	|--------------------------------------------------------------------------
	| List of countries currently crawling for events
	|--------------------------------------------------------------------------
	|
	| Sort them by priority
	|
	*/

	'event_countries' => [
		'Germany',
		'Netherlands',
		'Sweden',
		'Norway',
		'Finland',
		'Denmark',
		'Switzerland',
		'Austria',
		'Ireland',
		'Croatia',
		'United Kingdom',
		'United States',
		'Canada',
		'Australia',
		'New Zealand',
		'Spain',
		'Belgium',
		'Italy',
		//'Portugal',
	]


];
