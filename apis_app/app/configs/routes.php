<?php
// USER
Route::any('signin','User@login');
Route::get('signup','User@register');

// SITEMAPS
Route::get('sitemap.xml', 			'Sitemap@index');
Route::get('sitemap/schema.xsl', 	'Sitemap@schema');
Route::get('sitemap_misc.xml',		'Sitemap@misc');
Route::get('sitemap_pages.xml',		'Sitemap@pages');

return array();