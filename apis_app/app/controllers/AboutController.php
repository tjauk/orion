<?php

class AboutController extends Controller
{
	public function boot(){
		//Doc::view('layout-pages');
	}

    /**
     * INDEX
     */
    function index()
    {
    	Doc::title('About');
        return view('about');
    }

}
