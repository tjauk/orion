<?php
class AdminApiKeyController extends AdminController
{
    public $model = 'ApiKey';
    public $baseurl = '/admin/apikey';


    /**
     * Index page
     *
     * @return View
     */
    function index()
    {
        $model = $this->model;
        
        Doc::title("API Access Keys");

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add API Access Key',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'key,user_id' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search')->search('keywords')->placeholder('Search');
        $form->label('Per page')->select('perpage')->values([10,20,50,100]);

        $table = Table::make()
                ->headings(['#','Key','User','Created At','Expires At','Today','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = linkto('/admin/apikey/edit/'.$item->id,$item->key);
            $data[] = $item->user->email.' '.linkto('/admin/user/edit/'.$item->user->id,'<i class="fa fa-pencil"></i>');
            $data[] = UI::dateTime($item->created_at);
            $data[] = UI::dateTime($item->expires_at);
            $data[] = '0';
            $actions = UI::btnEdit($item->id,'apikey');
            $actions.= UI::btnDelete($item->id,'apikey');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    /**
     * Edit page
     *
     * @return View
     */
    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('apikey');

        $form->label('User Id')->input('user_id');
        $form->label('Key')->input('key');
        $form->label('Expires At')->datePicker('expires_at');

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }


    /**
     * Delete
     *
     * @return Response
     */
    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/apikey')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
