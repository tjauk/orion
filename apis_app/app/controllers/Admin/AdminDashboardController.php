<?php

class AdminDashboardController extends AdminController
{

    /**
     * INDEX
     */
    function index()
    {
        Doc::title('Dashboard');
        return View::make('admin/dashboard/sample');
    }

}
