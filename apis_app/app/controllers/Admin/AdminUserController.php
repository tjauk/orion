<?php
class AdminUserController extends AdminController
{
    public $model = 'User';
    public $baseurl = '/admin/user';

    function index()
    {

        $model = $this->model;	
        
        Doc::title(str_plural($model));

        $items = new $model();

        if( Auth::can('user.create') ){
            $buttons = Forms::make();
            $buttons->btnAdd('Add User',$this->baseurl.'/edit');
        }

        $filter = new SearchFilter();

        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, ['username','email','first_name','last_name'] );
        }
        // filter user group
        if( $filter->notEmpty('usergroup') ){
            $items = $items->where( 'usergroup', '=', $filter->usergroup );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('Search')->focus();
        $form->label('Per page');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');
        $form->label('Usergroup');
        $form->select('usergroup')->options(UserGroup::options())->klass('form-control');


    	$table = Table::make()->headings([
                    '#',
                    'Username',
                    'Email',
                    'First name',
                    'Last name',
                    'Last seen',
                    'Usergroup',
                    'Plan',
                    'Requests',
                    'Actions'
                ]);

        $items = $filter->paginate( $items )->get();

    	foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = '<a href="/admin/user/edit/'.$item->id.'">'.$item->username.'</a>';
            $data[] = $item->email;
            $data[] = $item->first_name;
            $data[] = $item->last_name;
            $data[] = $item->last_seen;
            $data[] = $item->usergroup;
            $data[] = $item->plan->name;
            $data[] = $item->requests;

            $actions = UI::btnEdit($item->id,'user');
            $actions.= UI::btnDelete($item->id,'user');
            if( Auth::can('user.switchto') ){
                $actions.= '<a class="btn btn-xs btn-warning" href="/admin/user/switch_to/'.$item->id.'">SWITCH&nbsp;TO</a> ';
            }
            if( $item->blocked==1 ){
                $actions.= '<a class="btn btn-xs btn-success" href="/admin/user/unblock/'.$item->id.'">UNBLOCK</a> ';
            }else{
                $actions.= '<a class="btn btn-xs btn-danger" href="/admin/user/block/'.$item->id.'">BLOCK</a> ';
            }
            $data[] = $actions;
    		$table->addRow($data);
    	}
        $table = $table->render('admin/table/grid');

        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    /**
     * Edit user
     *
     * @return Response
     */
    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->username = Request::post('username');
            if( !empty(Request::post('password')) ){
                $item->password = Request::post('password');
            }
            $item->save();
            return redirect('admin/user')->with('msg',"$model {$item->username} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item->toArray());
        }
        $form->open('user');

        $form->div('.col-md-6');
        $form->panel('Personal info');

            $form->label('First Name')->input('first_name');
            $form->label('Last Name')->input('last_name');
            $form->label('Email')->email('email')->value($item->email);
            
            $form->label('Last seen')->input('last_seen')->readonly()->value($item->last_seen);
    
        $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-6');
        $form->panel('User info');

            $form->label('Username')->input('username')->addon('fa-user');
            $form->label('Password')->input('password')->addon('fa-lock')->value('')->placeholder('Leave blank or enter new password to change it');

            $form->label('Usergroup')->select('usergroup')->options(UserGroup::options());
            
            $yesno = ['1'=>"Yes",'0'=>"No"];
            $form->label('Blocked')->radioGroup('blocked')->options($yesno);


        $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-12');
        $form->panel('Subscription');

            $form->label('Subscription plan')->select('plan_id')->options( Plan::options() );
            $form->label('Requests made this month')->number('requests');

        $form->closePanel();
        $form->closeDiv();

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // add auth can delete model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('admin/user')->with('msg',"{$this->model} #{$item->id} deleted");
    }

    function block($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // add auth can block user
        if( $item->id>0 ){
            $item->blocked = 1;
            $item->save();
        }
        return redirect('admin/user')->with('msg',"{$this->model} #{$item->id} blocked");
    }

    function unblock($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // add auth can unblock user
        if( $item->id>0 ){
            $item->blocked = 0;
            $item->save();
        }
        return redirect('admin/user')->with('msg',"{$this->model} #{$item->id} blocked");
    }

    function switchTo($id=0)
    {
        if( Auth::can('user.switchto') ){
            $user = Auth::switchTo($id);
            return redirect('admin/user')->with('msg',"Switched to #{$user->username}");    
        }
        return redirect('admin/user')->with('msg',"You are not allowed to switch to another user");
    }

    function switchBack()
    {
        if( Session::has('auth.original') ){
            $user = Auth::switchBack();
            return redirect('admin/user')->with('msg',"Switched back to #{$user->username}");    
        }
        return redirect('admin/user')->with('msg',"You are not allowed to switch to another user");
    }

    function rememberSidebarState($state){
        if( in_array($state,['opened','closed']) ){
            if( $state=='closed' ){
                Session::put('user-sidebar-state','closed sidebar-collapse');
            }else{
                Session::put('user-sidebar-state','sidebar-open');
            }
            return true;
        }
        return false;
    }

    function profile($id=0)
    {
        Doc::title('User Profile');
        
        if( $id>0 ){
            $user = User::find($id);
        }else{
            $user = Auth::user();
        }

        return View::make('admin/user/profile')->put($user,'user');
    }

}
