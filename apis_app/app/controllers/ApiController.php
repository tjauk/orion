<?php

class ApiController extends Controller
{
	public function boot()
	{
		Doc::json();

        ApiLog::request();

        $this->response = [];
        $this->response['success'] = true;
        
        // test
        $access_token = AccessToken::findByToken(Request::all('access_token'));
        if( $access_token->id>0 ){
            // check if valid
            $access_token->user()->increment('requests');
            // check rates
            //$this->response['access_token'] = $access_token;
        }else{
            $error = [
                'message'   => "Invalid API Access Token",
            ];
            $this->response['error'] = $error;
            $this->response['success'] = false;
        }

        if( !isset($this->response['error']) ){
            $this->response['message'] = 'OK';
        }
        $this->response['data'] = [];
	}

    /**
     * INDEX
     */
    function index()
    {
    	return [
    		'countries'	=> ROOTURL.'/api/countries',
    	];
    }


    /**
     * COUNTRIES
     */
    function countries()
    {
        if( !isset($this->response['error']) ){
            $this->response['data'] = Country::all();
        }
        return $this->response;
    }

    /**
     * COUNTRY
     */
    function country()
    {
        $id = (int)Request::all('id');

        if( !isset($this->response['error']) ){
            $this->response['data'] = Country::find($id);
        }
        return $this->response;
    }


    /**
     * EVENTS
     */
    function events()
    {
        $max_limit = 250;
        
        $country = urldecode( Request::get('country') );
        $city = urldecode( Request::get('city') );

        $since = urldecode( Request::get('since') );//today or yesterday or date
        $since_id = urldecode( Request::get('since_id') );//id

        $limit = (int)Request::get('limit');
        if( $limit<1 or $limit>$max_limit ){ $limit = $max_limit; }

        $page = (int)Request::get('page');
        if( $page<1 ){ $page = 1; }
        $from = $limit*($page-1);

        if( !isset($this->response['error']) ){
            $events = FbeEvent::whereNull('duplicate')
                                ->whereNotNull('cover_source')
                                ->whereCountry($country);
            if( !empty($city) ){
                $events = $events->whereCity($city);
            }
            if( !empty($since_id) ){
                $events = $events->where( 'id', '>=', $since_id );
            }else if( !empty($since) ){
                $since_date = date('Y-m-d',strtotime($since));
                $events = $events->where( DB::raw("TIMESTAMP(changed_at)"), '>=', DB::raw("TIMESTAMP('{$since_date}')") );
            }
            $events = $events->skip($from)->take($limit);
            $this->response['data'] = $events->get();
        }
        return $this->response;
    }


    /**
     * TOTAL EVENTS PER COUNTRIES
     */
    function eventsPerCountries()
    {
        $sql = "
SELECT country, COUNT(*) AS c
FROM fbe_events
WHERE
     DATE(start_time)>=CURRENT_DATE()
     AND
     cover_source IS NOT NULL
GROUP BY country
HAVING c>300
ORDER BY c DESC";

        $results = DB::select(DB::raw($sql));
        $rows = [];
        foreach($results as $result){
            if( !empty($result->country) ){
                $rows[] = [$result->country,$result->c];
            }
        }

        return $rows;
    }



    /**
     * CHECK ACCESS TOKEN
     */
    function accessToken()
    {
        if( !isset($this->response['error']) ){
            $access_token = AccessToken::findByToken(Request::all('access_token'));
            $this->response['data'] = $access_token;
        }
        return $this->response;
    }


}
