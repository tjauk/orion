<?php

class ContactController extends Controller
{
	public function boot(){
		//Doc::view('layout-pages');
	}

    /**
     * INDEX
     */
    function index()
    {
    	Doc::title('Contact');
        return view('contact');
    }

}
