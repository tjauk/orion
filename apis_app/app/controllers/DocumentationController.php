<?php

class DocumentationController extends Controller
{
	public function boot(){
		//Doc::view('layout-pages');
	}

    /**
     * INDEX
     */
    function index()
    {
    	Doc::title('Documentation');
        return view('documentation');
    }

}
