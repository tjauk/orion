<?php

class FaqController extends Controller
{
	public function boot(){
		//Doc::view('layout-pages');
	}

    /**
     * INDEX
     */
    function index()
    {
    	Doc::title('FAQ');
        return view('faq');
    }

}
