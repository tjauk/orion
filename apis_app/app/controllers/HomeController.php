<?php

class HomeController extends Controller
{

    /**
     * INDEX
     */
    function index()
    {
    	Doc::title("FREE powerful APIs for developers");
        return view('home');
    }

}
