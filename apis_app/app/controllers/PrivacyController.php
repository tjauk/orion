<?php

class PrivacyController extends Controller
{
	public function boot(){
		//Doc::view('layout-pages');
	}

    /**
     * INDEX
     */
    function index()
    {
    	Doc::title('Privacy Policy');
        return view('privacy');
    }

}
