<?php

class TermsController extends Controller
{
	public function boot(){
		//Doc::view('layout-pages');
	}

    /**
     * INDEX
     */
    function index()
    {
    	Doc::title('Terms & Conditions');
        return view('terms');
    }

}
