<?php
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class TestController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    function index()
    {
        //
    }

    function generate($user_id=1){
        $key = ApiKey::create(['user_id'=>1]);

        return $key;
    }

    function key($key){
        $key = ApiKey::findByKey($key);
        return $key;
    }

    function user($key){
        return ApiKey::findByKey($key)->user;
    }


}
