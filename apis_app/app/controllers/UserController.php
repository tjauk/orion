<?php
class UserController extends Controller
{


    /**
     * 
     *
     * @return Response
     */
    public function index()
    {
        return redirect('user/login');
    }



    /**
     * 
     *
     * @return Response
     */
    public function login()
    {
        Doc::title("Sign-in to your account");
        
        $form = Forms::make();
        $form->open('user_login');
        $form->label('Username')->input('username');
        $form->label('Password')->password('password');
        $form->submit('Sign in');
        $form->close();

        return View::make('user/login')->put($form,'form');
    }


    /**
     * 
     *
     * @return Response
     */
    public function postLogin()
    {
        $post = Request::post();
        $credentials = ['username'=>$post['username'],'password'=>$post['password']];
        if( Auth::attempt($credentials) ){
            $intended = Session::get('intended');
            if( !empty($intended) ){
                Session::put('intended',null);
                return redirect($intended);
            }
            return redirect('account/index');
        }
        return redirect('user/login')->with('msg',"Invalid credentials");
    }


    /**
     * 
     *
     * @return Response
     */
    public function logout()
    {
        Auth::logout();
        return redirect('user/login');
    }


    /**
     * 
     *
     * @return Response
     */
    public function register()
    {
        $form = Forms::make();
        $form->open('user/register');
        $form->label('Username')->input('username');
        $form->label('Password')->password('password');
        $form->label('Repeated password')->password('repeated_password');
        $form->submit('Register');
        $form->close();

        return View::make('user/register')->put($form,'form');
    }


    /**
     * 
     *
     * @return Response
     */
    public function postRegister()
    {
        $post = Request::post();

        $credentials = ['username'=>$post['username'],'password'=>$post['password']];
        // check existence
        $user = User::whereUsername($post['username'])->first();
        if( $user->id>0 ){
            // try auth attempt and redirect to account on success
            // or return with error message
            return redirect('/user/register')->with('msg',"Username already exists");
        }

        if( $post['password']!==$post['repeated_password'] ){
            return redirect('/user/register')->with('msg',"Password and repeated password must match");
        }

        $user = new User();
        $user->username = $post['username'];
        $user->email = $post['username'];
        $user->password = $post['password'];
        $user->usergroup = 'user';
        $user->save();

        if( Auth::attempt($credentials) ){
            return redirect('account/index')->with('msg',"Created new user account");
        }

        return redirect('user/register')->with('msg',"Invalid credentials");
    }


    /**
     * 
     *
     * @return Response
     */
    public function lostPassword()
    {
        $form = Forms::make();
        $form->open('user/lost-password');
        $form->label('Email')->email('email')->addClass("form-control")->placeholder("my@email.com");
        $form->submit('Send My New Password')->addClass("btn btn-primary");
        $form->close();
        return View::make('user/lost-password')->put($form,'form');
    }


    /**
     * 
     *
     * @return Response
     */
    public function changePassword()
    {
        $form = Forms::make();
        $form->open('user/change-password');
        $form->label('Password')->password('password')->addClass("form-control")->placeholder("Enter new password");
        $form->submit('Change')->addClass("btn btn-primary");
        $form->close();
        return View::make('user/change-password')->put($form,'form');
    }


    /**
     * 
     *
     * @return Response
     */
    public function postChangePassword()
    {
        $post = Request::post();
        $user = Auth::user();
        //
        return View::make('user/change-password')->put($form,'form');
    }


    /**
     * 
     *
     * @return Response
     */
    public function postLostPassword()
    {
        $email = Request::post('email');
        $user = User::findByEmail($email);
        if( $user->id>0 ){
            // generate token
            $token = Auth::generateToken();
            // save to password_resets table
            DB::table('password_resets')->insert([
                'email'=>$email,
                'token'=>$token,
                'created_at'=>Carbon::now()
            ]);
            // send mail
            
            // show message
            $msg = "Poslali smo vam mail s uputama kako resetirati svoju lozinku";
            
        }else{
            // redirect with message
            return redirect('user/lost-password')->with(['msg'=>"Nismo pronašli korisnik s takvom email adresom"]);
        }
        $form = Forms::make();
        $form->open('user/lost-password');
        $form->label('Email')->email('email');
        $form->close();
        return View::make('user/lost-password')->put($form,'form');
    }


    /**
     * 
     *
     * @return Response
     */
    public function verifyToken()
    {
        $token = Request::get('token');
        if( empty($token) ){

        }
        $user = User::findByToken($token);
        return View::make('user/verify-token');
    }


}
