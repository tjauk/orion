<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion fbe:dict2
class FbeDict2Job extends Command
{
    static $fbi = 0;
    static $tokens = null;

    static $limit = 100;

    static $wait_between = 7;
    static $wait_on_error = 1200; // 60 * 20 min

    public $output;

    protected function configure()
    {
        $this
            ->setName('fbe:dict2')
            ->setDescription('Get all facebook events within country using dictionary attack v2')
            ->addArgument(
                'country',
                InputArgument::REQUIRED,
                'Country'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '256M');


        $this->output = $output;

        $limit = 100;
        $wait_max_sec = static::$wait_between;

        $country = $input->getArgument('country');

        // create dictionary queries
        /*
            1. country name
            2. all city names within the country
            3. for each city with more then limit/2 results add 10 more word combinations
            4. for each city with more then ?? results add 20 more word combinations
            5. for each city with more then ?? results add 30 custom word combinations
            6. country name + 100 top words
            7. done, move to next country in queue
        */

        $dict = [];

        // 1. country name
        $dict[$country] = 0;
        $query = FbeQuery::register($country,$country,1);
        $this->search($query);

        // 2. all city names within the country
        $cities = FbeCity::whereCountry($country)->get();
        foreach($cities as $city){
            $dict[$city->name] = 0;
            $query = FbeQuery::register($country,$city->name,2);
            $this->search($query);
            $dict[$city->name] = 1;

            $progress = array_sum($dict);
            $total = count($cities);
            $proc = round($progress/$total*100);
            $eta = ceil(($total-$progress)/60*static::$wait_between);
            $this->output->writeln(" ");
            $this->output->writeln("Phase 2 ---------- $progress / $total ---------- $proc% / $eta min");
        }

        // 3. try 200 most common word for this country
        $words = collect( FbeDict::top($country,200)->get(['word']) )->pluck('word')->toArray();
        foreach($words as $word){
            $dict[$word] = 0;
            $query = FbeQuery::register($country,$word,3);
            $this->search($query);
            $dict[$word] = 1;

            $progress = array_sum($dict);
            $total = count($dict)+count($words);
            $proc = round($progress/$total*100);
            $eta = ceil(($total-$progress)/60*static::$wait_between);
            $this->output->writeln(" ");
            $this->output->writeln("Phase 3 ---------- $progress / $total ---------- $proc% / $eta min");
        }

        // 4. table matrix up to 1000 calls
        $total_cities = FbeQuery::whereCountry($country)->whereIn('phase',['1','2'])->where('found','>',1)->count();
        $NR = floor($total_cities);
        $faktor = 0.1;

        $cities = collect( FbeQuery::whereCountry($country)
                                    ->whereIn('phase',['1','2'])
                                    ->where('found','>',99)
                                    ->orderBy('found',DESC)
                                    ->take($NR)
                                    ->get(['query'])
                            )->pluck('query')->toArray();

        $morewords = collect( FbeDict::top($country,$NR*2)
                                    ->whereNotIn('word',$cities)
                                    ->get(['word'])
                            )->pluck('word')->toArray();
        $words = [];
        foreach($morewords as $word){
            $has = FbeQuery::whereCountry($country)->whereQuery($word)->first()->found;
            if( $has>99 ){
                $words[] = $word;
            }
        }

        $matrix = [];
        foreach($words as $wi=>$word){
            foreach($cities as $ci=>$city){
                if( ( ($ci+1) * ($wi+1) )*$faktor < $NR ){
                    $keyword = $city.' '.$word;
                    $dict[$keyword] = 0;
                }
            }
        }

        foreach($words as $wi=>$word){
            foreach($cities as $ci=>$city){
                if( ( ($ci+1) * ($wi+1) )*$faktor < $NR ){
                    $keyword = $city.' '.$word;
                    $query = FbeQuery::register($country,$keyword,4);
                    $this->search($query);
                    $dict[$keyword] = 1;
                    $progress = array_sum($dict);
                    $total = count($dict);
                    $proc = round($progress/$total*100);
                    $eta = ceil(($total-$progress)/60*static::$wait_between);
                    $this->output->writeln(" ");
                    $this->output->writeln("Phase 4 ---------- $progress / $total ---------- $proc% / $eta min");
                }
            }
        }

        
        exit();

        //foreach($matrix as $keyword)

        // 4. try 30 most common words for each city (phase:1,2) with more then 90 results
        $cities = collect( FbeQuery::whereCountry($country)->where('found','>',89)->whereIn('phase',['1','2'])->get(['query']) )->pluck('query')->toArray();
        $words = collect( FbeDict::top($country,30)->whereNotIn('word',$cities)->get(['word']) )->pluck('word')->toArray();

        foreach($cities as $city){
            foreach($words as $word){
                $dict[$city.' '.$word];
                $query = FbeQuery::register($country,$word,4);
                $this->search($query);
            }
        }

        // 5. for each city with more then ?? results add 20 more word combinations
        $words = ['monday','friday','2016','party','festival','congress'];
        foreach($cities as $city){
            foreach($words as $word){
                $dict[$city.' '.$word];
            }
        }

        // 6. for each city with more then ?? results add 30 custom word combinations
        $words = ['monday','friday','2016','party','festival','congress'];
        foreach($cities as $city){
            foreach($words as $word){
                $dict[$city.' '.$word];
            }
        }

        // 7. country name + 100 top words
        $words = ['monday','friday','2016','party','festival','congress'];
        foreach($words as $word){
            $dict[$country.' '.$word];
        }

        // 8. done, move to next country in queue

        print_r($dict);
        exit();


            // refresh
            $words = collect( FbeDict::top($country,$take)->get(['word']) )->pluck('word')->toArray();
            $cities = collect(DB::select( DB::raw("
                    SELECT city, COUNT(*) AS c
                    FROM fbe_events
                    WHERE country='$country'
                    GROUP BY city
                    HAVING c>0
                    ORDER BY c DESC
                    LIMIT $take
                ") ) )->pluck('city')->toArray();

            // create temp matrix
            $matrix = [];
            foreach($words as $word){
                foreach($cities as $city){
                    $key = $word.' '.$city;
                    $matrix[$key] = 0;
                    if( isset(static::$matrix[$key]) ){
                        $matrix[$key] = static::$matrix[$key];
                    }
                }
            }
            



        $this->output->writeln("----------------------------------");
        $this->output->writeln("");
        $this->output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $this->output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');
    }


    public function getFacebookAccessToken($index=null)
    {
        if( !static::$tokens ){
            static::$tokens = config('facebook');
        }
        $tokens = static::$tokens;

        static::$fbi++;
        if( !is_null($index) ){
            static::$fbi = $index;
        }
        if( static::$fbi > count($tokens)-1 ){
            static::$fbi = 0;
        }
        $token = $tokens[static::$fbi];
        echo $token['name']."\n";

        return $token['fb_page_access_token'];
    }





    public function search($query)
    {
        if( $query->notChecked() ){

            $access_token = $access_token = $this->getFacebookAccessToken();

            $keywords = urlencode($query->query);

            $counter = 0;
            $daily_counter = CACHEDIR.'/'.date('Y-m-d').'_fbequery_count';
            if( file_exists($daily_counter) ){
                $counter = (int)file_get_contents($daily_counter);
            }
            $counter++;

            if( $counter>(20*500) ){ // 10000
                exit("Daily FbeQuery limit exceed");
            }else{
                file_put_contents($daily_counter, $counter);
            }
            $this->output->writeln(" ");
            $this->output->writeln($counter." SEARCHING: $keywords");
            $this->output->writeln(" ");
            
            $result = Remote::bee("https://graph.facebook.com/v2.6/search?q={$keywords}&type=event&access_token={$access_token}&fields=name,description,category,cover,place,start_time,end_time,ticket_uri,attending_count,declined_count,timezone,updated_time&limit=".static::$limit);

            $json = json_decode($result,true);

            // on error, dump and wait
            if( isset($json['error']) ){
                print_r($json);
                
                $query->status='err';
                $query->save();

                $wait = static::$wait_on_error;
                $this->output->writeln("");
                $this->output->write('Waiting '.$wait.'s ');
                foreach(range(1,$wait) as $seconds){
                    sleep(1);
                    $this->output->write('.');
                }
                return [
                    'error'=>$json['error'],
                    'query'=>$query
                ];
            }

            if( is_array($json['data']) ){
                $found = count($json['data']);
                foreach($json['data'] as $event){
                    $event = FbeEvent::import($event);
                    if( $event->is_new ){
                        $this->output->writeln("Found new ".strip_tags($event->name));
                    }else if( $event->is_changed ){
                        //$this->output->writeln("Changed ".strip_tags($event->name));
                    }
                    
                    /*
                    $out = $event->downloadCover();
                    if( !empty($out) ){
                        $this->output->writeln($out);
                    }
                    */
                }

                $query->done = date('Y-m-d H:i:s');
                $query->found = $found;
                $query->status='ok';
                $query->save();

                $wait = static::$wait_between;
                $this->output->writeln("");
                $this->output->write('Waiting '.$wait.'s ');
                foreach(range(1,$wait) as $seconds){
                    sleep(1);
                    $this->output->write('.');
                }

                return [
                    'found'=>count($json['data']),
                    'data'=>$json['data'],
                    'query'=>$query
                ];
            }
        }

        return ['query'=>$query];
    }

}