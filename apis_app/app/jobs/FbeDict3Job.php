<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion fbe:dict3
class FbeDict3Job extends Command
{
    static $fbi = 0;
    static $tokens = null;

    static $limit = 100;

    static $wait_between = 8;
    static $wait_on_error = 1800; // 60 * 30 min

    public $output;

    protected function configure()
    {
        $this
            ->setName('fbe:dict3')
            ->setDescription('Get facebook events within countries using dictionary attack v3')
            ->addArgument(
                'countries',
                InputArgument::OPTIONAL,
                'Countries'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '256M');

        $this->output = $output;
        
        $script = 'pid-fbe-dict3';
        if( file_exists(CACHEDIR.'/'.$script) ){
            $spid = file_get_contents(CACHEDIR.'/'.$script);
            $ret = shell_exec("ps aux | grep dict3");
            if( !empty($ret) and strpos($ret, $spid) ){
                $counter = 0;
                $daily_counter = CACHEDIR.'/'.date('Y-m-d').'_fbequery_count';
                if( file_exists($daily_counter) ){
                    $counter = (int)file_get_contents($daily_counter);
                }
                $this->output->writeln("Script already running in PID:$spid");
                $this->output->writeln("Daily counter: $counter");
                exit();
            }
        }

        $pid = getmypid();
        file_put_contents(CACHEDIR.'/'.$script, $pid);


        $limit = 100;
        $wait_max_sec = static::$wait_between;

        $countries = $input->getArgument('countries');
        if( empty($countries) ){
            $countries = config('crawler.event_countries');
        }
        $countries = explode(",",$countries);

        logme("STARTED fbe:dict3",$countries,"apis_app");

        // create dictionary queries
        /*
            1. country name
            2. all city names within the country
            3. for each city with more then limit/2 results add 10 more word combinations
            4. for each city with more then ?? results add 20 more word combinations
            5. for each city with more then ?? results add 30 custom word combinations
            6. country name + 100 top words
        */

        // 0. done, clean up past events and optimize database
        $this->output->writeln("----------------------------------");
        $this->output->writeln("REMOVING EVENTS WITHOUT country");
        $nocountry  = DB::select(DB::raw("DELETE FROM fbe_events WHERE country IS NULL"));
        
        $this->output->writeln("----------------------------------");
        $this->output->writeln("REMOVING EVENTS WITHOUT cover_source");
        $nocover    = DB::select(DB::raw("DELETE FROM fbe_events WHERE cover_source IS NULL"));
        
        $older_then_days = 30;
        $this->output->writeln("----------------------------------");
        $this->output->writeln("REMOVING PAST EVENTS OLDER THEN $older_then_days DAYS");
        $older    = DB::select(DB::raw("
                DELETE
                FROM fbe_events
                WHERE 
                      ( DATE(start_utc)<(NOW()-INTERVAL $older_then_days DAY) AND end_utc IS NULL )
                      OR
                      ( DATE(end_utc)<(NOW()-INTERVAL $older_then_days DAY) )
            "));
        

        $dict = [];


        // 1. country name
        foreach($countries as $country){
            $dict[$country][$country] = 0;
            $query = FbeQuery::register($country,$country,1);
            $this->search($query);
            $this->output->writeln(" ");
            $this->output->writeln("{$country} / P1");
        }

        // 2. all city names within the country
        foreach($countries as $country){
            if( $this->phase($country.'2') ){
                $cities = FbeCity::whereCountry($country)->get();
                foreach($cities as $city){
                    $dict[$country][$city->name] = 0;
                    $query = FbeQuery::register($country,$city->name,2);
                    $this->search($query);
                    $dict[$country][$city->name] = 1;

                    $progress = array_sum($dict[$country]);
                    $total = count($cities);
                    $proc = round($progress/$total*100);
                    $eta = ceil(($total-$progress)/60*static::$wait_between);
                    $this->output->writeln(" ");
                    $this->output->writeln("{$country} / P2 - $progress/$total - $proc% / $eta min | ".$city->name);
                }
                $this->phase($country.'2','DONE');
            }
        }
        logme("DONE fbe:dict3 Phase 2",$countries,"apis_app");

        // 3. try 100 most common word for each country
        foreach($countries as $country){
            if( $this->phase($country.'3') ){
                $words = collect( FbeDict::top($country,100)->get(['word']) )->pluck('word')->toArray();
                foreach($words as $word){
                    $dict[$country][$word] = 0;
                    $query = FbeQuery::register($country,$word,3);
                    $this->search($query);
                    $dict[$country][$word] = 1;

                    $progress = array_sum($dict[$country]);
                    $total = count($dict[$country])+count($words);
                    $proc = round($progress/$total*100);
                    $eta = ceil(($total-$progress)/60*static::$wait_between);
                    $this->output->writeln(" ");
                    $this->output->writeln("{$country} / P3 - $progress/$total - $proc% / $eta min | ".$word);
                }
                $this->phase($country.'3','DONE');
            }
        }
        logme("DONE fbe:dict3 Phase 3",$countries,"apis_app");

        // 4. try next 100 most common word for each country
        foreach($countries as $country){
            if( $this->phase($country.'4') ){
                $words = collect( FbeDict::top($country,100)->skip(100)->get(['word']) )->pluck('word')->toArray();
                foreach($words as $word){
                    $dict[$country][$word] = 0;
                    $query = FbeQuery::register($country,$word,4);
                    $this->search($query);
                    $dict[$country][$word] = 1;

                    $progress = array_sum($dict[$country]);
                    $total = count($dict[$country])+count($words);
                    $proc = round($progress/$total*100);
                    $eta = ceil(($total-$progress)/60*static::$wait_between);
                    $this->output->writeln(" ");
                    $this->output->writeln("{$country} / P4 - $progress/$total - $proc% / $eta min | ".$word);
                }
                $this->phase($country.'4','DONE');
            }
        }
        logme("DONE fbe:dict3 Phase 4",$countries,"apis_app");

        // 5. try next 100 most common word for each country
        foreach($countries as $country){
            if( $this->phase($country.'5') ){
                $words = collect( FbeDict::top($country,100)->skip(200)->get(['word']) )->pluck('word')->toArray();
                foreach($words as $word){
                    $dict[$country][$word] = 0;
                    $query = FbeQuery::register($country,$word,5);
                    $this->search($query);
                    $dict[$country][$word] = 1;

                    $progress = array_sum($dict[$country]);
                    $total = count($dict[$country])+count($words);
                    $proc = round($progress/$total*100);
                    $eta = ceil(($total-$progress)/60*static::$wait_between);
                    $this->output->writeln(" ");
                    $this->output->writeln("{$country} / P5 - $progress/$total - $proc% / $eta min | ".$word);
                }
                $this->phase($country.'5','DONE');
            }
        }
        logme("DONE fbe:dict3 Phase 5",$countries,"apis_app");


        // 6. combine 25x25 words where found 100 results
        if( $this->phase('combined'.'6') ){
            $words1 = collect( FbeQuery::where('found','=',100)
                                        ->wherePhase(3)
                                        ->take(25)
                                        ->orderBy('done',ASC)
                                        ->get(['query'])
                                )->pluck('query')->toArray();

            $words2 = collect( FbeDict::take(25)
                                        ->orderBy('found',DESC)
                                        ->distinct()
                                        ->get(['word'])
                                )->pluck('word')->toArray();

            foreach($words1 as $word1){
                $dict['combined'][$word1] = 0;
                foreach($words2 as $word2){
                    $combined = $word1." ".$word2;
                    $reversed = $word2." ".$word1;
                    if( 
                        $word1!==$word2 and 
                        ( !isset($dict['combined'][$combined]) or !isset($dict['combined'][$reversed]) )
                    ){
                        $dict['combined'][$combined] = 0;
                    }
                }
            }

            if( !is_array($dict['combined']) ){
                $dict['combined'] = [];
            }
            foreach($dict['combined'] as $word=>$done){
                $query = FbeQuery::register('combined',$word,6);
                $this->search($query);
                $dict['combined'][$word] = 1;
                
                $progress = array_sum($dict['combined']);
                $total = count($dict['combined'])+count($words);
                $proc = round($progress/$total*100);
                $eta = ceil(($total-$progress)/60*static::$wait_between);
                $this->output->writeln(" ");
                $this->output->writeln("COMBINED / P6 - $progress/$total - $proc% / $eta min | ".$word);
            }

            $this->phase('combined'.'6','DONE');
        }
        logme("DONE fbe:dict3 Phase 6",$countries,"apis_app");



        $this->phase('','RESET');

        $info = [
            'peak_memory'   => (memory_get_peak_usage()/1024/1024),
            'runtime'       => round(microtime(true)-ORION_STARTED,2)
        ];
        logme("FINISHED fbe:dict3",$info,"apis_app");


        // DONE
        $this->output->writeln("----------------------------------");
        $this->output->writeln("");
        $this->output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $this->output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');

        exit();





//************** solve, optimize & move above *********************




        // 4. table matrix up to 1000 calls
        $total_cities = FbeQuery::whereCountry($country)->whereIn('phase',['1','2'])->where('found','>',1)->count();
        $NR = floor($total_cities);
        $faktor = 0.1;

        $cities = collect( FbeQuery::whereCountry($country)
                                    ->whereIn('phase',['1','2'])
                                    ->where('found','>',99)
                                    ->orderBy('found',DESC)
                                    ->take($NR)
                                    ->get(['query'])
                            )->pluck('query')->toArray();

        $morewords = collect( FbeDict::top($country,$NR*2)
                                    ->whereNotIn('word',$cities)
                                    ->get(['word'])
                            )->pluck('word')->toArray();
        $words = [];
        foreach($morewords as $word){
            $has = FbeQuery::whereCountry($country)->whereQuery($word)->first()->found;
            if( $has>99 ){
                $words[] = $word;
            }
        }

        $matrix = [];
        foreach($words as $wi=>$word){
            foreach($cities as $ci=>$city){
                if( ( ($ci+1) * ($wi+1) )*$faktor < $NR ){
                    $keyword = $city.' '.$word;
                    $dict[$keyword] = 0;
                }
            }
        }

        foreach($words as $wi=>$word){
            foreach($cities as $ci=>$city){
                if( ( ($ci+1) * ($wi+1) )*$faktor < $NR ){
                    $keyword = $city.' '.$word;
                    $query = FbeQuery::register($country,$keyword,4);
                    $this->search($query);
                    $dict[$keyword] = 1;
                    $progress = array_sum($dict);
                    $total = count($dict);
                    $proc = round($progress/$total*100);
                    $eta = ceil(($total-$progress)/60*static::$wait_between);
                    $this->output->writeln(" ");
                    $this->output->writeln("Phase 4 ---------- $progress / $total ---------- $proc% / $eta min");
                }
            }
        }



        // *********************************************************************

        $this->output->writeln("----------------------------------");
        $this->output->writeln("");
        $this->output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $this->output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');  

        exit();
        
        // *********************************************************************



        //foreach($matrix as $keyword)

        // 4. try 30 most common words for each city (phase:1,2) with more then 90 results
        $cities = collect( FbeQuery::whereCountry($country)->where('found','>',89)->whereIn('phase',['1','2'])->get(['query']) )->pluck('query')->toArray();
        $words = collect( FbeDict::top($country,30)->whereNotIn('word',$cities)->get(['word']) )->pluck('word')->toArray();

        foreach($cities as $city){
            foreach($words as $word){
                $dict[$city.' '.$word];
                $query = FbeQuery::register($country,$word,4);
                $this->search($query);
            }
        }

        // 5. for each city with more then ?? results add 20 more word combinations
        $words = ['monday','friday','2016','party','festival','congress'];
        foreach($cities as $city){
            foreach($words as $word){
                $dict[$city.' '.$word];
            }
        }

        // 6. for each city with more then ?? results add 30 custom word combinations
        $words = ['monday','friday','2016','party','festival','congress'];
        foreach($cities as $city){
            foreach($words as $word){
                $dict[$city.' '.$word];
            }
        }

        // 7. country name + 100 top words
        $words = ['monday','friday','2016','party','festival','congress'];
        foreach($words as $word){
            $dict[$country.' '.$word];
        }

        print_r($dict);
        exit();


            // refresh
            $words = collect( FbeDict::top($country,$take)->get(['word']) )->pluck('word')->toArray();
            $cities = collect(DB::select( DB::raw("
                    SELECT city, COUNT(*) AS c
                    FROM fbe_events
                    WHERE country='$country'
                    GROUP BY city
                    HAVING c>0
                    ORDER BY c DESC
                    LIMIT $take
                ") ) )->pluck('city')->toArray();

            // create temp matrix
            $matrix = [];
            foreach($words as $word){
                foreach($cities as $city){
                    $key = $word.' '.$city;
                    $matrix[$key] = 0;
                    if( isset(static::$matrix[$key]) ){
                        $matrix[$key] = static::$matrix[$key];
                    }
                }
            }
            



        $this->output->writeln("----------------------------------");
        $this->output->writeln("");
        $this->output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $this->output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');
    }


    public function getFacebookAccessToken($index=null)
    {
        if( !static::$tokens ){
            static::$tokens = config('facebook');
            foreach(static::$tokens as $fbi=>$token){
                static::$tokens[$fbi]['error'] = 0;
            }
        }
        
        $tokens = static::$tokens;
        $total_tokens = count($tokens);

        static::$fbi++;
        if( !is_null($index) ){
            static::$fbi = $index;
        }
        if( static::$fbi > $total_tokens-1 ){
            static::$fbi = 0;
        }

        $loop_protect = 0;
        while( static::$tokens[static::$fbi]['error']>5 ){
            static::$tokens[static::$fbi]['error']--;
            static::$fbi++;
            $loop_protect++;
            if( $loop_protect>=$total_tokens ){
                @mail('tihomir.jauk@gmail.com', "Error in fbe:dict3", $counter." Loop protection activated!" );
                print_r(static::$tokens);
                exit("Loop protection activated");
            }
        }
        
        $token = $tokens[static::$fbi];
        echo $token['name']."\n";
        return $token['fb_page_access_token'];
    }




    public function search($query)
    {
        if( $query->notChecked() ){

            $access_token = $this->getFacebookAccessToken();
            if( empty($access_token) ){
                $access_token = $this->getFacebookAccessToken();
            }

            $keywords = trim(urlencode($query->query));
            if( empty($keywords) ){
                $query->done = date('Y-m-d H:i:s');
                $query->found = 0;
                $query->status='ok';
                $query->save();
                return [
                    'found'=>0,
                    'data'=>[],
                    'query'=>$query
                ];
            }


            $counter = 0;
            $daily_counter = CACHEDIR.'/'.date('Y-m-d').'_fbequery_count';
            if( file_exists($daily_counter) ){
                $counter = (int)file_get_contents($daily_counter);
            }
            $counter++;

            if( $counter>(20*600) ){ // 12000
                exit("Daily FbeQuery limit exceed");
            }else{
                file_put_contents($daily_counter, $counter);
            }
            $this->output->writeln(" ");
            $this->output->writeln($counter." SEARCHING: $keywords");
            $this->output->writeln(" ");
            
            $result = Remote::bee("https://graph.facebook.com/v2.6/search?q={$keywords}&type=event&access_token={$access_token}&fields=name,description,category,cover,place,start_time,end_time,ticket_uri,attending_count,declined_count,timezone,updated_time&limit=".static::$limit);
            if( getenv('APP_DEBUG')=='true' ){
                print_r($result);
            }

            $json = json_decode($result,true);

            // on error, dump and wait
            if( isset($json['error']) ){
                print_r($json);
                
                $query->status='err';
                $query->save();



                $wait = static::$wait_on_error;
                $wait = 10;

                // Don't need those emails any more
                //@mail('tihomir.jauk@gmail.com', "Error in fbe:dict3", $counter." SEARCHING: $keywords<br>\n".json_encode($json)."\n".static::$tokens[static::$fbi]['name'] );

                // increase delay between calls to graph api
                // static::$wait_between++;
                
                // mark this token had error
                static::$tokens[static::$fbi]['error']++;


                $this->output->writeln("");
                $this->output->write('Waiting '.$wait.'s ');
                foreach(range(1,$wait) as $seconds){
                    sleep(1);
                    $this->output->write('.');
                }
                return [
                    'error'=>$json['error'],
                    'query'=>$query
                ];
            }

            if( is_array($json['data']) ){
                $found = count($json['data']);
                foreach($json['data'] as $event){
                    $event = FbeEvent::import($event);
                    if( $event->is_new ){
                        $this->output->writeln("Found new ".strip_tags($event->name));
                    }else if( $event->is_changed ){
                        //$this->output->writeln("Changed ".strip_tags($event->name));
                    }else if( $event->is_spam ){
                        
                    }
                    
                    /*
                    $out = $event->downloadCover();
                    if( !empty($out) ){
                        $this->output->writeln($out);
                    }
                    */
                }

                $query->done = date('Y-m-d H:i:s');
                $query->found = $found;
                $query->status='ok';
                $query->save();

                $wait = static::$wait_between;
                $this->output->writeln("");
                $this->output->write('Waiting '.$wait.'s ');
                foreach(range(1,$wait) as $seconds){
                    sleep(1);
                    $this->output->write('.');
                }

                return [
                    'found'=>count($json['data']),
                    'data'=>$json['data'],
                    'query'=>$query
                ];
            }
        }

        return ['query'=>$query];
    }

    public function phase($str,$done=false)
    {
        $path = CACHEDIR.'/dict3-phases';
        // progress, phase skip
        $phases = [];
        if( file_exists($path) ){
            $phases = explode("\n",file_get_contents($path));
        }
        if( $done=='RESET' ){
            $phases = [];
            file_put_contents($path, implode("\n",$phases) );
            return true;
        }
        if( $done=='DONE' or $done!==false ){
            $phases[] = $str;
            file_put_contents($path, implode("\n",$phases) );
            return true;
        }

        if( in_array($str, $phases) ){
            return false;
        }
        return true;
    }

}