<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Helper\Table;

// Example: orion fbe:duplicates
class FbeDuplicatesJob extends Command
{

    protected function configure()
    {
        $this
            ->setName('fbe:duplicates')
            ->setDescription('Detect and remove duplicate events')
            ->addArgument(
                'country',
                InputArgument::OPTIONAL,
                'Country'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '128M');

        $country = $input->getArgument('country');
        if( !empty($country) ){
            $countries = [$country];
        }

        $potential = DB::select(DB::raw("
            SELECT *, COUNT(*) AS c
            FROM fbe_events
            WHERE
                duplicate IS NULL
                AND address IS NOT NULL
                AND country='{$country}'
            GROUP BY start_time,address,city,country
            HAVING c>1
        "));

        foreach($potential as $pot){

            $output->writeln($pot->id." : ".$pot->name);
            $start_stamp = strtotime($pot->start_utc);
            $duplicates = FbeEvent::where('start_utc','=',$pot->start_utc) // not working here
                                    ->whereAddress($pot->address)
                                    ->whereCity($pot->city)
                                    ->whereCountry($pot->country)
                                    ->get();
            
            $max = 0;
            $max_id = 0;
            $score = [];
            foreach($duplicates as $dup){
                $dup->calculateQualityScore();
                if( $dup->quality>$max ){
                    $max = $dup->quality;
                    $max_id = $dup->id;
                }
                $output->writeln("D: {$dup->id} Q:{$dup->quality} : ".$dup->name);
                //$dup->duplicate = $pot->id;
                //$dup->save();
            }
            foreach($duplicates as $dup){
                if( $dup->id<>$max_id ){
                    $dup->duplicate = $max_id;
                    $dup->save();
                }
            }
            //exit("---");
        }

        $output->writeln("");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');
    }

}