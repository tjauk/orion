<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Helper\Table;

// Example: orion fbe:totals
class FbeTotalsJob extends Command
{

    protected function configure()
    {
        $this
            ->setName('fbe:totals')
            ->setDescription('Show total number of upcoming events per countries')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $sql = "
SELECT country, COUNT(*) AS c
FROM fbe_events
WHERE
     DATE(start_time)>=CURRENT_DATE()
     AND
     cover_source IS NOT NULL
GROUP BY country
HAVING c>100
ORDER BY c DESC";

        $results = DB::select(DB::raw($sql));
        $rows = [];
        foreach($results as $result){
            $rows[] = [$result->country,$result->c];
        }

        $table = new Table($output);
        $table
            ->setHeaders(['Country', 'Upcoming events'])
            ->setRows($rows)
        ;
        $table->render();

        $output->writeln("");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');
    }

}