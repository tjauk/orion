<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion import:seatwave
class ImportSeatwaveJob extends Command
{
    static $fbi = 0;
    static $tokens = null;

    static $limit = 100;

    static $wait_between = 8;
    static $wait_on_error = 1800; // 60 * 30 min

    public $output;

    protected function configure()
    {
        $this
            ->setName('events:seatwave')
            ->setDescription('Import SeatWave events')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '256M');



        /*
        
        Create SeatWaveAPI class

        1. Import Genre (map to EventCategories)
        2. Import Categories (use as additional tags maybe)
        3. Discover events
        4. Import Venue info by VenueId
        5. Import Event by EventGroupId
        
        */


        // DONE
        $output->writeln("----------------------------------");
        $output->writeln("");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');

    }



}