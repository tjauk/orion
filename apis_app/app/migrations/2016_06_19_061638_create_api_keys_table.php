<?php

class CreateApiKeysTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_keys', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->dateTime('expires_at')->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('key')->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('api_keys');
    }

}
