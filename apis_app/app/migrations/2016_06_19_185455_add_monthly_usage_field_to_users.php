<?php

class AddMonthlyUsageFieldToUsers extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->integer('plan_id')->default(1);
            $table->integer('requests')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('plan_id');
            $table->dropColumn('requests');
        });
    }

}
