<?php

class CreateAccessTokensTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_tokens', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('token')->unique();
            $table->integer('user_id')->unsigned();
            $table->dateTime('expires_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('access_tokens');
    }

}
