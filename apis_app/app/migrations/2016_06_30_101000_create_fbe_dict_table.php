<?php

class CreateFbeDictTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fbe_dict', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('country')->nullable();
            $table->string('word')->default('');
            $table->integer('found')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fbe_dict');
    }

}
