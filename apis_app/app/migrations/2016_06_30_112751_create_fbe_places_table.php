<?php

class CreateFbePlacesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fbe_places', function($table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            
            $table->bigInteger('fbid')->unsigned()->unique();
            $table->string('name')->default('');
            
            $table->string('status')->nullable();
            $table->string('page')->nullable();

            $table->string('address')->nullable();
            $table->string('zip')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            
            $table->decimal('lat',10,8)->nullable();
            $table->decimal('lon',11,8)->nullable();
            
            $table->string('category')->nullable();
            $table->string('category_id')->nullable();
            $table->string('category_name')->nullable();
            $table->string('category_tags')->nullable();

            $table->string('jsonhash')->default('');
            $table->dateTime('changed_at')->nullable();

            $table->integer('crawled')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fbe_places');
    }

}
