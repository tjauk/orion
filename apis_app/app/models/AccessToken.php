<?php
class AccessToken extends Model
{
    protected $table = 'access_tokens';

    protected $fillable = [
		'token',
		'user_id',
		'expires_at'
    ];

    /*
     * Relations
     */
    public function user()
    {
        return $this->belongsTo('User');
    }

    /*
     * Boot, register events
     */
    static function boot(){

        AccessToken::creating(function($object){
            if( empty($object->token) ){
                $object->token = AccessToken::generate();
            }
        });

    }



    /*
     * Generate
     */
    static function generate($length = 64)
    {
        do {
            $key = Str::random($length,'alnum');
            $unique = static::whereToken($key)->count();
        } while( $unique>0 );

        return $key;
    }


    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopeValid($query)
    {
        $query->where(function($query){
            $query->orWhere('expires_at', '>', \DB::raw('NOW()'));
            $query->orWhereNull('expires_at');
        });
    }


    /*
     * Getters
     */
    public function getExpiresAtAttribute()
    {
        if( empty($this->attributes['expires_at']) ){
            return '';
        }
        return date('Y-m-d',strtotime($this->attributes['expires_at']));
    }

    /*
     * Setters
     */
    public function setExpiresAtAttribute($time)
    {
        if( empty($time) ){
            $time = null;
        }else{
            $time = date( 'Y-m-d H:i:00', strtotime($time) );
        }
        $this->attributes['expires_at'] = $time;
    }

    public function setTokenAttribute($str)
    {
        if( empty($str) ){
            $str = static::generate();
        }
        $this->attributes['token'] = $str;
    }

}
