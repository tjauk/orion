<?php
class ApiKey extends Model
{
    protected $table = 'api_keys';

    protected $fillable = [
		'expires_at',
		'user_id',
		'key'
    ];

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
        'user_id',
    ];

    /*
     * Relations
     */
    public function user()
    {
        return $this->belongsTo('User');
    }


    /*
     * Boot, register events
     */
    static function boot(){

        ApiKey::creating(function($object){
            if( empty($object->key) ){
                $object->key = ApiKey::generate();
            }
        });

    }


    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopeValid($query)
    {
        $query->where('expires_at', '>', \DB::raw('NOW()'));
    }

    /*
     * Generate
     */
    static function generate($length = 64)
    {
        do {
            $key = Str::random($length,'alnum');
            $unique = static::whereKey($key)->count();
        } while( $unique>0 );

        return $key;
    }


    /*
     * Getters
     */



    /*
     * Setters
     */
    public function setKeyAttribute($str)
    {
        if( empty($str) ){
            $str = static::generate();
        }
        $this->attributes['key'] = $str;
    }


}
