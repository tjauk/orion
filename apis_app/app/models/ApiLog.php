<?php
class ApiLog extends Model
{
    protected $table = 'api_logs';

    public $fillable = [
        'ip_address',
		'method',
		'url',
		'params',
		'ip_address',
        'access_token',
		'user_id',
		'lat',
		'lon'
    ];

    protected $casts = [
        'params' => 'json'
    ];

    /*
     * Relations
     */
    public function user()
    {
        return $this->belongsTo('User');
    }


    static function request()
    {
        $request = Request::make();

        $apilog = new ApiLog();
        $apilog->ip_address = Request::ip();
        $apilog->method = Request::method();
        $apilog->url = $request->getPathInfo();
        $params = array_merge(Request::get(),Request::post());
        $apilog->params = $params;

        $fields = $apilog->fillable;
        unset($fields['ip_address']);
        unset($fields['method']);
        unset($fields['url']);
        unset($fields['params']);

        foreach($fields as $field){
            if( isset($params[$field]) ){
                $apilog->$field = $params[$field];
            }
        }

        $apilog->save();
    }



    /*
     * Options
     */


    /*
     * Scopes
     */


    /*
     * Getters
     */


    /*
     * Setters
     */


}
