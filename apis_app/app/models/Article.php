<?php
class Article extends Model {
	
	protected $fillable = ['title'];

	public function getLinkAttribute()
	{
		return '/article/view/'.$this->id;
	}
}