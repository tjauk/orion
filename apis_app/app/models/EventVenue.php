<?php
class EventVenue extends Model
{
    protected $table = 'event_venue';

    protected $fillable = [
        'src',
        'src_id',

        'name',
        'description',
        'photo',

        'address',
        'zip',
        'city',
        'state',
        'country',

        'lat',
        'lon',

        'json',
        'jsonhash',
        'changed_at'
    ];

    public $is_new = false;
    public $is_changed = false;


    /*
     * Importer
     */
    static function import($data,$src='SeatWave')
    {
        if( empty($data['id']) ){
            return false;
        }

        $json = json_encode($data);
        $jsonhash = md5($json);

        $data['src'] = $src;
        $data['json'] = $json;
        $data['jsonhash'] = $jsonhash;

        $class = "\\Importers\\".$src."\\Venue";
        $importer = new $class;
        $venue = $importer->import($data);
        
        return $venue;
    }

}
