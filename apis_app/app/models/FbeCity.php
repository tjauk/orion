<?php
class FbeCity extends Model
{
    protected $table = 'fbe_cities';

    protected $fillable = [
		'country',
		'region',
		'name'
    ];

    public $is_new = false;
    public $is_changed = false;


    /*
     * Importer
     */
    static function import($data)
    {
        $country = $data['country_name'];
        $region = $data['region'];
        $name = $data['name'];

        $city = static::whereCountry($country)->whereName($name)->first();
        if( $city->id>0 ){
        }else{
            $city = new static;
            $city->is_new = true;
            $city->country = $country;
            if( !empty($region) ){
                $city->region = $region;
            }
            $city->name = $name;
            $city->save();
        }
        return $city;
    }

}
