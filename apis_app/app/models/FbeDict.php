<?php
class FbeDict extends Model
{
    protected $table = 'fbe_dict';

    protected $fillable = [
        'country',
        'word',
        'found'
    ];

    /*
     * Scopes
     */
    public function scopeTop($query,$country,$take=100)
    {
        $query->where('country', '=', $country )->orderBy('found','DESC')->take($take);
    }

}
