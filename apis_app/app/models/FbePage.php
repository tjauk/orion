<?php
class FbePage extends Model
{
    protected $table = 'fbe_pages';

    protected $fillable = [
		'name',
		'about',
		'description',
        'likes',
		'phone',
		'website',

		'jsonhash',
		'changed_at',
		'crawled'
    ];


    /*
     * Getters
     */


    /*
     * Setters
     */


}
