<?php
class FbeQuery extends Model
{
    protected $table = 'fbe_queries';

    protected $fillable = [
		'country',
		'query',
		'done',
		'found',
        'phase',
        'status',
    ];

    /**
     * Scopes
     */
    public function scopeNext($query,$country,$take=1)
    {
        $query->where( DB::raw('DATE(done)'), '<>', DB::raw('CURRENT_DATE()') )
                ->whereCountry($country)
                ->take($take);
    }

    public function scopeToday($query)
    {
        $query->where( DB::raw('DATE(done)'), '<', DB::raw('CURRENT_DATE()') );
    }


    /*
     * Getters
     */
    public function notChecked()
    {
        // true will check
        // false will skip
        
        // don't check blacklisted words
        $explicit_words = [
            'vagina','fuck','fetish','erotic','sex','penis','vagina','blowjob','escort','whore','anal','gaysail'
        ];
        foreach($explicit_words as $word){
            if( strtolower($this->query)==$word){
                return false;
            }
        }
        //

        $done = $this->done; // -1 day ?
        if( empty($done) ){
            return true;   
        }
        if( time() > strtotime($done." +1 days") ){
            if( $this->found>90 ){
                return true;
            }
            if( $this->found>70 and time()>strtotime($done." +2 days") ){
                return true;
            }
            if( $this->found>25 and time()>strtotime($done." +3 days") ){
                return true;
            }
            if( $this->found>0 and time()>strtotime($done." +4 days") ){
                return true;
            }
            if( $this->found==0 and time()>strtotime($done." +7 days") ){
                return true;
            }
        }
        return false;
    }


    /*
     * Setters
     */
    static function register($country,$query,$phase=0)
    {
        $item = static::whereQuery($query)->first();//whereCountry($country)->
        if( $item->id > 0 ){
        
        }else{
            $item = new static;
            $item->country = $country;
            $item->query = $query;
            $item->phase = $phase;
            $item->status = 'new';
            $item->save();
        }
        return $item;
    }


}
