<?php
class Place extends Model
{
    protected $table = 'places';

    protected $fillable = [
		'name',
		'slug',
		'address',
		'zip',
		'city',
		'country_id',
		'lat',
		'lon',
		'alt',
		'radius'
    ];


    /*
     * Relations
     */
    public function country()
    {
        return $this->belongsTo('Country');
    }


    /*
     * Scopes
     */


    /*
     * Getters
     */


    /*
     * Setters
     */
    public function setSlugAttribute($str)
    {
        if( $str=='' ){
            $str=$this->name;
        }
        $this->attributes['slug'] = str_slug($str);
    }
    public function setLatAttribute($str)
    {
        if( $str=='' ){
            $str=null;
        }
        $this->attributes['lat'] = $str;
    }
    public function setLonAttribute($str)
    {
        if( $str=='' ){
            $str=null;
        }
        $this->attributes['lon'] = $str;
    }



}
