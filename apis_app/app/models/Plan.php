<?php
class Plan extends Model
{
    protected $table = 'plans';

    protected $fillable = [
		'name',
		'period',
		'price',
		'requests'
    ];

    /*
     * Relations
     */



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */


    /*
     * Setters
     */


}
