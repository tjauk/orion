<?php 

class User extends Model {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $UserGroups = array();
	protected $Permissions = null;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 
		'email',
		'password',
		'usergroup',
		'first_name',
		'last_name',
		'plan_id',
		'requests'
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'email', 'remember_token'];


    /*
     * Relations
     */
    public function plan()
    {
        return $this->belongsTo('Plan','plan_id');
    }



	public function can($permission){
		if( empty($this->UserGroups) ){
			$usergroups = explode(',',$this->usergroup);
			
			foreach($usergroups as $usergroup){
				$this->UserGroups[] = UserGroup::findByName($usergroup);
			}
		}
		$valid = false;
		foreach($this->UserGroups as $UserGroup){
			if( is_object($UserGroup) and $UserGroup->has($permission) ){
				$valid = true;
			}
		}
		return $valid;
	}

	/*
	 * Options
	 */
	static function options()
	{
		$options = [''=>'---'];
		if( Auth::user()->can('admin') ){
			$items = static::orderBy('last_name')->get();
		}else{
			$items = [Auth::user()];
		}
		
		foreach($items as $item){
			if( !empty($item->email) ){
				$options[$item->id] = $item->full_title;
			}
		};
		return $options;
	}


	/*
	 * Get full_name
	 */
	public function getFullNameAttribute(){
		return $this->last_name.' '.$this->first_name;
	}

	/*
	 * Get full_title and (email)
	 */
	public function getFullTitleAttribute(){
		return $this->last_name.' '.$this->first_name.' ('.$this->email.')';
	}

	/*
	 * Setters
	 */
	public function setPasswordAttribute($password)
	{
		$this->attributes['password'] = Hash::make($password);
	}

	
}
