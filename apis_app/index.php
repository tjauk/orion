<?php
/**
 * Orion - A PHP Framework
 *
 * @package  Trinium
 * @author   Tihomir Jauk <tihomirjauk@gmail.com>
 */

/*
|--------------------------------------------------------------------------
| Bootstrap framework
|--------------------------------------------------------------------------
*/
define( 'ROOTDIR', __DIR__ );
define( 'ROOTURL', $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'] );
define( 'APPDIR', __DIR__.'/app' );
define( 'CACHEDIR', __DIR__.'/cache' );

if( file_exists("C:/_WWW/orion/pp") or isset($_REQUEST['access_token']) ){
	require __DIR__.'/../framework/bootstrap.php';
}else{
	echo "Comming soon";
}
