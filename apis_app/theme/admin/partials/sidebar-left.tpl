<!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">{{"sidebar.MAIN NAVIGATION"|t}}</li>
            
            <li>
              <a href="/admin/dashboard">
                <i class="fa fa-dashboard"></i> <span>{{"sidebar.Dashboard"|t}}</span>
              </a>
            </li>

            <li class="treeview">
              <a href="/admin/user">
                <i class="fa fa-user"></i> <span>{{"sidebar.Users"|t}}</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li class="active"><a href="/admin/user"><i class="fa fa-circle-o"></i> {{"sidebar.Users"|t}}</a></li>
                  <li><a href="/admin/usergroup"><i class="fa fa-circle-o"></i> {{"sidebar.Users groups"|t}}</a></li>
              </ul>
            </li>

            <!-- in case free -->
            <li>
              <a href="/admin/beaconcatcher">
                <i class="fa fa-dashboard"></i> <span>Upgrade</span>
              </a>
            </li>

            <li>
              <a href="/admin/subscription">
                <i class="fa fa-dashboard"></i> <span>Subscription Plan</span>
              </a>
            </li>

            @if Auth:can('admin')
            <li>
              <a href="/admin/plan">
                <i class="fa fa-dashboard"></i> <span>Plans</span>
              </a>
            </li>
            @end

            <li>
              <a href="/admin/account">
                <i class="fa fa-dashboard"></i> <span>Account</span>
              </a>
            </li>

            <li>
              <a href="/admin/payment">
                <i class="fa fa-dashboard"></i> <span>Payment</span>
              </a>
            </li>

            <li>
              <a href="/admin/api-usage">
                <i class="fa fa-dashboard"></i> <span>API Usage</span>
              </a>
            </li>

            @if Auth:can('admin')
            <li>
              <a href="/admin/access-token">
                <i class="fa fa-dashboard"></i> <span>API Access Tokens</span>
              </a>
            </li>
            @end

            @if Auth:can('admin')
            <li>
              <a href="/admin/api-log">
                <i class="fa fa-dashboard"></i> <span>API Log</span>
              </a>
            </li>
            @end

            <li>
              <a href="/admin/user/logout">
                <i class="fa fa-lock"></i> <span>Log Out</span>
              </a>
            </li>


            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>

          </ul>