<div class="container">
	<div class="row">

	<div class="col-md-3">
		<nav id="navbar-docs"
			class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix"
			 data-spy="affix" data-offset-top="160" data-offset-bottom="200"
		>
			<ul>
				<li><a href="#doc_start">Getting started</a></li>
				<li><a href="#doc_syntax">Syntax</a></li>
				<li><a href="#doc_example">Example</a></li>
				<li><a href="#">Getting started</a></li>
				<li><a href="#">Getting started</a></li>
				<li><a href="#">Getting started</a></li>
			</ul>
		</nav>
	</div>

	<div class="col-md-9">

	<h1 id="doc_start">Documentation</h1>
	<p>Lorem ipsum dolor sit amet</p>
	<p>
	Syntax Check<br>
	Free Basic Pro Enterprise<br>
	<br>
	When requesting the verification of an email address the mailboxlayer API will first and foremost perform a standard email syntax check, hence, verify its compliance with regular expression rules, such as:<br>
	<br>
	The format of email addresses is local-part@domain where the local-part may be up to 64 characters long and the domain name may have a maximum of 253 characters. The formal definitions are in RFC 5322 and RFC 5321 – with a more readable form given in the informational RFC 3696.<br>
	<br>
	Local part:<br>
	<br>
	    Uppercase and lowercase Latin letters (A–Z, a–z) (ASCII: 65–90, 97–122)<br>
	    Digits 0 to 9 (ASCII: 48–57)<br>
	    These special characters: # - _ ~ ! $ & ' ( ) * + , ; = : and percentile encoding i.e. %20<br>
	    Character . (dot, period, full stop), ASCII 46, provided that it is not the first or last character, and provided also that it does not appear consecutively (e.g. John..Doe@example.com is not allowed).<br>
	    [...]<br>
	    <br>
	Domain part:<br>
	<br>
	The domain name part of an email address has to conform to strict guidelines: it must match the requirements for a hostname, consisting of letters, digits, hyphens and dots. In addition, the domain part may be an IP address literal, surrounded by square braces, although this is rarely seen except in email spam.<br>
	<br>
	<h3 id="doc_example">Example:</h3>
	<br>
	<br>
	The API response's format_valid JSON object will return true or false depending on whether or not the email's syntax is found valid.<br>
	</p>

	<p>
	<h3 id="doc_syntax">Syntax Check</h3>
	<br>
	Free Basic Pro Enterprise<br>
	<br>
	When requesting the verification of an email address the mailboxlayer API will first and foremost perform a standard email syntax check, hence, verify its compliance with regular expression rules, such as:<br>
	<br>
	The format of email addresses is local-part@domain where the local-part may be up to 64 characters long and the domain name may have a maximum of 253 characters. The formal definitions are in RFC 5322 and RFC 5321 – with a more readable form given in the informational RFC 3696.<br>
	<br>
	Local part:<br>
	<br>
	    Uppercase and lowercase Latin letters (A–Z, a–z) (ASCII: 65–90, 97–122)<br>
	    Digits 0 to 9 (ASCII: 48–57)<br>
	    These special characters: # - _ ~ ! $ & ' ( ) * + , ; = : and percentile encoding i.e. %20<br>
	    Character . (dot, period, full stop), ASCII 46, provided that it is not the first or last character, and provided also that it does not appear consecutively (e.g. John..Doe@example.com is not allowed).<br>
	    [...]<br>
	    <br>
	Domain part:<br>
	<br>
	The domain name part of an email address has to conform to strict guidelines: it must match the requirements for a hostname, consisting of letters, digits, hyphens and dots. In addition, the domain part may be an IP address literal, surrounded by square braces, although this is rarely seen except in email spam.<br>
	<br>
	Example:<br>
	<br>
	The API response's format_valid JSON object will return true or false depending on whether or not the email's syntax is found valid.<br>
	</p>

	</div>
	</div>
</div>