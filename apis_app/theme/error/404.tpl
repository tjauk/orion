 <section class="hgroup">
      <div class="container">
           <h1>Page not found</h1>
           <ul class="breadcrumb pull-right">
                <li class="active">Something went wrong!</li>
           </ul>
      </div>
 </section>
 <section class="call_to_action four-o-four"> <i class="fa fa-ambulance"></i>
      <div class="container">
           <h3>error 404 is nothing to really worry about...</h3>
           <h4>you may have mis-typed the URL, please check your spelling and try again.</h4>
      </div>     
 </section>