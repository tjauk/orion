<!--BEGIN:header-->
<header>
     <div class="container">
          <div class="navbar navbar-default" role="navigation">
               <div class="navbar-header">
                    <a class="navbar-brand" href="/">
                         <img src="/theme/images/globe.png" alt="optional logo" height="90" width="90" style="margin-right:12px;">
                         <span class="logo_title">Net{API}s</span>
                         <span class="logo_subtitle">for professional developers</span>
                    </a>
                    <a class="btn btn-navbar btn-default navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                         <span class="nb_left pull-left"><span class="fa fa-reorder"></span></span>
                         <span class="nb_right pull-right"></span>
                    </a>
               </div>

               @inc "menu-main"
          </div>

          <!--
          <div id="social_media_wrapper">
               <a href="https://www.facebook.com"><i class="fa fa-facebook"></i></a>
               <a href="https://twitter.com/leonartgr"><i class="fa fa-twitter"></i></a>
               <a href="#googleplus"><i class="fa fa-google-plus"></i></a>
          </div>
          -->
          @if Auth:check()
          <div id="sign" class="text-right">
               <a href="/signup" class="btn btn-primary"><i class="fa fa-user"></i><span>My account</span></a>
               <a href="/admin" class="btn btn-primary"><i class="fa fa-user"></i><span>Admin</span></a>
               <a href="/user/logout" class="btn btn-primary"><i class="fa fa-user"></i><span>Log Out</span></a>
          </div>
          @else
          <div id="sign" class="text-right">
               <a href="/signup" class="btn btn-primary"><i class="fa fa-user"></i><span>FREE Sign Up</span></a>
               <a href="/signin" class="btn btn-primary"><i class="fa fa-user"></i><span>Log In</span></a>
          </div>
          @end

     </div>
</header>
<!--END:header-->