<!--LAYOUT:layouts/chipoteka-->
<!DOCTYPE html>
<html lang="en" ng-app>
    <head>

        <title>{{doc.title}}</title>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {{doc.meta}}

        <link rel="icon" type="image/png" href="/theme/favicon.png">

        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,900,700,400italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link href="/theme/css/bootstrap.min.css" rel="stylesheet">
        <link href="/theme/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="/theme/font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet">
        @less "styles.less"
        {{doc.css}}

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        @asset "jquery"
        @asset "vue"
        @js "bootstrap.min.js"
        {{doc.js}}

    </head>

    <body>

            <!-- header -->
            <header id="header" class="row">

                <div class="container">

<nav class="navbar navbar-default">
  <div class="container-fluid">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed btn-primary" data-toggle="collapse" data-target="#bs-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Brand</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">111 Link <span class="sr-only">(current)</span></a></li>
        <li><a href="#">Link</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>

        <ul class="nav navbar-nav navbar-right">
            <li><a href="/documentation"><i class="fa fa-credit-card"></i><span> Documentation</span></a></li>
            @if Auth:guest()
                <li><a href="/user/register"><i class="fa fa-user"></i> Use it for FREE</a></li>
                <li><a href="/user/login"><i class="fa fa-sign-in"></i> Login</a></li>
                </li>
            @else
                <li><a href="/account/index"><i class="fa fa-user"></i> My Account</a></li>
                <li class="toggle-help"><a href="/user/help"><i class="fa fa-file-text-o"></i> pomoć</a>
                    <div id="menu-help">

                    </div>
                </li>
                @if Auth:can('admin')
                    <li><a href="/admin/dashboard"><i class="fa fa-sign-out"></i> admin</a></li>

                @end
                <li><a href="/user/logout"><i class="fa fa-sign-out"></i> Sign Out</a></li>
            @end
        </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<!--
                                        <li><a href="/about"><i class="fa fa-truck"></i><span> About</span></a></li>
                                        <li><a href="/faq"><i class="fa fa-comments"></i><span> FAQ</span></a></li>
                                        <li><a href="/contact"><i class="fa fa-envelope"></i><span> Contact</span></a></li>
                                        <li><a href="/api/countries"><i class="fa fa-building-o"></i><span> Test API</span></a></li>

                                        
-->
                    <div class="navbar-header">
                    <a class="navbar-brand" href="/"><h1>APIs</h1></a>
                    </div>

                        

                </div>

            </header>

            <!-- main -->
            <div id="main" class="row">
                <div class="container">
                    <section id="content" class="content-wrapper col-md-12">
                        {{CONTENT}}
                    </section>
                </div>
            </div>

            <!-- footer -->
            <div class="container">
                    @inc "footer"
            </div>


        {{doc.jsend}}
    </body>
</html>
