<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<title>{{doc.title}}</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">
{{doc.meta}}

<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,300italic" rel="stylesheet" type="text/css">
<link href="/theme/css/animate.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/theme/js/woothemes-FlexSlider-06b12f8/flexslider.css" type="text/css" media="screen">
<link rel="stylesheet" href="theme/js/prettyPhoto_3.1.5/prettyPhoto.css" type="text/css" media="screen">
<link href="/theme/style.css" rel="stylesheet" type="text/css">
<link href="/theme/font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet">
{{doc.css}}

<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/theme/images/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/theme/images/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="/theme/images/apple-touch-fa-57x57-precomposed.png">
<link rel="shortcut icon" href="/theme/images/favicon.png">
<script type="text/javascript" src="/theme/js/modernizr.custom.48287.js"></script>

@asset "jquery"
{{doc.js}}

</head>
<body class="collapsing_header">

@inc "header"

@inc "home-slider"

<div class="main">
     
     <section class="call_to_action">
          <div class="container">
               <h3>focus on what’s important</h3>
               <h4>and  make the web a little bit  prettier</h4>
               <a class="btn btn-primary btn-lg" href="#">Buy this theme!</a>
          </div>     
     </section>
     <section class="features_teasers_wrapper">
          <div class="container">
               <div class="row">
                    <div class="feature_teaser col-sm-4 col-md-4"> <img alt="responsive" src="/theme/images/responsive.png">
                         <h3>Clean, Responsive Design</h3>
                         <p>A multipurpose but mainly<strong> business oriented</strong> design, built to serve as a foundation for your web projects. Suspendisse nec risus fermentum sapien congue fermentum sed at lorem.</p>
                    </div>
                    <div class="feature_teaser col-sm-4 col-md-4"> <img alt="responsive" src="/theme/images/git.png">
                         <h3>Based on Twitter Bootstrap</h3>
                         <p>The front-end development framework with a <strong>steep learning curve</strong>. It changes the way you develop sites. Suspendisse nec risus fermentum sapien congue fermentum sed at lorem.</p>
                    </div>
                    <div class="feature_teaser col-sm-4 col-md-4"> <img alt="responsive" src="/theme/images/less.png">
                         <h3>Makes real use of {LESS}</h3>
                         <p><strong>Restart</strong> comes with a style.less file that tries to use all the power of <strong>{less} and bootstrap combined</strong>. Suspendisse nec risus fermentum sapien congue fermentum sed at lorem.</p>
                    </div>
               </div>
          </div>
     </section>
     <section class="portfolio_teasers_wrapper">
          <div class="container">
               <h2 class="section_header fancy centered">Recent Work or Projects from Portfolio<small>we take pride in our work</small></h2>
               <div class="portfolio_strict row">
                    <div class="col-sm-4 col-md-4">
                         <div class="portfolio_item wow animated flipInX"> <a href="portfolio_item.html" data-path-hover="M 180,190 0,158 0,0 180,0 z">
                              <figure style="background-image:url(/theme/images/portfolio/p4.jpg)">
                                   <svg viewBox="0 0 180 320" preserveAspectRatio="none">
                                        <path d="M 180,0 0,0 0,0 180,0 z"/>
                                   </svg>
                                   <figcaption>
                                        <p>Description of the project dapibus, tellus ac cursus commodo, mauesris condime ntum nibh, ut fermentum....</p>
                                        <div class="view_button">View</div>
                                   </figcaption>
                              </figure>
                              </a>
                              <div class="portfolio_description">
                                   <h3><a href="portfolio_item.html">The {re}start Project</a></h3>
                                   <p>Design</p>
                              </div>
                         </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                         <div class="portfolio_item wow animated flipInX"> <a href="portfolio_item.html" data-path-hover="M 180,190 0,158 0,0 180,0 z">
                              <figure style="background-image:url(/theme/images/portfolio/a5.jpg)">
                                   <svg viewBox="0 0 180 320" preserveAspectRatio="none">
                                        <path d="M 180,0 0,0 0,0 180,0 z"/>
                                   </svg>
                                   <figcaption>
                                        <p>Description of the project dapibus, tellus ac cursus commodo, mauesris condime ntum nibh, ut fermentum....</p>
                                        <div class="view_button">View</div>
                                   </figcaption>
                              </figure>
                              </a>
                              <div class="portfolio_description">
                                   <h3><a href="portfolio_item.html">Colorfull iOS Apps</a></h3>
                                   <p>Development</p>
                              </div>
                         </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                         <div class="portfolio_item wow animated flipInX"> <a href="portfolio_item.html" data-path-hover="M 180,190 0,158 0,0 180,0 z">
                              <figure style="background-image:url(/theme/images/portfolio/p1.jpg)">
                                   <svg viewBox="0 0 180 320" preserveAspectRatio="none">
                                        <path d="M 180,0 0,0 0,0 180,0 z"/>
                                   </svg>
                                   <figcaption>
                                        <p>Description of the project dapibus, tellus ac cursus commodo, mauesris condime ntum nibh, ut fermentum....</p>
                                        <div class="view_button">View</div>
                                   </figcaption>
                              </figure>
                              </a>
                              <div class="portfolio_description">
                                   <h3><a href="portfolio_item.html">POETIC Magazine</a></h3>
                                   <p>Illustrations</p>
                              </div>
                         </div>
                    </div>
               </div>
          </div>     
     </section>
     <section class="clients_section wow animated fadeInUp">
          <div class="container">
               <h2 class="section_header elegant centered">Our clients<small>all of them are satisfied</small></h2>
               <div class="clients_list"> <a href="#"><img src="/theme/images/clients/wordpress.jpg" alt="client"></a> <a href="#"><img src="/theme/images/clients/jquery.jpg" alt="client"></a> <a href="#"><img src="/theme/images/clients/microlancer.jpg" alt="client"></a> <a href="#"><img src="/theme/images/clients/bbpress.jpg" alt="client"></a> <a href="#"><img src="/theme/images/clients/wpml.jpg" alt="client"></a> </div>
          </div>     
     </section>

     @inc "footer"
     
</div>

<script src="/theme/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/theme/js/woothemes-FlexSlider-06b12f8/jquery.flexslider-min.js"></script>
<script src="/theme/js/prettyPhoto_3.1.5/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="/theme/js/isotope/jquery.isotope.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/theme/js/jquery.ui.totop.js"></script>
<script type="text/javascript" src="/theme/js/easing.js"></script>
<script type="text/javascript" src="/theme/js/wow.min.js"></script>
<script type="text/javascript" src="/theme/js/snap.svg-min.js"></script>
<script type="text/javascript" src="/theme/js/restart_theme.js"></script>
<!-- <script type="text/javascript" src="/theme/js/collapser.js"></script> -->
{{doc.jsend}}
</body>
</html>