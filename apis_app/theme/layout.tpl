<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<title>{{doc.title}}</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="robots" content="noindex,nofollow">
{{doc.meta}}

<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,300italic" rel="stylesheet" type="text/css">
<link href="/theme/css/animate.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/theme/js/woothemes-FlexSlider-06b12f8/flexslider.css" type="text/css" media="screen">
<link rel="stylesheet" href="theme/js/prettyPhoto_3.1.5/prettyPhoto.css" type="text/css" media="screen">
<link href="/theme/style.css" rel="stylesheet" type="text/css">
<link href="/theme/font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet">
{{doc.css}}

<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/theme/images/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/theme/images/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="/theme/images/apple-touch-fa-57x57-precomposed.png">
<link rel="shortcut icon" href="/theme/images/favicon.png">
<script type="text/javascript" src="/theme/js/modernizr.custom.48287.js"></script>

@asset "jquery"
{{doc.js}}

</head>
<body class="collapsing_header">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-79996782-1', 'auto');
  ga('send', 'pageview');

</script>
@inc "header"

<div class="main">
     
{{CONTENT}}

     @inc "footer"
</div>

<script src="/theme/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/theme/js/woothemes-FlexSlider-06b12f8/jquery.flexslider-min.js"></script>
<script src="/theme/js/prettyPhoto_3.1.5/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="/theme/js/isotope/jquery.isotope.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/theme/js/jquery.ui.totop.js"></script>
<script type="text/javascript" src="/theme/js/easing.js"></script>
<script type="text/javascript" src="/theme/js/wow.min.js"></script>
<script type="text/javascript" src="/theme/js/snap.svg-min.js"></script>
<script type="text/javascript" src="/theme/js/restart_theme.js"></script>
<!-- <script type="text/javascript" src="/theme/js/collapser.js"></script> -->
{{doc.jsend}}
<!-- Begin Cookie Consent script http://cookie-consent.org/ -->
<script type="text/javascript">
    window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":null,"theme":"dark-bottom"}; var cookieconsent_id = '050b931c-9d9d-472e-9e5d-0323de8a492a';</script>

<script type="text/javascript" src="//cdn.front.to/libs/cookieconsent.min.4.js"></script>

<noscript><a href="http://cookie-consent.org/">EU cookie consent script</a></noscript>
<!-- End Cookie Consent script -->
</body>
</html>