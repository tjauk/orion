<html>
<head>
	<title>Ponuda: { order.mark }</title>
    <meta charset="utf-8">
	<style>
* {
	font-family: dejavusans;
	font-size: 14px;
}
.title {
	font-size: 24px;
	font-weight: bold;
}
.subtitle {
	font-size: 16px;
}
p {
	padding: 0;
}
h2 {
	font-size: 18px;
	line-height: 20px;
}
	</style>
</head>
<body>

<table>
	<tr>
		<td style="width:640px;padding:20px 50px;">

<table border=0>
	<tr>
		<td style="width:320px;font-size:12px;">
				<span class="subtitle"><b>{ seller.name }</b></span><br>
				OIB: { seller.oib }<br>
				{ seller.address }<br>
				{ seller.zip } { seller.place } , { seller.country }<br>
		</td>
		<td style="width:320px">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<br>
			<p style="font-size:24px;line-height:22px">Ponuda : { order.mark }</p>
			<br>
		</td>
	</tr>
	<tr>
		<td valign="top">	
			<p>
				<span style="font-size:18px;line-height:18px"><b>{ order.info.buyer.name }</b></span><br>
				{ order.info.buyer.address }<br>
				{ order.info.buyer.zip } { order.info.buyer.place }<br>
				{ order.info.buyer.country }<br>
				@not order.info.buyer.oib empty
					OIB: { order.info.buyer.oib }<br>
				@end
				<br>
				@not order.info.buyer.email empty
					E-mail: { order.info.buyer.email }<br>
				@end
				@not order.info.buyer.phone empty
					Telefon: { order.info.buyer.phone }<br>
				@end
			</p>
		</td>
		<td valign="top" style="text-align:right;">
			<p>
				Datum izdavanja: { order.created_at | date "d.m.Y." }<br>
				<br>
				IBAN račun: { seller.ziro }<br>
				Poziv na broj: <?php echo substr($order['mark'],1); ?><br>
				<br>
				<b>Ponuda vrijedi: 7 dana.</b><br>
					
				<br>
				<?php if( $order['info']['agent']['id']>1){ ?>
					Prodajni zastupnik:<br>
					{ order.info.agent.name } { order.info.agent.surname }<br>
					{ order.info.agent.phone }<br>
					<br>
				<?php }; ?>
			</p>
		</td>
	</tr>

</table>


<br>
<br>
<br>

<table cellpadding="3">
	<thead>
		<tr style="color:#fff;background-color:#444;">
			<th style="width:70px;padding:2px;">Šifra</th>
			<th style="width:220px;padding:2px;">Naziv</th>
			<th style="width:100px;padding:2px;text-align:right;">Jed.cijena</th>
			<th style="width:100px;padding:2px;text-align:right;">Količina</th>
			<th style="width:100px;padding:2px;text-align:right;">Cijena</th>
		</tr>
	</thead>
	
	<tbody>
	@loop order.info.items as item
		<tr>
			<td>{ item.product.code }</td>
			<td>{ item.product.title }</td>
			<td style="text-align:right;">{ item.product.vpc|price }</td>
			<td style="text-align:right;">{ item.quantity }</td>
			<td style="text-align:right;"><?php echo Num::price( $item['product']['vpc'] * $item['quantity'] ); ?></td>
		</tr>
	@end
		<tr style="border-top:solid 2px #000">
			<td colspan="3">&nbsp;</td>
			<td style="text-align:right;"><b>Ukupno bez PDV-a:</b></td>
			<td style="text-align:right;"><b>{ order.info.total_netto|price } kn</b></td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
			<td style="text-align:right;"><b>Iznos PDV-a (25%):</b></td>
			<td style="text-align:right;"><b>{ order.info.total_tax|price } kn</b></td>
		</tr>
		<tr style="border-top:solid 1px #000">
			<td colspan="3">&nbsp;</td>
			<td style="text-align:right;"><h3>Ukupno za platiti:</h3></td>
			<td style="text-align:right;"><h3>{ order.info.total|price } kn</h3></td>
		</tr>
	</tbody>
</table>

<p>
	<br>
	<br>
	Ponudu možete platiti na IBAN račun { seller.ziro }.<br>
	Obavezno navedite u pozivu na broj : <?php echo substr($order['mark'],1); ?>
</p>

		</td>
	</tr>
</table>
</body>
</html>