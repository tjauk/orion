<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  {{doc.meta}}

    <!-- Site Properties -->
  <title>{{doc.title}}</title>

  <link rel="icon" type="image/png" href="/theme/favicon.png">

  <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,900,700,400italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

  <link href="/theme/font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet">

  @css "semantic.min.css"

  {{doc.css}}

  <style type="text/css">

    .hidden.menu {
        display: none;
    }

    .masthead.segment {
      min-height: 100px;
      padding: 1em 0em;
    }
    .masthead .logo.item img {
      margin-right: 1em;
    }
    .masthead .ui.menu .ui.button {
      margin-left: 0.5em;
    }
    .masthead h1.ui.header {
      margin-top: 2em;
      margin-bottom: 0em;
      font-size: 4em;
      font-weight: normal;
    }
    .masthead h2 {
      font-size: 1.7em;
      font-weight: normal;
    }
    .masthead {
        background: #000 url("/theme/images/bgx2.jpg") no-repeat top center fixed !important;
    }

    .ui.vertical.stripe {
      padding: 8em 0em;
    }
    .ui.vertical.stripe h3 {
      font-size: 2em;
    }
    .ui.vertical.stripe .button + h3,
    .ui.vertical.stripe p + h3 {
      margin-top: 3em;
    }
    .ui.vertical.stripe .floated.image {
      clear: both;
    }
    .ui.vertical.stripe p {
      font-size: 1.33em;
    }
    .ui.vertical.stripe .horizontal.divider {
      margin: 3em 0em;
    }

    .quote.stripe.segment {
      padding: 0em;
    }
    .quote.stripe.segment .grid .column {
      padding-top: 5em;
      padding-bottom: 5em;
    }

    .footer.segment {
      padding: 5em 0em;
    }

    .secondary.pointing.menu .toc.item {
      display: none;
    }

    @media only screen and (max-width: 700px) {
      .ui.fixed.menu {
        display: none !important;
      }
      .secondary.pointing.menu .item,
      .secondary.pointing.menu .menu {
        display: none;
      }
      .secondary.pointing.menu .toc.item {
        display: block;
      }
      .masthead.segment {
        min-height: 250px;
      }
      .masthead h1.ui.header {
        font-size: 2em;
        margin-top: 1.5em;
      }
      .masthead h2 {
        margin-top: 0.5em;
        font-size: 1.5em;
      }
    }


  </style>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        @asset "jquery"
        @js "semantic.min.js"
        {{doc.js}}


  <script>
  $(document)
    .ready(function() {

      // fix menu when passed
      $('.masthead')
        .visibility({
          once: false,
          onBottomPassed: function() {
            $('.fixed.menu').transition('fade in');
          },
          onBottomPassedReverse: function() {
            $('.fixed.menu').transition('fade out');
          }
        })
      ;

      // create sidebar and attach to menu open
      $('.ui.sidebar')
        .sidebar('attach events', '.toc.item')
      ;

    })
  ;
  </script>
</head>
<body>

<!-- Following Menu -->
<div class="ui large top fixed hidden menu">
  <div class="ui container">
    <a href="/" class="active item">Our APIs</a>
    <a href="/about" class="item">About</a>
    <a href="/faq" class="item">Faq</a>
    <div class="right menu">
        <a href="/documentation" class="item">Documentation</a>
        <div class="item">
            <a href="/signup" class="ui primary button">Use it for FREE</a>
        </div>
        <div class="item">
            <a href="/signin" class="ui button">Sign in</a>
        </div>
    </div>
  </div>
</div>

<!-- Sidebar Menu -->
<div class="ui vertical inverted sidebar menu">
  <a href="/" class="active item">Our APIs</a>
  <a href="/about" class="item">About</a>
  <a href="/faq" class="item">Faq</a>
  <a href="/documentation" class="item">Documentation</a>
  <a href="/signup" class="item">Use it for FREE</a>
  <a href="/signin" class="item">Sign in</a>
</div>


<!-- Page Contents -->
<div class="pusher">


  <div class="ui inverted vertical masthead center aligned segment">
    <div class="ui container">
      <div class="ui large secondary inverted pointing menu">
        <a class="toc item">
            <div class="ui inverted button"><i class="fa fa-bars"></i></div>
        </a>
        <a href="/" class="active item">Our APIs</a>
        <a href="/documentation" class="item">Documentation</a>
        <a href="/about" class="item">About</a>
        <a href="/faq" class="item">Faq</a>
        <div class="right item">
          <a href="/signup" class="ui inverted button">FREE Sign up</a>
          <a href="/signin" class="ui inverted button">Sign in</a>
        </div>
      </div>
    </div>
  </div>


  <div class="ui container segment">
      {{CONTENT}}
  </div>

  @inc "footer"

</div>

</body>

</html>
