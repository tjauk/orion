<div class="container">
    <h1>Terms of Use & API License Agreement</h1>

    <p><em>Version 1.1, last updated: 08-20-2015</em></p>

    <h1>Overview</h1>
        
        <p>All use of the website, information, services, and content distributed through or in conjunction with or made available on the website is offered to you subject to your acceptance of these Terms & Conditions, our <a class="blue_link" href="/privacy">Privacy Policy</a> and <a class="blue_link" href="/permitted-prohibited-use">Permitted and Prohibited Uses Policy</a>. By purchasing any goods or services from this website, you are furthermore bound by this site's <a class="blue_link" href="/service-agreement">Service Agreement</a>.</p>
        
        <p>The following terms and conditions incorporate a legal agreement between you (“You”) and apilayer GmbH (“apilayer”, “EarthAPI”) and set forth the terms and conditions by which apilayer will make the EarthAPI API service available in order to electronically provide You with use of and access to EarthAPI Data & Services. </p>
        
        <h4>EarthAPI API Services</h4>
        <p>For the purposes of this Agreement, “EarthAPI API Services” shall consist of: 
        <ul class="mt-5">
            <li>all  data and information feeds, media, software, and/or electronic documentation made available or supplied by apilayer or its licensees and</li>
            <li>certain other software, data, and/or documents supplied by apilayer or its licensees to facilitate use of such data or services. “EarthAPI API Data and Services” means apilayer’s service or application programming interface that allows You and other authorized licensees to access EarthAPI API Data & Services and make use of EarthAPI API Services. This Agreement includes all amendments, addendums, and revisions to the terms herein in effect between You and apilayer from time to time.</li> 
        </ul>
        </p>
        
        <h4>Acknowledgement</h4>
        <p>By accessing or otherwise using the EarthAPI API, You acknowledge that You: 
        <ul class="mt-5">
            <li>have read all of the terms and conditions of this Agreement;</li>
            <li>agree to be bound by the terms and conditions of this Agreement; </li>
            <li>agree to check the EarthAPI API pages from time to time for any updates or amendments to the terms and conditions of this Agreement; and,</li>
            <li>have the power and authority to enter into this Agreement. If You do not acknowledge the above, You may not access or use the EarthAPI API. If You are dissatisfied with the terms, conditions, rules, policies, guidelines, or practices of apilayer, Your sole and exclusive remedy is to discontinue accessing or otherwise using the EarthAPI API.</li>
        </ul> 
        </p>
                                 
     <h1>Company Information</h1>
     
        <p>www.EarthAPI.com is a site operated by apilayer, an Austrian technology company registered in Vienna, Austria at the following location:</p>
        
        <div class="inline"><strong>Vienna Office (HQ)</strong><br>Hietzinger Hauptstraße 153/6<br>1130 Vienna<br>Austria</div>
        
        <br>

     <h1>Scope of License</h1>
     
        <h4>Grant of License</h4>
        <p>apilayer hereby grants to You a non-transferable, non-exclusive, revocable, and limited license for the term of this Agreement to access the EarthAPI API and use EarthAPI API Data & Services strictly for the limited purposes, and under the terms and conditions, as are set forth in this Agreement.</p>
        
        <h4>Scope of the EarthAPI API License</h4>
        <p>Subject to Your compliance with the terms of this Agreement, You are hereby permitted to receive, process, and display EarthAPI API Data & Services to individual end-users of your application(s), provided 
        <ul class="mt-5">
            <li>such end users use EarthAPI API Data & Services strictly for their own personal use,</li>
            <li>You do not permit Your end users to store, distribute, or otherwise exploit EarthAPI API Data & Services for any other purposes.</li>
        </ul>
        </p>
        
        <p>For clarity, and without limiting the forgoing, You are permitted solely to use EarthAPI API Data & Services for reference by Your end users. Except as expressly provided herein, under no circumstances whatsoever may You transfer or permit the transfer of EarthAPI API Data & Services outside of your application(s).</p>
        
     <h1>Use of License</h1>
     
        <h4>Restrictions</h4>
        <p>You shall not: 
        <ul class="mt-5">
            <li>reproduce, copy, modify, distribute, market, display, transfer, sublicense, assign, prepare derivative work(s) or adaptation(s) based on, sell, exchange, barter or transfer, rent, lease, loan, time-share, resell, or in any other manner exploit EarthAPI API Data & Services or the EarthAPI API;</li>
            <li>make access to EarthAPI API Data & Services or the EarthAPI API available to unauthorized parties, or otherwise post or distribute EarthAPI API Data & Services in any public forum including, but not limited to, print, newsletters, radio, television, or internet; </li>
            <li>remove, obscure, or alter any required notice or any notice of apilayer’s intellectual property rights present on or in the EarthAPI API, including, but not limited to, copyright, trademark, and/or patent notices; </li>
            <li>disassemble, translate, reverse engineer or otherwise decompile EarthAPI API Data & Services or the EarthAPI API; </li>
            <li>knowingly cause or permit anything that will impair, jeopardize, violate, or infringe the intellectual property rights of apilayer in EarthAPI API Data & Services or the EarthAPI API; </li>
            <li>directly or indirectly dispute or contest the ownership, validity, or enforceability of the intellectual property rights of apilayer in EarthAPI API Data & Services or the EarthAPI API, or assist any party to do so; </li>
            <li>through Your use of EarthAPI API Data & Services or the EarthAPI API, knowingly cause or permit anything that You know or ought to know will prejudice or hamper the reputation or goodwill of apilayer; </li>
            <li>claim, use, or apply to register, record, or file any trade mark, trade name, copyright, or design that is identical or confusingly similar to apilayer’s trademarks, or assist any other party to do so; </li>
            <li>use, store, or access any EarthAPI API Data & Services, or the EarthAPI API after the termination of this Agreement; </li>
            <li>use or permit others to use, market, distribute, or export EarthAPI API Data & Services, or the EarthAPI API; or, </li>
            <li>use EarthAPI API Data & Services or the EarthAPI API in any manner or for any purpose that violates any law, regulation or right of any person.</li>
        </ul>
        </p>

        <h4>Attribution</h4>
        <p>While usage of the EarthAPI API is not subject to Your end users being made aware by You that apilayer is supplying the functionality you are making use of in your application(s), Your inclusion of a clickable hyperlink to the EarthAPI API website at <em>https://EarthAPI.com</em> would be highly appreciated.</p>
        
        <h4>Your Responsibility to Safeguard</h4>
        <p>You shall use Your best efforts and take all necessary steps to safeguard EarthAPI API Data & Services to ensure that no unauthorized reproduction, publication, disclosure, modification, distribution, or other use of any EarthAPI API Data & Services is made in whole or in part. To the extent that You become aware of any such unauthorized use of EarthAPI API Data & Services, You shall immediately notify apilayer by emailing “support@apilayer.com”. </p>
        
        <h4>Misuse of the Licensed Product</h4>
        <p>In the event that You or any end users use EarthAPI API Data & Services or the EarthAPI API in violation of this Agreement, as applicable, apilayer shall have the option of electing liquidated damages.</p>

        <h1>Services and Data</h1>
     
        <h4>Use of the EarthAPI API</h4>
        <p>Subject to the terms herein, apilayer will provide You with access to EarthAPI API Data & Services over the Internet through the EarthAPI API. All calls to the EarthAPI API must reference the credentials issued to You by apilayer. You may not disclose Your credentials to any other party. You are solely responsible for ensuring the secrecy and security of Your credentials and will be responsible for all activities that occur using such credentials. You shall not use the EarthAPI API in a manner that exceeds reasonable request volume or constitutes excessive or abusive usage.</p>
     
        <h4>Usage Limits</h4>
        <p>The extent of Your usage of the EarthAPI API is dependent on Your subscription plan. You shall not exceed the usage limits associated with Your account. Unused amounts of call usage will not be carried over from one monthly period to the next. If You exceed Your monthly usage limit, the EarthAPI API will return an error message stating that Your monthly usage limit has been reached and no further API calls can be made within the current monthly term. It is Your sole responsibility to ensure that your application(s) properly detect and handle any returned error messages. apilayer will make reasonable efforts to help You identify and resolve usage problems, or to review the suitability of Your current subscription plan for Your needs. apilayer reserves the right to limit or throttle the number of network calls available to Your application(s) if apilayer (in its sole discretion) believes that such calls are being made for malicious reasons, or as the result of a technical error.</p>
     
        <h4>Responsibility for Software and Hardware</h4>
        <p>You shall be solely responsible for acquiring, providing, and maintaining all software/hardware and Internet Services required to access the EarthAPI API, including but not limited to telecommunication and internet connections, ISP, web browsers, and/or other equipment and software required to access and use the EarthAPI API.</p>
     
        <h4>Services May Be Inaccessible Or Inoperable</h4>
        <p>You hereby acknowledge that from time to time, the EarthAPI API may be inaccessible or inoperable for any reason, including, without limitation: 
        <ul class="mt-5">
            <li>equipment (hardware) malfunctions; </li>
            <li>software malfunctions; </li>
            <li>periodic maintenance procedures or repairs which apilayer may undertake from time to time; and/or, </li>
            <li>causes beyond the reasonable control of apilayer and/or not reasonably foreseeable by apilayer.</li>
        </ul>
        </p>
     
        <h4>Service Level Agreement</h4>
        <p>If the EarthAPI API is unavailable to all EarthAPI API customers for more than three consecutive hours in any one calendar day, beginning at 12:00:01 AM London time (GMT + 01:00) and ending at 11:59:59 PM London time (GMT + 01:00) of the same day, an outage will be deemed to have occurred. If there are more than three outages within any one calendar week, beginning at 12:00:01 AM London time (GMT + 01:00) on a Monday and ending at 11:59:59 PM London time (GMT + 01:00) on a Sunday, then You may terminate this Agreement, without liability, by providing written notice to apilayer of Your intention to do so within one calendar week of the third outage. Upon such termination being accepted by apilayer, You shall be entitled to a refund equal to the amount You have paid for the latest term.</p>
     
        <h4>Right to Change Content or Format</h4>
        <p>You acknowledge that apilayer may from time to time, and in its sole discretion, update or change the content or format of EarthAPI API  Data or the EarthAPI API. apilayer shall strive to provide reasonable advance notification of such changes; however, from time to time sudden changes may be required, and since apilayer strives to accurately reflect these and adjust to these changes, significant advance notice is not always possible. You may be required to use the most recent version of the EarthAPI API in order to ensure functionality of Your application(s) with EarthAPI API Data & Services.</p>
     
     <h1>Publicity</h1>
     
     	<p>Each Subscriber is permitted to state publicly that such Subscriber is a Subscriber of the Services. Each Subscriber agrees that EarthAPI (apilayer) may include such Subscriber’s name and trademarks in a list of EarthAPI (apilayer) Subscribers, online or in promotional materials. Each Subscriber also agrees that EarthAPI (apilayer) may verbally reference such Subscriber as a Subscriber of the Services. Subscriber may opt out of the provisions in this section by e-mailing a request to <a class="blue_link fw_400" href="mailto:support@apilayer.com?subject=[EarthAPI] Subject:">support@apilayer.com</a>.</p>
     
     <h1>Proprietary Rights</h1>
     
        <h4>Trademarks</h4>
        <p>"EarthAPI", "EarthAPI.com", the EarthAPI logo and other identifying marks of EarthAPI are and shall remain the trademarks and trade names and exclusive property of apilayer, and any unauthorized use of these marks is unlawful. Other trade-marks on the Services are the property of their respective owners.</p>
     
        <h4>Licensed, Not Sold</h4>
        <p>Use of EarthAPI API Data & Services and the EarthAPI API is licensed, and not sold.</p>
     
        <h4>Consent to Collection, Use, Retention, and Disclosure of Information</h4>
        <p>In the course of providing the EarthAPI API, apilayer collects information, including standard information of the type normally exchanged when accessing any web site or web service. This information pertains only to the access of the EarthAPI API, and is no different in nature to the information available to any Internet web server during the course of fulfilling a standard web request. Please read apilayer’s Privacy Policy. By agreeing to the terms of this Agreement, You are expressly consenting to apilayer’s collection, use, retention, and disclosure of information as set out in its Privacy Policy.</p>
     
        <h4>No Granting of Ownership Rights</h4>
        <p>You acknowledge that apilayer owns all right, title, and interest (including but not limited to all copyright rights therein) in and to the EarthAPI API Data & Services or the EarthAPI API, and that You shall not take any action inconsistent with such ownership. Nothing contained in this Agreement shall be construed as granting You or any end users any ownership rights in or to any EarthAPI API Data & Services or the EarthAPI API.</p>
     
        <h4>apilayer’s Right to Terminate</h4>
        <p>apilayer reserves the right at any time to terminate this Agreement, or withdraw from distributing EarthAPI API Data & Services or providing access to the EarthAPI API, if for any reason it no longer retains the right to publish or distribute the EarthAPI API Data & Services.</p>
     
     <h1>Disclaimer</h1>
     
        <h4>Provided in Good Faith</h4>
        <p>The EarthAPI API is provided by apilayer in good faith and although apilayer endeavours to ensure that EarthAPI API Data & Services and the EarthAPI API are accurate and obtained from sources believed to be reliable, apilayer does not guarantee or warrant: 
        <ul class="mt-5">
            <li>the accuracy, authenticity, timeliness, reliability, appropriateness, correct sequencing, or completeness of EarthAPI API Data & Services or the EarthAPI API; or 
            <li>that the EarthAPI API Data & Services or the EarthAPI API is free from errors or other material defects.
        </ul>
        </p>
     
        <h4>Disclaimer</h4>
        <p>The EarthAPI API and EarthAPI API Data & Services are provided “as is” and “as available”, without any warranty of any kind and apilayer, its affiliates, agents and/or suppliers, officers, directors, and employees expressly disclaim any and all warranties, whether expressed or implied, including without limitation the warranties of merchantability, fitness for a particular purpose, and non-infringement of the rights of third parties. In no event shall apilayer be liable for indirect, special, punitive, or consequential damages including, but not limited to, lost profits, lost data, or otherwise. In no event shall apilayer, any of its affiliates, or any of their data providers involved in supplying, developing, operating, or managing the EarthAPI API, be liable for indirect, special, punitive, or consequential damages including, but not limited to, lost profits, lost data, or otherwise. In the event the limitations and disclaimers contained in this Article are found by a court of competent jurisdiction to be ineffective or inapplicable, the parties agree that apilayer’s maximum aggregate liability to You and all end users, whether arising in contract, tort, strict liability, or otherwise, shall be limited, and in no event shall it exceed, the amount of fees paid by You for the current term.</p>
     
              
        <h4>Disclaimer</h4>
        <p>apilayer does not represent or warrant that the EarthAPI API, or access to the EarthAPI API, will be uninterrupted or error-free or that errors in the EarthAPI API and/or EarthAPI API Data & Services will be corrected.</p>
     
        <h4>Disclaimer</h4>
        <p>In no event shall apilayer, any of its affiliates, or any of their data providers involved in supplying, developing, operating, or managing the EarthAPI API be liable to You or to any end users in any manner whatsoever for any interruptions, delays, the unavailability or inoperability, inaccuracies, errors, or omissions, regardless of cause, in the EarthAPI API and/or the EarthAPI API Data & Services or for any losses, damages, liabilities or expenses resulting therefrom regardless of cause, even if apilayer knew or should have known of the possibility of or could have prevented such damages.</p>
     
        <h4>Indemnification by You</h4>
        <p>You shall indemnify, defend, and save harmless apilayer, its affiliates and subsidiaries, and their officers, directors, employees and agents from any loss, damage, liability or expense (including reasonable attorneys’ fees, accountants’ fees and other legal expenses) to which any of them may become subject to and which are in any way related to or which have arisen under or in connection with: 
        <ul class="mt-5">
            <li>any act or omission by You or any person connected, affiliated, or associated with You with respect to this Agreement and/or the EarthAPI API; and/or,</li> 
            <li>any non-fulfillment or breach of any covenant or agreement on Your part pursuant to this Agreement or any incorrectness in or breach of any representation or warranty made by You contained in this Agreement.</li>
        </ul>
        </p>
     
        
     <h1>General</h1>
     
        <p>These Terms & Conditions incorporate by reference all notices and disclaimers contained in the Services constitute the entire agreement between you and EarthAPI with respect to access to, and use of, the Services. Except as specifically set forth herein, these Terms & Conditions supersede any prior agreements, including prior oral and/or written statements or representations not contained herein, between you and EarthAPI relating to the Services. This agreement is not intended to create a partnership, joint venture or agency relationship between you and EarthAPI.</p>
        
        <p>If any provision of these Terms & Conditions is invalid, illegal or unenforceable in any respect under any applicable statute or rule of law, the provision shall be deemed omitted to the extent that it is invalid, illegal or unenforceable. In such a case, the remaining provisions of these Terms & Conditions shall be construed in a manner as to give greatest effect to the original intention of the parties hereto. EarthAPI's failure to insist upon, or enforce, strict performance of any right or provision of these Terms & Conditions shall not constitute or be construed or deemed to be a waiver of such right or provision in the future or a waiver of any other right or provision under these Terms & Conditions.</p> 
        
        <p>You agree that these Terms & Conditions, EarthAPI's Privacy Policy and other legal documents posted through the Services have been drawn up in English. Although translations in other languages of any of the foregoing documents may be available, such translations may not be up to date or complete. Accordingly, you agree that in the event of any conflict between the English language version of the foregoing documents and any other translations thereto, the English language version of such documents shall govern.</p>
        
        <p>If you are a consumer, please note that these Terms & Conditions, its subject matter and its formation, are governed by English law. You and we both agree to that the courts of England and Wales will have non-exclusive jurisdiction. However, if you are a resident of Northern Ireland you may also bring proceedings in Northern Ireland, and if you are resident of Scotland, you may also bring proceedings in Scotland.
If you are a business, these Terms & Conditions, its subject matter and its formation (and any non-contractual disputes or claims) are governed by English law. We both agree to the exclusive jurisdiction of the courts of England and Wales.</p>

     <div class="divide_line legal"></div>

     <h1>Contact</h1>
     
        <p>Should you have any questions, complaints, or comments about these Terms & Conditions, this service, or website, or require further clarification of any kind, please do not hesitate to contact us at: <a class="blue_link fw_400" href="mailto:info@trinium-media.hr?subject=[EarthAPI] Subject:">info@trinium-media.hr</a>


</div>