<section>
    <div class="container">
         <div class="row">
              <div class="col-sm-3 col-md-3">&nbsp;</div>
              <div class="col-sm-6 col-md-6">
                   <div class="signin">
                        <div class="social_sign">
                             <h3>Sign in with your social account</h3>
                             <a class="fb" href="#facebook"><i class="fa fa-facebook"></i></a> <a class="tw" href="#twitter"><i class="fa fa-twitter"></i></a> <a class="gp" href="#googleplus"><i class="fa fa-google-plus"></i></a> </div>
                        <div class="or">
                             <div class="or_l"></div>
                             <span>or</span>
                             <div class="or_r"></div>
                        </div>
                        <p class="sign_title">Log in with your site account</p>
                        <div class="row">
                             <div class="col-lg-2"></div>
                             <div class="form col-lg-8">
                                  {{form..Open}}
                                       <input name="username" placeholder="Email" class="form-control" type="text">
                                       <input name="password" placeholder="Password" class="form-control" type="password">
                                       <?php if( Session::has('msg') ){ ?>
                                            <div class="alert alert-danger"><?php echo Session::get('msg'); ?></div>
                                        <?php }; ?>
                                       <button type="submit" class="btn btn-primary btn-lg">Log In</button>
                                       <div class="forgot" style="margin-top:20px;">
                                            <!--<div class="checkbox">
                                                 <label class="">
                                                      <input type="checkbox">
                                                      Remember me </label>
                                            </div>-->
                                            <a href="/user/lost-password">Forgot password?</a>
                                        </div>
                                  {{form..Close}}
                             </div>
                             <div class="col-lg-2"></div>
                        </div>
                   </div>
              </div>
              <div class="col-sm-3 col-md-3">&nbsp;</div>

         </div>
    </div>
</section>