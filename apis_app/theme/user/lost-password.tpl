<!--BEGIN:user/lost-password-->
<section>
	<div class="container">
		<div class="row">
			<h1>Lost password?</h1>

			<p>
			No problem!
			<br>
			Just enter your account email address and we'll send you easy to follow steps on how to recover access to your account.
			</p>

			{{form..Open}}
			<div class="form-group">
			{{form..email}}
			</div>
			<div class="form-group">
			{{form..Buttons}}
			</div>
			{{form..Close}}
		</div>
	</div>
</section>
<!--END:user/lost-password-->