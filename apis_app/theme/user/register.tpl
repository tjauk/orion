<section>
    <div class="container">
         <div class="row">
              <div class="col-sm-6 col-md-6">
                   <div class="signup">
                        <form action="/user/register" method="POST">
                             <fieldset>
                                  <div class="social_sign">
                                       <h3>Don't have a site account yet?</h3>
                                       <a><i class="fa fa-user"></i></a> </div>
                                  <p class="sign_title">Create one now, it's fast &amp; free!</p>
                                  <div class="row">
                                       <div class="col-lg-2"></div>
                                       <div class="col-lg-8">
                                            <input id="Emailaddress" name="username" placeholder="Email address" class="form-control" required="" type="text">
                                            <input id="Password" name="password" placeholder="Password" class="form-control" required="" type="password">
                                            <input id="Password" name="repeated_password" placeholder="Password" class="form-control" required="" type="password">
                                            <div class="checkbox">
                                                 <label class="">
                                                      <input name="checkboxes" value="Option one" type="checkbox">
                                                      I agree to the <a href="/terms">terms and conditions</a> </label>
                                            </div>
                                            <?php if( Session::has('msg') ){ ?>
                                                <div class="alert alert-danger"><?php echo Session::get('msg'); ?></div>
                                            <?php }; ?>
                                       </div>
                                       <div class="col-lg-2"></div>
                                  </div>
                                  <button type="submit" class="btn btn-success btn-lg">Create your account</button>
                             </fieldset>
                        </form>
                   </div>
              </div>
              <div class="col-sm-6 col-md-6">
                  <div class="signup">

                  </div>
              </div>
         </div>
    <div class="container">
</section>