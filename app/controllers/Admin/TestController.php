<?php
namespace Admin;

use \Doc as Doc;
use \Request as Request;

class TestController extends \Controller
{

    function index()
    {
        $this->_list_actions(__CLASS__);
        //return redirect()->to('admin/test/login');
    }

    function login()
    {
        Doc::view('user/admin_login');
    }

    function postLogin()
    {
        dd(Request::make());
    }

}
