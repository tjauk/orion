<?php

class UserController extends AdminController
{


    function index()
    {

        $ctrlname = str_replace('Controller','',__CLASS__);
        echo h3( $ctrlname );
        $actions = get_class_methods( __CLASS__ );
        sort( $actions );
        echo '<ul>';
        echo '<li>'.strtolower( $ctrlname ).'/'.'</li>';
            echo '<li><ul>';        
            foreach( $actions as $action ) {
                if( substr( $action, 0, 1 ) !== '_' and substr( $action, 0, 4 ) !== 'post' and substr( $action, 0, 6 ) !== 'widget' ) {
                          echo  '<li><a href="/'.strtolower( $ctrlname ).'/'.$action.'">'.$action.'</a></li>';
                }
            }
            echo '</ul></li>';        
        echo '</ul>';
    }

    function dashboard()
    {
        Doc::view('layouts/admin_dashboard');
    }

    function login()
    {
        Doc::view('user/admin_login');
    }

    function postLogin()
    {
        Doc::none();
        pr($_POST);
    }



}
