<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion events:sync
class EventsDummyJob extends Command
{

    public $output;

    protected function configure()
    {
        $this
            ->setName('events:dummy')
            ->setDescription('Copy and create new event site for country')
            ->addArgument(
                'CountryName',
                InputArgument::REQUIRED,
                'Enter Country name?'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $CountryName = $input->getArgument('CountryName');
        $country = str_slug($CountryName);
        $folder = "events_{$country}";

        // copy events__dummy folder to events_{country}
        $output->writeln("Copying events__dummy to events_{$country}");
        exec("cp -R events__dummy {$folder}");

        // copy env.txt to .env
        exec("cp {$folder}/env.txt {$folder}/.env");
        $output->writeln("Copied env.txt to .env");

        // open .env, find events__dummy and replace with events_{country}
        $env = file_get_contents("{$folder}/.env");
        $env = str_replace("events__dummy", $folder, $env);
        file_put_contents("{$folder}/.env", $env);
        $output->writeln(".env database changed to {$folder}");

        // open every6h.sh, find events__dummy and replace with events_{country}
        $every6h = file_get_contents("{$folder}/every6h.sh");
        $every6h = str_replace("events__dummy", $folder, $every6h);
        file_put_contents("{$folder}/every6h.sh", $every6h);
        $output->writeln(".env database changed to {$folder}");

        // open config/app.php and replace Dummy with CountryName
        $cfg = file_get_contents("{$folder}/app/configs/app.php");
        $cfg = str_replace("Dummy", $CountryName, $cfg);
        file_put_contents("{$folder}/app/configs/app.php", $cfg);
        $output->writeln("Changed site title to {$CountryName}");

        // open config/database.php and replace events__dummy with events_{country}
        $cfg = file_get_contents("{$folder}/app/configs/db.php");
        $cfg = str_replace("events__dummy", $folder, $cfg);
        file_put_contents("{$folder}/app/configs/db.php", $cfg);
        $output->writeln("Changed database in config to {$folder}");


        $output->writeln("----------------------------------");
        $output->writeln("");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');
    }

}