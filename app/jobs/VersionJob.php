<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion version
class VersionJob extends Command
{
    protected function configure()
    {
        $this
            ->setName('version')
            ->setDescription('Get version');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $text = phpversion();
        $output->writeln($text);
    }
}