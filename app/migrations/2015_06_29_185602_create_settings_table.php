<?php

class CreateSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('settings', function($table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->string('name')->nullable();
			$table->text('value')->nullable();
			$table->text('description')->nullable();
		});
/*
		Settings::create([
			'name'=>"DeleteVideoFromKioskAfterDays",
			'value'=>"95",
			'description'=>"Obriši video bez incidenata sa kioska nakon zadanog broja dana (90-180)"
		]);

		Settings::create([
			'name'=>"DebugUseLDAPUsers",
			'value'=>"1",
			'description'=>"Test: Koristi LDAP autentifikaciju (0/1)"
		]);

		Settings::create([
			'name'=>"DailyQueueStartTime",
			'value'=>"00:15",
			'description'=>"Vrijeme nakon kojeg kiosk pokreće slanje datoteka na centralni server (HH:MM)"
		]);

		Settings::create([
			'name'=>"CameraResolution",
			'value'=>"720P30",
			'description'=>"Ispravne vrijednosti za rezoluciju kamere: 1080P, 960P, 720P60, 720P30"
		]);

		Settings::create([
			'name'=>"CameraBitrate",
			'value'=>"H",
			'description'=>"Ispravne vrijednost za bitrate kamere: L kao Low, H kao High"
		]);

		Settings::create([
			'name'=>"CameraTimeDisplay",
			'value'=>"True",
			'description'=>"Ispravne vrijednost za time display kamere: True za uključiti, False za isključiti"
		]);
*/
		

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('settings');
	}

}
