<?php
namespace Test;

use \Trinium\Model as Model;

class Test extends Model {
	
	protected $table	= 'test';
	protected $fillable = ['title'];

	public function getLinkAttribute()
	{
		return '/test/view/'.$this->id;
	}
}