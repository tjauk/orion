<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model {

	/**
	 * All possible permissions
	 */
	static $all_permissions = [
			'kiosk_camera_access',
			'login',
			'dashboard',
			'video',
			'kiosk',
			'camera',
			'audit',
			'user',
			'usergroup',
			'incident',
			'settings',
			'edit_video',
			'only_own_videos',
			'only_own_unitcode',
			'delete_video',
			'download_video',
			'download_preview',
			'edit_kiosk',
			'edit_user',
			'edit_usergroup',
			'edit_incident',
			'admin'
		];

	/**
	 * Permissions descriptions
	 */
	static $permission_descriptions = [
			'kiosk_camera_access'	=> "Uzimanje i vraćanje kamere s kioska",
			'login'			=> "Korisnik ima pravo pristupiti web sustavu",
			'dashboard'		=> "Vidi stranicu Pregled",
			'video'			=> "Vidi stranicu Video",
			'kiosk'			=> "Vidi stranicu Kiosci",
			'camera'		=> "Vidi stranicu Kamere",
			'audit'			=> "Vidi stranicu Aktivnosti",
			'user'			=> "Vidi stranicu Korisnici",
			'usergroup'		=> "Vidi stranicu Korisničke grupe",
			'incident'		=> "Vidi stranicu Tipovi događaja",
			'settings'		=> "Vidi stranicu Interne postavke",
			'edit_video'	=> "Može mijenjati opise i postavke video datoteka",
			'only_own_videos' => "Može vidjeti i mijenjati samo vlastite video datoteke",
			'only_own_unitcode' => "Može vidjeti samo zapise i kioske iz vlastite ustrojstvene jedinice",
			'use_all_unitcodes' => "Može vidjeti samo zapise i kioske iz svoje PU",
			'delete_video'	=> "Može obrisati video datoteku sa kioska",
			'download_video'=> "Može preuzeti originalnu video datoteku",
			'download_preview'=> "Može preuzeti oglednu datoteku (preview)",
			'edit_kiosk'	=> "Može mijenjati nazive i opise kioska",
			'edit_user'		=> "Može upravljati korisnicima",
			'edit_usergroup'=> "Može upravljati korisničkim grupama",
			'edit_incident'	=> "Može unositi tipove događaja",
			'admin'			=> "Administrator, vidi sve"
		];
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	public $fillable = [ 
			'name',
			'permissions'
		];

	/*
	 * Relations
	 */

	/*
	 * Finders
	 */
	static function findByName($name)
	{
		return self::where('name','=',$name)->first();
	}

	public function getPermissionsAttribute(){
		return explode(',',$this->attributes['permissions']);
	}
	public function setPermissionsAttribute($permissions){
		if( empty($permissions) ){
			$permissions=array();
		}
		$this->attributes['permissions'] = implode(',',$permissions);
	}

	public function has($permission){
		return in_array($permission,$this->permissions);
	}



}
