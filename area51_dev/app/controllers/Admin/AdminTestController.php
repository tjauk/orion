<?php
class AdminTestController extends AdminController
{
    public $model = 'Test';
    public $baseurl = '/admin/test';

    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Test',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Per page');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');


        $items = $filter->paginate( $items )->get();

        $table = Table::make()->autohead();
        foreach($items as $item){
            $data = [];
            $data['#'] = $item->id;
            $data['Name'] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->name.'</a>';
            $data['Status'] = $item->status;
            $data['User'] = $item->user_id;
            $data['Lat'] = $item->lat;
            $data['Lon'] = $item->lon;
            $data['Address'] = $item->address;
            $data['City'] = $item->city;
            $data['Region'] = $item->region;
            $data['Country'] = $item->country_id;
            $data['Test'] = UI::btn($item->id)->title("Click me!")->icon('fa-cog')->link('/admin/test/photo/'.$item->id)->addClass('btn-primary');
            $actions = UI::btnEdit($item->id,'test');
            $actions.= UI::btnDelete($item->id,'test');
            $data['Actions'] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('test');
        $form->label('Name')->input('name');

        $form->label('Status')->select('status')->values(['Draft','Published','Canceled']);

        $form->label('User')->select2('user_id')->options(User::options());

        $form->label('Latitude')->number('lat')->step('0.000001');

        $form->label('Longitude')->number('lon')->step('0.000001');

        $form->label('Address')->input('address');

        $form->label('City')->input('city');

        //$form->label('Tags')->tags('tags');

        $form->label('Region')->input('region');

        $form->label('Country')->select2('country_id')->ajax('/admin/test/ajax');

        $form->label('Logos')->photos('logo');
        $form->label('Gallery')->photos('photos');


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function ajax($q='')
    {
        // single instance
        $id = Request::get('id');
        if( !empty($id) ){
            return Country::find($id);
        }

        $q = Request::get('q');
        $page = Request::get('page',1);
        $perpage = 30;
        $items = Country::search($q,'name,code');
        return [
            'total_count'         => $items->count(),
            'incomplete_results'    => true,
            'items'                 => $items->take($perpage)->skip($perpage*($page-1))->get(['id','name','code'])
        ];
    }


    function photo($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        $post = Request::post();
        if( $post ){
            $item->fill($post);
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('test');
        
        $form->label('Name')->input('name');
        $form->label('Photo')->photo('photo');

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function geo()
    {
        $address = 'Mandlova 1, Zagreb, Croatia';
        return Geo::code($address);
    }

    function img(){
        $src = 'board-361516_640.jpg';

        //
        $img = Img::make($src);
        pr($img);

        echo Img::thumb($src,'160x90xA').br();
        echo $img->thumb('160x90xA')->img().br();

        echo $img->thumb('160x90xC').br();
        echo $img->thumb('160x90xC')->img().br();

        echo $img->thumb('160x90xW').br();
        echo $img->thumb('160x90xW')->img().br();
        //$img->resize(100,100);
        //echo $img->width();
        //echo Html::img();
        //

        //Img::thumb($src,'200x100xC')->url();
        //echo Img::src($src)->resize(200,200)->crop(100,100)->img();
    }

    function flexi()
    {
        $form = Forms::make()->format('EDIT');
        $form->label('Flexible')->flexible('content');

        return $form->toHtml();
    }

}
