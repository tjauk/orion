<?php
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class TestController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    function index()
    {
        //
    }

    /**
     * 
     *
     * @return Response
     */
    function collect()
    {
        $items = Country::all();

        return $items->every(5);
    }

    function filters()
    {
        $price = 1234.99;
        return View::make('test/test')->put($price,'price');
    }

    function lang()
    {
        //echo t('auth.login').br();
        //echo t('auth.signin').br();
        //echo t('auth.user').br();
        Localization::config();

        echo Localization::translate('auth.register');
    }

    function crawl()
    {
        $url = 'http://www.vegicept.com';
        $client = new Client();
        $crawler = $client->request('GET', $url);
        
        $found = $crawler->filter('a')->each(function (Crawler $node, $i) {
            return $node->attr('href');
        });
       
        $links = [];
        foreach($found as $link){
            if( !empty($link) ){
                $temp = $this->getAbsoluteLink($link,$url);
                if( !empty($temp) ){
                    $links[] = $temp;
                }
            }
        }

        $links = array_unique($links);

        foreach($links as $link){
            $crawler = $client->request('GET', $link);
        
            $subs = $crawler->filter('a')->each(function (Crawler $node, $i) {
                return $node->attr('href');
            });

            foreach($subs as $sub){
                if( !empty($sub) ){
                    $temp = $this->getAbsoluteLink($sub,$url);
                    if( !empty($temp) ){
                        $links[] = $temp;
                    }
                }
            }
        }

        $links = array_unique($links);

        return $links;
    }

    function getAbsoluteLink($str,$current){
        $home = trim($current,'/');
        if( substr($str,0,7)=='mailto:' ){
            return '';
        }
        if( substr($str,0,1)=='/' ){
            return $home.$str;
        }
        return $str;
    }

    function compression(){
        $txt = 'Wikipedia (Listeni/ˌwɪkᵻˈpiːdiə/ or Listeni/ˌwɪkiˈpiːdiə/ WIK-i-PEE-dee-ə) is a free-access, free-content Internet encyclopedia, supported and hosted by the non-profit Wikimedia Foundation. Those who can access the site can edit most of its articles.[5] Wikipedia is ranked among the ten most popular websites,[4] and constitutes the Internet\'s largest and most popular general reference work.[6][7][8]

Jimmy Wales and Larry Sanger launched Wikipedia on January 15, 2001. Sanger[9] coined its name,[10] a portmanteau of wiki[notes 3] and encyclopedia. Initially only in English, Wikipedia quickly became multilingual as it developed similar versions in other languages, which differ in content and in editing practices. The English Wikipedia is now one of 291 Wikipedia editions and is the largest with 5,118,057 articles (having reached 5,000,000 articles in November 2015). There is a grand total, including all Wikipedias, of over 38 million articles in over 250 different languages.[12] As of February 2014, it had 18 billion page views and nearly 500 million unique visitors each month.[13]

A peer review of 42 science articles found in both Encyclopædia Britannica and Wikipedia was published in Nature in 2005, and found that Wikipedia\'s level of accuracy approached Encyclopedia Britannica\'s.[14] Criticisms of Wikipedia include claims that it exhibits systemic bias, presents a mixture of "truths, half truths, and some falsehoods",[15] and that in controversial topics it is subject to manipulation and spin.[16]';
        $txt = Remote::get('http://www.bug.hr/');
        $txtlen = mb_strlen($txt);
        foreach(range(0,9) as $level){
            $c = gzcompress($txt,$level);
            $clen = mb_strlen($c);
            $crate = round($clen/$txtlen*100,3);
            echo "Level $level : $txtlen -> $clen = $crate%<br>";
        }
    }

    function url()
    {
        $url = 'http://www.vegicept.com/recepti/?key=val#hash';
        $info = parse_url($url);
        pr($info);
        echo http_build_url($info);
        exit();
    }


}
