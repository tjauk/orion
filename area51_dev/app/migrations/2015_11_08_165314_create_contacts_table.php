<?php

class CreateContactsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            
            $table->string('first_name',100)->default('');
            $table->string('last_name',100)->default('');
            $table->string('company',100)->default('');
            $table->string('company_long')->default('');
            $table->string('oib',20)->default('');
            $table->string('mb',20)->default('');
            
            $table->string('address',100)->default('');
            $table->string('address_2',100)->default('');
            $table->string('zip',20)->default('');
            $table->string('city',50)->default('');
            $table->integer('country_id')->default(52);

            $table->string('email',100)->default('');
            $table->string('mobile',50)->default('');
            $table->string('phone',50)->default('');
            $table->string('fax',50)->default('');
            $table->string('web',100)->default('');

            $table->text('note')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contacts');
    }

}
