<?php

class CreateTestsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->default('');
            $table->string('status')->default('');
            $table->integer('user_id')->unsigned()->nullable();
            $table->decimal('lat',10,8)->nullable();
            $table->decimal('lon',11,8)->nullable();
            $table->string('address')->default('');
            $table->string('city')->default('');
            $table->string('region')->default('');
            $table->integer('country_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tests');
    }

}
