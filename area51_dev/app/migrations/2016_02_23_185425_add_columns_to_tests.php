<?php

class AddColumnsToTests extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tests', function($table)
        {
            $table->text('logo')->default('');
            $table->text('photos')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tests', function($table)
        {
            $table->dropColumn('logo');
            $table->dropColumn('photos');
        });
    }

}
