<?php
class Test extends Model
{
    use \Trinium\Traits\GetSetPhotosAttribute;
    
    protected $table = 'tests';

    protected $fillable = [
		'name',
		'status',
		'user_id',
		'lat',
		'lon',
		'address',
		'city',
		'region',
		'country_id',
        'logo',
        'photos'
    ];

    protected $casts = [
        'photos'        => 'json'
    ];

    protected $defaults = [
        'name'          => "New test",
        'country_id'    => 52
    ];

    /*
     * Relations
     */
    public function user()
    {
        return $this->belongsTo('User');
    }
    public function country()
    {
        return $this->belongsTo('Country');
    }



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */



}
