<?php
class Tomo extends Model
{
    protected $table = 'tomos';

    protected $fillable = [
		'name',
		'publish_at',
		'note'
    ];

    /*
     * Relations
     */



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */


}
