<!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">{{"sidebar.MAIN NAVIGATION"|t}}</li>
            
            <li>
              <a href="/admin/dashboard">
                <i class="fa fa-dashboard"></i> <span>{{"sidebar.Dashboard"|t}}</span>
              </a>
            </li>

            <li>
              <a href="/admin/beaconcatcher">
                <i class="fa fa-dashboard"></i> <span>{{"sidebar.Beacon Catcher"|t}}</span>
              </a>
            </li>

            <li>
              <a href="/admin/test">
                <i class="fa fa-cog"></i> <span>{{"sidebar.Tests"|t}}</span>
              </a>
            </li>

            <li class="treeview">
              <a href="/admin/user">
                <i class="fa fa-user"></i> <span>{{"sidebar.Users"|t}}</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li class="active"><a href="/admin/user"><i class="fa fa-circle-o"></i> {{"sidebar.Users"|t}}</a></li>
                  <li><a href="/admin/usergroup"><i class="fa fa-circle-o"></i> {{"sidebar.Users groups"|t}}</a></li>
              </ul>
            </li>


            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>

          </ul>