<?php

return array(

    // rent app
    'menu.dashboard'		=> "Show Dashboard page",
    'menu.contact'			=> "Show Contacts page",
    'menu.contracts'		=> "Show Contracts page",

    'edit.contact'			=> "Can edit Contacts",

);
