<?php
class AdminBookController extends AdminController
{
    public $model = 'Book';
    public $baseurl = '/admin/book';


    /**
     * Index page
     *
     * @return View
     */
    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Book',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search')->search('keywords')->placeholder('Search');
        $form->label('Per page')->select('perpage')->values([10,20,50,100]);

        $table = Table::make()
                ->headings(['#','Name','Active','Body','Price','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->name.'</a>';
            $data[] = UI::checked($item->active);
            $data[] = $item->body;
            $data[] = $item->price;
            $actions = UI::btnEdit($item->id,'book');
            $actions.= UI::btnDelete($item->id,'book');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    /**
     * Edit page
     *
     * @return View
     */
    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        $post = Request::post();
        if( $post ){
            $item->fill($post);
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('book');
        $form->label('Name')->input('name')->style('width:100px')->required();

        $form->label('Active')->radioList('active')->options(['0'=>"NE",'1'=>"Da"]);

        $form->label('Body')->ckeditor('body');

        $form->label('Price')->input('price');

        $form->label('Države')->select2('country_id')->options(Country::options());


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }


    /**
     * Delete
     *
     * @return Response
     */
    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/book')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
