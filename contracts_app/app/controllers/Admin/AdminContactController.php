<?php
class AdminContactController extends AdminController
{
    public $model = 'Contact';
    public $baseurl = '/admin/contact';

    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Dodaj kontakt',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Per page');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');

        $table = Table::make()
                ->headings(['#','Firstname','Lastname','Company','Oib','Address','Zip','City','Country','Phone','Email','Web','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = $item->firstname;
            $data[] = $item->lastname;
            $data[] = $item->company;
            $data[] = $item->oib;
            $data[] = $item->address;
            $data[] = $item->zip;
            $data[] = $item->city;
            $data[] = $item->country->name;
            $data[] = $item->phone;
            $data[] = $item->email;
            $data[] = $item->web;
            $actions = UI::btnEdit($item->id,'contact');
            $actions.= UI::btnDelete($item->id,'contact');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        if( !Auth::user()->can('edit.contact') ){
            return redirect('http://www.google.com');
        }

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('contact');

        $form->label('Firstname')->input('firstname');

        $form->label('Lastname')->input('lastname');

        $form->label('Company')->input('company');

        $form->label('Oib')->input('oib');

        $form->label('Address')->input('address');

        $form->label('Zip')->input('zip');

        $options = ['Yes'=>'Da','No'=>'Ne'];
        $form->label('City')->checkList('city')->options($options);

        $form->label('Country')->country('country_id');

        $form->label('Phone')->phone('phone');

        $form->label('Email')->email('email');

        $form->label('Web')->web('web');

        $form->label('Test')->ckeditor('bla');


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/contact')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
