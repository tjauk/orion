<?php

class CreateContactsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('firstname')->default('');
            $table->string('lastname')->default('');
            
            $table->string('company')->default('');
            $table->string('oib')->default('');
            
            $table->string('address')->default('');
            $table->string('zip')->default('');
            $table->string('city')->default('');
            $table->integer('country_id')->default(52);
            
            $table->string('phone')->default('');
            $table->string('email')->default('');
            $table->string('web')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contacts');
    }

}
