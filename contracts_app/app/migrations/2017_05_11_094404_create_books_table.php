<?php

class CreateBooksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->default('');
            $table->integer('active')->default(1);
            $table->text('body');
            $table->float('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('books');
    }

}
