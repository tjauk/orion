<?php
class Book extends Model
{
    protected $table = 'books';

    protected $fillable = [
		'name',
		'active',
		'body',
		'price'
    ];

    /*
     * Relations
     */



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */

    /*
     * Getters
     */


    /*
     * Setters
     */


}
