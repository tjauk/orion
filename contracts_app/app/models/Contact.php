<?php
class Contact extends Model
{
    protected $table = 'contacts';

    protected $fillable = [
		'firstname',
		'lastname',
		'company',
		'oib',
		'address',
		'zip',
		'city',
		'country_id',
		'phone',
		'email',
		'web'
    ];

    /*
     * Relations
     */
    public function country()
    {
        return $this->belongsTo('Country');
    }



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->fullname;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */
    public function getFullnameAttribute()
    {
        return $this->firstname.' '.$this->lastname;
    }


}
