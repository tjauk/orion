<?php
class Test extends Model
{
    protected $table = 'tests';

    protected $fillable = [
		'name',
		'status',
		'user_id',
		'lat',
		'lon',
		'address',
		'city',
		'region',
		'country_id'
    ];

    /*
     * Relations
     */
    public function user()
    {
        return $this->belongsTo('User');
    }
    public function country()
    {
        return $this->belongsTo('Country');
    }



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */


}
