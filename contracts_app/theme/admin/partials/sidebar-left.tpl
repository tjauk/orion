<!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">GLAVNA NAVIGACIJA</li>
            
            <li>
              <a href="/admin/dashboard">
                <i class="fa fa-dashboard"></i> <span>Kontrolna ploča</span>
              </a>
            </li>

            <li>
              <a href="/admin/contact">
                <i class="fa fa-dashboard"></i> <span>Kontakti</span>
              </a>
            </li>

            <li class="treeview">
              <a href="/admin/user">
                <i class="fa fa-user"></i> <span>Users</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li class="active"><a href="/admin/user"><i class="fa fa-circle-o"></i> Users</a></li>
                  <li><a href="/admin/usergroup"><i class="fa fa-circle-o"></i> User groups</a></li>
              </ul>
            </li>


            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>

          </ul>