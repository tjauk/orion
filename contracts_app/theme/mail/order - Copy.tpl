<html>
<head>
	<title>Ponuda: { order.mark }</title>
</head>
<body>
<h1>Ponuda: { order.mark }</h1>

<b>Vrijeme: { order.created_at }</b>

<table style="width:100%;">
	<tr>
		<td>
<h2>Kupac:</h2>
<p>
	<b>{ order.info.buyer.name }</b><br>
	OIB: { order.info.buyer.oib }<br>
	{ order.info.buyer.address }<br>
	{ order.info.buyer.zip } { order.info.buyer.place }<br>
	{ order.info.buyer.country }<br>
	<br>
	E-mail: { order.info.buyer.email }<br>
	Telefon: { order.info.buyer.phone }<br>
</p>
		</td>
		<td>
<p>
	<b>{ seller.name }</b><br>
	OIB: { seller.oib }<br>
	{ seller.address }<br>
	{ seller.zip } { seller.place } , { seller.country }<br>
	<br>
	IBAN račun: { seller.ziro }<br>
	Poziv na broj: <?php echo substr($order['mark'],1); ?><br>
	<br>
	<b>Ponuda vrijedi: 7 dana.</b><br>
	<br>
	@not order.info.agent empty
		Prodajni zastupnik:<br>
		{ order.info.agent.name } { order.info.agent.surname }<br>
		{ order.info.agent.phone }<br>
		<br>
	@end
</p>
		</td>
	</tr>
</table>


<h2>Proizvodi u ponudi</h2>

<table style="width:100%;padding:2px;">
	<thead>
		<tr style="color:#fff;background-color:#444;">
			<th>Šifra</th>
			<th>Naziv</th>
			<th style="text-align:right;">Jed.cijena</th>
			<th style="text-align:right;">Količina</th>
			<th style="text-align:right;">Cijena</th>
		</tr>
	</thead>
	
	<tbody>
	@loop order.info.items as item
		<tr>
			<td>{ item.product.code }</td>
			<td>{ item.product.title }</td>
			<td style="text-align:right;">{ item.product.vpc|price }</td>
			<td style="text-align:right;">{ item.quantity }</td>
			<td style="text-align:right;"><?php echo Num::price( $item['product']['vpc'] * $item['quantity'] ); ?></td>
		</tr>
	@end
		<tr style="border-top:solid 2px #000">
			<td colspan="3">&nbsp;</td>
			<td style="text-align:right;"><b>Ukupno bez PDV-a:</b></td>
			<td style="text-align:right;"><b>{ order.info.total_netto|price } kn</b></td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
			<td style="text-align:right;"><b>Iznos PDV-a (25%):</b></td>
			<td style="text-align:right;"><b>{ order.info.total_tax|price } kn</b></td>
		</tr>
		<tr style="border-top:solid 1px #000">
			<td colspan="3">&nbsp;</td>
			<td style="text-align:right;"><h3>Ukupno za platiti:</h3></td>
			<td style="text-align:right;"><h3>{ order.info.total|price } kn</h3></td>
		</tr>
	</tbody>
</table>

<?php //px($order); ?>
<p>
Ponudu možete platiti na IBAN račun { seller.ziro }.<br>
Obavezno navedite u pozivu na broj : <?php echo substr($order['mark'],1); ?>
</p>

</body>
</html>