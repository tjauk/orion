<?php
return array(

	// Apple Push Notification Service
	'apns.devicetoken' => str_replace(' ','','30b36e49 ddb60f0f b6292b4f 13c62895 f9b509d7 55e3865f 54c0c65d a3bc389d'),
	
/*
	'apns.gateway_url'	=> 'ssl://gateway.sandbox.push.apple.com:2195',
	'apns.certificate'	=> __DIR__.'/CroDealsDevPush.pem',
	'apns.passphrase'	=> '',
*/
	'apns.gateway_url'	=> 'ssl://gateway.push.apple.com:2195',
	'apns.certificate'	=> __DIR__.'/CroDealsAppStorePush.pem',
	'apns.passphrase'	=> '',

	// Google Cloud Messaging
	'gcm.send_url'		=> 'https://android.googleapis.com/gcm/send',
	'gcm.google_api_key'	=> 'AIzaSyCrZqjN3nw8RRIoTI9J6HLvqgNWkEKzWc4',
	/*
	'gcm.sender_id'		=> '679368914025',
	Key for server apps (with IP locking)API key:
	AIzaSyDCXAYgp6DMM8UAepq_Ml1aD9UmUZme33c
	IPs:Any IP allowedActivated on:May 17, 2014 5:34 PM
	 */
);
