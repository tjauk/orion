<?php
class AdminDealController extends AdminController
{
    public $model = 'Deal';
    public $baseurl = '/admin/deal';


    /**
     * Index page
     *
     * @return View
     */
    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = Deal::with('Place')->select(DB::raw('
            deals.*,
            places.name as place_name,
            places.region_id as place_region_id,
            places.address as place_address,
            places.city as place_city
        '))
        ->leftJoin('places','places.id','=','deals.place_id');

        $buttons = Forms::make();
        $buttons->btnAdd('Add Deal',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;
        $filter->sortby = 'deals.created_at';
        $filter->sortdir = DESC;

        $sortby_options = [
            'deals.created_at'  => "Creation time",
            'deals.title'       => "Title",
            'deals.discount'    => "Discount",
            'deals.rating_calc' => "Rating"
        ];
        $sortdir_options = [ASC=>"Ascending",DESC=>"Descending"];


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'title,summary,description,discount_note,places.name,places.address,places.city' );
        }

        // search by region
        if( $filter->notEmpty('region_id') ){
            $items = $items->whereRaw( "(places.region_id='{$filter->region_id}' OR deals.region_id='{$filter->region_id}')" );
        }

        // search by city
        if( $filter->notEmpty('city') ){
            $items = $items->where( 'places.city' ,'=', $filter->city );
        }

        // search by category
        if( $filter->notEmpty('category') ){
            $items = $items->where( 'deals.category','=',$filter->category );
        }

        // content filters
        $content_options = [
            ''          => " -- ",
            'checked' => "Only checked",
            'not_checked' => "Only NOT checked",
            'only_ta'   => "With TripAdvisor link",
            'no_photos' => "Without photos",
            'no_place' => "Without place",
            'no_description' => "Without description",
            'no_summary' => "Without summary",
        ];
        if( $filter->content=='checked' ){
            $items = $items->where('deals.checked','=','1');
        }
        if( $filter->content=='not_checked' ){
            $items = $items->where('deals.checked','=','0');
        }
        if( $filter->content=='only_ta' ){
            $items = $items->where('deals.tripadvisor','<>','');
        }
        if( $filter->content=='no_photos' ){
            $items = $items->where('deals.photos','=','');
        }
        if( $filter->content=='no_place' ){
            $items = $items->whereNull('deals.place_id');
        }
        if( $filter->content=='no_description' ){
            $items = $items->where('deals.description','=','');
        }
        if( $filter->content=='no_summary' ){
            $items = $items->where('deals.summary','=','');
        }


        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search')->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Region')->select('region_id')->options(Region::options());
        $form->label('City')->select2('city')->options(Place::city_options());
        $form->label('Category')->select('category')->options(Deal::category_options());
        $form->label('Content')->select('content')->options($content_options);
        $form->label('Sort by')->select('sortby')->options($sortby_options);
        $form->label('Sort direction')->select('sortdir')->options($sortdir_options);
        $form->label('Per page')->select('perpage')->values([10,20,50,100,250,500,1000]);

        $table = Table::make()
                ->headings(['#','Title','Discount','Discount Note','Address','Region','Photos','Summary','Category','Tags','Tripadvisor','Rating','Valid From','Valid To','Checked','Note','Actions']);

        $items = $items->orderBy($filter->sortby,$filter->sortdir);
        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->title.'</a>';
            $data[] = $item->discount.'%';
            $data[] = $item->discount_note;
            $data[] = '<span style="color:#555;font-size:12px;">'.$item->place->address.'<br>'.$item->place->city.'</span>';
            $data[] = $item->region->name;
            $data[] = UI::images($item->photos);
            $data[] = $item->summary;
            $data[] = $item->category;
            $data[] = UI::simpleTags($item->tags);
            if( !empty($item->tripadvisor) ){
                $data[] = '<a href="'.$item->tripadvisor.'" target="_blank"><i class="fa fa-globe"></i></a>';
            }else{
                $data[] = '-';
            }
            $data[] = $item->rating_calc.' ('.$item->rating.')';
            $data[] = UI::date($item->valid_from);
            $data[] = UI::date($item->valid_to);
            $data[] = UI::checked($item->checked);
            $data[] = $item->note;
            $actions = UI::btnEdit($item->id,'deal');
            $actions.= UI::btnDelete($item->id,'deal');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    /**
     * Edit page
     *
     * @return View
     */
    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $data = Request::post();
            //dd($data);
            if( empty($data['valid_from']) ){
                $data['valid_from'] = date('Y-m-d');
            }
            $item->fill($data);
            
            // checkboxes
            if( Request::post('checked') ){ $item->checked=1; }else{ $item->checked=0; }

            // create Place
            if( $data['place_id']>0 ){
                // sensible update here ???
                $place = Place::find($data['place_id']);
                $fields = ['address','zip','city'];
                foreach($fields as $field){
                    if( !empty($data['Place'][$field]) ){
                        $place->$field = $data['Place'][$field];
                    }
                }
                $place->save();
                
            }else{
                $data['Place']['name'] = $data['title'];
                $data['Place']['country_id'] = 52;
                $place = new Place();
                foreach($data['Place'] as $pk=>$pv){
                    $place = $place->where($pk,'=',$pv);
                }
                $place = $place->first();
                if( $place->id>0 ){
                    $fields = ['address','zip','city'];
                    foreach($fields as $field){
                        if( !empty($data['Place'][$field]) ){
                            $place->$field = $data['Place'][$field];
                        }
                    }
                    $place->save();

                }else{
                    $place = new Place();
                    $place->fill($data['Place']);
                    $address = trim($data['Place']['address'].$data['Place']['zip'].$data['Place']['city']);
                    if( empty($address) ){
                        $geo = new Place();
                        $geo->geoCodeByAddress($data['title']);
                        $place->name = $data['title'];
                        $place->lat = $geo->lat;
                        $place->lon = $geo->lon;
                        $place->region_id = $geo->region_id;
                    }
                    $place->save();
                }

                $item->place_id = $place->id;
                if( empty($item->region_id) ){
                    $item->region_id = $place->region_id;
                }

                if( $item->place_id>0 ){

                }else{
                    $item->checked=0;
                }
            }

            if( empty($item->region_id) ){
                $item->region_id = $item->place->region_id;
            }

            if( empty($item->region_id) or empty($item->title) or empty($item->category) ){
                $item->checked=0;
            }


            $item->save();
            $item->recalculateRating();
            
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        
        }else{

            if( empty($item->region_id) and $item->place->region_id>0 ){
                $item->region_id = $item->place->region_id;
            }
        
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('deal');

        $form->div('.col-md-6');
        $form->panel('Deal info');
            
            $form->label('Title')->input('title')->popover("Enter title","","focus");
            
            $form->label('Discount')->number('discount')->addon('%');
            $form->label('Discount Note')->text('discount_note');

            $form->label('Category')->radioGroup('category')->options(Deal::$categories);

            $form->label('')->hidden('place_id');
            $place = new Place();
            if( $item->place_id>0 ){
                $place = Place::find($item->place_id);
            }
            $form->label('Address')->input('Place[address]')->value($place->address);
            $form->label('Zip')->input('Place[zip]')->value($place->zip);
            $form->label('City')->input('Place[city]')->value($place->city);
            $form->label('Region')->select('region_id')->options(Region::options())->value($place->region_id);
/*
            $form->label('Country')->select('country_id')->options(Country::options());
            $form->label('Place')->connectOrNew('place_id')
                                    ->ajax('/admin/place/ajax')
                                    ->createButtonTitle('<i class="fa fa-plus"></i> add new Place')
                                    ->createForm(Place::createForm())
                                    ->createModel('Place');
*/

            $form->label('Summary')->text('summary')->autogrow();
            $form->label('Description')->text('description')->autogrow();

            $form->label('Tags')->simpleTags('tags')->max(5);

            $form->label('Web page')->input('info_web');
            $form->label('Email')->input('info_email');
            $form->label('Phone')->input('info_phone');

        $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-6');
        $form->panel('Legal info');

            $form->label('Valid From')->datePicker('valid_from');
            $form->label('Valid To')->datePicker('valid_to');
            //$form->label('Company')->select2('company_id')->options(Company::options());
            $form->label('Internal note')->text('note')->autogrow();
            $form->label('Reviewed / Checked')->checkBox('checked');

        $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-6');
        $form->panel('Media');

            //$form->label('Main photo')->photo('photo');
            $form->label('Gallery')->photos('photos');

        $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-6');
        $form->panel('TripAdvisor info');
            
            $form->label('TripAdvisor link')->web('tripadvisor');
            $form->label('')->html('<span id="btn_extract_info" class="btn btn-warning" onclick="ta_extract();return false;"><i class="fa fa-spinner fa-pulse" style="display:none;margin-right:10px;"></i>Extract Info</span>');

            $form->label('Rating')->number('rating')->step('0.01');
            $form->label('Our rating')->input('our_rating')->value(Rating::average('Deal',$id))->disabled();

            $form->label('Region')->select('region_id')->options(Region::options());


        $form->closePanel();
        $form->closeDiv();


        $form->btnSave();

        $form->close();

jsblock("
function ta_extract(){
    var url = $('#tripadvisor').val();
    $('#btn_extract_info i').show();
    $.ajax({
        'url':'/tripadvisor/parse',
        'data':{url:url},
        'method':'POST',
        'dataType':'json'
    }).done(function( data ) {
        $('#btn_extract_info i').hide();
        if( data.success==false ){
            alert(data.error);
        }else{
            $('#title').val(data.title);
            
            $('input[name=\"Place[name]\"]').val(data.title);
            $('input[name=\"Place[address]\"]').val(data.address);
            $('input[name=\"Place[zip]\"]').val(data.zip);
            $('input[name=\"Place[city]\"]').val(data.city);
            $('input[name=\"Place[address]\"]').val(data.address);
            
            $('#address').val(data.address);
            $('#city').val(data.city);
            $('#zip').val(data.zip);
            $('#region_id').val(data.region_id);
            $('#country').val(data.country);

            $('#info_phone').val(data.phone);
            //$('#description').val(data.description);
            $('#rating').val(data.rating);
            $('#photos').val(data.photo);
            showPhotos_photos('photos');
        }
    }).always(function(){
        
    });
}
");

        return View::make('admin/edit/default')->put($form,'form');
    }


    /**
     * Delete
     *
     * @return Response
     */
    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/deal')->with('msg',"{$this->model} #{$item->id} deleted");
    }


    /**
     * Ajax search
     *
     * @return Response
     */
    function ajax($q='')
    {
        // single instance
        $id = Request::get('id');
        if( !empty($id) ){
            return Deal::find($id);
        }

        $q = Request::get('q');
        $page = Request::get('page',1);
        $perpage = 30;
        $items = Deal::search($q,'title,summary,description,discount_note');
        $data = [
            'total_count'         => $items->count(),
            'incomplete_results'    => true,
            'items'                 => $items->take($perpage)->skip($perpage*($page-1))->get()
        ];

        if( !empty(Request::get('tpl')) ){
            Doc::none();
            $data['key'] = Request::get('key');
            return View::make(Request::get('tpl'))
                        ->put($data);
        }

        return $data;
    }


    /**
     * List connected deals
     *
     * @return Response
     */
    function connected($ids='')
    {
        Doc::none();

        if( empty($ids) ){
            $ids = Request::all('ids');
        }

        if(!is_array($ids)){
            $ids = array_filter(explode(',',$ids));
        }

        $key = Request::get('key');

        $model = $this->model;
        $items = $model::whereIn('id',$ids)->get();

        return View::make('admin/connected/deals')
                    ->put($key,'key')
                    ->put($items,'items');
    }



    /**
     * Duplicate all Billa deals and change discounts from 10% to 5%
     * also set rating to 5.0 for 10% and 4.9 for 5% deals
     */
    function billa()
    {
        /*
        $deals = Deal::whereCategory('shopping')->whereDiscount(10)->get();
        foreach($deals as $deal){
            $deal->rating = 5;
            $deal->save();

            $new = $deal;
            $new->exists = 0;
            $new->id=null;
            $new->discount = 5;
            $new->discount_copy = 10;
            $new->rating = 4.9;
            $new->save();
            echo $new->title.br();
        }
        */
    }

}
