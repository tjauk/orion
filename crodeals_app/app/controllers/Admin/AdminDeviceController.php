<?php
class AdminDeviceController extends AdminController
{
    public $model = 'Device';
    public $baseurl = '/admin/device';


    /**
     * Index page
     *
     * @return View
     */
    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Device',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'device_uid,user_id,token,gcm,platform' );
        }

        // platform
        if( $filter->notEmpty('platform') ){
            $items = $items->wherePlatform( $filter->platform );
        }

        // test device
        if( in_array((string)$filter->test,['0','1']) ){
            $items = $items->whereTest( $filter->test );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search')->search('keywords')->placeholder('Search');
        $form->label('Platform')->select('platform')->options([''=>" --- ",'android'=>"Android",'ios'=>"iOS"]);
        $form->label('Test')->select('test')->options([''=>" --- ",'0'=>"No",'1'=>"Yes"]);
        $form->label('Per page')->select('perpage')->values([10,20,50,100]);

        $table = Table::make()
                ->headings(['#','User','Last Seen','Device Uid','GCM Token','Lat','Lon','Region','Is Paid','Paid Until','Platform','Version','Test','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = $item->user_id;
            $data[] = $item->last_seen;
            $data[] = $item->device_uid;
            if( empty($item->gcm) ){
                $data[] = '-';
            }else{
                $data[] = '<i class="fa fa-mobile" alt="'.$item->gcm.'" title="'.$item->gcm.'"></i>';
            }
            $data[] = $item->lat;
            $data[] = $item->lon;
            $data[] = $item->region_id;
            $data[] = $item->is_paid;
            $data[] = $item->paid_until;
            $data[] = $item->platform;
            $data[] = $item->version;
            $data[] = UI::checked($item->test);
            $actions = UI::btnEdit($item->id,'device');
            $actions.= UI::btnDelete($item->id,'device');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    /**
     * Edit page
     *
     * @return View
     */
    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('device');
        $form->label('User Id')->input('user_id');

        $form->label('Last Seen')->datePicker('last_seen');

        $form->label('Device Uid')->input('device_uid');

        $form->label('Token')->input('token');

        $form->label('GCM Token')->input('gcm');

        $form->label('Lat')->input('lat');

        $form->label('Lon')->input('lon');

        $form->label('Region')->select('region_id')->options(Region::options());

        $form->label('Is paid')->radioGroup('is_paid')->options(['0'=>"No",'1'=>"Yes"]);

        $form->label('Paid Until')->datePicker('paid_until');

        $form->label('Platform')->input('platform');

        $form->label('Version')->input('version');

        $form->label('Test device')->radioGroup('test')->options(['0'=>"No",'1'=>"Yes"]);

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }


    /**
     * Delete
     *
     * @return Response
     */
    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/device')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
