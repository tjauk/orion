<?php
class AdminMenuController extends AdminController
{
    public $model = 'Menu';
    public $baseurl = '/admin/menu';


    /**
     * Index page
     *
     * @return View
     */
    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Menu',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 500;
        $filter->page = 1;

        $groups = Menu::options();

        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name' );
        }

        if( $filter->notEmpty('category_id') ){
            $items = $items->whereCategoryId( $filter->category_id );
        }

        if( $filter->notEmpty('group') ){
            $items = $items->whereGroup( $filter->group );
        }

        if( $filter->notEmpty('region_id') ){
            $items = $items->whereRegionId( $filter->region_id );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search')->search('keywords')->placeholder('Search');
        $form->label('Category')->select('category_id')->options(MenuCategory::options())->onChange("$('form[name=searchform]').submit();");
        $form->label('Group')->select('group')->options($groups)->onChange("$('form[name=searchform]').submit();");
        $form->label('Region')->select('region_id')->options(Region::options())->onChange("$('form[name=searchform]').submit();");
        //$form->label('Per page')->select('perpage')->values([10,20,50,100,500,1000]);

        $table = Table::make()
                ->headings(['#','Region','Group','Pri','Name','Category','Color','Images','Icons','Action','Active','Private','Actions']);

        $items = $items->orderBy('pri','ASC');
        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = $item->region->name;            
            $data[] = $item->group;
            $data[] = $item->pri;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->name.'</a>';
            $data[] = $item->category->name;
            $data[] = '<div style="display:inline-block;width:24px;height:24px;background-color:'.$item->category->color.';"></div>';
            $data[] = UI::images($item->images)->thumbsize('128x24xA');
            $data[] = UI::images($item->icons);
            $data[] = $item->action;
            $data[] = UI::checked($item->active);
            $data[] = UI::checked($item->private);
            $actions = UI::btnEdit($item->id,'menu');
            $actions.= UI::btn()->title('COPY <i class="fa fa-copy"></i>')->link('/admin/menu/edit/?copy='.$item->id);
            $actions.= UI::btnDelete($item->id,'menu');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    /**
     * Edit page
     *
     * @return View
     */
    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }else if( Request::get('copy')>0 ){
            $item = $model::find(Request::get('copy'));
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }

        $form->open('menu');

        $form->label('Name')->input('name');
        $form->label('Slug')->input('slug');
        $form->label('Group')->input('group');
        $form->label('Region')->select('region_id')->options(Region::options());
        $form->label('Priority')->input('pri');
        $form->label('Category')->select('category_id')->options(MenuCategory::options());
        $form->label('Images')->photos('images');
        $form->label('Icons')->photos('icons');
        $form->label('Action')->text('action');
        $form->label('Active')->radioGroup('active')->options(['1'=>"Yes",'0'=>"No"]);
        $form->label('Private')->radioGroup('private')->options(['1'=>"Yes",'0'=>"No"]);

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }


    /**
     * Delete
     *
     * @return Response
     */
    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/menu')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
