<?php
class AdminPageController extends AdminController
{
    public $model = 'Page';
    public $baseurl = '/admin/page';


    /**
     * Index page
     *
     * @return View
     */
    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Page',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'title' );
        }

        if( $filter->notEmpty('region_id') ){
            $items = $items->whereRegionId( $filter->region_id );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search')->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Region')->select('region_id')->options(Region::options())->onChange("$('form[name=searchform]').submit();");
        $form->label('Per page')->select('perpage')->values([10,20,50,100])->klass('form-control');


        $table = Table::make()
                ->headings(['#','Title','Excerpt','Region','Menu','Photos','Tags','Status','Priority','Published At','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->title.'</a>';
            $data[] = $item->excerpt;
            $data[] = $item->region->name;
            $data[] = $item->menu->title;
            //$data[] = count($item->photos);
            $data[] = UI::images($item->photos);
            $data[] = UI::simpleTags($item->tags);
            $data[] = $item->status;
            $data[] = $item->pri;
            $data[] = UI::date($item->published_at);
            $actions = UI::btnEdit($item->id,'page');
            $actions.= UI::btnDelete($item->id,'page');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    /**
     * Edit page
     *
     * @return View
     */
    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            //dd(Request::post('menus'));
            $item->fill(Request::post());
            $item->menus = Request::post('menus');

            if( empty($item->slug) ){
                $item->slug = str_slug($item->title);
            }
            $item->save();
            Menu::reconnectActions();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('page');

        $form->div('.col-md-8');
        $form->panel('Page content');
            
            $form->label('Title')->input('title');
            $form->label('Slug')->input('slug');
            $form->label('Excerpt')->text('excerpt')->autogrow();
            $form->label('Photos')->photos('photos');
            $form->label('Content')->ckeditor('content')->autogrow();
            
        $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-4');
        $form->panel('Page info');
            $menus = Menu::whereActive(1)->get();
            $categories = [''=>' --- '];
            foreach($menus as $menu){
                $categories[$menu->id] = $menu->name;
            };
            //$form->label('Menu')->select2('menus')->name('menus[]')->multiple(5)->options(Menu::optionsAll());

            $form->label('Region')->select('region_id')->options(Region::options());
            
            $menu_options = Menu::optionsAll();
            unset($menu_options['']);
            $form->label('Menu')->checkList('menus')->options($menu_options);
            $form->label('Priority')->number('pri');
            
            $form->label('Status')->radioList('status')->options(Page::statusOptions());
            $form->label('Published At')->datePicker('published_at');
            //$form->label('Featured')->checkBox('featured');
            $form->label('Tags')->simpleTags('tags');

            $form->label('Places')->connectMany('places')
                                    ->placeholder('Search Places')
                                    ->setTemplate('admin/connected/places')
                                    ->ajax('/admin/place/ajax')
                                    ->listConnected('/admin/place/connected');

            $form->label('Deals')->connectMany('deals')
                                    ->placeholder('Search Deals')
                                    ->setTemplate('admin/connected/deals')
                                    ->ajax('/admin/deal/ajax')
                                    ->listConnected('/admin/deal/connected');
            
        $form->closePanel();
        $form->closeDiv();

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }


    /**
     * Delete
     *
     * @return Response
     */
    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/page')->with('msg',"{$this->model} #{$item->id} deleted");
    }


    /**
     * List connected pages
     *
     * @return Response
     */
    function connected($ids='')
    {
        Doc::none();

        if(!is_array($ids)){
            $ids = array_filter(explode(',',$ids));
        }

        $model = $this->model;
        $items = $model::whereIn('id',$ids)->get();

        return View::make('admin/connected/pages')->put($items,'items');
    }

}
