<?php
class AdminPlaceController extends AdminController
{
    public $model = 'Place';
    public $baseurl = '/admin/place';


    /**
     * Index page
     *
     * @return View
     */
    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Place',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 100;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name,address,zip,city,lat,lon' );
        }

        // search region
        if( $filter->notEmpty('region_id') ){
            $items = $items->whereRegionId( $filter->region_id );
        }

        // search city
        if( $filter->notEmpty('city') ){
            $items = $items->whereCity( $filter->city );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search')->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Region')->select('region_id')->options(Region::options())->klass('form-control');
        $form->label('City')->select2('city')->options(Place::city_options())->klass('form-control');
        $form->label('Per page')->select('perpage')->values([10,20,50,100])->klass('form-control');

        $table = Table::make()
                ->headings(['#','Name','Address','Zip','City','Region','Country','Coordinates','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->name.'</a>';
            //$data[] = $item->slug;
            $data[] = $item->address;
            $data[] = $item->zip;
            $data[] = $item->city;
            $data[] = $item->region->name;
            $data[] = $item->country->name;
            $data[] = $item->lat.';'.$item->lon;
            $actions = UI::btnEdit($item->id,'place');
            $actions.= UI::btnDelete($item->id,'place');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    /**
     * Edit page
     *
     * @return View
     */
    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            // geo code if empty lat and lon
            if( empty($item->lat) and empty($item->lon) ){
                $item->geoCodeByAddress();
            }
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('place');
        $form->label('Name')->input('name');

        //$form->label('Slug')->input('slug');

        $form->label('Address')->input('address');
        $form->label('Zip')->input('zip');
        $form->label('City')->input('city');
        $form->label('Region')->select('region_id')->options(Region::options());
        $form->label('Country')->select('country_id')->options(Country::options());
        
        $form->label('')->html('<a class="btn btn-warning" onclick="refreshMap();return false;">Refresh Map</a>');

        $form->label('Lat')->input('lat')->onChange("refreshMap();");
        $form->label('Lon')->input('lon')->onChange("refreshMap();");
        $form->label('Map')->gmap('map');

        jsblock("
function refreshMap(){
    var title = $('#name').val();
    var lat = parseFloat($('#lat').val());
    var lon = parseFloat($('#lon').val());
    placeMarkerAndPanTo({lat:lat,lng:lon},map);
}
");

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }


    /**
     * Delete
     *
     * @return Response
     */
    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/place')->with('msg',"{$this->model} #{$item->id} deleted");
    }


    /**
     * Ajax search
     *
     * @return Response
     */
    function ajax($q='')
    {
        // single instance
        $id = Request::get('id');
        if( !empty($id) ){
            return Place::find($id);
        }

        $q = Request::get('q');
        $page = Request::get('page',1);
        $perpage = 30;
        $items = Place::search($q,'name,address,city');
        $data = [
            'total_count'         => $items->count(),
            'incomplete_results'    => true,
            'items'                 => $items->take($perpage)->skip($perpage*($page-1))->get(['id','name','address','zip','city'])
        ];

        if( !empty(Request::get('tpl')) ){
            Doc::none();
            $data['key'] = Request::get('key');
            return View::make(Request::get('tpl'))
                        ->put($data);
        }

        return $data;
    }


    /**
     * List connected places
     *
     * @return Response
     */
    function connected($ids='')
    {
        Doc::none();

        if( empty($ids) ){
            $ids = Request::all('ids');
        }

        if(!is_array($ids)){
            $ids = array_filter(explode(',',$ids));
        }

        $key = Request::get('key');

        $model = $this->model;
        $items = $model::whereIn('id',$ids)->get();

        return View::make('admin/connected/places')
                    ->put($key,'key')
                    ->put($items,'items');
    }

}
