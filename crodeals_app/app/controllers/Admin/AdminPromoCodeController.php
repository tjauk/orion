<?php
class AdminPromocodeController extends AdminController
{
    public $model = 'PromoCode';
    public $baseurl = '/admin/promocode';

    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add new PromoCode',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'code,description,discount,discount_note' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Per page');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');

        $table = Table::make()
                ->headings(['Code','Description','Discount','Discount Note','Valid From','Valid To','Max Usage','Used','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->code;
            $data[] = $item->description;
            $data[] = $item->discount.'%';
            $data[] = $item->discount_note;
            $data[] = UI::date($item->valid_from);
            $data[] = UI::date($item->valid_to);
            $data[] = $item->max_usage;
            $data[] = $item->used;
            $actions = UI::btnEdit($item->id,'promocode');
            $actions.= UI::btnDelete($item->id,'promocode');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('promocode');
        $form->label('Code')->input('code');

        $form->label('Description')->text('description');

        $form->label('Discount')->number('discount');

        $form->label('Discount Note')->input('discount_note');

        $form->label('Valid From')->datePicker('valid_from');

        $form->label('Valid To')->datePicker('valid_to');

        $form->label('Max Usage<br><small>(enter 0 for unlimited)</small>')->number('max_usage');

        $form->label('Used')->number('used');


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/promocode')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
