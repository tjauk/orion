<?php
class AdminPushNotificationController extends AdminController
{
    public $model = 'PushNotification';
    public $baseurl = '/admin/push-notification';


    /**
     * Index page
     *
     * @return View
     */
    function index()
    {
        $model = $this->model;
        
        Doc::title('Push Notifications');

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Push Notification',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'title' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search')->search('keywords')->placeholder('Search');
        $form->label('Per page')->select('perpage')->values([10,20,50,100]);

        $table = Table::make()
                ->headings(['#','Title','Message','Action','Groups','Status','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->title.'</a>';
            $data[] = $item->message;
            $data[] = $item->action;
            $data[] = '<small>'.implode(', ',$item->groups).'</small>';
           
            $queued = $item->pushqueue()->whereStatus(0)->count();
            $sent = $item->pushqueue()->whereStatus(1)->count();
            $failed = $item->pushqueue()->whereStatus(-1)->count();
            $data[] = "$queued / $sent / $failed";
            
            $actions = UI::btn($item->id,'REFRESH QUEUE','/admin/push-notification/refresh/');
            $actions.= UI::btn($item->id,'SEND QUEUE','/admin/push-notification/send/');
            $actions.= UI::btnEdit($item->id,'push-notification');
            $actions.= UI::btnDelete($item->id,'push-notification');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    /**
     * Edit page
     *
     * @return View
     */
    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->groups = Request::post('groups');
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('pushnotification');
        $form->label('Title')->input('title');

        $form->label('Message')->text('message');

        //$form->label('Action')->text('action');

        $groups = PushNotification::$device_groups;
        $form->label('Device groups')->checkList('groups')->options($groups)->addClassToItems('col-md-4 col-sm-6 col-xs-12');

        //$form->label('Status')->input('status');


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }


    /**
     * Refresh queue
     *
     * @return Response
     */
    function refresh($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        if( $item->id>0 ){
            $item->resetQueue();
        }
        return redirect('/admin/push-notification')->with('msg',"Devices added to send queue");
    }


    /**
     * Send queue
     *
     * @return Response
     */
    function send($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        if( $item->id>0 ){
            $item->resetQueue();
        }

        $queued = PushQueue::wherePushNotificationId($item->id)->get();

        foreach($queued as $push){
            $results = $push->send();
        }

        return redirect('/admin/push-notification')->with('msg',"Devices notified");
    }


    /**
     * Delete
     *
     * @return Response
     */
    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/push-notification')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
