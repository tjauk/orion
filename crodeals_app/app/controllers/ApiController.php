<?php
class ApiController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        ApiLog::request();
    }


    /**
     * 
     *
     * @return Response
     */
    public function index()
    {
        Doc::set('layout/blank');
        Doc::title('API');
        $txt = "
Deals
GET /api/deals
Defaults:
?page=1
?perpage=100
?sortby=[title,rating,distance,discount]
?sortdir=[ASC,DESC]

FIlters:
&search={String Query}
&region_id={Integer RegionID}
&lat={Float Latitude} & lon={Float Longitude} & sortby=distance
&range={Integer Meters} ---> NOT IMPLEMENTED


Deal
GET /api/deal/{Integer PromoCodeID}

Regions
GET /api/regions

Countries
GET /api/countries

Place
GET /api/place/{Integer PlaceID}

Page
GET /api/page/{Integer PageID}

Pages (needs more filters)
GET /api/pages

Menus
GET /api/menus

Promo Codes
Check if promocode is valid
GET /api/check_code/{String PromoCode}
returns false or PromoCode object on success

Increment promocode usage field
GET /api/use_code/{String PromoCode}
returns false or PromoCode object on success
";
        return str_replace("\n","<br>",$txt);
    }

    /**
     * 
     *
     * @return Response
     */
    public function init()
    {
        $json = [];
        $json['Tutorials'] = ApiTutorial::all();
        $json['Regions'] = ApiRegion::orderBy(DB::raw('id<>6,id'))->get();
        $categories = [];
        foreach(Deal::$categories as $key=>$title){
            if( Deal::whereCategory($key)->whereChecked(1)->count() > 0 ){
                $categories[]=['key'=>$key,'title'=>$title];
            }
        }
        $json['DealCategories'] = $categories;
        return $json;
    }

    /**
     * 
     *
     * @return Response
     */
    public function deals()
    {
        $items = ApiDeal::with(['Place'])->select(DB::raw('
            deals.*,
            places.lat as place_lat,
            places.lon as place_lon
        '))
        ->active()
        ->leftJoin('places','places.id','=','deals.place_id');
        
        $filter = SearchFilter::stateless();
        $filter->perpage = 100;
        $filter->page = 1;
        $filter->sortby = 'discount'; // column or distance
        $filter->sortdir = 'DESC';

        $device = null;
        $device = Device::register($filter->device_uid);
        $region_id = $this->autoRegion($device);
        $filter->set('region_id',$region_id);
        
        if( !in_array($filter->sortby,['title','rating','distance','discount']) ){
            $filter->set('sortby','discount');
        }
        
        if( !in_array($filter->sortdir,['asc','desc','ASC','DESC']) ){
            $filter->set('sortdir','DESC');
        }
        
        if( $filter->notEmpty('search') ){
            $items = $items->search( $filter->search, 'title,description,summary,discount,discount_note,tags,places.name,places.address,places.city' );
        }

        if( $filter->notEmpty('region_id') ){
            $items = $items->where('places.region_id', '=', $region_id );
        }

        
        if( $filter->sortby=='distance' ){
            $calculate_distance = true;

            // gps enabled
            if( $filter->notEmpty('lat') and $filter->notEmpty('lon') ){
                $lat = $this->sanitizeLatLon($filter->lat);
                $lon = $this->sanitizeLatLon($filter->lon);
            }

            // last device position
            if( empty($lat) ){
                $lat = (float)$device->lat;
                $lon = (float)$device->lon;
            }

            // region center
            if( empty($lat) ){
                $region = Region::find($region_id);
                $lat = (float)$region->lat;
                $lon = (float)$region->lon;
            }
            
            /* See http://stackoverflow.com/questions/16465779/sorting-mysql-query-by-latitude-longitude
            That query doesn't account for the fact that longitude is measured east-west from the Greenwich meridian,
            so it will return very long distances for two places near each other and near the 180th meridian.
            */
            $lat = $this->sanitizeLatLon($lat);
            $lon = $this->sanitizeLatLon($lon);

            $filter->set('sortby', DB::raw("(POW((lon-$lon),2) + POW((lat-$lat),2))") );
            $filter->set('sortdir', 'ASC' );

        }

        // all deals
        if( $filter->sortby=='rating' ){
            $filter->set('sortby','rating_calc');
            $filter->set('sortdir','DESC');
        }

        if( $filter->notEmpty('category') ){
            $items = $items->where('category', '=', $filter->category );
        }
        if( $filter->notEmpty('key') ){
            $items = $items->where('category', '=', $filter->key );
        }


        $items->orderBy($filter->sortby,$filter->sortdir);
        $items = $filter->paginate( $items )->get();

        if( $calculate_distance ){
            $items->map(function($item,$key) use ($lat,$lon){
                $item->distance = Geo::distance((float)$item->place_lat,(float)$item->place_lon,$lat,$lon,'m');
                return $item;
            });
        }
        return $items;
    }


    /**
     * 
     *
     * @return Response
     */
    public function deal($id=0)
    {
        $get = Request::getAsObject();
        if( !empty($get->id) ){
            return ApiDeal::with(['Place'])->find($get->id);
        }
        return ApiDeal::with(['Place'])->find($id);
    }


    /**
     * 
     *
     * @return Response
     */
    public function place($id=0)
    {
        $place = Place::find($id);
        if( $place->id>0 ){
            return $place;
        }
        return ['error'=>"No place found"];
    }


    /**
     * 
     *
     * @return Response
     */
    public function places()
    {
        return Place::all();
    }


    /**
     * 
     *
     * @return Response
     */
    public function map()
    {
        
        $items = ApiPlacePin::select('places.*')->join('deals', function($join) {
                                    $join->on('deals.place_id', '=', 'places.id')->where('deals.checked','=',1);
                                });

        $filter = SearchFilter::stateless();
        $filter->perpage = 40;
        $filter->page = 1;
        $filter->sortby = 'id'; // column or distance
        $filter->sortdir = 'ASC';

        $device = null;
        if( $filter->notEmpty('device_uid') ){
            $device = Device::register($filter->device_uid);
        }
     
        if( $filter->notEmpty('search') ){
            $items = $items->search( $filter->search, 'name,address,city' );
        }

        if( $filter->notEmpty('region_id') ){
        }

        // remove places not geocoded
        $items = $items->whereNotNull('lat');
        
        if( ($filter->notEmpty('lat') and $filter->notEmpty('lon')) 
            or $filter->notEmpty('region_id') 
            or (string)$filter->region_id=='6' ){


            $calculate_distance = true;


            if( $filter->notEmpty('lat') and $filter->notEmpty('lon') ){
                $lat = $this->sanitizeLatLon($filter->lat);
                $lon = $this->sanitizeLatLon($filter->lon);

                $filter->set('sortby', DB::raw("(POW((lon-$lon),2) + POW((lat-$lat),2))") );
                $filter->set('sortdir', 'ASC' );

            }else if( $filter->notEmpty('region_id') ){
                $region_id = $this->autoRegion($device);
                $items = $items->whereRegionId( $region_id );
                //    $region = ApiRegion::find($filter->region_id);
                //    $lat = (float)$region->lat;
                //    $lon = (float)$region->lon;
            }
        }
        /*
        */

        $items = $items->orderBy($filter->sortby,$filter->sortdir);


        $items = $items = $filter->paginate( $items )->get();
        
        if( $calculate_distance ){
            $items->map(function($item,$key) use ($lat,$lon){
                $item->distance = Geo::distance((float)$item->lat,(float)$item->lon,$lat,$lon,'m');
                return $item;
            });
        }

        return $items;
    }


    /**
     * 
     *
     * @return Response
     */
    public function pages()
    {
        $items = ApiPage::published();
        
        $filter = SearchFilter::stateless();
        $filter->perpage = 100;
        $filter->page = 1;
        $filter->sortby = 'pri';
        $filter->sortdir = 'DESC';

        if( $filter->notEmpty('search') ){
            $items = $items->search( $filter->search, 'title,excerpt,tags' );
        }

        if( $filter->notEmpty('featured') ){
            $items = $items->whereFeatured( 1 );
        }

        if( $filter->notEmpty('menu') ){
            $menu = Menu::findByGroup($filter->menu);
            //$items = $items->whereMenuId($menu->id);
            $items = $items->whereRaw("(menu_id='".$menu->id."' or FIND_IN_SET('".$menu->id."',menus))");
        }

        if( $filter->notEmpty('menu_id') ){
            $menu = Menu::find($filter->menu_id);
            $items = $items->whereRaw("(menu_id='".$menu->id."' or FIND_IN_SET('".$menu->id."',menus))");
        
        }else if( $filter->notEmpty('region_id') and (string)$filter->region_id!=='6' ){
            $items = $items->whereRegionId($filter->region_id);
        }

        $items->orderBy($filter->sortby,$filter->sortdir);
        $items->orderBy('published_at',ASC);
        
        return $items = $filter->paginate( $items )->get();
    }


    /**
     * 
     *
     * @return Response
     */
    public function page($id=0)
    {
        $get = Request::getAsObject();
        if( !empty($get->id) ){
            return $this->sendPage( ApiPage::find($get->id) );
        }
        if( !empty($get->slug) ){
            return $this->sendPage( ApiPage::findBySlug($get->slug) );
        }
        if( is_numeric($id) ){
            return $this->sendPage( ApiPage::find($id) );
        }
        return $this->sendPage( ApiPage::findBySlug($id) );
    }
    private function sendPage($page)
    {
        if( $page->id>0 ){
            return $page;
        }

        // 404 page
        $page = new ApiPage();
        $page->title = "Not found";
        $page->content = "<h1>Page not found</h1>";
        $page->excerpt = "";
        $page->photos = ['page_not_found.png'];
        return $page;
    }


    /**
     * 
     *
     * @return Response
     */
    public function menus($group='')
    {
        if( empty($group) ){    $group = Request::get('group'); }
        $device_uid = Request::get('device_uid');
        $device = Device::register($device_uid);
        $region_id = $this->autoRegion($device);
        
        $menus = ApiMenu::whereActive(1);
        $menus = $menus->orderBy('pri',ASC);

        if( !empty($group) ){   $menus = $menus->whereGroup($group); }
        if( $group=='home' or $group=='sidemenu' ){
        }else if( $region_id>0 ){
            $menus = $menus->whereRegionId($region_id);
        }

        $menus = $menus->get();

        $items = [];
        foreach($menus as $menu){
            $menu->action = Action::parse($menu->action);
            $items[] = $menu;
        }

        return $items;
    }

    private function sanitizeLatLon($ll)
    {
        $ll = str_replace(',', '.', $ll);
        return (float)$ll;
    }

    private function autoRegion($device=null)
    {
        $get = Request::getAsObject();
        $region_id = $get->region_id;
        if( $region_id==6 ){
            if( is_null($device) ){
                $device = Device::register($device_uid);
            }
            $lat = $this->sanitizeLatLon($get->lat);
            $lon = $this->sanitizeLatLon($get->lon);
            if( empty($lat) ){
                if( !empty($device->lat) ){
                    $lat = $device->lat;
                    $lon = $device->lon;
                }
            }
            if( empty($lat) ){
                $region_id = 1;
            }else{
                $region = $this->getRegionByLocation($lat,$lon);
                $region_id = $region->id;
            }
        }
        
        if( $region_id<1 or $region_id>6 ){
            $region_id = 1;
        }
        
        return $region_id;
    }

    public function getRegionByLocation($lat,$lon)
    {
        $items = new ApiPlacePin;
        $items = $items->whereNotNull('lat');
        $items = $items->where('region_id','>',0);
        $items = $items->orderBy(DB::raw("(POW((lon-$lon),2) + POW((lat-$lat),2))"),ASC);
        $place = $items->first();
        $region = Region::find($place->region_id);
        return $region;
    }


    /**
     * 
     *
     * @return Response
     */
    public function region()
    {
        
        $get = Request::getAsObject();
        if( !empty($get->lat) and !empty($get->lon) ){
            $items = new ApiPlacePin;
            $items = $items->whereNotNull('lat');
            $items = $items->where('region_id','>',0);
            $lat = $this->sanitizeLatLon($get->lat);
            $lon = $this->sanitizeLatLon($get->lon);
            $items = $items->orderBy(DB::raw("(POW((lon-$lon),2) + POW((lat-$lat),2))"),ASC);
            $place = $items->first();
            $region = Region::find($place->region_id);

        }else if( !empty( $get->id ) ){
            $region = Region::find($get->id); 
        }

        if( empty($region) ){
            $region = Region::find(1);
        }

        return $region;
    }


    /**
     * 
     *
     * @return Response
     */
    public function regions()
    {
        return ApiRegion::orderBy(DB::raw('id<>6,id'))->get();
    }


    /**
     * 
     *
     * @return Response
     */
    public function countries()
    {
        return Country::all();
    }


    /**
     * 
     *
     * @return Response
     */
    public function tutorials()
    {
        return ApiTutorial::all();
    }


    /**
     * 
     *
     * @return Response
     */
    public function postRating()
    {
        $post = Request::postAsObject();
        
        if( $post->score<1 or $post->score>5 ){ return ['error'=>"Score must be number between 1 and 5"]; }
        if( empty($post->device_uid) ){         return ['error'=>"No device uid sent"]; }
        Device::register($post->device_uid);
        
        if( $post->deal_id>0 ){
            $post->item = 'Deal';
            $post->item_id = $post->deal_id;
        }else if( !empty($post->item) and $post->item_id>0 ){

        }else{
            return ['error'=>"Not found item to rate"];
        }

        $rating = Rating::whereDeviceUid($post->device_uid)
                        ->whereItem($post->item)
                        ->whereItemId($post->item_id)
                        ->first();
        if( $rating->id>0 ){
            $rating->score = $post->score;
        }else{
            $rating = new Rating();
            $rating->item = $post->item;
            $rating->item_id = $post->item_id;
            $rating->score = $post->score;
            $rating->device_uid = $post->device_uid;
        }
        $rating->save();
        
        if( $post->item=='Deal' ){
            $deal = Deal::find($post->item_id);
            if( $deal->id>0 ){
                $deal->recalculateRating();
            }
        }

        return ['score'=>Rating::average($rating->item,$rating->item_id)];
    }


    /**
     * 
     *
     * @return Response
     */
    public function checkCode($code='')
    {
        $promo = PromoCode::valid()->whereCode($code)->first();
        return empty($promo) ? ['success'=>false] : $promo;
    }


    /**
     * 
     *
     * @return Response
     */
    public function useCode($code='')
    {
        $promo = PromoCode::findByCode($code);
        if( $promo ){
            $promo->increment('used');
        }
        return empty($promo) ? false : $promo;
    }


    /**
     * POST user-register
     *
     * @return Response
     */
    public function postUserRegister()
    {
        if( Request::method()=='POST' ){
            $post = Request::postAsObject();
            if( empty($post->first_name) ){ return ['error'=>"Field first_name is required"]; }
            if( empty($post->email) ){ return ['error'=>"Field email is required"]; }
            if( empty($post->device_uid) ){ return ['error'=>"Field device_uid is required"]; }
            if( empty($post->password) ){ return ['error'=>"Field password is required"]; }

            $device = Device::register($post->device_uid);

            $user = User::whereEmail($post->email)->first();
            if( $user->id>0 ){
                return ['error'=>"User with this email already exists"];
            }

            $user = new User();
            $user->usergroup = 'user';
            $user->first_name = $post->first_name;
            $user->email = $user->username = $post->email;

            $user->password = $post->password;
            $user->facebook_id = $post->facebook_id;

            $user->device_uid = $post->device_uid;
            $user->token = User::generateToken();
            $user->save();
            
            $user_id = $user->id;

            $user->saveLastPosition();

            $user = User::find($user_id);
            User::syncPaid($user,$device);

            return ApiUser::find($user_id);
        }

    }


    /**
     * GET api/user-check
     * 
     * @return Response
     */
    public function userCheck()
    {

        // check

        $json = [
            'is_paid' => 0,
            'paid_until' => null
        ];

        $get = Request::getAsObject();
        if( !empty($get->token) ){
            $user = User::whereToken($get->token)->first();
            if( $user->id>0 ){
                $user->saveLastPosition();
                if( $user->isPaid()==true ){
                    $json['is_paid'] = $user->is_paid;
                    $json['paid_until'] = $user->paid_until;
                }
            }else{
                return ['error'=>"User token invalid"];
            }
        }
        if( !empty($get->device_uid) and !empty($json) ){
            $device = Device::register($get->device_uid);
            if( $device->isPaid()==true ){
                $json['is_paid'] = $device->is_paid;
                $json['paid_until'] = $device->paid_until;
            }
        }
        
        return $json;
    }


    /**
     * POST api/user-login
     *
     * @return Response
     */
    public function postUserLogin()
    {
        $post = Request::postAsObject();
        $password = $post->password;

        if( empty($post->email) ){ return ['error'=>"Field email is required"]; }
        if( empty($post->device_uid) ){ return ['error'=>"Field device_uid is required"]; }
        if( !empty($post->facebook_id) ){
            $password = md5($post->facebook_id);
        }
        if( empty($password) ){ return ['error'=>"Field password is required"]; }

        $credentials = ['email'=>$post->email,'password'=>$password];

        if( $user = Auth::attempt($credentials) ){

            $devices = $user->devices;

            if( $user->devices->where('device_uid','<>',$post->device_uid)->count()>1 ){
                return ['error'=>"Too many devices connected to this user"];
            }

            // regenerate token
            if( empty($user->token) ){
                $user->token = User::generateToken();
            }
            $user->device_uid = $post->device_uid;
            $user->save();

            $device = Device::register($post->device_uid);
            $device->user_id = $user->id;
            $device->save();
            $device->saveLastPosition();

            User::syncPaid($user,$device);
            
            return ApiUser::find($user->id);
        }

        return ['error'=>"Invalid credentials"];
    }


    /**
     * POST api/user-login
     *
     * @return Response
     */
    public function postUserFacebook()
    {
        $post = Request::postAsObject();
        
        if( empty($post->first_name) ){ return ['error'=>"Field first_name is required"]; }
        if( empty($post->facebook_id) ){ return ['error'=>"Field facebook_id is required"]; }
        if( empty($post->device_uid) ){ return ['error'=>"Field device_uid is required"]; }
        
        $device = Device::register($post->device_uid);

        $user = User::findByFacebookId($post->facebook_id);

        if( $user->id>0 ){
            $user->first_name = $post->first_name;
            if( empty($user->token) ){
                $user->token = User::generateToken();
            }
            $user->save();
        }else{
            $user = new User();
            $user->usergroup = 'user';
            $user->first_name = $post->first_name;
            $user->email = $user->username = $post->facebook_id.'@facebook.com';
            $user->facebook_id = $post->facebook_id;
            $user->password = md5($post->facebook_id);

            $user->device_uid = $post->device_uid;
            if( empty($user->token) ){
                $user->token = User::generateToken();
            }
            $user->save();
        }
        $user_id = $user->id;

        $user->saveLastPosition();

        $user = User::find($user_id);
        User::syncPaid($user,$device);

        return ApiUser::find($user_id);

        /*{"Error":"Not implemented yet","received_data":{"id":"0","created_at":"1\/1\/0001 12:00:00 AM","updated_at":"1\/1\/0001 12:00:00 AM","email":"almin_bug@hotmail.com","first_name":"Almin Milanovic","is_paid":"False","paid_until":"1\/1\/0001 12:00:00 AM","facebook_id":"10153564570002525","region_id":"1","lat":"0","lon":"0","device_uid":"E76E8938-1514-4DE4-A0F2-E199A263E68F"}}
        */

    }


    /**
     * 
     *
     * @return Response
     */
    public function postUserLogout()
    {

    }


    /**
     * 
     *
     * @return Response
     */
    public function postGcm()
    {
        $post = Request::postAsObject();
        $device_uid = $post->uid;
        $platform = $post->p;
        $gcm = $post->gcm;

        if( $platform=='android' or $platform=='a' ){
            $platform = 'android';
        }else{
            $platform = 'ios';
        }

        $device = Device::register($device_uid);
        $device->platform = $platform;
        $device->gcm = $gcm;
        $device->save();

        return $device;
    }

}
