<?php

class BraintreeController extends Controller
{

	public function __construct()
	{
		parent::__construct();
    	Braintree_Configuration::environment( config('braintree.environment') );
		Braintree_Configuration::merchantId( config('braintree.merchant_id') );
		Braintree_Configuration::publicKey( config('braintree.public_key') );
		Braintree_Configuration::privateKey( config('braintree.private_key') );
	}


    /**
     * GET /braintree/client-token
     * generate braintree client token
     */
    public function getClientToken()
    {
    	$clientToken = Braintree_ClientToken::generate();
        return [
        	'clientToken'=>$clientToken
        ];
    }


    /**
     * POST /braintree/payment-method
     * receive payment method nonce
     */
    public function postPaymentMethod()
    {
    	$post = Request::all();

        // promocode usage add nesto

        //
        if( empty($post['device_uid']) ){
            return ['success'=>false,'message'=>"Missing device_uid"];
        }
        $device_uid = $post['device_uid'];

        //
        if( empty($post['package_id']) ){
            return ['success'=>false,'message'=>"Missing package_id"];
        }
        $package_id = $post['package_id'];

        //
        $packages = $this->packages($post['promocode']);
        $package = [];
        foreach($packages as $item){
            if( $item['id']==$package_id ){
                $package = $item;
            }
        }
        if( empty($package) ){
            return ['success'=>false,'message'=>"Package not found"];
        }

        $amount = $package['amount'];

        if( (int)$amount===0 ){
            // check promocode, if valid set user/device to paid
            if(!empty($post['promocode'])){
                $promo = PromoCode::valid()->whereCode($post['promocode'])->first();
                if( $promo->id>0 ){
                    $promo->markAsUsed();

                    $json['success'] = true;
                    $json['device_uid'] = $device_uid;
                    $json['transaction_id'] = strval(rand(100,10000000));

                    $device = Device::register($device_uid);
                    $device->is_paid = 1;
                    if( empty($device->paid_until) ){
                        $str = 'now';
                    }else if( strtotime($device->paid_until)<time() ){
                        $str = 'now';
                    }else if( strtotime($device->paid_until)>=time() ){
                        $str = $device->paid_until;
                    }
                    $device->paid_until = date('Y-m-d H:i:s', strtotime($str.' +'.$package['duration'].' days') );
                    $device->save();

                    $json['is_paid'] = $device->is_paid;
                    $json['paid_until'] = $device->paid_until;
                    
                    if( !empty($post['token']) ){
                        $user = User::whereToken($post['token'])->first();
                        if( $user->id>0 ){
                            User::syncPaid($user,$device);
                            $user = User::whereToken($post['token'])->first();
                            $json['is_paid'] = $user->isPaid();
                            $json['paid_until'] = $user->paid_until;
                        }
                    }
                    
                    // notify post affiliate app
                    include_once(APPDIR.'/libraries/PapApi.class.php');
                    $pap_url = 'https://crodeals.postaffiliatepro.com';

                    $saleTracker = new Pap_Api_SaleTracker($pap_url.'/scripts/sale.php');

                    $sale = $saleTracker->createSale();
                    $sale->setProductID($package['title']);
                    $sale->setCouponCode($post['promocode']);
                    $sale->setAffiliateId($promo->affiliate_id);
                    $sale->setTotalCost($package['amount']);
                    $sale->setOrderID($result->transaction->id);

                    $saleTracker->register();


                    Audit::log( 'braintree', "Skipped payment", $json );
                    return $json;

                }
            }
        }

        //
        if( empty($post['payment_method_nonce']) ){
            return ['success'=>false,'message'=>"Payment method nonce is required"];
        }
        $nonce = $post['payment_method_nonce'];


    	$result = Braintree_Transaction::sale([
				'amount' => number_format($amount,2,'.',''),
				'paymentMethodNonce' => $nonce,
				'options' => [
				    //'submitForSettlement' => true
				]
			]);

/*
        print_r($result->errors->deepAll());
        exit();
*/
        $json = [];
        $json['success'] = $result->success;
        $json['transaction_status'] = $result->transaction->status;
        $json['device_uid'] = $device_uid;
        
        if( $result->success ){
            $json['transaction_id'] = $result->transaction->id;
            //$json['transaction'] = (array)$result->transaction->_attributes;
            $device = Device::register($device_uid);
            $device->is_paid = 1;
            if( empty($device->paid_until) ){
                $str = 'now';
            }else if( strtotime($device->paid_until)<time() ){
                $str = 'now';
            }else if( strtotime($device->paid_until)>=time() ){
                $str = $device->paid_until;
            }
            $device->paid_until = date('Y-m-d H:i:s', strtotime($str.' +'.$package['duration'].' days') );
            $device->save();

            $json['is_paid'] = $device->is_paid;
            $json['paid_until'] = $device->paid_until;
            
            if( !empty($post['token']) ){
                $user = User::whereToken($post['token'])->first();
                if( $user->id>0 ){
                    User::syncPaid($user,$device);
                    $user = User::whereToken($post['token'])->first();
                    $json['is_paid'] = $user->isPaid();
                    $json['paid_until'] = $user->paid_until;
                }
            }

            if(!empty($post['promocode'])){
                $promo = PromoCode::valid()->whereCode($post['promocode'])->first();
                if( $promo->id>0 ){
                    $promo->markAsUsed();
                    
                    // notify post affiliate app
                    include_once(APPDIR.'/libraries/PapApi.class.php');
                    $pap_url = 'https://crodeals.postaffiliatepro.com';

                    $saleTracker = new Pap_Api_SaleTracker($pap_url.'/scripts/sale.php');

                    $sale = $saleTracker->createSale();
                    $sale->setProductID($package['title']);
                    $sale->setCouponCode($post['promocode']);
                    $sale->setAffiliateId($promo->affiliate_id);
                    $sale->setTotalCost($package['amount']);
                    $sale->setOrderID($result->transaction->id);

                    $saleTracker->register();

                }
            }

        }else{
            
        }

        Audit::log( 'braintree', "Payment method", $json );
    	return $json;
    }


   /**
     * List all packages
     */
    public function packages($promocode='')
    {
        $discount = 0;
        $is_promo = false;
        if( !empty($promocode) ){
            $promo = PromoCode::valid()->whereCode($promocode)->first();
            if( $promo->id>0 ){
                $discount = (float)$promo->discount;
                if( $discount>0 ){
                    $is_promo = true;
                }
            }
        }

        $packages = [
            [
                'id'    => 10,
                'title' => "Activate for 3 days",
                'description'   => "Use great deals for 3 full days in any of our locations",
                'amount' => 15.0,
                'currency'  => 'EUR',
                'amount_formated'   => '15.00 €',
                'duration'  => 3,
                'is_promo'  => $is_promo,
            ],
            [
                'id'    => 20,
                'title' => "Activate for 10 days",
                'description'   => "Use great deals for 10 full days in any of our locations",
                'amount' => 22.0,
                'currency'  => 'EUR',
                'amount_formated'   => '22.00 €',
                'duration'  => 10,
                'is_promo'  => $is_promo,
            ],
        ];
        
        foreach($packages as $index=>$package){
            $package['amount'] = round($package['amount'] * (1-($discount/100)),2);
            $package['amount_formated'] = number_format($package['amount'],2,'.','').' €';
            $packages[$index] = $package;
        }
        
        return $packages;
    }



/*
    public function test()
    {
        $post = Request::get();

        //
        if( empty($post['device_uid']) ){
            return ['success'=>false,'message'=>"Missing device_uid"];
        }
        $device_uid = $post['device_uid'];

        //
        if( empty($post['package_id']) ){
            return ['success'=>false,'message'=>"Missing package_id"];
        }
        $package_id = $post['package_id'];

        //
        $packages = $this->packages();
        $package = [];
        foreach($packages as $item){
            if( $item['id']==$package_id ){
                $package = $item;
            }
        }
        if( empty($package) ){
            return ['success'=>false,'message'=>"Package not found"];
        }

        $amount = $package['amount'];


        $user = Device::register($device_uid);
        $user->is_paid = 1;
        //echo $user->paid_until;
        if( empty($user->paid_until) ){
            $str = 'now';

        }else if( strtotime($user->paid_until)<time() ){
            $str = 'now';
        }else if( strtotime($user->paid_until)>=time() ){
            $str = $user->paid_until;
        }
        $user->paid_until = date('Y-m-d H:i:s', strtotime($str.' +'.$package['duration'].' days') );
        $user->save();


        return $user;
    }
*/


}