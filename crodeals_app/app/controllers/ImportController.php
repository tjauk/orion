<?php

class ImportController extends Controller
{

    function restorani2()
    {
    	$map = explode(',',"rb,title,discount,discount_note,placanje,period,note,tripadvisor,address,city");
    	$csv = File::load(ROOTDIR.'/files/2016-05-08 - dealovi.csv');
    	$rows = explode("\n",$csv);
		foreach($rows as $index=>$row){
			$row = explode("\t",$row);
			$rows[$index] = $row;
		}

		$data = [];
		foreach($rows as $row){
			$dat = array();
			foreach($map as $index=>$mapkey){
				$dat[$mapkey] = trim( $row[$index] );
			}
			$dat['discount'] = trim(str_replace("%","",$dat['discount']));
			$period = explode('-',$dat['period']);
			$dat['valid_from'] = date('Y-m-d',strtotime($period[0]));
			$dat['valid_to'] = date('Y-m-d',strtotime($period[1]));

			if( !empty($dat['title']) ){
				$data[]=$dat;
			}
		}
		unset($data[0]);
		foreach($data as $row){
			if( Deal::where('title','=',$row['title'])->count()>0 ){
				echo "Already exists ".$row['title'].br();
			}else{
				$deal = new Deal();
				$deal->fill($row);
				$deal->checked=0;
				$deal->save();
				
				if( !empty($deal->tripadvisor) ){
					//$trip = Tripadvisor::parseRestaurant($deal->tripadvisor);
					//pr($trip);
					//exit();
				}
				//Deal::create($deal);
				if( !empty($row['address']) ){

					$geo = new Place();
                    $geo->geoCodeByAddress($row['address'].' , '.$row['city'].' , Croatia');

					$place = new Place();
					$place->name = $deal->title;
					$place->address = $row['address'];
					$place->city = $row['city'];
					$place->country_id = 52;

					$place->lat = $geo->lat;
                    $place->lon = $geo->lon;
                    $place->region_id = $geo->region_id;

                    $place->save();

                    $deal->place_id = $place->id;
                    $deal->region_id = $place->region_id;
                    $deal->save();
				}
			}
		}
		pr($data);
		//Table::make($data)->out();
    }

}
