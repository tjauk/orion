<?php
class PageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Page::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function view($slug=0)
    {
        if( is_numeric($slug) ){
            $page = Page::find($slug);
        }else{
            $page = Page::findBySlug($slug);            
        }
        Doc::json();
        
        return $page;

        /*
        Doc::title($page->title);
        
        return View::make('page/view')
                    ->put($page,'page');
        */
    }


}
