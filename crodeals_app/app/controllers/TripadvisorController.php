<?php
use Symfony\Component\DomCrawler\Crawler;

class TripadvisorController extends Controller
{

	public function __construct()
	{
		parent::__construct();
	}


    /**
     * GET /tripadvisor/parse
     */
    public function parse()
    {
        Doc::json();

        $url = trim(Request::post('url'));

        $data = Tripadvisor::parseRestaurant($url);

        return $data;
    }




}