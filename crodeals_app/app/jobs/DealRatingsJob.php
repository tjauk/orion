<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use League\CLImate\CLImate;


// Example: orion deal:ratings

class DealRatingsJob extends Command
{
    protected function configure()
    {
        $this
            ->setName('deal:ratings')
            ->setDescription('Recalculate all deal ratings')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $climate = new CLImate;

        $deals = Deal::all();
        foreach($deals as $deal){
            $deal->recalculateRating();
            $output->writeln($deal->rating_calc.' - '.$deal->title);
        }

        $output->writeln('DONE');
    }
}
