<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use League\CLImate\CLImate;


// Example: orion bee:get

class ImportRestoraniJob extends Command
{
    protected function configure()
    {
        $this
            ->setName('import:restorani')
            ->setDescription('Import restorani.csv')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $climate = new CLImate;

        $map = explode(',',"rb,title,discount,discount_note,placanje,period,note,tripadvisor");
        $csv = File::load(ROOTDIR.'/files/restorani.csv');
        $rows = explode("\n",$csv);
        unset($rows[0]);
        foreach($rows as $index=>$row){
            $row = explode("\t",$row);
            $rows[$index] = $row;
        }

        $data = [];
        foreach($rows as $row){
            $dat = array();
            foreach($map as $index=>$mapkey){
                $dat[$mapkey] = trim( $row[$index] );
            }

            if( !empty($dat['title']) ){

                $dat['discount'] = trim(str_replace("%","",$dat['discount']));
                $period = explode('-',$dat['period']);
                $dat['valid_from'] = date('Y-m-d',strtotime($period[0]));
                $dat['valid_to'] = date('Y-m-d',strtotime($period[1]));

                if( !empty($dat['tripadvisor']) ){
                    $output->write('Parsing TA link ... ');
                    $ta = Tripadvisor::parseRestaurant($dat['tripadvisor']);
                    if( $ta['success']==true ){
                        $output->writeln('OK');
                        $dat = array_merge($ta,$dat);
                        $dat['photos'] = $dat['photo'];
                    }else{
                        $output->writeln('Not found');
                    }
                }

                // register Place
                if( !empty($dat['address']) and !empty($dat['city']) ){
                    $place = Place::whereAddress($dat['address'])->whereCity($dat['city'])->whereZip($dat['zip'])->first();
                    if( $place->id>0 ){

                    }else{
                        $place = new Place();
                        $place->name = $dat['title'];
                        $place->address = $dat['address'];
                        $place->zip = $dat['zip'];
                        $place->city = $dat['city'];
                        $place->country_id = 52;
                        $place->geoCodeByAddress();
                        $place->save();
                    }
                    $dat['place_id'] = $place->id;
                }

                // register Deal

                $deal = Deal::wherePlaceId($dat['place_id'])->whereTitle($dat['title'])->first();
                if( $deal->id>0 ){
                    $output->write('Skipped ');
                }else{
                    $dat['verified']=0;
                    $deal = new Deal();
                    $deal->fill($dat);
                    $deal->save();
                    $output->write('Added new deal ');
                }

                $output->writeln($deal->title);
            }
            
        }
        


        $output->writeln('DONE');
    }
}
