<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use League\CLImate\CLImate;


// Example: orion bee:get

class PushSendJob extends Command
{
    protected function configure()
    {
        $this
            ->setName('push:send')
            ->setDescription('Send push notifications from queue')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $climate = new CLImate;

        $queued = PushQueue::next(10)->get();

        foreach($queued as $push){
            $results = $push->send();
            print_r($results);
        }
        


        $output->writeln('DONE');
    }
}
