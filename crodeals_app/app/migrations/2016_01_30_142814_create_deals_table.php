<?php

class CreateDealsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->integer('company_id')->nullable();
            $table->string('title')->default('');
            $table->text('description');
            $table->string('rating')->default('');
            $table->string('tripadvisor')->default('');
            $table->string('discount')->default('');
            $table->string('discount_note')->default('');
            $table->integer('city_id')->nullable();
            $table->dateTime('valid_from')->nullable();
            $table->dateTime('valid_to')->nullable();
            $table->text('note');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deals');
    }

}
