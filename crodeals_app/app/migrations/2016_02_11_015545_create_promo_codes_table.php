<?php

class CreatePromoCodesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_codes', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('code')->default('');
            $table->text('description');
            $table->string('discount')->default('');
            $table->string('discount_note')->default('');
            $table->dateTime('valid_from')->nullable();
            $table->dateTime('valid_to')->nullable();

            $table->integer('max_usage')->nullable();
            $table->integer('used')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('promo_codes');
    }

}
