<?php

class CreatePlacesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function($table)
        {
            $table->increments('id');
            $table->timestamps();

            $table->string('name')->default('');
            $table->string('slug')->default('');

            $table->string('address')->default('');
            $table->string('zip')->default('');
            $table->string('city')->default('');
            $table->integer('country_id')->unsigned()->nullable();

            $table->decimal('lat',10,8)->nullable();
            $table->decimal('lon',11,8)->nullable();
            $table->integer('alt')->nullable();
            $table->integer('radius')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('places');
    }

}
