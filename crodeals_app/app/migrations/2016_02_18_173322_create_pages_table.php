<?php

class CreatePagesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('status')->default('draft');
            $table->dateTime('published_at')->nullable();
            $table->string('title')->default('');
            $table->string('slug')->default('');
            $table->text('excerpt')->default('');
            $table->text('content')->default('');
            $table->text('photos')->default('');
            $table->text('tags')->default('');
            $table->tinyInteger('featured')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }

}
