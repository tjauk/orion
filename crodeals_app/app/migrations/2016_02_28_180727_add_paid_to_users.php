<?php

class AddPaidToUsers extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->tinyInteger('is_paid')->default(0);
            $table->dateTime('paid_until')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('is_paid');
            $table->dropColumn('paid_until');
        });
    }

}
