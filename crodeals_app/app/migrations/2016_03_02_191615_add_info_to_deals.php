<?php

class AddInfoToDeals extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deals', function($table)
        {
            $table->string('info_web')->default('');
            $table->string('info_email')->default('');
            $table->string('info_phone')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deals', function($table)
        {
            $table->dropColumn('info_web');
            $table->dropColumn('info_email');
            $table->dropColumn('info_phone');
        });
    }

}
