<?php

class CreateMenusTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->default('');
            $table->string('slug')->default('');
            $table->string('group')->default('');
            $table->integer('pri')->unsigned()->default(0);
            $table->text('images')->default('');
            $table->text('icons')->default('');
            $table->text('action')->default('');
            $table->tinyInteger('active')->default(1);
            $table->tinyInteger('private')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menus');
    }

}
