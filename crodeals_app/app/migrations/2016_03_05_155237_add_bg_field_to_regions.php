<?php

class AddBgFieldToRegions extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('regions', function($table)
        {
            $table->text('background')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('regions', function($table)
        {
            $table->dropColumn('background');
        });
    }

}
