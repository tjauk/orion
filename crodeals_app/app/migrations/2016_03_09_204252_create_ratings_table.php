<?php

class CreateRatingsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function($table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('item')->default('Deal');
            $table->bigInteger('item_id')->unsigned()->default(0);
            $table->float('score');
            $table->string('device_uid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ratings');
    }

}
