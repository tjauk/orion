<?php

class AddColorToMenus extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menus', function($table)
        {
            $table->string('color')->default('#777');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menus', function($table)
        {
            $table->dropColumn('color');
        });
    }

}
