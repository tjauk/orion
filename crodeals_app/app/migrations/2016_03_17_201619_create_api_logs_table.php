<?php

class CreateApiLogsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_logs', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('method',10)->default('GET');
            $table->string('url')->default('');
            $table->text('params');

            $table->string('device_uid')->default('');
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('region_id')->nullable()->unsigned();
            $table->decimal('lat',10,8)->nullable();
            $table->decimal('lon',11,8)->nullable();

            $table->string('ip_address',20)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('api_logs');
    }

}
