<?php

class AddIpAddressToApiLogsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('api_logs', function($table)
        {
            $table->string('ip_address',20)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('api_logs', function($table)
        {
            $table->dropColumn('ip_address');
        });
    }

}
