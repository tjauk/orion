<?php

class AddLastLatLonRegionToUsers extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->decimal('lat',10,8)->nullable();
            $table->decimal('lon',11,8)->nullable();
            $table->integer('region_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('lat');
            $table->dropColumn('lon');
            $table->dropColumn('region_id');
        });
    }

}
