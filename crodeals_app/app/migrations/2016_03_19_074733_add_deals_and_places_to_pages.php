<?php

class AddDealsAndPlacesToPages extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages', function($table)
        {
            $table->text('deals')->default('');
            $table->text('places')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pages', function($table)
        {
            $table->dropColumn('deals');
            $table->dropColumn('places');
        });
    }

}
