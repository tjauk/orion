<?php

class CreateDevicesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id')->nullable()->unsigned();
            $table->dateTime('last_seen');
            $table->string('device_uid')->unique()->default('');
            $table->string('token')->default('');

            $table->decimal('lat',10,8)->nullable();
            $table->decimal('lon',11,8)->nullable();
            $table->integer('region_id')->unsigned()->nullable();

            $table->tinyInteger('is_paid')->default(0);
            $table->dateTime('paid_until')->nullable();
            $table->string('platform')->nullable();
            $table->string('version')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('devices');
    }

}
