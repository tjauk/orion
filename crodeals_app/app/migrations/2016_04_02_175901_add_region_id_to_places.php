<?php

class AddRegionIdToPlaces extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('places', function($table)
        {
            $table->integer('region_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('places', function($table)
        {
            $table->dropColumn('region_id');
        });
    }

}
