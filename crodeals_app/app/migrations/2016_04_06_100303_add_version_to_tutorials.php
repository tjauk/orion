<?php

class AddVersionToTutorials extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutorials', function($table)
        {
            $table->integer('version')->default(0)->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutorials', function($table)
        {
            $table->dropColumn('version');
        });
    }

}
