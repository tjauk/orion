<?php

class AddCheckedToDeals extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deals', function($table)
        {
            $table->tinyInteger('checked')->default(0);
            $table->float('rating_calc')->default(0.0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deals', function($table)
        {
            $table->dropColumn('checked');
            $table->dropColumn('rating_calc');
        });
    }

}
