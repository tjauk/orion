<?php

class AddGcmToDevices extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('devices', function($table)
        {
            $table->string('gcm')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('devices', function($table)
        {
            $table->dropColumn('gcm');
        });
    }

}
