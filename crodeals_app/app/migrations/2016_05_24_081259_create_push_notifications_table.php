<?php

class CreatePushNotificationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push_notifications', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('title')->default('');
            $table->text('message')->default('');
            $table->text('action')->default('');
            $table->string('to_group');
            $table->tinyInteger('to_ios')->unsigned()->default(0);
            $table->tinyInteger('to_android')->unsigned()->default(0);
            $table->string('status')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('push_notifications');
    }

}
