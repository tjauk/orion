<?php

class CreatePushQueuesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push_queues', function($table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('device_id')->unsigned();
            $table->integer('push_notification_id')->unsigned();
            $table->tinyInteger('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('push_queues');
    }

}
