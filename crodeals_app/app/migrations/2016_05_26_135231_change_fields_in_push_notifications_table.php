<?php

class ChangeFieldsInPushNotificationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('push_notifications', function($table)
        {
            $table->dropColumn('to_ios');
            $table->dropColumn('to_android');
            $table->dropColumn('to_group');
        });
        Schema::table('push_notifications', function($table)
        {
            $table->text('groups')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('push_notifications');
    }

}
