<?php

class AddLangsToTutorials extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutorials', function($table)
        {
            $table->text('images_ml')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutorials', function($table)
        {
            $table->dropColumn('images_ml');
        });
    }

}
