<?php

class AddAffiliateIdToPromocodes extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promo_codes', function($table)
        {
            $table->string('affiliate_id')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promo_codes', function($table)
        {
            $table->dropColumn('affiliate_id');
        });
    }

}
