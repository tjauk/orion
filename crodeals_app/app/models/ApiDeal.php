<?php
class ApiDeal extends Deal
{
    
    protected $hidden = [
        'created_at',
        //'updated_at',
        'place_lat',
        'place_lon',
        'company_id',
        'place_id',
        'city_id',
        'note',
        'checked',
    ];

    protected $appends = [
        'tripadvisor_reviews',
        'html_desc'
    ];

    public function getDiscountNoteAttribute()
    {
        return mb_strtoupper($this->attributes['discount_note']);
    }

    public function getPhotosAttribute()
    {
        $photos = array_filter(explode(';',$this->attributes['photos']));
        $items = [];
        foreach($photos as $item){
            if( !empty($item) ){
                $img = Img::make($item);
                if( $img->width()>1000 or $img->height()>900 ){
                    $items[] = $img->thumb('1080x600xA')->url();
                }else{
                    $items[] = $img->url();
                }
            }
        }
        return $items;
    }

    public function getRatingAttribute()
    {
        $rating = $avg = Rating::average('Deal',$this->id);
        if( floatval($this->attribute['rating'])>0 ){
            $rating = ( $rating + floatval($this->attribute['rating']) ) / 2;
        }
        return $rating;
    }

    public function getTripadvisorReviewsAttribute()
    {
        if( !empty($this->tripadvisor) ){
            return $this->tripadvisor.'#REVIEWS';
        }
        return '';
    }

    public function getHtmlDescAttribute()
    {
        return str_replace("\r\n","<br>\n",$this->description);
    }

}
