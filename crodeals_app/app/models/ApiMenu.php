<?php
class ApiMenu extends Menu
{

    protected $hidden = [
        'created_at',
        'active',
        'private',
    ];

    public function getImagesAttribute()
    {
        $photos = array_filter(explode(';',$this->attributes['images']));
        $items = [];
        foreach($photos as $item){
            if( !empty($item) ){
                $items[] = Img::thumb($item,'1080x400xA')->url();
            }
        }
        return $items;
    }

    public function getIconsAttribute()
    {
        $icons = array_filter(explode(';',$this->attributes['icons']));
        $items = [];
        foreach($icons as $item){
            if( !empty($item) ){
                $items[] = Img::url($item);
            }
        }
        return $items;
    }

}
