<?php
class ApiPlacePin extends ApiPlace
{
    public $_deals = [];
    public $_pages = [];

    protected $appends = [
        'phone',
        'action',
    	'icon',
    ];

    public function getIconAttribute()
    {
        /*
        if( count($this->deals)>0 ){
            $deal = ApiDeal::find($this->deals[0]);
            $icon = "default";
        }
        */
        if( count($this->pages)>0 ){
            $page = ApiPage::find($this->pages[0]);
            $icon = $page->menu->category->icon;
        }
        if( empty($icon) ){
            $icon = "default";
        }
    	return $icon;
    }

    public function getPhoneAttribute()
    {
    	$deals = Deal::wherePlaceId($this->id)->get(['id','info_phone']);
    	if( count($deals)>0 ){
    		$deal = $deals[0];
    		if( !empty($deal->info_phone) ){
    			return $deal->info_phone;
    		}
    	}
    	return '';
    }

    public function getActionAttribute()
    {
    	$deals = $this->deals;
    	if( count($deals)>0 ){
    		$command = "dealsMore\nid:".$deals[0];
    		return Action::parse($command);
    	}
    	$pages = $this->pages;
    	if( count($pages)>0 ){
    		$command = "page\nid:".$pages[0];
    		return Action::parse($command);
    	}
    	return null;
    }


    public function getPagesAttribute()
    {
    	$ids = [];
        if( empty($this->_pages) ){
    	   $pages = ApiPage::whereRaw('FIND_IN_SET('.$this->id.',places)')->get(['id']); 
        }else{
            $pages = $this->_pages;
        }
    	foreach($pages as $page){
    		$ids[] = $page->id;
    	}
    	return $ids;
    }

    public function getDealsAttribute()
    {
    	$ids = [];
        if( empty($this->_deals) ){
            $deals = Deal::wherePlaceId($this->id)->get(['id']);
        }else{
            $deals = $this->_deals;
        }
    	foreach($deals as $deal){
    		$ids[] = $deal->id;
    	}
    	return $ids;
    }

}
