<?php
class ApiRegion extends Region
{

    public function getBackgroundAttribute()
    {
        $background = array_filter(explode(';',$this->attributes['background']));
        $items = [];
        foreach($background as $item){
            if( !empty($item) ){
                $items[] = Img::url($item);
            }
        }
        return $items;
    }

}
