<?php
class ApiTutorial extends Tutorial
{

    public $appends = [];
    public $hidden = ['images_ml'];

    public function getImagesAttribute()
    {
        $lang = strtolower( Request::get('lang') );

        $images = '';

        $ml = json_decode($this->attributes['images_ml'],true);
        if( isset($ml[$lang]) ){
            $bla = (string)$ml[$lang];
            $images = array_filter( explode(';', $bla) );
        }

        if( empty($images) ){
            $images = array_filter( explode(';', $this->attributes['images']) );
        }

        $items = [];
        foreach($images as $item){
            if( !empty($item) ){
                $img = Img::make($item);
                $items[] = $img->url();
            }
        }

        return $items;
    }

}
