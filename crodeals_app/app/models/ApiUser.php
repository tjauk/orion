<?php 

class ApiUser extends User {

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [
		'username',
		'password',
		'usergroup',
		'remember_token',
		'last_seen',
		'blocked',
		'device_uid'
	];

    protected $casts = [
        'lat'   => 'double',
        'lon'   => 'double',
    ];
    	
}
