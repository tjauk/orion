<?php
class Company extends Model
{
    protected $table = 'companies';

    protected $fillable = [
		'name',
		'oib',
		'address',
		'zip',
		'city',
		'country_id',
		'note'
    ];

    /*
     * Relations
     */
    public function country()
    {
        return $this->belongsTo('Country');
    }



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */


}
