<?php
class Device extends Model
{
    protected $table = 'devices';

    protected $fillable = [
		'device_uid',
        'user_id',
        'last_seen',
		'token',
		'lat',
		'lon',
		'region_id',
		'is_paid',
		'paid_until',
		'platform',
		'version',
        'gcm',
        'test'
    ];

    /*
     * Relations
     */
    public function region()
    {
        return $this->belongsTo('Region');
    }
    public function user()
    {
        return $this->belongsTo('User');
    }

    static function register($device_uid)
    {
        if( !empty($device_uid) ){

            $device = Device::findByDeviceUid($device_uid);

            // new device
            if( !$device ){
                $device = new Device();
                $device->device_uid     = $device_uid;
                $device->last_seen = date('Y-m-d H:i:s');
                $device->save();
            }

            // platform
            if( is_null($device->platform) ){
                $get = Request::getAsObject();
                if( $get->p=='a' ){
                    $device->platform = 'android';
                }else{
                    $device->platform = 'ios';
                }
                $device->save();
            }
            // token (for push notifications)
            if( empty($device->token) ){
                $get = Request::getAsObject();
                if( !empty($get->pushtoken) ){
                    $device->token = $get->pushtoken;
                    $device->save();
                }
            }

            // save position
            $device->saveLastPosition();

            return $device;
        }
        return false;
    }


    /*
     * Last position
     */
    function saveLastPosition()
    {
        $data = Request::all();
        $keys = ['lat','lon','region_id'];
        $changed = false;
        foreach ($keys as $key) {
            if( isset($data[$key]) and !empty($data[$key]) and $data[$key]!=="null" ){
                if( $key!=='region_id' ){
                    $this->$key = $this->sanitizeLatLon($data[$key]);
                }else{
                    $this->$key = $data[$key];
                }
                $changed = true;
            }
        }
        if( $changed ){
            $this->save();

            if( $this->user() ){
                $user = $this->user();
                if( $user->id>0 ){
                    $user->saveLastPosition();
                }
            }
        }
    }

    private function sanitizeLatLon($ll)
    {
        $ll = str_replace(',', '.', $ll);
        return (float)$ll;
    }
    

    function isPaid()
    {
        $date = date('Y-m-d',strtotime('now'));
        if( $this->attributes['paid_until'] >= $date ){
            return true;
        }
        return false;
    }

    public function getPaidUntilAttribute()
    {
        if( empty($this->attributes['paid_until']) ){
            return '';
        }
        return date('Y-m-d',strtotime($this->attributes['paid_until']));
    }

    public function setPaidUntilAttribute($date)
    {
        if( empty($date) ){
            $date = null;
        }else{
            $date = date('Y-m-d',strtotime($date));
        }
        $this->attributes['paid_until'] = $date;
    }

    public function setLastSeenAttribute($date)
    {
        if( empty($date) ){
            $date = date('Y-m-d');
        }else{
            $date = date('Y-m-d',strtotime($date));
        }
        $this->attributes['last_seen'] = $date;
    }


}
