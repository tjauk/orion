<?php
class Menu extends Model
{

    use \Trinium\Traits\GetSetIconsAttribute;
    use \Trinium\Traits\GetSetImagesAttribute;

    protected $table = 'menus';

    protected $fillable = [
		'name',
		'slug',
		'group',
        'pri',
		'images',
		'icons',
		'action',
		'active',
		'private',
        'region_id',
        'color',
        'category_id',
    ];

    protected $hidden = [
        'created_at',
    ];

    protected $casts = [
        'pri'       => 'integer',
        'action'    => 'json',
    ];

    /*
     * Relations
     */
    public function region()
    {
        return $this->belongsTo('Region');
    }
    public function category()
    {
        return $this->belongsTo('MenuCategory','category_id');
    }

    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::groupBy('group')->orderBy('name',ASC)->get();

        foreach($items as $item){
            $options[$item->group] = $item->group;
        };

        return $options;
    }

    static function optionsAll()
    {
        $options = [''=>'---'];
        $items = static::orderBy('region_id',ASC)->orderBy('pri',ASC)->get();

        foreach($items as $item){
            $options[$item->id] = $item->title;
        };

        return $options;
    }


    /*
     * Scopes
     */


    /*
     * Getters
     */
    public function getTitleAttribute()
    {
        if( $this->region_id>0 ){
            return $this->name.' ('.$this->region->name.')';
        }
        return $this->name.' ('.$this->group.')';
    }
    public function getColorAttribute()
    {
        return $this->category->color;
    }


    /*
     * Setters
     */


    /*
     * Custom
     */
    static function reconnectActions()
    {
        $menus = Menu::all();
        foreach($menus as $menu){
            echo $menu->action;
            $action = Action::parse($menu->action);
            
            // convert explorePages to single page action
            if( $action->name == 'explorePages' and $menu->region_id>0 ){
                $page_count = Page::whereRaw("(menu_id='".$menu->id."' or FIND_IN_SET('".$menu->id."',menus))")->count();
                if( $page_count==1 ){
                    $page = Page::whereRaw("(menu_id='".$menu->id."' or FIND_IN_SET('".$menu->id."',menus))")->first();
                    $menu->action = "page\nid:{$page->id}";
                    $menu->save();
                }
            }

            // convert page to explorePages
            if( $action->name == 'page' and $menu->region_id>0 ){
                $page_count = Page::whereRaw("(menu_id='".$menu->id."' or FIND_IN_SET('".$menu->id."',menus))")->count();
                if( $page_count<>1 ){
                    $menu->action = "explorePages\nmenu_id:CURRENT\nregion_id:CURRENT";
                    $menu->save();
                }
            }

        }
    }
}
