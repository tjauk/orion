<?php
class MenuCategory extends Model
{
    protected $table = 'menu_categories';

    protected $fillable = [
		'name',
		'color',
        'icon'
    ];

    /*
     * Relations
     */
    public function menu()
    {
        $this->hasMany('Menu');
    }



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }

    static function icons()
    {
        $icons = ['default','culture','cuisine','nature','activities'];
        $options = [];
        foreach($icons as $icon){
            $options[$icon] = '<img src="'.ROOTURL.'/theme/images/flags/flag_'.$icon.'.png" alt="'.$icon.'" title="'.$icon.'">';
        }
        return $options;
    }


    /*
     * Scopes
     */


    /*
     * Getters
     */
    public function getIconUrlAttribute()
    {
        return ROOTURL.'/theme/images/flags/flag_'.$this->icon.'.png';
    }


    /*
     * Setters
     */


}
