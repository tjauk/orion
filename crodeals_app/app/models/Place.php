<?php
class Place extends Model
{
    protected $table = 'places';

    protected $fillable = [
		'name',
		'slug',
		'address',
		'zip',
		'city',
        'region_id',
		'country_id',
		'lat',
		'lon',
		'alt',
		'radius'
    ];

    protected $hidden = [
        'created_at',
        'slug',
        'alt',
        'radius',
    ];

    protected $casts = [
        'region_id' => 'integer',
        'lat'   => 'double',
        'lon'   => 'double',
    ];

    protected $defaults = [
        'country_id' => 52
    ];


    /*
     * Relations
     */
    public function region()
    {
        return $this->belongsTo('Region');
    }
    public function country()
    {
        return $this->belongsTo('Country');
    }
    public function deals()
    {
        return $this->belongsTo('Deal','id','place_id')->active();
    }

    /*
     * Cities
     */
    static function city_options()
    {
        $options = [''=>'---'];
        $items = static::groupBy('city')->orderBy('city',ASC)->get();

        foreach($items as $item){
            if( !empty($item->city) ){
                $options[$item->city] = $item->city;
            }
        };

        return $options;
    }

    public static function boot(){

        parent::boot();

        static::saving(function($model){
            if( empty($model->slug) ){
                $model->slug = str_slug($model->name);
            }
            if( (empty($model->lat) and empty($model->lon)) or empty($model->region_id) ){
                $model->geoCodeByAddress();
            }
        });
    }


    /*
     * Methods
     */
    public function geoCodeByAddress($address=null)
    {
        if( is_null($address) ){
            $address = $this->address.', '.$this->zip.' '.$this->city.', Croatia';
        }

        //echo $address;
        $results = Geo::code($address);

        if( $results['response']['status']=='ZERO_RESULTS' ){
            $address = $this->city.', Croatia';
            $results = Geo::code($address);
        }

        if( !empty($results['lat']) ){
            $this->lat = $results['lat'];
            $this->lon = $results['lon'];
        }

        // connect region
        $region_id = null;
        if( is_array($results['response']['results'][0]['address_components']) ){
            foreach($results['response']['results'][0]['address_components'] as $parts){
                $region_id = Region::matchCounty(str_slug($parts['long_name']));
                if( $region_id>0 ){
                    $this->region_id = $region_id;
                    break;
                }
            }
        }

        if( $this->region_id<1 ){
            //pr($results);
            //exit();
        }


        return $results;
    }


    /*
     * Form
     */
    static function createForm()
    {
        $form = Forms::make()->format('EDIT_VERTICAL');
        $form->fill(['Place.country_id'=>52]);
        $form->label('Place Name')->input('name');
        $form->label('Address')->input('address');
        $form->label('Zip')->input('zip');
        $form->label('City')->input('city');
        $form->label('Country')->select('country_id')->options(Country::options());
        return $form;
    }


    /*
     * Getters
     */
    public function getLatitudeAttribute()
    {
        return $this->lat;
    }
    public function getLongitudeAttribute()
    {
        return $this->lon;
    }


    /*
     * Setters
     */
    public function setSlugAttribute($str)
    {
        if( $str=='' ){
            $str=$this->name;
        }
        $this->attributes['slug'] = str_slug($str);
    }
    public function setLatAttribute($str)
    {
        if( $str=='' ){
            $str=null;
        }
        $this->attributes['lat'] = $str;
    }
    public function setLonAttribute($str)
    {
        if( $str=='' ){
            $str=null;
        }
        $this->attributes['lon'] = $str;
    }
    public function setLatitudeAttribute($str)
    {
        $this->setLatAttribute($str);
    }
    public function setLongitudeAttribute($str)
    {
        $this->setLonAttribute($str);
    }



}
