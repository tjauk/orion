<?php
class PromoCode extends Model
{
    protected $table = 'promo_codes';

    protected $fillable = [
		'code',
		'description',
		'discount',
		'discount_note',
		'valid_from',
		'valid_to',
		'max_usage',
		'used',
        'affiliate_id'
    ];

    /*
     * Relations
     */
    public function markAsUsed()
    {
        $this->used = $this->used + 1;
        $this->save();
    }

    /*
     * Scopes
     */
    public function scopeValid($query)
    {
        
        $query->where('valid_from', '<', \DB::raw('NOW()'));
        $query->where('valid_to', '>', \DB::raw('NOW()'));

        $query->where(function($query){
            $query->orWhere(function($query){
                $query->where('max_usage', '>', 0 );
                $query->where('max_usage', '>', 'used' );
            });
            $query->orWhere('max_usage','=',0);
        });

    }


    /*
     * Getters
     */

    /*
     * Setters
     */
    public function setCodeAttribute($code)
    {
        $this->attributes['code'] = strtoupper($code);
    }
    public function setValidFromAttribute($str)
    {
        $val = date('Y-m-d',strtotime($str));
        if( empty($str) or $str=='0000-00-00' ){
            $val = null;
        }
        $this->attributes['valid_from'] = $val;
    }
    public function setValidToAttribute($str)
    {
        $val = date('Y-m-d',strtotime($str));
        if( empty($str) or $str=='0000-00-00' ){
            $val = null;
        }
        $this->attributes['valid_to'] = $val;
    }

}
