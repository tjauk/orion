<?php
class PushNotification extends Model
{
    protected $table = 'push_notifications';

    protected $fillable = [
		'title',
		'message',
		'action',
		'groups',
		'status'
    ];

    static $device_groups = [
        'ios'       => "iOS devices",
        'android'   => "Android devices",
        //'paid'      => "Paid",
        //'notpaid'   => "Not paid",
        //'new'       => "New devices (today)",
        'test'      => "Test devices"
    ];

    /*
     * Relations
     */
    public function pushqueue()
    {
        return $this->hasMany('PushQueue');
    }



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */


    /*
     * Getters
     */
    public function getGroupsAttribute(){
        return explode(',',$this->attributes['groups']);
    }

    /*
     * Setters
     */
    public function setGroupsAttribute($groups){
        if( empty($groups) ){
            $groups=array();
        }
        if( !is_array($groups) ){
            $groups = explode(',',$groups);
        }
        $this->attributes['groups'] = implode(',',$groups);
    }
    

    /*
     * Filters
     */


    /*
     * Actions
     */
    public function createQueue()
    {
        $groups = $this->getGroupsAttribute();
        $devices = new Device();

        // ios and/or android
        if( in_array('ios',$groups) and in_array('android',$groups) ){
            // all
        }else if( in_array('ios',$groups) ){
            $devices = $devices->wherePlatform('ios');
        }else if( in_array('android',$groups) ){
            $devices = $devices->wherePlatform('android');
        }

        // only test
        if( in_array('test',$groups) ){
            $devices = $devices->whereTest(1);
        }
        
        // get devices
        $devices = $devices->get();
        foreach($devices as $device){
            PushQueue::create([
                'device_id' => $device->id,
                'push_notification_id'  => $this->id,
                'sent'  => 0
            ]);
        }
    }

    public function deleteQueue()
    {
        PushQueue::where('push_notification_id','=',$this->id)->delete();
    }

    public function resetQueue()
    {
        $this->deleteQueue();
        $this->createQueue();
    }


}
