<?php
class PushQueue extends Model
{
    protected $table = 'push_queues';

    protected $fillable = [
		'device_id',
		'push_notification_id',
		'status'
    ];

    /*
     * Relations
     */
    public function device()
    {
        return $this->belongsTo('Device');
    }
    public function push_notification()
    {
        return $this->belongsTo('PushNotification');
    }



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopeNext($query,$take=10)
    {
        $query->whereStatus(0)->take($take);
    }


    /*
     * Getters
     */


    /*
     * Setters
     */

    public function send()
    {
        $results = [];
        $device = $this->device;

        // send to android devices
        if( $device->platform=='android' ){
            $gcm                = new \Trinium\PushNotification\GoogleCloudMessaging();
            $gcm->tokens        = [$device->gcm];
            $gcm->title         = $this->push_notification->title;
            $gcm->message       = $this->push_notification->message; 
            $gcm->action        = Action::parse($this->push_notification->action);
            $results['android'] = $gcm->send();
        }

        // send to ios devices
        if( $device->platform=='ios' ){
            $apns               = new \Trinium\PushNotification\ApplePushNotificationService();
            $apns->tokens       = [$device->gcm];
            $apns->title        = $this->push_notification->title;
            $apns->message      = $this->push_notification->message; 
            $apns->action       = Action::parse($this->push_notification->action);
            $results['ios']     = $apns->send();
        }

        return $results;
    }


}
