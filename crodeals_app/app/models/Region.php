<?php
class Region extends Model
{
    protected $table = 'regions';

    protected $fillable = [
		'name',
		'country_id',
		'lat',
		'lon',
        'background',
        'description'
    ];

    /*
     * Relations
     */
    public function country()
    {
        return $this->belongsTo('Country');
    }



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }

    static function counties()
    {
        // political, long_name, administrative_area_level_1
        return [
            'bjelovar-bilogora-county'=>2,
            'bjelovarsko-bilogorska-zupanija'=>2,
            'brod-posavina-county'=>3,
            'brodsko-posavska-zupanija'=>3,
            'dubrovacko-neretvanska-zupanija'=>5,
            'dubrovnik-neretva-county'=>5,
            'istria-county'=>4,
            'istarska-zupanija'=>4,
            'karlovac-county'=>1,
            'karlovacka-zupanija'=>1,
            'koprivnica-krizevci-county'=>1,
            'koprivnicko-krizevacka-zupanija'=>1,
            'krapina-zagorje-county'=>2,
            'krapinsko-zagorska-zupanija'=>2,
            'licko-senjska-zupanija'=>4,
            'lika-senj-county'=>4,
            'medimurje-county'=>2,
            'medimurska-zupanija'=>2,
            'osijek-baranja-county'=>3,
            'osjecko-baranjska-zupanija'=>3,
            'osijecko-baranjska-zupanija'=>3,
            'pozega-slavonia-county'=>3,
            'pozesko-slavonska-zupanija'=>3,
            'primorje-gorski-kotar-county'=>4,
            'primorsko-goranska-zupanija'=>4,
            'sisak-moslavina-county'=>1,
            'sisacko-moslavacka-zupanija'=>1,
            'split-dalmatia-county'=>5,
            'splitsko-dalmatinska-zupanija'=>5,
            'sibenik-knin-county'=>5,
            'sibensko-kninska-zupanija'=>5,
            'varazdin-county'=>2,
            'varazdinska-zupanija'=>2,
            'virovitica-podravina-county'=>3,
            'viroviticko-podravska-zupanija'=>3,
            'vukovar-srijem-county'=>3,
            'vukovarsko-srijemska-zupanija'=>3,
            'zadar-county'=>5,
            'zadarska-zupanija'=>5,
            'zagreb-county'=>1,
            'zagrebacka-zupanija'=>1,
            'city-of-zagreb'=>1,
            'grad-zagreb'=>1,
        ];
    }
    static function matchCounty($str)
    {
        $region_id = null;
        $counties = static::counties();
        if( isset($counties[$str]) ){
            return $counties[$str];
        }
        return null;
    }



    /*
     * Getters
     */

    /*
     * Setters
     */
    public function setLatAttribute($str)
    {
        if( $str=='' ){
            $str=null;
        }
        $this->attributes['lat'] = $str;
    }
    public function setLonAttribute($str)
    {
        if( $str=='' ){
            $str=null;
        }
        $this->attributes['lon'] = $str;
    }

    public function getBackgroundAttribute()
    {
        return array_filter(explode(';',$this->attributes['background']));
    }

    public function setBackgroundAttribute($background)
    {
        if( is_array($background) ){
            $background = implode(';',array_filter($background));
        }
        if( is_null($background) ){
            $background = '';
        }
        $this->attributes['background'] = $background;
    }
}
