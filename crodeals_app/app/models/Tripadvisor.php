<?php
use Symfony\Component\DomCrawler\Crawler;

class Tripadvisor
{

    /**
     * Extract info from Tripadvisor restaurant page
     */
    static function parseRestaurant($url)
    {

        $body = Remote::get($url);

        $data = [];

        try {

            $html = new Crawler($body);
            
            $data['success'] = true;
            $data['url']     = $url;
            $data['title'] = $html->filter('#HEADING')->first()->text();
            $data['address'] = $html->filter('span[property="streetAddress"]')->first()->text();
            $data['city'] = $html->filter('span[property="addressLocality"]')->first()->text();
            $data['zip'] = $html->filter('span[property="postalCode"]')->first()->text();
            $data['country'] = $html->filter('span[property="addressCountry"]')->first()->text();
            
            $place = new Place();
            $place->address = $data['address'];
            $place->zip = $data['zip'];
            $place->city = $data['city'];
            $place->country_id =  52;
            $place->geoCodeByAddress();

            $data['region_id'] = $place->region_id;
            $data['phone'] = $html->filter('div.fl.phoneNumber')->first()->text();
            $data['rating'] = $html->filter('img[property="ratingValue"]')->first()->attr('alt');
            $data['rating'] = trim(str_replace(" of 5 stars","",$data['rating']));
            
            //$data['description'] = $html->filter('div.details_block div.content')->last()->text();

    /*
    ,   {"data":"https://media-cdn.tripadvisor.com/media/photo-s/06/f0/18/5d/fronta-part2.jpg","scroll":false,"tagType":"img","id":"HERO_PHOTO","priority":100,"logerror":false}
    */
            $lines = explode("\n",$body);
            foreach($lines as $line){
                if( strpos($line,"data") and strpos($line,"HERO_PHOTO") ){
                    $start = strpos($line,'"data":"')+8;
                    $end = strpos($line,'","scroll":');
                    $data['image'] = substr($line, $start,$end-$start);
                }
            }

            // save image to disk
            if( !empty($data['image']) ){
                $image = Remote::get($data['image']);
                $ext = File::ext($data['image']);
                $filepath = File::unique(ROOTDIR.'/images/'.File::safe($data['title']).'.'.$ext);
                File::save($filepath,$image);

                $data['photo'] = str_replace(ROOTDIR,'',$filepath);
            }
            

            foreach($data as $key=>$val){
                $data[$key] = trim($val);
            }

            /*
            $found = $html->filter('a')->each(function (Crawler $node, $i) {
                return $node->attr('href');
            });
            $data['links']=$found;
            */

        } catch (\Exception $e) {
            return ['success'=>false,'error'=>$e->getMessage()];
        }

        return $data;
    }




}