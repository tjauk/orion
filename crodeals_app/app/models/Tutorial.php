<?php
class Tutorial extends Model
{
    //use \Trinium\Traits\GetSetImagesAttribute;

    public $table = 'tutorials';

    public $fillable = [
        'name',
        'screen',
        'version',
        'images',
        'images_ml',
        'images_en',
        'images_de',
        'images_it',
        'images_cz',
        'images_pl',
        'images_hu',
    ];

    public $appends = [
        'images_en',
        'images_de',
        'images_it',
        'images_cz',
        'images_pl',
        'images_hu',
    ];



    /*
     * Relations
     */



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */
    public function getImagesAttribute()
    {
        return array_filter(explode(';',$this->attributes['images']));
    }

    public function getMultiLangImages($lang)
    {
        $ml = json_decode($this->attributes['images_ml'],true);
        $images = (string)$ml[$lang];
        return array_filter(explode(';',$images));
    }

    public function getImagesEnAttribute(){ return $this->getMultiLangImages('en'); }
    public function getImagesDeAttribute(){ return $this->getMultiLangImages('de'); }
    public function getImagesItAttribute(){ return $this->getMultiLangImages('it'); }
    public function getImagesCzAttribute(){ return $this->getMultiLangImages('cz'); }
    public function getImagesPlAttribute(){ return $this->getMultiLangImages('pl'); }
    public function getImagesHuAttribute(){ return $this->getMultiLangImages('hu'); }

    

    /*
     * Setters
     */
    public function setImagesAttribute($images)
    {
        if( is_array($images) ){
            $images = implode(';',array_filter($images));
        }
        if( is_null($images) ){
            $images = '';
        }
        $this->attributes['images'] = $images;
    }

    public function setMultiLangImages($lang,$images)
    {
        if( is_array($images) ){
            $images = implode(';',array_filter($images));
        }
        if( is_null($images) ){
            $images = '';
        }
        $ml = json_decode($this->attributes['images_ml'],true);
        $ml[$lang] = $images;
        $this->attributes['images_ml'] = json_encode($ml);
    }

    public function setImagesEnAttribute($images){ $this->setMultiLangImages('en',$images); }
    public function setImagesDeAttribute($images){ $this->setMultiLangImages('de',$images); }
    public function setImagesItAttribute($images){ $this->setMultiLangImages('it',$images); }
    public function setImagesCzAttribute($images){ $this->setMultiLangImages('cz',$images); }
    public function setImagesPlAttribute($images){ $this->setMultiLangImages('pl',$images); }
    public function setImagesHuAttribute($images){ $this->setMultiLangImages('hu',$images); }


}
