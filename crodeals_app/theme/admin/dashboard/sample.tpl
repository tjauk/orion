<!-- Morris chart -->
@css "morris/morris.css"
<!-- Morris.js charts -->
@jsend "https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"
@jsend "morris/morris.min.js"
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<?php /*@jsend "pages/dashboard.js" */ ?>

  <!-- Content Header (Page header) -->
  <!--
        <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
  -->
  
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{Deal:active()->count()}}</h3>
                  <p>Active deals</p>
                </div>
                <div class="icon">
                  <i class="fa fa-calendar"></i>
                </div>
                <a href="/admin/deal" class="small-box-footer">show more <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{Page:count()}}</h3>
                  <p>Active pages</p>
                </div>
                <div class="icon">
                  <i class="fa fa-file-text"></i>
                </div>
                <a href="/admin/page" class="small-box-footer">show more <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{Place:count()}}</h3>
                  <p>Active places</p>
                </div>
                <div class="icon">
                  <i class="fa fa-flag"></i>
                </div>
                <a href="/admin/place" class="small-box-footer">show more <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

          </div>

          <!-- Main row -->
          <div class="row">


          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
