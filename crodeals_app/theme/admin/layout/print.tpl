<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{doc.title}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    @css "bootstrap.min.css"
    <!-- Font Awesome -->
    @css "https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"
    <!-- Ionicons -->
    @css "https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"
    <!-- font -->
    @css "https://fonts.googleapis.com/css?family=Roboto:400,700,300"
    <!-- Theme style -->
    @css "AdminLTE.min.css"
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    @css "skins/_all-skins.min.css"


    @css "styles.css"

    {{doc.css}}
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
body {
    /*
    -ms-transform: scale(0.7,0.7);
    -webkit-transform: scale(0.7,0.7);
    transform: scale(0.7,0.7);
    background-color: #fff;
    */
    font-family: Arial;
    font-size: 10px;
}
em {
    font-size: 9px;
}
.wrapper {
    width:680px;
    margin: auto 0;
}
.h1, h1 {
    font-size: 26px;
}
</style>

  </head>
  <body class="hold-transition">
    <div class="wrapper">

        <!-- Main content -->
        <section class="content">
          {{CONTENT}}
        </section>

    </div><!-- ./wrapper -->

  </body>
</html>
