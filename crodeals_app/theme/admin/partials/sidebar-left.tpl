<!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">GLAVNI IZBORNIK</li>
            
            @if Auth:can('menu.dashboard')
            <li>
              <a href="/admin/dashboard">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            @end

            @if Auth:can('menu.deal')
            <li>
              <a href="/admin/deal">
                <i class="fa fa-calendar"></i> <span>Deals</span>
                <small class="label pull-right bg-gray">{{Deal:count()}}</small>
              </a>
            </li>
            @end

            @if Auth:can('menu.place')
            <li>
              <a href="/admin/place">
                <i class="fa fa-flag"></i> <span>Places</span>
                <small class="label pull-right bg-gray">{{Place:count()}}</small>
              </a>
            </li>
            @end
            
            @if Auth:can('menu.region')
            <li>
              <a href="/admin/region">
                <i class="fa fa-flag"></i> <span>Regions</span>
                <small class="label pull-right bg-gray">{{Region:count()}}</small>
              </a>
            </li>
            @end
            
            @if Auth:can('menu.country')
            <li>
              <a href="/admin/country">
                <i class="fa fa-flag"></i> <span>Countries</span>
                <small class="label pull-right bg-gray">{{Country:count()}}</small>
              </a>
            </li>
            @end
            
            @if Auth:can('menu.promocode')
            <li>
              <a href="/admin/promocode">
                <i class="fa fa-dollar"></i> <span>Promo Codes</span>
                <small class="label pull-right bg-gray">{{PromoCode:count()}}</small>
              </a>
            </li>
            @end
            
            @if Auth:can('menu.page')
            <li>
              <a href="/admin/page">
                <i class="fa fa-file-text"></i> <span>Pages</span>
                <small class="label pull-right bg-gray">{{Page:count()}}</small>
              </a>
            </li>
            @end
            
            @if Auth:can('menu.menu')
            <li>
              <a href="/admin/menu">
                <i class="fa fa-bars"></i> <span>Menus</span>
                <small class="label pull-right bg-gray">{{Menu:count()}}</small>
              </a>
            </li>
            @end
            
<!--
            @if Auth:can('menu.contact')
            <li>
              <a href="/admin/contact">
                <i class="fa fa-user-plus"></i> <span>Imenik</span>
                <small class="label pull-right bg-gray">{{Contact:count()}}</small>
              </a>
            </li>
            @end
-->

            @if Auth:can('menu.user')
            <li>
              <a href="/admin/user">
                <i class="fa fa-user-plus"></i> <span>Users</span>
                <small class="label pull-right bg-gray">{{User:count()}}</small>
              </a>
            </li>
            @end

            @if Auth:can('access.api-log')
            <li>
              <a href="/admin/device">
                <i class="fa fa-mobile"></i> <span>Devices</span>
              </a>
            </li>
            @end

            @if Auth:can('menu.push')
            <li>
              <a href="/admin/push-notification">
                <i class="fa fa-mobile"></i> <span>Push Notifications</span>
              </a>
            </li>
            @end

            @if Auth:can('menu.user')
            <li>
              @if Auth:can('manage.usergroups')
                <li><a href="/admin/usergroup"><i class="fa fa-circle-o"></i> User groups</a></li>
              @end
            </li>
            @end

            @if Auth:can('access.api-log')
            <li>
              <a href="/admin/api-log">
                <i class="fa fa-database"></i> <span>API logs</span>
              </a>
            </li>
            @end

            @if Auth:can('admin')
            <li>
              <a href="/admin/tutorial">
                <i class="fa fa-info-circle"></i> <span>Tutorials</span>
              </a>
            </li>
            @end

            @if Auth:can('menu.company')
            <li>
              <a href="/admin/company">
                <i class="fa fa-user-plus"></i> <span>Companies</span>
                <small class="label pull-right bg-gray">{{Company:count()}}</small>
              </a>
            </li>
            @end
            
<!--
            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
-->
          </ul>