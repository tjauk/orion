#!/bin/bash

# set all folders
declare -a folders=(events_austria events_belgium events_czech events_denmark events_finland events_germany events_greece events_ireland events_italy events_luxembourg events_malta events_netherlands events_norway events_poland events_portugal events_spain events_switzerland events_southafrica events_sweden)

# loop through folders and execute
for folder in "${folders[@]}"
do
	find /var/www/orion/$folder/cache/globals/*.same.* -type f -delete
	find /var/www/orion/$folder/cache/globals/city.* -type f -delete
done

echo "Cache cleared on EU41 server"
