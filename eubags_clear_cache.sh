#!/bin/bash

# set all folders
declare -a folders=( events_spain events_italy events_germany )

# loop through folders and execute
for folder in "${folders[@]}"
do
	find /var/www/orion/$folder/cache/globals/*.same.* -type f -delete
	find /var/www/orion/$folder/cache/globals/city.* -type f -delete
	find /var/www/orion/$folder/cache/globals/home.* -type f -delete
done

echo "Cache cleared on EUBAGS server"