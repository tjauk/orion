#!/bin/bash

# set all folders
declare -a folders=( events_spain events_italy events_germany )

# loop through folders and execute
for folder in "${folders[@]}"
do
	rm -r /var/www/orion/$folder/images/covers/
done

echo "Removed covers on EUBAGS server"