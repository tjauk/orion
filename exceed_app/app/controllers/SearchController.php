<?php

class SearchController extends Controller
{

    public function boot()
    {
    	Doc::view('layouts/default');
        //Breadcrumbs::view('breadcrumbs/index');
        //Breadcrumbs::addHome('Home');
        //Breadcrumbs::addTitle(t('Search'));
        //Breadcrumbs::add($crumb['href'],$crumb['title']);
    }

    /**
     * TOP SEARCH FORM
     */
    function top()
    {
        $request = Request::all();
        $q = $request->get('q');
        return view('search/top')->put($q,'q');
    }

    /**
     * INDEX
     */
    function index()
    {
        //DB::enableQueryLog();

        Doc::meta('robots','noindex, nofollow');

        $items = new Video();

        $filter = SearchFilter::stateless();
        $filter->perpage = 24;
        $filter->page = 1;
        $filter->sortby = 'title';
        $filter->sortdir = 'ASC';

        $searchform = $this->searchForm($filter);

        /*
        // master of disaster
        $items = $items ->masterFilter($filter)
                        ->masterSort($filter);
		*/
        // search keywords
        if( !empty($filter->q) ){
        	$items = $items->search($filter->q,'title,tags');
        }
        

        $items = $filter->paginate( $items );
        $items = $items->get();


        if( $filter->pagination->total_results==1 ){
            return redirect($items[0]->href);
        }

        //SearchLog::add($filter->q);

        return View::make('search/results')
                    ->put($searchform,'searchform')
                    ->put($filter,'filter')
                    ->put($items,'items');
    }


    /**
     * JSON
     */
    function ajax()
    {
        Doc::json();

        $items = Product::active();

        $filter = SearchFilter::stateless();
        $filter->perpage = 24;
        $filter->page = 1;
        $filter->sortby = 'price';
        $filter->sortdir = 'ASC';

        // master of disaster
        $items = $items ->masterFilter($filter)
                        ->masterSort($filter);

        $items = $filter->paginate( $items );
        $items = $items->get();

        //SearchLog::add($filter->q);

        return $items;
    }

    function searchForm($filter=null)
    {
        $form = Forms::make($filter);
        $form->open('search')->method('GET');
        $form->label('Search')->input('q');
        $form->submit('Search');
        $form->close();

        return $form;
    }

    function postTop(){
        
        $show_max = 5;

        Doc::json();
        
        $query = Request::post('q');

        // events
        $events = Event::active();
        $events = $events->search($query,'name,domain,slug,place_name,address,city')
                        ->take($show_max*2)
                        ->orderBy('quality',DESC)
                        ->get();

        // places
        //$places = Place::search($query,'name,address,city')->take(5)->get();
        $places = [];

        // cities
        $cities = new City();
        $cities = $cities->search($query,'name,fb_name')
                    ->where('active_events','>',0)
                    ->take($show_max)
                    ->orderBy('name',ASC)
                    ->get();
        
        foreach($events as $event){
            $cities->push($event->cities);
            //$categories->push($event->category);
        }
        //$places = $places->unique('id');
        $cities = $cities->unique('id');

        foreach($events as $index=>$event){
            if( $index>=$show_max ){
                unset($events[$index]);
            }
        }

        foreach($cities as $index=>$city){
            if( $index>=$show_max ){
                unset($cities[$index]);
            }
        }

        return View::make('search/suggestions')
                    ->put($events,'events')
                    ->put($places,'places')
                    ->put($cities,'cities');
    }


}
