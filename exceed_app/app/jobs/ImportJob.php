<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion import
/*
function replace_unicode_escape_sequence($match) {
    return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
}
*/
class ImportJob extends Command
{
    protected function configure()
    {
        $this
            ->setName('import')
            ->setDescription('Import vids csv')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '2048M');
        

        $commit_after = 500;

        $usecache = true;

        $filepath = 'D:\Downloads\Datasets\xvideos.com-db.csv\xvideos.com-db.csv';

        $delimiter = ';';

        if( ($handle = fopen($filepath, 'r')) !== false){
            // get the first row, which contains the column-titles (if necessary)
            // $header = fgetcsv($handle);

            $nr = 0;
            // loop through the file line-by-line
            while(($data = fgetcsv($handle,0,$delimiter)) !== false)
            {
                Video::create([
                    'title'     => $data[1],
                    'category'  => $data[7],
                    'tags'      => $data[5],
                    'thumb_url' => $data[3],
                    'url'       => $data[0],
                    'duration'  => $data[2],
                    'embed'     => $data[4],
                    'xid'       => $data[6],
                    'active'    => 1,
                ]);
                ++$nr;
                $output->writeln("{$nr}. {$data[1]}");

                unset($data);
            }
            fclose($handle);
        }

        /*
        $transaction_block = $commit_after;
        DB::beginTransaction();

        DB::commit();
        */
        
        $output->writeln("----------------------------------");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');

    }

}