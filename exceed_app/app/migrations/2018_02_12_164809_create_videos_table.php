<?php

class CreateVideosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('title')->default('');
            $table->string('category')->default('');
            $table->text('tags');
            $table->text('thumb_url');
            $table->text('url');
            $table->string('duration')->default('');
            $table->text('embed');
            $table->bigInteger('xid')->unsigned();
            $table->tinyInteger('active')->unsigned()->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos');
    }

}
