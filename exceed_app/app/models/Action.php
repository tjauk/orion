<?php
class Action extends Model
{
    protected $table = 'actions';

    protected $fillable = [
        'name',
        'args',
    ];



    /*
     * Options
     */
    static function options()
    {
        return [
            ''                      => '---',
            'none'                  => 'none',
            'home'                  => 'home',
            'deals'                 => 'deals',
            'dealsMore'             => 'deals',
            'search'                => 'search',
            'dealsMore'             => 'dealsMore',
            'explore'               => 'explore',
            'exploreFeatured'       => 'exploreFeatured',
            'exploreMoreFeatured'   => 'exploreFeatured',
            'exploreMore'           => 'exploreMore',
        ];
    }

    static function parse($str){
        /*
explore
bla:Test
        */
        $lines = array_filter(explode("\n",str_replace("\r","",$str)));

        $action = new Action();
        $action->name = $lines[0];
        
        $args = [];
        unset($lines[0]);
        foreach($lines as $line){
            $p = array_filter(explode(":", $line));
            $key = array_shift($p);
            $args[$key] = implode(":",$p);
        }
        $action->args = $args;
        return $action;
    }

    /*
     * Scopes
     */


    /*
     * Getters
     */


    /*
     * Setters
     */


}
