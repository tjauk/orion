<?php
class Beacon extends Model
{
    protected $table = 'beacons';

    protected $fillable = [
		'client_id',
		'code',
		'major',
		'minor',
		'status',
		'note',
		'immediate',
		'near',
		'far',
		'lat',
		'lon',
		'address',
		'city',
		'region',
		'country_id'
    ];

    /*
     * Relations
     */


    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */


}
