<?php
class Video extends Model
{
    protected $table = 'videos';

    protected $fillable = [
        'title',
        'category',
        'tags',
        'thumb_url',
        'url',
        'duration',
        'embed',
        'xid',
        'active',
    ];

    /*
     * Relations
     */


    /*
     * Methods
     */


    /*
     * Options
     */


    /*
     * Scopes
     */


    /*
     * Getters
     */
    public function getHrefAttribute()
    {
        return $this->url;
    }


    /*
     * Setters
     */


}
