<!DOCTYPE html>
<html>
<head>

	<title>{{doc.title}}</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	{{doc.meta}}
	
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="/theme/css/bootstrap.min.css">
    <link rel="stylesheet" href="/theme/font-awesome-4.7.0/css/font-awesome.min.css">
  	
    @less "styles.less"

    <link rel="stylesheet" href="/theme/app.css">
	{{doc.css}}


    @jquery
	{{doc.js}}


</head>
<body>

	<div id="app">
	  
	<!-- header -->
	<!--@include "header"-->

	<!-- main -->
	<div id="content" class="container">
		{{CONTENT}}
	<div>

	<!-- footer -->
	<!--@include "footer"-->

	</div>

	<!-- end scripts -->
  	<script type="text/javascript" src="/theme/app.js"></script>
	{{doc.jsend}}
</body>
</html> 