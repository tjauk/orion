<!--BEGIN:search/results-->

@if filter.pagination..total_results>0

	<h2>Found {{filter.pagination..total_results}} results</h2>

<div class="row">
@loop items as item
	<div class="col-md-3">
	    <a href="{{item.href}}">
		    <img src="{{item.thumb_url}}" /><br>
			{{item.title}}
		</a>
	</div>
@end
</div>


	<div class="clearfix"></div>

	{{filter.pagination}}

@else
	<div class="clearfix">
		<h3><i class="fa fa-warning text-yellow"></i> {{"No results found"|t}}</h3>
	</div>
@end
<!--END:search/results-->