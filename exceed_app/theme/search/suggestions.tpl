<!--BEGIN:search/suggestions-->
<?php
function highlight($text){
	$word = Request::post('q');
	$word = str_replace("/"," ",$word);
	$words = explode(" ",$word);
	$words = array_filter($words);
	usort($words,function($a,$b){
		return strlen($b)-strlen($a);
	});
	foreach($words  as $word){
		$reg = preg_quote($word);
		$regEx = '\'(?!((<.*?)|(<span.*?)))('. $reg . ')(?!(([^<>]*?)>)|([^>]*?</span>))\'si';
		$text = preg_replace($regEx, '<span class="highlight">\4</span>', $text);
	}
	return $text;
}
?>
<div class="">
	<div class="col-sm-4" style="background-color:#fcfcfc">
		<b>{{"Cities"|t}}</b>
		<ul>
		@loop cities as city
			<li><a class="text-dark" href="{{city.href}}">{{city.name|highlight}}</a></li>
		@end
		</ul>
	</div>
	<div class="col-sm-8" style="background-color:#fafafa">
		<b>{{"Events"|t}}</b>
		<ul>
		@loop events as event
			<li>
				<a class="text-dark" href="{{event.href}}">{{event.name|highlight}}</a>
			</li>
		@end
		</ul>
	</div>
</div>
<div class="clearfix"></div>
<!--END:search/suggestions-->