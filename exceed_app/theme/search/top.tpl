<!--BEGIN:search/top-->
<form id="top-search" action="/search" method="GET">
	<div class="input-group">
		<input class="form-control" autocomplete="off" type="text" name="q" value="<?php echo htmlentities(Request::get('q'));?>" placeholder="{{"Find your city or event"|t}}" />
		<div class="input-group-btn">
			<button class="btn btn-default"><span class="fa fa-search fa-fw"></span></button>
		</div>
	</div>
	<div id="search-top-results" class="col-xs-12" style="display:none;"></div>
</form>


<script>
function closeAll(){
    $('#search-top-results').slideUp(150);
    $('#menu-categories').removeClass('open');
    $('#product-categories').removeClass('open');
}
$(document).ready(function(){
	keypress_timer = 0;
	$('input[name="q"]').on('keydown focus',function(event){
		clearTimeout(keypress_timer);
		keypress_timer=setTimeout(function(){
			var q=$('input[name="q"]').val();
			if( q.length<2 ){
				$('#search-top-results').slideUp(150);
				closeAll();
			}else{
				$.ajax({
					url: '/search/top',
					type: 'POST',
					dataType: 'html',
					data: {q: q},
				})
				.done(function(html) {
					$('#search-top-results').html(html).slideDown(300);
				})
				.fail(function() {
				})
				.always(function() {
				});
			}
		},300);
	});

	$('body').on('click',function(event){
        closeAll();
    });
});

</script>

<!--END:search/top-->