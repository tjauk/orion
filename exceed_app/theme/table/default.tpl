@@ham
table .uk-table .uk-table-striped .uk-table-condensed .uk-table-hover 
	@not table.caption empty
		caption : {{table.caption}}
	@end
	@not table.headings empty
		thead style="color:#fff;background-color:#444;"
			tr
				@loop table.headings as title
					th : {{title}}
				@end
	@end
	@not table.rows empty
		tbody
			@loop table.rows as row
				tr
					@loop row as cell
						td : {{cell}}
					@end
			@end
	@end
	@not table.footer empty
		tfoot
			tr
				@loop table.footer as title
					td : {{title}}
				@end
	@end
