<?php
class AdminBillController extends AdminController
{
    public $model = 'Bill';
    public $baseurl = '/admin/bill';


    /**
     * Index page
     *
     * @return View
     */
    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Bill',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search')->search('keywords')->placeholder('Search');
        $form->label('Per page')->select('perpage')->values([10,20,50,100]);

        $table = Table::make()
                ->headings(['#','Name','Year','Number','Operator','Bill Time','Done Time','Paid At','Paid Amount','Account Bank','Account Number','Billing Method','Billing Call Number','Billing Valute','Client','Sub Amount','Tax Procent','Tax Amount','Total','Note','Note2','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->name.'</a>';
            $data[] = $item->year;
            $data[] = $item->number;
            $data[] = $item->operator;
            $data[] = $item->bill_time;
            $data[] = $item->done_time;
            $data[] = $item->paid_at;
            $data[] = $item->paid_amount;
            $data[] = $item->account_bank;
            $data[] = $item->account_number;
            $data[] = $item->billing_method;
            $data[] = $item->billing_call_number;
            $data[] = $item->billing_valute;
            $data[] = $item->client->title;
            $data[] = $item->oib;
            $data[] = $item->sub_amount;
            $data[] = $item->tax_procent;
            $data[] = $item->tax_amount;
            $data[] = $item->total;
            $data[] = $item->status;
            $data[] = $item->note;
            $data[] = $item->note2;
            $actions = UI::btnEdit($item->id,'bill');
            $actions.= UI::btnDelete($item->id,'bill');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    /**
     * Edit page
     *
     * @return View
     */
    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        $post = Request::post();
        if( $post ){
            $item->fill($post);
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('bill');
        $form->label('Name')->input('name');

        $form->label('Year')->input('year');

        $form->label('Number')->input('number');

        $form->label('Operator')->input('operator');

        $form->label('Bill Time')->datePicker('bill_time');

        $form->label('Done Time')->datePicker('done_time');

        $form->label('Paid At')->datePicker('paid_at');

        $form->label('Paid Amount')->input('paid_amount');

        $form->label('Account Bank')->input('account_bank');

        $form->label('Account Number')->input('account_number');

        $form->label('Billing Method')->input('billing_method');

        $form->label('Billing Call Number')->input('billing_call_number');

        $form->label('Billing Valute')->input('billing_valute');

        $form->label('Client Id')->input('client_id');

        $form->label('Client Name')->input('client_name');

        $form->label('Address')->input('address');

        $form->label('Address2')->input('address2');

        $form->label('Zip')->input('zip');

        $form->label('Place')->input('place');

        $form->label('Country Id')->input('country_id');

        $form->label('Country')->input('country');

        $form->label('Oib')->input('oib');

        $form->label('Sub Amount')->input('sub_amount');

        $form->label('Tax Procent')->input('tax_procent');

        $form->label('Tax Amount')->input('tax_amount');

        $form->label('Total')->input('total');

        $form->label('Note')->text('note');

        $form->label('Note2')->text('note2');

        $form->label('Header')->text('header');

        $form->label('Footer')->text('footer');


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }


    /**
     * Delete
     *
     * @return Response
     */
    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/bill')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
