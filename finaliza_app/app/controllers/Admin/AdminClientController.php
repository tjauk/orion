<?php
class AdminClientController extends AdminController
{
    public $model = 'Client';
    public $baseurl = '/admin/client';

    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Client',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'first_name,last_name,company,company_long,oib,address,zip,city,email,phone,web,billing_email' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Per page');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');

        $table = Table::make()
                ->headings([
                    '#',
                    'First Name',
                    'Last Name',
                    'Company',
                    'Company Long',
                    'Oib',
                    'Mb',
                    'Ziro',
                    'Address',
                    'Address 2',
                    'Zip',
                    'City',
                    'Country Id',
                    'Email',
                    'Mobile',
                    'Phone',
                    'Fax',
                    'Web',
                    'Billing Email',
                    'Notes',
                    'Actions'
                ]);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = $item->first_name;
            $data[] = $item->last_name;
            $data[] = $item->company;
            $data[] = $item->company_long;
            $data[] = $item->oib;
            $data[] = $item->mb;
            $data[] = $item->ziro;
            $data[] = $item->address;
            $data[] = $item->address_2;
            $data[] = $item->zip;
            $data[] = $item->city;
            $data[] = $item->country->name;
            $data[] = $item->email;
            $data[] = $item->mobile;
            $data[] = $item->phone;
            $data[] = $item->fax;
            $data[] = $item->web;
            $data[] = $item->billing_email;
            $data[] = $item->notes;
            $actions = UI::btnEdit($item->id,'client');
            $actions.= UI::btnDelete($item->id,'client');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('client');
        
        $form->label('First Name')->input('first_name');
        $form->label('Last Name')->input('last_name');
        $form->label('Company')->input('company');
        $form->label('Company Long')->input('company_long');
        $form->label('Oib')->input('oib');
        $form->label('Mb')->input('mb');
        $form->label('Ziro')->input('ziro');
        $form->label('Address')->input('address');
        $form->label('Address 2')->input('address_2');
        $form->label('Zip')->input('zip');
        $form->label('City')->input('city');
        $form->label('Country Id')->select('country_id')->options(Country::options());
        $form->label('Email')->email('email');
        $form->label('Mobile')->input('mobile');
        $form->label('Phone')->input('phone');
        $form->label('Fax')->input('fax');
        $form->label('Web')->input('web');
        $form->label('Billing Email')->email('billing_email');
        $form->label('Notes')->text('notes');

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/client')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
