<?php
class AdminCurrencyController extends AdminController
{
    public $model = 'Currency';
    public $baseurl = '/admin/currency';


    /**
     * Index page
     *
     * @return View
     */
    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Currency',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name,code,mark,symbol,country' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search')->search('keywords')->placeholder('Search');
        $form->label('Per page')->select('perpage')->values([10,20,50,100]);

        $table = Table::make()
                ->headings(['#','Name','Mark','Symbol','Code','Units','Buying Rate','Rate','Selling Rate','Country','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->name.'</a>';
            $data[] = $item->mark;
            $data[] = $item->symbol;
            $data[] = $item->code;
            $data[] = $item->units;
            $data[] = $item->buying_rate;
            $data[] = $item->rate;
            $data[] = $item->selling_rate;
            $data[] = $item->country;
            $actions = UI::btnEdit($item->id,'currency');
            $actions.= UI::btnDelete($item->id,'currency');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    /**
     * Edit page
     *
     * @return View
     */
    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        $post = Request::post();
        if( $post ){
            $item->fill($post);
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('currency');
        $form->label('Name')->input('name');

        $form->label('Mark')->input('mark');

        $form->label('Symbol')->input('symbol');

        $form->label('Code')->input('code');

        $form->label('Units')->input('units');

        $form->label('Buying Rate')->input('buying_rate');

        $form->label('Rate')->input('rate');

        $form->label('Selling Rate')->input('selling_rate');

        $form->label('Country')->input('country');


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }


    /**
     * Delete
     *
     * @return Response
     */
    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/currency')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
