<?php
class AdminHostingController extends AdminController
{
    public $model = 'Hosting';
    public $baseurl = '/admin/hosting';

    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = Hosting::select(DB::raw('
            hostings.*,
            contacts.first_name as clients_first_name,
            contacts.last_name as clients_last_name,
            contacts.company as clients_company,
            servers.name as server_name,
            servers.provider as server_provider,
            ABS(DATEDIFF(NOW(), hostings.expires_at)) as expires_in_days
        '))
        ->leftJoin('contacts','contacts.id','=','hostings.client_id')
        ->leftJoin('servers','servers.id','=','hostings.server_id');

        $buttons = Forms::make();
        $buttons->btnAdd('Add Hosting',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 100;
        $filter->page = 1;
        $filter->sortby = 'expires_at';
        $filter->sortdir = ASC;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'hostings.name,account,servers.name,hostings.notes' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Per page');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');

        $table = Table::make()
                ->headings(['Name','Opened','Expires','in days','Client','Status','Server','Account','Price','Cash','Notes','Active','Actions']);

        $items = $items->orderBy($filter->sortby,$filter->sortdir);
        $items = $filter->paginate( $items )->get();

        $total_price = 0.0;
        $total_price_cash = 0.0;

        foreach($items as $item){
            $data = [];
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->name.'</a>';
            $data[] = UI::date($item->opened_at);
            $data[] = UI::date($item->expires_at);

            if( $item->expires_in_days>180 ){
                $class = 'label-success';
            } else if( $item->expires_in_days>60 ){
                $class = 'label-info';
            } else if( $item->expires_in_days>30 ){
                $class = 'label-warning';
            } else {
                $class = 'label-danger';
            }

            $data[] = '<div class="label '.$class.'">'.$item->expires_in_days.'</div>';
            
            $data[] = $item->client->title;
            $data[] = $item->status;
            $data[] = $item->server->name;
            $data[] = $item->account;
            $data[] = Num::price($item->price);
            $data[] = Num::price($item->price_cash);
            $data[] = $item->notes;
            $data[] = UI::checked($item->active)->toggle('Hosting',$item->id,'active');
            $actions = UI::btnEdit($item->id,'hosting');
            $actions.= UI::btnDelete($item->id,'hosting');
            $data[] = $actions;
            $table->addRow($data);

            $total_price += ($item->price > 0) ? $item->price : 0;
            $total_price_cash += ($item->price_cash > 0) ? $item->price_cash : 0;
        }

        //

        $data = ['&nbsp;','&nbsp;','&nbsp;','&nbsp;','&nbsp;','&nbsp;'];
        $data[] = '&nbsp;'; // Num::price(196.25 * 12);
        $data[] = b(Num::price($total_price));
        $data[] = b(Num::price($total_price_cash));
        $data[] = '&nbsp;';
        $data[] = '-'.Num::price(196.25 * 12).' = '.Num::price( $total_price + $total_price_cash - (196.25 * 12) );
        $data[] = '&nbsp;';
        $table->addRow($data);

        //

        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('hosting');

        
        
        
        $form->div('.col-md-6');
            $form->panel('Account info');

                $form->label('Name')->input('name');
                $form->label('Account')->input('account');
                $form->label('Password')->input('passw');
                $form->label('Server')->select2('server_id')->options(Server::options());
                $form->label('Status')->input('status');

                $form->label('Notes')->text('notes');

            $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-6');
            $form->panel('Client info');

                $form->label('Client')->select2('client_id')->options(Contact::options());

                $form->label('Opened At')->datePicker('opened_at');
                $form->label('Expires At')->datePicker('expires_at');
            
                $form->label('Price')->input('price');
                $form->label('Price Cash')->input('price_cash');
                
            $form->closePanel();
        $form->closeDiv();

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/hosting')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
