<?php
class AdminServerController extends AdminController
{
    public $model = 'Server';
    public $baseurl = '/admin/server';


    /**
     * Index page
     *
     * @return View
     */
    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Server',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name,ip,provider,price,period,domains,roles,notes' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search')->search('keywords')->placeholder('Search');
        $form->label('Per page')->select('perpage')->values([10,20,50,100]);

        $table = Table::make()
                ->headings(['#','Name','Ip','Provider','Location','Price','Currency','Period','Domains','Roles','Notes','Active','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->name.'</a>';
            $data[] = $item->ip;
            $data[] = $item->provider;
            $data[] = $item->location;
            $data[] = $item->price;
            $data[] = $item->currency->name;
            $data[] = $item->period;
            $data[] = $item->domains;
            $data[] = $item->roles;
            $data[] = $item->notes;
            $data[] = UI::checked($item->active)->toggle('Server',$item->id,'active');
            $actions = UI::btnEdit($item->id,'server');
            $actions.= UI::btnDelete($item->id,'server');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    /**
     * Edit page
     *
     * @return View
     */
    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        $post = Request::post();
        if( $post ){
            $item->fill($post);
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('server');

        $form->label('Name')->input('name');
        $form->label('Ip')->input('ip');
        $form->label('Provider')->input('provider');
        $form->label('Location')->input('location');
        $form->label('Price')->input('price');
        $form->label('Currency')->select('currency_id')->options(Currency::options());
        $form->label('Period')->radioGroup('period')->values('monthly,quaterly,yearly');
        $form->label('Domains')->text('domains')->autogrow();
        $form->label('Roles')->text('roles')->autogrow();
        $form->label('Nameservers')->text('nameservers')->autogrow();
        $form->label('Notes')->text('notes')->autogrow();
        $form->label('Active')->radioGroup('active')->options(['0'=>'No','1'=>'Yes']);

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }


    /**
     * Delete
     *
     * @return Response
     */
    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/server')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
