<?php
use Symfony\Component\HttpFoundation\Response as Response;
use Illuminate\Database\Schema\Blueprint as Blueprint;

class TestController extends Controller
{

    function index()
    {
        $ctrlname = str_replace('Controller','',__CLASS__);
        echo h3( $ctrlname );
        $actions = get_class_methods( __CLASS__ );
        sort( $actions );
        foreach( $actions as $action ) {
            if( substr( $action, 0, 1 ) !== '_' and substr( $action, 0, 4 ) !== 'post' and substr( $action, 0, 6 ) !== 'widget' ) {
                    $path = strtolower( $ctrlname ).'/'.$action;
                    echo  '<a href="/'.$path.'">'.$path.'</a><br>';
            }
        }
    }

    function r1()
    {
        Doc::view('admin/layout/print');
        $items = [];
        return view('bill/r1')->put($items,'items');
    }

    /**
     * ONE
     */
    function one(){
        return $_REQUEST;
    }

    function two(){
        return $_REQUEST;
    }

    function three(){
        return $_REQUEST;
    }

    function view($id=0){
        return $id;
    }

    function byTitle($id=0){
        return Article::findByTitle('test');
    }

    function all(){
        return Article::all();
    }

    function getShow(){
        return __METHOD__;
    }

    function server(){
        return $_SERVER;
    }

    function env($name){
        return getenv($name);
    }

    function session(){
        return $_SESSION;
    }

    function json(){
    	return ['key'=>"val"];
    }

    function response(){
        $response = new Response();
        $response->setContent('My response');
        $response->setStatusCode(Response::HTTP_NOT_FOUND);
        return $response;
    }
    
    function schema(){
        if( !Schema::hasTable('users') ){
            Schema::create('users', function(Blueprint $table)
            {
                $table->increments('id');
                $table->timestamps();
                $table->string('username')->unique();
                $table->string('email')->unique();
                $table->string('password', 60);
                $table->string('first_name')->default('');
                $table->string('last_name')->default('');
                $table->rememberToken();
            });
        }
        if( !Schema::hasTable('password_resets') ){
            Schema::create('password_resets', function(Blueprint $table)
            {
                $table->timestamps();
                $table->string('email')->index();
                $table->string('token')->index();
            });
        }
        return true;
    }
    function drop(){
        if(Schema::hasTable('users') ){
            Schema::drop('users');
        }
        if(Schema::hasTable('password_resets') ){
            Schema::drop('password_resets');
        }
        return true;
    }

    function query()
    {
        return DB::table("articles")->get();
    }

    function columns()
    {
        return Schema::getColumnListing("articles");
    }

    function select()
    {
        //return get_class_methods(Capsule::$instance->connection());
        return DB::select("SELECT * FROM articles LIMIT 1",[]);
    }

    function fill(){
        $id = DB::table('articles')->insertGetId(['title'=>"Test"]);
        return $id;
    }

    function fluid(){
        // create this using fluid model concept as in previous tests
        $book = new Book();
        $book->title = 'My title';
        $book->author = 'B.H.';
        $book->save();

    }

}
