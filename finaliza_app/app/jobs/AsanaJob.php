<?php

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion asana

class AsanaJob extends Command
{
    public $client = null;
    public $workspace_id = '68887204868291'; // Trinium Media
    public $project_id = '150702805785158'; // Todo

    protected function configure()
    {
        $this
            ->setName('asana')
            ->setDescription('Testing Asana');
    }

    public function addTodoTask($name)
    {
        /*
        $task = $this->client->tasks->createInWorkspace($this->workspace_id, array(
            'name' => $name,
            'projects' => array($this->project_id),
        ));
        */
        $task = $this->client->tasks->create(array(
            'name' => $name,
            'projects' => array($this->project_id),
        ));
        echo "{$name} : task {$task->id} created.\n";

        return $task;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->client = Asana\Client::accessToken(getenv('ASANA_PERSONAL_ACCESS_TOKEN'));

        // $me = $this->client->users->me();
        // echo "Hello " . $me->name;
        // print_r($me);

        // Workspace: [id] => 68887204868289, [name] => Trinium Media
        // $workspace_id = 68887204868289;
        // Project: [id] => 150702805785158, [name] => Todo
        // $projects = $this->client->projects->findByWorkspace($workspace_id, null, array('iterator_type' => false, 'page_size' => null))->data;
        // print_r($projects);
        // $this->addTodoTask('Testing');
        // exit();

        // *********************************************************************
        $now = Carbon::now();
        $dt = $now;
        // *********************************************************************
        $dayOfWeek = $now->dayOfWeek; // dayOfWeek returns a number between 0 (sunday) and 6 (saturday)
        $dayInMonth = $now->day;
        $curMonth = $now->month;
        $curYear = $now->year;
        $prevMonth = $now->subMonth()->month;
        $prevMonthYear = $now->subMonth()->year;
        $nextMonth = $now->addMonth()->month;
        $weekOfMonth = $now->weekOfMonth;
        $weekOfYear = $now->weekOfYear;

        // *********************************************************************
        // Svaki prvi u mjesecu, izvadi izvode
        // *********************************************************************
        /*
        if( $dayInMonth == 1 ){
            $task = $this->addTodoTask("RRR - skini izvode za {$prevMonth}mj.");
        }
        */

        // *********************************************************************
        // Svaki 1 u mjesecu, pošalji podsjetnik za evidentiranje stanja
        // *********************************************************************
        if ($dayInMonth == 1) {
            $task = $this->addTodoTask('FIRE - Evidentiraj Stanja i Net prvog u mjesecu');
        }

        // *********************************************************************
        // Svaki prvi u mjesecu, napravi račune
        // *********************************************************************
        if ($dayInMonth == 1) {
            // Tihi Studio
            $task = $this->addTodoTask("TS - Pošalji račune za {$curMonth}mj.");
            $task = $this->addTodoTask("TS - Napiši račune za {$curMonth}mj.");
            $task = $this->addTodoTask("TS - Plati najam mami za {$prevMonthYear}-{$prevMonth} mj.");

            // LumiVerse
            $task = $this->addTodoTask("LV - Pošalji račune za {$curMonth}mj.");
            $task = $this->addTodoTask("LV - Pošalji sve ulazne račune iz {$prevMonth}mj. u Aestus");

            $task = $this->addTodoTask("LV - Isplati najam ureda za {$curYear}-{$curMonth} mj.");
            $task = $this->addTodoTask("LV - Isplati najam vozila za {$curYear}-{$curMonth} mj.");

            // $task = $this->addTodoTask('LV - Napravi i pošalji fakturu Recognyte-u unutar radnog vremena');
        }


        // *********************************************************************
        // Svaki drugi u mjesecu, pošalji sve u knjigovodstvo
        // *********************************************************************
        /*
        if( $dayInMonth == 2 ){
            $task = $this->addTodoTask("RRR - pošalji sve za {$prevMonth}mj.");
        }
        */

        // *********************************************************************
        // LumiVerse / Aestus
        // *********************************************************************
        if ($dayInMonth == 4) {
            $task = $this->addTodoTask("LV - Plaća za {$prevMonth}mj.");
        }

        // *********************************************************************
        // Svaki 5 u mjesecu, plati odbojku
        // *********************************************************************
        if ($dayInMonth == 5) {
            $task = $this->addTodoTask("Home - Plati odbojku za Mašu");
        }

        // *********************************************************************
        // Svaki 6 u mjesecu, plati Tele2
        // *********************************************************************
        if ($dayInMonth == 6) {
            $task = $this->addTodoTask("Home - Skini Tele2 račun za {$prevMonth} mj. i plati");
        }

        // *********************************************************************
        // Svaki 7 u mjesecu, pošalji podsjetnik za platiti Kaparu/najam
        // *********************************************************************
        /*
        if ($dayInMonth == 7) {
            $task = $this->addTodoTask("Plati kaparu/najam za {$curMonth}mj.");
        }
        */

        // *********************************************************************
        // Svaki 9 u mjesecu, pošalji podsjetnik za platiti karticu
        // *********************************************************************
        /*
        if ($dayInMonth == 9) {
            $task = $this->addTodoTask("{$curMonth}mj. - 1000kn - Kartica");
            // $task = $this->addTodoTask("{$curMonth}mj. - 1400kn - Helena");
        }
        */

        // *********************************************************************
        // Upozori na automatski subscription na Drops app
        // *********************************************************************
        if ($dayInMonth == 15 and $curMonth == 7) {
            $task = $this->addTodoTask('Home - Za tjedan dana će ti automatski naplatiti godišnju pretplatu na Drops!');
        }

        // *********************************************************************
        // Svaki 15 u mjesecu, pošalji podsjetnik za investiranje
        // *********************************************************************
        if ($dayInMonth == 15) {
            $task = $this->addTodoTask('FIRE - Investiraj, štedi, skupljaj, smišljaj, djeluj!');
        }

        // *********************************************************************
        // Svaki 17 u mjesecu, pošalji podsjetnik, provjeri poštu
        // *********************************************************************
        if ($dayInMonth == 17) {
            $task = $this->addTodoTask('Home - Provjeri poštu');
        }

        // *********************************************************************
        // Svaki 18 u mjesecu, plati Totohost račun za reseller
        // *********************************************************************
        if ($dayInMonth == 18) {
            $task = $this->addTodoTask("TS - Plati Totohost reseller račun za {$curMonth}mj.");
        }

        // *********************************************************************
        // Provjeri poreznu iza 20 i 27 dana svaki mjesesc
        // *********************************************************************
        if ($dayInMonth == 20) {
            $task = $this->addTodoTask('TS - Provjeri e-Poreznu, plati razlike');
        }
        if ($dayInMonth == 27) {
            $task = $this->addTodoTask('TS - Provjeri e-Poreznu, još jedamput za svaki slučaj');
        }

        // *********************************************************************
        // Svaki 25. u mjesecu, napravi račun za Google
        // *********************************************************************
        /*
        if ($dayInMonth == 25) {
            $task = $this->addTodoTask("Napiši račun za Google Ads");
        }
        */

        // *********************************************************************
        // Svaki 29 u mjesecu, provjeri Fanthom wallet i claim rewards
        // *********************************************************************
        if ($dayInMonth == 29) {
            // $task = $this->addTodoTask('FIRE - Provjeri staked FTM stanja i restake ako je veće od 1 FTM');
        }

        // *********************************************************************
        // Provjeri hostinge, javi ako ističe za 30 dana
        // *********************************************************************
        $hostings = Hosting::expiresIn(30)->get();
        foreach ($hostings as $hosting) {
            $expire_date = date('d.m.Y.', strtotime($hosting->expires_at));
            $task = $this->addTodoTask("{$hosting->name} ističe {$expire_date}");
        }

        // *********************************************************************
        // Provjeri hostinge, javi ako ističe za 15 dana
        // *********************************************************************
        $hostings = Hosting::expiresIn(15)->get();
        foreach ($hostings as $hosting) {
            $expire_date = date('d.m.Y.', strtotime($hosting->expires_at));
            $task = $this->addTodoTask("Za 15 dana ističe {$hosting->name} ({$expire_date})");
        }

        // *********************************************************************
        // Provjeri hostinge, javi ako ističe za 5 dana
        // *********************************************************************
        $hostings = Hosting::expiresIn(5)->get();
        foreach ($hostings as $hosting) {
            $expire_date = date('d.m.Y.', strtotime($hosting->expires_at));
            $task = $this->addTodoTask("Za 5 dana ističe {$hosting->name} ({$expire_date})");
        }

        // *********************************************************************
        // Podsjetnik za uplatiti komorske doprinose kvartalno u 2019
        // *********************************************************************
        $ko = 'TS - Uplatiti komorske doprinose, 228kn';
        if ($curMonth == 2 and $dayInMonth == 23) {
            $task = $this->addTodoTask($ko);
        }
        if ($curMonth == 5 and $dayInMonth == 24) {
            $task = $this->addTodoTask($ko);
        }
        if ($curMonth == 8 and $dayInMonth == 25) {
            $task = $this->addTodoTask($ko);
        }
        if ($curMonth == 11 and $dayInMonth == 26) {
            $task = $this->addTodoTask($ko);
        }

        // *********************************************************************
        // Podsjetnik za stan - Markuševečka cesta 20C
        // *********************************************************************
        if ($dayOfWeek == 2 and $weekOfYear % 2 > 0) {
            $ko = 'Home - U utorak ide odvoz PAPIRA - plavi tjedan';
            $task = $this->addTodoTask($ko);
        }
        if ($dayOfWeek == 2 and $weekOfYear % 2 == 0) {
            $ko = 'Home - U utorak ide odvoz PLASTIKE I METALA - žuti tjedan';
            $task = $this->addTodoTask($ko);
        }

        if ($curYear == 2020 and $dayInMonth == 1) {
            if (in_array($curMonth, [5, 9])) {
                $ko = 'Home - Održavanje kuće - komunalni otpad';
                $task = $this->addTodoTask($ko);
            }
            if (in_array($curMonth, [5, 8, 11])) {
                $ko = 'Home - Održavanje kuće - stubište x2 mjesečno';
                $task = $this->addTodoTask($ko);
            }
        }

        // *********************************************************************
        // Smeće odvoze ponedjeljkom i četvrtkom, pošalji u nedjelju i srijedu
        // *********************************************************************
        /*
        if ($dayOfWeek == 0 or $dayOfWeek == 3) {
            $ko = 'Sutra ide smeće';
            $task = $this->addTodoTask($ko);
        }
        */

        // *********************************************************************
        // Podsjetnik za odlediti hladnjak svakih godinu dana
        // *********************************************************************
        if ($curMonth == 3 and $dayInMonth == 27) {
            $task = $this->addTodoTask('Home - Odlediti i očistiti hladnjak');
        }

        // *********************************************************************
        $output->writeln('----------------------------------');
        $output->writeln('Peak memory: ' . (memory_get_peak_usage() / 1024 / 1024));
        $output->writeln('DONE IN ' . round(microtime(true) - ORION_STARTED, 2) . ' seconds');
    }
}
