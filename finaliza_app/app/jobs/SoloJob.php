<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion solo

class SoloJob extends Command
{
    private $api_token = '4697ec3d8ab9ed8a176763750b493290e';

    protected function configure()
    {
        $this
            ->setName('solo')
            ->setDescription('Testing Solo servis')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $api_token = getenv('SOLO_API_TOKEN');

        // *********************************************************************
        $now = Carbon::now();
        $dt = $now;
        // *********************************************************************
        $dayInMonth = $now->day;
        $curMonth = $now->month;
        $prevMonth = $now->subMonth()->month;
        $nextMonth = $now->addMonth()->month;
        

        // *********************************************************************
        // TEST
        // *********************************************************************
        $api_base = 'https://api.solo.com.hr';
        $respose = Remote::get("{$api_base}/racun?token={$api_token}&id=4f57fed795816d5e1adb4fff820378aad3af7a7e44690b3178fa1f30757813e7");
        print_r($respose);
        exit("done");


        // *********************************************************************
        $output->writeln("----------------------------------");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');
    }

    public function get($url,$params)
    {

    }


    
}

/*

Za developere.
Statusi računa i ponuda

U nastavku je popis podržanih statusa i na koje se tipove dokumenata odnosi.
Kroz API šalješ njihov ID broj iz desne kolumne, a ne naziv. API će vratiti grešku ako unos ne bude numerički.
Otvori  1,2,3,4     1
Pošalji     računi i ponude     2
Opomeni     računi  3
Plaćeno     1,2,3,4     5



Primjer responsa za racun

{
  "status": 0,
  "racun": {
    "id": "4f57fed795816d5e1adb4fff820378aad3af7a7e44690b3178fa1f30757813e7",
    "broj_racuna": "5-1-1",
    "tip_usluge": "1",
    "prikazi_porez": "0",
    "tip_racuna": "R",
    "kupac_naziv": "FLOW AND FORM j.d.o.o.",
    "kupac_adresa": "Bukovačka cesta 208, 10000 Zagreb",
    "kupac_oib": "40515554988",
    "usluge": [
      {
        "broj": "1",
        "opis_usluge": "Usluga programiranja",
        "jed_mjera": "-",
        "kolicina": "1",
        "cijena": "10.500,00",
        "popust": "0",
        "porez_stopa": "0",
        "suma": "10.500,00"
      }
    ],
    "neto_suma": "10.500,00",
    "porezi": [
      {
        "stopa": "0",
        "osnovica": "10500",
        "porez": "0,00"
      }
    ],
    "bruto_suma": "10.500,00",
    "nacin_placanja": "1",
    "operater": "Tihomir Jauk",
    "racun_izdao": "Tihomir Jauk",
    "likvidator": "Tihomir Jauk",
    "datum_racuna": "4.4.2018. 10:48:00",
    "rok_placanja": "11.4.2018.",
    "datum_isporuke": "",
    "napomene": "Navedena cijena usluge ne sadržava PDV prema čl.90 Zakona o PDV-u (NN 73/13).",
    "ponavljanje": "0",
    "iban": "HR09 2340 0091 1604 9603 9",
    "jezik_racuna": "1",
    "valuta_racuna": "HRK",
    "tecaj": "1",
    "status": "5",
    "boja": "#a9bf15",
    "pdf": "https://solo.com.hr/download/4f57fed795816d5e1adb4fff820378aad3af7a7e44690b3178fa1f30757813e7"
  },
  "message": "Detalji računa _5-1-1_."
}

*/