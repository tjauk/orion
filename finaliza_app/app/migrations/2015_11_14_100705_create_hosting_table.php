<?php

class CreateHostingTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hostings', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->default('');

            $table->integer('client_id')->nullable();
            
            $table->dateTime('opened_at')->nullable();
            $table->dateTime('expires_at')->nullable();
            $table->tinyInteger('active')->default(1);
            
            $table->string('status');
            $table->integer('server_id')->nullable();
            
            $table->string('account');
            $table->string('passw');

            $table->float('price')->nullable();
            $table->float('price_cash')->nullable();

            $table->text('notes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hostings');
    }

}
