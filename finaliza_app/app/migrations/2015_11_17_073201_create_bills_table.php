<?php

class CreateBillsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->default('');
            
            $table->integer('year');
            $table->integer('number');
            $table->string('operator');
            
            $table->dateTime('bill_time');
            $table->dateTime('done_time');

            $table->dateTime('paid_at');
            $table->float('paid_amount');

            $table->string('account_bank');
            $table->string('account_number');

            $table->string('billing_method');
            $table->string('billing_call_number');
            $table->string('billing_valute');

            $table->integer('client_id')->nullable();
            $table->string('client_name')->default('');
            $table->string('address')->default('');
            $table->string('address2')->default('');
            $table->string('zip')->default('');
            $table->string('place')->default('');
            $table->integer('country_id')->nullable();
            $table->string('country_name')->default('');
            $table->string('oib')->default('');

            // items
            $table->text('items');
            
            $table->float('sub_amount')->default(0.0);
            $table->float('tax_procent')->default(0.0);
            $table->float('tax_amount')->default(0.0);
            $table->float('total')->default(0.0);

            //

            $table->string('status')->default('');

 
            $table->text('note');
            $table->text('note2');

            $table->text('header');
            $table->text('footer');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bills');
    }

}
