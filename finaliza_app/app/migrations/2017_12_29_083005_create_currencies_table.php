<?php

class CreateCurrenciesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->default('');
            $table->string('mark')->default('')->unique();
            $table->string('symbol')->default('');
            $table->string('code')->default('')->unique();
            $table->integer('units')->default(1);
            $table->double('buying_rate')->default(1.0);
            $table->double('rate')->default(1.0);
            $table->double('selling_rate')->default(1.0);
            $table->string('country')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('currencies');
    }

}
