<?php

class CreateServersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->default('');
            $table->string('ip')->default('');
            $table->string('provider')->default('');
            $table->string('location')->default('');
            $table->float('price')->nullable();
            $table->integer('currency_id')->nullable();
            $table->string('period')->default('');
            $table->text('domains');
            $table->text('nameservers');
            $table->text('roles');
            $table->text('notes');
            $table->tinyInteger('active')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('servers');
    }

}
