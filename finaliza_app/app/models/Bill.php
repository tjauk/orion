<?php
class Bill extends Model
{
    protected $table = 'bills';

    static $STATUSES = [
        'SHOPPING',
        'REVIEWING',
        'SENDING',
        'PENDING',
        'PENDING_PG',
        'PARTIAL',
        'PAID',
        'DECLINED',
        'SHIPPING',
        'COMPLETE',
        'CANCELED'
    ];

    static $PAYMENT_METHODS = [
        'virman'    => "Internet bankarstvom, općom uplatnicom ili virmanom",
        'kartica'   => "Karticom (jednokratno ili na rate)",
        'gotovina'   => "Gotovina"
    ];

    static $CREDIT_CARDS_TYPES = [
        'amex'      => "American Express",
        'diners'    => "Diners",
        'master'    => "MasterCard",
        'maestro'   => "Maestro",
        'visa'      => "Visa",
    ];

    protected $fillable = [
		'name',
		'year',
		'number',
		'operator',

		'bill_time',
		'done_time',

        'paid_at',
        'paid_amount',

		'account_bank',
		'account_number',

		'billing_method',
		'billing_call_number',
		'billing_valute',

        'client_id',
        'client_name',
		'address',
		'address2',
		'zip',
		'place',
        'country_id',
		'country_name',
		'oib',

        'items',

		'sub_amount',
		'tax_procent',
		'tax_amount',
		'total',

        'status',

		'note',
		'note2',

        'header',
        'footer',
    ];

    /*
     * Boot
     */

    public static function boot()
    {
        parent::boot();

        // on create
        static::creating(function($order){

            if( !Auth::guest() ){
                $order->user_id = Auth::user()->id;
            }
            if( empty($order->status) ){
                $order->status = 'SHOPPING';
            }

        });

        // on save
        static::saving(function($order){

            // recalculate to attributes
            $order->subtotal = $order->subtotal;
            $order->shipment_cost = $order->shipment_cost;
            $order->total = $order->total;

            if( $order->status == 'SHOPPING' ){
                $order->refreshAllPrices();
            }

        });

    }

    /*
     * Relations
     */
    public function client()
    {
        return $this->belongsTo('Contact','client_id');
    }


    /*
     * Options
     */


    /*
     * Scopes
     */
    public function scopePending($query)
    {
        $query->where('status', '=', 'PENDING');
    }
    public function scopeToday($query)
    {
        $query->whereRaw( 'DATE(created_at)=CURDATE()' );
    }
    public function scopeYesterday($query)
    {
        $query->whereRaw( 'DATE(created_at)=DATE_SUB(CURDATE(), INTERVAL 1 DAY)' );
    }
    public function scopeBetween($query,$from=null,$to=null)
    {
        if( $from==null ){
            $from = date('Y-m-d',time());
        }
        if( $to==null ){
            $to = date('Y-m-d',time());
        }

        $from = date('Y-m-d',strtotime($from));
        $to = date('Y-m-d',strtotime($to));

        $query->whereRaw( "( DATE(created_at)>=DATE('{$from}') AND DATE(created_at)<=DATE('{$to}') )" );
    }


    /*
     * Generate
     */
    public function assignNumber()
    {
        $year   = intval(date('Y'));
        $max    = DB::table('orders')->where('year','=',$year)->max('number');
        $number = intval($max)+1;

        $this->year = $year;
        $this->number = $number;
        $this->name = 'WEB'.$year.'-'.str_pad($number, 6, '0', STR_PAD_LEFT);

        //$this->save();
    }

    public function setStatus($status=null)
    {
        $this->status = $status;
    }


    /*
     * Getters
     */
    public function getItemsAttribute()
    {
        $items = json_decode($this->attributes['items'],true);
        if( empty($items) ){
            $items = array();
        }
        return $items;
    }


    /*
     * Setters
     */
    public function setItemsAttribute($items)
    {
        $this->attributes['items'] = json_encode($items);
    }


    /*
     * Checkers
     */
    public function isEmpty()
    {
        if( empty($this->items) ){
            return true;
        }
        return false;
    }

    /*
     * Validate current info
     */
    public function validate()
    {
        $valid = false;

        // validate
        $required = Valid::required();
        $oib = Valid::oib();
        $email = Valid::email();

        if( $oib->not_valid($this->oib) ){
            return false;
        }

        return true;
    }

    /*
     * Send this bill to mail
     */
    public function sendToMail($to=null)
    {
        if( is_null($to) ){
            $to = 'tihomir.jauk@gmail.com';
        }
        $mail = new Mail();
        // $mail->from(null); // use default
        $mail->to($to);
        $mail->subject ='Chipoteka.hr - Narudžba '.$this->name;
        $mail->load('mail/bill');
        $mail->put($this,'bill');

        return $mail->send() ? true : false;
    }


    /*
     * Generate PDF
     */
    public function toPDF()
    {
        return View::make('bill/pdf')->toPDF();
    }

}
