<?php
class Currency extends Model
{
    protected $table = 'currencies';

    protected $fillable = [
		'name',
		'mark',
		'symbol',
        'code',
        'units',
        'buying_rate',
		'rate',
        'selling_rate',
		'country'
    ];

    /*
     * Relations
     */


    /*
     * Relations
     */


    /*
     * Options
     */
    static function options()
    {
        $options = [];
        //$options = [''=>'---'];
        
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */


    /*
     * Getters
     */
    public function getNameAttribute()
    {
        if( empty($this->attributes['name']) ){
            return $this->attributes['mark'];
        }
        return $this->attributes['name'];
    }


    /*
     * Setters
     */


    static function hnb()
    {

    }
}
