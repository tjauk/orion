<?php
class Hosting extends Model
{
    protected $table = 'hostings';

    protected $fillable = [
		'name',
		
		'client_id',
        
        'opened_at',
        'expires_at',
        'active',

        'status',
		'server_id',
		
        'account',
		'passw',
		
        'price',
		'price_cash',
		
        'notes',
    ];

    /*
     * Relations
     */
    public function client()
    {
        return $this->belongsTo('Contact');
    }

    public function server()
    {
        return $this->belongsTo('Server');
    }



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }
    public function scopeExpired($query)
    {
        $query->where('expires_at', '<', \DB::raw('NOW()'));
    }
    public function scopeExpiresIn($query,$number_of_days)
    {
        $query->whereRaw("DATE(expires_at) = DATE(DATE_ADD(NOW(), INTERVAL {$number_of_days} DAY))");
    }


    /*
     * Getters
     */

    /*
     * Setters
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = trim($value);
    }

    public function setClientIdAttribute($value)
    {
        $this->attributes['client_id'] = empty($value) ? null : $value;
    }
    
    public function setServerIdAttribute($value)
    {
        $this->attributes['server_id'] = empty($value) ? null : $value;
    }


    public function setOpenedAtAttribute($value)
    {
        $this->attributes['opened_at'] = empty($value) ? null : date('Y-m-d',strtotime($value));
    }

    public function setExpiresAtAttribute($value)
    {
        $this->attributes['expires_at'] = empty($value) ? null : date('Y-m-d',strtotime($value));
    }

    public function setPriceAttribute($value)
    {
        $value = str_replace(",", ".", $value);
        $this->attributes['price'] = empty($value) ? 0 : $value;
    }

    public function setPriceCashAttribute($value)
    {
        $value = str_replace(",", ".", $value);
        $this->attributes['price_cash'] = empty($value) ? 0 : $value;
    }


}
