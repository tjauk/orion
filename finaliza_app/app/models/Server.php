<?php
class Server extends Model
{
    protected $table = 'servers';

    protected $fillable = [
		'name',
		'ip',
		'provider',
		'location',
		'price',
		'currency_id',
		'period',
		'domains',
        'nameservers',
		'roles',
		'notes',
        'active',
    ];

    /*
     * Relations
     */
    public function currency()
    {
        return $this->belongsTo('Currency');
    }



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */


    /*
     * Setters
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = trim($value);
    }

    public function setProviderAttribute($value)
    {
        $this->attributes['provider'] = trim($value);
    }


}
