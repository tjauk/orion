
<div class="row">
	<div class="col-md-6">
		<img src="logo.jpg">
	</div>
	<div class="col-md-6">
		Trinium media d.o.o.
		Pavla Radića 38
		10000 Zagreb, Hrvatska
		OIB: 83445447856
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<p>
			Trinium media d.o.o.
			Pavla Radića 38
			10000 Zagreb, Hrvatska
			OIB: 83445447856
		</p>
		<p>
			KOMET STANDARD d.o.o.
			Aleja Blaža Jurišića 9
			10000 Zagreb
			OIB: 05029171760
		</p>
	</div>
	<div class="col-md-6">
		<p>
			Račun: 1/1/1
			Datum računa: 03.01.2018.
			Vrijeme računa: 0:23:03
			Datum isporuke: 03.01.2018.
			Oznaka operatera: 01
			Način plaćanja: transakcijski račun
			IBAN račun:
			Poziv na broj: 2018-111
			Valuta plaćanja: 14 dana
		</p>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="items">
			<thead>
				<tr>
					<td>Rb.</td>
					<td>Naziv / opis</td>
					<td>Jed.cijena</td>
					<td>Količina</td>
					<td>Rabat</td>
					<td>Iznos</td>
				</tr>
			</thead>
			<tbody>
				@loop items
				<tr>
					<td>{{item.nr}}</td>
					<td>{{item.title}}</td>
					<td>{{item.baseprice}}</td>
					<td>{{item.quantity}}</td>
					<td>{{item.discount}}</td>
					<td>{{item.subtotal}}</td>
				</tr>
				@end
			</tbody>
		</table>
	</div>

	<div class="col-md-6">
		&nbsp;
	</div>
	<div class="col-md-6">
		<table class="totals">
			<tbody>
				<tr>
					<td>Iznos:</td>
					<td>300,00 Kn</td>
				</tr>
				<tr>
					<td>Osnovica za PDV:</td>
					<td>300,00 Kn</td>
				</tr>
				<tr>
					<td>Iznos PDV-a (25%):</td>
					<td>75,00 Kn</td>
				</tr>
				<tr>
					<td>Ukupno:</td>
					<td>375,00 Kn</td>
				</tr>
			</tbody>
		</table>
	</div>

</div>
<div class="row">
	ITEMS
</div>
<div class="row">
	<p>
		Račun izdano:<br>
		Trinium media d.o.o.<br>
	</p>
	<p>
		<b>Napomena: Oporezivanje prema naplaćenim naknadama po Odredbi čl.139 Zakona o PDV-u.</b>
	</p>
	<p>
		Račun je pravovaljan bez žiga i potipsa jer je ispisan na računalu.
	</p>
</div>
<div class="row">
	<p>
		TRINIUM MEDIA d.o.o. za usluge informacijskog društva | Registrirano kod: Trgovački sud u Zagrebu, Tt-12/3989-6, MBS: 080794850 | MB: 2869543 | Temeljni kapital: 20.000 kn uplaćen u cijelosti | IBAN žiro račun: HR8124840081106114335, kod banke Raiffeisenbank Austria d.d., Zagreb | Član uprave: Tihomir Jauk
	</p>
</div>

