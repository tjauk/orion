﻿# MySQL-Front 5.0  (Build 1.96)

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='NO_ENGINE_SUBSTITUTION' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;


# Host: 127.0.0.1    Database: rent_app
# ------------------------------------------------------
# Server version 5.6.17

#
# Table structure for table bookings
#

DROP TABLE IF EXISTS `bookings`;
CREATE TABLE `bookings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `booked_by` int(10) unsigned NOT NULL,
  `from` datetime DEFAULT NULL,
  `to` datetime DEFAULT NULL,
  `vehicle_id` int(10) unsigned NOT NULL,
  `km_start` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `km_end` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `price` double(8,2) NOT NULL,
  `client_id` int(10) unsigned DEFAULT NULL,
  `partner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `paid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `related_doc` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `partial_amount` double(8,2) NOT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT '0',
  `service_type` tinyint(4) NOT NULL DEFAULT '10',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table bookings
#
LOCK TABLES `bookings` WRITE;
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;

INSERT INTO `bookings` VALUES (1,'2015-12-17 20:16:03','2015-12-21 12:53:08',0,'2015-12-19 08:00:00','2015-12-21 08:00:00',3,'119194','',975,3,'1','','Ponuda\r\n2015482',20,'',0,0,10);
INSERT INTO `bookings` VALUES (2,'2015-12-17 20:20:08','2015-12-21 13:00:52',0,'2015-12-22 08:00:00','2015-12-23 08:00:00',4,'','',478,4,'1','','Ponuda 2015481',20,'',0,0,10);
INSERT INTO `bookings` VALUES (3,'2015-12-18 11:08:59','2015-12-21 12:40:56',0,'2015-12-09 12:00:00','2015-12-24 12:00:00',5,'83000','',4702,9,'1','','Ponuda\r\n2015470',1,'',0,0,10);
INSERT INTO `bookings` VALUES (4,'2015-12-18 11:19:46','2015-12-21 12:47:11',0,'2016-01-02 08:00:00','2016-01-11 08:00:00',5,'','',3240,10,'2','','bez računa',1,'',0,0,10);
INSERT INTO `bookings` VALUES (5,'2015-12-18 11:30:57','2015-12-21 12:52:33',0,'2015-12-11 08:00:00','2016-01-11 07:00:00',7,'','',9843,11,'1','','Ponuda\r\n2015456',20,'',0,0,10);
INSERT INTO `bookings` VALUES (6,'2015-12-18 11:31:47','2015-12-21 12:48:26',0,'2016-01-11 08:00:00','2016-02-11 07:00:00',7,'','',9843,11,'1','','Treba poslati\r\nponudu 3.1',1,'',0,0,10);
INSERT INTO `bookings` VALUES (7,'2015-12-18 11:32:45','2015-12-27 13:41:24',0,'2015-12-11 07:00:00','2016-01-11 07:00:00',10,'','',9843,11,'1','','Ponuda\r\n2015456',10,'',0,0,10);
INSERT INTO `bookings` VALUES (8,'2015-12-18 11:34:34','2015-12-21 12:48:42',0,'2016-01-11 08:00:00','2016-02-11 07:00:00',10,'','',9843,11,'1','','Treba poslati\r\nponudu 3.1',1,'',0,0,10);
INSERT INTO `bookings` VALUES (9,'2015-12-18 11:35:25','2015-12-21 12:58:50',0,'2015-12-26 08:00:00','2016-01-26 07:00:00',9,'','',9843,11,'1','','Ponuda\r\n2015457',20,'',0,0,10);
INSERT INTO `bookings` VALUES (10,'2015-12-18 11:37:17','2015-12-21 12:49:09',0,'2016-01-26 08:00:00','2016-02-26 07:00:00',9,'','',9843,11,'1','','Treba poslati\r\nponudu 3.1',1,'',0,0,10);
INSERT INTO `bookings` VALUES (11,'2015-12-18 11:39:47','2015-12-21 12:52:50',0,'2015-12-15 08:00:00','2015-12-19 08:00:00',8,'','',1347,12,'1','','Ponuda\r\n2015474',20,'',0,0,10);
INSERT INTO `bookings` VALUES (12,'2015-12-18 11:43:59','2015-12-21 13:02:29',0,'2016-01-09 08:00:00','2016-01-31 07:00:00',8,'','',7218,12,'1','','Ponuda\r\n2015474',20,'',0,0,10);
INSERT INTO `bookings` VALUES (14,'2015-12-18 11:50:09','2015-12-21 12:58:10',0,'2015-12-27 12:00:00','2016-01-02 11:00:00',8,'','',2580,13,'2','','Ponuda\r\n2015299\r\n05.12/500kn',10,'',0,0,10);
INSERT INTO `bookings` VALUES (15,'2015-12-18 11:54:26','2015-12-21 13:01:10',0,'2016-01-01 08:00:00','2016-01-11 08:00:00',11,'','',4875,15,'1','','Ponuda\r\n2015459',20,'',0,0,10);
INSERT INTO `bookings` VALUES (16,'2015-12-18 11:56:00','2015-12-21 12:47:48',0,'2016-01-15 07:00:00','2016-01-18 08:00:00',11,'','',1560,14,'2','','Bez računa',1,'',0,0,10);
INSERT INTO `bookings` VALUES (17,'2015-12-18 12:05:17','2015-12-21 12:50:18',0,'2016-02-01 08:00:00','2016-03-01 07:00:00',8,'','',9515,12,'1','','Ponuda poslana\r\n20154745',1,'',0,0,10);
INSERT INTO `bookings` VALUES (18,'2015-12-18 12:10:49','2015-12-28 11:35:53',0,'2015-12-01 09:00:00','2015-12-31 08:00:00',6,'','',9375,16,'1','','',10,'Račun 239-1-1',1234,1,10);
INSERT INTO `bookings` VALUES (19,'2015-12-19 13:28:23','2015-12-23 10:45:03',0,'2015-12-22 07:30:00','2015-12-25 08:00:00',3,'','',1295,0,'2','','Ponuda 2015302',99,'R1-123',0,0,10);
INSERT INTO `bookings` VALUES (20,'2015-12-28 17:01:10','2015-12-28 17:01:10',0,'2015-11-01 01:00:00','2015-12-01 01:00:00',0,'123123','',12345,4,'2','','TEST',1,'',0,2,10);
INSERT INTO `bookings` VALUES (21,'2015-12-28 17:03:15','2016-01-08 19:06:56',0,'2015-12-01 08:00:00','2015-12-03 12:00:00',4,'12000','12500',12345,4,'2','','TEST',1,'',0,2,20);
INSERT INTO `bookings` VALUES (22,'2015-12-29 13:08:59','2015-12-29 13:08:59',0,'2015-12-29 01:00:00','2015-12-29 01:00:00',5,'','',0,3,'1','','',1,'',0,0,30);
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table checklists
#

DROP TABLE IF EXISTS `checklists`;
CREATE TABLE `checklists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `check_date` date NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table checklists
#
LOCK TABLES `checklists` WRITE;
/*!40000 ALTER TABLE `checklists` DISABLE KEYS */;

INSERT INTO `checklists` VALUES (1,'2015-12-28 13:31:31','2015-12-28 16:54:27','2015-11-29',4,'{\"kilometraza_vozila\":\"\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"\",\"tekucina_za_hladenje\":\"\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"\",\"rad_ventilacije_brzine\":\"\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"\",\"stanje_ut_prostora\":\"\",\"pp_aparat_ispravnost\":\"\",\"rezervna_guma\":\"\",\"dizalica\":\"\",\"prva_pomoc\":\"\",\"zarulje\":\"\",\"prsluk\":\"\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (2,'2015-12-28 16:50:02','2015-12-29 12:33:11','2015-12-30',10,'{\"kilometraza_vozila\":\"123\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"-\",\"tekucina_za_stakla\":\"-\",\"tekucina_za_hladenje\":\"-\",\"tekucina_serva\":\"-\",\"tekucina_kocnica\":\"-\",\"gume_tlak_stanje\":\"-\",\"rad_ventilacije_brzine\":\"-\",\"rad_klima_uredaja\":\"-\",\"stanje_sjedala\":\"-\",\"stanje_ut_prostora\":\"-\",\"pp_aparat_ispravnost\":\"-\",\"rezervna_guma\":\"-\",\"dizalica\":\"-\",\"prva_pomoc\":\"-\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"napomena\":\"TEST\",\"btnSave\":\"\"}');
/*!40000 ALTER TABLE `checklists` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table contacts
#

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `company` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `company_long` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `oib` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mb` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address_2` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `zip` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `country_id` int(11) NOT NULL DEFAULT '52',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobile` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fax` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `web` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `groups` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1326 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table contacts
#
LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;

INSERT INTO `contacts` VALUES (1,'2015-12-17 19:57:41','2015-12-18 11:30:30','','','KOMET STANDARD d.o.o.','KOMET STANDARD d.o.o.','05029171760','','Aleja Blaža Jurišića 9','Ulica Dragutina Mandla 1','10040','Zagreb',52,'info@komet-prijevoz.hr','00385981756002','0038517888123','','http://flexirent.hr','','Pravna osoba,Partner,Najmodavac','','');
INSERT INTO `contacts` VALUES (2,'2015-12-17 19:59:37','2015-12-17 20:21:25','','','FLEXI RENT j.d.o.o.','FLEXI RENT j.d.o.o.','21113779642','04302028 ','Aleja Blaža Jurišića 9','Ulica Dragutina Mandla 1','10040','Zagreb',52,'info@flexirent.hr','00385981756002','0038517888123','','http://flexirent.hr','','Pravna osoba,Partner,Najmodavac','','');
INSERT INTO `contacts` VALUES (3,'2015-12-17 20:09:49','2015-12-21 13:09:30','','','6 SIGMA d.o.o.','6 SIGMA d.o.o.','89359809737','','BUNIĆEVA 7 ','','10000','Zagreb',52,'culjat13@gmail.com','00385 99 2720212','','','','Firma ima najam kombija, uzimali kod nas a mi možemo i kod njih.\r\nImaju Opel Vivare 2013 ili mlađe.','Pravna osoba,Dobavljač','','');
INSERT INTO `contacts` VALUES (4,'2015-12-17 20:13:00','2015-12-17 20:13:00','','','ACNielsen d.o.o.','ACNielsen d.o.o.','77114819074','01479776','Budmanijeva ulica  1','','10000','',52,'marina.leko@nielsen.com','','','','','','Pravna osoba,Partner','','');
INSERT INTO `contacts` VALUES (5,'2015-12-18 10:54:53','2015-12-20 19:57:50','','','AUTODUBRAVA d.o.o.','AUTODUBRAVA d.o.o.','82053451114','','Ulica Dragutina Mandal 1','','10040','Zagreb',52,'branka.glojnaric@baotic.hr','00385 1 2900191','00385 1 2900191','','','\r\n','Pravna osoba,Dobavljač,Servisni centar','','');
INSERT INTO `contacts` VALUES (6,'2015-12-18 10:58:34','2015-12-20 19:55:25','','','AUTO ART j.d.o.o.','AUTO ART j.d.o.o.','93130677665','','Bjelovarska 123','','10370 ','Dugo Selo',52,'autoart@net.amis.hr','00385 98 330176','','','','- nije u PDV-u, račune tražiti na FLEXI RENT','Pravna osoba,Dobavljač,Servisni centar','','');
INSERT INTO `contacts` VALUES (7,'2015-12-18 10:59:51','2015-12-20 19:58:27','Mario','Limar ','Mario Limar','Mario Limar','','','','','','',52,'','00385 99 6374958','','','','- za štete koje ne plaćamo preko firmi','Fizička osoba,Dobavljač,Servisni centar','','');
INSERT INTO `contacts` VALUES (8,'2015-12-18 11:03:42','2015-12-20 19:59:23','','','LONTRA d.o.o.','LONTRA d.o.o.','','','Bože Huzanića 72 A','','10370','Dugo Selo',52,'lontradv@gmail.com','00385 98 640094','','','','- vučna služba\r\n- limarija preko kaska\r\nDodatni brojevi\r\n+385/98/415-036 - Dražen','Pravna osoba,Dobavljač,Servisni centar','','');
INSERT INTO `contacts` VALUES (9,'2015-12-18 11:13:11','2015-12-20 19:56:19','','','AURO DOMUS d.o.o.','AURO DOMUS','46688992812','','Tometići 1/D','','51000','Kastav',52,'snjezana.traunkar@aurodomus.com','00385 91 4201044','','','','Firma se bavi otkupom zlata. \r\nUredan platiša.','Pravna osoba','','');
INSERT INTO `contacts` VALUES (10,'2015-12-18 11:14:49','2015-12-18 11:14:49','Igor','Vlahek','','','','','','','','',52,'','','','','','','Fizička osoba','','');
INSERT INTO `contacts` VALUES (11,'2015-12-18 11:26:56','2015-12-20 20:01:16','','','Zagreb Montaža d.o.o.','Zagreb Montaža d.o.o.','06588149401','','Roberta Frangeša Mihanovića 9','','10000','Zagreb',52,'dmartinovic@zagreb-montaza.hr','00385 98 485735','','','','- kasne sa plaćanjem\r\n- Damir je uredu osoba\r\n','Pravna osoba','','');
INSERT INTO `contacts` VALUES (12,'2015-12-18 11:28:52','2015-12-20 20:00:22','','','STROJOPROMET-ZAGREB d.o.o.','STROJOPROMET-ZAGREB d.o.o.','97994010225','','Zagrebačka 6','','10292','Šenkovec',52,'','00 385 99 2531 884','','','','- uredan platiša\r\n- bave se izgradnjom','Pravna osoba','','');
INSERT INTO `contacts` VALUES (15,'2015-12-18 11:52:44','2015-12-18 11:59:05','','','PLINOSERVIS KUZMAN d.o.o.','PLINOSERVIS KUZMAN d.o.o.','53047211759','','Mrzopoljska 2','','10000','',52,'info@plin-grijanje-kuzman.hr','00385 98 248 256','','','','-kontakt Joško iz Baotića','Pravna osoba','','');
INSERT INTO `contacts` VALUES (16,'2015-12-18 12:09:40','2015-12-18 12:09:40','','','HRVATSKI CRVENI KRIŽ','HRVATSKI CRVENI KRIŽ','72527253659','','Ulica Crvenog križa 14','','10000','Zagreb',52,'','','','','','Usmena komunikacija, nema mail.','Pravna osoba','','');
INSERT INTO `contacts` VALUES (18,'2015-12-21 09:47:44','2015-12-21 09:47:44','','','ALLIANZ OSIGURANJE','','23759810849','','Heinzelova 70','','10000','Zagreb',52,'','','','','','- Irena Štern za kasko\r\n- prijava po kasku se može raditi i u AUTODUBRAVI kod Tihomira','Pravna osoba,Dobavljač','','');
INSERT INTO `contacts` VALUES (25,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','N.S. DUBRAVA','','47521323377','','Cerska 1','','10040','Zagreb',52,'racunovodstvo@ns-dubrava.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (26,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Martina Poletto','','','','Kupac građanin','','','',52,'teapoletto@virgilio.it','','','','','','','','');
INSERT INTO `contacts` VALUES (27,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Veleposlanstvo NR Kine','','84022382972','','Mlinovi 132','','10000',' Zagreb',52,'veleposlanstvo.nr.kine1@zg.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (28,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','EUROCOP d.o.o.','','48400313356','','Zvečajska 5','','10000','Zagreb',52,'ivan.curic@eurocop.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (29,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','SPEGRA INŽINJERING d.o.o.','','33172675964','',' Ante Petravića 23','','21000','Split',52,'boris.pavic@spegra.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (30,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','Agencija za znanost i visoko obrazovanje','','83358955356','','Donje Svetice 38/5','','10000','Zagreb',52,'josip.supukovic@azvo.hr','','+385 1 6274 895','','','','','','');
INSERT INTO `contacts` VALUES (31,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MEDICUS COFFEA MM AK','','','','','','','',52,'mcoffea.mmak@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (32,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','AUTOTRANS d.o.o','','19819724166','','Žabica 1','','51000','Rijeka',52,'ivanka.gerbus@autotrans.hr','','+385-91-3090920','','','','','','');
INSERT INTO `contacts` VALUES (33,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','KAUFLAND HRVATSKA','','','','Vile Velebita','','10000','Zagreb',52,'martina.matic@kaufland.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (34,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','LOGISTIKA VIOLETA d.o.o.','','62874063131','','Obrež Zelinski 55','','10380','Sv.Ivan Zelina',52,'josipa.knezovic@violeta.com','','','','','','','','');
INSERT INTO `contacts` VALUES (35,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','HRVATSKI CURLING SAVEZ','','','','','','','',52,'drazen.cutic@curling.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (36,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','ZAGREBAČKO KAZALIŠTE MLADIH','','13254939546','','Teslina 7','','10000','Zagreb',52,'kresimir.juric@zekaem.hr','','+385-91-487-2577','','','','','','');
INSERT INTO `contacts` VALUES (39,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','test','','','','','','','',52,'igor.cukac@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (40,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','Bijuk HPC d.o.o','','61373622132','','Vrbovec Samoborski 1/A','','10430','Samobor',52,'jasminka.vrancic@bijuk-hpc.hr','','+385 1 3335 800','','','','','','');
INSERT INTO `contacts` VALUES (44,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','HŠSG','','59944829246','','Maksimirska 51','','10000','Zagreb',52,'hss.gluhih@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (45,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','E-PORT SOLUTIONS SISTEM d.o.o.','','20122571244','','Maksimirska cesta','','10000','Zagreb',52,'tamara@eport.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (48,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','BET-ARTIS d.o.o.','','73551968549','','Selčinska 27','','10360','Sesvete',52,'t.rukavina@bet-artis.hr','','','','','2015126 - kombi ide u Istru','','','');
INSERT INTO `contacts` VALUES (53,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','HRVATSKO DRUŠTVO KARIKATURISTA','','15978402700','','Savska 100','','10000','Zagreb',52,'trgovcevic@tzv-gredelj.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (56,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','IVAMONT d.o.o.','','86660343460','','VULINČEVA 24','','10310','Ivanić Grad',52,'tomislav.vukojevic@ivamont.hr','','+385-91-2882-533','','','','','','');
INSERT INTO `contacts` VALUES (57,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','KOJAN d.o.o.','','53448760343','','Kokoti 3','','20215','Gruda',52,'info@kojankoral.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (58,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','ELEKTRO SERVIS GB','','53029252370','','Ulica Hrvatskog proljeća 36','','10040','Zagreb',52,'servisgb@hotmail.com','','00385 98 415 401','','','','','','');
INSERT INTO `contacts` VALUES (59,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','Končar D&ST d.d','','49214559889','','J. Mokrovica 8; p.p. 100','','10090','Zagreb',52,'neno.preglej@koncar-dst.hr','','+385 99 264 8840','','','','','','');
INSERT INTO `contacts` VALUES (60,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Qatar Airways','','','','','','','',52,'mherceg@hr.qatarairways.com','','','','','','','','');
INSERT INTO `contacts` VALUES (61,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Goran Zecevic','','','','Kupac gradanin','','','',52,'goran.zecevic@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (65,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Miroslav Cipic','','','','Kupac građanin','','','',52,'miroslav.cipri@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (66,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','EMOS d.o.o.','','','','','','','',52,'emos@emoszg.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (67,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DK DUBRAVA','','','','','','','',52,'sanja.tisljaric@kazalistedubrava.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (68,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','QUADRATA m2 d.o.o.','','61672319010','','Jadranska avenija b.b.','','10000','Zagreb',52,'zlatko@quadrata-m2.hr','','+385-916443555','','','','','','');
INSERT INTO `contacts` VALUES (69,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Denis Lukačin','','','','Kupac građanin','','','',52,'','','','','','','','','');
INSERT INTO `contacts` VALUES (70,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','IRO','','88292527040','','Preradovićeva 33/I ','','10000','Zagreb',52,'daracic@iro.hr','','+385 1 4817195','','','','','','');
INSERT INTO `contacts` VALUES (71,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','SARGON d.o.o.','','17128515960','','KRALJA TOMISLAVA 98','','35410','Nova Kapela',52,'info@kombi-rent.com','','','','','','','','');
INSERT INTO `contacts` VALUES (72,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Tea Božičević','','','','Kupac građanin','','','',52,'tea.bozicevic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (73,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','KLETT VERLAG d.o.o.','','04776093937','','Domagojeva 15','','10000','Zagreb',52,'ivan@klett.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (74,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','FRESENIUS KABI HRVATSKA d.o.o','','43007220940','','Trg J.F.Kennedyja 6/B','','10000','Zagreb',52,'linda.franicevic@fresenius-kabi.com','','+385 98 641 412','','','','','','');
INSERT INTO `contacts` VALUES (75,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','PAPAGENO TOURS KELE d.o.o.','','92540258836','','Gomboševa 4','','10000','Zagreb',52,'papageno-tours@zg.htnet.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (76,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Neven Cegnar','','','','Kupac građanin','','','',52,'ncegnar@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (81,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','EXPRO d.o.o.','','05891754957','','Jelenovac 38/6','','10000','Zagreb',52,'zeljko@hidalgo.hr','','+385993047555','','','','','','');
INSERT INTO `contacts` VALUES (82,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Damir Petrić','','','','-kupac građanin','','','',52,'tomislav.stipanovic@xnet.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (83,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','UNIQA OSIGURANJE d.d.','','75665455333','','Savska 106','','10000','Zagreb',52,'miroslav.beslija@uniqa.hr','','+385-91-6886473','','','','','','');
INSERT INTO `contacts` VALUES (84,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Auto-Sport','','','','','','','',52,'adrianasekulic@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (85,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ALIA d.o.o.','','','','','','','',52,'dzaje14@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (86,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','T.S. ELEKTROTIM ','','','','TAVANKUTSKA 2','','10000','Zagreb',52,'tomislav.supina@xnet.hr','','+385-95-9102697','','','','','','');
INSERT INTO `contacts` VALUES (87,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','Orhideja praonica rublja j.d.o.o','','25975336177','','Mostarska 1C, Soblinec','','10360','Sesvete',52,'babic.mato@googlemail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (88,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','DOKUMENTA d.o.o.','','00414682067','','Rudeška cesta 99','','10000','Zagreb',52,'josip.dadic@dokumenta.hr','','+385-993322381','','','2015200 - Zagreb - max 50km (29.04.2015 pokupili stvari iz IKEA-e za Pašman)','','','');
INSERT INTO `contacts` VALUES (93,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','NEO ORBIS d.o.o.','','45109449461','','Pokornoga 81','','10000','Zagreb',52,'darko@pentax.hr','','+386-91-1524859','','','','','','');
INSERT INTO `contacts` VALUES (94,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ILIJA KOVAČEVIĆ','','','','KUPAC GRAĐANIN','','','',52,'','','','','','','','','');
INSERT INTO `contacts` VALUES (95,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','Romano Miljković','','','','Benka Benkovića 3a','','23000','Zadar',52,'romano.miljkovic@gmail.com','','','','','popust rani booking','','','');
INSERT INTO `contacts` VALUES (96,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Luka De Marco','','','','Kupac gradanin','','','',52,'demarco.lka@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (97,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','SLOBOĐANAC d.o.o.','','57865866839','','Kozari bok IV.odvojak 6','','10000','Zagreb',52,'slobodjanac1@zg.t-com.hr','','+385-91-316-2300','','','Korisnik ostvaruje popust 15%. Višekratno korištenje','','','');
INSERT INTO `contacts` VALUES (98,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mladen Rajković','','','','Kupac građanin','','','',52,'mladenrajkovic@hotmail.com','','','','','Modest preporuka','','','');
INSERT INTO `contacts` VALUES (107,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','STRUKTURA d.o.o.','','','','','','','',52,'darko@struktura.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (108,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','MEDI PRODUCT d.o.o.','','77224327384','','Mesnička 26','','10000','Zagreb',52,'arsov.dancho@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (109,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Josip Pende','','','','Kupac gradanin','','','',52,'jpende5@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (110,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GLAS ZIVOTINJA','','','','','','','',52,'ttabak37@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (112,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Tomislav Jech','','','','Kupac gradanin','','','',52,'tomislav@terraneofestival.com','','','','','','','','');
INSERT INTO `contacts` VALUES (117,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','Agencija za znanost i visoko  obrazovanje','','83358955356','','Donje Svetice 38/5','','10000','Zagreb',52,'jadranka.zimbrek@azvo.hr','','+385 1 6274 895','','','','','','');
INSERT INTO `contacts` VALUES (119,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','Scrinium d.o.o.','','','','B. Adžije 34 ','','10000','Zagreb',52,'info@scrinium-tours.hr','','+38513646 751','','','','','','');
INSERT INTO `contacts` VALUES (121,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivan Trgo','','','','Kupac građanin','','','',52,'ivantrgo@net.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (122,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','ACCESSUS d.o.o.','','94661221794','','Hruševečka 1','','10000','Zagreb',52,'v.belina@retailsolutions.hr','','+385 91 6658 188','','','','','','');
INSERT INTO `contacts` VALUES (123,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Dejan','','','','Kupac građanin','','','',52,'mdejan@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (124,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mladen Rutalj','','','','Kupac građanin','','','',52,'mrutalj@mup.hr','','','','','Fali kategorija7pa smo dlai 15% popusta.','','','');
INSERT INTO `contacts` VALUES (125,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','Bio Pharm Vet d.o.o','','59417050556','','Medvedgradska 1c','','10000','Zagreb',52,'a.klasic@bio-pharm-vet.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (126,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','STYROTHERM d.o.o','','55985296493','','Stupničke Šipkovine 3','','10250','Donji Stupnik',52,'styrotherm@bih.net.ba','','','','','Popust 15% jer fali veći kombi.','','','');
INSERT INTO `contacts` VALUES (127,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','FOTO SERVIS d.o.o.','','65187328485','','Nova cesta 171','','10000','Zagreb',52,'fotoservisdoo@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (128,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','HRVATSKO KASKADERSKO DRUŠTVO','','38953235314','','Čikoševa 2','','10000','Zagreb',52,'stunt.toni.bobeta@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (129,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','INTESA SANPAOLO CARD D.O.O.','','63558150971','','Radnička cesta 50','','10000','Zagreb',52,'denita.burcar@intesasanpaolocard.com','','+385-99-730-2201','','','','','','');
INSERT INTO `contacts` VALUES (130,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KOŠĆAL d.o.o.','','','','','','','',52,'marko@koscal.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (131,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','ETNOGRAFSKI MUZEJ GRADA ZAGREBA','','99544354954','','Trg Ivana Mažuranića 14 ','','10000','Zagreb',52,'tajnistvo@emz.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (138,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Nino Klobas','','','','','','','',52,'nino.klobas@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (139,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','ZU Ljekarna Pablo d.o.o.','','23197705042','','Šarengradska 6','','10000','Zagreb',52,'igor.milak@ljekarna-pablo.hr','','+385-99-2563765','','','18% popusta zbog ranijeg vračanja. 7 sati ranije','','','');
INSERT INTO `contacts` VALUES (140,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivana Sadrija','','','','Kupac građanin','','','',52,'ivana.sadrija@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (141,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','AMPEU','','','','','','','',52,'stjepan.juricic@mobilnost.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (142,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','PRIJEVOZNIČKI OBRT HONESTA','','6577321233','','Đakovačka 23','','10000','Zagreb',52,'honesta@net.hr','','+385-95-7413751','','','Popust-modest preporuka','','','');
INSERT INTO `contacts` VALUES (143,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','BEKAMENT','','','','','','','',52,'info@bekament.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (144,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SV Group d.o.o.','','','','','','','',52,'drazen.petrovic@svgroup.hr','','','','','Kratki najam i fali vozilo','','','');
INSERT INTO `contacts` VALUES (145,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','PLAYBOX GRUPA D.O.O.','','25553634985','','Zagrebačka cesta 156D','','10000','Zagreb',52,'manuel.stiglic@playboxgrupa.hr','','','','','2015267 - cijena je 350 zbog veče mjesečne cijene (1/4)','','','');
INSERT INTO `contacts` VALUES (146,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','Ordinacija Bastaić','','','','Domjanićeva 5','','10000','Zagreb',52,'ljubisa.bastaic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (147,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','INSig2 d.o.o.','','33116950552','','Buzinska cesta 58','','10000','Zagreb',52,'danijel.blazevic@insig2.eu','','','','','2015231 - Osijek','','','');
INSERT INTO `contacts` VALUES (148,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','FIL.B.IS Projekt d.o.o.','','737027026','','Osječka 34','','10000','Zagreb',52,'info@filbis.hr','','','','','2015198 - Ljubljana  2015201 - produžek najma iz 2015198','','','');
INSERT INTO `contacts` VALUES (153,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mislav Kasalo','','','','-kupac građanin','','','',52,'','','','','','','','','');
INSERT INTO `contacts` VALUES (159,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','AVAC d.o.o.','','02814749529','','Kesten Brijeg 5','','10000','Zagreb',52,'avac@avac.hr','','+385 1 4580275','','','','','','');
INSERT INTO `contacts` VALUES (160,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Franjo Cavar','','','','','','','',52,'pansion.cavar@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (162,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ante Šprljan','','','','','','','',52,'asprljan@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (163,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','SUTON GRAF d.o.o.','','18277198474','','Ožujska 2','','10000','Zagreb',52,'boris.golubic@sutongraf.hr','','+385-99-5263111','','','','','','');
INSERT INTO `contacts` VALUES (164,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','TEHNIČKA ŠKOLA RUĐERA BOŠKOVIĆA','','49811265576','','Getaldičeva 4','','10000','Zagreb',52,'res.ipaprojekt@gmail.com','','+385-1-2371061','','','','','','');
INSERT INTO `contacts` VALUES (165,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Zeljko Savic','','','','','','','',52,'zsavic1974@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (167,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','PLAYBOXGRUPA D.O.O.','','25553634985','','Šubićeva 21','','10000','Zagreb',52,'maja.barakovic@playboxgrupa.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (173,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','PILANA SIMUNIC','','','','','','','',52,'pilanasimunic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (174,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hrvoje Bradica','','','','','','','',52,'hbradica@gmail.com','','','','','Popust zbog ranog bookinga','','','');
INSERT INTO `contacts` VALUES (175,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Simo','','','','','','','',52,'simo1000@net.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (176,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','udruga puz','','','','','','','',52,'puz@udrugapuz.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (177,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','NILA MEDIA GRUPA D.O.O.','','83572273882','','Ivana Lozice 3','','21000','Split',52,'nilamediagrupa@gmail.com','','+385-91-319-18-99','','','','','','');
INSERT INTO `contacts` VALUES (178,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Zvonko Zagrajski','','','','','','','',52,'zvonko.zagrajski@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (179,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','SUPER PROGRAM J D.O.O.','','98717070183','','Travanjska 16','','10000','Zagreb',52,'bozo.kvesic@superprogram.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (180,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','FRIMANTLE MEDIA ','','','','Oporovecka 12','','10000','Zagreb',52,'valentina.klasic@fremantle.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (181,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GKP STEFANJE d.o.o.','','64324403899','','','','','',52,'denis@stefanje.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (183,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','LjubišaBastaić','','-kupac građanin','','Domjanićeva 5','','10000','Zagreb',52,'ljubisa.bastaic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (187,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','GIP STUDIO D.O.O.','','91468164660','','Savska cesta 56','','10000','Zagreb',52,'intermedo@gmail.com','','+385-1-6177-119','','','GIP STUDIO D.O.O.','','','');
INSERT INTO `contacts` VALUES (188,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','ATM d.o.o.','','','','Slavoska avenija 22e','','10000','Zagreb',52,'vlatka.kink@atm.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (189,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','AGROPLAST SISAK d.o.o.','','70023893910','','Lipa ulica 20','','44000','Sisak',52,'agroplast01@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (190,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','ENERGOSPEKTAR d.o.o','','40966163739','','Alberta Fortisa 1','','10000','Zagreb',52,'milodrag.gadze@energospektar.hr','','+385-98-5263113','','','','','','');
INSERT INTO `contacts` VALUES (191,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Biserka Tešević','','','','','','','',52,'biba.tesevic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (192,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','FremantleMedia Hrvatska d.o.o.','','','','','','','',52,'damir.silov@fremantle.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (193,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Zlatko Vlajčić','','','','','','','',52,'zlatkovlajcic@yahoo.com','','+385-98-607760','','','','','','');
INSERT INTO `contacts` VALUES (194,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ZEM NADZOR d.o.o.','','','','','','','',52,'elamihovilovic@hotmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (195,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','NEMETH-PROJEKT d.o.o.','','88463041378','','Radnička 57','','10000','Zagreb',52,'boran.petljak@nemeth-projekt.hr','','+385-99-4683445','','','','','','');
INSERT INTO `contacts` VALUES (196,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DELTRON d.o.o.','','','','','','','',52,'antonija.gabela@deltron.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (203,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','AUREL-MED d.o.o.','','16597482939','','Ive Paraća  6  ','','10360',' Sesvete  ',52,'zlatkovlajcic@yahoo.com','','+385-98-607760','','','','','','');
INSERT INTO `contacts` VALUES (207,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mario Turkalj','','','','Kupac gradanin','','','',52,'turkaljmario@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (208,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Imas viska','','','','','','','',52,'miro.lujic@rtl.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (209,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','GRAFIČKI URED VL.MARIO ANIČIĆ','','99033755649','','Mesnička 35','','10000','Zagreb',52,'mario.ured@gmail.com','','+385--98-384-895','','','','','','');
INSERT INTO `contacts` VALUES (210,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Marko Konjevod','','','','Kupac građanin','','','',52,'marko@easeofmove.com','','','','','','','','');
INSERT INTO `contacts` VALUES (211,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MENORAH FILM','','','','','','','',52,'fjp@menorah-film.com','','','','','','','','');
INSERT INTO `contacts` VALUES (212,'0000-00-00 00:00:00','2015-12-22 09:19:39','','','UDRUGA BOSANSKIH HRVATA PRSTEN','','92653606230','','Grada Vukovara 235','','10000','Zagreb',52,'biskicfilip@gmail.com','','','','','humanitarna pomoć','','','');
INSERT INTO `contacts` VALUES (221,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','ADRIA MEDIA ZAGREB d.o.o.','','30421345069','','Radnička cesta 39','','10000','Zagreb',52,'t.rakos@adriamedia.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (223,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','BIJELA HARMONIJA d.o.o.','','31022857153','','Samoborska cesta 266','','10000','Zagreb',52,'prodaja@harmonija-usluge.com','','','','','','','','');
INSERT INTO `contacts` VALUES (227,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Gordana Matijević','','','','Kupac građanin','','','',52,'gmatijevic0807@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (228,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','CONTING d.o.o.','','58276100989','','Vučnik 92','','51300','Delnice',52,'conting@ri.t-com.hr','','+385-98-9834788','','','','','','');
INSERT INTO `contacts` VALUES (229,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ŽEPOH d.o.o.','','','','','','','',52,'zeljko.bosnar@zepoh.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (230,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ALFA TRANS','','','','','','','',52,'alfatrans0@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (231,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','KUHN-HRVATSKA d.o.o.','','04057188037','','Rakitnica 4','','10000','Zagreb',52,'danko.weber@kuhn.hr','','+385-99-3101555','','','','','','');
INSERT INTO `contacts` VALUES (232,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','UniCredit Leasing Croatia d.o.o.','','','','Centar Bundek – D. T. Gavrana 17','','10000','Zagreb',52,'ivan.spoljar@unicreditleasing.hr','','','','','popust 20% ako je rezervacija izvšena do23.07.2013','','','');
INSERT INTO `contacts` VALUES (233,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','HRVATSKI POLJOPRIVREDNI ZADRUŽNI SAVEZ','','','','','','','',52,'bozo.volic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (237,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivana Sladić','','','','Kupac građanin','','','',52,'ivana.lenic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (238,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Nikola Salinovic','','','','','','','',52,'johnny53ster@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (239,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ZAGREBAČKA UMJETNIČKA GIMNAZIJA','','','','','','','',52,'vanda.koludrovic@umjetnicka-gimnazija.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (243,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Maja Vujnovac','','','','','','','',52,'maja37@windowslive.com','','','','','','','','');
INSERT INTO `contacts` VALUES (244,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','Ured za suzbijanje zlouporabe droga Vlade Republike Hrvatske','','20706369236','','Preobraženska 4/II','','10000','Zagreb',52,'marina.fulir@uredzadroge.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (245,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','Međunarodna komisija za sliv rijeke Save','','','','Kneza Branimira 29','','10000','Zagreb',52,'isrbc@savacommission.org','','','','','2015161 - rani booking, treći najam, Ljubljana- Beaograd maraton','','','');
INSERT INTO `contacts` VALUES (246,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Marko Stokov','','','','','','','',52,'marko.stokov@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (247,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hrvatsko društvo za robotiku','','','','','','','',52,'jelka4@net.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (248,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Vanja Šikić','','','','','','','',52,'vanja.sikic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (250,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','ATLAS d.d.','','','','Izidora Kršnjavoga 1','','10000','Zagreb',52,'zdravko.krsnik@atlas.hr','','+385 1 4698 015','','','','','','');
INSERT INTO `contacts` VALUES (251,'0000-00-00 00:00:00','2015-12-22 09:14:15','','','SINAGO d.o.o.','','64020845944','','Mokrice 180D','','','Oroslavje',52,'proizvodnja@sinago.hr','','+385 1 4550615','','','2015192 - Zlatna Cipelica (Kutjevo)  2015263 - Čavli-Rijeka','','','');
INSERT INTO `contacts` VALUES (252,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Lidl Hrvatska d.o.o k.d.','','','','','','','',52,'mislav.gajinov@lidl.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (253,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Martina Golić','','','','','','','',52,'golic.martina@hotmail.fr','','','','','','','','');
INSERT INTO `contacts` VALUES (254,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ADRIATIKA DRVO d.o.o.','','','','','','','',52,'adriatika.drvo@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (255,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Danijel Vezmarović','','','','Kupac građanin','','','',52,'vezmarovic.danijel2@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (258,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Goran Šamec','','','','','','','',52,'','','','','','','','','');
INSERT INTO `contacts` VALUES (259,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Saša Šafranič','','','','Kupac građanin','','','',52,'safranic@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (260,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Dasović','','','','','','','',52,'dasovic@msn.com','','','','','','','','');
INSERT INTO `contacts` VALUES (261,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','AZONPRINTER d.o.o.','','76288894727','','Matije Jandrića 20','','10000','Zagreb',52,'anamarija@azonprinter.com','','','','','','','','');
INSERT INTO `contacts` VALUES (262,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Peter Churdo','','','','','','','',52,'pchurdo@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (263,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','PLIVA HRVATSKA d.o.o.','','','','Prilaz baruna Filipovića 25','','10000','Zagreb',52,'maja.mihoci@pliva.com','','','','','','','','');
INSERT INTO `contacts` VALUES (264,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','DTR d.d.','','29787128314','','Preradovićeva 20','','10000','Zagreb',52,'vprerad@deteer.hr','','+385 91 518 3135','','','','','','');
INSERT INTO `contacts` VALUES (265,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Zrinka Milošev','','','','Kupac građanin','','','',52,'zrinka.milosev@gmail.com','','','','','15.03 ---- 4 dana----','','','');
INSERT INTO `contacts` VALUES (266,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hrvoje ','','','','','','','',52,'htroselj@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (267,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','PERIVAN USLUGE d.o.o.','','','','','','','',52,'ivpercic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (270,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','KUĆA PRODAJE d.o.o.','','50539738333','','Ulica grada Vukovara 269d','','10000','Zagreb',52,'cimbur55@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (271,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Sandra Mavračić','','','','','','','',52,'sandramavracic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (272,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Maja Šilhard','','','','Kupac građanin','','','',52,'majasilhard@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (275,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','UGO GRUPA d.o.o.','','','','Savska cesta 165','','10000','Zagreb',52,'uprava@ugo-grupa.hr','','00385-99-204-5025','','','','','','');
INSERT INTO `contacts` VALUES (276,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','KOVINOPOJASAR d.o.o.','','01102530839','','Šarengradska 11','','10000','Zagreb',52,'kovinopojasar-dpb@zg.t-com.hr','','+385-98-9851-882','','','','','','');
INSERT INTO `contacts` VALUES (277,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Vjekoslav Ćavar','','','','Kupac građanin','','','',52,'vcavar@net.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (278,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Damir Šimić','','','','Kupac građanin','','','',52,'sime.damir@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (279,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Krešo Kristo','','','','Kupac građanin','','','',52,'kreso.kristo@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (280,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Branko Ducato','','','','','','','',52,'branko.ducato@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (281,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','FRENCH CONNECTION d.o.o.','','08494126774','','Rimski jarak 24','','10000','Zagreb',52,'frenchconnection.hr@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (282,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Marina Leko','','','','','','','',52,'romana.zidak@nielsen.com','','','','','','','','');
INSERT INTO `contacts` VALUES (285,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','ŽRK Samobor','','93078403397','','Perkovčeva 90','','10430','Samobor ',52,'htroselj@gmail.com','','+385-995355723','','','','','','');
INSERT INTO `contacts` VALUES (286,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Štefica Vincek','','','','Kupac građanin','','','',52,'stefica.vincek@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (287,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Domagoj Matašić','','','','Kupac građanin','','','',52,'dmatasic@hotmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (288,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Smiljan Prpić','','','','Kupac građanin','','','',52,'smiljan48@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (297,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','TEHNO-VENT D.O.O.','','','','','','','',52,'tehno-vent@zg.t-com.hr','','','','','TEHNO-VENT@ZG.T-COM.HR','','','');
INSERT INTO `contacts` VALUES (298,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Pero Posavi','','','','Kupac građanin','','','',52,'perodani@net.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (300,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','MEĐUNARODNI SAJAM SERVIS d.o.o.','','39683805636','','Rijeznica 66','','10000','Zagreb',52,'alen.huskic@if-services.net','','','','','','','','');
INSERT INTO `contacts` VALUES (301,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','NUM','','','','','','','',52,'mario@num.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (302,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','INTERMEZZO UDRUGA','','','','','','','',52,'intermezzo.kultura@gmail.com','','','','','Vozili prošle godine','','','');
INSERT INTO `contacts` VALUES (303,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','INTERIJER PROJEKT d.o.o.','','','','','','','',52,'info@interijerprojekt.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (304,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','DESANO STUDIO d.o.o.','','28348688204','',' Petrovaradinska 69','','10000','Zagreb',52,'sanja@desano-studio.hr','','','','','2015159 - Poreč','','','');
INSERT INTO `contacts` VALUES (305,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Jelens Stanin','','','','Kupac građanin','','','',52,'jelena.stanin@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (306,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','RECRO','','','','Avenija Većeslava Holjevca 40','','10000','Zagreb',52,'nina.vujicic@recro-net.hr','','+385 99 3030 652','','','','','','');
INSERT INTO `contacts` VALUES (307,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','Ministarstvo graditeljstva i prostornog uređenja','','','','Ulica Republike Austrije 20','','10000','Zagreb',52,'ivan.derek@mgipu.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (308,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Nikšić','','','','Kupac građanin','','','',52,'vniksic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (309,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','HRVATSKI MUZEJ TURIZMA','','47076735780','','Park Angiolina 1','','51410','Opatija',52,'marin.pintur@hrmt.hr','','','','','Veza ETnografski muzej','','','');
INSERT INTO `contacts` VALUES (310,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Dario Radović','','','','Kupac građanin','','','',52,'milica.radovic@infodom.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (311,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','DALEKOVOD PROIZVODNJA','','','','Vukomerička 9','','10410','Velika Gorica',52,'dubravko.mestrovic@dalekovod.hr','','','','','već bili','','','');
INSERT INTO `contacts` VALUES (314,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','BELVISIO d.o.o.','','19736265288','','Rakovčeva 16','','10000','Zagreb',52,'iprgomet1@net.amis.hr','','','','','igor','','','');
INSERT INTO `contacts` VALUES (315,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Studentski zbor Sveučilišta u Rijeci','','','','','','','',52,'medunarodna@sz.uniri.hr','','0998855652','','','Popust 15% ako plate rezervaciju do 8.4','','','');
INSERT INTO `contacts` VALUES (316,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','TEHNO DOM d.o.o.','','','','','','','',52,'ivan.miksa@tehno-dom.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (318,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','IPA projekta FERTEO','','','','','','','',52,'andrija.petrovic@zagreb.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (320,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Vladimir Nikšić','','','','-kupac građanin','','','',52,'vniksic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (323,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivo Uvodić','','','','','','','',52,'iuvodic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (324,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Goran Karlović','','','','Kupac građanin','','','',52,'kgogoo@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (325,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','ORGANIZACIJA MLADIH STATUS: M','','96065912144','','Zinke Kunc 3','','10000','Zagreb',52,'mislav.mandir@status-m.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (326,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','DIALOG KOMUNIKACIJE','','','','Šoštarićeva 10','','10000','Zagreb',52,'veronika.slogar@dialog-komunikacije.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (327,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','Vijeće bošnjačke nacionalne manjine Grada Zagreba','','44926257293','','Trnjanska cesta 35','','10000','Zagreb',52,'vbnmgz@vbnmgz.hr','','01 631 41 09','','','','','','');
INSERT INTO `contacts` VALUES (328,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','UDRUGA INOVATORA HRVATSKE','','','','','','','',52,'idujmovic@inovatorstvo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (329,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Domagoj Miletić','','','','','','','',52,'do.miletic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (330,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','NETGEN d.o.o.','','56703914419','','Ante Mike Tripala 3','','10000','Zagreb',52,'davorka@netgen.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (331,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','HESET d.o.o.','','04135878306','','Mandlova 3','','10000','Zagreb',52,'dijana.bello@heset.hr','','+385-95-1972121','','','','','','');
INSERT INTO `contacts` VALUES (337,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Vicko Fiorenini','','','','Kupac građanin','','','',52,'vicko.fiorenini@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (338,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mirjana Strbac','','','','Kupac građanin','','','',52,'mirjanastrbac@yahoo.de','','','','','','','','');
INSERT INTO `contacts` VALUES (339,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Matija Bahorski','','','','Kupac građanin','','','',52,'matija.bahorski@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (340,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','HSA-SF','','','','ivana lučića 5','','10000','zagreb',52,'filip.merdic@gmail.com','','','','','-7 dana...7.7.2012 10..','','','');
INSERT INTO `contacts` VALUES (341,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','FUČKALA d.o.o.','','','','','','','',52,'fuckala@fuckala.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (342,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Elsa Lindgreen','','','','Kupac građanin','','','',52,'elsalehen@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (343,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Vedran Zoričić','','','','Kupac građanin','','','',52,'vedran.zoricic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (344,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Nikolina Radočaj','','','','Kupac građanin','','','',52,'nikolina.radocaj@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (345,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','INETEC','','69707113052','','Dolenica 28','','10250','Lučko/Zagreb',52,'marina.bedran@inetec.hr','','','','','2015107 - 03.05.2015- dali nedelju gratis, 4000km ukupno po kombiju Rumunjska','','','');
INSERT INTO `contacts` VALUES (346,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Marion Špehar','','','','','','','',52,'marion.spehar@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (349,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','STUDIO DENTALNE MEDICINE  FRNTIĆ','','51719765777','','Mlinovi 159a','','10000','Zagreb',52,'info@studiodental-frntic.com','','','','','','','','');
INSERT INTO `contacts` VALUES (350,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','NOVALIJA KOMPAS','','','','','','','',52,'novalija-kompas@gs.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (351,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ŠKOLSKA KNJIGA','','','','','','','',52,'renata.sadzak@skolskaknjiga.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (352,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DHC','','','','','','','',52,'info@dhc.com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (353,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','MEDIAVAL d.o.o.','','','','Budmanijeva 5','','10000','Zagreb',52,'zdravko.ivic@media-val.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (354,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','REDOX d.o.o.','','36138701402','','Lanište 24','','10000','Zagreb',52,'redox@redox.hr','','+385-98-274981','','','','','','');
INSERT INTO `contacts` VALUES (355,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','MAJER-PROM d.o.o.','','33412382465','','Kriška 32','','10000','Zagreb',52,'kristina.juricic@ad-promet.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (356,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','SPEKTAR PUTOVANJA d.o.o.','','39672837472','','Tkalčićeva 15','','10000','Zagreb',52,'glodic.goran@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (358,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','FLUID SISAK d.o.o.','','83678016096','','A.Starčevića 1A','','43000','Sisak',52,'cafefluid@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (359,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Tihomir Trikvić','','','','Kupac građanin','','','',52,'ttihomir@hotmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (360,'0000-00-00 00:00:00','2015-12-22 09:20:01','','','HORTILAB d.o.o.','','','','Sisačka 4','','10410','Velika Gorica',52,'hortilab.info@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (361,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MULTIMATIKA','','','','','','','',52,'multimatika@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (362,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivica Srncevic','','','','Kupac građanin','','','',52,'srna1@windowslive.com','','','','','','','','');
INSERT INTO `contacts` VALUES (363,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ORBICO','','','','','','','',52,'darko.krnic@orbico.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (365,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','INTERIER MONT d.o.o.','','35036017877','','Želježnička 41','','32227','Borovo',52,'zarko@interier-mont.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (367,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Elsa Lindgrehh','','','','-kupac građanin','','','',52,'elsalehen@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (374,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','FALKO d.o.o.','','29928371299','','Krvarić 76 C ','','10000','Zagreb',52,'ttihomir@hotmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (376,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SPLITSKI SPORTSKI SAVEZ GLUHIH','','','','','','','',52,'mirek2001hr@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (377,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','PERO oplate i skele','','','','','','','',52,'danka.pamic@peri.com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (379,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','shaun  underwood','','','','','','','',52,'julia@iasnikatravel.com','','','','','','','','');
INSERT INTO `contacts` VALUES (380,'0000-00-00 00:00:00','2015-12-22 09:14:15','','','DAMONT INTERIJERI d.o.o.','','27473530124','','Lukačićeva 7','','','Samobor',52,'persicj@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (381,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Daniel Kempgen','','','','','','','',52,'Daniel.Kempgen@statravel.de','','','','','','','','');
INSERT INTO `contacts` VALUES (387,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SAKI&SAKI d.o.o.','','','','','','','',52,'sadik.rakovic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (388,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','Hr CČP','','','','Savska c. 41/IV','','10000','Zagreb',52,'ivana.ivicic@cro-cpc.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (389,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','GENERA LIJEKOVI d.o.o.','',' 98483262055','','Svetonedeljska 2','','10436','Rakov Potok',52,'DPisonic@genera.hr','','+385 99 78 88 623','','','','','','');
INSERT INTO `contacts` VALUES (390,'0000-00-00 00:00:00','2015-12-22 09:20:01','','','UDRUGA ŠAPICA','','30697626511','','Kanadska 3','','10290','Zaprešić',52,'vukasovi@unhcr.org','','','','','','','','');
INSERT INTO `contacts` VALUES (391,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GDi','','','','','','','',52,'tomislav.svilicic@gdi.net','','','','','','','','');
INSERT INTO `contacts` VALUES (392,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Kathrin Kaufhold','','','','','','','',52,'kaufhold@isw-gmbh.de','','','','','','','','');
INSERT INTO `contacts` VALUES (393,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SMELT INŽINJERING d.o.o.','','','','','','','',52,'instalacije.mamic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (394,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','RODIN LET D.O.O.','','33704349594','','Čanićeva 14','','10000','Zagreb',52,'tijana@roda.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (395,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','KERAMIKA MODUS d.o.o.','','93756665118','','Vladimira Nazora 67','','33515','Orahovica',52,'ghorvat@keramika-modus.com','','+385-98-205-681','','','','','','');
INSERT INTO `contacts` VALUES (396,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KOCKICA','','','','','','','',52,'tajnik@kockice.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (397,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','GEBRUDER WEISS d.o.o.','','05216322294','','Jankomir 25','','10000','Zagreb',52,'marija.jaksic@gw-world.com','','','','','','','','');
INSERT INTO `contacts` VALUES (398,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','MULTI FORMA d.o.o.','','69146219847','','Jakuševečka 15','','10000','Zagreb',52,'multi-forma@zg.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (399,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ante Prizmić','','','','','','','',52,'tonci94@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (400,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','HRVATSKO UDRUŽENJE STUDENATA BRODOGRADNJE','','74463657147','','Ivana Lučića 5','','10000','Zagreb',52,'tomich.denis@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (415,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Grdović','','','','','','','',52,'jgrdovic@zv.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (416,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','SVEUČILIŠTE U RIJECI TEHNIČKI FAKULTET','','46319717480','','Vukovarska 58','','51000','Rijeka',52,'tonik@riteh.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (417,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','LOGISTIKA VIOLETA','','62874063131','','Obrež Zelinksi 55','','10380','Sv.Ivan Zelina',52,'dijana.bilic@violeta.com','','','','','','','','');
INSERT INTO `contacts` VALUES (418,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Damir Marijan','','','','','','','',52,'damirmarijan@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (419,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ALFA TRANS d.o.o.','','','','','','','',52,'dejan.buic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (420,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Miroslav Bukovšćak','','','','Kupac građanin','','','',52,'miroslav.bukovscak@zg.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (421,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','POSILOVIĆ D.ZAŠTITA k.d..','','63051259879','','Ljudevita Posavskog 14','','10000','Zagreb',52,'sanja.subotnik@posilovic-zastita.hr','','','','','skračeno i redovito','','','');
INSERT INTO `contacts` VALUES (422,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','FORTUNA KOMERS d.o.o.','','','','','','','',52,'martina.mihelj@fortuna-digital.com','','','','','','','','');
INSERT INTO `contacts` VALUES (423,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','OBRT ZA TRGOVINU, PRIJEVOZ I IZNAJMLJIVANJE ZLATNA CIPELICA , VL. ANKICA GOJEVIĆ','','75651976730','','Okučanska 11','','10040','Zagreb',52,'ankica_gojevic@hotmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (425,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','BACAČI SJENKI','','75226535695','','Bosanska ulica 10','','10040','Zagreb',52,'bacaci.sjenki@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (426,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mislav Šajnović','','','','','','','',52,'sajnovic166@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (427,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','RODA','','','','','','','',52,'ured@roda.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (428,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','E-DIZAJN uslužni obrt i putnička agencija','','13863801781','','Škorpikova 11','','10000','Zagreb',52,'karlo@e-dizajn.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (429,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Damir','','','','','','','',52,'dapola@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (430,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','ADMETAM BUSINESS CONSULTANTS d.o.o.','','93922704282','','Rapska 46','','10000','Zagreb',52,'s.curic@admetam.com','','','','','','','','');
INSERT INTO `contacts` VALUES (431,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ichtis d.o.o.','','','','','','','',52,'operations@ichtisonline.com','','','','','','','','');
INSERT INTO `contacts` VALUES (438,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','MEĐ. SAJAM SERVIS d.o.o.','','39683805636','','Rijeznica 66','','10000','Zagreb',52,'Ifs@if-services.net','','','','','','','','');
INSERT INTO `contacts` VALUES (439,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ILOV','','','','','','','',52,'info@ilov.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (445,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','Centar za građanske inicijative Poreč','','11593010193','','Partizanska 2d','','52440','Poreč',52,'cgiporec@cgiporec.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (446,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ECOINA d.o.o.','','','','','','','',52,'etudic@ecoina.com','','','','','','','','');
INSERT INTO `contacts` VALUES (447,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','ZLARING d.o.o.','','97665390982','','Crnčićeva 9','','10000','Zagreb',52,'zlaring@zlaring.hr','','+385 1 2303937','','','','','','');
INSERT INTO `contacts` VALUES (448,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','MEDIA REGIS d.o.o.','','49012352778','','Matice Hrvatske 7','','10410','Velika Gorica',52,'vedran@mediaregis.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (449,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','JK - NAUTIKA','','00648383346','','Jagodno 105b','','10415','Novo Čiče',52,'info@jk-nautika.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (450,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','THERMIA d.o.o.','','81433864455','','Vukovarska 142','','31540','Donji Miholjac',52,'luka.kovacevic@thermia.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (451,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','LS TURISTIČKA AGENCIJA','','','','Ante Starčevića 71','','21300','Makarska',52,'','','+385 21 678 566 ','','','','','','');
INSERT INTO `contacts` VALUES (452,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GEO ISTRAŽIVANJE','','','','','','','',52,'geoistrazivanje@zg.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (453,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Luka Despot','','','','Kupac građanin','','','',52,'ldespot@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (454,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','FOTOGRAFICA','','','','','','','',52,'info@fotografica.biz','','','','','','','','');
INSERT INTO `contacts` VALUES (455,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','z@corkx.co.uk','','','','','','','',52,'z@corkx.co.uk','','','','','','','','');
INSERT INTO `contacts` VALUES (456,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','NATURA ISA d.o.o.','','33041190079','','Bulvanova 14','','10000','Zagreb',52,'zoranh@naturaisa.com','+385-95-8000464','','','','','','','');
INSERT INTO `contacts` VALUES (457,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Koraljka Stazić','','','','Kupac građanin','','','',52,'koraljka.stazic@zg.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (458,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','BAYER d.o.o.','',' 56386591827','','Radnička cesta 80','','10000','Zagreb',52,'kresimir.simicic@bayer.com','','','','','15.05','','','');
INSERT INTO `contacts` VALUES (466,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','TEHNOKOM d.o.o.','','','','','','','',52,'renato.gecan@tehnokom.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (467,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Bonfanti d.o.o','','','','','','','',52,'vickovic@meinl.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (468,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Bojan Kuscevic','','','','','','','',52,'bojan.kuscevic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (469,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','PROMO IDEJA d.o.o.','','','','','','','',52,'info@promologic.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (470,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','GAMBOR d.o.o.','','88521601653','','Šenova 4','','10000','Zagreb',52,'flanjka@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (474,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Tomislav Vučković','','','','Kupac građanin','','','',52,'t.vuchkovich@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (475,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','MORIS d.o.o.','','46480621776','','Belostenčeva 3','','10000','Zagreb',52,'nadabenko@gmail.com','+385-98-452775','','','','','','','');
INSERT INTO `contacts` VALUES (476,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','HORECA TRGOVINA d.o.o','','69500958846','','Čulinečka cesta 152','','10040','Zagreb',52,'sasa@horeca-trgovina.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (477,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','VETERINARSKA STANICA VRBOVEC d.o.o.','','43025336094','','Kolodvorska 68','','10340','Vrbovec',52,'vujevic.vet@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (478,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Kazalište, vizualne umjetnosti i kultura Gluhih - DLAN','','','','','','','',52,'udruga.dlan.zagreb@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (479,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','TECUS d.o.o.','','49889862881','','Radnička cesta 48,','','10000','Zagreb',52,'ambalaza@ambalaza.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (480,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','Intercon Dubrovnik tur. I pom.agencija d.o.o.','','13369950936','','Dr.A.Starčevića 69','','20000','Dubrovnik‐RH',52,'intercon@intercon.hr','','+385-98-393008','','','','','','');
INSERT INTO `contacts` VALUES (481,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','IVAS GRUPA d.o.o.','','22926614320','','Donje Svetice 40','','10000','Zagreb',52,'andreja.tot@ivas.hr','','','','','2015237 - Ljubljana   2015254 - Vodice','','','');
INSERT INTO `contacts` VALUES (482,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','UDRUGA STUDENATA MEĐIMURJA','','28396279066','','Ilica 10/3','','10000','Zagreb',52,'udruga.studenata.medjimurja@gmail.com','+385-98-763-658','','','','','','','');
INSERT INTO `contacts` VALUES (493,'0000-00-00 00:00:00','2015-12-22 09:19:40','','','WAWA d.o.o.','','59785180614','','Zagrebačka 26','','10430','Samobor',52,'ivan.jezic@wawa.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (494,'0000-00-00 00:00:00','2015-12-22 09:20:01','','','Športsko društvo gluhih \"Silent\"','','09942019810','','Ulica grada Vukovara 284/5','','10000','Zagreb',52,'hss.gluhih@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (495,'0000-00-00 00:00:00','2015-12-22 09:20:01','','','MIRT INTERIJER d.o.o. ','','28491086971','','1. novački odvojak 15','','10437','Bestovje',52,'mirt.interijer.d.o.o@zg.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (499,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','UGOSTITELJSKI OBRT CAFFE BAR \"BOOGIE JUNGLE\"','','','','','','','',52,'boogiejungle@net.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (500,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','SCHENKER d.o.o., Croatia','','','','Slavonska avenija 52i','','10000','Zagreb',52,'marina.baron@dbschenker.com','','','','','','','','');
INSERT INTO `contacts` VALUES (501,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ANKA PRANJIĆ','','','','','','','',52,'spektra.tim@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (503,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','FAKULTET STROJARSTVA I BRODOGRADNJE','','','','Ivana Lučića 5','','10000','Zagreb',52,'nikola.maslov@fesb.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (504,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','INGRA-M.E. d.o.o.','','48123751191','','Humboldta Alexandera 4/b','','10000','Zagreb',52,'milan.cicvara@ingra.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (505,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','OPT Tržnica','','','','','','','',52,'niko.pervan@otp-trznca.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (509,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','Javna ustanova RERA SD','','','','Domovinskog rata 2','','21000','Split',52,'marija.vucica@rera.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (510,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','PRESSCUT d.o.o.','','34672089688','','Domagojeva 2','','10000','Zagreb',52,'marko.poljak@presscut.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (511,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Srečko Pačić','','','','','','','',52,'srecko.pacic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (512,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','AQUA PURITAS','','','','','','','',52,'aqua.puritas@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (513,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Goran Ivezić','','','','Kupac građanin','','','',52,'goran.ivezic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (514,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','EBIT','','','','','','','',52,'michael@ebit.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (515,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','ZVONA USLUGE d.o.o.','','99421577215','','Busovačka 19','','10000','Zagreb',52,'tomislav@zvonacatering.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (516,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','METRO CONSILIUM d.o.o.','','07078076353','','Milana Butkovića 4','','51000','Rijeka',52,'mladen@colledani.hr','','+385994426967','','','','','','');
INSERT INTO `contacts` VALUES (517,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','HRVATSKI ŠPORTSKO RIBOLOVNI SAVEZ','','','','Krešimira Čosića 11','','10000','Zagreb',52,'rentacar.modest@gmail.com','','','','','19.07. do 29.07. 540,00 ','','','');
INSERT INTO `contacts` VALUES (518,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','KRAS GRUPA d.o.o.','','53200971578','','26 DIVIZIJE 8','','51415','LOVRAN',52,'info@kras-grupa.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (519,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','PRINT GRUPA d.o.o.','','07275785182','','Brezjanski put 33','','10431','Sveta Nedjelja',52,'valentina@printgrupa.com','','','','','','','','');
INSERT INTO `contacts` VALUES (520,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','FESB','','','','','','','',52,'dbalic@fesb.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (521,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','MI-STAR d.o.o.','','44816778493','','Novska ulica 24','','10000','Zagreb',52,'mario@mistar.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (522,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','Sveučilište u Zagrebu Agronomski fakultet','','76023745044','','Svetošimunska c. 25','','10000','Zagreb',52,'dpreiner@agr.hr','','+385 98 391836','','','','','','');
INSERT INTO `contacts` VALUES (523,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','SABRINA DOMINUS d.o.o.','','25414636119','','4.Luka 14','','10040','Zagreb',52,'parfumerija.dominus@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (524,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Nenad Crnica','','','','Kupac građanin','','','',52,'neno@sampos.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (525,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','G.K. MAKSIMIR','','26735791870','','Ulica grada Vukovara 284','','10000','Zagreb',52,'sanja.samec@hotmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (526,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Zlatko Čule','','','','Kupac građanin','','','',52,'zlatko.cule@ri.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (527,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','HUCK FINN d.o.o.','','66802959608','','Vukovarska 271','','10000','Zagreb',52,'bruno@huck-finn.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (536,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','INGRA ME d.o.o.','','48123751191','','Alexandera Humboldta 4B','','10000','Zagreb',52,'milan.cicvara@ingra.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (540,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','HRVATSKI SAVEZ SJEDEĆE ODBOJKE','','72382180496','',' K o l a r o v a 18','','10000','Zagreb',52,'tajnik.hsso@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (541,'0000-00-00 00:00:00','2015-12-22 09:20:01','','','GRAD DUBROVNIK - UPRAVNI ODJEL ZA KULTURU','','21712494719','','Branitelja Dubrovnika 7','','20000','Dubrovnik',52,'ssimunovic@dura.hr','','+ 385 99 52 000 46','','','','','','');
INSERT INTO `contacts` VALUES (542,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','HULAHOP d.o.o.','','00595398836','','Vlaška 72a','','10000','Zagreb',52,'nino@animafest.hr','','+385-91-1765636','','','','','','');
INSERT INTO `contacts` VALUES (551,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Marin Bartolić','','','','Kupac građanin','','','',52,'fertoll@post.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (555,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Tanja Pajk','','','','Kupac građanin','','','',52,'tanja.pajk@zagreb.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (556,'0000-00-00 00:00:00','2015-12-22 09:14:16','','','CONUS INŽENJERING d.o.o.','','15242307029','','Žrtava fašizma 1B','','','Umag',52,'info@conus-ing.com','','','','','','','','');
INSERT INTO `contacts` VALUES (557,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','VIKAPOTA','','','','','','','',52,'info@vikapota.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (558,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','AKTERPUBLIC','','','','Remetinečka cesta 9d ','','10000','Zagreb',52,'ivan.bartolovic@akterpublic.hr','','+385 91 5858 769','','','','','','');
INSERT INTO `contacts` VALUES (559,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Sanja Žugaj','','','','','','','',52,'udut.zagreb@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (569,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Krešimir Vuković','','','','Kupac građanin','','','',52,'vukovic.kresimir@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (570,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mate Dragičević','','','','Kupac građanin','','','',52,'kardumzrinka10@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (573,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','PDR d.o.o.','','42222541140','','Trg Žrtava fašizma 6','','10290','Zaprešić',52,'info@pdr.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (574,'0000-00-00 00:00:00','2015-12-22 09:14:16','','','UDRUGA BUDISTIČKO DRUŠTVO SHECHEN','',' 14856368987','','Antuna Mihića 10','','',' 51410 Opatija',52,'shechencroatia@gmail.com','','+385-98-267-427 ','','','','','','');
INSERT INTO `contacts` VALUES (575,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','SARGON j.d.o.o.','','58114974989','',' Kralja Tomislava 98','','35410','Nova Kapela',52,'info@kombi-rent.com','','','','','','','','');
INSERT INTO `contacts` VALUES (576,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivan Kljaić','','','','Kupac građanin','','','',52,'ivan.kljaic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (577,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','PROMONDIS d.o.o.','','92796174019','','Slovenska 13','','10437','Bestovje',52,'ana.profaca@promondis.hr','','','','','2015112 - na drugo vozilo dodatnih 5% jer idu u paru  2015233 - Požega  2015235 - Vukovar','','','');
INSERT INTO `contacts` VALUES (578,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','POU Mato Došen','','','','','','','',52,'dora.markuz@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (584,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','KUD \"ZORA\"','','57615715265','','Kneza Adama 1,','','10363','Belovar',52,'ivansikic@net.hr','','+385-095-5919 627','','','','','','');
INSERT INTO `contacts` VALUES (585,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Dubravka Ciglenecki','','','','Kupac građanin','','','',52,'dubravkaciglenecki@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (591,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Slaven Gašparić','','','','Kupac građanin','','','',52,'','','','','','','','','');
INSERT INTO `contacts` VALUES (592,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GORAN HANŽEK','','','','','','','',52,'marin@meneghetti.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (593,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','VEKTOR GRUPA d.o.o.','','40485772360','','Joha 3','','10040','Zagreb',52,'antonio@vektorgrupa.com','','+385-99-2437900','','','','','','');
INSERT INTO `contacts` VALUES (594,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hrvatska škola Outward Bound','','','','','','','',52,'dubravka.simunec@outwardbound.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (595,'0000-00-00 00:00:00','2015-12-22 09:14:16','','','DADO SISAK d.o.o.','','79282509413','',' Petrinjska 156','','','Sisak',52,'dado1sisak@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (596,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','B.N.S. GRADNJA d.o.o.','','65325497806','','Polanjščak 6','','10000','Zagreb',52,'bbrzic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (597,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','POU AMC Nova Gradiška','','','','KOARSKA 2','','35400','Nova Gradiška',52,'gbakunic@pou-amc.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (598,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','OMEGA GRUPA d.o.o.','','34767079413','','Tomislavova 11/2','','10000','Zagreb',52,'boris@omega-grupa.hr','','','','','DARKO KOVAČ','','','');
INSERT INTO `contacts` VALUES (599,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','FESB SPLIT','','','','','','','',52,'ups.fesb@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (600,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Smiljan Berišić','','','','Kupac građanin','','','',52,'stjohnspark2014@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (601,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','HIDRAULIKA SERVISI d.o.o.','','18117849564','','Žitnjak b.b.','','10000','Zagreb',52,'info@hidraulika-servisi.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (602,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Klaudio Skok','','','','Kupac građanin','','','',52,'klaudio.skok1@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (613,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Marko Kramarić','','','','','','','',52,'marko.kramaric@fero-term.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (614,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','Hrvatska komora dentalne medicine','','','','Kurelčeva 3','','10000','Zagreb',52,'ivana.staresinic@hkdm.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (615,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','KOTURIĆ d.o.o.','','01928398713','','Čulinečka Cesta 221/e','','10000','Zagreb',52,'koturic@koturic.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (616,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','GRAFIK.NET d.o.o.','','25187588059','','Odranska 1/1','','10000','Zagreb',52,'mirko@grafiknet.hr','+385-99-3097-836','','','','','','','');
INSERT INTO `contacts` VALUES (617,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','NEBESKI BALONI d.o.o.','','18577584563','','Savska cesta 50','','10000','Zagreb',52,'info@nebeskibaloni.hr','','+385-91-6930307','','','','','','');
INSERT INTO `contacts` VALUES (624,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Krešimir Gagulić','','','','Kupac građanin','','','',52,'kresimir.gagulic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (625,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MIROSLAV VIDOVIĆ','','','','','','','',52,'info@morris-studio.com','','','','','','','','');
INSERT INTO `contacts` VALUES (626,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','RHINO PRODUKCIJA d.o.o.','','40826829003','','Brodska 7','','10000','Zagreb',52,'mladen@rhino.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (632,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Darko Brajdić','','','','Kupac građanin','','','',52,'sandra@brajdic.de','','','','','','','','');
INSERT INTO `contacts` VALUES (634,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','ELLABO d.o.o.','','','','Kamenarka 24','','10000','Zagreb',52,'andrea.simic@ellabo.hr','','+385 1 6611115','','','','','','');
INSERT INTO `contacts` VALUES (635,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Zlata Jauk','','','','Kupac građanin','','','',52,'zlata.jauk2@h-1.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (636,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','PRECI','','','','','','','',52,'precibh@gmail.cim','','','','','','','','');
INSERT INTO `contacts` VALUES (637,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','TEHNOMEDIKA doo','','','','','','','',52,'zoran.krtinic@tehnomedika.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (638,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','TADEJA KETEKOZARY','','','','','','','',52,'tadeja.ketekozary@hep.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (639,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Bruno Lalić','','','','','','','',52,'bruno.lalic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (640,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Jelena Šerić','','','','Kupac građanin','','','',52,'','','','','','','','','');
INSERT INTO `contacts` VALUES (641,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','ZAKLADANI STAVEB a.s.','','66304376486','','II Cvjetno Naselje 19','','10000','Zagreb',52,'zakladani@zakladani.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (642,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Denis Bajraktarević','','','','Kupac građanin','','','',52,'dinopromet@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (643,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','SUNCE ZA VAS d.o.o.','','84185112238','','Šubičeva 64','','10000','Zagreb',52,'www-sun-for-you@zg.t-com.hr','','+385-98-754142','','','','','','');
INSERT INTO `contacts` VALUES (644,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','EKO LIBERTAS','','','','','','','',52,'d.simovic3@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (645,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','Blitz film i video distribucija d.o.o.','','69856063967','','Kamenarka 1','','10000','Zagreb',52,'maja.pavlinusic@blitz.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (646,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MAGURO d.o.o.','','','','','','','',52,'sasa@maguro-fishing.com','','','','','','','','');
INSERT INTO `contacts` VALUES (661,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Marina Pavičić','','','','Kupac građanin','','','',52,'marinap@phy.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (662,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','ETRANET GRUPA d.o.o.','','36044044039','','Ulica Frana Folnegovića 1B,','','10000','Zagreb',52,'marin.ljoljic@etranet.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (663,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Darka Zimić','','','','Kupac građanin','','','',52,'dzimic@hotmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (665,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','BLITZ FILM','','','','','','','',52,'maja.pavlinusic@blitz.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (670,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','COBRA SISAK d.o.o.','','45195887063','','I. Gundulića 30','','44000','Sisak',52,'iv.volaric@gmail.com','','+385-91-5543580','','','','','','');
INSERT INTO `contacts` VALUES (671,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','UNICEF','','18282095247','','Radnička cesta 41','','10000','Zagreb',52,'dgorski@unicef.org','','+385-1-2329 606','','','','','','');
INSERT INTO `contacts` VALUES (672,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','LJEPOTICA d.o.o.','','','','Jakova Gotovca 17/1','','10360','Sesvete',52,'komercijala.ljepotica@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (675,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','Ured UNICEF-a u RH','','18282095247','','Radnička cesta 41/VII','','10000','Zagreb',52,'dgorski@unicef.org','','+385-1-2329 606','','','','','','');
INSERT INTO `contacts` VALUES (676,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','URBANA GALERIJA ','','','','','','','',52,'marijana@urbana-galerija.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (677,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','VODOPRIVREDA ZAGREB d.d.','','','','','','','',52,'vodoprivreda02@vzg.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (678,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','VERGL','','','','','','','',52,'vergl@vergl.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (679,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','OPG Milica Kuzmić','','16076556994','','Negrijeva 4','','52100','Pula',52,'milan.kuzmic@pu.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (680,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Filip Buzuk','','','','Kupac građanin','','','',52,'filip.buzuk@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (684,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','TRGOVAČKO USLUŽNI OBRT WOX Vl.Božo Medić','','20256467728','','ZAGREBAČKA 13','','10410','Velika Gorica',52,'wox2@net.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (685,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Janković','','','','','','','',52,'jankovic@sfzg.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (686,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Nino Barčanac','','','','Kupac građanin','','','',52,'barcanac@fkit.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (687,'0000-00-00 00:00:00','0000-00-00 00:00:00','','',' Ivan Greganić','','','','Kupac građanin','','','',52,'ivan.greganic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (693,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SELECTI AREA d.o.o.','','','','','','','',52,'tomasic.mario@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (694,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','FERROSTIL MONT d.o.o.','','','','','','','',52,'boris.sajko@ferrostilmont.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (695,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','AEROTEH d.o.o.','','','','','','','',52,'josipa.tausan@aeroteh.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (702,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','BOR TRANS vl.Marijana Boršić','','30659765058','','Grletinec 47/2','','49231','Hum na Sutli',52,'ivica@bortrans.hr','','+385 (91) 340 5840','','','','','','');
INSERT INTO `contacts` VALUES (705,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','BEVEL d.o.o.','','60271794480','','Narodnog oslobođenja 9','','51306','Čabar',52,'info@bevel.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (706,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','MANIFEST MEDIA','','','','Hebrangova 11','','10000','Zagreb',52,'dario@manifest.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (707,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Maja Tolić','','','','Kupac građanin','','','',52,'mayacr7@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (708,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','UABHDR','','','','Kneza Mutimira 5','','10000','Zagreb',52,'uabhdr@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (710,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MULTI CONNECTO d.o.o.','','','','','','','',52,'igor@multiconnecto.com','','','','','','','','');
INSERT INTO `contacts` VALUES (712,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','WHITE PEARL d.o.o.','','99388954115','','Trepčanka 21','','10040','Zagreb',52,'ured.visnjan@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (713,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','ESTARE CULTO d.o.o.','','91151135167','','Folnegovićeva 1C','','10000','Zagreb',52,'sasakondic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (714,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Dijana Martine','','','','Kupac građanin','','','',52,'dijana55@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (715,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Gordan Barić','','','','','','','',52,'baricgordan@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (716,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ON-AIR sistemi d.o.o.','','','','','','','',52,'miroslav.jeras@broadstream.com','','','','','','','','');
INSERT INTO `contacts` VALUES (719,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','STAR PRODUKCIJA d.o.o.','','89276507815','','Čire Truhelke 21','','10000','Zagreb',52,'elfpecunia@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (720,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Velimir Kukas','','','','Kupac građanin','','','',52,'velimirkukas@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (721,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','MIRIAM','','','','Kotrmanova 9','','10410','Velika Gorica',52,'info@miriam-dg.com','','','','','','','','');
INSERT INTO `contacts` VALUES (722,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','METAMORFOZA d.o.o.','','24880192958','','Bukovačka cesta 174','','10000','Zagreb',52,'vedran@metamorfoza.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (723,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','OK VIHOR','','','','','','','',52,'vihor@vihor.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (727,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','LIVADA PRODUKCIJA d.o.o.','','65708682409','','Zavrtnica 17','','10000','Zagreb',52,'toni@livada-produkcija.com','','','','','','','','');
INSERT INTO `contacts` VALUES (728,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','SENNA d.o.o.','','60437320326','','Negovečka 6','','10000','Zagreb',52,'sennam@gmail.com','','+385-91-2011056','','','','','','');
INSERT INTO `contacts` VALUES (729,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','MAJO COMMERCE d.o.o.','','81876879825','','Tina Ujevića 13','','10000','Zagreb',52,'majo-commerce@xnet.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (730,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','SPORT CONCEPT d.o.o.','','','','Ravnica b.b.','','10000','Zagreb',52,'ana@sportconcept.hr','','','','','2015118 - traže dostavni auto za Begrad. Zato je putnički tako jeftin','','','');
INSERT INTO `contacts` VALUES (731,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','MIRIAM LOGISTIKA d.o.o.','','86060931675','','Kotrmanova 9','','10410','Velika Gorica',52,'info@miriam-dg.com','','','','','','','','');
INSERT INTO `contacts` VALUES (733,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Siniša Bajšić','','','','Kupac građanin','','','',52,'sbajsic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (734,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GROMEL d.o.o.','','','','','','','',52,'jlackovic@gromel.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (738,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ENIKON PROJEKTIRANJE','','','','','','','',52,'boris.petrovic@enikon.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (739,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GAM INTERIJER d.o.o.','','','','','','','',52,'ognjen@gam-interijer.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (740,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ENDORA d.o.o.','','','','','','','',52,'goran@endora.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (741,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','NE-KOM OBRT','','92629689944','','Braće Ribara 33','','10437','Bestovje',52,'neno2905@net.hr','','','','','u 4mj popravak RI-645-UP - u Vodicama - korisnik platio račun  u 5.mj 1250 umanjili račin za 1250kuna  u 4mj ri-815-ua - sajba oštećena','','','');
INSERT INTO `contacts` VALUES (742,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Miljenko Žeger','','','','Kupac građanin','','','',52,'mzeger@net.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (743,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Josip Saraga','','','','Kupac građanin','','','',52,'josip.saraga@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (744,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Krešimir Burnać','','','','Kupac građanin','','','',52,'k.burnac@hotmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (745,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hlupić','','','','','','','',52,'','','','','','','','','');
INSERT INTO `contacts` VALUES (746,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MARTINGRAD d.o.o.','','','','','','','',52,'martin.grad@optinet.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (754,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Boris Vižintin','','','','Kupac građanin','','','',52,'k.burnac@hotmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (755,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Damon Ronald Antičević','','','','','','','',52,'zeljkaanticevic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (756,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KLAROM DELICIJE d.o.o.','','','','','','','',52,'klarom-delicije@zg.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (757,'0000-00-00 00:00:00','2015-12-22 09:19:41','','','TERAD d.o.o.','','22078329702','','Gojlanska ulica 41','','10040','Zagreb',52,'kresimir.karacic@terad.hr','','+385-98386297','','','','','','');
INSERT INTO `contacts` VALUES (758,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mario KESER','','','','Kupac građanin','','','',52,'mariok2306@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (759,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','Heinirch Boell Stiftung','','73690702958','','Preobraženska 2','','10000','Zagreb',52,'Maja.BucicGrabic@hr.boell.org','','','','','','','','');
INSERT INTO `contacts` VALUES (760,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','HRABRI TELEFON','','','','','','','',52,'ella@hrabritelefon.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (761,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','Mototrip d.o.o.','','81732611777','','A. Mihanovića 14','','10450','Jastrebarsko',52,'info@mototrip-tours.com','','','','','','','','');
INSERT INTO `contacts` VALUES (765,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Vatroslava Mišetić','','','','Kupac građanin','','','',52,'vatroslava.misetic@zg.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (767,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SING','','','','','','','',52,'miroslav.cukovic@sing.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (768,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Michael Dagostin','','','','','','','',52,'m.dagostin@inet.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (769,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','HRVATSKO DRUŠTVO LIKOVNIH UMJETNIKA','','','','','','','',52,'luka.hrvoj@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (772,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','DEMA Karlovac','','5465023912','','Varaždinska 1','','47000','Karlovac‐HR',52,'info@dema.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (774,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Julia Underwood','','','','','','','',52,'underwood.j@iasnikatravel.com','','','','','','','','');
INSERT INTO `contacts` VALUES (776,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','Zagrebački ronilački klub','','37596479831','','Hanamanova 1 A','','10000','Zagreb',52,'velimirkukas@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (780,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Milenium promocija d.o.o.','','','','','','','',52,'mihaela.miljkovic@mpr.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (781,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','OMP obrada metala i trgovina','','93831820829','','Gubaševo 3','','49210','Zabok',52,'plesko@plesko.hr','','+385 98 351153','','','','','','');
INSERT INTO `contacts` VALUES (782,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KUD PRLJAVAC','','','','','','','',52,'prljavac@live.com','','','','','','','','');
INSERT INTO `contacts` VALUES (783,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Martina Kontošić','','','','','','','',52,'martina.kont@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (784,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','Ivan i Marko d.o.o.','','62909566494','','Gornji Bukovac 101','','10000','Zagreb',52,'marko.kontek@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (785,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Martina Bezan','','','','','','','',52,'martina_bezan@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (786,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hrvoje Grubać','','','','Kupac građanin','','','',52,'hrvojegrubac@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (787,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','REAL CARGO CROATIA','','77758714505','','Radnička cesta 39','','10000','Zagreb',52,'tboris65@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (788,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','Tip putovanja-turistička agencija d.o.o.','','16520487009','','Vončinina 2/1','','10000','Zagreb',52,'maja@tiptours.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (789,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','DRUGI PLAN D.O.O.','','','','Heinzelova 62a','','10000','Zagreb',52,'sanja.nikic@gmail.com','','+385 99 3563 272','','','','','','');
INSERT INTO `contacts` VALUES (794,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ANTE SKORUP','','','','','','','',52,'anteskorup@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (795,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','APPLICON D.O.O','','76817252728','','Prisavlje 2','','10000','Zagreb',52,'anteb@pandopad.com','','','','','','','','');
INSERT INTO `contacts` VALUES (796,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Savez društava multiple skleroze Hrvatske','','','','','','','',52,'sdms_hrvatske@sdmsh.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (797,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DRAGICA BIKIĆ','','','','','','','',52,'dbgagi@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (798,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KRISTIAN KRALJ','','','','','','','',52,'kristiankralj@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (799,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','HROK D.O.O.','','','','','','','',52,'ured@hrok.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (800,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','ZIT d.o.o','','58721577676','','Rakitnica 2','','10000','Zagreb',52,'mario.radujkovic@zit-zg.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (801,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Krunoslav Milatović','','','','','','','',52,'kruno.milatovic@stipic.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (802,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','SALOMON d.o.o.','','94022363843','',' Rudolfa Bičanića 28','','10000','Zagreb',52,'denis@falizmaj.org','','','','','','','','');
INSERT INTO `contacts` VALUES (803,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','Zaklada \"Hrvatska kuća srca\"','','','','Ilica 5/II','','10000','Zagreb',52,'tea.lovric@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (804,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Daniel Fekter','','','','Kupac građanin','','','',52,'daniel.fekter@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (805,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Marko Planinić','','','','-kupac građanin','','','',52,'marinap@phy.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (809,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Kristan Herwing Kralj','','','','-kupac građanin','','','',52,'kristiankralj@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (816,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ŽELJKO ČAVLEK','','','','','','','',52,'zeljko.cavlek@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (817,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivo Žinić','','','','','','','',52,'isostari@zsem.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (818,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ilija Mikušić','','','','Kupac građanin','','','',52,'imisukic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (819,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','PRVO PLINARSKO DRUŠTVO d.o.o.','','58292277611','','Kardinala Alojzija Stepinca 27','','10000','Zagreb',52,'antonija.glavas@ppd.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (820,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MIRTA PRODUKCIJA','','','','','','','',52,'mirta.jusic.d@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (821,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Test za ponude','','','','','','','',52,'testzaponude@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (822,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ADVENA d.o.o.','','','','','','','',52,'advena@advena.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (827,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Matija Gikić','','','','','','','',52,'matija@gikic.com','','','','','','','','');
INSERT INTO `contacts` VALUES (828,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Lidija Vrbanek','','','','Kupac građanin','','','',52,'lidija.vrbanek@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (829,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','CHAMPSTAT OBRT','','','','','','','',52,'karlo.sukalic@gmail.com','','','','','Dobio 2 sata gratis 21.03.2015 jer je ranije vratio kombi najam prije.','','','');
INSERT INTO `contacts` VALUES (830,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','Hyperborea d.o.o.','','87800945941','','Stepinčeva 38','','21311','Stobreč',52,'hyperborea.hr@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (831,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','APIN SUSTAVI d.o.o.','','22659472331','','Ožujska 8','','10000','Zagreb',52,'goran.krizanac@apin.hr','','','','','0','','','');
INSERT INTO `contacts` VALUES (832,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Maja Komljenović','','','','Kupac građanin','','','',52,'maja_komljenovic@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (833,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','IVAN FILIPČIĆ','','','','','','','',52,'ivanfilipcic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (834,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Danijel Slišković','','','','','','','',52,'danijel.sliskovic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (835,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Krešimir Terzić','','','','Kupac građanin','','','',52,'kresimir.terzic@genera.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (841,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mrakec','','','','','','','',52,'mrakec@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (846,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Nora Šoda','','','','-kupac građanin','','','',52,'danijel.sliskovic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (847,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','SIGET – DIZALA d.o.o.','','19841995263','','Prečko 61','','10000','Zagreb',52,'tomislav.boskovic@hotmail.com','','+38598 9812760','','','','','','');
INSERT INTO `contacts` VALUES (848,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Edison Krammer','','','','Kupac građanin','','','',52,'edison.krammer@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (850,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Sarfi Elvis','','','','Kupac građanin','','','',52,'elvis.sarfi@strabag.com','','','','','','','','');
INSERT INTO `contacts` VALUES (851,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ana Vučković','','','','Kupac građanin','','','',52,'ekipavuckici@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (852,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Branumir Borojević','','','','Kupac građanin','','','',52,'etsongo@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (853,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DENTAL PRACTICE ŠVAJHLER','','','','','','','',52,'ivan.svajhler@dent-med.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (855,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Oskar Rugas','','','','Kupac građanin','','','',52,'orugas@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (856,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','HNS','','','','','','','',52,'helena.puskar@hns-cff.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (857,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KROKO TRADE','','','','','','','',52,'krokotradeeu@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (858,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Antonio Skorjanec','','','','Kupac građanin','','','',52,'askorjanec@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (866,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hrvoje Bartolić','','','','','','','',52,'zglogistic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (867,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','MAPEI d.o.o.','','89471789472','','Purgarija 14, Kerestinec','','10431','Sveta Nedelja',52,'majda.lukic@mapei.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (868,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','INSTITUT TRADICIONALNOG TAEKWON-DOA','','80363051470','','Ivana Gundulića 3','','35000','Slavonski Brod',52,'itf.croatia@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (870,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','CHAMPIONCHIP CROATIA - CHAMPSTAT','','26912562962','','Matije Skurjenija 153','','10290','Zaprešić',52,'karlo.sukalic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (876,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DiArt d.o.o.','','','','','','','',52,'ivica.diart@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (877,'0000-00-00 00:00:00','2015-12-22 09:14:17','','','KOMIS d.o.o.','','','',' Pavla bb','','',' 21212 Kaštel Sućurac',52,'ivan@komis.hr','','+385-91-531-6070','','','','','','');
INSERT INTO `contacts` VALUES (878,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mladen Šimić','','','','','','','',52,'mladensimic@ymail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (879,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Krešimir Vodopivec','','','','','','','',52,'kresimir.vodopivec@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (880,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','AKADEMIJA DRAMSKIH UMJETNOSTI','','','','','','','',52,'dapetkovic@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (881,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Odrčić','','','','','','','',52,'ajodorcic@live.com','','','','','','','','');
INSERT INTO `contacts` VALUES (882,'0000-00-00 00:00:00','2015-12-22 09:14:17','','','POS SERVISI d.o.o.','','30421345069','','Čista velika I 82','','','Čista velika',52,'bruno.brckovic@pos-servis.eu','','+385 91 4456450','','','2014114 - 01.04.2015 - ima jednu paletu tonera, a mi nemamo teretnih kombija','','','');
INSERT INTO `contacts` VALUES (883,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ninoslav Andrašević','','','','Kupac građanin','','','',52,'ninoslav.andrasevic1@zg.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (887,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Miroslav Zemljić','','','','','','','',52,'miroslav@bakina-kuca.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (888,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','Hrvatsko etnološko društvo','','','',' Ivana Lučića 3','','10000','Zagreb',52,'zorany@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (889,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Via Tours','','','','','','','',52,'i.merkas@via-tours.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (890,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Kosta Uremović','','','','','','','',52,'kostaurumovic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (893,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','M San Grupa d.d.','','','','Buzinski prilaz','','10000','Zagreb',52,'zlatan.macan@msan.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (894,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ZMRP','','','','','','','',52,'jjuracak@agr.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (895,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ŽENE U DOMOVINSKO RATU','','','','','','','',52,'ruzica.posao@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (896,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','MMP - AUTOMATSKA VRATA d.o.o. ','','49261752334','','Božidara Kunca 1/IV','','10000','Zagreb',52,'info@mmp-automatskavrata.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (897,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivan Ketović','','','','Kupac građanin','','','',52,'','','','','','','','','');
INSERT INTO `contacts` VALUES (898,'0000-00-00 00:00:00','2015-12-22 09:14:17','','','POS-SERVIS d.o.o.','','30421345069','','Čista Velika 1-82','','',' 22214 Čista Velika',52,'bruno.brckovic@pos-servis.eu','','','','','','','','');
INSERT INTO `contacts` VALUES (905,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Vlatko Brčić','','','','Kupac građanin','','','',52,'vlatko.brcic@hgi-cgs.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (906,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Miroslav Bučanac','','','','','','','',52,'mbucanac@net.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (907,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Tomislav Kralj','','','','','','','',52,'kraljtomislav21@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (908,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ŽKK NOVI ZAGREB','','','','','','','',52,'kiki13jaga@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (909,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','Montažstroj','','19673322859','','Vojnovićeva 22','','10000','Zagreb',52,'petra.prsa@montazstroj.hr','','','','','2015170 - 30% - u najavi Berlin, Rijeka, Crna gora..','','','');
INSERT INTO `contacts` VALUES (910,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Jurica Vincelj','','','','','','','',52,'juvincelj@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (911,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SALONA d.o.o.','','','','','','','',52,'r.renata97@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (912,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Tomislav Šumski','','','','','','','',52,'tsumski.vt@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (913,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivan Banožić','','','','','','','',52,'ivan.banozic007@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (914,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Vesna Spoza','','','','','','','',52,'rpretkovic@vesnasposa.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (924,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','PUČKI PRAVOBRANITELJ','','08026537914','','Trg hrvatskih velikana 6','','10000','Zagreb',52,'jelena.music@ombudsman.hr','','+385 1 4851855','','','','','','');
INSERT INTO `contacts` VALUES (925,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','INŽINJERING MTK d.o.o.','','','','','','','',52,'mtk@mtk.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (926,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','FZOEU','','85828625994','','Radnička 80','','10000','Zagreb',52,'ojdana.ra@fzoeu.hr','','099 4382 920','','','','','','');
INSERT INTO `contacts` VALUES (928,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','ROMSKO NACIONALNO VIJEĆE','','','','Poljana J.Andrassyja 9','','10000','Zagreb',52,'beganovic1992@hotmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (929,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','PERI OPLATE I SKELE d.o.o.','','','','','','','',52,'danka.pamic@peri.com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (934,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SKIT','','','','','','','',52,'kresimir.selci@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (940,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Amatersko dramsko kazalište','','','','','','','',52,'adksv.kriz.zacretje@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (941,'0000-00-00 00:00:00','0000-00-00 00:00:00','','',' Kineziološki fakultet Zagreb','','','','','','','',52,'dodosh8@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (942,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','JAGMA d.o.o.','','','','','','','',52,'marijo.rakic@jagma.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (943,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','NICRO','','','','','','','',52,'helga.cubric@nicro.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (944,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','FREUND j.d.o.o.','','12519744471','','Kutjevačka 3','','10000','Zagreb',52,'bnmcentar@gmail.com','','+385-99-6631004','','','','','','');
INSERT INTO `contacts` VALUES (945,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','AUTO ODAK','','','','','','','',52,'odak@globalnet.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (946,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','MAJOR INTERNATIONAL d.o.o.','','49604933226','','Zagrebačka avenija 100A','','10000','Zagreb',52,'damir.hasakovic@major.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (948,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','MONTAŽSTROJ d.o.o.','','89876133223','','Vojnovićeva 22','','10000','Zagreb',52,'zdravko.popovic@montazstroj.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (950,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','TIP TOURS - PUTNIČKA AGENCIJA d.o.o.','','83011422157','','Nova Ves 69','','10000','Zagreb',52,'maja@tiptours.hr','','','','','1','','','');
INSERT INTO `contacts` VALUES (953,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KV knjigovodstvo i marketing','','','','','','','',52,'kv.knjigovodstvo@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (954,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','NICRO d.o.o.','','71179651211','','Marijana Čavića 9','','10000','Zagreb',52,'igor.matisic@nicro.hr','','+385 1 2450 009','','','','','','');
INSERT INTO `contacts` VALUES (955,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','Studio Moderna - TV Prodaja d.o.o.','','','','Zadarska 80','','10000','Zagreb',52,'ivan.buzanic@studio-moderna.com','','','','','','','','');
INSERT INTO `contacts` VALUES (968,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','OBRT KISKO','','','','','','','',52,'branko.tumpa@zg.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (973,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ENERGOMETALI E.P.','','','','','','','',52,'energometal.ep@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (980,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','Bravo putovanja d.o.o.','','92714688296','','Draškovićeva 55','','10000','Zagreb',52,'denis.cetusic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (981,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','VIT TRANSLOGISTIKA d.o.o.','','','','','','','',52,'vit-translogistika@email.ht.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (988,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','ZATON GRUPA D.O.O.','','36264929889','','POLJANA 36','','23232','Zaton',52,'','','','','','','','','');
INSERT INTO `contacts` VALUES (991,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','ČISTA VODA d.o.o.','','42375187043','','Kovinska 4a','','10000','Zagreb',52,'stefica.greblicki@clearwater.hr','','+385 1 3463 430','','','','','','');
INSERT INTO `contacts` VALUES (992,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','STOMATOLOŠKA ORDINACIJA DOKTORICE UDILJAK','','','','','','','',52,'udiljak.iva@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (993,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','ORGINAL KORČULA TRAVEL ','',' 36535173505','','Trg Sv. Justine 8 ','','20260','Korčula',52,'info@korcula-travel.com','','','','','','','','');
INSERT INTO `contacts` VALUES (995,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','AQUAMONT d.o.o.','','85058985197','','Sobolski put 16','','10000','Zagreb',52,'ante.matic@aquamont.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (999,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','24sata d.o.o. ','','78093047651','','Oreškovićeva 6H/1','','10000','Zagreb',52,'krunoslav.bagaric@24sata.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1000,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','Centar za organizaciju građenja','','51521331536','','Gračanska cesta 39','','10000','Zagreb',52,'miroslavzemlji294@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1001,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','J.Britvec','','','','','','','',52,'j.britvec@brucha.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1002,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','Teatar EXIT','','','','Ilica 208','','10000','Zagreb',52,'exit@teatarexit.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1003,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','TRAVABLED TURISTIČKA AGENCIJA J.D.O.O. ','','69431968517','','Antuna Bauera 19','','10000','Zagreb',52,'skekic7@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1004,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','CETIS d.o.o.','','','','','','','',52,'cetis@zg.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1005,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','MILENIJ HOTELI d.o.o.','','78796880101','','Ulica Viktora Cara Emina','','51000','Opatija',52,'skekic7@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1008,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','METALVAR d.o.o.','','','','','','','',52,'metalvar@metalvar.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1009,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Economic Chamber of Macedonia','','','','','','','',52,'snezana@mchamber.mk','','','','','','','','');
INSERT INTO `contacts` VALUES (1010,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','CENTROTRANS','','','','','','','',52,'d.tolic@centrotrans.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1011,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','TRIPLEJUMP d.o.o.','','35875278809','','Selska 34','','10000','Zagreb',52,'dalibor.fustar@triple-jump.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1017,'0000-00-00 00:00:00','2015-12-22 09:19:42','','','KLAPŠEC INTERIJERI d.o.o.','','46070195460','','Žuti breg 119','','10000','Zagreb',52,'klapsecinterijeri@gmail.com','','+385 91 2801801','','','','','','');
INSERT INTO `contacts` VALUES (1018,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','OMNIS POWER j.d.o.o.','','','','','','','',52,'poweromnis@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1029,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','TRGOVAČKA ŠKOLA','','56064164023','','Trg J. F. Kennedyja br. 4','','10000','Zagreb',52,'visnja.birus@trgovacka-skola.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1030,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SEUL RESTORAN','','','','','','','',52,'seul.restoran@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1032,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MAK DIZAJN I SAVJETOVANJA','','','','','','','',52,'mak.dizajn.savjet@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1037,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Odvjetnik Nataša Tatarić','','34528292763','','Donji Prčac 23','','10000','Zagreb',52,'odvjetnik.natasa.tataric@zg.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1038,'0000-00-00 00:00:00','2015-12-22 09:19:43','','',' Nacionalni centar za vanjsko vrednovanje obrazovanja ','','','','Petračićeva 4','','10000','Zagreb',52,'vesna.vukovic@ncvvo.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1039,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Stype CS d.o.o ','','','','Velikopoljska 32','','10010','Zagreb',52,'np@stypegrip.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1040,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','AG,Matas d.o.o','','','','3.Cvjetno naselje 23','','10000','Zagreb',52,'agmatas1@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1041,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','VIVAT FINA VINA d.o.o.','','','','Frana Folnegovića 1b','','10000','Zagreb',52,'morana.petricevic@vivat-finavina.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1042,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GALERIJA PRICA','','','','','','','',52,'martina_kalle@net.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1046,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','PIK Vinkovci d.d.','','','','Matije Gupca 130','','32100','Vinkovci',52,'','','','','','','','','');
INSERT INTO `contacts` VALUES (1047,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','EUROINSPEKT CROATIAKONTROLA d.o.o.','','50024748563','','Karlovačka cesta 4L','','10000','Zagreb',52,'info@croatiakontrola.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1048,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hsk-Adriatic d.o.o.','','19436019856','','Rusevje 11 ','','','',52,'sasa.popovic@hsk-adriatic.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1049,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Goethe-Institut Kroatien','','','','Ulica grada Vukovara 64','','10000','Zagreb',52,'doroteja.jakovic@zagreb.goethe.org','','','','','','','','');
INSERT INTO `contacts` VALUES (1058,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','NATURPRODUKT d.o.o.','','','','','','','',52,'robert.milek@naturprodukt.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1059,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','TE-MARINE d.o.o.','','58003854845','','Šoštarićeva 4','','10000','Zagreb',52,'info@temarin.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1060,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Ustanova Zoološki vrt grada Zagreba ','','','','Maksimirski perivoj bb','','10000','Zagreb',52,'andrea@zoo.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1061,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Intercars d.o.o','','','','','','','',52,'kristijan.pajur@intercars.eu','','','','','','','','');
INSERT INTO `contacts` VALUES (1062,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','M.T.F. d.o.o.','','','','Hondlova 2/2','','10000','Zagreb',52,'m.mestric@mtf.hr','','','','','tri vozila budmipešta','','','');
INSERT INTO `contacts` VALUES (1063,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','HUDEK ZAGREB d.o.o.','','43013193376','','Sunekova 145','','10000','Zagreb',52,'hudek@hudek.hr','','','','','-popust 10.03 je zato jer je kombi 2 dana vani ne tri cijela.uzima utorak 20 sati/povrat petak u 8.00','','','');
INSERT INTO `contacts` VALUES (1067,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','KATAPULT d.o.o','','48566967897','',' Ulica Gordana Lederera 4 ','','10000','Zagreb',52,'kresimir@SHOOSTER.HR','','','','','','','','');
INSERT INTO `contacts` VALUES (1068,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SRETNO PAŠKO SUNCE d.o.o.','','','','','','','',52,'sretno-pasko-sunce@hotmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1069,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Sindikat PPDIV','','','','','','','',52,'ivan.klakocer@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1070,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Media net d.o.o.','',' 60259436947','','Petrinjska 81','','10000','Zagreb',52,'ivana.rudelic@presscut.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1071,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Krka-farma d.o.o.','','','','Radnička cesta 48/II','','10000','Zgareb',52,'darija.tabulov@krka.biz','','','','','','','','');
INSERT INTO `contacts` VALUES (1079,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Fox International Channels','','','','','','','',52,'Milena.Marinic@fox.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1080,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','TURISTICKA AGENCIJA OMEGA GRUPA','','','','Tomislavova 11/2','','10000','Zagreb',52,'boris@omega-grupa.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1081,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','RAČAN ELEKTRONIKA d.o.o.','','52795324928','','Klekova 9','','10000','Zagreb',52,'velimir.racan@zg.t-com.hr','','+385 98 282894','','','23.03. - ide u Dubrovnik','','','');
INSERT INTO `contacts` VALUES (1082,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','AS Sesvete-Bus d.o.o.','','38774034343','','Ljudevita Posavskog 3','','10360','Sesvete',52,'as.sesvetebus@gmail.com','','','','','16.04- put u Beograd i locco Beograd sa turistima','','','');
INSERT INTO `contacts` VALUES (1083,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','REHAU d.o.o.','','','','Franje Lučića 34','','10000','Zagreb',52,'stanislava.gasparevic-relic@rehau.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1084,'0000-00-00 00:00:00','2015-12-22 09:14:18','','','VITKOVIĆ PROM d.o.o.','','50705099618','','Bocakova ul. 48','','','10380, Sveti Ivan Zelina',52,'nevitkovic@net.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1086,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Smart industrial services d.o.o.','','','','','','','',52,'marko.zubcic@smartservices.hr','','','','','6.4 - nizozemska radnici','','','');
INSERT INTO `contacts` VALUES (1087,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','MG design d.o.o.','',' 30833969357','','Poduzetnička cesta b.b.','','10314','Križ',52,'kr.kamenscak@mgdesign.hr','','','','','2015111 - 27.03-putuje u Banja Luku','','','');
INSERT INTO `contacts` VALUES (1093,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','NOVOGRADNJA ZAGORKA','','','','','','','',52,'tomasic@novogradnja-zagorka.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1097,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','CUNAE j.d.o.o.','','44676497343','','R. Cimermana 68','','10000','Zagreb',52,'info@noona.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1098,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','DB TRANS d.o.o.','','25245842783','','Stjepana Fabijančića 135','','10410','Velika Gorica',52,'info@vg-dbtrans.eu','','+385-91-1556666','','','2015120 - LJUBLJANA','','','');
INSERT INTO `contacts` VALUES (1099,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Abrakadabra  integrirane komunikacije d.o.o.','','','','Metalčeva 5/VIII','','10000','Zagreb',52,'nina.remenar@abrakadabra.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1102,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','NORINGRADNJA','','','','','','','',52,'noringradnja@gmail.com','','','','','2015122 - Plitvice','','','');
INSERT INTO `contacts` VALUES (1105,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Mirko Hanzlić','','01628828415','','J.J.Strossmayera 13a','','31500','Našice',52,'','','','','','','','','');
INSERT INTO `contacts` VALUES (1107,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ATALIAN d.o.o.','','','','','','','',52,'ivanka.cevak@atalian.hr','','','','','2015123 - 3 vozila- Rijeka- jednokratno 30% popusta','','','');
INSERT INTO `contacts` VALUES (1108,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','FAKULTET ELEKTROTEHNIKE I. RAČUNARSTVA','','57029260362','','Unska 3','','10000','Zagreb',52,'kristian.jambrosic@fer.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1109,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ANITA BOŠNJAK','','','','','','','',52,'anita.bosnjak1001@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1110,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','LIBERTAS','','','','','','','',52,'snjezana.baban@os.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1111,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ELKOM d.o.o.','','','','','','','',52,'elkom@bj.t-com.hr','','','','','-jednokratan poposi 23% - mali km','','','');
INSERT INTO `contacts` VALUES (1112,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ŽUPANISKO DRŽAVNO ODVJETNIŠTVO','','','','','','','',52,'vlatka.durak@zdozg.dorh.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1119,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','HRVATSKI SABOR KULTURE','','45263394181','','Ulica kralja Zvonimira 17','','10000','Zagreb',52,'kazaliste@hrsk.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1120,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MAsaprint d.o.o.','','','','','','','',52,'josipa@masaprint.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1121,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','LAVITO usluge d.o.o. ','','96202705185','','Maksimirska 19','','10000','Zagreb',52,'kristina.juricic@ad-promet.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1123,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DOMOTEH d.o.o.','','','','','','','',52,'darko.tomljenovic@domoteh.hr','','','','','2014143 - zagreb - max 150km','','','');
INSERT INTO `contacts` VALUES (1124,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','CONTRES PROJEKTI D.O.O. ','','','','','','','',52,'marina@contres.hr','','','','','2014144 - Bled-rezervacija 3 tjedan prije najma','','','');
INSERT INTO `contacts` VALUES (1125,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Akademija za politički razvoj','','','','Makančeva 16','','10000','Zagreb',52,'jakov.zizic@politicka-akademija.org','','','','','','','','');
INSERT INTO `contacts` VALUES (1126,'0000-00-00 00:00:00','2015-12-22 09:14:18','','','KUPOLE - bolje od šatora d.o.o.','','77581043464','',' Lučko 47 B','','','Lučko',52,'mia@kupole.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1132,'0000-00-00 00:00:00','2015-12-22 09:14:18','','','AUTO BENUSSI d.o.o.','','96262119913','',' Industrijska ulica 2/D','','','Pula',52,'','','','','','','','','');
INSERT INTO `contacts` VALUES (1133,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Triumph International d.o.o.','','','','','','','',52,'hrvojeselinger@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1134,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MEP RIJEKA D.O.O.','','','','','','','',52,'ante@mep.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1137,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','BIZZ putovanja d.o.o.','','','','II Ravnice 1A','','10000','Zagreb',52,'tamara@bizztravel.biz','',' +385 1 4111 522','','','','','','');
INSERT INTO `contacts` VALUES (1138,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Terme Tuhelj d.o.o.','','56566580479','','Ljudevita Gaja 4','','49215','Tuhelj',52,'sanja.popovic@terme-tuhelj.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1139,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Electus DGS d.o.o.','','','','Hondlova 2/11','','10000','Zagreb',52,'ana.horvat@electus.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1140,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','STATIPROM d.o.o.','','','','','','','',52,'statiprom@email.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1141,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','OPG MRAOVIĆ','','','','','','','',52,'kontakt@staridud.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1142,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KUNEŠTRA, obrt za trgovinu, vl. Nikolina Vrtodušić','','91120329117','',' Istarska 18','','','Novalja',52,'kunestra@gmail.com','','','','','2 kombija na 3 mjeseca ','','','');
INSERT INTO `contacts` VALUES (1143,'0000-00-00 00:00:00','2015-12-23 10:11:12','','',' AS INTERIJERI d.o.o.','','60694690625','','Resnički put 55','','10000','Zagreb',52,'as.interijeri@zg.t-com.hr','','+385-91-4405070','','','2015163 - Rijeka - 2 kombija','','sdfsdfsdf','234234234');
INSERT INTO `contacts` VALUES (1144,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','INSTITUT IGH d.d.','','79766124714','','Janka Rakuše 1','','10000','Zagreb',52,'ivan.mavar@igh.hr','','+385-98-418748','','','2015164 - TIRANA  2015172 - TIRANA PRODUŽETEK  2015191 - ZAGRBE LOCCO /MAX 200KM DAN','','','');
INSERT INTO `contacts` VALUES (1146,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Veleposlanstvo Japana','','65169155509','','Boškovićeva 2','','10000','Zagreb',52,'prvonozac@zr.mofa.go.jp','','','','','','','','');
INSERT INTO `contacts` VALUES (1147,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ME TE ME d.o.o.','','','','','','','',52,'ana@meetme.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1148,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','DGS Ltd. Maritime Crewing Agency','','','','Milutina Baraca 7','','51000','Rijeka',52,'filip@dgs.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1149,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KFK TEHNIKA d.o.o.','','','','','','','',52,'andrea.bartolic@kfk.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1150,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','BIO NATURAL MED','','','','','','','',52,'bnmcentar@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1152,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','ALCON FARMACEUTIKA d.o.o','','97660092353','','Avenija Dubrovnik 16','','10000','Zagreb',52,'andres.arandia-kresic@alcon.com','','+385-91-4841426','','','','','','');
INSERT INTO `contacts` VALUES (1158,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','DGS d.o.o.','','96250701832','','Milutina Barača 7','','51000','Rijeka',52,'filip@dgs.hr','','','','','1','','','');
INSERT INTO `contacts` VALUES (1160,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Amali Doare Production','','','','Stare Gajnice 3','','10000','Zagreb',52,'uprava@topdestinacije.hr','','+385 95 8042 320','','','','','','');
INSERT INTO `contacts` VALUES (1170,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','TEMPERO VERO','','','','','','','',52,'tempero.vero@gmail.com','','','','','slovenija','','','');
INSERT INTO `contacts` VALUES (1171,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Fond za financiranje razgradnje NEK','','','','Radnička cesta 47','','10000','Zagreb',52,'sanja.miscevic@fond-nek.hr','','099/2717-174','','','','','','');
INSERT INTO `contacts` VALUES (1172,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Entrio tehnologije d.o.o.','','30513194761','','Ul.grada Vukovara 237A / I','','10000','Zagreb',52,'barbara@entrio.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1177,'0000-00-00 00:00:00','2015-12-22 09:20:02','','','BAUERELEKTRO d.o.o.','','32896907245','','Nikole Tesle 111','','10410','Velika Gorica',52,'bauelektro@bauelektro.hr','','+385-1-6251-826','','','','','','');
INSERT INTO `contacts` VALUES (1182,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','EXPLANTA d.o.o.','','','','Kralja Tomislava 41/A','','10312','Kloštar Ivanić',52,'info@explanta.hr','','+385 1 2892 645','','','2015205 - Pula','','','');
INSERT INTO `contacts` VALUES (1183,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','SUIDUM j.d.o.o.','','04179791202','','Dragutina Domjanića 25','','10410','Velika Gorica',52,'info@mondays.hr','','','','','2015206 - Berlin-Zagreb','','','');
INSERT INTO `contacts` VALUES (1184,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Muzej za umjetnost i obrt','','','','','','','',52,'miroslav.gasparovic@muo.hr','','','','','2015207-Rim','','','');
INSERT INTO `contacts` VALUES (1186,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Adriatic Escape','','','','','','','',52,'maja@adriaticescape.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1187,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','MAJOR INTERNACIONAL d.o.o.','','49604933226','','Zagrebačka avenija 100a','','10090','Zagreb',52,'nives.futac@major.hr','','+385 99 3177249','','','2015211 - Čakovec max 300km','','','');
INSERT INTO `contacts` VALUES (1193,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Državni zavod za statistiku','','','','Branimirova 19','','10000','Zagreb',52,'brumnica@dzs.hr','','','','','2015220 - Beć','','','');
INSERT INTO `contacts` VALUES (1194,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MAKRO EKO','','','','','','','',52,'info@makroeko.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1196,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','ĐURĐEVIĆ d.o.o.','','96560811477','','Brače Domany 8','','10000','Zagreb',52,'durdevic@zg.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1202,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','Veleposlanstvo Islamske Republike Iran u Zagrebu','','00960251459','','Pantovčak 125c','','10000','Zagreb',52,'vbnmgz@vbnmgz.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1203,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','AQUILONIS','','','','','','','',52,'vesna.zivkovic@aquilonis.hr','','','','','2015228 - najam sa vozačem Zagreb-Čnomelj','','','');
INSERT INTO `contacts` VALUES (1204,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hen-Mar Proces d.o.o.','','','','','','','',52,'hen-mar@hen-mar.hr','','','','','2015232 - Banja luka','','','');
INSERT INTO `contacts` VALUES (1210,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','AQUILONIS D.O.O.','',' 62288271688','','Vankina 14','','10000','Zagreb',52,'vesna.zivkovic@aquilonis.hr','','','','','2015228 - najam sa vozačem Zagreb-Čnomelj','','','');
INSERT INTO `contacts` VALUES (1211,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Genesis d.o.o.','','','','','','','',52,'goran.kezic@genesis.hr','','','','','2015234 - Banja Luka','','','');
INSERT INTO `contacts` VALUES (1217,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','PRIMALAB d.o.o.','','83028109264','','Matije Gupca 12a','','49210','Zabok',52,'marko.posavec@primalab.hr','','+385 99 807 3292','','','2015249 - Zabok selidba','','','');
INSERT INTO `contacts` VALUES (1223,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','FAKULTET ELEKTROTEHNIKE I RAČUNARSTVA','','57029260362','','Unska 3','','10000','Zagreb',52,'kristian.jambrosic@fer.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1224,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Branko Boras','','','','','','','',52,'branko.boras@konzum.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1225,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DECATHLON','','','','','','','',52,'antun.petrusa@decathlon.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1232,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GENOS','','','','','','','',52,'glauc@genos.hr','','','','','2015265-Zagreb-Rijeka -Zaostrog','','','');
INSERT INTO `contacts` VALUES (1233,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MONOTIP D.O.O','','','','','','','',52,'veselina.harabajs@monotip.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1234,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','COLLERS GRUPA d.o.o.','','59683724378','','Gustava Krkleca 6','','10090','Zagreb',52,'topfishing.zagreb@gmail.com','','+385-95-5755977','','','','','','');
INSERT INTO `contacts` VALUES (1235,'0000-00-00 00:00:00','2015-12-22 09:19:43','','','INTEGRA d.o.o.','','13405669320','','Trg kralja Tomislava 4','','42000','Varaždin',52,'zagreb@ivora.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1243,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','SOLLERS GRUPA d.o.o.','','59683724378','','Gustava Krkleca 6','','10090','Zagreb',52,'topfishing.zagreb@gmail.com','','+385-95-5755977','','','','','','');
INSERT INTO `contacts` VALUES (1245,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','BroadStream Solutions d.o.o.','','01777360796','','Horvatova 82','','10010','Zagreb',52,'miroslav.jeras@broadstream.com','','+385 98 265 059','','','','','','');
INSERT INTO `contacts` VALUES (1246,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','STYPE CS d.o.o.','','','','','','','',52,'robert.r@stypegrip.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1247,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','INDURESCO j.d.o.o.','','74454322085','','Prisavlje 2','','10000','Zagreb',52,'gallery.tempera@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1248,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','Specijalistička ordinacija obiteljske medicine Vesna Bajer ','','92437420910','','Trg Antuna Mihanovića 1','','42223','Varaždinske toplice',52,'viktor.domislovic@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1249,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DENT OPREMA d.o.o.','','','','','','','',52,'katica.spehar@zg.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1257,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SINMAR d.o.o.','','','','','','','',52,'info@asap.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1258,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Davor Ivanov','','','','','','','',52,'davor.ivanov83@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1259,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','int','','','','','','','',52,'','','','','','','','','');
INSERT INTO `contacts` VALUES (1260,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Kazalište Oberon','','','','','','','',52,'damirmadaric@hotmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1261,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','HORIZONT film j.d.o.o.','','30909739307','','Nad tunelom 9','','10000','Zagreb',52,'branko@horizont-production.eu','','+385 98 250 255','','','','','','');
INSERT INTO `contacts` VALUES (1262,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','TORBO COLOR d.o.o.','','','','','','','',52,'farbo.interijeri@zg.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1263,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','BAJKMONT d.o.o.','','','','Svetomatejska 12','','10360','Sesvete',52,'miroslav@bajkmont.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1264,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SEKA VARAŽDIN','','','','','','','',52,'seka33@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1265,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','S1 d.o.o.','','10020122641','','Stjepana Širole 8','','10000','Zagreb',52,'s-1@inet.hr','','+385 95 5619324','','','','','','');
INSERT INTO `contacts` VALUES (1266,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','LIMEA TRAVEL ZAGREB d.o.o.','','','','','','','',52,'limea.travelzg@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1267,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','VLADO ANDRAŠEVIĆ','','','','','','','',52,'vlado_and@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1274,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','NORMAN GRUPA d.o.o.','','','','','','','',52,'ante.odak@norman-grupa.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1277,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mateja Nemet Andrasevic','','','','-kupac građanin','','','',52,'vlado_and@yahoo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1279,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SUMME SENSUS d.o.o.','','','','','','','',52,'tina@summesensus.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1280,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','PRO OPTIMUS','','','','Augusta Brauna 3','','71000','Sarajevo BIH',52,'pco@pro-optimus.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1281,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Euro-logistične usluge d.o.o.','','','','','','','',52,'eurologu@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1282,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Synthesis d.o.o. ','','','','','','','',52,'jurica@synthesis.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1284,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','JEDINSTVO D.D.','','','','','','','',52,'jmihalic@jedinstvo.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1285,'0000-00-00 00:00:00','2015-12-23 09:59:07','','',' CENTAR ZA AUTIZAM','','','','','','','',52,'nada.bolont@gmail.com','','','','','bla,12345\r\n','','','');
INSERT INTO `contacts` VALUES (1286,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','V.D.P d.o.o.','','','','','','','',52,'stipcic.vinko@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1288,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GALKO d.o.o.','','','','','','','',52,'marin@galko.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1289,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KMAG d.o.o.','','','','','','','',52,'iviana.penic@kmag.net','','','','','','','','');
INSERT INTO `contacts` VALUES (1290,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MLC','','','','','','','',52,'zdravko.babic@mlc.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1291,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','HETA d.o.o.','','157962295615','','Vlaška 62','','10000','Zagreb',52,'eniz@heta.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1292,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SUPER ŠANSA d.o.o.','','','','','','','',52,'bozena.bicanic@stanleybet.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1293,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','PRIMOTRONIC d.o.o.','','01358353636','','Savska cesta 118','','10000','Zagreb',52,'sinisa.jorgic@primotronic.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1294,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','ŠUMARSKI FAKULTET','','07699719217','','Sveto Šimunska 25','','10000','Zagreb',52,'smikac@gmail.com','+385 99 3133795','','','','STJEPAN MIKAC - KONTAKT','','','');
INSERT INTO `contacts` VALUES (1295,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','AGENTURA d.o.o.','','68817739468','','Rubeši 79','','51215','Kastav',52,'agentura@ri.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1296,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','HUBITK','','','','Trnjanska 140','','10000','Zagreb',52,'matea.uhlik-vidovic@hubitk.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1297,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','UNITAS d.d.','','','','','','','',52,'prodaja@unitas.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1298,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','STROJOGRADNJA ŠIVAK d.o.o.','','79517833007','','Demerska 9','','10251','Hrvatski Leskovac',52,'veljko.sivak@zg.t-com.hr','','+385 1 6578400','','','','','','');
INSERT INTO `contacts` VALUES (1307,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','NOVA rent a car','','','','','','','',52,'josko@rentacarsplit.net','','','','','','','','');
INSERT INTO `contacts` VALUES (1308,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','HUSZPO','','50729905264','','Berislavićeva 6','','10000','Zagreb',52,'huszpo@huszpo.hr','','+385 1 611 48 67','','','','','','');
INSERT INTO `contacts` VALUES (1309,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','SYSTEM ONE d.o.o.','','43566773819','','Ul. Grada Vukovara 237 D','','10000','Zagreb',52,'tomas.blaha@s1see.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1310,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','PUŠIĆ d.o.o.','','','','','','','',52,'administracija@pusic.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1311,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','INVENTA d.o.o.','','99213847706','','K. A. Stepinca 7','','32000','Vukovar',52,'ana.krstic@inventa.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1312,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','3d studio','','','','','','','',52,'info@3dstudio.hr','','','','',',','','','');
INSERT INTO `contacts` VALUES (1314,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','START KONGRESI','','','','','','','',52,'start@start-kongresi.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1315,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','TEST MAIL','','','','','','','',52,'igor.cukac@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1320,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','EMPE BIRO D.O.O.','','98617709143','','','','10360','Sesvete',52,'mandaric.srecko@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1321,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','SELES ZASTUPSTVA  d.o.o.','','18163429716','','Varaždinska 23','','10360','Sesvete',52,'mario.kovac@seles.hr','','+385 98 9824655','','','','','','');
INSERT INTO `contacts` VALUES (1322,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KB Dubrava Klinika za plastičnu kirurgiju','','','','','','','',52,'zstanec@kbd.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1323,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','ŠPREM-AMARENA d.o.o.','','90196496457','',' Ivana Gundulića 18','','42240','Ivanec',52,'sprem-amarena@vz.t-com.hr','','','','','','','','');
INSERT INTO `contacts` VALUES (1324,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','RIPINA d.o.o.','','','','','','','',52,'ripina.info@gmail.com','','','','','','','','');
INSERT INTO `contacts` VALUES (1325,'0000-00-00 00:00:00','2015-12-22 09:19:44','','','HERMES INTERNATIONAL D.O.O','','81810672657','',' Nova ulica 5','','42204','Turčin',52,'mladen.ivanus1@gmail.com','','','','','','','','');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table contract_templates
#

DROP TABLE IF EXISTS `contract_templates`;
CREATE TABLE `contract_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table contract_templates
#
LOCK TABLES `contract_templates` WRITE;
/*!40000 ALTER TABLE `contract_templates` DISABLE KEYS */;

/*!40000 ALTER TABLE `contract_templates` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table contracts
#

DROP TABLE IF EXISTS `contracts`;
CREATE TABLE `contracts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `client` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `pdf` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table contracts
#
LOCK TABLES `contracts` WRITE;
/*!40000 ALTER TABLE `contracts` DISABLE KEYS */;

INSERT INTO `contracts` VALUES (2,'2015-12-21 10:35:13','2015-12-21 10:35:13','test','','','','');
/*!40000 ALTER TABLE `contracts` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table countries
#

DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pri` smallint(6) NOT NULL DEFAULT '0',
  `iso3166` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `iso3166a3` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table countries
#
LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;

INSERT INTO `countries` VALUES (1,'Afghanistan','af',2,'af','');
INSERT INTO `countries` VALUES (2,'Albania','al',3,'al','');
INSERT INTO `countries` VALUES (3,'Algeria','dz',4,'dz','');
INSERT INTO `countries` VALUES (4,'American Samoa','as',5,'as','');
INSERT INTO `countries` VALUES (5,'Andorra','ad',6,'ad','');
INSERT INTO `countries` VALUES (6,'Angola','ao',7,'ao','');
INSERT INTO `countries` VALUES (7,'Anguilla','ai',8,'ai','');
INSERT INTO `countries` VALUES (8,'Antarctica','aq',9,'aq','');
INSERT INTO `countries` VALUES (9,'Antigua and Barbuda','ag',10,'ag','');
INSERT INTO `countries` VALUES (10,'Argentina','ar',11,'ar','');
INSERT INTO `countries` VALUES (11,'Armenia','am',12,'am','');
INSERT INTO `countries` VALUES (12,'Aruba','aw',13,'aw','');
INSERT INTO `countries` VALUES (13,'Australia','au',14,'au','');
INSERT INTO `countries` VALUES (14,'Austria','at',15,'at','');
INSERT INTO `countries` VALUES (15,'Azerbaijan','az',16,'az','');
INSERT INTO `countries` VALUES (16,'Bahamas','bs',17,'bs','');
INSERT INTO `countries` VALUES (17,'Bahrain','bh',18,'bh','');
INSERT INTO `countries` VALUES (18,'Bangladesh','bd',19,'bd','');
INSERT INTO `countries` VALUES (19,'Barbados','bb',20,'bb','');
INSERT INTO `countries` VALUES (20,'Belarus','by',21,'by','');
INSERT INTO `countries` VALUES (21,'Belgium','be',22,'be','');
INSERT INTO `countries` VALUES (22,'Belize','bz',23,'bz','');
INSERT INTO `countries` VALUES (23,'Benin','bj',24,'bj','');
INSERT INTO `countries` VALUES (24,'Bermuda','bm',25,'bm','');
INSERT INTO `countries` VALUES (25,'Bhutan','bt',26,'bt','');
INSERT INTO `countries` VALUES (26,'Bolivia','bo',27,'bo','');
INSERT INTO `countries` VALUES (27,'Bosnia and Herzegovina','ba',28,'ba','');
INSERT INTO `countries` VALUES (28,'Botswana','bw',29,'bw','');
INSERT INTO `countries` VALUES (29,'Bouvet Island','bv',30,'bv','');
INSERT INTO `countries` VALUES (30,'Brazil','br',31,'br','');
INSERT INTO `countries` VALUES (31,'British Indian Ocean Territory','io',32,'io','');
INSERT INTO `countries` VALUES (32,'Brunei darussalam','bn',33,'bn','');
INSERT INTO `countries` VALUES (33,'Bulgaria','bg',34,'bg','');
INSERT INTO `countries` VALUES (34,'Burkina Faso','bf',35,'bf','');
INSERT INTO `countries` VALUES (35,'Burundi','bi',36,'bi','');
INSERT INTO `countries` VALUES (36,'Cambodia','kh',37,'kh','');
INSERT INTO `countries` VALUES (37,'Cameroon','cm',38,'cm','');
INSERT INTO `countries` VALUES (38,'Canada','ca',39,'ca','');
INSERT INTO `countries` VALUES (39,'Cape Verde','cv',40,'cv','');
INSERT INTO `countries` VALUES (40,'Cayman Island','ky',41,'ky','');
INSERT INTO `countries` VALUES (41,'Central African Republic','cf',42,'cf','');
INSERT INTO `countries` VALUES (42,'Chad','td',43,'td','');
INSERT INTO `countries` VALUES (43,'Chile','cl',44,'cl','');
INSERT INTO `countries` VALUES (44,'China','cn',45,'cn','');
INSERT INTO `countries` VALUES (45,'Christmas Island','cx',46,'cx','');
INSERT INTO `countries` VALUES (46,'Cocos (Keeling) Island','cc',47,'cc','');
INSERT INTO `countries` VALUES (47,'Colombia','co',48,'co','');
INSERT INTO `countries` VALUES (48,'Comoros','km',49,'km','');
INSERT INTO `countries` VALUES (49,'Congo','cg',50,'cg','');
INSERT INTO `countries` VALUES (50,'Cook Island','ck',51,'ck','');
INSERT INTO `countries` VALUES (51,'Costa Rica','cr',52,'cr','');
INSERT INTO `countries` VALUES (52,'Hrvatska (Croatia)','hr',1,'hr','');
INSERT INTO `countries` VALUES (53,'Cuba','cu',54,'cu','');
INSERT INTO `countries` VALUES (54,'Cyprus','cy',55,'cy','');
INSERT INTO `countries` VALUES (55,'Czech Republic','cz',56,'cz','');
INSERT INTO `countries` VALUES (56,'Denmark','dk',57,'dk','');
INSERT INTO `countries` VALUES (57,'Djibouti','dj',58,'dj','');
INSERT INTO `countries` VALUES (58,'Dominica','dm',59,'dm','');
INSERT INTO `countries` VALUES (59,'Dominican Republic','do',60,'do','');
INSERT INTO `countries` VALUES (61,'Ecuador','ec',62,'ec','');
INSERT INTO `countries` VALUES (62,'Egypt','eg',63,'eg','');
INSERT INTO `countries` VALUES (63,'El Salvador','sv',64,'sv','');
INSERT INTO `countries` VALUES (64,'Equatorial Guinea','gq',65,'gq','');
INSERT INTO `countries` VALUES (65,'Eritrea','er',66,'er','');
INSERT INTO `countries` VALUES (66,'Estonia','ee',67,'ee','');
INSERT INTO `countries` VALUES (67,'Ethiopia','et',68,'et','');
INSERT INTO `countries` VALUES (68,'Falkland Islands (Malvinas)','fk',69,'fk','');
INSERT INTO `countries` VALUES (69,'Faroe Islands','fo',70,'fo','');
INSERT INTO `countries` VALUES (70,'Fiji','fj',71,'fj','');
INSERT INTO `countries` VALUES (71,'Finland','fi',72,'fi','');
INSERT INTO `countries` VALUES (72,'France','fr',73,'fr','');
INSERT INTO `countries` VALUES (74,'French Guiana','gf',75,'gf','');
INSERT INTO `countries` VALUES (75,'French Polynesia','pf',76,'pf','');
INSERT INTO `countries` VALUES (76,'French Southern Territories','tf',77,'tf','');
INSERT INTO `countries` VALUES (77,'Gabon','ga',78,'ga','');
INSERT INTO `countries` VALUES (78,'Gambia','gm',79,'gm','');
INSERT INTO `countries` VALUES (79,'Georgia','ge',80,'ge','');
INSERT INTO `countries` VALUES (80,'Germany','de',81,'de','');
INSERT INTO `countries` VALUES (81,'Ghana','gh',82,'gh','');
INSERT INTO `countries` VALUES (82,'Gibraltar','gi',83,'gi','');
INSERT INTO `countries` VALUES (83,'Greece','gr',84,'gr','');
INSERT INTO `countries` VALUES (84,'Greenland','gl',85,'gl','');
INSERT INTO `countries` VALUES (85,'Grenada','gd',86,'gd','');
INSERT INTO `countries` VALUES (86,'Guadeloupe','gp',87,'gp','');
INSERT INTO `countries` VALUES (87,'Guam','gu',88,'gu','');
INSERT INTO `countries` VALUES (88,'Guatemala','gt',89,'gt','');
INSERT INTO `countries` VALUES (89,'Guinea','gn',90,'gn','');
INSERT INTO `countries` VALUES (90,'Guinea Bissau','gw',91,'gw','');
INSERT INTO `countries` VALUES (91,'Guyana','gy',92,'gy','');
INSERT INTO `countries` VALUES (92,'Haiti','ht',93,'ht','');
INSERT INTO `countries` VALUES (93,'Heard Island and Mc Donald Islands','hm',94,'hm','');
INSERT INTO `countries` VALUES (94,'Honduras','hn',95,'hn','');
INSERT INTO `countries` VALUES (95,'Hong Kong','hk',96,'hk','');
INSERT INTO `countries` VALUES (96,'Hungary','hu',97,'hu','');
INSERT INTO `countries` VALUES (97,'Iceland','is',98,'is','');
INSERT INTO `countries` VALUES (98,'India','in',99,'in','');
INSERT INTO `countries` VALUES (99,'Indonesia','id',100,'id','');
INSERT INTO `countries` VALUES (100,'Iran (Islamic republic of)','ir',101,'ir','');
INSERT INTO `countries` VALUES (101,'Iraq','iq',102,'iq','');
INSERT INTO `countries` VALUES (102,'Ireland','ie',103,'ie','');
INSERT INTO `countries` VALUES (103,'Israel','il',104,'il','');
INSERT INTO `countries` VALUES (104,'Italy','it',105,'it','');
INSERT INTO `countries` VALUES (106,'Jamaica','jm',107,'jm','');
INSERT INTO `countries` VALUES (107,'Japan','jp',108,'jp','');
INSERT INTO `countries` VALUES (108,'Jordan','jo',109,'jo','');
INSERT INTO `countries` VALUES (109,'Kazakhstan','kz',110,'kz','');
INSERT INTO `countries` VALUES (110,'Kenya','ke',111,'ke','');
INSERT INTO `countries` VALUES (111,'Kiribati','ki',112,'ki','');
INSERT INTO `countries` VALUES (112,'Korea, Democratic people´s Republic of','kp',113,'kp','');
INSERT INTO `countries` VALUES (113,'Korea, Republic of','kr',114,'kr','');
INSERT INTO `countries` VALUES (114,'Kuwait','kw',115,'kw','');
INSERT INTO `countries` VALUES (115,'Kyrgyzstan','kg',116,'kg','');
INSERT INTO `countries` VALUES (116,'Lao people´s democratic republic','la',117,'la','');
INSERT INTO `countries` VALUES (117,'Latvia','lv',118,'lv','');
INSERT INTO `countries` VALUES (118,'Lebanon','lb',119,'lb','');
INSERT INTO `countries` VALUES (119,'Lesotho','ls',120,'ls','');
INSERT INTO `countries` VALUES (120,'Liberia','lr',121,'lr','');
INSERT INTO `countries` VALUES (121,'Libyan Arab Jamahiriya','ly',122,'ly','');
INSERT INTO `countries` VALUES (122,'Liechtenstein','li',123,'li','');
INSERT INTO `countries` VALUES (123,'Lithuania','lt',124,'lt','');
INSERT INTO `countries` VALUES (124,'Luxembourg','lu',125,'lu','');
INSERT INTO `countries` VALUES (125,'Macau','mo',126,'mo','');
INSERT INTO `countries` VALUES (126,'Madagascar','mg',127,'mg','');
INSERT INTO `countries` VALUES (127,'Malawi','mw',128,'mw','');
INSERT INTO `countries` VALUES (128,'Malaysia','my',129,'my','');
INSERT INTO `countries` VALUES (129,'Maldives','mv',130,'mv','');
INSERT INTO `countries` VALUES (130,'Mali','ml',131,'ml','');
INSERT INTO `countries` VALUES (131,'Malta','mt',132,'mt','');
INSERT INTO `countries` VALUES (132,'Marshall islands','mh',133,'mh','');
INSERT INTO `countries` VALUES (133,'Martinique','mq',134,'mq','');
INSERT INTO `countries` VALUES (134,'Mauritania','mr',135,'mr','');
INSERT INTO `countries` VALUES (135,'Mayotte','yt',136,'yt','');
INSERT INTO `countries` VALUES (136,'Mexico','mx',137,'mx','');
INSERT INTO `countries` VALUES (137,'Micronesia (Federated States of)','fm',138,'fm','');
INSERT INTO `countries` VALUES (138,'Moldova, Republic of','md',139,'md','');
INSERT INTO `countries` VALUES (139,'Monaco','mc',140,'mc','');
INSERT INTO `countries` VALUES (140,'Mongolia','mn',141,'mn','');
INSERT INTO `countries` VALUES (141,'Monserrat','ms',142,'ms','');
INSERT INTO `countries` VALUES (142,'Morocco','ma',143,'ma','');
INSERT INTO `countries` VALUES (143,'Mozambigue','mz',144,'mz','');
INSERT INTO `countries` VALUES (144,'Myanmar','mm',145,'mm','');
INSERT INTO `countries` VALUES (145,'Namibia','na',146,'na','');
INSERT INTO `countries` VALUES (146,'Nauru','nr',147,'nr','');
INSERT INTO `countries` VALUES (147,'Nepal','np',148,'np','');
INSERT INTO `countries` VALUES (148,'Netherlands','nl',149,'nl','');
INSERT INTO `countries` VALUES (149,'Netherlands Antilles','an',150,'an','');
INSERT INTO `countries` VALUES (150,'New Caledonia','nc',151,'nc','');
INSERT INTO `countries` VALUES (151,'New Zealand','nz',152,'nz','');
INSERT INTO `countries` VALUES (152,'Nicaragua','ni',153,'ni','');
INSERT INTO `countries` VALUES (153,'Niger','ne',154,'ne','');
INSERT INTO `countries` VALUES (154,'Nigeria','ng',155,'ng','');
INSERT INTO `countries` VALUES (155,'Niue','nu',156,'nu','');
INSERT INTO `countries` VALUES (156,'Norfolk Islands','nf',157,'nf','');
INSERT INTO `countries` VALUES (157,'Northern Mariana Islands','mp',158,'mp','');
INSERT INTO `countries` VALUES (158,'Norway','no',159,'no','');
INSERT INTO `countries` VALUES (159,'Oman','om',160,'om','');
INSERT INTO `countries` VALUES (160,'Pakistan','pk',161,'pk','');
INSERT INTO `countries` VALUES (161,'Palau','pw',162,'pw','');
INSERT INTO `countries` VALUES (162,'Panama','pa',163,'pa','');
INSERT INTO `countries` VALUES (163,'Papua New Guinea','pg',164,'pg','');
INSERT INTO `countries` VALUES (164,'Paraguay','py',165,'py','');
INSERT INTO `countries` VALUES (165,'Peru','pe',166,'pe','');
INSERT INTO `countries` VALUES (166,'Philippines','ph',167,'ph','');
INSERT INTO `countries` VALUES (167,'Pitcairn','pn',168,'pn','');
INSERT INTO `countries` VALUES (168,'Poland','pl',169,'pl','');
INSERT INTO `countries` VALUES (169,'Portugal','pt',170,'pt','');
INSERT INTO `countries` VALUES (170,'Puerto Rico','pr',171,'pr','');
INSERT INTO `countries` VALUES (171,'Qatar','qa',172,'qa','');
INSERT INTO `countries` VALUES (172,'Reunion','re',173,'re','');
INSERT INTO `countries` VALUES (173,'Romania','ro',174,'ro','');
INSERT INTO `countries` VALUES (174,'Russia Federation','ru',175,'ru','');
INSERT INTO `countries` VALUES (175,'Rwanda','rw',176,'rw','');
INSERT INTO `countries` VALUES (176,'Saint Helena','sh',177,'sh','');
INSERT INTO `countries` VALUES (177,'Saint Kitts and Nevis','kn',178,'kn','');
INSERT INTO `countries` VALUES (178,'Saint Lucia','lc',179,'lc','');
INSERT INTO `countries` VALUES (179,'Saint Pierre et Miquelon','pm',180,'pm','');
INSERT INTO `countries` VALUES (180,'Saint Vincent and the Grenadines','vc',181,'vc','');
INSERT INTO `countries` VALUES (181,'Samoa','ws',182,'ws','');
INSERT INTO `countries` VALUES (182,'San Marino','sm',183,'sm','');
INSERT INTO `countries` VALUES (183,'Sao Tome and Principe','st',184,'st','');
INSERT INTO `countries` VALUES (184,'Saudi Arabia','sa',185,'sa','');
INSERT INTO `countries` VALUES (185,'Senegal','sn',186,'sn','');
INSERT INTO `countries` VALUES (186,'Serbia','rs',187,'rs','');
INSERT INTO `countries` VALUES (187,'Seychelles','sc',188,'sc','');
INSERT INTO `countries` VALUES (188,'Sierra Leone','sl',189,'sl','');
INSERT INTO `countries` VALUES (189,'Singapore','sg',190,'sg','');
INSERT INTO `countries` VALUES (190,'Slovakia','sk',191,'sk','');
INSERT INTO `countries` VALUES (191,'Slovenia','si',192,'si','');
INSERT INTO `countries` VALUES (192,'Solomon Islands','sb',193,'sb','');
INSERT INTO `countries` VALUES (193,'Somalia','so',194,'so','');
INSERT INTO `countries` VALUES (194,'South Africa','za',195,'za','');
INSERT INTO `countries` VALUES (195,'Spain','es',196,'es','');
INSERT INTO `countries` VALUES (196,'Sri Lanka','lk',197,'lk','');
INSERT INTO `countries` VALUES (197,'Sudan','sd',198,'sd','');
INSERT INTO `countries` VALUES (198,'Suriname','sr',199,'sr','');
INSERT INTO `countries` VALUES (199,'Svalbard and Jan Mayen','sj',200,'sj','');
INSERT INTO `countries` VALUES (200,'Swaziland','sz',201,'sz','');
INSERT INTO `countries` VALUES (201,'Sweden','se',202,'se','');
INSERT INTO `countries` VALUES (202,'Switzerland','ch',203,'ch','');
INSERT INTO `countries` VALUES (203,'Syria Arab Republic','sy',204,'sy','');
INSERT INTO `countries` VALUES (204,'Taiwan, Province of China','tw',205,'tw','');
INSERT INTO `countries` VALUES (205,'Tajikistan','tj',206,'tj','');
INSERT INTO `countries` VALUES (206,'Tanzania, United Republic Of','tz',207,'tz','');
INSERT INTO `countries` VALUES (207,'Thailand','th',208,'th','');
INSERT INTO `countries` VALUES (208,'Togo','tg',209,'tg','');
INSERT INTO `countries` VALUES (209,'Tokelau','tk',210,'tk','');
INSERT INTO `countries` VALUES (210,'Tonga','to',211,'to','');
INSERT INTO `countries` VALUES (211,'Trinidad and Tobago','tt',212,'tt','');
INSERT INTO `countries` VALUES (212,'Tunisia','tn',213,'tn','');
INSERT INTO `countries` VALUES (213,'Turkey','tr',214,'tr','');
INSERT INTO `countries` VALUES (214,'Turkmenistan','tm',215,'tm','');
INSERT INTO `countries` VALUES (215,'Turks and Caicos Islands','tc',216,'tc','');
INSERT INTO `countries` VALUES (216,'Tuvalu','tv',217,'tv','');
INSERT INTO `countries` VALUES (217,'Uganda','ug',218,'ug','');
INSERT INTO `countries` VALUES (218,'Ukraine','ua',219,'ua','');
INSERT INTO `countries` VALUES (219,'United Arab Emirates','ae',220,'ae','');
INSERT INTO `countries` VALUES (220,'United Kingdom','gb',221,'gb','');
INSERT INTO `countries` VALUES (221,'United States','us',222,'us','');
INSERT INTO `countries` VALUES (222,'United States Minor Outlying Islands','um',223,'um','');
INSERT INTO `countries` VALUES (223,'Uruguay','uy',224,'uy','');
INSERT INTO `countries` VALUES (224,'Uzbekistan','uz',225,'uz','');
INSERT INTO `countries` VALUES (225,'Vanuatu','vu',226,'vu','');
INSERT INTO `countries` VALUES (226,'Vatican City State (Holy See)','va',227,'va','');
INSERT INTO `countries` VALUES (227,'Venezuela','ve',228,'ve','');
INSERT INTO `countries` VALUES (228,'Vietnam','vn',229,'vn','');
INSERT INTO `countries` VALUES (229,'Virgin Islands (British)','vg',230,'vg','');
INSERT INTO `countries` VALUES (230,'Virgin Islands (U.S.)','vi',231,'vi','');
INSERT INTO `countries` VALUES (231,'Wallis and Futuna Islands','wf',232,'wf','');
INSERT INTO `countries` VALUES (232,'Western Sahara','eh',233,'eh','');
INSERT INTO `countries` VALUES (233,'Yemen','ye',234,'ye','');
INSERT INTO `countries` VALUES (236,'Zambia','zm',237,'zm','');
INSERT INTO `countries` VALUES (237,'Zimbabwe','zw',238,'zw','');
INSERT INTO `countries` VALUES (238,'Aland Islands','ax',240,'ax','');
INSERT INTO `countries` VALUES (239,'Confo, The Democratic Republic of thec','cd',240,'cd','');
INSERT INTO `countries` VALUES (240,'Cote D\'ivoire','ci',240,'ci','');
INSERT INTO `countries` VALUES (241,'Guernsey','gg',240,'gg','');
INSERT INTO `countries` VALUES (243,'Isle of Man','im',240,'im','');
INSERT INTO `countries` VALUES (244,'Jersey','je',240,'je','');
INSERT INTO `countries` VALUES (245,'Macedonia (The Former Yugoslav Republic of)','mk',240,'mk','');
INSERT INTO `countries` VALUES (246,'Mauritius','mu',240,'mu','');
INSERT INTO `countries` VALUES (247,'Montenegro','me',240,'me','');
INSERT INTO `countries` VALUES (248,'Palestinian Territory, Occupied','ps',240,'ps','');
INSERT INTO `countries` VALUES (249,'Saint Barthelemy','bl',240,'bl','');
INSERT INTO `countries` VALUES (250,'Saint Martin','mf',240,'mf','');
INSERT INTO `countries` VALUES (251,'South Georgia and the South Sandwich Islands','gs',240,'gs','');
INSERT INTO `countries` VALUES (252,'Timor-Leste','tl',240,'tl','');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table damages
#

DROP TABLE IF EXISTS `damages`;
CREATE TABLE `damages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `vehicle_id` int(11) NOT NULL DEFAULT '0',
  `vehicle_km` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `date` date DEFAULT NULL,
  `where` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table damages
#
LOCK TABLES `damages` WRITE;
/*!40000 ALTER TABLE `damages` DISABLE KEYS */;

INSERT INTO `damages` VALUES (1,'2015-12-18 12:12:58','2015-12-18 12:44:32','Oštećenje desne strane',7,'250000','2015-07-22','Novalje','1','U najmu kod KUNEŠTE oštećena desna stranica (30cm ogrebotina)','- prijavljeno kasku\r\n- naplaćena franšiza 1.250,00kuna\r\n- popravak nakon povratka iz Zagreb Montaže','');
INSERT INTO `damages` VALUES (2,'2015-12-18 12:15:51','2015-12-18 12:15:51','Desna stranica',4,'','2015-10-06','Zagreb','1','Oštećena desna stranica kod NEKOM OBRT','- prijavljeno po kasko\r\n- franšizu prebili popravkom kuplunga u Šibeniku iz 7.mj.2015\r\n- popravak dogovoren u Baotiću kada se vrati vozilo iz najma\r\n- naknadno ćemo urediti sitna oštećenje na ljevoj stranici\r\npreko limara iz Baotića (1500/2000kn)','');
/*!40000 ALTER TABLE `damages` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table documents
#

DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uploaded_by` int(10) unsigned NOT NULL DEFAULT '0',
  `shared` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ext` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `size` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table documents
#
LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;

INSERT INTO `documents` VALUES (1,'2015-12-15 09:53:11','2015-12-15 09:53:11',1,0,'Capture.PNG','PNG',12340,'/files/Capture.PNG');
INSERT INTO `documents` VALUES (2,'2015-12-21 08:08:07','2015-12-21 08:08:07',2,0,'OTPLATNI_PLAN_Master_RI-815-UA.pdf','pdf',91041,'/files/OTPLATNI_PLAN_Master_RI-815-UA.pdf');
INSERT INTO `documents` VALUES (3,'2015-12-21 08:08:10','2015-12-21 08:08:10',2,0,'AO_2015_RI-815-UA.pdf','pdf',139541,'/files/AO_2015_RI-815-UA.pdf');
INSERT INTO `documents` VALUES (4,'2015-12-21 08:08:25','2015-12-21 08:08:25',2,0,'KASKO_2015_RI-815-UA.pdf','pdf',1311459,'/files/KASKO_2015_RI-815-UA.pdf');
INSERT INTO `documents` VALUES (5,'2015-12-21 08:59:11','2015-12-21 08:59:11',2,0,'AO_RI-629-UP_2015.pdf','pdf',133763,'/files/AO_RI-629-UP_2015.pdf');
INSERT INTO `documents` VALUES (6,'2015-12-21 08:59:12','2015-12-21 08:59:12',2,0,'OTPLATNI_PLAN__Master_RI-629-UP.pdf','pdf',72249,'/files/OTPLATNI_PLAN__Master_RI-629-UP.pdf');
INSERT INTO `documents` VALUES (7,'2015-12-21 08:59:17','2015-12-21 08:59:17',2,0,'KASKO_2015.pdf','pdf',1326561,'/files/KASKO_2015.pdf');
INSERT INTO `documents` VALUES (8,'2015-12-21 10:20:24','2015-12-21 10:20:24',1,0,'unnamed.png','png',230909,'/files/unnamed.png');
INSERT INTO `documents` VALUES (9,'2015-12-21 11:00:38','2015-12-21 11:00:38',2,0,'KASKO_2015.pdf','pdf',151863,'/files/KASKO_2015.pdf');
INSERT INTO `documents` VALUES (10,'2015-12-21 11:00:38','2015-12-21 11:00:38',2,0,'OTPLATNI_PLAN__Master_RI-838-VH.pdf','pdf',83376,'/files/OTPLATNI_PLAN__Master_RI-838-VH.pdf');
INSERT INTO `documents` VALUES (11,'2015-12-21 11:07:15','2015-12-21 11:07:15',2,0,'OTPLATNI_PLAN__Master_RI-838-VH.pdf','pdf',83376,'/files/OTPLATNI_PLAN__Master_RI-838-VH.pdf');
INSERT INTO `documents` VALUES (12,'2015-12-21 11:15:07','2015-12-21 11:15:07',2,0,'AO_2015.pdf','pdf',141470,'/files/AO_2015.pdf');
INSERT INTO `documents` VALUES (13,'2015-12-21 11:15:07','2015-12-21 11:15:07',2,0,'OTPLATNI_PLAN__Vivaro_RI-165-UA.pdf','pdf',92603,'/files/OTPLATNI_PLAN__Vivaro_RI-165-UA.pdf');
INSERT INTO `documents` VALUES (14,'2015-12-21 11:15:54','2015-12-21 11:15:54',2,0,'RI-165-UA.doc','doc',172544,'/files/RI-165-UA.doc');
INSERT INTO `documents` VALUES (15,'2015-12-23 18:24:52','2015-12-23 18:24:52',1,0,'4b01eb5f06a8c9ff76f0baa745b1bfaf0a7b250249bdded2feb214c0e1047b84.jpg','jpg',107612,'/files/4b01eb5f06a8c9ff76f0baa745b1bfaf0a7b250249bdded2feb214c0e1047b84.jpg');
INSERT INTO `documents` VALUES (16,'2015-12-28 16:15:49','2015-12-28 16:15:49',1,0,'4b01eb5f06a8c9ff76f0baa745b1bfaf0a7b250249bdded2feb214c0e1047b84.jpg','jpg',107612,'/files/4b01eb5f06a8c9ff76f0baa745b1bfaf0a7b250249bdded2feb214c0e1047b84.jpg');
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table migrations
#

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `migrations_migration_unique` (`migration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table migrations
#
LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` VALUES ('2015_11_03_071430_create_user_groups_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_04_194404_create_users_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_05_071833_create_password_resets_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_05_072457_create_countries_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_155533_create_vehicles_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_155741_create_vehicle_services_tables.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_161025_create_damages_tables.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_164321_create_tasks_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_165212_create_bookings_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_165314_create_contacts_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_14_084224_create_contract_templates_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_14_084433_create_contracts_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_19_091720_create_translation_tags_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_19_091923_create_translations_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_21_143830_add_registration_columns_to_vehicle.php',1);
INSERT INTO `migrations` VALUES ('2015_11_24_171819_add_pp_columns_to_vehicles.php',1);
INSERT INTO `migrations` VALUES ('2015_11_24_172626_add_km_column_to_vehicle_services.php',1);
INSERT INTO `migrations` VALUES ('2015_11_24_175001_add_groups_column_to_contacts.php',1);
INSERT INTO `migrations` VALUES ('2015_11_29_121909_create_documents_table.php',1);
INSERT INTO `migrations` VALUES ('2015_12_01_193345_add_columns_to_vehicles.php',1);
INSERT INTO `migrations` VALUES ('2015_12_09_194008_add_docs_to_damages_and_bookings.php',1);
INSERT INTO `migrations` VALUES ('2015_12_10_091238_create_checklists_table.php',1);
INSERT INTO `migrations` VALUES ('2015_12_15_203702_add_column_km_to_vehicles.php',2);
INSERT INTO `migrations` VALUES ('2015_12_15_205229_add_column_service_km_to_vehicles.php',3);
INSERT INTO `migrations` VALUES ('2015_12_20_131141_switch_docs_and_note_fields.php',4);
INSERT INTO `migrations` VALUES ('2015_12_23_095550_add_extra_fields_to_contacts.php',5);
INSERT INTO `migrations` VALUES ('2015_12_23_102014_add_extra_fields_to_bookings.php',6);
INSERT INTO `migrations` VALUES ('2015_12_24_083322_create_reminders_table.php',7);
INSERT INTO `migrations` VALUES ('2015_12_28_113013_add_column_booking_state.php',7);
INSERT INTO `migrations` VALUES ('2015_12_28_232621_add_columns_to_reminders.php',8);
INSERT INTO `migrations` VALUES ('2015_12_29_115401_add_service_type_to_bookings.php',9);
INSERT INTO `migrations` VALUES ('2015_12_31_093015_add_fields_to_vehicle_services.php',10);
INSERT INTO `migrations` VALUES ('2016_01_04_123959_alter_services_from_to_nullable.php',11);
INSERT INTO `migrations` VALUES ('2016_01_12_121543_add_show_on_calendar_to_vehicles.php',12);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table password_resets
#

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  UNIQUE KEY `password_resets_email_unique` (`email`),
  UNIQUE KEY `password_resets_token_unique` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table password_resets
#
LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;

/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table reminders
#

DROP TABLE IF EXISTS `reminders`;
CREATE TABLE `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `assigned_to` int(10) unsigned NOT NULL DEFAULT '0',
  `followers` text COLLATE utf8_unicode_ci NOT NULL,
  `done` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `done_at` datetime NOT NULL,
  `done_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table reminders
#
LOCK TABLES `reminders` WRITE;
/*!40000 ALTER TABLE `reminders` DISABLE KEYS */;

/*!40000 ALTER TABLE `reminders` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table tasks
#

DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start` datetime NOT NULL,
  `due` datetime NOT NULL,
  `finish` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table tasks
#
LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;

/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table translation_tags
#

DROP TABLE IF EXISTS `translation_tags`;
CREATE TABLE `translation_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scope` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table translation_tags
#
LOCK TABLES `translation_tags` WRITE;
/*!40000 ALTER TABLE `translation_tags` DISABLE KEYS */;

INSERT INTO `translation_tags` VALUES (1,'2015-12-15 02:21:15','2015-12-15 02:21:15','','Pretraga');
INSERT INTO `translation_tags` VALUES (2,'2015-12-15 02:21:15','2015-12-15 02:21:15','','Vozilo');
INSERT INTO `translation_tags` VALUES (3,'2015-12-15 02:21:15','2015-12-15 02:21:15','','Per page');
INSERT INTO `translation_tags` VALUES (4,'2015-12-15 02:21:15','2015-12-15 02:21:15','','No matching results found.');
INSERT INTO `translation_tags` VALUES (5,'2015-12-15 02:21:19','2015-12-15 02:21:19','','Po stranici');
INSERT INTO `translation_tags` VALUES (6,'2015-12-15 02:21:36','2015-12-15 02:21:36','','Mjesto');
INSERT INTO `translation_tags` VALUES (7,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Registarska tablica');
INSERT INTO `translation_tags` VALUES (8,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Naziv vozila');
INSERT INTO `translation_tags` VALUES (9,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Marka vozila');
INSERT INTO `translation_tags` VALUES (10,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Model');
INSERT INTO `translation_tags` VALUES (11,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Tip');
INSERT INTO `translation_tags` VALUES (12,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Godina proizvodnje');
INSERT INTO `translation_tags` VALUES (13,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Veličina guma');
INSERT INTO `translation_tags` VALUES (14,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Boja');
INSERT INTO `translation_tags` VALUES (15,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Boja (kod)');
INSERT INTO `translation_tags` VALUES (16,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Broj šasije');
INSERT INTO `translation_tags` VALUES (17,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Partner');
INSERT INTO `translation_tags` VALUES (18,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Leasing kuća');
INSERT INTO `translation_tags` VALUES (19,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Rata leasinga');
INSERT INTO `translation_tags` VALUES (20,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Registriran do');
INSERT INTO `translation_tags` VALUES (21,'2015-12-15 09:00:53','2015-12-15 09:00:53','','AO osiguranje vrijedi do');
INSERT INTO `translation_tags` VALUES (22,'2015-12-15 09:00:53','2015-12-15 09:00:53','','AO osg.kuća');
INSERT INTO `translation_tags` VALUES (23,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Kasko vrijedi do');
INSERT INTO `translation_tags` VALUES (24,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Kasko osg.kuća');
INSERT INTO `translation_tags` VALUES (25,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Periodički pregled vrijedi do');
INSERT INTO `translation_tags` VALUES (26,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Protupožarni aparat vrijedi do');
INSERT INTO `translation_tags` VALUES (27,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Dokumenti');
INSERT INTO `translation_tags` VALUES (28,'2015-12-15 09:20:42','2015-12-15 09:20:42','','Name');
INSERT INTO `translation_tags` VALUES (29,'2015-12-15 09:20:42','2015-12-15 09:20:42','','Status');
INSERT INTO `translation_tags` VALUES (30,'2015-12-15 09:20:42','2015-12-15 09:20:42','','Client');
INSERT INTO `translation_tags` VALUES (31,'2015-12-15 09:20:42','2015-12-15 09:20:42','','Data');
INSERT INTO `translation_tags` VALUES (32,'2015-12-15 09:20:42','2015-12-15 09:20:42','','Pdf');
INSERT INTO `translation_tags` VALUES (33,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Ime');
INSERT INTO `translation_tags` VALUES (34,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Prezime');
INSERT INTO `translation_tags` VALUES (35,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Tvrtka (kratki naziv)');
INSERT INTO `translation_tags` VALUES (36,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Tvrtka (dugi naziv)');
INSERT INTO `translation_tags` VALUES (37,'2015-12-15 09:20:49','2015-12-15 09:20:49','','OIB');
INSERT INTO `translation_tags` VALUES (38,'2015-12-15 09:20:49','2015-12-15 09:20:49','','MB');
INSERT INTO `translation_tags` VALUES (39,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Adresa');
INSERT INTO `translation_tags` VALUES (40,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Adresa 2');
INSERT INTO `translation_tags` VALUES (41,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Poštanski broj');
INSERT INTO `translation_tags` VALUES (42,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Država');
INSERT INTO `translation_tags` VALUES (43,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Tip kontakta');
INSERT INTO `translation_tags` VALUES (44,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Email');
INSERT INTO `translation_tags` VALUES (45,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Mobilni tel.');
INSERT INTO `translation_tags` VALUES (46,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Tel.');
INSERT INTO `translation_tags` VALUES (47,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Fax');
INSERT INTO `translation_tags` VALUES (48,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Web');
INSERT INTO `translation_tags` VALUES (49,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Napomena');
INSERT INTO `translation_tags` VALUES (50,'2015-12-15 09:52:53','2015-12-15 09:52:53','','Tip servisa');
INSERT INTO `translation_tags` VALUES (51,'2015-12-15 09:52:53','2015-12-15 09:52:53','','Početni datum servisa');
INSERT INTO `translation_tags` VALUES (52,'2015-12-15 09:52:53','2015-12-15 09:52:53','','Završni datum servisa');
INSERT INTO `translation_tags` VALUES (53,'2015-12-15 09:52:53','2015-12-15 09:52:53','','Kilometraža');
INSERT INTO `translation_tags` VALUES (54,'2015-12-15 09:52:53','2015-12-15 09:52:53','','Servisni centar');
INSERT INTO `translation_tags` VALUES (55,'2015-12-15 09:52:53','2015-12-15 09:52:53','','Cijena servisa');
INSERT INTO `translation_tags` VALUES (56,'2015-12-15 09:52:53','2015-12-15 09:52:53','','Cijena dijelova');
INSERT INTO `translation_tags` VALUES (57,'2015-12-15 09:54:20','2015-12-15 09:54:20','','Search');
INSERT INTO `translation_tags` VALUES (58,'2015-12-15 09:54:20','2015-12-15 09:54:20','','Usergroup');
INSERT INTO `translation_tags` VALUES (59,'2015-12-15 09:54:28','2015-12-15 09:54:28','','Username');
INSERT INTO `translation_tags` VALUES (60,'2015-12-15 09:54:28','2015-12-15 09:54:28','','Password');
INSERT INTO `translation_tags` VALUES (61,'2015-12-15 09:54:28','2015-12-15 09:54:28','','First Name');
INSERT INTO `translation_tags` VALUES (62,'2015-12-15 09:54:28','2015-12-15 09:54:28','','Last Name');
INSERT INTO `translation_tags` VALUES (63,'2015-12-17 18:37:00','2015-12-17 18:37:00','','Prešao kilometara');
INSERT INTO `translation_tags` VALUES (64,'2015-12-17 18:37:00','2015-12-17 18:37:00','','Redoviti servis svakih (km)');
INSERT INTO `translation_tags` VALUES (65,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Početni datum');
INSERT INTO `translation_tags` VALUES (66,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Početno vrijeme');
INSERT INTO `translation_tags` VALUES (67,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Završni datum');
INSERT INTO `translation_tags` VALUES (68,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Završno vrijeme');
INSERT INTO `translation_tags` VALUES (69,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Početna kilometraža');
INSERT INTO `translation_tags` VALUES (70,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Završna kilometraža');
INSERT INTO `translation_tags` VALUES (71,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Ukupna cijena');
INSERT INTO `translation_tags` VALUES (72,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Klijent');
INSERT INTO `translation_tags` VALUES (73,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Preko koje firme');
INSERT INTO `translation_tags` VALUES (74,'2015-12-17 18:42:25','2015-12-17 18:42:25','','Naslov');
INSERT INTO `translation_tags` VALUES (75,'2015-12-17 18:42:25','2015-12-17 18:42:25','','Datum događaja');
INSERT INTO `translation_tags` VALUES (76,'2015-12-17 18:42:25','2015-12-17 18:42:25','','Mjesto događaja');
INSERT INTO `translation_tags` VALUES (77,'2015-12-17 18:42:25','2015-12-17 18:42:25','','Km na vozilu');
INSERT INTO `translation_tags` VALUES (78,'2015-12-17 18:42:25','2015-12-17 18:42:25','','Opis');
INSERT INTO `translation_tags` VALUES (79,'2015-12-17 18:42:25','2015-12-17 18:42:25','','Tko je oštetio vozilo');
INSERT INTO `translation_tags` VALUES (80,'2015-12-18 12:52:15','2015-12-18 12:52:15','','Tvrtka');
INSERT INTO `translation_tags` VALUES (81,'2015-12-18 12:58:09','2015-12-18 12:58:09','','Kontakt osoba');
INSERT INTO `translation_tags` VALUES (82,'2015-12-18 12:58:10','2015-12-18 12:58:10','','Kontakt telefon');
INSERT INTO `translation_tags` VALUES (83,'2015-12-18 13:00:45','2015-12-18 13:00:45','','Kontakt mobitel');
INSERT INTO `translation_tags` VALUES (84,'2015-12-18 13:05:48','2015-12-18 13:05:48','','Mobitel');
INSERT INTO `translation_tags` VALUES (85,'2015-12-20 13:46:31','2015-12-20 13:46:31','','Language');
INSERT INTO `translation_tags` VALUES (86,'2015-12-20 13:46:31','2015-12-20 13:46:31','','Translation');
INSERT INTO `translation_tags` VALUES (87,'2015-12-20 13:46:32','2015-12-20 13:46:32','','Actions');
INSERT INTO `translation_tags` VALUES (88,'2015-12-20 13:46:48','2015-12-20 13:46:48','','Tag');
INSERT INTO `translation_tags` VALUES (89,'2015-12-20 13:46:49','2015-12-20 13:46:49','','Auto');
INSERT INTO `translation_tags` VALUES (90,'2015-12-21 10:29:54','2015-12-21 10:29:54','','EDIT');
INSERT INTO `translation_tags` VALUES (91,'2015-12-21 10:29:54','2015-12-21 10:29:54','','DELETE');
INSERT INTO `translation_tags` VALUES (92,'2015-12-21 10:49:01','2015-12-21 10:49:01','','Are you sure?');
INSERT INTO `translation_tags` VALUES (93,'2015-12-21 11:43:32','2015-12-21 11:43:32','','Status plaćanja');
INSERT INTO `translation_tags` VALUES (94,'2015-12-21 12:10:20','2015-12-21 12:10:20','','Najmodavac');
INSERT INTO `translation_tags` VALUES (95,'2015-12-21 15:27:58','2015-12-21 15:27:58','','Profile');
INSERT INTO `translation_tags` VALUES (96,'2015-12-21 15:30:19','2015-12-21 15:30:19','','Sign out');
INSERT INTO `translation_tags` VALUES (97,'2015-12-22 09:20:32','2015-12-22 09:20:32','','Save');
INSERT INTO `translation_tags` VALUES (98,'2015-12-22 12:06:22','2015-12-22 12:06:22','','Od datuma');
INSERT INTO `translation_tags` VALUES (99,'2015-12-22 12:06:22','2015-12-22 12:06:22','','Do datuma');
INSERT INTO `translation_tags` VALUES (100,'2015-12-23 10:42:44','2015-12-23 10:42:44','','Vezani dokument');
INSERT INTO `translation_tags` VALUES (101,'2015-12-24 10:24:12','2015-12-24 10:24:12','','Prati datum');
INSERT INTO `translation_tags` VALUES (102,'2015-12-27 13:53:19','2015-12-27 13:53:19','','Reset filters');
INSERT INTO `translation_tags` VALUES (103,'2015-12-28 10:54:18','2015-12-28 10:54:18','','Uplaćeni iznos');
INSERT INTO `translation_tags` VALUES (104,'2015-12-28 11:35:41','2015-12-28 11:35:41','','Status najma');
INSERT INTO `translation_tags` VALUES (105,'2015-12-28 11:46:25','2015-12-28 11:46:25','','Upiši pregled');
INSERT INTO `translation_tags` VALUES (106,'2015-12-28 16:39:50','2015-12-28 16:39:50','','<i class=\'fa fa-save\'></i> Upiši pregled');
INSERT INTO `translation_tags` VALUES (107,'2015-12-28 16:41:46','2015-12-28 16:41:46','','Zapamti provjeru');
INSERT INTO `translation_tags` VALUES (108,'2015-12-28 16:41:46','2015-12-28 16:41:46','','<i class=\'fa fa-save\'></i> Zapamti provjeru');
INSERT INTO `translation_tags` VALUES (109,'2015-12-29 12:06:24','2015-12-29 12:06:24','','Vrsta usluge');
INSERT INTO `translation_tags` VALUES (110,'2015-12-29 12:49:33','2015-12-29 12:49:33','','Text');
INSERT INTO `translation_tags` VALUES (111,'2015-12-29 12:49:33','2015-12-29 12:49:33','','Docs');
INSERT INTO `translation_tags` VALUES (112,'2015-12-29 12:49:33','2015-12-29 12:49:33','','Created By');
INSERT INTO `translation_tags` VALUES (113,'2015-12-29 12:49:33','2015-12-29 12:49:33','','Assigned To');
INSERT INTO `translation_tags` VALUES (114,'2015-12-29 12:49:33','2015-12-29 12:49:33','','Followers');
INSERT INTO `translation_tags` VALUES (115,'2015-12-29 12:49:33','2015-12-29 12:49:33','','Done');
INSERT INTO `translation_tags` VALUES (116,'2015-12-29 12:49:33','2015-12-29 12:49:33','','Done At');
INSERT INTO `translation_tags` VALUES (117,'2015-12-29 12:49:33','2015-12-29 12:49:33','','Done By');
INSERT INTO `translation_tags` VALUES (118,'2015-12-31 09:33:52','2015-12-31 09:33:52','','Status servisa');
INSERT INTO `translation_tags` VALUES (119,'2015-12-31 09:36:55','2015-12-31 09:36:55','','Odvesti na kilometara');
INSERT INTO `translation_tags` VALUES (120,'2016-01-04 13:12:04','2016-01-04 13:12:04','','Title');
INSERT INTO `translation_tags` VALUES (121,'2016-01-04 13:12:04','2016-01-04 13:12:04','','Permissions');
INSERT INTO `translation_tags` VALUES (122,'2016-01-12 12:22:55','2016-01-12 12:22:55','','Prikaži ovo vozilo u rasporedu');
/*!40000 ALTER TABLE `translation_tags` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table translations
#

DROP TABLE IF EXISTS `translations`;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `translation_tag_id` int(10) unsigned NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `translation` text COLLATE utf8_unicode_ci NOT NULL,
  `auto` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table translations
#
LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;

INSERT INTO `translations` VALUES (1,'2015-12-15 02:21:15','2015-12-20 13:48:02',57,'hr','Traži',0);
INSERT INTO `translations` VALUES (2,'2015-12-15 02:21:15','2015-12-15 02:21:15',2,'hr','Vozilo',1);
INSERT INTO `translations` VALUES (3,'2015-12-15 02:21:15','2015-12-20 13:46:59',3,'hr','Po stranici',0);
INSERT INTO `translations` VALUES (4,'2015-12-15 02:21:15','2015-12-15 02:21:15',4,'hr','No matching results found.',1);
INSERT INTO `translations` VALUES (5,'2015-12-15 02:21:19','2015-12-15 02:21:19',5,'hr','Po stranici',1);
INSERT INTO `translations` VALUES (6,'2015-12-15 02:21:36','2015-12-15 02:21:36',6,'hr','Mjesto',1);
INSERT INTO `translations` VALUES (7,'2015-12-15 09:00:53','2015-12-15 09:00:53',7,'hr','Registarska tablica',1);
INSERT INTO `translations` VALUES (8,'2015-12-15 09:00:53','2015-12-15 09:00:53',8,'hr','Naziv vozila',1);
INSERT INTO `translations` VALUES (9,'2015-12-15 09:00:53','2015-12-15 09:00:53',9,'hr','Marka vozila',1);
INSERT INTO `translations` VALUES (10,'2015-12-15 09:00:53','2015-12-15 09:00:53',10,'hr','Model',1);
INSERT INTO `translations` VALUES (11,'2015-12-15 09:00:53','2015-12-15 09:00:53',11,'hr','Tip',1);
INSERT INTO `translations` VALUES (12,'2015-12-15 09:00:53','2015-12-15 09:00:53',12,'hr','Godina proizvodnje',1);
INSERT INTO `translations` VALUES (13,'2015-12-15 09:00:53','2015-12-15 09:00:53',13,'hr','Veličina guma',1);
INSERT INTO `translations` VALUES (14,'2015-12-15 09:00:53','2015-12-15 09:00:53',14,'hr','Boja',1);
INSERT INTO `translations` VALUES (15,'2015-12-15 09:00:53','2015-12-15 09:00:53',15,'hr','Boja (kod)',1);
INSERT INTO `translations` VALUES (16,'2015-12-15 09:00:53','2015-12-15 09:00:53',16,'hr','Broj šasije',1);
INSERT INTO `translations` VALUES (17,'2015-12-15 09:00:53','2015-12-15 09:00:53',17,'hr','Partner',1);
INSERT INTO `translations` VALUES (18,'2015-12-15 09:00:53','2015-12-15 09:00:53',18,'hr','Leasing kuća',1);
INSERT INTO `translations` VALUES (19,'2015-12-15 09:00:53','2015-12-15 09:00:53',19,'hr','Rata leasinga',1);
INSERT INTO `translations` VALUES (20,'2015-12-15 09:00:53','2015-12-15 09:00:53',20,'hr','Registriran do',1);
INSERT INTO `translations` VALUES (21,'2015-12-15 09:00:53','2015-12-15 09:00:53',21,'hr','AO osiguranje vrijedi do',1);
INSERT INTO `translations` VALUES (22,'2015-12-15 09:00:53','2015-12-15 09:00:53',22,'hr','AO osg.kuća',1);
INSERT INTO `translations` VALUES (23,'2015-12-15 09:00:53','2015-12-15 09:00:53',23,'hr','Kasko vrijedi do',1);
INSERT INTO `translations` VALUES (24,'2015-12-15 09:00:53','2015-12-15 09:00:53',24,'hr','Kasko osg.kuća',1);
INSERT INTO `translations` VALUES (25,'2015-12-15 09:00:53','2015-12-15 09:00:53',25,'hr','Periodički pregled vrijedi do',1);
INSERT INTO `translations` VALUES (26,'2015-12-15 09:00:53','2015-12-15 09:00:53',26,'hr','Protupožarni aparat vrijedi do',1);
INSERT INTO `translations` VALUES (27,'2015-12-15 09:00:53','2015-12-15 09:00:53',27,'hr','Dokumenti',1);
INSERT INTO `translations` VALUES (28,'2015-12-15 09:20:42','2015-12-15 09:20:42',28,'hr','Name',1);
INSERT INTO `translations` VALUES (29,'2015-12-15 09:20:42','2015-12-15 09:20:42',29,'hr','Status',1);
INSERT INTO `translations` VALUES (30,'2015-12-15 09:20:42','2015-12-15 09:20:42',30,'hr','Client',1);
INSERT INTO `translations` VALUES (31,'2015-12-15 09:20:42','2015-12-15 09:20:42',31,'hr','Data',1);
INSERT INTO `translations` VALUES (32,'2015-12-15 09:20:42','2015-12-15 09:20:42',32,'hr','Pdf',1);
INSERT INTO `translations` VALUES (33,'2015-12-15 09:20:49','2015-12-15 09:20:49',33,'hr','Ime',1);
INSERT INTO `translations` VALUES (34,'2015-12-15 09:20:49','2015-12-15 09:20:49',34,'hr','Prezime',1);
INSERT INTO `translations` VALUES (35,'2015-12-15 09:20:49','2015-12-15 09:20:49',35,'hr','Tvrtka (kratki naziv)',1);
INSERT INTO `translations` VALUES (36,'2015-12-15 09:20:49','2015-12-15 09:20:49',36,'hr','Tvrtka (dugi naziv)',1);
INSERT INTO `translations` VALUES (37,'2015-12-15 09:20:49','2015-12-15 09:20:49',37,'hr','OIB',1);
INSERT INTO `translations` VALUES (38,'2015-12-15 09:20:49','2015-12-15 09:20:49',38,'hr','MB',1);
INSERT INTO `translations` VALUES (39,'2015-12-15 09:20:49','2015-12-15 09:20:49',39,'hr','Adresa',1);
INSERT INTO `translations` VALUES (40,'2015-12-15 09:20:49','2015-12-15 09:20:49',40,'hr','Adresa 2',1);
INSERT INTO `translations` VALUES (41,'2015-12-15 09:20:49','2015-12-15 09:20:49',41,'hr','Poštanski broj',1);
INSERT INTO `translations` VALUES (42,'2015-12-15 09:20:49','2015-12-15 09:20:49',42,'hr','Država',1);
INSERT INTO `translations` VALUES (43,'2015-12-15 09:20:49','2015-12-15 09:20:49',43,'hr','Tip kontakta',1);
INSERT INTO `translations` VALUES (44,'2015-12-15 09:20:49','2015-12-15 09:20:49',44,'hr','Email',1);
INSERT INTO `translations` VALUES (45,'2015-12-15 09:20:49','2015-12-15 09:20:49',45,'hr','Mobilni tel.',1);
INSERT INTO `translations` VALUES (46,'2015-12-15 09:20:49','2015-12-15 09:20:49',46,'hr','Tel.',1);
INSERT INTO `translations` VALUES (47,'2015-12-15 09:20:49','2015-12-15 09:20:49',47,'hr','Fax',1);
INSERT INTO `translations` VALUES (48,'2015-12-15 09:20:49','2015-12-15 09:20:49',48,'hr','Web',1);
INSERT INTO `translations` VALUES (49,'2015-12-15 09:20:49','2015-12-15 09:20:49',49,'hr','Napomena',1);
INSERT INTO `translations` VALUES (50,'2015-12-15 09:52:53','2015-12-15 09:52:53',50,'hr','Tip servisa',1);
INSERT INTO `translations` VALUES (51,'2015-12-15 09:52:53','2015-12-15 09:52:53',51,'hr','Početni datum servisa',1);
INSERT INTO `translations` VALUES (52,'2015-12-15 09:52:53','2015-12-15 09:52:53',52,'hr','Završni datum servisa',1);
INSERT INTO `translations` VALUES (53,'2015-12-15 09:52:53','2015-12-15 09:52:53',53,'hr','Kilometraža',1);
INSERT INTO `translations` VALUES (54,'2015-12-15 09:52:53','2015-12-15 09:52:53',54,'hr','Servisni centar',1);
INSERT INTO `translations` VALUES (55,'2015-12-15 09:52:53','2015-12-15 09:52:53',55,'hr','Cijena servisa',1);
INSERT INTO `translations` VALUES (56,'2015-12-15 09:52:53','2015-12-15 09:52:53',56,'hr','Cijena dijelova',1);
INSERT INTO `translations` VALUES (57,'2015-12-15 09:54:20','2015-12-15 09:54:20',57,'hr','Search',1);
INSERT INTO `translations` VALUES (58,'2015-12-15 09:54:20','2015-12-15 09:54:20',58,'hr','Usergroup',1);
INSERT INTO `translations` VALUES (59,'2015-12-15 09:54:28','2015-12-15 09:54:28',59,'hr','Username',1);
INSERT INTO `translations` VALUES (60,'2015-12-15 09:54:28','2015-12-15 09:54:28',60,'hr','Password',1);
INSERT INTO `translations` VALUES (61,'2015-12-15 09:54:28','2015-12-15 09:54:28',61,'hr','First Name',1);
INSERT INTO `translations` VALUES (62,'2015-12-15 09:54:28','2015-12-15 09:54:28',62,'hr','Last Name',1);
INSERT INTO `translations` VALUES (63,'2015-12-17 18:37:00','2015-12-17 18:37:00',63,'hr','Prešao kilometara',1);
INSERT INTO `translations` VALUES (64,'2015-12-17 18:37:00','2015-12-17 18:37:00',64,'hr','Redoviti servis svakih (km)',1);
INSERT INTO `translations` VALUES (65,'2015-12-17 18:41:46','2015-12-17 18:41:46',65,'hr','Početni datum',1);
INSERT INTO `translations` VALUES (66,'2015-12-17 18:41:46','2015-12-17 18:41:46',66,'hr','Početno vrijeme',1);
INSERT INTO `translations` VALUES (67,'2015-12-17 18:41:46','2015-12-17 18:41:46',67,'hr','Završni datum',1);
INSERT INTO `translations` VALUES (68,'2015-12-17 18:41:46','2015-12-17 18:41:46',68,'hr','Završno vrijeme',1);
INSERT INTO `translations` VALUES (69,'2015-12-17 18:41:46','2015-12-17 18:41:46',69,'hr','Početna kilometraža',1);
INSERT INTO `translations` VALUES (70,'2015-12-17 18:41:46','2015-12-17 18:41:46',70,'hr','Završna kilometraža',1);
INSERT INTO `translations` VALUES (71,'2015-12-17 18:41:46','2015-12-17 18:41:46',71,'hr','Ukupna cijena',1);
INSERT INTO `translations` VALUES (72,'2015-12-17 18:41:46','2015-12-17 18:41:46',72,'hr','Klijent',1);
INSERT INTO `translations` VALUES (73,'2015-12-17 18:41:46','2015-12-17 18:41:46',73,'hr','Preko koje firme',1);
INSERT INTO `translations` VALUES (74,'2015-12-17 18:42:25','2015-12-17 18:42:25',74,'hr','Naslov',1);
INSERT INTO `translations` VALUES (75,'2015-12-17 18:42:25','2015-12-17 18:42:25',75,'hr','Datum događaja',1);
INSERT INTO `translations` VALUES (76,'2015-12-17 18:42:25','2015-12-17 18:42:25',76,'hr','Mjesto događaja',1);
INSERT INTO `translations` VALUES (77,'2015-12-17 18:42:25','2015-12-17 18:42:25',77,'hr','Km na vozilu',1);
INSERT INTO `translations` VALUES (78,'2015-12-17 18:42:25','2015-12-17 18:42:25',78,'hr','Opis',1);
INSERT INTO `translations` VALUES (79,'2015-12-17 18:42:25','2015-12-17 18:42:25',79,'hr','Tko je oštetio vozilo',1);
INSERT INTO `translations` VALUES (80,'2015-12-18 12:52:15','2015-12-18 12:52:15',80,'hr','Tvrtka',1);
INSERT INTO `translations` VALUES (81,'2015-12-18 12:58:09','2015-12-18 12:58:09',81,'hr','Kontakt osoba',1);
INSERT INTO `translations` VALUES (82,'2015-12-18 12:58:10','2015-12-18 12:58:10',82,'hr','Kontakt telefon',1);
INSERT INTO `translations` VALUES (83,'2015-12-18 13:00:45','2015-12-18 13:00:45',83,'hr','Kontakt mobitel',1);
INSERT INTO `translations` VALUES (84,'2015-12-18 13:05:48','2015-12-18 13:05:48',84,'hr','Mobitel',1);
INSERT INTO `translations` VALUES (85,'2015-12-20 13:46:31','2015-12-20 13:46:31',85,'hr','Language',1);
INSERT INTO `translations` VALUES (86,'2015-12-20 13:46:31','2015-12-20 13:46:31',86,'hr','Translation',1);
INSERT INTO `translations` VALUES (87,'2015-12-20 13:46:32','2015-12-20 13:46:32',87,'hr','Actions',1);
INSERT INTO `translations` VALUES (88,'2015-12-20 13:46:49','2015-12-20 13:46:49',88,'hr','Tag',1);
INSERT INTO `translations` VALUES (89,'2015-12-20 13:46:49','2015-12-20 13:46:49',89,'hr','Auto',1);
INSERT INTO `translations` VALUES (90,'2015-12-20 13:48:14','2015-12-20 13:48:14',1,'hr','Pretraga',1);
INSERT INTO `translations` VALUES (91,'2015-12-21 10:29:54','2015-12-21 10:34:03',90,'hr','PROMIJENI',0);
INSERT INTO `translations` VALUES (92,'2015-12-21 10:29:54','2015-12-21 10:34:23',91,'hr','OBRIŠI',0);
INSERT INTO `translations` VALUES (93,'2015-12-21 10:49:02','2015-12-21 10:49:28',92,'hr','Jeste li sigurni?',0);
INSERT INTO `translations` VALUES (94,'2015-12-21 11:43:32','2015-12-21 11:43:32',93,'hr','Status plaćanja',1);
INSERT INTO `translations` VALUES (95,'2015-12-21 12:10:20','2015-12-21 12:10:20',94,'hr','Najmodavac',1);
INSERT INTO `translations` VALUES (96,'2015-12-21 15:27:58','2015-12-21 15:27:58',95,'hr','Profile',1);
INSERT INTO `translations` VALUES (97,'2015-12-21 15:30:19','2015-12-21 15:30:19',96,'hr','Sign out',1);
INSERT INTO `translations` VALUES (98,'2015-12-22 09:20:32','2015-12-22 09:20:32',97,'hr','Save',1);
INSERT INTO `translations` VALUES (99,'2015-12-22 12:06:22','2015-12-22 12:06:22',98,'hr','Od datuma',1);
INSERT INTO `translations` VALUES (100,'2015-12-22 12:06:22','2015-12-22 12:06:22',99,'hr','Do datuma',1);
INSERT INTO `translations` VALUES (101,'2015-12-23 10:42:44','2015-12-23 10:42:44',100,'hr','Vezani dokument',1);
INSERT INTO `translations` VALUES (102,'2015-12-24 10:24:12','2015-12-24 10:24:12',101,'hr','Prati datum',1);
INSERT INTO `translations` VALUES (103,'2015-12-27 13:53:19','2015-12-27 13:53:19',102,'hr','Reset filters',1);
INSERT INTO `translations` VALUES (104,'2015-12-28 10:54:18','2015-12-28 10:54:18',103,'hr','Uplaćeni iznos',1);
INSERT INTO `translations` VALUES (105,'2015-12-28 11:35:41','2015-12-28 11:35:41',104,'hr','Status najma',1);
INSERT INTO `translations` VALUES (106,'2015-12-28 11:46:25','2015-12-28 11:46:25',105,'hr','Upiši pregled',1);
INSERT INTO `translations` VALUES (107,'2015-12-28 16:39:50','2015-12-28 16:39:50',106,'hr','<i class=\'fa fa-save\'></i> Upiši pregled',1);
INSERT INTO `translations` VALUES (108,'2015-12-28 16:41:46','2015-12-28 16:41:46',107,'hr','Zapamti provjeru',1);
INSERT INTO `translations` VALUES (109,'2015-12-28 16:41:46','2015-12-28 16:41:46',108,'hr','<i class=\'fa fa-save\'></i> Zapamti provjeru',1);
INSERT INTO `translations` VALUES (110,'2015-12-29 12:06:24','2015-12-29 12:06:24',109,'hr','Vrsta usluge',1);
INSERT INTO `translations` VALUES (111,'2015-12-29 12:49:33','2015-12-29 12:49:33',110,'hr','Text',1);
INSERT INTO `translations` VALUES (112,'2015-12-29 12:49:33','2015-12-29 12:49:33',111,'hr','Docs',1);
INSERT INTO `translations` VALUES (113,'2015-12-29 12:49:33','2015-12-29 12:49:33',112,'hr','Created By',1);
INSERT INTO `translations` VALUES (114,'2015-12-29 12:49:33','2015-12-29 12:49:33',113,'hr','Assigned To',1);
INSERT INTO `translations` VALUES (115,'2015-12-29 12:49:33','2015-12-29 12:49:33',114,'hr','Followers',1);
INSERT INTO `translations` VALUES (116,'2015-12-29 12:49:33','2015-12-29 12:49:33',115,'hr','Done',1);
INSERT INTO `translations` VALUES (117,'2015-12-29 12:49:33','2015-12-29 12:49:33',116,'hr','Done At',1);
INSERT INTO `translations` VALUES (118,'2015-12-29 12:49:33','2015-12-29 12:49:33',117,'hr','Done By',1);
INSERT INTO `translations` VALUES (119,'2015-12-31 09:33:52','2015-12-31 09:33:52',118,'hr','Status servisa',1);
INSERT INTO `translations` VALUES (120,'2015-12-31 09:36:55','2015-12-31 09:36:55',119,'hr','Odvesti na kilometara',1);
INSERT INTO `translations` VALUES (121,'2016-01-04 13:12:04','2016-01-04 13:12:04',120,'hr','Title',1);
INSERT INTO `translations` VALUES (122,'2016-01-04 13:12:04','2016-01-04 13:12:04',121,'hr','Permissions',1);
INSERT INTO `translations` VALUES (123,'2016-01-12 12:22:55','2016-01-12 12:22:55',122,'hr','Prikaži ovo vozilo u rasporedu',1);
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table user_groups
#

DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE `user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table user_groups
#
LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;

INSERT INTO `user_groups` VALUES (1,'2015-12-13 14:30:54','2016-01-13 15:06:07','Administrator','admin','access.admin,menu.user,user.create,user.switchto,user.ownpassword,manage.usergroups,menu.dashboard,menu.calendar,menu.booking,menu.contact,menu.vehicle,menu.vehicle-service,menu.damage,menu.checklist');
INSERT INTO `user_groups` VALUES (2,'2016-01-04 13:12:45','2016-01-12 12:51:24','Zaposlenik','worker','access.admin,user.ownpassword,menu.calendar,menu.booking,menu.contact,menu.vehicle,menu.vehicle-service,menu.damage,menu.checklist');
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table users
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `usergroup` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `blocked` tinyint(3) unsigned DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table users
#
LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` VALUES (1,'2015-12-13 14:30:54','2016-01-15 12:18:40','tihomir.jauk@gmail.com','admin','$2y$10$7Bz.2Pjvx2BgsOWB.a.9IOUvgm1swWIyB3v/MBbejM51n3.dsUJpW','admin','Tihomir','Jauk','',NULL,'2016-01-15 12:18:40');
INSERT INTO `users` VALUES (2,'2015-12-15 09:56:54','2016-01-06 14:02:07','info@komet-prijevoz.hr','icukac','$2y$10$vco2ZznU5gwaVNWEd/8erOAScI1JdIns.yofV48LV8pQM6tX3PtpW','worker','Igor','Čukac','',NULL,'2015-12-21 14:08:02');
INSERT INTO `users` VALUES (3,'2016-01-12 11:51:56','2016-01-12 11:52:03','test@test.com','test','$2y$10$YgS5bgMVP/O5dubBhYOW7e4n76eB.2bLHUbV5NY8d6G06EEqNF.FG','worker','Testni','','',NULL,'2016-01-12 11:52:03');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table vehicle_services
#

DROP TABLE IF EXISTS `vehicle_services`;
CREATE TABLE `vehicle_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `vehicle_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Redovan',
  `from` date DEFAULT NULL,
  `to` date DEFAULT NULL,
  `service_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parts_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `vehicle_km` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `service_id` int(11) DEFAULT NULL,
  `service_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table vehicle_services
#
LOCK TABLES `vehicle_services` WRITE;
/*!40000 ALTER TABLE `vehicle_services` DISABLE KEYS */;

INSERT INTO `vehicle_services` VALUES (3,'2015-12-18 10:48:41','2016-01-04 12:59:54',3,'Redovan','2015-11-10',NULL,'444','','1','','- redoviti servis napravljen ranije jer je vozilo išlo u dugu rentu','116000',5,3);
INSERT INTO `vehicle_services` VALUES (4,'2015-12-21 11:18:54','2016-01-12 12:09:42',8,'Pregled','2015-12-21','2015-12-22','','','0','','- redoviti servis i pregled zbog duge rente Strojopromet','219761',5,1);
INSERT INTO `vehicle_services` VALUES (5,'2015-12-21 11:25:45','2016-01-04 13:00:00',5,'Redovan','2015-11-10','2015-11-10','','555','1','','- redoviti servis','80687',5,1);
INSERT INTO `vehicle_services` VALUES (6,'2015-12-21 11:28:12','2016-01-04 12:59:47',9,'Redovan','2015-11-20','2015-11-21','1111','2222','1','','- reoviti servis i montaža guma','111396',5,2);
INSERT INTO `vehicle_services` VALUES (7,'2016-01-04 12:43:08','2016-01-04 12:43:08',2,'Redovan',NULL,NULL,'','','','','','',0,1);
INSERT INTO `vehicle_services` VALUES (8,'2016-01-04 12:43:40','2016-01-04 12:49:42',4,'Redovan',NULL,NULL,'','','0','','','220000',6,1);
/*!40000 ALTER TABLE `vehicle_services` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table vehicles
#

DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `brand` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `partner` tinyint(4) NOT NULL DEFAULT '0',
  `year` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'White',
  `color_hex` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#FFFFFF',
  `chassis_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `leasing` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `leasing_amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tire_size` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pp_expires` date DEFAULT NULL,
  `casco_expires` date DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `licence_plate` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `licence_expires` date DEFAULT NULL,
  `ppa_expires` date DEFAULT NULL,
  `ao_expires` date DEFAULT NULL,
  `ao_house` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `kasko_expires` date DEFAULT NULL,
  `kasko_house` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `km` int(10) unsigned NOT NULL DEFAULT '0',
  `service_km` int(10) unsigned NOT NULL DEFAULT '50000',
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  `show_on_calendar` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table vehicles
#
LOCK TABLES `vehicles` WRITE;
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;

INSERT INTO `vehicles` VALUES (2,'2015-12-17 19:52:22','2016-01-12 12:26:03','Renault Master RI-815-UA','Renault ','Master','Teretni',1,'2011','Bijela','#ffffff','VF1MAF4CE43355780','IMPULS LEASING','2065','225/65/16c','2016-01-20',NULL,'- financijski leasing\r\n- EURO+ asistencija\r\n- kasko se automatski obnavlja na rate','RI-815-UA',NULL,'2016-01-20','2016-01-20','CROATIA OSIGURANJE d.d.',NULL,'TEST',279681,40000,'',0);
INSERT INTO `vehicles` VALUES (3,'2015-12-17 20:05:57','2015-12-21 09:03:35','Renault Master RI-629-UP','Renault','Master','Teretni',1,'2014','Bijela','#ffffff','VF1MAF4DE49720410','IMPULS LEASING','3090','225/65/16c','2016-05-06',NULL,'- operativni leasing\r\n- EURO+ asistencija\r\n- kasko se automatski obnavlja na rate','RI-629-UP','2016-05-06','2016-05-06','2016-05-06','ALLIANZ',NULL,'',119645,0,'',1);
INSERT INTO `vehicles` VALUES (4,'2015-12-18 10:08:28','2015-12-18 10:44:42','Fiat Ducato RI-654-UP','Fiat','Ducato','Teretni',1,'2011','Bijela','#000000','ZFA25000001961707','IMPULS LEASING','2524','215/70/15c','2016-04-25',NULL,'- financijski leasing\r\n- EURO+ asistencija\r\n- kasko se automatski obnavlja na rate','RI-654-UP','2016-04-20','2016-04-25','2016-04-20','CROATIA OSIGURANJE d.d.',NULL,'',207039,0,'',1);
INSERT INTO `vehicles` VALUES (5,'2015-12-18 10:15:19','2015-12-21 11:04:40','Renault Master RI-838-VH','Renault ','Master','Teretni',1,'2013','Bijela','#000000','VF1MAF4DE49002123','IMPULS LEASING','2830','225/65/16c','2016-03-30',NULL,'- financijski leasing\r\n- EURO+ asistencija\r\n- kasko se automatski obnavlja na rate','RI-838-VH','2016-09-29','2016-03-30','2016-09-29','UNIQA OSIGURANJE','0000-00-00','ALLIANZ OSIGURANJE',83000,40000,'9,10',1);
INSERT INTO `vehicles` VALUES (6,'2015-12-18 10:20:02','2015-12-18 10:21:36','Renault Master RI-540-VN','Renault','Master','Teretni',1,'2015','Bijela','#000000','VF1MAF4SE51411871','IMPULS LEASING','3197','225/65/16c','2016-05-27',NULL,'- financijski leasing\r\n - EURO+ asistencija\r\n - kasko se automatski obnavlja na rate','RI-540-VN','2016-05-27','2016-05-27','2016-05-26','CROATIA OSIGURANJE d.d.',NULL,'',32000,40000,'',1);
INSERT INTO `vehicles` VALUES (7,'2015-12-18 10:27:13','2015-12-18 10:27:13','Opel Vivaro RI-121-SR','Opel','Vivaro','Putnički',1,'2011','Met.siva','#7a7676','W0LJ7BHBSBV643848','IMPULS LEASING','2103','215/65/16c','2016-06-30',NULL,'- finacijski leasing\r\n- kasko u Eurohercu treba\r\nsvake godine tražiti na rate','RI-121-SR','2016-06-30','2016-05-05','2016-06-30','CROATIA OSIGURANJE d.d.',NULL,'',250000,40000,'',1);
INSERT INTO `vehicles` VALUES (8,'2015-12-18 10:31:35','2015-12-21 11:18:54','Opel Vivaro RI-165-UA','Opel','Vivaro','Putnički',1,'2011','Met.siva','#000000','W0LJ7BHB6BV623525','IMPULS LEASING','2333','215/65/16c','2016-06-14',NULL,'-svi podatci provjereni\r\n- financijski leasing\r\n- EURO+ asistencija\r\n- kasko se automatski sam obnavlja','RI-165-UA','2016-06-14','2016-12-20','2016-06-14','CROATIA OSIGURANJE d.d.','2016-04-18','ALLIANZ OSIGURANJE',219761,40000,'12,13',1);
INSERT INTO `vehicles` VALUES (9,'2015-12-18 10:37:48','2015-12-21 11:29:05','VW Transporter T5 RI-668-TC','Volkswagen','Transporter T%','Putnički',1,'2011','Bijela','#ffffff','WV2ZZZ7HZCH029963','IMPULS LEASING','2830','215/65/16c','2016-02-15',NULL,'-financijski leasing\r\n- EURO+ asistencija\r\n- kasko se automatski\r\nobnavlja na rate','RI-668-TC','2016-09-28','2016-09-06','2016-09-28','UNIQA OSIGURANJE','0000-00-00','',116000,30000,'',1);
INSERT INTO `vehicles` VALUES (10,'2015-12-18 10:42:55','2016-01-12 12:29:01','Opel Vivaro RI-353-UE','Opel','Vivaro','Putnički',1,'2013','Met.siva','#000000','W0LJ7B7BSCV639987','IMPULS LEASING','2830','215/65/16c','2016-01-18',NULL,'- finansijski leasing\r\n- EURO+ asistencija\r\n- kasko se automatski\r\nobnavlja na rate','RI-353-UE','2016-07-18','2015-05-01','2016-07-18','UNIQA OSIGURANJE',NULL,'',117000,40000,'',0);
INSERT INTO `vehicles` VALUES (11,'2015-12-18 11:48:40','2015-12-18 11:48:40','Renault Trafic','Renault','Trafic','Putnički',1,'2015','Bijela','#ffffff','','IMPULS LEASING','3850','2015/65/16c','0000-00-00',NULL,'- financiski leasing\r\n- EURO+ asistencija\r\n- kasko sa automatski\r\nobnavlja na rate','','0000-00-00','0000-00-00','0000-00-00','ALLIANZ OSIGURANJE','0000-00-00','ALLINAZ OSIGURANJE',2,40000,'',1);
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;
UNLOCK TABLES;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
