<?php
class AdminDamageController extends AdminController
{
    public $model = 'Damage';
    public $baseurl = '/admin/damage';

    function index()
    {
        $model = $this->model;

        Doc::title("Pregled oštećenja");

        $items = new $model();

        if( Auth::can('manage.damage') ){
            $buttons = Forms::make();
            $buttons->btnAdd('Evidentiraj novo oštećenje',$this->baseurl.'/edit');
        }

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name,vehicle_km,where,description,note' );
        }
        // search vehicle
        if( $filter->notEmpty('vehicle_id') ){
            $items = $items->whereVehicleId( $filter->vehicle_id );
        }
        // filter period from
        if( $filter->notEmpty('from') ){
            $items = $items->where( DB::raw('DATE(`date`)'),'>=', $filter->from );
        }
        // filter period to
        if( $filter->notEmpty('to') ){
            $items = $items->where( DB::raw('DATE(`date`)'),'<=', $filter->to );
        }


        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Pretraga')->search('keywords')->klass('pull-left')->placeholder('po ključnim riječima');
        $form->label('Vozilo')->select2('vehicle_id')->options(Vehicle::options());
        $form->label('Od datuma')->datePicker('from');
        $form->label('Do datuma')->datePicker('to');
        $form->label('Per page')->select('perpage')->values([10,20,50,100]);


        $items = $items->orderBy('date','DESC');
        $items = $filter->paginate( $items )->get();

        $table = Table::make()
                ->headings(['Datum','Vozilo','Kilometri','Mjesto događaja','Status','Opis','Napomena','Akcije']);

        $statuses = Damage::$statuses;

        foreach($items as $item){
            $data = [];
            $data[] = UI::date($item->date);
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->vehicle->title.'</a>';
            $data[] = $item->vehicle_km;
            $data[] = $item->where;
            if( in_array($item->status,[0,1]) ){
                $data[] = '<span class="label label-danger">'.strtoupper($statuses[$item->status]).'</span>';
            }else{
                $data[] = '<span class="label label-primary">'.strtoupper($statuses[$item->status]).'</span>';
            }
            $data[] = $item->description;
            $data[] = $item->note;

            if( Auth::can('manage.damage') ){
                $actions = UI::btnEdit($item->id,'damage');
                $actions.= UI::btnDelete($item->id,'damage');
            }else{
                $actions = '-';
            }
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');

        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;

        Doc::title('Oštećenje');

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() and Auth::can('manage.damage') ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('damage');

        $form->label('Naslov')->input('name');
        $form->label('Vozilo')->select('vehicle_id')->options(Vehicle::options());
        $form->label('Datum događaja')->datePicker('date')->readonly();
        $form->label('Mjesto događaja')->text('where');
        $form->label('Km na vozilu')->number('vehicle_km');
        $form->label('Opis')->text('description');
        $form->label('Tko je oštetio vozilo')->select2('contact_id')->options(Contact::options());
        $form->label('Status')->radioGroup('status')->options(Damage::$statuses);
        $form->label('Dokumenti')->documents('docs');
        $form->label('Napomena')->text('note');

        $form->btnSave();

        $form->close();

        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 and Auth::can('manage.damage') ){
            $item->delete();
        }
        return redirect('/admin/damage')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
