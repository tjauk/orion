<?php
class AdminReminderController extends AdminController
{
    public $model = 'Reminder';
    public $baseurl = '/admin/reminder';

    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Reminder',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Per page');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');

        $table = Table::make()
                ->headings(['#','Name','Text','Docs','Created By','Assigned To','Followers','Done','Done At','Done By','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->name.'</a>';
            $data[] = $item->text;
            $data[] = $item->docs;
            $data[] = $item->created_by;
            $data[] = $item->assigned_to;
            $data[] = $item->followers;
            $data[] = $item->done;
            $data[] = $item->done_at;
            $data[] = $item->done_by;
            $actions = UI::btnEdit($item->id,'reminder');
            $actions.= UI::btnDelete($item->id,'reminder');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('reminder');
        $form->label('Name')->input('name');

        $form->label('Text')->text('text');

        $form->label('Docs')->text('docs');

        $form->label('Created By')->input('created_by');

        $form->label('Assigned To')->input('assigned_to');

        $form->label('Followers')->text('followers');

        $form->label('Done')->input('done');

        $form->label('Done At')->datepicker('done_at');

        $form->label('Done By')->input('done_by');


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/reminder')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
