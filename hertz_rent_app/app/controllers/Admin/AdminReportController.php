<?php
class AdminReportController extends AdminController
{
    public $model = 'Report';
    public $baseurl = '/admin/report';


    /**
     * Index page
     *
     * @return View
     */
    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Report',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search')->search('keywords')->placeholder('Search');
        $form->label('Per page')->select('perpage')->values([10,20,50,100]);

        $table = Table::make()
                ->headings(['#','Created By','Dated At','From','To','Period','Name','Status','Note','Data','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = $item->created_by;
            $data[] = $item->dated_at;
            $data[] = $item->from;
            $data[] = $item->to;
            $data[] = $item->period;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->name.'</a>';
            $data[] = $item->status;
            $data[] = $item->note;
            $data[] = $item->data;
            $actions = UI::btnEdit($item->id,'report');
            $actions.= UI::btnDelete($item->id,'report');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    /**
     * Edit page
     *
     * @return View
     */
    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        $post = Request::post();
        if( $post ){
            $item->fill($post);
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('report');
        $form->label('Created By')->input('created_by');

        $form->label('Dated At')->datePicker('dated_at');

        $form->label('From')->datePicker('from');

        $form->label('To')->datePicker('to');

        $form->label('Period')->input('period');

        $form->label('Name')->input('name');

        $form->label('Status')->input('status');

        $form->label('Note')->text('note');

        $form->label('Data')->text('data');


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }


    /**
     * Delete
     *
     * @return Response
     */
    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/report')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
