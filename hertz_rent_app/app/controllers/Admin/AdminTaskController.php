<?php
class AdminTaskController extends AdminController
{
    public $model = 'Task';
    public $baseurl = '/admin/task';

    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Task',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'title' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Per page');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');

        $table = Table::make()
                ->headings(['#','Created By','Assigned To','Title','Description','Status','Start','Due','Finish','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = $item->created_by;
            $data[] = $item->assigned_to;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->title.'</a>';
            $data[] = $item->description;
            $data[] = $item->status;
            $data[] = $item->start;
            $data[] = $item->due;
            $data[] = $item->finish;
            $actions = UI::btnEdit($item->id,'task');
            $actions.= UI::btnDelete($item->id,'task');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('task');
        $form->label('Created By')->input('created_by');

        $form->label('Assigned To')->input('assigned_to');

        $form->label('Title')->input('title');

        $form->label('Description')->text('description');

        $form->label('Status')->input('status');

        $form->label('Start')->datepicker('start');

        $form->label('Due')->datepicker('due');

        $form->label('Finish')->datepicker('finish');


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/task')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
