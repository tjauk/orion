<?php
class AdminTimelineController extends AdminController
{
    public $model = 'Booking';
    public $baseurl = '/admin/timeline';

    function index()
    {
        $model = $this->model;
        Doc::title("Raspored");
        return view('admin/timeline/full');
    }

    function timeline()
    {
        $model = $this->model;
        Doc::title("Raspored");
        return view('admin/timeline/timeline');
    }

    function diff($start,$somedate)
    {
        $first = strtotime($start);
        $from = strtotime($somedate);
        $diff = floor(($from-$first)/(60*60*24));
        return $diff;
    }

    function events()
    {
        // GET/POST : start, end, vehicles[]

        $filter = new SearchFilter;
        $filter->fill(Request::all());

        $vehicles = trim($filter->vehicles);

        if( $vehicles=='*' ){
            $vehicles = Vehicle::active()->pluck('id')->toArray();
        }
        if( empty($vehicles) ){
            $vehicles = array();
        }

        if( empty($filter->start) ){
            $filter->set('start',date('Y-m-01',strtotime('now -30 days')));
        }
        if( empty($filter->end) ){
            $filter->set('end',date('Y-m-d',strtotime('first day of month +6 months')));
        }



        // filter only vehicles with show_on_calendar=1 flag
        foreach($vehicles as $index=>$vehicle_id){
            $vehicle = Vehicle::find($vehicle_id);
            if( $vehicle and $vehicle->show_on_calendar<>1 ){
                unset($vehicles[$index]);
            }
        }

        $events = array();

        // bookings
        $bookings = Booking::inPeriod( $filter->start, $filter->end )->get();
        foreach ($bookings as $booking) {
            if( in_array($booking->vehicle_id,$vehicles) ){

                $cell_width = 45;
                // cells
                $cell_from  = $this->diff($filter->start,$booking->from);
                $cell_to    = $this->diff($filter->start,$booking->to);
                //
                $from = UI::dateTime($booking->from);
                $from = str_replace('&nbsp;', ' ', $from);
                //
                $to = UI::dateTime($booking->to);
                $to = str_replace('&nbsp;', ' ', $to);
                //
                $event = [
                    'id'        => $booking->id,
                    'vehicle_id'=> $booking->vehicle_id,
                    'title'     => $booking->client->title,//.' ( '.$from.' - '.$to.' )',
                    'model'     => 'Booking',
                    'allDay'    => false,
                    'start'     => $booking->from,
                    'start_formated'     => (string)UI::dateTime($booking->from),
                    'end'       => $booking->to,
                    'end_formated'     => (string)UI::dateTime($booking->to),
                    'contracted_km' => $booking->contracted_km,
                    'description'   => $booking->vehicle->title,
                    'color'     => '#fff',
                    'backgroundColor' => $booking->vehicle->color_hex,
                    'cell_from' => $cell_from,
                    'cell_to'   => $cell_to,
                ];
                $events[] = $event;
            }
        }


        // services
        $services = VehicleService::inPeriod( $filter->start, $filter->end )->get();
        foreach ($services as $service) {
            if( in_array($service->vehicle_id,$vehicles) ){
                $cell_width = 45;
                // cells
                $cell_from  = $this->diff($filter->start,$service->from);
                $cell_to    = $this->diff($filter->start,$service->to);
                //
                $event = [
                    'id'        => $service->id,
                    'vehicle_id'=> $service->vehicle_id,
                    'title'     => $service->vehicle->title,
                    'model'     => 'VehicleService',
                    'allDay'    => false,
                    'start'     => $service->from,
                    'end'       => $service->to,
                    'description'   => $booking->type,
                    'color'     => '#fff',
                    'backgroundColor' => '#ca0',
                    'cell_from' => $cell_from,
                    'cell_to'   => $cell_to,
                ];
                $events[] = $event;
            }
        }

        // PP
        $vehicles = Vehicle::ppExpires( $filter->start, '2099-01-01' )->whereShowOnCalendar(1)->get();
        foreach ($vehicles as $vehicle) {
            // cells
            $cell_from  = $this->diff($filter->start,$vehicle->pp_expires);
            $cell_to    = $this->diff($filter->start,$vehicle->pp_expires);
            //
            $event = [
                'id'        => $vehicle->id,
                'vehicle_id'=> $vehicle->id,
                'title'     => 'PP '.$vehicle->name,
                'model'     => 'Vehicle',
                'allDay'    => true,
                'start'     => $vehicle->pp_expires,
                'end'       => $vehicle->pp_expires,
                'description'   => 'PP',
                'color'     => '#fff',
                'backgroundColor' => '#DD4B39',
                'cell_from' => $cell_from,
                'cell_to'   => $cell_to,
            ];
            $events[] = $event;
        }
        // PPA
        $vehicles = Vehicle::ppaExpires( $filter->start, '2099-01-01' )->whereShowOnCalendar(1)->get();
        foreach ($vehicles as $vehicle) {
            // cells
            $cell_from  = $this->diff($filter->start,$vehicle->ppa_expires);
            $cell_to    = $this->diff($filter->start,$vehicle->ppa_expires);
            //
            $event = [
                'id'        => $vehicle->id,
                'vehicle_id'=> $vehicle->id,
                'title'     => 'PP aparat '.$vehicle->name,
                'model'     => 'Vehicle',
                'allDay'    => true,
                'start'     => $vehicle->ppa_expires,
                'end'       => $vehicle->ppa_expires,
                'description'   => 'PP aparat',
                'color'     => '#fff',
                'backgroundColor' => '#DD4B39',
                'cell_from' => $cell_from,
                'cell_to'   => $cell_to,
            ];
            $events[] = $event;
        }

        // AO
        $vehicles = Vehicle::aoExpires( $filter->start, '2099-01-01' )->whereShowOnCalendar(1)->get();
        foreach ($vehicles as $vehicle) {
            // cells
            $cell_from  = $this->diff($filter->start,$vehicle->ppa_expires);
            $cell_to    = $this->diff($filter->start,$vehicle->ppa_expires);
            //
            $event = [
                'id'        => $vehicle->id,
                'vehicle_id'=> $vehicle->id,
                'title'     => 'AO osiguranje za '.$vehicle->name,
                'model'     => 'Vehicle',
                'allDay'    => true,
                'start'     => $vehicle->ppa_expires,
                'end'       => $vehicle->ppa_expires,
                'description'   => 'AO osiguranje',
                'color'     => '#fff',
                'backgroundColor' => '#DD4B39',
                'cell_from' => $cell_from,
                'cell_to'   => $cell_to,
            ];
            $events[] = $event;
        }

        // Kasko
        $vehicles = Vehicle::kaskoExpires( $filter->start, '2099-01-01' )->whereShowOnCalendar(1)->get();
        foreach ($vehicles as $vehicle) {
            // cells
            $cell_from  = $this->diff($filter->start,$vehicle->kasko_expires);
            $cell_to    = $this->diff($filter->start,$vehicle->kasko_expires);
            //
            $event = [
                'id'        => $vehicle->id,
                'vehicle_id'=> $vehicle->id,
                'title'     => 'Kasko za '.$vehicle->name,
                'model'     => 'Vehicle',
                'allDay'    => true,
                'start'     => $vehicle->kasko_expires,
                'end'       => $vehicle->kasko_expires,
                'description'   => 'Kasko',
                'color'     => '#fff',
                'backgroundColor' => '#DD4B39',
                'cell_from' => $cell_from,
                'cell_to'   => $cell_to,
            ];
            $events[] = $event;
        }

        // Registracije
        $vozila = Vehicle::licenceExpires( $filter->start, '2099-01-01' )->whereShowOnCalendar(1)->get();
        foreach ($vozila as $vehicle) {
            // cells
            $cell_from  = $this->diff($filter->start,$vehicle->licence_expires);
            $cell_to    = $this->diff($filter->start,$vehicle->licence_expires);
            //
            $event = [
                'id'        => $vehicle->id,
                'vehicle_id'=> $vehicle->id,
                'title'     => 'Registracija za '.$vehicle->name,
                'model'     => 'Vehicle',
                'allDay'    => true,
                'start'     => $vehicle->licence_expires,
                'start_formated'     => (string)UI::dateTime($vehicle->licence_expires),
                'end'       => $vehicle->licence_expires,
                'end_formated'     => (string)UI::dateTime($vehicle->licence_expires),
                'description'   => 'Registracija',
                'color'     => '#fff',
                'backgroundColor' => '#DD4B39',
                'cell_from' => $cell_from,
                'cell_to'   => $cell_to,
            ];
            $events[] = $event;
        }

        return $events;
    }

    function bookings()
    {
        return Booking::all();
    }

    function vehicles()
    {
        return Vehicle::all();
    }

    function vehicleservices()
    {
        return VehicleService::all();
    }

    function damages()
    {
        return Damage::all();
    }

    function contacts()
    {
        return Contact::all();
    }

}
