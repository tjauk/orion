<?php

class AdminVehicleController extends AdminController
{
    public $model = 'Vehicle';
    public $baseurl = '/admin/vehicle';

    public function index()
    {
        $model = $this->model;

        Doc::title('Vozila');

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Dodaj Vozilo', $this->baseurl.'/edit');

        $filter = new SearchFilter();
        $filter->perpage = 100;
        $filter->page = 1;

        // search keywords
        if ($filter->notEmpty('keywords')) {
            $items = $items->search($filter->keywords, 'name');
        }

        // filter vehicle_type (Vrsta vozila)
        $vehicle_types = ['' => '---'];
        foreach (Vehicle::types() as $key => $val) {
            $vehicle_types[$key] = $val;
        }
        if ($filter->notEmpty('vehicle_type') or $filter->vehicle_type == '0') {
            $items = $items->where('vehicles.type', '=', $vehicle_types[$filter->vehicle_type]);
        }
        $byactive_options = ['' => 'Sva vozila', 'with_partners' => 'Samo aktivna', 'without_partners' => 'Bez partnerskih', 'only_partners' => 'Samo partnerska'];
        if ($filter->notEmpty('byactive')) {
            if ($filter->byactive === 'with_partners') {
                $items = $items->active();
            }
            if ($filter->byactive === 'without_partners') {
                $items = $items->active()->where('type', 'NOT LIKE', '%Partnerski%');
            }
            if ($filter->byactive === 'only_partners') {
                $items = $items->active()->where('type', 'LIKE', '%Partnerski%');
            }
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Pretraga');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('po ključnim riječima');
        $form->label('Tip vozila')->select('vehicle_type')->options($vehicle_types);
        $form->label('Aktivna vozila')->select('byactive')->options($byactive_options);
        $form->label('Po stranici')->select('perpage')->values([10, 20, 50, 100])->klass('form-control');

        $table = Table::make()
                ->headings([
                            'Naziv',
                            //'Marka',
                            //'Model',
                            'Tip',
                            //'Partner',
                            'Godina / Boja / Tren.km / (Km u renti)',
                            'Registracija',
                            'Protupožarni',
                            //'Per.Pregled',
                            //'Slj.servis',
                            //'Boja (kod)',
                            //'Broj šasije',
                            //'Leasing',
                            //'Veličina guma',
                            'Dano učešće',
                            'Rata leasinga',
                            'AO mjesečno',
                            'Kasko mjesečno',
                            'Trošak',

                            'Prihod',
                            'U renti',
                            'Km/danu',

                            'Napomena',
                            'Actions',
                        ]);

        $items = $items->orderBy('active', DESC);

        $items = $filter->paginate($items)->get();

        $totals = [
            'leasing_stake' => 0,
            'leasing_amount' => 0,
            'ao_amount' => 0,
            'kasko_amount' => 0,
            'trosak' => 0,
        ];

        foreach ($items as $item) {
            $data = [];
            if ($item->active == 0) {
                $title = '<a href="'.$this->baseurl.'/edit/'.$item->id.'" style="color:#444;text-decoration:line-through;">'.$item->name.'</a>';
            } else {
                $title = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->name.'</a>';
            }

            $data[] = $title;
            //$data[] = $item->brand;
            //$data[] = $item->model;
            $data[] = $item->type;
            //$data[] = $item->partnervozila->company;
            if ($item->tire_type == 'summer') {
                $year = '<i class="fa fa-sun-o"></i>&nbsp;';
            } elseif ($item->tire_type == 'winter') {
                $year = '<i class="fa fa-snowflake-o"></i>&nbsp;';
            } else {
                $year = '<i class="fa fa-warning"></i>&nbsp;';
            }
            $year .= $item->year;
            $color = empty($item->color) ? '-' : $item->color;
            $tkm = $item->km.'&nbsp;km';
            $result = DB::select(DB::raw("
                    SELECT
                        SUM(ABS(km_start-km_end)) AS km
                    FROM bookings
                    WHERE 
                        vehicle_id={$item->id} AND 
                        `status`=2 AND 
                        km_start>0 AND
                        km_end>0
                    "));
            $km_u_renti = intval($result[0]->km);
            $kmr = "($km_u_renti&nbsp;km)";
            $cell = [$year, $color, $tkm, $kmr];
            $data[] = implode('<br/>', $cell);

            $data[] = UI::date($item->licence_expires); // @TODO: paint in red if expired, paint in orange if close
            $data[] = UI::date($item->ppa_expires);
            //$data[] = UI::date($item->pp_expires);
            //$data[] = $item->next_service;

            //$data[] = $item->color_hex;
            //$data[] = $item->chassis_number;
            //$data[] = $item->leasing;
            //$data[] = $item->tire_size;
            $totals['leasing_stake'] += Str::toFloat($item->leasing_stake);
            $data[] = empty($item->leasing_stake) ? '-' : Num::price($item->leasing_stake);

            $totals['leasing_amount'] += Str::toFloat($item->leasing_amount);
            $data[] = empty($item->leasing_amount) ? '-' : Num::price($item->leasing_amount);

            $totals['ao_amount'] += Str::toFloat($item->ao_amount);
            $data[] = empty($item->ao_amount) ? '-' : Num::price($item->ao_amount);

            $totals['kasko_amount'] += Str::toFloat($item->kasko_amount);
            $data[] = empty($item->kasko_amount) ? '-' : Num::price($item->kasko_amount);

            $trosak = Str::toFloat($item->leasing_amount) + Str::toFloat($item->ao_amount) + Str::toFloat($item->kasko_amount);
            $totals['trosak'] += $trosak;
            $data[] = Num::price($trosak);

            // indikatori
            $cell = [];
            $ukupan_prihod = Booking::whereVehicleId($item->id)->whereStatus(2)->sum('price');

            $result = DB::select(DB::raw("
                    SELECT SUM(DATEDIFF(bookings.to,bookings.from)) AS u_renti 
                    FROM bookings 
                    WHERE 
                        vehicle_id={$item->id} AND 
                        status=2 AND 
                        DATEDIFF(bookings.to,bookings.from)>0
                    "));
            $broj_dana_u_renti = $result[0]->u_renti;

            $prihod_po_danu = $broj_dana_u_renti > 0 ? round($ukupan_prihod / $broj_dana_u_renti, 2) : 0;

            // $km = intval($item->km); // @todo: replace with sum of booking start-end km difference
            $prihod_po_km = $km_u_renti > 0 ? round($ukupan_prihod / $km_u_renti, 2) : 0;
            $prosjecno_km_po_danu = $broj_dana_u_renti > 0 ? round($km_u_renti / $broj_dana_u_renti, 2) : 0;

            $cell = [];
            $cell[] = Num::price($ukupan_prihod);
            $cell[] = $prihod_po_danu.'&nbsp/danu';
            $cell[] = $prihod_po_km.'&nbsp/km';
            $data[] = implode('<br/>', $cell);

            $data[] = $broj_dana_u_renti;
            $data[] = $prosjecno_km_po_danu;

            $data[] = $item->note;
            $actions = UI::btnEdit($item->id, 'vehicle');
            $actions .= UI::btnDelete($item->id, 'vehicle');
            $data[] = $actions;
            $table->addRow($data);
        }

        // totals
        $data = [];
        $data[] = '';
        $data[] = '';
        $data[] = '';
        $data[] = '';
        $data[] = '';
        $data[] = b(Num::price($totals['leasing_stake']));
        $data[] = b(Num::price($totals['leasing_amount']));
        $data[] = b(Num::price($totals['ao_amount']));
        $data[] = b(Num::price($totals['kasko_amount']));
        $data[] = b(Num::price($totals['trosak']));
        $data[] = '';
        $data[] = '';
        $data[] = '';
        $data[] = '';
        $data[] = '';
        $table->addRow($data);

        $table = $table->render('admin/table/grid');

        return View::make('admin/list/default')
                    ->put($this, 'controller')
                    ->put($buttons, 'buttons')
                    ->put($filter, 'filter')
                    ->put($form, 'searchform')
                    ->put($table, 'table');
    }

    public function edit($id = 0)
    {
        $model = $this->model;

        Doc::title('Vozilo');

        $item = $model::firstOrNew(['id' => $id]);

        $post = Request::post();
        if ($post) {
            if ($post['active'] == '1') {
                $post['active'] = 1;
            } else {
                $post['active'] = 0;
                $post['show_on_calendar'] = 0;
            }
            $item->fill($post);
            VehicleKm::register($item->id, $item->km);
            $item->save();

            return redirect($this->baseurl)->with('msg', "Snimljeno vozilo {$item->name}");
        }

        $form = Forms::make()->format('EDIT');
        if (!empty($item)) {
            $form->fill($item);
        }
        $form->open('vehicle');

        $form->div('.col-md-6');
        $form->panel('Osnovne informacije');
        $form->label('Registarska tablica')->input('licence_plate');
        $form->label('Naziv vozila')->input('name');
        $form->label('Marka vozila')->input('brand');
        // $form->label('Marka vozila')->simpleTags('brand')->options(['BMW', 'Audi', 'Test'])->max(1)->value(['name' => $item->brand]);
        $form->label('Model')->input('model');
        $form->label('Tip')->radioGroup('type')->values(Vehicle::types());
        $form->label('Broj šasije')->input('chassis_number');
        $form->label('Godina proizvodnje')->select('year')->range(1990, intval(date('Y')));
        $form->label('Prešao kilometara')->number('km');
        $form->label('Redoviti servis svakih (km)')->number('service_km');
        $form->label('Veličina guma')->input('tire_size');
        $form->label('Tip guma')->radiogroup('tire_type')->options(Vehicle::tire_types());
        $form->label('Boja')->input('color');
        //$form->label('Partner')->select2('partner')->options(Contact::options());
        $form->label('Leasing kuća')->input('leasing');
        $form->label('Iznos učešća')->price('leasing_stake')->currency('kn');
        $form->label('Rata leasinga')->price('leasing_amount')->currency('kn');
        $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-6');
        $form->panel('Dodatne informacije (trenutne osg.kuće za vozila)');
        $form->label('Registriran do')->datePicker('licence_expires');
        $form->br();
        $form->label('AO osiguranje vrijedi do')->datePicker('ao_expires');
        $form->label('')->price('ao_amount')->placeholder('mjesečni iznos')->currency('kn / mj.');
        $form->label('')->input('ao_house')->placeholder('naziv kuće'); //.AO osg.kuća
        $form->br();
        $form->label('Kasko vrijedi do')->datePicker('kasko_expires');
        $form->label('')->price('kasko_amount')->placeholder('mjesečni iznos')->currency('kn / mj.');
        $form->label('')->input('kasko_house')->placeholder('naziv kuće'); //.Kasko osg.kuća
        $form->br();
        $form->label('Periodički pregled vrijedi do')->datePicker('pp_expires');
        $form->label('Protupožarni aparat vrijedi do')->datePicker('ppa_expires');
        $form->br();
        $form->label('Prikaži ovo vozilo u rasporedu')->radioGroup('show_on_calendar')->options(['1' => 'Da', '0' => 'Ne']);
        $form->label('Boja za prikaz')->colorPicker('color_hex');
        $form->label('Vozilo je aktivno')->checkBox('active');
        $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-12');
        $form->panel('Dokumenti');
        // prvo prebaci docs u notes, pa onda postavi komponente !!!!
        $form->label('Dokumenti')->documents('docs');
        $form->label('Napomena')->text('note')->autogrow();
        $form->closePanel();
        $form->closeDiv();

        $form->btnSave();

        $form->close();

        return View::make('admin/edit/default')->put($form, 'form');
    }

    public function modal($id = 0)
    {
        Doc::view('admin/modal/default');

        $model = $this->model;

        Doc::title('Vozilo');

        $item = $model::firstOrNew(['id' => $id]);

        $post = Request::post();
        if ($post) {
            if ($post['active'] == '1') {
                $post['active'] = 1;
            } else {
                $post['active'] = 0;
            }
            $item->fill($post);
            $item->save();

            return redirect($this->baseurl)->with('msg', "Snimljeno vozilo {$item->name}");
        }

        $form = Forms::make()->format('EDIT');
        if (!empty($item)) {
            $form->fill($item);
        }

        $form->open('modal-booking-form')
                ->ajax('/admin/vehicle/modal/'.$item->id)
                ->onAjaxDone('$("#calendar").fullCalendar("refetchEvents");');

        $form->div('.col-md-6');
        $form->panel('Osnovne informacije');
        $form->label('Registarska tablica')->input('licence_plate');
        $form->label('Naziv vozila')->input('name');
        $form->label('Marka vozila')->input('brand');
        $form->label('Model')->input('model');
        $form->label('Tip')->radioGroup('type')->values(Vehicle::types());
        $form->label('Broj šasije')->input('chassis_number');
        $form->label('Godina proizvodnje')->select('year')->range(1990, 2016);
        $form->label('Prešao kilometara')->number('km');
        $form->label('Redoviti servis svakih (km)')->number('service_km');
        $form->label('Veličina guma')->input('tire_size');
        $form->label('Boja')->input('color');
        $form->label('Vozilo je aktivno')->checkBox('active');
        //$form->label('Boja (kod)')->colorPicker('color_hex');
        //$form->label('Partner')->select2('partner')->options(Contact::options());
        $form->label('Leasing kuća')->input('leasing');
        $form->label('Rata leasinga')->price('leasing_amount')->currency('kn');
        $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-6');
        $form->panel('Dodatne informacije (trenutne osg.kuće za vozila)');
        $form->label('Registriran do')->datePicker('licence_expires');
        $form->label('AO osiguranje vrijedi do')->datePicker('ao_expires');
        $form->label('AO osg.kuća')->input('ao_house');
        $form->label('Kasko vrijedi do')->datePicker('kasko_expires');
        $form->label('Kasko osg.kuća')->input('kasko_house');
        $form->label('Periodički pregled vrijedi do')->datePicker('pp_expires');
        $form->label('Protupožarni aparat vrijedi do')->datePicker('ppa_expires');
        $form->label('Prikaži ovo vozilo u rasporedu')->radioGroup('show_on_calendar')->options(['1' => 'Da', '0' => 'Ne']);
        $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-12');
        $form->panel('Dokumenti');
        // prvo prebaci docs u notes, pa onda postavi komponente !!!!
        $form->label('Dokumenti')->documents('docs');
        $form->label('Napomena')->text('note')->autogrow();
        $form->closePanel();
        $form->closeDiv();

        $form->btnSave();

        $form->close();

        return View::make('admin/edit/default')->put($form, 'form');
    }

    public function delete($id = 0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if ($item->id > 0 and $item->active == 1) {
            $item->active = 0;
            $item->save();
            //$item->delete();
            return redirect('/admin/vehicle')->with('msg', "{$this->model} #{$item->id} deactived");
        } elseif ($item->id > 0 and $item->active == 0) {
            $item->delete();

            return redirect('/admin/vehicle')->with('msg', "{$this->model} #{$item->id} deleted");
        }

        return redirect('/admin/vehicle')->with('msg', 'Vehicle not found');
    }

    public function get($id = 0)
    {
        return Vehicle::find($id);
    }

    /*
     REPORTING
    */
    public function reporting()
    {
        Doc::title('Pregled po vozilima');

        $vehicles = Vehicle::all();

        $items = new \Illuminate\Support\Collection();
        $items = [];

        foreach ($vehicles as $vehicle) {
            $item = new Model();
            $item->id = $vehicle->id;
            $item->vehicle = $vehicle;
            $results = DB::select('
                    SELECT
                          *,
                          SUM(days) AS total_days,
                          SUM(price) AS total_price,
                          SUM(km_diff) AS total_km,
                          SUM(price)/SUM(days) AS price_per_day,
                          SUM(km_diff)/SUM(days)AS km_per_day
                    FROM (
                        SELECT
                               vehicles.name,
                               bookings.vehicle_id,
                               bookings.from,
                               bookings.to,
                               bookings.km_start,
                               bookings.km_end,
                               abs(datediff(bookings.from, bookings.to)) as days,
                               abs(bookings.km_start-bookings.km_end) as km_diff,
                               bookings.price,
                               bookings.price/abs(datediff(bookings.from, bookings.to)) as price_per_day,
                               abs(bookings.km_start-bookings.km_end)/abs(datediff(bookings.from, bookings.to)) as km_per_day
                        FROM vehicles
                        JOIN bookings ON bookings.vehicle_id=vehicles.id
                        WHERE bookings.vehicle_id=?
                    ) as t
                    ', [$vehicle->id]);
            foreach ($results[0] as $key => $val) {
                $item->$key = $val;
            }
            $items[] = $item;
        }

        $filter = new SearchFilter();
        $filter->perpage = 50;
        $filter->page = 1;

        // search keywords
        if ($filter->notEmpty('keywords')) {
            //$items = $items->search( $filter->keywords, 'name' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Pretraga');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('po ključnim riječima');
        $form->label('Po stranici');
        $form->select('perpage')->values([10, 20, 50, 100])->klass('form-control');

        $table = Table::make()
                ->headings(['#',
                                'Vozilo',
                                'Broj dana',
                                'Prihod',
                                'Prihod/dan',
                                'Kilometraža',
                                'Km/dan',
                            ]);

        $items = $filter->paginate($items);

        foreach ($items as $item) {
            $data = [];
            $data[] = $item->id;
            $data[] = $item->name;
            $data[] = $item->total_days;
            $data[] = Num::price($item->total_price);
            $data[] = Num::price($item->price_per_day);
            $data[] = round($item->total_km);
            $data[] = Num::price($item->km_per_day);
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');

        return View::make('admin/list/default')
                    ->put($this, 'controller')
                    ->put($filter, 'filter')
                    ->put($form, 'searchform')
                    ->put($table, 'table');
    }
}
