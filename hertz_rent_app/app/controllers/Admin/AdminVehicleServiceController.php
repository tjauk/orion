<?php
class AdminVehicleServiceController extends AdminController
{
    public $model = 'VehicleService';
    public $baseurl = '/admin/vehicle-service';

    function index()
    {
        $model = $this->model;

        Doc::title("Servisi");

        $items = new $model();
        $items = VehicleService::with('Vehicle')->select(DB::raw('
            vehicle_services.*,
            contacts.first_name as clients_first_name,
            contacts.last_name as clients_last_name,
            contacts.company as clients_company,
            vehicles.name as vehicle_name
        '))
        ->leftJoin('contacts','contacts.id','=','vehicle_services.service_id')
        ->leftJoin('vehicles','vehicles.id','=','vehicle_services.vehicle_id');

        if( Auth::can('manage.vehicle-service') ){
            $buttons = Forms::make();
            $buttons->btnAdd('Dodaj Servis',$this->baseurl.'/edit');
        }

        $filter = new SearchFilter;
        $filter->perpage = 100;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'vehicle_services.vehicle_km,vehicle_services.note,contacts.company,vehicles.name' );
        }
        // search vehicle
        if( $filter->notEmpty('vehicle_id') ){
            $items = $items->whereVehicleId( $filter->vehicle_id );
        }

        // filter from and to
        if( $filter->notEmpty('from') and $filter->notEmpty('to') ){

            $items = $items->whereRaw("(DATE(`from`)<=DATE('{$filter->to}') AND DATE(`to`)>=DATE('{$filter->from}'))");
        }else{
            // filter period from
            if( $filter->notEmpty('from') ){
                $items = $items->where( DB::raw('DATE(`from`)'),'>=', $filter->from );
            }
            // filter period to
            if( $filter->notEmpty('to') ){
                $items = $items->where( DB::raw('DATE(`from`)'),'<=', $filter->to );
            }
        }
        // filter paid_status
        if( $filter->paid_status=='0' or $filter->paid_status=='1' ){
            $items = $items->whereStatus( $filter->paid_status );
        }
        // filter service_status
        if( $filter->notEmpty('service_status') ){
            $items = $items->whereServiceStatus( $filter->service_status );
        }
        // filter service_type
        if( $filter->notEmpty('service_type') ){
            $items = $items->where( 'vehicle_services.type','=',$filter->service_type );
        }

        $paid_status_options = [''=>'---'];
        foreach(VehicleService::$statuses as $key=>$val){
            $paid_status_options[$key]=$val;
        }
        $paid_label_colors = [
            0=>'label-danger',
            1=>'label-success',
        ];

        $service_status_options = [''=>'---'];
        foreach(VehicleService::service_status_options() as $key=>$val){
            $service_status_options[$key]=$val;
        }
        $status_label_colors = [
            1=>'label-warning',
            2=>'label-primary',
            3=>'label-success',
        ];


        $service_type_options = [''=>'---'];
        foreach(VehicleService::service_type_options() as $key=>$val){
            $service_type_options[$key]=$val;
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Pretraga')->search('keywords')->klass('form-control pull-left')->placeholder('po ključnim riječima');
        $form->label('Vozilo')->select2('vehicle_id')->options(Vehicle::options());
        $form->label('Status servisa')->select('service_status')->options($service_status_options);
        $form->label('Status plaćanja')->select('paid_status')->options($paid_status_options);
        $form->label('Od datuma')->datePicker('from');
        $form->label('Do datuma')->datePicker('to');
        $form->label('Tip servisa')->select('service_type')->options($service_type_options);
        $form->label('Per page')->select('perpage')->values([20,50,100,250,500])->klass('form-control');


        $items = $items->orderBy('vehicle_services.from','DESC');
        //exit($items->toSql());
        $items = $filter->paginate( $items )->get();

        $table = Table::make()
                ->headings([
                    'Vozilo (Trenutna km)',
                    'Vrijeme servisa',
                    'Status',
                    'Tip (km)',
                    'Servisni centar',
                    'Cijena servisa / dijelova',
                    'Plaćanje',
                    'Napomena',
                    'Akcije'
                ]);

        $total_service_price = 0.0;
        $total_parts_price = 0.0;

        foreach($items as $item){
            $data = [];
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->vehicle->title.'</a>'.'<br><span style="font-size:13px;color:#777;">( '.$item->vehicle->km.' km )</span>';
            $data[] = UI::date($item->from).'<br>'.UI::date($item->to);
            $data[] = '<span class="label '.$status_label_colors[$item->service_status].'">'.strtoupper($service_status_options[$item->service_status]).'</span>';
            if( !empty($item->vehicle_km) ){
                $data[] = $item->type.' na '.$item->vehicle_km.' km';
            }else{
                $data[] = $item->type;

            }
            $data[] = $item->service->title;
            $data[] = Num::price($item->service_price) .' / '.Num::price($item->parts_price);
            $data[] = '<span class="label '.$paid_label_colors[$item->status].'">'.strtoupper(VehicleService::$statuses[$item->status]).'</span>';
            $data[] = $item->note;
            if( Auth::can('manage.vehicle-service') ){
                $actions = UI::btnEdit($item->id,'vehicle-service');
                $actions.= UI::btnDelete($item->id,'vehicle-service');
            }else{
                $actions = '-';
            }
            $data[] = $actions;

            $table->addRow($data);

            $total_service_price += floatval($item->service_price);
            $total_parts_price += floatval($item->parts_price);
        }

        $data = ['&nbsp;','&nbsp;','&nbsp;','&nbsp;','&nbsp;'];
        $data[] = b(Num::price($total_service_price)).' / '.b(Num::price($total_parts_price));
        $data[] = '&nbsp;';
        $data[] = '&nbsp;';
        $data[] = '&nbsp;';
        $table->addRow($data);


        $table = $table->render('admin/table/grid');

        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;

        Doc::title("Servis");

        $previous = Session::get('previous');
        Session::forget('previous');

        if( !empty($previous) and empty($id) ){
            //dd($previous);
            $id = 0;
            $item = new $model;
            $item->vehicle_id = $previous->vehicle_id;
            $item->type = $previous->type;
            $item->vehicle_km = $previous->vehicle_km;
            //$item->from = $previous->to;
            $item->from = '';
            $item->service_id = $previous->service_id;
            $item->service_price = 0;
            $item->parts_price = 0;
            $item->status = '0';
            $item->service_status = '1';
            $item->docs = $previous->docs;

        }else{
            $item = $model::firstOrNew(['id'=>$id]);
        }


        $post = Request::post();
        if( $post and Auth::can('manage.vehicle-service') ){
            $old_status = $item->service_status;
            //dd($post);
            $item->fill($post);
            $item->save();

            // service ended, reopen new
            if( $item->service_status=='3' and $old_status!=='3' ){
                return redirect($this->baseurl.'/edit')
                    ->with('service_msg',"Prethodni redovni servis je završio. Možete odmah postaviti slijedeći novi servis.")
                    ->with('previous',$item);
            }else{
                return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
            }
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('vehicleservice');

        $service_msg = Session::get('service_msg');
        if( !empty($service_msg) ){
            Session::forget('service_msg');
            $form->add('<div class="alert alert-info"> <i class="fa fa-info-circle fa-2x"></i> &nbsp;&nbsp; <b>'.$service_msg.'</b></div>');
        }


        $form->label('Vozilo')->select('vehicle_id')->options(Vehicle::options_with_km());
        $form->label('Tip servisa')->radioGroup('type')->values(VehicleService::service_type_options());
        $form->label('Početni datum servisa')->datePicker('from');
        $form->label('Završni datum servisa')->datePicker('to');
        $form->label('Servisni centar')->select2('service_id')->options(Contact::options('Servisni centar'));
        $form->label('Cijena servisa')->price('service_price')->currency('kn');
        $form->label('Cijena dijelova')->price('parts_price')->currency('kn');
        $form->label('Status plaćanja')->radioList('status')->options(['0'=>"Nije plaćeno",'1'=>"Plaćeno"]);
        $form->label('Status servisa')->radioGroup('service_status')->options(VehicleService::service_status_options());
        $form->label('Odvesti na kilometara')->number('vehicle_km');//->suffix('km');
        $form->label('Dokumenti')->documents('docs');
        $form->label('Napomena')->text('note')->autogrow();

        $form->btnSave();

        $form->close();

        jsready("
            $('#from').on('change',function(){
                if( $('#to').val()=='' ){
                    $('#to').val($('#from').val());
                }
            });
        ");

        return View::make('admin/edit/default')->put($form,'form');
    }

    function modal($id=0)
    {
        Doc::view('admin/modal/default');

        $model = $this->model;

        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() and Auth::can('manage.vehicle-service') ){
            $item->fill(Request::post());
            $item->save();
            return $item;
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }

        $form->open('modal-vehicleservices-form')
                ->ajax('/admin/vehicle-service/modal/'.$item->id)
                ->onAjaxDone('$("#calendar").fullCalendar("refetchEvents");');

        $form->label('Vozilo')->select('vehicle_id')->options(Vehicle::options());
        $form->label('Tip servisa')->radioGroup('type')->values(['Redovan','Izvanredni']);
        $form->label('Početni datum servisa')->datePicker('from');
        $form->label('Završni datum servisa')->datePicker('to');
        $form->label('Servisni centar')->select2('service_id')->options(Contact::options('Servisni centar'));
        $form->label('Cijena servisa')->price('service_price')->currency('kn');
        $form->label('Cijena dijelova')->price('parts_price')->currency('kn');
        $form->label('Status')->radioList('status')->options(['0'=>"Nije plaćeno",'1'=>"Plaćeno"]);
        $form->label('Status servisa')->radioGroup('service_status')->options(VehicleService::service_status_options());
        $form->label('Odvesti na kilometara')->number('vehicle_km');//->suffix('km');
        $form->label('Dokumenti')->text('docs');
        $form->label('Napomena')->text('note');

        $form->btnSave();

        $form->close();

        jsready("
            $('#from').on('change',function(){
                if( $('#to').val()=='' ){
                    $('#to').val($('#from').val());
                }
            });
        ");

        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 and Auth::can('manage.vehicle-service') ){
            $item->delete();
        }
        return redirect('/admin/vehicle-service')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
