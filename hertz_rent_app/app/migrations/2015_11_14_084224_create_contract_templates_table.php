<?php

class CreateContractTemplatesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_templates', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->default('');
            $table->string('slug')->default('');
            $table->string('template')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contract_templates');
    }

}
