<?php

class AddKmColumnToVehicleServices extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicle_services', function($table)
        {
            $table->string('vehicle_km')->default('');
            $table->integer('service_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_services', function($table)
        {
            $table->dropColumn('vehicle_km');
            $table->dropColumn('service_id');
        });
    }

}
