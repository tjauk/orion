<?php

class AddColumnsToVehicles extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicles', function($table)
        {
            $table->date('ao_expires')->nullable();
            $table->string('ao_house')->default('');
            $table->date('kasko_expires')->nullable();
            $table->string('kasko_house')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function($table)
        {
            $table->dropColumn('ao_expires');
            $table->dropColumn('ao_house');
            $table->dropColumn('kasko_expires');
            $table->dropColumn('kasko_house');
        });
    }

}
