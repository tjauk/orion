<?php

class AddDocsToDamagesAndBookings extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function($table)
        {
            $table->text('docs')->default('');
        });
        Schema::table('damages', function($table)
        {
            $table->text('docs')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function($table)
        {
            $table->dropColumn('docs');
        });
        Schema::table('damages', function($table)
        {
            $table->dropColumn('docs');
        });
    }

}
