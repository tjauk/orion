<?php

class CreateChecklistsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklists', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->date('check_date');
            $table->integer('vehicle_id');
            $table->text('data')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('checklists');
    }

}
