<?php

class AddExtraFieldsToContacts extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function($table)
        {
            $table->string('contact_person')->default('');
            $table->string('contact_phone')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function($table)
        {
            $table->dropColumn('contact_person');
            $table->dropColumn('contact_phone');
        });
    }

}
