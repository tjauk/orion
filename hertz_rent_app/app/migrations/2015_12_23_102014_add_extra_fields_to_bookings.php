<?php

class AddExtraFieldsToBookings extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function($table)
        {
            $table->string('related_doc')->default('');
            $table->float('partial_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function($table)
        {
            $table->dropColumn('related_doc');
            $table->dropColumn('partial_amount');
        });
    }

}
