<?php

class AddColumnsToReminders extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reminders', function($table)
        {
            $table->integer('created_by')->unsigned()->default(0);
            $table->integer('assigned_to')->unsigned()->default(0);
            $table->text('followers')->default('');
            $table->tinyInteger('done')->unsigned()->default(0);
            $table->dateTime('done_at');
            $table->integer('done_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reminders', function($table)
        {
        	$table->dropColumn('created_by');
        	$table->dropColumn('assigned_to');
        	$table->dropColumn('followers');
        	$table->dropColumn('done');
        	$table->dropColumn('done_at');
        	$table->dropColumn('done_by');
        });
    }

}
