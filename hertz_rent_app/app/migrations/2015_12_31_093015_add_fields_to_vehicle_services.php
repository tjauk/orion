<?php

class AddFieldsToVehicleServices extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicle_services', function($table)
        {
            $table->tinyInteger('service_status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_services', function($table)
        {
            $table->dropColumn('service_status');
        });
    }

}
