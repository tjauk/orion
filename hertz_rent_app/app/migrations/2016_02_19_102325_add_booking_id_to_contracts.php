<?php

class AddBookingIdToContracts extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts', function($table)
        {
            $table->integer('booking_id')->nullable();
            $table->integer('vehicle_id')->nullable();
            $table->integer('client_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts', function($table)
        {
            $table->dropColumn('booking_id');
            $table->dropColumn('vehicle_id');
            $table->dropColumn('client_id');
        });
    }

}
