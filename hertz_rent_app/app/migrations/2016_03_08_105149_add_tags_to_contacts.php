<?php

class AddTagsToContacts extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function($table)
        {
            $table->text('tags')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function($table)
        {
            $table->dropColumn('tags');
        });
    }

}
