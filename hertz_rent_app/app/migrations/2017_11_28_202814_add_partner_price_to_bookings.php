<?php

class AddPartnerPriceToBookings extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function($table)
        {
            $table->float('partner_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function($table)
        {
            $table->dropColumn('partner_price');
        });
    }

}
