<?php

class AddPerMonthAmountsToVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicles', function ($table) {
            $table->string('ao_amount')->default('');
            $table->text('ao_history')->nullable();
            $table->string('kasko_amount')->default('');
            $table->text('kasko_history')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function ($table) {
            $table->dropColumn('ao_amount');
            $table->dropColumn('ao_history');
            $table->dropColumn('kasko_amount');
            $table->dropColumn('kasko_history');
        });
    }
}
