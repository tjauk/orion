<?php

class Contract extends Model
{
    protected $table = 'contracts';

    protected $fillable = ['name', 'dated_at', 'status', 'booking_id', 'client_id', 'vehicle_id', 'data', 'pdf'];

    public static $meta = [
        'najmoprimac',
        'adresa',
        'oib',
        'registracija_vozila',

        'vozac',
        'vozac_adresa',
        'vozac_zemlja',
        'vozac_broj_vozacke',
        'vozac_telefon',
        'vozac_oib',
        'vozac_datum_rodjenja',

        'datum_preuzimanja',
        'vrijeme_preuzimanja',
        'datum_povratka',
        'vrijeme_povratka',

        'tip_cijene',
        'cijena_po_danu',
        'ukupno_dana',
        'dnevna_kilometraza',
        'popust',

        'dodatni_kilometri',
        'dodatni_sat_najma',
        'preuzimanje_izvan_rv',
        'dostava_vozila_na_adresu',

        'napomena',

        'napomena2',
        'ugovor_napisao',
    ];

    public static $meta_labels = [
        'najmoprimac' => 'Ime ili naziv Najmoprimca',
        'adresa' => 'Adresa sjedišta',
        'oib' => 'OIB',
        'registracija_vozila' => 'Tip i registracija vozila',

        'vozac' => '<br><br>Vozač',
        'vozac_adresa' => 'Adresa vozača',
        'vozac_zemlja' => 'Zemlja vozača',
        'vozac_broj_vozacke' => 'Broj vozačke',
        'vozac_telefon' => 'Broj telefona vozača',
        'vozac_oib' => 'OIB vozača',
        'vozac_datum_rodjenja' => 'Datum rođenja',

        'datum_preuzimanja' => '<br><br>Datum preuzimanja',
        'vrijeme_preuzimanja' => 'Vrijeme preuzimanja',
        'datum_povratka' => 'Datum povratka',
        'vrijeme_povratka' => 'Vrijeme povratka',

        'tip_cijene' => '<br><br>Tip cijene',
        'cijena_po_danu' => 'Iznos (bez PDV-a)',
        'ukupno_dana' => '<br><br>Ukupno dana',
        'dnevna_kilometraza' => 'Dnevna kilometraža (km)',
        'popust' => 'Popust (%)',

        'dodatni_kilometri' => 'Dodatni kilometri (km)',
        'dodatni_sat_najma' => 'Dodatni sat najma (kn)',
        'preuzimanje_izvan_rv' => 'Preuzimanje izvan radnog vremena (kn)',
        'dostava_vozila_na_adresu' => 'Dostava vozila na adresu (kn)',

        'napomena' => 'Dodatna napomena (kod preuzimanja)',

        'napomena2' => '<br><br>Dodatna napomena (kod vraćanja)',
        'ugovor_napisao' => 'Ugovor napisao',
    ];

    public static $meta_forms = [
        'najmoprimac' => 'input',
        'adresa' => 'input',
        'oib' => 'input',
        'registracija_vozila' => 'input',

        'vozac' => 'input',
        'vozac_adresa' => 'input',
        'vozac_zemlja' => 'input',
        'vozac_broj_vozacke' => 'input',
        'vozac_telefon' => 'input',
        'vozac_oib' => 'input',
        'vozac_datum_rodjenja' => 'input',

        'datum_preuzimanja' => 'datePicker',
        'vrijeme_preuzimanja' => 'timePicker',
        'datum_povratka' => 'datePicker',
        'vrijeme_povratka' => 'timePicker',

        'tip_cijene' => 'select-price-type',
        'cijena_po_danu' => 'price',
        'ukupno_dana' => 'number',
        'dnevna_kilometraza' => 'number',
        'popust' => 'number',

        'dodatni_kilometri' => 'number',
        'dodatni_sat_najma' => 'number',
        'preuzimanje_izvan_rv' => 'input',
        'dostava_vozila_na_adresu' => 'input',

        'napomena' => 'text',

        'napomena2' => 'text',
        'ugovor_napisao' => 'input',
    ];

    /*
     * Relations
     */
    public function vehicle()
    {
        return $this->belongsTo('Vehicle');
    }

    public function client()
    {
        return $this->belongsTo('Contact', 'client_id', 'id');
    }

    public function booking()
    {
        return $this->belongsTo('Booking');
    }

    /*
     * Options
     */

    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }

    /*
     * Getters
     */

    /*
     * Getters
     */
    public function getDatedAtAttribute()
    {
        if (empty($this->attributes['dated_at'])) {
            return '';
        }

        return date('Y-m-d', strtotime($this->attributes['dated_at']));
    }

    public function getDataAttribute()
    {
        return json_decode($this->attributes['data'], true);
    }

    public function unpackData()
    {
        $data = $this->data;
        foreach ($data as $key => $val) {
            $this->attributes[$key] = $val;
        }
    }

    /*
     * Setters
     */
    public function setDatedAtAttribute($date)
    {
        $date = date('Y-m-d', strtotime($date));
        $this->attributes['dated_at'] = $date;
    }

    public function setDataAttribute($data = [])
    {
        foreach ($data as $key => $val) {
            if (in_array($key, $this->fillable) or in_array($key, ['id', 'created_at', 'updated_at'])) {
                unset($data[$key]);
            }
        }
        $this->attributes['data'] = json_encode($data);
    }
}
