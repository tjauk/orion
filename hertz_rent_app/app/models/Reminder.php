<?php

class Reminder extends Model
{
    protected $table = 'reminders';

    protected $fillable = [
		'name',
		'text',
		'docs',
        'created_by',
        'assigned_to',
        'followers',
        'done',
        'done_at',
        'done_by',
    ];

    /*
     * Relations
     */
    
    /*
     * Users
     */
    public function userCreated(){
        return User::find($this->created_by);
    }
    
    public function userAssigned(){
        return User::find($this->assigned_to);
    }

    public function userDone(){
        return User::find($this->done_by);
    }

    public function usersFollowing(){
        $followers = explode(',',$this->followers);
        return User::whereIn('id',$followers);
    }

    


    /*
     * Options
     */

    /*
     * Scopes
     */
    public function scopeDone($query)
    {
        $query->where('done', '=', '1');
    }


    /*
     * Getters
     */


}
