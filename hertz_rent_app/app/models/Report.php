<?php
class Report extends Model
{
    protected $table = 'reports';

    protected $fillable = [
		'created_by',
		'dated_at',
        'from',
        'to',
        'period',

        'name',
		'status',
        'note',
        
		'data'
    ];

    /*
     * Relations
     */



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */
    public function getDatedAtAttribute()
    {
        if( empty($this->attributes['dated_at']) ){
            return '';
        }
        return date('Y-m-d',strtotime($this->attributes['dated_at']));
    }

    public function getDataAttribute()
    {
        return json_decode($this->attributes['data'],true);
    }
    public function unpackData()
    {
        $data = $this->data;
        foreach($data as $key=>$val)
        {
            $this->attributes[$key] = $val;
        }
    }


    /*
     * Setters
     */
    public function setDatedAtAttribute($date)
    {
        $date = date('Y-m-d',strtotime($date));
        $this->attributes['dated_at'] = $date;
    }
    public function setDataAttribute( $data=[] )
    {
        foreach($data as $key=>$val){
            if( in_array($key,$this->fillable) or in_array($key,['id','created_at','updated_at']) ){
                unset($data[$key]);
            }
        }
        $this->attributes['data'] = json_encode($data);
    }

}
