<?php
class Task extends Model
{
    protected $table = 'tasks';

    protected $fillable = ['created_by','assigned_to','title','description','status','start','due','finish'];

    /*
     * Relations
     */


    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopeToday($query)
    {
        $query->where(DB::raw("DATE(due)"), '=', DB::raw('DATE(NOW())'));
    }


    /*
     * Getters
     */


}
