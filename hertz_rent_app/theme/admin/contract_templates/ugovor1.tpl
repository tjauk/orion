<?php
$contact = Contact::find($najmoprimac);
$per_km_price = 0.5;
$mjesecni_najam = false;
if($tip_cijene=="permonth"){
	$cijena_po_mjesecu = $cijena_po_danu;
	$cijena_po_danu = $cijena_ukupna / $ukupno_dana;
}
?>
<style>
table.predlozak {
	border-top: solid 1px #333;
	border-left: solid 1px #333;
	border-right: solid 1px #333;
}
table.predlozak tr td {
	border-left: solid 1px #777;
	border-right: solid 1px #777;
	padding-left: 2px;
}
table.nutarnja {
	border: none;
}
table.nutarnja tr {
	border: none;
}
table.nutarnja tr td {
	border: none;
}
.farban, .farban td {
	background-color: #eee !important;
	-webkit-print-color-adjust: exact;
}
td.text-right {
	padding-right: 4px;
}
.crtadolje {
	border: solid 3px #333;
}
</style>
<!--
<img src="/images/flexirent-logo-mini.png" />
-->
<img src="/images/flexirent-logo-mini-vozila.png" />

<div style="float:right;text-align: right;">
<?php
if( $najmodavac==1 ){ $pdv = 1.25;
?>
<p>
KOMET STANDARD d.o.o. , OIB: 05029171760 <br>A.B. Jurišića 9, 10040 Zagreb, Hrvatska<br>+385 1 7888 123 / +385 98 1756002
</p>
<?php }else if( $najmodavac==2 ){ $pdv = 1.25; ?>
<p>
FLEXI RENT j.d.o.o. , OIB: 21113779642 <br>A.B. Jurišića 9, 10040 Zagreb, Hrvatska<br>+385 1 7888 123 / +385 98 1756002
</p>
<?php }else{ $pdv = 1.0; ?>
<p>
SISTEM ZA NAJAM VOZILA j.d.o.o. , OIB: 56539022690 <br>A.B. Jurišića 9, 10040 Zagreb, Hrvatska<br>+385 1 7888 123 / +385 98 1756002
</p>
<?php }; ?>
</div>

<div style="clear:both;"></div>

<h1 style="
display: block;
float: left;
width: 40%;
font-size: 24px;
font-weight: bold;
margin-top: 0px;
">Ugovor o najmu vozila</h1>
<div style="
display: block;
float: right;
width: 55%;
border: solid 1px red;
font-size: 9px;
padding: 1px 5px;
margin-bottom: 5px;
font-stretch: semi-condensed;
">
NAPOMENA: sastavni dio ugovora su knjižica vozila, ključevi vozila, AO polica, uvjeti najma na poleđini ugovora s kojima je 
korisnik najma upoznat prije preuzimanja vozila.<br>Ugovor je sastavljen u 2 (dva) identična primjerka od kojih svaka strana 
zadržava po 1 (jedan) primjerak.
</div>
<table class="predlozak" style="width:100%">
	<tr class="farban">
		<td style="font-weight: bold;width:25%;">KORISNIK NAJMA</td>
		<td style="font-weight: bold;width:20%;">VOZAČ 1 <span style="font-size:8px;">(PREUZIMATELJ)</span></td>
		<td style="font-weight: bold;width:20%;">OSOBA <span style="font-size:8px;">KOJA VRAĆA VOZILO</span></td>
		<td style="font-weight: bold;width:35%;">VOZILO I TERMIN NAJMA</td>
	</tr>
	<tr>
		<td style="font-weight: bold;">Ime ili naziv</td>
		<td style="font-weight: bold;">Ime i prezime, adresa</td>
		<td style="font-weight: bold;">Ime i prezime, adresa</td>
		<td style="font-weight: bold;">Vozilo</td>
	</tr>
	<tr>
		<td>{{najmoprimac}}</td>
		<td>{{vozac}}</td>
		<td></td>
		<td>{{registracija_vozila}}</td>
	</tr>
	<tr>
		<td style="font-weight: bold;">Adresa sjedišta</td>
		<td>{{vozac_adresa}}</td>
		<td>&nbsp;</td>
		<td style="font-weight: bold;">Poslovnica:</td>
	</tr>
	<tr>
		<td>{{adresa}}</td>
		<td><span style="font-weight:bold;">OIB:</span> {{vozac_oib}}</td>
		<td><span style="font-weight:bold;">OIB:</span>&nbsp;</td>
		<td>Ulica Dragutina Mandla 1-10040 Zagreb</td>
	</tr>
	<tr>
		<td style="font-weight: bold;">Oib broj:</td>
		<td style="font-weight: bold;">Broj vozačke</td>
		<td style="font-weight: bold;">Broj vozačke</td>
		<td style="font-weight: bold;">Početak najma (datum i sat)</td>
	</tr>
	<tr>
		<td>{{oib}}</td>
		<td>{{vozac_broj_vozacke}}</td>
		<td>&nbsp;</td>
		<td>{{datum_preuzimanja}} {{vrijeme_preuzimanja}}</td>
	</tr>
	<tr>
		<td style="font-weight: bold;">Telefon:</td>
		<td style="font-weight: bold;">Telefon:</td>
		<td style="font-weight: bold;">Telefon:</td>
		<td style="font-weight: bold;">Kraj najma (datum i sat)</td>
	</tr>
	<tr>
		<td>{{vozac_telefon}}</td>
		<td>{{vozac_telefon}}</td>
		<td></td>
		<td>{{datum_povratka}} {{vrijeme_povratka}}</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>{{vozac_datum_rodjenja}}</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>

<table class="predlozak" style="width:100%">
	<tr class="farban">
		<td style="width:65%"><b>STANJE VOZILA I OPREME NA POČETKU I KRAJU NAJMA:</b></td>
		<td><b>OBAČUN NAJMA</b></td>
	</tr>
	<tr>
		<td style="vertical-align: top;text-align: center;"><img style="width:320px;height:180px" src="/images/slika-vozila.png" /></td>
		<td style="vertical-align: top;">
			<table class="nutarnja" style="width:100%">
				<tr>
					@if tip_cijene = "permonth"
						<td style="width:50%">Cijena</td>
						<td style="width:30%" class="text-right">{{Num:price($cijena_po_mjesecu)}} </td>
					@else
						<td style="width:50%">Dnevna cijena</td>
						<td style="width:30%" class="text-right">{{Num:price($cijena_po_danu)}} </td>
					@end
					<td style="width:20%">kn</td>
				</tr>
				<tr>
					<td>Dnevna kilometraža</td>
					<td class="text-right">{{dnevna_kilometraza}} </td>
					<td>km/dan</td>
				</tr>
				<tr>
					<td>Trajanje najma</td>
					<td class="text-right">{{ukupno_dana}} </td>
					<td>dan(a)</td>
				</tr>
				<tr>
					<td>Ukupno kilometara</td>
					<td class="text-right">
						<?php $ukupno_kilometara = $ukupno_dana * $dnevna_kilometraza; ?>
						{{ukupno_kilometara}}
					</td>
					<td>km</td>
				</tr>
				<tr>
					<td>CDW - kasko</td>
					<td class="text-right">0 </td>
					<td>kn</td>
				</tr>
				<tr>
					<td>TP - osiguranje</td>
					<td class="text-right">0 </td>
					<td>kn</td>
				</tr>
				<tr>
					<td>AO - osiguranje</td>
					<td class="text-right">0 </td>
					<td>kn</td>
				</tr>
				<tr>
					<td>Čišćenje vozila</td>
					<td class="text-right">0 </td>
					<td>kn</td>
				</tr>
				<tr>
					<td>Zeleni karton</td>
					<td class="text-right">0 </td>
					<td>kn</td>
				</tr>
				<tr>
					<td>Dodatni vozač</td>
					<td class="text-right">0 </td>
					<td>kn</td>
				</tr>
				<tr>
					<td colspan="3"><small>NAPOMENA: iznad uključene kilometraže je naplata 0,50kn/km</small></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<table style="width:100%;border-bottom: solid 1px #777;" class="predlozak">
	<tr>
		<td style="width:65%;vertical-align: top;">

			<table class="nutarnja" style="width:100%">
				<tr>
					<td><b>Vozilo na početku najma:</b></td>
					<td>&nbsp;</td>
					<td></td>
				</tr>
				<tr>
					<td colspan="2"><b>Popis oštećenja na vozilu:</b></td>
					<td><b>Važeća oprema:</b></td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>&#9634; Sigurnosni trokut</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>&#9634; Sigurnosni prsluk</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>&#9634; Kutija prve pomoći</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>&#9634; Protupožarni aparat</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>&#9634; Rezervni kotač</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>&#9634; Dizalica vozila</td>
				</tr>

				<tr>
					<td colspan="2"><b>Dodatna napomena (ako postoji:)</b></td>
					<td><b>Pregledao:</b></td>
				</tr>
				<tr>
					<td colspan="2">{{napomena}}</td>
					<td><span style="color:#f00;">x</span>&nbsp;</td>
				</tr>
				<tr style="border-bottom: solid 1px #777;">
					<td  colspan="2">&nbsp;</td>
					<td class="crtadolje"></td>
				</tr>

				<tr>
					<td><b>Vozilo na kraju najma:</b></td>
					<td>&nbsp;</td>
					<td></td>
				</tr>
				<tr>
					<td colspan="2"><b>Popis novonastalih oštećenja na vozilu:</b></td>
					<td><b>Važeća oprema:</b></td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>&#9634; Sigurnosni trokut</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>&#9634; Sigurnosni prsluk</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>&#9634; Kutija prve pomoći</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>&#9634; Protupožarni aparat</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>&#9634; Rezervni kotač</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>&#9634; Dizalica vozila</td>
				</tr>

				<tr>
					<td colspan="2"><b>Dodatna napomena (ako postoji:)</b></td>
					<td><b>Pregledao:</b></td>
				</tr>
				<tr>
					<td colspan="2">{{napomena2}}</td>
					<td><span style="color:#4EBE19;">x</span>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3"></td>
				</tr>

			</table>

		</td>
		<td style="vertical-align: top;">

			<table class="nutarnja" style="width:100%">
				<tr>
					<td colspan="3"><b>DODACI NAJMA:</b></td>
				</tr>
				<tr>
					<td colspan="3">(ako je iznos 0,00kn usluga nije ugovorena)</td>
				</tr>

				<tr>
					<td>Dodatni kilometri</td>
					<td class="text-right">{{intval($dodatni_kilometri)}}</td>
					<td>km</td>
				</tr>
				<tr>
					<td>Ukupno kilometri</td>
					<td class="text-right"><?php echo Num::price($dodatni_kilometri * $per_km_price);?> </td>
					<td>kn</td>
				</tr>
				<tr>
					<td>Dodatni sat najma</td>
					<td class="text-right">{{Num:price($dodatni_sat_najma)}} </td>
					<td>kn</td>
				</tr>
				<tr>
					<td>Rad izvan R.V.</td>
					<td class="text-right">{{Num:price($preuzimanje_izvan_rv)}} </td>
					<td>kn</td>
				</tr>
				<tr>
					<td>Dostava vozila</td>
					<td class="text-right">{{Num:price($dostava_vozila_na_adresu)}} </td>
					<td>kn</td>
				</tr>
				<tr>
					<td><b>Ukupno dodatci</b></td>
					<td class="text-right"><?php
					$ukupno_dodatci = $dodatni_kilometri * $per_km_price;
					$ukupno_dodatci+= $dodatni_sat_najma;
					$ukupno_dodatci+= $preuzimanje_izvan_rv;
					$ukupno_dodatci+= $dostava_vozila_na_adresu;
					echo $ukupno_dodatci;
					?> </td>
					<td>kn</td>
				</tr>

				<tr>
					<td colspan="3"><b>OBRAČUN NAJMA SA DODACIMA:</b></td>
				</tr>
				<tr>
					<td style="width:50%;">Najam</td>
					<td style="width:30%;"class="text-right">
						<?php
							if($tip_cijene == "permonth"){
								$cijena_najma = (float)$cijena_po_mjesecu;
							}else{
								$cijena_najma = (int)$ukupno_dana * (float)$cijena_po_danu;
							}
						?>
						{{Num:price($cijena_najma)}}
					</td>
					<td style="width:20%;">kn</td>
				</tr>
				<tr>
					<td>Popust</td>
					<td class="text-right"><?php echo (int)$popust;?></td>
					<td>%</td>
				</tr>
				<tr>
					<td>Popust u kn</td>
					<td class="text-right">
					<?php $popust_u_kn = $cijena_najma * ((int)$popust/100); ?>
					{{Num:price($popust_u_kn)}}
					</td>
					<td>kn</td>
				</tr>
				<tr>
					<td>Ukupno najam</td>
					<td class="text-right">
						<?php $ukupno_najam = $cijena_najma - $popust_u_kn; ?>
						{{Num:price($ukupno_najam)}}
					</td>
					<td>kn</td>
				</tr>
				<tr>
					<td>Dodatci ukupno</td>
					<td class="text-right">{{Num:price($ukupno_dodatci)}}</td>
					<td>kn</td>
				</tr>
				<?php
				$ukupno_sve = $ukupno_najam + $ukupno_dodatci;
				$ukupno_bez_pdva = $ukupno_sve;
				$ukupno_sa_pdvom = $ukupno_bez_pdva * $pdv;
				$samo_pdv = $ukupno_sa_pdvom - $ukupno_bez_pdva;

				$ugovorena_kilometraza = $ukupno_kilometara + $dodatni_kilometri;
				?>
				<tr>
					<td>Ukupno bez PDV-a</td>
					<td class="text-right">{{Num:price($ukupno_bez_pdva)}}</td>
					<td>kn</td>
				</tr>
				<tr>
					<td>PDV (<?php echo ($pdv-1)*100;?>%)</td>
					<td class="text-right">{{Num:price($samo_pdv)}}</td>
					<td>kn</td>
				</tr>
				<tr>
					<td><b>Ukupan iznos:</b></td>
					<td class="text-right"><b>{{Num:price($ukupno_sa_pdvom)}}</b></td>
					<td>kn</td>
				</tr>
				<tr>
					<td><b>Ugovorena kilometraža:</b></td>
					<td class="text-right"><b>{{intval($ugovorena_kilometraza)}}</b></td>
					<td>km</td>
				</tr>

				<tr>
					<td colspan="3"><b>Ugovor sastavio:</b> {{ugovor_napisao}}</td>
				</tr>
				<tr style="border-bottom: solid 1px #777;">
					<td colspan="3"><span style="color:#4EBE19;">x</span></td>
				</tr>

				<tr>
					<td colspan="3"><b>U ime korisnika najma:<b></td>
				</tr>
				<tr>
					<td colspan="3"><span style="color:#f00;">x</span></td>
				</tr>
				<tr>
					<td colspan="3" style="font-size:6px">Osoba izjavljuje da je ovlaštena od korisnika najma za preuzimanje vozila.</td>
				</tr>
<!--
				<tr>
					<td colspan="3">Vozilo pregledao i preuzeo na kraju najma:</td>
				</tr>
				<tr>
					<td colspan="3">MP i potpis</td>
				</tr>
				-->

			</table>


		</td>
	</tr>
</table>

<table style="width:100%;border-bottom: solid 1px #777;" class="predlozak">
	<tr>
		<td colspan="4"><b>Preuzimanje vozila</b></td>
		<td colspan="4"><b>Povratak vozila</b></td>
	</tr>
	<tr>
		<td><small>Datum i vrijeme</small></td>
		<td><small>Kilometraža</small></td>
		<td><small>Stanje goriva</small></td>
		<td><small>U ime korisnika najma</small></td>

		<td><small>Datum i vrijeme</small></td>
		<td><small>Kilometraža</small></td>
		<td><small>Stanje goriva</small></td>
		<td><small>U ime najmodavca</small></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><span style="color:#f00;">x</span></td>

		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><span style="color:#4EBE19;">x</span></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>

		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
