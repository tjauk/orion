<!-- Morris chart -->
@css "morris/morris.css"
<!-- Morris.js charts -->
@jsend "https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"
@jsend "morris/morris.min.js"

<?php
$popups = [];

$TODAY = date('Y-m-d H:i:s');
$DAYS = [];
if( date('l',strtotime($TODAY.' -1 day'))=='Sunday' ){
  $DAYS[] = date('Y-m-d H:i:s',strtotime($TODAY.' -1 day'));
}
$DAYS[] = $TODAY;

// Istekla registracija
$msg = "Istekla registracija";
foreach($DAYS as $NOW){
    $items = Vehicle::active()
                ->where(\DB::raw("TIMESTAMP(`licence_expires`)"), '<', $NOW)
                ->get();
    foreach($items as $item){
        $popups[] = [
            'msg'=>$msg,
            'group'=>$item->title,
            'link'=>'/admin/vehicle/edit/'.$item->id,
        ];
    }
}

// Istekao PP aparat
$msg = "Istekao PP aparat";
foreach($DAYS as $NOW){
    $items = Vehicle::active()
                ->where(\DB::raw("TIMESTAMP(`ppa_expires`)"), '<', $NOW)
                ->get();
    foreach($items as $item){
        $popups[] = [
            'msg'=>$msg,
            'group'=>$item->title,
            'link'=>'/admin/vehicle/edit/'.$item->id,
        ];
    }
}

// Isteklo kasko osiguranje
$msg = "Isteklo kasko osiguranje";
foreach($DAYS as $NOW){
    $items = Vehicle::active()
                ->where(\DB::raw("TIMESTAMP(`kasko_expires`)"), '<', $NOW)
                ->get();
    foreach($items as $item){
        $popups[] = [
            'msg'=>$msg,
            'group'=>$item->title,
            'link'=>'/admin/vehicle/edit/'.$item->id,
        ];
    }
}

// Isteklo AO osiguranje
$msg = "Isteklo AO osiguranje";
foreach($DAYS as $NOW){
    $items = Vehicle::active()
                ->where(\DB::raw("TIMESTAMP(`ao_expires`)"), '<', $NOW)
                ->get();
    foreach($items as $item){
        $popups[] = [
            'msg'=>$msg,
            'group'=>$item->title,
            'link'=>'/admin/vehicle/edit/'.$item->id,
        ];
    }
}

// Istekao periodički pregled
$msg = "Istekao periodički pregled";
foreach($DAYS as $NOW){
    $items = Vehicle::active()
                ->where(\DB::raw("TIMESTAMP(`pp_expires`)"), '<', $NOW)
                ->get();
    foreach($items as $item){
        $popups[] = [
            'msg'=>$msg,
            'group'=>$item->title,
            'link'=>'/admin/vehicle/edit/'.$item->id,
        ];
    }
}

// Nije počeo najam a datum i vrijeme su prošli
$msg = "Nije počeo najam, a datum i vrijeme su prošli";
foreach($DAYS as $NOW){
    $items = Booking::active()
                    ->where(\DB::raw("TIMESTAMP(`from`)"), '<', $NOW)
                    ->where(\DB::raw("DATE(`from`)"), '=', \DB::raw("DATE('$NOW')"))
                    ->where('status','=',0)
                    ->get();
    foreach($items as $item){
        $popups[] = [
            'msg'=>$msg,
            'group'=>$item->client->title.'<br>'.$item->title,
            'link'=>'/admin/booking/edit/'.$item->id,
        ];
    }
}

// Najam je istekao, a nije označen da je završio
$msg = "Najam je istekao, a nije označen da je završio";
foreach($DAYS as $NOW){
    $items = Booking::active()
                    ->where(\DB::raw("TIMESTAMP(`to`)"), '<', $NOW)
                    ->where('status','=',1)
                    ->get();
    foreach($items as $item){
        $popups[] = [
            'msg'=>$msg,
            'group'=>$item->client->title.'<br>'.$item->title,
            'link'=>'/admin/booking/edit/'.$item->id,
        ];
    }
}

// Nije odveženo vozilo na servis a datum je počeo
$msg = "Nije odveženo vozilo na servis, a datum je počeo";
foreach($DAYS as $NOW){
    $items = VehicleService::whereServiceStatus(1) // Treba odvesti
                    ->where(\DB::raw("DATE(`from`)"), '<=', \DB::raw("DATE('$NOW')"))
                    ->get();
    foreach($items as $item){
        $popups[] = [
            'msg'=>$msg,
            'group'=>'Servis '.$item->title,
            'link'=>'/admin/vehicle-service/edit/'.$item->id,
        ];
    }
}

// Nije zatvoren servis a datum je prošao
$msg = "Nije zatvoren servis, a datum je prošao";
foreach($DAYS as $NOW){
    $items = VehicleService::whereServiceStatus(2) // Traje
                    ->where(\DB::raw("DATE(`to`)"), '<', \DB::raw("DATE('$NOW')"))
                    ->get();
    foreach($items as $item){
        $popups[] = [
            'msg'=>$msg,
            'group'=>'Servis '.$item->title,
            'link'=>'/admin/vehicle-service/edit/'.$item->id,
        ];
    }
}

$popover_content = [];
foreach($popups as $popup){
  $popover_content[] = '<a style="color:#000;font-size:10px" href="'.$popup['link'].'">'.$popup['msg'].'<br>'.$popup['group'].'</a>';
}
$popover_html = implode('<br><br>',$popover_content);
$popover_html = str_replace(["'","\r","\n"],["\\'","",""],$popover_html);
/*
<?php if(!empty($popover_content)){ ?>
      $('#servicesBox').popover('show');
   <?php }; ?>
*/
?>
<script>
$(function () {
   $('#servicesBox').popover({
      html: true,
      content: '{{popover_html}}',
   });

});

</script>


        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{Booking:startsToday()->withoutOtherServices()->count()}}</h3>
                  <p>Danas počinje</p>
                </div>
                <div class="icon">
                  <i class="fa fa-truck"></i>
                </div>
                <a href="/admin/booking?keywords=&vehicle_id=&partner=&paid_status=&from=<?php echo date('Y-m-d');?>&to=<?php echo date('Y-m-d');?>&look=from&perpage=1000" class="small-box-footer">pogledaj sve <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3>{{Booking:endsToday()->withoutOtherServices()->count()}}</h3>
                  <p>Danas završava</p>
                </div>
                <div class="icon">
                  <i class="fa fa-truck"></i>
                </div>
                <a href="/admin/booking?keywords=&vehicle_id=&partner=&paid_status=&from=<?php echo date('Y-m-d');?>&to=<?php echo date('Y-m-d');?>&look=to&perpage=1000" class="small-box-footer">pogledaj sve <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-blue">
                <div class="inner">
                  <h3>{{VehicleService:inPeriod()->count()}}</h3>
                  <p>Danas na servisu</p>
                </div>
                <div class="icon">
                  <i class="fa fa-wrench"></i>
                </div>
                <a href="/admin/vehicle-service/?from=<?php echo date('Y-m-d');?>&to=<?php echo date('Y-m-d');?>" class="small-box-footer">pogledaj sve <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div>

<?php /*
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>65</h3>
                  <p>Tren</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">pogledaj sve <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
*/ ?>


<?php
$start_date = date('Y-m-d');
$week_later = date('Y-m-d',strtotime('now +7days'));
?>
          <div class="row">
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{Booking:startsThisWeek()->withoutOtherServices()->count()}}</h3>
                  <p>Počinje unutar 7 dana</p>
                </div>
                <div class="icon">
                  <i class="fa fa-truck"></i>
                </div>
                <a href="/admin/booking?keywords=&vehicle_id=&partner=&paid_status=&from=<?php echo $start_date;?>&to=<?php echo $week_later;?>&look=from&perpage=1000" class="small-box-footer">pogledaj sve <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3>{{Booking:endsThisWeek()->withoutOtherServices()->count()}}</h3>
                  <p>Završava unutar 7 dana</p>
                </div>
                <div class="icon">
                  <i class="fa fa-truck"></i>
                </div>
                <a href="/admin/booking?keywords=&vehicle_id=&partner=&paid_status=&from=<?php echo $start_date;?>&to=<?php echo $week_later;?>&look=to&perpage=1000" class="small-box-footer">pogledaj sve <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div
              id="servicesBox"
              class="col-lg-4 col-xs-6"
              data-toggle="popover"
              data-trigger="click focus"
              data-placement="bottom"
              title="Neriješeno"
            >
              <!-- small box -->
              <div class="small-box bg-red" style="cursor: pointer;">
                <div class="inner">
                  <h3>{{popups|count}}</h3>
                  <p><b>NERIJEŠENO</b></p>
                </div>
                <div class="icon">
                  <i class="fa fa-info"></i>
                </div>
                <a href="#" class="small-box-footer">klikni za pregled <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->



          <div class="row">
            <div class="col-md-12">
              <?php
              // samo "naša",... bez partnerskih
              $our_rent = Booking::join('vehicles', 'vehicles.id', '=', 'bookings.vehicle_id')->whereStatus(1)->where('type','NOT LIKE','%Partner%')->count();
              $our_total = Vehicle::active()->where('type','NOT LIKE','%Partner%')->count();
              ?>
              <h3>Vozila u najmu ({{our_rent}} od {{our_total}})</h3>
            </div>
            <?php
            $vehicle_types = Vehicle::types();
            foreach($vehicle_types as $key=>$type){
              $in_rent = Booking::join('vehicles', 'vehicles.id', '=', 'bookings.vehicle_id')->whereStatus(1)->whereType($type)->count();
              $total_of_types = Vehicle::active()->whereType($type)->count();
            ?>
              <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                  <div class="inner">
                    <a style="color:#fff;" href="/admin/booking/?status=1&vehicle_type={{key}}">{{type}}: <b>{{in_rent}}</b> od {{total_of_types}}</a>a
                  </div>
                </div>
              </div><!-- ./col -->
            <?php }; ?>
          </div><!-- /.row -->



          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
@if Auth:can('show.totals')
              <!-- Custom tabs (Charts with tabs)-->
              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->

                <ul class="nav nav-tabs pull-right">
                  <li class="active"><a href="#revenue-chart" data-toggle="tab">Graf</a></li>
                  <!--<li><a href="#sales-chart" data-toggle="tab">Donut</a></li>-->
                  <li class="pull-left header"><i class="fa fa-inbox"></i> Prihodi </li>
                </ul>

                <div class="alert alert-info">
                  <p>
                    <i class="icon fa fa-info"></i> Prikaz od početnog dana najma po mjesecima bez obzira na status plaćanja.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* Komet Standard prikazuje iznose s uključenim PDV-om.
                  </p>
                </div>

                <div class="tab-content no-padding">
                  <!-- Morris chart - Sales -->
                  <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>
                  <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
                </div>
              </div><!-- /.nav-tabs-custom -->
@end



              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->

                <ul class="nav nav-tabs pull-right">
                  <li><a href="#expires-in-30" data-toggle="tab">30 dana</a></li>
                  <li class="active"><a href="#expires-in-15" data-toggle="tab">15 dana</a></li>
                  <!--<li><a href="#sales-chart" data-toggle="tab">Donut</a></li>-->
                  <li class="pull-left header"><i class="fa fa-bus"></i> Prikaz isteka registracija, kaska, periodičkih pregleda i protupožarnih aparata. </li>
                </ul>

                <div class="alert alert-info">
                  <p>
                    <i class="icon fa fa-info"></i>
                    Kada se napravi nova registracija, AO i protupožarnih aparata pod vozila obavezno unijeti datum isteka.
                  </p>
                </div>

                <div class="tab-content">
                  <div class="active tab-pane table-responsive" id="expires-in-15">
                    <?php
                    $days = 15;
                    $items = Vehicle::itemsExpiringSoon($days);
                    $table = Table::make()->headings(['Datum','Ističe','Vozilo']);
                    foreach($items as $item){
                        $data = [];
                        $data[] = UI::date($item->datum);
                        $data[] = $item->item;
                        $data[] = '<a href="/admin/vehicle/edit/'.$item->id.'">'.$item->name.'</a>';
                        $table->addRow($data);
                    }
                    $table->out('admin/table/grid');
                    ?>
                  </div>
                  <div class="tab-pane table-responsive" id="expires-in-30">
                    <?php
                    $days = 30;
                    $items = Vehicle::itemsExpiringSoon($days);
                    $table = Table::make()->headings(['Datum','Ističe','Vozilo']);
                    foreach($items as $item){
                        $data = [];
                        $data[] = UI::date($item->datum);
                        $data[] = $item->item;
                        $data[] = '<a href="/admin/vehicle/edit/'.$item->id.'">'.$item->name.'</a>';
                        $table->addRow($data);
                    }
                    $table->out('admin/table/grid');
                    ?>
                  </div>
                </div>

              </div><!-- /.nav-tabs-custom -->



              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->

                <ul class="nav nav-tabs pull-right">
                  <li class="active"><a href="#servis-1" data-toggle="tab">Po vozilima</a></li>
                  <li class="pull-left header"><i class="fa fa-bus"></i> Prikaz redovitog servisa, zadnjeg servisnog pregleda i najranijeg sljedećeg servisa </li>
                </ul>

                <div class="alert alert-info">
                  <p>
                    <i class="icon fa fa-info"></i>
                    <b>Redovite servise</b> raditi svakih 40000 km, a <b>preglede</b> svakih 10000 km.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                    <b>Redoviti servis</b> obavezno unijeti 3000km prije isteka u rubrici servisi.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                    <b>Preglede</b> obavezno unijeti 1000km prije isteka u rubrici servisi.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                    <b>Izvanredne servise</b> unijeti prema naputku servisnog centra.
                  </p>
                </div>

                <div class="tab-content">
                  <div class="active tab-pane  table-responsive" id="servis-1">
                    <?php
                    $highlight_km = 1000;
                    $vehicles = Vehicle::active()
                                        ->where('type','NOT LIKE','%Partnerski%')
                                        ->orderByRaw("FIELD(type,".implode(',',Vehicle::ordered_by_types()).") ASC")
                                        ->get();
                    $table = Table::make()
                                  ->headings([
                                              'Vozilo i trenutna kilometraža',
                                              'Zadnji redoviti servis',
                                              'Zadnji servisni pregled',
                                              'Sljedeći servis',
                                              'Svi servisi za vozilo'
                                            ]);
                    foreach($vehicles as $item){
                        $data = [];

                        $data[] = '<a href="/admin/vehicle-service?vehicle_id='.$item->id.'">'.$item->name.'</a><br>'.'<span style="text-align:right;font-weight:bold;">'.$item->km.' km</span>';

                        $last_regular = VehicleService::whereVehicleId($item->id)
                                                      ->whereType('Redovan')
                                                      ->whereServiceStatus(3) // finished
                                                      ->orderBy('to',DESC)
                                                      ->first();
                        $out = [];
                        if( !empty($last_regular->to) ){ $out[] = UI::date($last_regular->to); }
                        if( !empty($last_regular->vehicle_km) ){ $out[] = $last_regular->vehicle_km.' km + '.($item->km-$last_regular->vehicle_km).' km'; }
                        $data[] = implode('<br>',$out);

                        $last_check = VehicleService::whereVehicleId($item->id)
                                                      ->whereType('Pregled')
                                                      ->whereServiceStatus(3) // finished
                                                      ->orderBy('to',DESC)
                                                      ->first();
                        $out = [];
                        if( !empty($last_check->to) ){ $out[] = UI::date($last_check->to); }
                        if( !empty($last_check->vehicle_km) ){ $out[] = $last_check->vehicle_km.' km + '.($item->km-$last_check->vehicle_km).' km'; }
                        $data[] = implode('<br>',$out);

                        $next_services = VehicleService::nextTime($item)->get();
                        $next = [];
                        foreach ($next_services as $service) {
                            if( !empty($service->vehicle_km) ){
                                $out = $service->type.' na '.$service->vehicle_km.' km <span style="font-weight:bold;">(za '.($service->vehicle_km-$item->km).' km)</span>';

                                if( $service->vehicle_km-$item->km <= $highlight_km ){
                                  $out = '<span style="background-color: #f4b75a;">'.$out.'</span>';
                                }
                                $next[] = $out;
                            }else{
                                $next[] = UI::date($service->from).' - '.$service->type;
                            }
                        }
                        $data[] = implode('<br>',$next);

                        $data[] = '<a class="btn btn-xs btn-primary" href="/admin/vehicle-service?vehicle_id='.$item->id.'">Pogledaj</a>';

                        $table->addRow($data);
                    }
                    $table->out('admin/table/grid');
                    ?>
                  </div>
                </div>
              </div><!-- /.nav-tabs-custom -->








              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->

                <ul class="nav nav-tabs pull-right">
                  <li class="pull-left header"><i class="fa fa-warning"></i> Obrati pažnju! </li>
                </ul>


                <div class="tab-content">
                  <div class="active tab-pane table-responsive" id="expires-in-15">
                    <?php
                    $alerts = [];
                    $h = intval(date('G'));
                    if( $h>=7 and $h<=9 and date('D')=='Mon' ){
                      
                    }

                    if( $h>=6 and $h<12 and date('D')=='Mon' ){
                      $alerts[] = [
                                'msg'=>"Provjeriti po programu obaveze za cijeli tjedan",
                                'group'=>"Dobro jutro",
                                'link'=>'/admin/',
                            ];
                    }

                    if( $h>=6 and $h<12 and date('D')=='Fri' ){
                      $alerts[] = [
                                'msg'=>"Zatvoriti protekli tjedan najmove, servise i obaveze",
                                'group'=>"Dobro jutro",
                                'link'=>'/admin/booking',
                            ];
                    }

                    if( date('j')>=1 and date('j')<=3 ){
                      $alerts[] = [
                                'msg'=>"Zatvoriti protekli mjesec, arhiva ugovora, knjigovodstvo i pregled programa",
                                'group'=>"Dobro jutro",
                                'link'=>'/admin/contract/',
                            ];
                    }

                    //ako je prošla obaveza
                    $msg = "Prošla obaveza";
                    $fields = [
                      'pp_expires' => 'Periodički pregled',
                      'licence_expires' => 'Registracija',
                      'ppa_expires' => 'Protupožarni aparat',
                      'ao_expires' => 'Osiguranje',
                      'kasko_expires' => 'Kasko'
                    ];
                    foreach($fields as $field=>$label){
                        $items = Vehicle::where(\DB::raw("DATE(`$field`)"), '<=', \DB::raw("DATE(NOW())"))->get();
                        foreach($items as $item){
                            $alerts[] = [
                                'msg'=>"$msg ($label)",
                                'group'=>$item->title,
                                'link'=>'/admin/vehicle/edit/'.$item->id,
                            ];
                        }
                    }


                    //ako je prošao najam a nije zatvoren
                    $msg = "Najam nije zatvoren";
                    $items = Booking::active()
                                    ->where(\DB::raw("DATE(`to`)"), '<', \DB::raw("DATE(NOW())"))
                                    ->where('status','<',2)
                                    ->get();
                    foreach($items as $item){
                        $alerts[] = [
                            'msg'=>$msg,
                            'group'=>$item->client->title.'<br>'.$item->title,
                            'link'=>'/admin/booking/edit/'.$item->id,
                        ];
                    }


                    //ako nije upisana kilometraža - početna ili završna, ignoriraj partnerska vozila
                    $msg = "Nije upisana kilometraža";
                    $items = Booking::with('Vehicle')->active()
                                    ->where(\DB::raw("DATE(`from`)"), '<', \DB::raw("DATE(NOW())"))
                                    ->where('status','=',2)
                                    ->where('service_type','<>',30) // bez Ostalo
                                    ->where(function($query){
                                        $query->orWhere(function($query){
                                            $query->where('km_start','=','');
                                            $query->where('km_end','<>','');
                                        });
                                        $query->orWhere(function($query){
                                            $query->where('km_start','<>','');
                                            $query->where('km_end','=','');
                                        });
                                    })

                                    ->get();
                    foreach($items as $item){

                        if( substr($item->vehicle->type,0,7)!=='Partner' ){
                          $alerts[] = [
                              'msg'=>$msg,
                              'group'=>$item->title,
                              'link'=>'/admin/booking/edit/'.$item->id,
                          ];
                        }
                    }


                    //ako nije plaćeno, a zatvoren najam
                    $msg = " je zatvoren, a nije plaćen";
                    $items = Booking::active()
                                    ->where('status','=',2)
                                    ->where('paid','<=',1)
                                    ->get();

                    $service_type_options = Booking::service_type_options();
                    foreach($items as $item){
                        $service_type = $service_type_options[$item->service_type];

                        $alerts[] = [
                            'msg'=>$service_type.$msg,
                            'group'=>$item->client->title.'<br>'.$item->title,
                            'link'=>'/admin/booking/edit/'.$item->id,
                        ];
                    }

                    //ako nije označen zatvoren servis, a proša datum
                    $msg = "Servis nije zatvoren";
                    $items = VehicleService::whereIn('service_status',[1,2])
                                    ->where(function($query){
                                        $query->orWhere(\DB::raw("DATE(`from`)"), '<', \DB::raw("DATE(NOW())"));
                                        $query->orWhere(\DB::raw("DATE(`to`)"), '<', \DB::raw("DATE(NOW())"));
                                    })->get();
                    foreach($items as $item){
                        $alerts[] = [
                            'msg'=>$msg,
                            'group'=>'Servis '.$item->title,
                            'link'=>'/admin/vehicle-service/edit/'.$item->id,
                        ];
                    }

                    //ako nije počeo servis, a označen da treba
                    $msg = "Servis treba početi";
                    $items = VehicleService::whereIn('service_status',[1])
                                    ->where(\DB::raw("DATE(`from`)"), '<=', \DB::raw("DATE(NOW())"))
                                    ->get();
                    foreach($items as $item){
                        $alerts[] = [
                            'msg'=>$msg,
                            'group'=>'Servis '.$item->title,
                            'link'=>'/admin/vehicle-service/edit/'.$item->id,
                        ];
                    }


                    $table = Table::make()->headings(['Poruka','Naslov','Akcija']);
                    foreach($alerts as $alert){
                        $data = [];
                        $data[] = '<span class="label label-warning" style="font-size:13px">'.$alert['msg'].'</span>';
                        $data[] = '<a href="'.$alert['link'].'">'.$alert['group'].'</a>';
                        $data[] = '<a class="btn btn-xs btn-primary" href="'.$alert['link'].'">Pogledaj</a>';
                        $table->addRow($data);
                    }
                    $table->out('admin/table/grid');
                    ?>
                  </div>
                </div>

              </div><!-- /.nav-tabs-custom -->







<?php /*
              <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">To Do List</h3>
                  <div class="box-tools pull-right">
                    <ul class="pagination pagination-sm inline">
                      <li><a href="#">&laquo;</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">&raquo;</a></li>
                    </ul>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list">
                    <li>
                      <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text">Design a nice theme</span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Make the theme responsive</span>
                      <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Check your messages and notifications</span>
                      <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                  </ul>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix no-border">
                  <button class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
                </div>
              </div><!-- /.box -->
*/ ?>


            </section><!-- /.Left col -->
<?php /*
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-5 connectedSortable">

              <!-- solid sales graph -->
              <div class="box box-solid bg-teal-gradient">
                <div class="box-header">
                  <i class="fa fa-th"></i>
                  <h3 class="box-title">Sales Graph</h3>
                  <div class="box-tools pull-right">
                    <button class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body border-radius-none">
                  <div class="chart" id="line-chart" style="height: 250px;"></div>
                </div><!-- /.box-body -->
                <div class="box-footer no-border">
                  <div class="row">
                    <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                      <input type="text" class="knob" data-readonly="true" value="20" data-width="60" data-height="60" data-fgColor="#39CCCC">
                      <div class="knob-label">Mail-Orders</div>
                    </div><!-- ./col -->
                    <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                      <input type="text" class="knob" data-readonly="true" value="50" data-width="60" data-height="60" data-fgColor="#39CCCC">
                      <div class="knob-label">Online</div>
                    </div><!-- ./col -->
                    <div class="col-xs-4 text-center">
                      <input type="text" class="knob" data-readonly="true" value="30" data-width="60" data-height="60" data-fgColor="#39CCCC">
                      <div class="knob-label">In-Store</div>
                    </div><!-- ./col -->
                  </div><!-- /.row -->
                </div><!-- /.box-footer -->
              </div><!-- /.box -->

            </section><!-- right col -->
*/ ?>

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->

<?php
$currentYear = date('Y');
$months = range(0,15);
$data = [];
foreach($months as $month){
    $month_start= date('Y-m-d',strtotime($currentYear.'-01-01 +'.$month.'month'));
    $month_end  = date('Y-m-t',strtotime($month_start));

    $items = Booking::inMonth($month,$currentYear)->get();

    $total_price1 = 0.0;
    $total_price2 = 0.0;
    $total_price3 = 0.0;
    foreach ($items as $item) {
        $price = $item->price;
        if( $item->paid==99 ){
            $price = 0.0;
        }
        if( $item->partner==1 ){
            $total_price1+=$price;
        }
        if( $item->partner==2 ){
            $total_price2+=$price;
        }
        if( $item->partner==2318 ){
            $total_price3+=$price;
        }
    }

    $data[] = '{y: "'.date('Y-m',strtotime($month_start)).'", item1: '.$total_price1.', item2: '.$total_price2.', item3: '.$total_price3.'}';
}
?>

<script>
$(document).ready(function(){

  /* Morris.js Charts */
  // Prihodi

  var area = new Morris.Area({
    element: 'revenue-chart',
    resize: true,
    behaveLikeLine: true,
    data: [<?php echo implode(',',$data);?>],
    xkey: 'y',
    ykeys: ['item1', 'item2', 'item3'],
    labels: ['Komet', 'FlexiRent', 'Sistem'],
    lineColors: ['#a0d0e0', '#3c8dbc'],
    hideHover: 'auto'
  });


  /*
  var line = new Morris.Line({
    element: 'line-chart',
    resize: true,
    data: [
      {y: '2011 Q1', item1: 2666},
      {y: '2011 Q2', item1: 2778},
      {y: '2011 Q3', item1: 4912},
      {y: '2011 Q4', item1: 3767},
      {y: '2012 Q1', item1: 6810},
      {y: '2012 Q2', item1: 5670},
      {y: '2012 Q3', item1: 4820},
      {y: '2012 Q4', item1: 15073},
      {y: '2013 Q1', item1: 10687},
      {y: '2013 Q2', item1: 8432}
    ],
    xkey: 'y',
    ykeys: ['item1'],
    labels: ['Item 1'],
    lineColors: ['#efefef'],
    lineWidth: 2,
    hideHover: 'auto',
    gridTextColor: "#fff",
    gridStrokeWidth: 0.4,
    pointSize: 4,
    pointStrokeColors: ["#efefef"],
    gridLineColor: "#efefef",
    gridTextFamily: "Open Sans",
    gridTextSize: 10
  });
  */

  //Donut Chart
  /*
  var donut = new Morris.Donut({
    element: 'sales-chart',
    resize: true,
    colors: ["#3c8dbc", "#f56954", "#00a65a"],
    data: [
      {label: "Download Sales", value: 12},
      {label: "In-Store Sales", value: 30},
      {label: "Mail-Order Sales", value: 20}
    ],
    hideHover: 'auto'
  });
  */

  //Fix for charts under tabs
  $('.box ul.nav a').on('shown.bs.tab', function () {
    area.redraw();
    donut.redraw();
    line.redraw();
  });

});
</script>
