@asset "jquery"
@asset "moment"

<!-- used in modals -->
@asset "jquery-form"
@asset "daterangepicker"
@asset "timepicker"
@asset "select2"
@asset "dropzone"

<script type="text/javascript" src="https://cdn.rawgit.com/asvd/dragscroll/master/dragscroll.js"></script>

<style type="text/css">

.event-holder {
    display: block;
    position: relative;
    top: 0;
    left: 15px;
    
    width: 500px;
    height: 20px;
    
    overflow: hidden;

    color: #fff;
    background-color: #f00;
    border: none;
}


<?php
function timeline_localize($str){
  $trans = [
    'Mon' => 'Pon',
    'Tue' => 'Uto',
    'Wed' => 'Sri',
    'Thu' => 'Čet',
    'Fri' => 'Pet',
    'Sat' => 'Sub',
    'Sun' => 'Ned',
  ];
  return $trans[$str];
}
$cell_width = 45;
$cell_height = 25;
$row_width = $cell_width*65*4;

function weekendClass($day){
  $name = date('D',$day);
  if( $name=='Sat' or $name=='Sun' ){
    return 'weekend';
  }
  return '';
}
?>
.cells {
  display: block;
  position: relative;
  width: 100%;
  overflow:hidden;
  background-color:#fff;
}

.cell-row {
  display: block;
  width: {{row_width}}px !important;
  height: {{cell_height}}px !important;
  margin:0;
  padding:0;
  border: none;
  overflow: none;
  border-bottom: solid 1px #ddd;
}
.header-cell-row {
  width: {{row_width}}px !important;
  height: 60px !important;
}
.cell {
  display: block;
  width: {{cell_width}}px !important;
  height: {{cell_height}}px !important;
  float:left;
  margin:0;
  padding:0;
  border: none;
  overflow: none;
}




.header-cell-vehicle {
  display: block;
  height: 60px !important;
  background-color: #eee;
}
.cell-vehicle {
  display: block;
  height: {{cell_height}}px !important;
  border-right: solid 1px #ddd;
}

.header-cell-day {
  display: block;
  width: {{cell_width}}px !important;
  float:left;
  background-color: #eee;
  border-right: solid 1px #777;
  border-bottom: solid 1px #777;
  font-size: 13px;
  text-align: center;
  height: 40px !important;
}
.header-cell-month {
  display: block;
  width: {{cell_width}}px;
  height: 19px !important;
  float:left;
  background-color: #ddd;
  border-right: solid 1px #777;
  border-bottom: solid 1px #777;
  font-size: 13px;
  font-weight: bold;
  text-align: center;
}
.cell-day {
  display: block;
  width: {{cell_width}}px !important;
  height: {{cell_height}}px !important;
  float:left;
  background-color: #eee;
  border-right: solid 1px #ddd;
  text-align: center;
  color:#ddd;
  font-size:10px;
}

.cell-line {
  cursor:pointer;
  display: block;
  position: absolute;

  width: 100px;
  height: 20px;

  overflow: hidden;
  border: none;

  color: #fff;
  background-color: #f00;
  padding-left:2px;
}

.weekend {
  background-color: #fdd !important;
}


</style>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="box">
      <div class="box-body">
        <div class="col-md-6">
          <select class="form-control" name="vehicle_types" onchange="filterVehicleTypes(this)">
            <option value="all">Svi tipovi vozila</option>
            <?php foreach(Vehicle::types() as $type){ ?>
              <option value="{{type}}">{{$type}}</option>
            <?php }; ?>
          </select>
        </div>
        <div class="col-md-6">

        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <?php
    // get vehicle_ids sorted by difference between min and max km for the last 6 months
    $results = collect(DB::select(DB::raw("
      SELECT
         *,
         SUM(days) AS total_days,
         SUM(kms) AS total_kms,
         ROUND(SUM(kms)/SUM(days)) as total_avg
       FROM (
         SELECT 
            id,
            vehicle_id,
            ABS(DATEDIFF(bookings.to,bookings.from)) as days,
            ABS(km_end-km_start) as kms,
            ( ABS(km_end-km_start) / ABS(DATEDIFF(bookings.to,bookings.from)) ) as akms
         FROM bookings 
         WHERE
           km_start>0
           AND
           km_end>0
           AND
           ABS(DATEDIFF(bookings.to,bookings.from)) > 0
           AND
           DATE(bookings.to) > DATE_SUB(CURDATE(),INTERVAL 6 MONTH)
         ) as t
      GROUP BY vehicle_id
      ORDER BY total_avg
    ")));
    $ordered_vehicles = $results->pluck('vehicle_id')->toArray();
    $avg_kms = $results->pluck('total_avg')->toArray();
    $joined = array_combine($ordered_vehicles,$avg_kms);

    // $vehicles = Vehicle::active()->orderByRaw("FIELD(id,".implode(',',$ordered_vehicles).") ASC")->get();

    
    $vehicles = Vehicle::active()->orderByRaw("FIELD(type,".implode(',',Vehicle::ordered_by_types()).") ASC")->get();

    foreach($vehicles as $index=>$vehicle){
      if(isset($joined[$vehicle->id])){
        $vehicles[$index]->avg_kms = $joined[$vehicle->id];
      }else{
        $vehicles[$index]->avg_kms = 9999;
      }
    }
    // $vehicles = $vehicles->sortBy('avg_kms');
    $today = date('Y-m-d');
    $first = date('Y-m-01',strtotime('now -30 days'));
/*
pr($ordered_vehicles);
pr($avg_kms);
pr($joined);
pr($vehicles);
exit();
*/
    $show_days = 210;
    $days = [];
    foreach(range(0,$show_days-1) as $day_nr){
      $days[] = strtotime("$first +$day_nr days");
    }
    $total_width = count($days)*$cell_width;
    ?>
    <div class="col-xs-3 no-padding">
      <div class="cells">
        <div class="header-cell-row">
          <div class="header-cell-vehicle">
            <div class="box">
              <div class="box-body">
                <a class="btn btn-primary" href="{{ROOTURL}}/admin/booking/edit"><i class="fa fa-plus"></i> Otvori novi najam {{total_width}}</a>
              </div>
            </div>
          </div>
        </div>
        @loop vehicles
          <div class="cell-row" data-type="{{vehicle.type}}">
            <div class="cell-vehicle"><span style="background-color:{{vehicle.color_hex}};padding:1px 8px;">&nbsp;</span>&nbsp;{{vehicle.name}} 
              @if vehicle.avg_kms > 0
                @if vehicle.avg_kms < 9999
                &nbsp;&nbsp;<span style="color:#444;font-size:11px">({{vehicle.avg_kms}} km/dan)</span>
                @end
              @end
            </div>
          </div>
        @end
      </div>
    </div>
    <div id="my-event-rows" class="col-xs-9 no-padding">
      <div class="cells dragscroll" style="overflow-x: scroll; cursor: grab; cursor : -o-grab; cursor : -moz-grab; cursor : -webkit-grab;">
        <div class="header-cell-row" style="width: {{total_width}}px; overflow: hidden;">
          <?php
            $m = 0;
            foreach($days as $day){
              if( date('m',$day)!==$m ){
                $m = date('m',$day);
                $nd = date('t',$day);
                $month_width = intval($nd)*$cell_width;
                ?>
                <div class="header-cell-month" style="width: {{month_width}}px !important;">
                  <?php echo date('n',$day);?> mj.
                </div>
                <?php
              }
            }
          ?>
          <div class="clearfix"></div>
          

          @loop days
              <div class="header-cell-day {{weekendClass($day)}}"><?php echo date('j',$day);?><br><?php echo timeline_localize(date('D',$day));?></div>
          @end
        </div>
        @loop vehicles
          <div class="cell-row" id="vehicle-row-{{vehicle.id}}" data-type="{{vehicle.type}}" style="width: {{total_width}}px; overflow: hidden;">
            @loop days
              <div class="cell-day {{weekendClass($day)}}"><?php echo timeline_localize(date('d',$day));?></div>
            @end
          </div>
        @end
      </div>
    </div>

  </div>
</section><!-- /.content -->

<script>
$(document).ready(function(){

});

function filterVehicleTypes(el){
  var selected = $(el).val();
  if( selected=='all' || selected=='' ){
    $('[data-type]').show();
  }else{
    $('[data-type]').hide();
    $('[data-type="'+selected+'"]').show();
  }
}

var all = [];
$(function () {
    $('body').on('click','.cell-line',function(event){
        // open modal
        $('#ajaxModal').remove();

        var index = $(this).data('index');
        var obj = all[index];
        
        if( obj.model == 'Booking' ){
            var $remote = '/admin/booking/modal/'+obj.id;
        }else if( obj.model == 'VehicleService' ){
            var $remote = '/admin/vehicleservice/modal/'+obj.id;
        }else if( obj.model == 'Vehicle' ){
            var $remote = '/admin/vehicle/modal/'+obj.id;
        } 

        var $modal = $('<div class="modal fade" id="ajaxModal"></div>');
        $('body').append($modal);
        $modal.modal({backdrop: true, keyboard: true, show: false});
        $modal.load($remote,function(){
            $('.modal-body').addClass('md-effect-1');
            $('#ajaxModal').modal('show');
        });

        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
        
        $('#ajaxModal').on('show.bs.modal', function (e) {
          $('.modal-body').addClass('md-effect-1').addClass('md-show');
        });
    });

    function getEvents(){
        $.ajax({
            url: '/admin/timeline/events',
            type: 'POST',
            dataType: 'json',
            data: {'start':'','end':'','vehicles':'*'},
        })
        .done(function(data) {
            all = data;
            $.each(data, function(index, obj) {
                 // console.log(obj);
                 var cell = $('#vehicle-row-'+obj.vehicle_id+'');
                 var left = obj.cell_from*45;
                 var width = (obj.cell_to-obj.cell_from+1);
                 /*
                 if( width == 0 ){
                    width = 1;
                 }
                 */
                  var content = '<b>'+obj.title+'</b><br>';
                  if( obj.model == 'VehicleService' ){
                    content += 'SERVIS<br>';                    
                  }
                  if( obj.start_formated>0 ){
                    content += obj.start_formated+'<br>';
                  }
                  if( obj.end_formated>0 ){
                    content += obj.end_formated+'<br>';
                  }
                  if( obj.contracted_km>0 ){
                    content += 'Ugovoreno '+obj.contracted_km+' km<br>';
                  }

                 width = (width*45)-5;
                 //copiedEventObject.backgroundColor = $(this).css("background-color");
                 $(cell).append('<div style="left:'+left+'px;width:'+width+'px;background-color:'+obj.backgroundColor+';border-color:'+obj.borderColor+'" id="'+obj.model+'-'+obj.id+'" class="cell-line model'+obj.model+'" data-toggle="popover" data-placement="top" data-index="'+index+'" data-content="'+content+'">'+obj.title+'</div>');
            });

            // init popover on all
            $('[data-toggle="popover"]').popover({
              trigger:'hover',
              placement: 'mouse',
              html:true,
              viewport:'body'
            });

            // slide to show today
            $('.cells.dragscroll').scrollLeft(1390);

        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
        
    }
    getEvents();
});


</script>
