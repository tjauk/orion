<html>
 <head>
  <meta http-equiv="refresh" content="60">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head> 
<body>
<div class="row">
<div class="col-md-12">
<?php
$folder = __DIR__.'/cache/';


if( !empty($_REQUEST['date']) ){
	$file = date('Y-m-d',strtotime($_REQUEST['date'])).'.log';
}else{
	$file = date('Y-m-d').'.log';
}

if( !file_exists($folder.$file) ){
	exit("No log");
}
$raw = file_get_contents($folder.$file);
$lines = explode("\n",$raw);

// last 10 lines
$num_of_lines = 500;
$last_lines = array_slice($lines, -1*$num_of_lines, $num_of_lines);

foreach (range(7,0) as $day) {
	$date = date('Y-m-d',strtotime('now -'.$day.' day'));
	echo '<a href="/?date='.$date.'">'.$date.'</a>&nbsp;';
}
?>
<table class="table" style="width:100%">
	<?php foreach ($last_lines as $i => $line){
		if( !empty($line) ){
			list($time,$uid,$message,$data) = explode(" | ",$line);
	?>
	<tr>
		<td><?php echo $time;?></td>
		<td><?php echo $uid;?></td>
		<td><?php echo $message;?></td>
		<td><?php echo $data;?></td>
	</tr>
	<?php }; }; ?>
</table>
</div>
</div>
</body>
</html>