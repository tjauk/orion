#!/bin/bash

cd /var/www/orion/

# set all folders
declare -a folders=(events_netherlands events_germany events_norway events_denmark events_finland events_sweden events_switzerland events_austria events_ireland events_uk events_belgium events_spain events_italy events_portugal events_southafrica events_greece events_luxembourg events_malta events_czech)

# loop through folders and execute
for folder in "${folders[@]}"
do
	cd $folder
	echo ""
	echo $folder
	php -f orion migrate
	cd ..
done
