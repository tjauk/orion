#!/bin/bash

cd /var/www/orion/

# set all folders
declare -a folders=( bags events_spain events_italy events_germany )

# loop through folders and execute
for folder in "${folders[@]}"
do
	cd $folder
	echo ""
	echo $folder
	php -f orion migrate
	cd ..
done
