#!/bin/bash

cd /var/www/orion/

# set all folders
declare -a folders=(events_australia events_brazil events_colombia events_japan events_newzealand events_mexico events_singapore)

# loop through folders and execute
for folder in "${folders[@]}"
do
	cd $folder
	echo ""
	echo $folder
	php -f orion migrate
	cd ..
done
