#!/bin/bash

cd /var/www/orion/

# set all folders
declare -a folders=(area51_dev finaliza_app fintel_app moocho_app exceed_app rent_app demo_renta rentalica_app chipoteka_app crodeals_app grey_app apis_app quan_app events_croatia events_netherlands events_norway events_denmark events_finland events_sweden events_germany events_switzerland events_austria events_ireland events_poland events_portugal events_mexico events_luxembourg events_malta)

# loop through folders and execute
for folder in "${folders[@]}"
do
	cd $folder
	echo ""
	echo $folder
	php -f orion migrate
	cd ..
done
