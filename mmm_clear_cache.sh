#!/bin/bash

# set all folders
declare -a folders=(events_netherlands events_norway events_denmark events_finland events_sweden events_germany events_switzerland events_austria events_ireland events_uk events_belgium events_spain events_italy events_portugal events_japan events_southafrica events_greece events_luxembourg events_malta)

# loop through folders and execute
for folder in "${folders[@]}"
do
	find /var/www/orion/$folder/cache/globals/*.same.* -type f -delete
	find /var/www/orion/$folder/cache/globals/city.* -type f -delete
	find /var/www/orion/$folder/cache/globals/home.* -type f -delete
done

echo "Cache cleared on MMM server"