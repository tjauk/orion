<?php
// you can add functions before return array call and they will get executed


/*
https://graph.facebook.com/oauth/access_token?client_id=<your FB App ID >&client_secret=<your FB App secret>&grant_type=fb_exchange_token&fb_exchange_token=<your short-lived access token>

https://graph.facebook.com/224274760957225/accounts?access_token=EAAWRiqXPsh0BAAmXLMtsgIUWUN8w8S7LjmwzaWZAuByQkDGVZC9iLv9dNAClZCy9SPoTTZCu19or59hXchCLaWeXIApWZAmeZAfgLXtysaKEqszEEi4ZAlIGrpLYJYE7jQDbPPOyl9Ohnw60U6hbJVWfnSPGlHXAnQZD
*/

return [
	[
		'name'					=> "App: NonStop.hr, User: Tihomir Jauk",
		'fb_app_id'				=> "217657718287836",
		'fb_app_secret'			=> "1738c59cfeed960ca6bfaa139b6ca297",	
		'fb_page_access_token'	=> "CAADF9WL7LdwBAOG3V50eiOdLDDmO5CNcpK4C4LDpd7h4W67r7UKQP7q383Hyls7UFYGVT0wyu2auKDXPZCZC4ZAaRlv9tB60PezQtTNGmLzaQ68IiOMTE2ZBKJkFYA0qvDZAIxUepeTYdUCe5wLJWo4zZBbwYYuJmqETrhC3a9QhATZBQ5YUgF0a2ZAqjkiCi0gZD",
	],

	[
		'name'					=> "App: The Hive, User: Tihomir Jauk",
		'fb_app_id'				=> "1567399556919837",
		'fb_app_secret'			=> "2de3ffe5af5b54e5f2ecec96c3cb440e",	
		'fb_page_access_token'	=> "EAAWRiqXPsh0BAAmXLMtsgIUWUN8w8S7LjmwzaWZAuByQkDGVZC9iLv9dNAClZCy9SPoTTZCu19or59hXchCLaWeXIApWZAmeZAfgLXtysaKEqszEEi4ZAlIGrpLYJYE7jQDbPPOyl9Ohnw60U6hbJVWfnSPGlHXAnQZD",
	],

	[
		'name'					=> "App: vegicept.com, User: Tihomir Jauk",
		'fb_app_id'				=> "297044553750380",
		'fb_app_secret'			=> "1c59208f83dc5056ef125f9139b6fb31",	
		'fb_page_access_token'	=> "EAAEOKRMb12wBAMoX661qieIx6GAwKqe390ruKLy02IOZA8u9ONBfqc7S4QrG8uMEZCIQ0sI6ob04J5J3nJjRmftDd1XgxOUYDQ7eMsHPVsHYjqyBJHHMTkRBw6z8dZAZB3CBQYzWBUAM5zBI9jxM64vmXIu5qCVdJnW49WzccgZDZD",
	],

	[
		'name'					=> "App: Rexona, User: Tihomir Jauk",
		'fb_app_id'				=> "310529975772355",
		'fb_app_secret'			=> "d40b5d5cb957e98544e49c9959603cb0",	
		'fb_page_access_token'	=> "EAAEOKRMb12wBAJWwxoJVr72NZCyRwQNaCoYKJNm5LpZAgKI7WGbimNZANZBAvNGmgIHVWwyd3ZBq73Kr39yy481XFrN9b4QQ2ezf5qKHwLAX1mZCZCUm5Ssq9s3xbfIiPX7BfukKVQoWPjNuITGQDBzQYi0JushadtL2ZApzu7HZAcwZDZD",
	],

	[
		'name'					=> "App: Excedo Company, User: Tihomir Jauk",
		'fb_app_id'				=> "",
		'fb_app_secret'			=> "",	
		'fb_page_access_token'	=> "EAAEOKRMb12wBAEygIhcVuWnursdH23wLzj0QC7Nh5ShQlyGm4qriYfIVDYtLyUkmYPAx3ds3Y7SD3Oh5UgVhH6StM5QRwB3mAIklFr09mbixfgSo9GcnM5iVDZBHEe9Lz10K6uClLT9N9XBmMkaf1mqK1V2Q7g4JdSDq7WgZDZD",
	],

	[
		'name'					=> "App: Rexona - Živi UPsolutno Community, User: Tihomir Jauk",
		'fb_app_id'				=> "",
		'fb_app_secret'			=> "",	
		'fb_page_access_token'	=> "EAAEOKRMb12wBAJWwxoJVr72NZCyRwQNaCoYKJNm5LpZAgKI7WGbimNZANZBAvNGmgIHVWwyd3ZBq73Kr39yy481XFrN9b4QQ2ezf5qKHwLAX1mZCZCUm5Ssq9s3xbfIiPX7BfukKVQoWPjNuITGQDBzQYi0JushadtL2ZApzu7HZAcwZDZD",
	],

	[
		'name'					=> "App: Testna faca, User: Tihomir Jauk",
		'fb_app_id'				=> "",
		'fb_app_secret'			=> "",	
		'fb_page_access_token'	=> "EAAEOKRMb12wBAKsOG2QixpwhjYRZBvwo1ZAtATn6WGB9XzOXeGisZBDPc5MIonVTF5fVAYUT7QQDlZCzS6VlI3kjp0ezhcDDArgYZBeQLmrNa1RoRQfEA3ZC3MAi7G1aDMluJh3VZCiOXTQGGqLMyK3DejA2JKutfNPZAqjMZBZC2W2gZDZD",
	],

];
