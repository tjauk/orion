<?php
class AdminBeaconCatcherController extends AdminController
{
    public $model = 'BeaconCatcher';
    public $baseurl = '/admin/beacon_catcher';

    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Beacon Catcher',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'uid,minor,major,action,mac' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Per page');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');

        $table = Table::make()
                ->headings(['#','Uid','Minor','Major','Distance','Time','Action','Mac','Lat','Lon','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = $item->uid;
            $data[] = $item->minor;
            $data[] = $item->major;
            $data[] = $item->distance;
            $data[] = $item->time;
            $data[] = $item->action;
            $data[] = $item->mac;
            $data[] = $item->lat;
            $data[] = $item->lon;
            $actions = UI::btnEdit($item->id,'beacon_catcher');
            $actions.= UI::btnDelete($item->id,'beacon_catcher');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('beaconcatcher');
        $form->label('Uid')->input('uid');

        $form->label('Minor')->number('minor');

        $form->label('Major')->number('major');

        $form->label('Distance')->input('distance');

        $form->label('Time')->datePicker('time');

        $form->label('Action')->input('action');

        $form->label('Mac')->input('mac');

        $form->label('Lat')->input('lat');

        $form->label('Lon')->input('lon');


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/beacon_catcher')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
