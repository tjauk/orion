<?php
class AdminBeaconController extends AdminController
{
    public $model = 'Beacon';
    public $baseurl = '/admin/beacon';

    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Beacon',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Per page');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');

        $table = Table::make()
                ->headings(['#','Client','Code','Major','Minor','Status','Note','Immediate','Near','Far','Lat','Lon','Address','City','Region','Country','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = $item->client_id;
            $data[] = $item->code;
            $data[] = $item->major;
            $data[] = $item->minor;
            $data[] = $item->status;
            $data[] = $item->note;
            $data[] = $item->immediate;
            $data[] = $item->near;
            $data[] = $item->far;
            $data[] = $item->lat;
            $data[] = $item->lon;
            $data[] = $item->address;
            $data[] = $item->city;
            $data[] = $item->region;
            $data[] = $item->country_id;
            $actions = UI::btnEdit($item->id,'beacon');
            $actions.= UI::btnDelete($item->id,'beacon');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('beacon');
        $form->label('Client Id')->input('client_id');

        $form->label('Code')->input('code');

        $form->label('Major')->input('major');

        $form->label('Minor')->input('minor');

        $form->label('Status')->input('status');

        $form->label('Note')->text('note');

        $form->label('Immediate')->input('immediate');

        $form->label('Near')->input('near');

        $form->label('Far')->input('far');

        $form->label('Lat')->input('lat');

        $form->label('Lon')->input('lon');

        $form->label('Address')->input('address');

        $form->label('City')->input('city');

        $form->label('Region')->input('region');

        $form->label('Country Id')->input('country_id');


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/beacon')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
