<?php
class AdminEventController extends AdminController
{
    public $model = 'Event';
    public $baseurl = '/admin/event';

    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Event',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'title' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Per page');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');

        $table = Table::make()
                ->headings(['#','Status','User','Title','Slug','Place','Starts','Ends','Repeatable','Repeat Mode','Repeat Func','Src','Src Uid','Summary','Content','Photos','Cover','Flyer','Ticketed','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = $item->status;
            $data[] = $item->user_id;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->title.'</a>';
            $data[] = $item->slug;
            $data[] = $item->place_id;
            $data[] = $item->starts;
            $data[] = $item->ends;
            $data[] = $item->repeatable;
            $data[] = $item->repeat_mode;
            $data[] = $item->repeat_func;
            $data[] = $item->src;
            $data[] = $item->src_uid;
            $data[] = $item->summary;
            $data[] = $item->content;
            $data[] = $item->photos;
            $data[] = $item->cover;
            $data[] = $item->flyer;
            $data[] = $item->ticketed;
            $actions = UI::btnEdit($item->id,'event');
            $actions.= UI::btnDelete($item->id,'event');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('event');
        $form->label('Status')->input('status');

        $form->label('User Id')->input('user_id');

        $form->label('Title')->input('title');

        $form->label('Slug')->input('slug');

        $form->label('Place Id')->input('place_id');

        $form->label('Starts')->datepicker('starts');

        $form->label('Ends')->datepicker('ends');

        $form->label('Repeatable')->input('repeatable');

        $form->label('Repeat Mode')->input('repeat_mode');

        $form->label('Repeat Func')->input('repeat_func');

        $form->label('Src')->input('src');

        $form->label('Src Uid')->input('src_uid');

        $form->label('Summary')->text('summary');

        $form->label('Content')->text('content');

        $form->label('Photos')->text('photos');

        $form->label('Cover')->text('cover');

        $form->label('Flyer')->text('flyer');

        $form->label('Ticketed')->input('ticketed');


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/event')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
