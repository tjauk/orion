<?php
use HiveBee as Bee;
use HiveWork as Work;

class AdminHiveController extends AdminController
{
    public $model = 'HiveBee';
    public $baseurl = '/admin/hivebee';



    /**
     * Index page
     *
     * @return View
     */
    function works()
    {        
        Doc::title('Works');

        $items = new Work();

        $buttons = Forms::make();
        $buttons->btnAdd('Add work','/admin/hive/work');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name,description' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search')->search('keywords')->placeholder('Search');
        $form->label('Per page')->select('perpage')->values([10,20,50,100]);

        $table = Table::make()
                ->headings(['#','Name','Description','Work','Type','Params','Throttle','Last Run','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = '<a href="/admin/hive/work/'.$item->id.'">'.$item->name.'</a>';
            $data[] = $item->description;
            $data[] = $item->work;
            $data[] = $item->work_type;
            $data[] = $item->work_params;
            $data[] = $item->throttle;
            $data[] = $item->last_run;
            $actions = UI::btnEdit($item->id,'/admin/hive/work');
            $actions.= UI::btnDelete($item->id,'/admin/hive/deletework');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    /**
     * Index page
     *
     * @return View
     */
    function bees()
    {        
        Doc::title('Bees');

        $items = new Bee();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Bee','/admin/hive/bee');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name,ipaddress,keys,last_run' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search')->search('keywords')->placeholder('Search');
        $form->label('Per page')->select('perpage')->values([10,20,50,100]);

        $table = Table::make()
                ->headings(['#','Name','Endpoint','IP address','Keys','Is Proxy','Last Run','Status','Active','Ver.','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = '<a href="/admin/hive/bee/'.$item->id.'">'.$item->name.'</a>';
            $data[] = $item->endpoint;
            $data[] = $item->ipaddress;
            $data[] = $item->keys;
            $data[] = $item->is_proxy;
            $data[] = $item->last_run;
            if( strtotime($item->last_run)>time()-30 ){
                $data[] = 'RUNNING';
            }else{
                $data[] = $item->status;
            }
            $data[] = UI::checked($item->active);
            $data[] = '<a href="/admin/hive/checkbee/'.$item->id.'">'.$item->version.'</a>';
            $actions = UI::btnEdit($item->id,'/admin/hive/bee/');
            $actions.= UI::btnDelete($item->id,'/admin/hive/deletebee/');
            $actions.= UI::btn()->title('UPDATE')->icon('fa-cog')->link('/admin/hive/updatebee/'.$item->id)->addClass('btn-warning');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }




    /**
     * Edit Work
     *
     * @return View
     */
    function work($id=0)
    {
        $model = 'Work';
        
        Doc::title('Work');

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect('/admin/hive/works')->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('hivework');
        $form->label('Name')->input('name');
        $form->label('Description')->text('description');

        $form->label('Work')->input('work');
        $form->label('Work Type')->input('work_type');
        $form->label('Work Params')->input('work_params');

        $form->label('Throttle')->input('throttle');

        $form->label('Capabilities')->text('capabilities');

        $form->label('Last Run')->input('last_run')->readonly();

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }



    /**
     * Edit Bees
     *
     * @return View
     */
    function bee($id=0)
    {
        $model = 'Bee';
        
        Doc::title('Bee');

        $item = Bee::firstOrNew(['id'=>$id]);

        $post = Request::post();
        if( $post ){
            if( isset($post['active']) ){
                $post['active'] = 1;
            }else{
                $post['active'] = 0;
            }
            $item->fill($post);
            $item->save();
            return redirect('/admin/hive/bees')->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('hivebee');
        $form->label('Name')->input('name');

        $form->label('Endpoint')->input('endpoint');

        $form->label('Keys')->text('keys');

        $form->label('Is Proxy')->radioGroup('is_proxy')->options(["0"=>"No","1"=>"Yes"]);

        $form->label('Capabilities')->text('capabilities');

        $form->label('Last Run')->input('last_run')->readonly();

        $form->label('Status')->input('status');

        $form->label('Active/Disabled')->checkBox('active');

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }


    /**
     * Delete Bee
     *
     * @return Response
     */
    function deletebee($id=0)
    {
        $item = Bee::find($id);
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/hive/bees')->with('msg',"Bee #{$item->id} deleted");
    }

    /**
     * Delete Work
     *
     * @return Response
     */
    function deletework($id=0)
    {
        $item = Work::find($id);
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/hive/works')->with('msg',"Work #{$item->id} deleted");
    }


    /**
     * Check Bee
     *
     * @return Response
     */
    function checkbee($id=0)
    {
        $bee = Bee::find($id);
        if( $bee->id>0 ){
            $url = $bee->script;
            $result = json_decode(Remote::get($url),true);
            $bee->version = $result['bee_version'];
            $bee->save();
        }
        return redirect('/admin/hive/bees')->with('msg',"Bee #{$item->id} checked");
    }


    /**
     * Update Bee
     *
     * @return Response
     */
    function updatebee($id=0)
    {
        $bee = Bee::find($id);
        if( $bee->id>0 ){
            $url = $bee->script;
            $data = [
                'do'=>'UPDATE',
                'payload'=>base64_encode(file_get_contents(ROOTDIR.'/proxybee.php'))
            ];
            Remote::post($url,$data);
            return $this->checkbee($bee->id);
        }
        return redirect('/admin/hive/bees')->with('msg',"Bee #{$item->id} not found");
    }

}
