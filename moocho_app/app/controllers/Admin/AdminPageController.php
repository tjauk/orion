<?php
class AdminPageController extends AdminController
{
    public $model = 'Page';
    public $baseurl = '/admin/page';

    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'title' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Per page');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');

        $table = Table::make()
                ->headings(['#','Created At','Updated At','Status','User Id','Title','Slug','Summary','Content','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = $item->created_at;
            $data[] = $item->updated_at;
            $data[] = $item->status;
            $data[] = $item->user_id;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->title.'</a>';
            $data[] = $item->slug;
            $data[] = $item->summary;
            $data[] = $item->content;
            $actions = UI::btnEdit($item->id,'page');
            $actions.= UI::btnDelete($item->id,'page');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('page');
        $form->label('Status');
        $form->input('status')->klass('form-control');

        $form->label('User Id');
        $form->input('user_id')->klass('form-control');

        $form->label('Title');
        $form->input('title')->klass('form-control');

        $form->label('Slug');
        $form->input('slug')->klass('form-control');

        $form->label('Summary');
        $form->input('summary')->klass('form-control');

        $form->label('Content');
        $form->input('content')->klass('form-control');


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/page')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
