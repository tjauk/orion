<?php
class AdminPlaceController extends AdminController
{
    public $model = 'Place';
    public $baseurl = '/admin/place';

    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Place',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Per page');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');

        $table = Table::make()
                ->headings(['#','Status','User','Name','Slug','Lat','Lon','Radius','Address','City','Region','Country','Raw','Src','Src Uid','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = $item->status;
            $data[] = $item->user_id;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->name.'</a>';
            $data[] = $item->slug;
            $data[] = $item->lat;
            $data[] = $item->lon;
            $data[] = $item->radius;
            $data[] = $item->address;
            $data[] = $item->city;
            $data[] = $item->region;
            $data[] = $item->country_id;
            $data[] = $item->raw;
            $data[] = $item->src;
            $data[] = $item->src_uid;
            $actions = UI::btnEdit($item->id,'place');
            $actions.= UI::btnDelete($item->id,'place');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('place');
        $form->label('Status')->input('status');

        $form->label('User Id')->input('user_id');

        $form->label('Name')->input('name');

        $form->label('Slug')->input('slug');

        $form->label('Lat')->input('lat');

        $form->label('Lon')->input('lon');

        $form->label('Radius')->input('radius');

        $form->label('Address')->input('address');

        $form->label('City')->input('city');

        $form->label('Region')->input('region');

        $form->label('Country Id')->input('country_id');

        $form->label('Raw')->text('raw');

        $form->label('Src')->input('src');

        $form->label('Src Uid')->input('src_uid');


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/place')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
