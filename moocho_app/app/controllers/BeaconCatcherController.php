<?php
class BeaconCatcherController extends Controller
{

    /**
     * 
     *
     * @return Response
     */
    public function index()
    {
        return BeaconCatcher::all();
    }

    /**
     * 
     *
     * @return Response
     */
    public function create()
    {
        if( Request::post() ){
            return BeaconCatcher::create(Request::post());
        }
        return "Use POST method";
    }


}
