<?php
use HiveBee as Bee;

class HiveController extends Controller
{
    
    function __construct()
    {
        parent::__construct();
        Doc::json();
    }

    /**
     * INDEX
     */
    function index()
    {
        $ctrlname = str_replace('Controller','',__CLASS__);
        echo h3( $ctrlname );
        $actions = get_class_methods( __CLASS__ );
        sort( $actions );
        foreach( $actions as $action ) {
            if( substr( $action, 0, 1 ) !== '_' and substr( $action, 0, 4 ) !== 'post' and substr( $action, 0, 6 ) !== 'widget' ) {
                    $path = strtolower( $ctrlname ).'/'.$action;
                    echo  '<a href="/'.$path.'">'.$path.'</a><br>';
            }
        }
    }

    /**
     * REGISTER
     */
    function register(){
        $data = Request::all();
        $bee = Bee::findByIpaddress($data['ipaddress']);
        
        if( !$bee ){
            $bee = new Bee();
            $bee->endpoint = $data['endpoint'];
            $bee->name = str_replace('http://','',$data['endpoint']);
            $bee->ipaddress = $data['ipaddress'];
            $bee->is_proxy = 1;
            $bee->status = 'IDLE';
        }

        $bee->capabilities = json_encode(['_SERVER'=>$data['_SERVER'],'_ENV'=>$data['_ENV']]);
        $bee->save();

        return $bee->id;
    }

    /**
     * CHECK
     */
    function check(){
        return $_REQUEST;
    }

    /**
     * REPORT
     */
    function report(){
        return $_REQUEST;
    }


}
