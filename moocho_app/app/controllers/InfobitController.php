<?php
use InfoBit as Bit;
use InfoNet as Net;

class InfobitController extends Controller
{

    function test1()
    {
        Doc::json();
        Bit::label('word');
        $e1 = Bit::add("Bird");
        $e2 = Bit::add("Dog");
        $e3 = Bit::add("Cat");
        Net::connect($e1,$e2,0.5);
        Net::connect($e1,$e3,0.5);
        Net::connect($e2,$e3,0.5);

        return Net::all();
    }

    function test2()
    {
        Bit::label('word');

        $text = $this->text();
        //$result = preg_split('/((^\p{P}+)|(\p{P}*\s+\p{P}*)|(\p{P}+$))/', $text, -1, PREG_SPLIT_NO_EMPTY);
        $words = mb_str_split($text);
        $words = array_values($words);

        $c = count($words)-1;
        for($i=0;$i<$c;$i++){
            Net::label('word');
            Net::vector($words[$i],$words[$i+1]);
        }

        $title = Bit::add('Artificial intelligence','title');
        $un = array_unique($words);
        foreach($un as $word){
            $word = Bit::add($word,'word');
            Net::vector($word,$title);
        }

    }


    function test3()
    {

    }

    private function text()
    {
        return "
Artificial intelligence
From Wikipedia, the free encyclopedia
Changes must be reviewed before being displayed on this page.
Jump to: navigation, search
\"AI\" redirects here. For other uses, see Ai and Artificial intelligence (disambiguation).

Artificial intelligence (AI) is the intelligence exhibited by machines or software. It is also the name of the academic field of study which studies how to create computers and computer software that are capable of intelligent behavior. Major AI researchers and textbooks define this field as \"the study and design of intelligent agents\",[1] in which an intelligent agent is a system that perceives its environment and takes actions that maximize its chances of success.[2] John McCarthy, who coined the term in 1955,[3] defines it as \"the science and engineering of making intelligent machines\".[4]

AI research is highly technical and specialized, and is deeply divided into subfields that often fail to communicate with each other.[5] Some of the division is due to social and cultural factors: subfields have grown up around particular institutions and the work of individual researchers. AI research is also divided by several technical issues. Some subfields focus on the solution of specific problems. Others focus on one of several possible approaches or on the use of a particular tool or towards the accomplishment of particular applications.

The central problems (or goals) of AI research include reasoning, knowledge, planning, learning, natural language processing (communication), perception and the ability to move and manipulate objects.[6] General intelligence is still among the field's long-term goals.[7] Currently popular approaches include statistical methods, computational intelligence and traditional symbolic AI. There are a large number of tools used in AI, including versions of search and mathematical optimization, logic, methods based on probability and economics, and many others. The AI field is interdisciplinary, in which a number of sciences and professions converge, including computer science, mathematics, psychology, linguistics, philosophy and neuroscience, as well as other specialized fields such as artificial psychology.

The field was founded on the claim that a central property of humans, human intelligence—the sapience of Homo sapiens sapiens—\"can be so precisely described that a machine can be made to simulate it.\"[8] This raises philosophical arguments about the nature of the mind and the ethics of creating artificial beings endowed with human-like intelligence, issues which have been explored by myth, fiction and philosophy since antiquity.[9] Artificial intelligence has been the subject of tremendous optimism[10] but has also suffered stunning setbacks.[11] Today AI techniques have become an essential part of the technology industry, providing the heavy lifting for many of the most challenging problems in computer science.[
";
    }
}


function mb_str_split( $string ) {
  $split = preg_split('/\b([\(\).,\-\',:!\?;"\{\}\[\]„“»«‘\r\n]*)/u', $string, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
  return array_filter($split, 'trim_filter');
}

function trim_filter($val) {
  if (trim($val) != '') {
    return trim($val);
  }
  return false;
}