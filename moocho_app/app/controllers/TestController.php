<?php

class TestController extends Controller
{

    function index()
    {
        $ctrlname = str_replace('Controller','',__CLASS__);
        echo h3( $ctrlname );
        $actions = get_class_methods( __CLASS__ );
        sort( $actions );
        foreach( $actions as $action ) {
            if( substr( $action, 0, 1 ) !== '_' and substr( $action, 0, 4 ) !== 'post' and substr( $action, 0, 6 ) !== 'widget' ) {
                    $path = strtolower( $ctrlname ).'/'.$action;
                    echo  '<a href="/'.$path.'">'.$path.'</a><br>';
            }
        }
    }

    /**
     * REGISTER
     */
    function register(){
        return $_REQUEST;
    }

    function two(){
        return $_REQUEST;
    }

    function three(){
        return $_REQUEST;
    }

    function view($id=0){
        return $id;
    }

    function byTitle($id=0){
        return Article::findByTitle('test');
    }

    function all(){
        return Article::all();
    }

    function getShow(){
        return __METHOD__;
    }

    function server(){
        return $_SERVER;
    }

    function env($name){
        return getenv($name);
    }

    function session(){
        return $_SESSION;
    }

    function json(){
    	return ['key'=>"val"];
    }

    function response(){
        $response = new Response();
        $response->setContent('My response');
        $response->setStatusCode(Response::HTTP_NOT_FOUND);
        return $response;
    }
    
    function schema(){
        if( !Schema::hasTable('users') ){
            Schema::create('users', function(Blueprint $table)
            {
                $table->increments('id');
                $table->timestamps();
                $table->string('username')->unique();
                $table->string('email')->unique();
                $table->string('password', 60);
                $table->string('first_name')->default('');
                $table->string('last_name')->default('');
                $table->rememberToken();
            });
        }
        if( !Schema::hasTable('password_resets') ){
            Schema::create('password_resets', function(Blueprint $table)
            {
                $table->timestamps();
                $table->string('email')->index();
                $table->string('token')->index();
            });
        }
        return true;
    }
    function drop(){
        if(Schema::hasTable('users') ){
            Schema::drop('users');
        }
        if(Schema::hasTable('password_resets') ){
            Schema::drop('password_resets');
        }
        return true;
    }

    function query()
    {
        return DB::table("articles")->get();
    }

    function columns()
    {
        return Schema::getColumnListing("articles");
    }

    function select()
    {
        //return get_class_methods(Capsule::$instance->connection());
        return DB::select("SELECT * FROM articles LIMIT 1",[]);
    }

    function fill(){
        $id = DB::table('articles')->insertGetId(['title'=>"Test"]);
        return $id;
    }

    function fluid(){
        // create this using fluid model concept as in previous tests
        $book = new Book();
        $book->title = 'My title';
        $book->author = 'B.H.';
        $book->save();

    }

    function fb($address='Maksimirska 132,Zagreb'){
        Doc::none();
        Doc::json();
        //echo "Testing Facebook SDK";
        $info = Geo::code($address);

        $lat = $info['lat'];
        $lon = $info['lon'];
        $access_token = config('app.fb_page_access_token');

        $data = file_get_contents("https://graph.facebook.com/search?q=bar&type=place&center=$lat,$lon&distance=2000&access_token=$access_token");
        return $data;

        dd($data);
        // Create our Application instance (replace this with your appId and secret).
        $facebook = new Facebook(array(
            'appId'  => '217657718287836',
            'secret' => '1738c59cfeed960ca6bfaa139b6ca297',
        ));

        $user = $facebook->getUser();

        pr($user);
    }

    function compress()
    {
        $path = ROOTDIR.'/files/3665.html';
        $data = File::load($path);
        $cpath = ROOTDIR.'/files/3665.html.gz';
        File::compress($cpath,$data);
        $unc = File::uncompress($cpath);
        echo $unc;
    }



    function fbmap($country='')
    {
        $country = Request::get('country');
        $every = Request::get('every');

        if( empty($country) ){
            $country='United Kingdom';
        }
        if( empty($every) ){
            $every = 100;
        }
        
        Doc::none();
        $out = <<<EOD
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Markers</title>
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
  </head>
  <body>
    <div id="map"></div>
    <script>

      function initMap() {
        var myLatLng = {lat: 46, lng: 16};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: myLatLng
        });
EOD;
    
    $places = CrawlFacebookPlace::whereCountry($country)->where(DB::raw('id mod '.$every),'=',0)->get(['name','lat','lon']);
    //$places = CrawlFacebookPlace::take(100)->orderBy('id',DESC)->get(['id','name','lat','lon']);
    foreach($places as $place){
        $out.='var marker = new google.maps.Marker({position: {lat: '.$place->lat.', lng: '.$place->lon.'},map: map,title: "'.str_replace('"','\'',$place->id.' '.$place->name).'"});';
    }

        $out.=<<<EOD
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap">
    </script>
  </body>
</html>
EOD;

        return $out;
    }



    function fbevents($country='')
    {
        $country = Request::get('country');
        $every = Request::get('every');

        if( empty($country) ){
            $country='United Kingdom';
        }
        if( empty($every) ){
            $every = 100;
        }
        
        Doc::none();
        $out = <<<EOD
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Markers</title>
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
  </head>
  <body>
    <div id="map"></div>
    <script>

      function initMap() {
        var myLatLng = {lat: 46, lng: 16};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: myLatLng
        });
EOD;
    
    $places = FbeEvent::whereCountry($country)->whereNotNull('lat')->where(DB::raw('id mod '.$every),'=',0)->get(['name','lat','lon']);
    //$places = CrawlFacebookPlace::take(100)->orderBy('id',DESC)->get(['id','name','lat','lon']);
    foreach($places as $place){
        $out.='var marker = new google.maps.Marker({position: {lat: '.$place->lat.', lng: '.$place->lon.'},map: map,title: "'.str_replace(['"',','],['\'',' '],$place->id.' '.$place->name).'"});'."\n";
    }

        $out.=<<<EOD
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap">
    </script>
  </body>
</html>
EOD;

        return $out;
    }


    function infonet()
    {
        $tag1 = 'Radićeva 38';
        $tag2 = 'Zagreb';
        $tag3 = 'Hrvatska';
        
    }

    function train()
    {



        $training_data = [
            ['Notebooks'=>'Notebook Asus GL552VW i7/8GB/1TB/FHD/GTX960M/DOS/crni (K)'],
            ['Notebooks'=>'Notebook Lenovo IdeaPad 100 P/4GB/1TB/GF920/Win10 80QQ0030SC'],
            ['3D'=>'3D olovka, 1,75mm nit'],
            ['Apps'=>'Microsoft Office Home & Bus 2016 Cro Medialess'],
            ['Bateries'=>'Baterija akumulatorska 12V 1,2 Ah 97x48,5x50,5 mm, Fiamm'],
            ['Apps'=>'Microsoft Windows 10 Professional 64-bit Cro DVD'],
            ['Bateries'=>'Baterija alkalna basic AA MN 1500-K4  Duracell'],
            ['Obrt'=>'Obrt za urarske usluge i trgovinu,Marijana Vodanović Brezović'],
            /*['Text'=>'Recall measures the quantity of relevant results returned by a search, while precision is the measure of the quality of the results returned. Recall is the ratio of relevant results returned to all relevant results. Precision is the number of relevant results returned to the total number of results returned.

The diagram at right represents a low-precision, low-recall search. In the diagram the red and green dots represent the total population of potential search results for a given search. Red dots represent irrelevant results, and green dots represent relevant results. Relevancy is indicated by the proximity of search results to the center of the inner circle. Of all possible results shown, those that were actually returned by the search are shown on a light-blue background. In the example only one relevant result of three possible relevant results was returned, so the recall is a very low ratio of 1/3 or 33%. The precision for the example is a very low 1/4 or 25%, since only one of the four results returned was relevant.[3]

Due to the ambiguities of natural language, full text search systems typically includes options like stop words to increase precision and stemming to increase recall. Controlled-vocabulary searching also helps alleviate low-precision issues by tagging documents in such a way that ambiguities are eliminated. The trade-off between precision and recall is simple: an increase in precision can lower overall recall while an increase in recall lowers precision.[4]
See also: Precision and recall'],*/
        ];

        $training_data = [
            ['AI'=>"Artificial intelligence (AI) is the intelligence exhibited by machines or software. It is also the name of the academic field of study which studies how to create computers and computer software that are capable of intelligent behavior. Major AI researchers and textbooks define this field as \"the study and design of intelligent agents\",[1] in which an intelligent agent is a system that perceives its environment and takes actions that maximize its chances of success.[2] John McCarthy, who coined the term in 1955,[3] defines it as \"the science and engineering of making intelligent machines\".[4]

AI research is highly technical and specialized, and is deeply divided into subfields that often fail to communicate with each other.[5] Some of the division is due to social and cultural factors: subfields have grown up around particular institutions and the work of individual researchers. AI research is also divided by several technical issues. Some subfields focus on the solution of specific problems. Others focus on one of several possible approaches or on the use of a particular tool or towards the accomplishment of particular applications.

The central problems (or goals) of AI research include reasoning, knowledge, planning, learning, natural language processing (communication), perception and the ability to move and manipulate objects.[6] General intelligence is still among the field's long-term goals.[7] Currently popular approaches include statistical methods, computational intelligence and traditional symbolic AI. There are a large number of tools used in AI, including versions of search and mathematical optimization, logic, methods based on probability and economics, and many others. The AI field is interdisciplinary, in which a number of sciences and professions converge, including computer science, mathematics, psychology, linguistics, philosophy and neuroscience, as well as other specialized fields such as artificial psychology.

The field was founded on the claim that a central property of humans, human intelligence—the sapience of Homo sapiens sapiens—\"can be so precisely described that a machine can be made to simulate it.\"[8] This raises philosophical arguments about the nature of the mind and the ethics of creating artificial beings endowed with human-like intelligence, issues which have been explored by myth, fiction and philosophy since antiquity.[9] Artificial intelligence has been the subject of tremendous optimism[10] but has also suffered stunning setbacks.[11] Today AI techniques have become an essential part of the technology industry, providing the heavy lifting for many of the most challenging problems in computer science.[12]"],

            ["Science"=>"Science[nb 1] is a systematic enterprise that creates, builds and organizes knowledge in the form of testable explanations and predictions about the universe.[nb 2][2]:58

Contemporary science is typically subdivided into the natural sciences which study the material world, the social sciences which study people and societies, and the formal sciences like mathematics. The formal sciences are often excluded as they do not depend on empirical observations.[3] Disciplines which use science like engineering and medicine may also be considered to be applied sciences.[4]

During the middle ages in the Middle East, foundations for the scientific method were laid by Alhazen in his Book of Optics.[5][6][7] From classical antiquity through the 19th century, science as a type of knowledge was more closely linked to philosophy than it is now and, in fact, in the Western world, the term \"natural philosophy\" encompassed fields of study that are today associated with science, such as astronomy, medicine, and physics.[8]:3[nb 3]

In the 17th and 18th centuries scientists increasingly sought to formulate knowledge in terms of laws of nature. Over the course of the 19th century, the word \"science\" became increasingly associated with the scientific method itself, as a disciplined way to study the natural world. It was in the 19th century that scientific disciplines such as biology, chemistry, and physics reached their modern shapes. The same time period also included the origin of the terms \"scientist\" and \"scientific community,\" the founding of scientific institutions, and increasing significance of the interactions with society and other aspects of culture.[9][10]"],
            ["Science"=>"Biology is a natural science concerned with the study of life and living organisms, including their structure, function, growth, evolution, distribution, and taxonomy.[1] Modern biology is a vast and eclectic field, composed of many branches and subdisciplines. However, despite the broad scope of biology, there are certain general and unifying concepts within it that govern all study and research, consolidating it into single, coherent fields. In general, biology recognizes the cell as the basic unit of life, genes as the basic unit of heredity, and evolution as the engine that propels the synthesis and creation of new species. It is also understood today that all organisms survive by consuming and transforming energy and by regulating their internal environment to maintain a stable and vital condition.

Subdisciplines of biology are defined by the scale at which organisms are studied, the kinds of organisms studied, and the methods used to study them: biochemistry examines the rudimentary chemistry of life; molecular biology studies the complex interactions among biological molecules; botany studies the biology of plants; cellular biology examines the basic building-block of all life, the cell; physiology examines the physical and chemical functions of tissues, organs, and organ systems of an organism; evolutionary biology examines the processes that produced the diversity of life; and ecology examines how organisms interact in their environment.[2]"],
        ];


        $labels = [];
        $totals = [];

        foreach($training_data as $pair){
            $label = key($pair);
            $data = mb_strtolower($pair[$label]);
            echo $data.br();

            $words = ML::tokenizeWords($data,1);
            $words = array_filter($words,function($item){
                if( mb_strlen($item)>3 ){
                    return true;
                }
                return false;
            });
            //pr($words);
            
            $mc = ML::markovChain($words,3);
            $chain = $mc['parts'];
            arsort($chain,SORT_NUMERIC);
            pr($chain);
            
            foreach($chain as $bit=>$nr){
                if( !isset($labels[$label][$bit]) ){
                    $labels[$label][$bit]=0;
                }
                $labels[$label][$bit]+=$nr;
                
                if( !isset($totals[$bit]) ){
                    $totals[$bit]=0;
                }
                $totals[$bit]+=$nr;

            }

        }

        pr($labels);
        pr($totals);

        //ML::save($labels,"Test");     

        $test_data = [
            'Baterija litijeva  CR 2320, Camelion',
            'Microsoft Windows 7 Pro Eng OEM 64-bit',
            'Notebook Toshiba C55-C-1J0  i3/4GB/500GB/GF920M/Win10 bijeli',
            'Air mouse RKM MK706, miš, tipkovnica',
        ];


        $test_data = [
            "\"Neural network\" redirects here. For networks of living neurons, see Biological neural network. For the journal, see Neural Networks (journal). For the evolutionary concept, see Neutral network (evolution).
\"Neural computation\" redirects here. For the journal, see Neural Computation (journal).
Machine learning and
data mining
Kernel Machine.svg
Problems

Supervised learning
(classification • regression)

Clustering

Dimensionality reduction

Structured prediction

Anomaly detection

Neural nets

Theory

Machine learning venues


    Portal icon Machine learning portal

    v t e 

An artificial neural network is an interconnected group of nodes, akin to the vast network of neurons in a brain. Here, each circular node represents an artificial neuron and an arrow represents a connection from the output of one neuron to the input of another.

In machine learning and cognitive science, artificial neural networks (ANNs) are a family of models inspired by biological neural networks (the central nervous systems of animals, in particular the brain) and are used to estimate or approximate functions that can depend on a large number of inputs and are generally unknown. Artificial neural networks are generally presented as systems of interconnected \"neurons\" which exchange messages between each other. The connections have numeric weights that can be tuned based on experience, making neural nets adaptive to inputs and capable of learning.

For example, a neural network for handwriting recognition is defined by a set of input neurons which may be activated by the pixels of an input image. After being weighted and transformed by a function (determined by the network's designer), the activations of these neurons are then passed on to other neurons. This process is repeated until finally, an output neuron is activated. This determines which character was read.

Like other machine learning methods – systems that learn from data – neural networks have been used to solve a wide variety of tasks that are hard to solve using ordinary rule-based programming, including computer vision and speech recognition.",

    "Astronomy, a natural science, is the study of celestial objects (such as stars, galaxies, planets, moons, asteroids, comets and nebulae) and processes (such as supernovae explosions, gamma ray bursts, and cosmic microwave background radiation), the physics, chemistry, and evolution of such objects and processes, and more generally all phenomena that originate outside the atmosphere of Earth. A related but distinct subject, physical cosmology, is concerned with studying the Universe as a whole.[1]

Astronomy is one of the oldest sciences. The early civilizations in recorded history, such as the Babylonians, Greeks, Indians, Egyptians, Nubians, Iranians, Chinese, and Maya performed methodical observations of the night sky. Historically, astronomy has included disciplines as diverse as astrometry, celestial navigation, observational astronomy and the making of calendars, but professional astronomy is nowadays often considered to be synonymous with astrophysics.[2]

During the 20th century, the field of professional astronomy split into observational and theoretical branches. Observational astronomy is focused on acquiring data from observations of astronomical objects, which is then analyzed using basic principles of physics. Theoretical astronomy is oriented toward the development of computer or analytical models to describe astronomical objects and phenomena. The two fields complement each other, with theoretical astronomy seeking to explain the observational results and observations being used to confirm theoretical results.

Astronomy is one of the few sciences where amateurs can still play an active role, especially in the discovery and observation of transient phenomena. Amateur astronomers have made and contributed to many important astronomical discoveries.",
        ];

        foreach($test_data as $input){
            echo $input.br();
            $input = mb_strtolower($input);
            $words = ML::tokenizeWords($input,1);
            $words = array_filter($words,function($item){
                return mb_strlen($item)>3;
            });
            //pr($words);
            
            $mc = ML::markovChain($words,3);
            $chain = $mc['parts'];
            arsort($chain,SORT_NUMERIC);
            //pr($chain);
            
            //find
            $foundlabels = [];
            $foundtotals = [];
            foreach($words as $bit){
                if( isset($totals[$bit]) ){
                    foreach($labels as $label=>$nr){
                        if( isset($labels[$label][$bit]) ){
                            $foundlabels[] = $label; 
                        }
                    }
                }
            }

            $scored = ML::score($foundlabels,5);
            pr($scored);
            if( empty($scored) ){
                echo "Label not found".br();
            }else{
                echo "Labeled: ".b(key($scored)).br();
            }
            echo br();
        }

    }

    function classify()
    {
        $test_data = [
            [''=>'Baterija litijeva  CR 2320, Camelion'],
            [''=>'Microsoft Windows 7 Pro Eng OEM 64-bit'],
            [''=>'Notebook Toshiba C55-C-1J0  i3/4GB/500GB/GF920M/Win10 bijeli'],
            [''=>'Air mouse RKM MK706, miš, tipkovnica'],
        ];

    }


    function extractLinks()
    {
        Doc::none();
        $text = "apurna

Adresa: Klanjec 30a, Rakitje, Bestovje

Telefon: 01 33 85 533, 01 33 85 534

Web stranica: www.annapurna.hr

Bio Giardin

Adresa: Krešimirova 26a, 10000 Rijeka

Telefon: 095 8132 755

Web stranica: www.biogiardin.com

Bio Q

Adresa: Sunčana 11, 31000 Osijek

Telefon: 031 204 250

Bio Shop

Adresa: Bihaćka 2b, 21000 Split

Telefon: 021 486 596

Bio svijet d.o.o

Specijalizirana prodavaonica oganskih proizvoda

Adresa: Ante Starčevića bb, prizemlje Super Konzuma, 23000 Zadar

Telefon: 023 400 018

Web stranica: www.bio-svijet.hr/index.php/hr/kontakt

Bio&bio

Jurišićeva
https://maps.googleapis.com/
Adresa: Jurišićeva 28, 10000 Zagreb

Telefon: 01 48 76 269

Web stranica: www.biobio.com.hr/webshop.aspx

Bio&bio

Ilica

Adresa: Ilica 72, 10000 Zagreb

Telefon: 01 48 48 085

Web stranica: www.biobio.com.hr/webshop.aspx

Bio&bio

Ozaljska

Adresa: Ozaljska 87, 10000 Zagreb

Telefon: 01 36 94 607

Web stranica: www.biobio.com.hr/webshop.aspx";
        $urls = CrawlPage::extractTextualLinks($text);
        pr($urls);
    } 

    public function events()
    {
        $city = Request::get('city');

        $total = FbeEvent::active()
                ->whereCity($city)
                ->where(DB::raw("DATE(start_time)"),'>=',DB::raw("CURRENT_DATE()"))
                ->count();

        echo "<h1>$total events in $city</h1>";

        $events = FbeEvent::active()
                ->whereCity($city)
                ->where(DB::raw("DATE(start_time)"),'=',DB::raw("CURRENT_DATE()"))
                ->get();
        echo "<h2>Today</h2>";
        echo '<div class="row">';
        $i = 0;
        foreach($events as $event){
            echo View::make('event/single')->put($event,'item')->render();
            $i++;
            if( $i==4 ){
                $i=0;
                echo '</div><div class="row">';
            }
        }
        echo '</div>';

        $events = FbeEvent::active()
                ->whereCity($city)
                ->where(DB::raw("DATE(start_time)"),'>',DB::raw("CURRENT_DATE()"))
                ->get();
        echo "<h2>Later</h2>";
        echo '<div class="row">';
        $i = 0;
        foreach($events as $event){
            echo View::make('event/single')->put($event,'item')->render();
            $i++;
            if( $i==4 ){
                $i=0;
                echo '</div><div class="row">';
            }
        }
        echo '</div>';
    }

    public function event($id)
    {
        $event = FbeEvent::find($id);

        return View::make('event/view')->put($event,'item');
    }

    public function cities($country='')
    {
        $country = urldecode($country);
        if( empty($country) ){
            $country = 'Croatia';
        }
        $cities = DB::select( DB::raw("
                    SELECT city as name, COUNT(*) AS total
                    FROM fbe_events
                    WHERE
                        country='$country'
                        AND DATE(start_time)>=CURRENT_DATE()
                        AND duplicate IS NULL
                        AND cover IS NOT NULL
                    GROUP BY city
                    HAVING total>1
                    ORDER BY total DESC
                ") );

        $total = 0;
        foreach($cities as $city){
            echo '<a class="btn btn-primary" style="margin:5px;" href="/test/events/?city='.urlencode($city->name).'">'.$city->name.' ('.$city->total.')</a>';
            $total += $city->total;
        }
        echo "<h4>Total: $total</h4>";

    }

    public function countries()
    {
        $countries = DB::select( DB::raw("
                    SELECT country as name, COUNT(*) AS total
                    FROM fbe_events
                    WHERE
                        country<>''
                        AND DATE(start_time)>=CURRENT_DATE()
                        AND duplicate IS NULL
                        AND cover IS NOT NULL
                    GROUP BY country
                    HAVING total>4
                    ORDER BY total DESC
                ") );

        $total = 0;
        foreach($countries as $country){
            echo '<a class="btn btn-primary" style="margin:5px;" href="/test/cities/'.urlencode($country->name).'">'.$country->name.' ('.$country->total.')</a>';
                $total += $country->total;
        }
        echo "<h4>Total: $total</h4>";

    } 


    public function matrix($country='')
    {

        // 4. table matrix up to 1000 calls
        $total_cities = FbeQuery::whereCountry($country)->whereIn('phase',['1','2'])->where('found','>',1)->count();
        $NR = floor($total_cities/2);
        $faktor = 0.1;

        $cities = collect( FbeQuery::whereCountry($country)
                                    ->whereIn('phase',['1','2'])
                                    ->where('found','>',97)
                                    ->orderBy('found',DESC)
                                    ->take($NR)
                                    ->get(['query'])
                            )->pluck('query')->toArray();

        $morewords = collect( FbeDict::top($country,$NR*2)
                                    ->whereNotIn('word',$cities)
                                    ->get(['word'])
                            )->pluck('word')->toArray();
        $words = [];
        foreach($morewords as $word){
            $has = FbeQuery::whereCountry($country)->whereQuery($word)->first()->found;
            if( $has>97 ){
                $words[] = $word;
            }
        }

        $matrix = [];
        foreach($cities as $ci=>$city){
            foreach($words as $wi=>$word){
                if( ( ($ci+1) * ($wi+1) ) <= $NR ){
                    $matrix[] = $city.' '.$word;
                }
            }
        }
        $total = count($matrix);
        echo "<h1>NR:$NR / F:$faktor / calls:$total</h1>";


        $head = $cities;
        array_unshift($head,'Words');
        foreach($head as $i=>$h){
            $has = FbeQuery::whereCountry($country)->whereQuery($h)->first()->found;
            $head[$i] = $h.'('.$has.')';
        }
        $table = Table::make()->headings( $head );
        foreach($words as $wi=>$word){
            $data = [];
            $has = FbeQuery::whereCountry($country)->whereQuery($word)->first()->found;
            $data[] = $word.'('.$has.')';
            foreach($cities as $ci=>$city){
                if( ( ($ci+1) * ($wi+1) )*$faktor < $NR ){
                    $keyword = $city.' '.$word;
                    $has = FbeQuery::whereCountry($country)->whereQuery($keyword)->wherePhase(4)->first()->found;
                    if( $has>0 ){
                        $data[] = '<span style="background-color:#0f0">'.$has.'</span>';
                    }else{
                        $data[] = "X";
                    }
                }else{
                    $data[] = ".";
                }
            }
            $table->addRow($data);
        }

        $table->out();
    }

}
