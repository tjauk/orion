<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use League\CLImate\CLImate;

use HiveBee as Bee;

// Example: orion bee:get

class BeeGetJob extends Command
{
    protected function configure()
    {
        $this
            ->setName('bee:get')
            ->setDescription('Get remote url')
            ->addArgument(
                'url',
                InputArgument::OPTIONAL,
                'Enter url?'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $climate = new CLImate;

        $url = $input->getArgument('url');

        $result = Bee::remoteGet($url);
        print_r($result);
        $output->writeln('DONE via '.Bee::$last_name);
    }
}
