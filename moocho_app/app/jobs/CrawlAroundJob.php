<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use League\CLImate\CLImate;

use HiveBee as Bee;

// Example: orion bee:get

class CrawlAroundJob extends Command
{
    protected function configure()
    {
        $this
            ->setName('crawl:around')
            ->setDescription('Crawl around web site')
            ->addArgument(
                'depth',
                InputArgument::REQUIRED,
                'Single site link depth'
            )
            ->addArgument(
                'url',
                InputArgument::REQUIRED,
                'Enter web site url?'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '256M');

        $climate = new CLImate;
        CrawlPage::compress(false);

        $url = $input->getArgument('url');
        $depth = $input->getArgument('depth');

        $site = CrawlSite::register($url);

        // site, depth
        // get all links
        // get site with not crawled links
        // register site with found link (correct)

        // check if has links
        if( $site and $site->pages->count()==0 ){
            // if no, create site url as first link
            CrawlPage::register($url);
            // depth=0
        }

        // check for links not crawled
        $pages = CrawlPage::where('depth','<=',$depth)->next(10)->get();
        foreach($pages as $page){
            $page->crawling=1;
            $page->save();
        }
        while( count($pages)>0 ){
            foreach($pages as $page){
                $output->write('Extracting links from '.$page->url);
                $page->extractAllLinks();
                $output->writeln(' ... ok');
            }
            // loop until no more links
            $pages = CrawlPage::where('depth','<=',$depth)->next(10)->get();
            foreach($pages as $page){
                $page->crawling=1;
                $page->save();
            }

            $total_sites = CrawlSite::count();
            $total_pages = CrawlPage::count();
            $total_crawled = CrawlPage::whereCrawled(1)->count();

            $output->writeln("Sites: $total_sites Crawled: $total_crawled Pages: $total_pages");
            $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
            $output->writeln('Elapsed time: '.round(microtime(true)-ORION_STARTED,2).' seconds');
            $output->writeln("----------------------------------");
        }

        //$result = Bee::remoteGet($site->url);
        //print_r($result);
        $output->writeln('DONE');
    }
}
