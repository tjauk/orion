<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion crawl:fbedict
class CrawlFacebookCitiesJob extends Command
{
    static $fbi = 0;
    static $tokens = null;

    static $wait_between = 5;
    static $wait_on_error = 300;

    static $matrix = [];

    public $output;

    protected function configure()
    {
        $this
            ->setName('crawl:fbecities')
            ->setDescription('Get all facebook cities for countries')
            ->addArgument(
                'country',
                InputArgument::OPTIONAL,
                'Country'
            )
            ->addArgument(
                'from',
                InputArgument::OPTIONAL,
                'From'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '128M');

/*
        $access_token = $access_token = $this->getFacebookAccessToken();
        $result = Remote::get("https://graph.facebook.com/v2.6/search?type=adgeolocation&location_types=city&access_token={$access_token}&country_code=HR&limit=100&q=z");
        print_r($result);
        exit();
*/
        $limit = 100;


        $abc = 'a b c d e f g h i j k l m n o p r s t u v w x y z';
        $chars1 = $chars2 = explode(' ',$abc);
        
        $country = $input->getArgument('country');
        $mycountries = explode(",",$country);
        $from = $input->getArgument('from');
        
        $json = json_decode(File::load(ROOTDIR.'/app/data/fb_countries_data.json'),true);
        $countries = $json['data'];

        if( !empty($country) ){
            foreach($countries as $i=>$c){
                if( !in_array($c['name'], $mycountries) ){
                    unset($countries[$i]);
                }
            }
        }

        $qs = [];

        foreach($countries as $country_data){

            $country_name = $country_data['name'];
            $country_code = $country_data['country_code'];

            if( FbeCity::whereCountry($country_name)->count()<100 ){
                reset($chars1);
                reset($chars2);
                foreach($chars1 as $c1){
                    foreach($chars2 as $c2){
                        $qs[$country_code.":".$c1] = 0;//.$c2
                    }
                }
            }
        }

        /*
        print_r($qs);
        echo count($qs);
        exit();
        */

        foreach($qs as $key=>$done){

            list($code,$query) = explode(':',$key);
            
            if( empty($from) ){
                $run = true;
            }else if( $from==$query ){
                $from = '';
                $run = true;
            }else{
                $run = false;
            }


            if( $run ){
                echo "code=$code q=$query"."\n";

                $access_token = $access_token = $this->getFacebookAccessToken();

                $result = Remote::bee("https://graph.facebook.com/v2.6/search?type=adgeolocation&location_types=city&access_token={$access_token}&country_code={$code}&limit={$limit}&q={$query}");
                

                $json = json_decode($result,true);

                if( isset($json['error']) ){//or empty($json['data'])
                    print_r($json);
                    echo "Error ... waiting ".static::$wait_on_error." sec\n";
                    sleep(static::$wait_on_error);
                }

                $data = $json['data'];
                if( is_array($data) ){
                    foreach($data as $item){
                        $city = FbeCity::import($item);
                        if( $city->is_new ){
                            echo $city->name."\n";
                        }
                    }
                }

                echo "Waiting ".static::$wait_between." sec\n";
                sleep(static::$wait_between);
            }
        }

        

        $output->writeln("----------------------------------");
        $output->writeln("");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');
    }


    public function getFacebookAccessToken($index=null)
    {
        if( !static::$tokens ){
            static::$tokens = config('facebook');
        }
        $tokens = static::$tokens;

        static::$fbi++;
        if( !is_null($index) ){
            static::$fbi = $index;
        }
        if( static::$fbi > count($tokens)-1 ){
            static::$fbi = 0;
        }
        $token = $tokens[static::$fbi];
        //echo $token['name']."\n";

        return $token['fb_page_access_token'];
    }


    public function search($query)
    {
        $access_token = $access_token = $this->getFacebookAccessToken();

        $query = urlencode($query);
        
        $result = Remote::bee("https://graph.facebook.com/v2.6/search?q={$query}&type=event&access_token={$access_token}&fields=name,description,category,cover,place,start_time,end_time,ticket_uri,attending_count,declined_count,timezone,updated_time&limit=100");

        $json = json_decode($result,true);

        // on error, dump and wait
        if( isset($json['error']) ){
            print_r($json);
            $wait = $wait_max_sec;
            $this->output->writeln("");
            $this->output->write('Waiting '.$wait.'s ');
            foreach(range(1,$wait) as $seconds){
                sleep(1);
                $this->output->write('.');
            }
            return array();
        }

        if( is_array($json['data']) ){
            return $json['data'];
        }

        return [];
    }

}