<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion crawl:fbedict
class CrawlFacebookCoversJob extends Command
{
    static $fbi = 0;
    static $tokens = null;

    static $matrix = [];

    protected function configure()
    {
        $this
            ->setName('crawl:fbecovers')
            ->setDescription('Download Facebook Event Covers')
            ->addArgument(
                'country',
                InputArgument::OPTIONAL,
                'Country'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '128M');

        $country = $input->getArgument('country');
        if( !empty($country) ){
            $countries = array_filter(explode(',',$country));
        }else{
            $countries = collect(DB::select( DB::raw("
                    SELECT country, COUNT(*) AS c
                    FROM fbe_events
                    WHERE country<>''
                    GROUP BY country
                    HAVING c>0
                    ORDER BY c DESC
                ") ) )->pluck('country')->toArray();
            print_r($countries);
        }
        

        foreach($countries as $country){
            
            $covers_folder = ROOTDIR.'/images/covers/'.str_slug($country);
            Folder::make($covers_folder);

            if( !empty($country) ){
                $covers = FbeEvent::whereCountry($country)->takeCovers(1)->get();
            }else{
                $covers = FbeEvent::takeCovers(20)->get();
            }

            while( count($covers)>0 ){

                $number_of_retries_on_error = 3;
                foreach($covers as $cover){

                    $parsed = parse_url($cover->cover_source);
                    $path = $parsed['path'];
                    $ext = File::ext($path);
                    $crc = substr($cover->cover_fbid,-4,4);

                    /*
                    print_r($cover->toArray());
                    print_r($parsed);

                    exit($ext);
                    */

                    $filename =str_slug($cover->name.'-'.$crc).'.'.$ext;
                    //$filename = basename(File::unique($covers_folder.'/'.$filename));
                    $filepath = $covers_folder.'/'.$filename;
                    
                    $output->write("$filename ...");

                    if( !file_exists($filepath) ){

                        $data = Remote::get($cover->cover_source);

                        File::save($filepath,$data);

                        $filesize = filesize($filepath);

                        if( $filesize>99 ){
                            $cover->cover = '/images/covers/'.str_slug($country).'/'.$filename;
                            $cover->save();
                            $output->writeln(" OK");
                            $error_retry=0;
                        }else{
                            unlink($filepath);
                            $output->writeln(" ERROR");
                            $error_retry++;
                            if( $error_retry>$number_of_retries_on_error ){
                                $cover->cover = '';
                                $cover->save();
                            }
                        }

                    }else{
                        $cover->cover = '/images/covers/'.str_slug($country).'/'.$filename;
                        $cover->save();
                        $output->writeln(" ALREADY EXISTS");
                        $error_retry=0;
                    }

                }

                $covers = FbeEvent::whereCountry($country)->takeCovers(20)->get();

                $done = FbeEvent::whereCountry($country)->whereNotNull('cover_source')->whereNotNull('cover')->count();
                $total = FbeEvent::whereCountry($country)->whereNotNull('cover_source')->count();

                $output->writeln("Progress: $done / $total");
                $output->writeln("");

                if( $done>1 and $done==$total ){
                    break;
                }

            }

        }

        $output->writeln("----------------------------------");
        $output->writeln("");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');

    }


    public function getFacebookAccessToken($index=null)
    {
        if( !static::$tokens ){
            static::$tokens = config('facebook');
        }
        $tokens = static::$tokens;

        static::$fbi++;
        if( !is_null($index) ){
            static::$fbi = $index;
        }
        if( static::$fbi > count($tokens)-1 ){
            static::$fbi = 0;
        }
        $token = $tokens[static::$fbi];
        echo $token['name']."\n";

        return $token['fb_page_access_token'];
    }

}