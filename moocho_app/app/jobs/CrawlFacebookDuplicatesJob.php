<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion crawl:fbedict
class CrawlFacebookDuplicatesJob extends Command
{

    protected function configure()
    {
        $this
            ->setName('crawl:fbeduplicates')
            ->setDescription('Detect and mark duplicate events')
            ->addArgument(
                'country',
                InputArgument::OPTIONAL,
                'Country'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '128M');

        $country = $input->getArgument('country');
        if( !empty($country) ){
            $countries = [$country];
        }

        $potential = FbeEvent::query(DB::raw("
            SELECT *, COUNT(*) AS c
            FROM fbe_events
            WHERE
                duplicate IS NULL
                AND city='Zadar'
                AND country='Croatia'
            GROUP BY start_utc,place_name,city,country
            HAVING c>1
            ORDER BY start_time
        "))->get();

        foreach($potential as $pot){

            $output->writeln($pot->id." : ".$pot->name);

            $duplicates = FbeEvent::whereNotIn('id',[$pot->id])
                                    ->where(DB::raw("DATETIME(start_time)"),'=',DB::raw("DATETIME('{$pot->start_time}')")) // not working here
                                    ->wherePlaceName($pot->place_name)
                                    ->whereCity($pot->city)
                                    ->whereCountry($pot->country)
                                    ->get();
            /*
            $score = [];
            foreach($duplicates as $dup){

            }
            */
            foreach($duplicates as $dup){
                $output->writeln("D: {$dup->id} Q:{$dup->quality} : ".$dup->name);
                $dup->duplicate = $pot->id;
                //$dup->save();
            }

        }
        

        $output->writeln("----------------------------------");
        $output->writeln("");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');

    }


}