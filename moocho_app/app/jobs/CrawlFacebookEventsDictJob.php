<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion crawl:fbedict
class CrawlFacebookEventsDictJob extends Command
{
    static $fbi = 0;
    static $tokens = null;

    static $matrix = [];

    protected function configure()
    {
        $this
            ->setName('crawl:fbedict')
            ->setDescription('Get all facebook events within country using dictionary attack')
            ->addArgument(
                'country',
                InputArgument::REQUIRED,
                'Country'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '128M');

        $country = $input->getArgument('country');

        $wait_max_sec = 3;

        $matrix_file = CACHEDIR.'/matrix-'.$country.'.ser';
        if( empty(static::$matrix) and file_exists($matrix_file) ){
            static::$matrix = unserialize(File::load($matrix_file));
        }

        if( empty(static::$matrix) ){
            static::$matrix[$country] = 0;

                $access_token = $access_token = $this->getFacebookAccessToken();

                $query = urlencode($country);
                
                $result = Remote::bee("https://graph.facebook.com/v2.6/search?q={$query}&type=event&access_token={$access_token}&fields=name,description,category,cover,place,start_time,end_time,ticket_uri,attending_count,declined_count,timezone,updated_time&limit=100");

                $json = json_decode($result,true);

                if( isset($json['error']) ){
                    print_r($json);
                }
                
                /*
                print_r($json);
                exit();
                */

                $data = $json['data'];

                if( count($data)>0 ){
                    $has_more = true;
                }

                while($has_more and is_array($json)){
                    
                    $has_more = false;

                    foreach($data as $event){
                        $event = FbeEvent::import($event);
                        if( $event->is_new ){
                            $output->writeln("Found new ".$event->name);
                        }else if( $event->is_changed ){
                            $output->writeln("Changed ".$event->name);
                        }
                        $output->write( $event->downloadCover() );
                    }

                    $output->writeln('Memory: '.(memory_get_usage()/1024/1024));
                    
                    if( !empty( $json['paging']['next']) ){
                        $wait = $wait_max_sec;
                        $output->writeln("Getting next page");
                        $output->write('Waiting '.$wait.'s ');
                        foreach(range(1,$wait) as $seconds){
                            sleep(1);
                            $output->write('.');
                        }

                        $result = Remote::get($json['paging']['next']);
                        $json = json_decode($result,true);
                        $data = $json['data'];
                        if( count($data)>0 ){
                            $has_more = true;
                        }
                    }

                    $output->writeln("---");
                }

                if( !is_array($json) ){
                    echo $result;
                    // ERRORS:
                    // Operation timed out after 0 milliseconds with 0 out of 0 bytes received-
                    // Connection timed out after 5000 milliseconds
                }else{
                    //print_r($json);
                    static::$matrix[$key] = 1;
                    File::save($matrix_file,serialize(static::$matrix));
                }
        }

        $take = 40;

        $matrix_done = 0;
        $matrix_total = 1;

        while( $matrix_done < $matrix_total ){

            $words = collect( FbeDict::top($country,$take)->get(['word']) )->pluck('word')->toArray();
            $words[] = ' ';
            $cities = collect(DB::select( DB::raw("
                    SELECT city, COUNT(*) AS c
                    FROM fbe_events
                    WHERE country='$country'
                    GROUP BY city
                    HAVING c>0
                    ORDER BY c DESC
                    LIMIT 80
                ") ) )->pluck('city')->toArray();
            $cities[] = $country;
            $cities[] = ' ';
            $city_count = round(count($cities)/2);

            // create temp matrix
            $matrix = [];
            foreach($words as $word){
                foreach($cities as $city){
                    $key = $word.' '.$city;
                    $matrix[$key] = 0;
                    if( isset(static::$matrix[$key]) ){
                        $matrix[$key] = static::$matrix[$key];
                    }
                }
            }

            // copy temp matrix to static
            foreach($matrix as $key=>$val){
                static::$matrix[$key] = $val;
            }

            if( empty($words) and empty($cities) ){
                $matrix[$country] = 0;            
            }

            foreach($matrix as $key=>$done){

                if( $done ){
                    continue;
                }

                $output->writeln("Query: $key");

                $query = urlencode($key);

                $access_token = $access_token = $this->getFacebookAccessToken();
                $wait_max_sec = 3;


                $result = Remote::bee("https://graph.facebook.com/v2.6/search?q={$query}&type=event&access_token={$access_token}&fields=name,description,category,cover,place,start_time,end_time,ticket_uri,attending_count,declined_count,timezone,updated_time&limit=100");

                $json = json_decode($result,true);

                if( isset($json['error']) ){
                    print_r($json);
                    $wait = 240;
                    $output->writeln("");
                    $output->write('Waiting '.$wait.'s ');
                    foreach(range(1,$wait) as $seconds){
                        sleep(1);
                        $output->write('.');
                    }
                }
                

                $found_on_page = count($json['data']);

                /*
                print_r($json);
                exit();
                */

                $data = $json['data'];

                if( $found_on_page>0 ){
                    $has_more = true;
                }

                while($has_more and is_array($json)){
                    
                    $has_more = false;

                    foreach($data as $event){
                        $event = FbeEvent::import($event);
                        if( $event->is_new ){
                            $output->writeln("Found new ".$event->name);
                        }else if( $event->is_changed ){
                            $output->writeln("Changed ".$event->name);
                        }
                        $output->write( $event->downloadCover() );
                    }

                    $found_on_page = count($json['data']);

                    if( $found_on_page>90 ){
                        if( !empty( $json['paging']['next']) ){
                            $wait = $wait_max_sec;
                            $output->writeln("Getting next page");
                            $output->write('Waiting '.$wait.'s ');
                            foreach(range(1,$wait) as $seconds){
                                sleep(1);
                                $output->write('.');
                            }

                            $result = Remote::bee($json['paging']['next']);
                            $json = json_decode($result,true);
                            $data = $json['data'];
                            if( count($data)>0 ){
                                $has_more = true;
                            }
                        }
                    }

                    $output->writeln("---");
                }

                if( !is_array($json) ){
                    echo $result;
                    // ERRORS:
                    // Operation timed out after 0 milliseconds with 0 out of 0 bytes received-
                    // Connection timed out after 5000 milliseconds
                }else{
                    //print_r($json);
                    static::$matrix[$key] = 1;
                    File::save($matrix_file,serialize(static::$matrix));

                    $wait = $wait_max_sec;
                    $output->writeln("");
                    $output->write('Waiting '.$wait.'s ');
                    foreach(range(1,$wait) as $seconds){
                        sleep(1);
                        $output->write('.');
                    }
                }

                // check how much is done
                $matrix_done = 0;
                $matrix_total = 0;
                $temp = $matrix;
                foreach($temp as $key=>$val){
                    $matrix_total++;
                    if( $val==1 ){
                        $matrix_done++;
                    }
                }
                $output->writeln("--- $matrix_done / $matrix_total ---");

            }

            // refresh
            $words = collect( FbeDict::top($country,$take)->get(['word']) )->pluck('word')->toArray();
            $cities = collect(DB::select( DB::raw("
                    SELECT city, COUNT(*) AS c
                    FROM fbe_events
                    WHERE country='$country'
                    GROUP BY city
                    HAVING c>0
                    ORDER BY c DESC
                    LIMIT $take
                ") ) )->pluck('city')->toArray();

            // create temp matrix
            $matrix = [];
            foreach($words as $word){
                foreach($cities as $city){
                    $key = $word.' '.$city;
                    $matrix[$key] = 0;
                    if( isset(static::$matrix[$key]) ){
                        $matrix[$key] = static::$matrix[$key];
                    }
                }
            }
            

        }

        $output->writeln("----------------------------------");
        $output->writeln("");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');
    }


    public function getFacebookAccessToken($index=null)
    {
        if( !static::$tokens ){
            static::$tokens = config('facebook');
        }
        $tokens = static::$tokens;

        static::$fbi++;
        if( !is_null($index) ){
            static::$fbi = $index;
        }
        if( static::$fbi > count($tokens)-1 ){
            static::$fbi = 0;
        }
        $token = $tokens[static::$fbi];
        echo $token['name']."\n";

        return $token['fb_page_access_token'];
    }

}