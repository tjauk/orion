<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion crawl:facebookevents
class CrawlFacebookEventsJob extends Command
{
    static $fbi = 0;
    static $tokens = null;

    protected function configure()
    {
        $this
            ->setName('crawl:facebookevents')
            ->setDescription('Get all facebook events within country / city')
            ->addArgument(
                'country',
                InputArgument::REQUIRED,
                'Country'
            )
            ->addArgument(
                'address',
                InputArgument::OPTIONAL,
                'Starting address'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '128M');

        $country = $input->getArgument('country');
        $addresses = explode(',',$input->getArgument('address'));

        foreach($addresses as $address){

            $address = urlencode($address);

            $access_token = $access_token = $this->getFacebookAccessToken();
            $wait_max_sec = 3;

            //$result = Remote::get("https://graph.facebook.com/v2.6/search?q={$address},{$country}&type=event&access_token={$access_token}");
            
            //search?q=Zagreb&type=event&fields=name,cover,place,category
            
            $result = Remote::get("https://graph.facebook.com/v2.6/search?q={$address}&type=event&access_token={$access_token}&fields=name,description,category,cover,place,start_time,end_time,ticket_uri,attending_count,declined_count,timezone,updated_time&limit=100");

            $json = json_decode($result,true);

            if( isset($json['error']) ){
                print_r($json);
            }
            
            /*
            print_r($json);
            exit($address);
            */

            $data = $json['data'];

            if( count($data)>0 ){
                $has_more = true;
            }

            while($has_more and is_array($data)){
                
                $has_more = false;

                foreach($data as $event){
                    $event = FbeEvent::import($event);
                    if( $event->is_new ){
                        $output->writeln("Found new ".$event->name);
                    }else if( $event->is_changed ){
                        $output->writeln("Changed ".$event->name);
                    }
                    $output->write( $event->downloadCover() );
                }

                $output->writeln('Memory: '.(memory_get_usage()/1024/1024));
                
                if( !empty( $json['paging']['next']) ){
                    $wait = $wait_max_sec;
                    $output->writeln("Getting next page");
                    $output->write('Waiting '.$wait.'s ');
                    foreach(range(1,$wait) as $seconds){
                        sleep(1);
                        $output->write('.');
                    }

                    $result = Remote::get($json['paging']['next']);
                    $json = json_decode($result,true);
                    $data = $json['data'];
                    if( count($data)>0 ){
                        $has_more = true;
                    }
                }

                $output->writeln("---");
            }

            if( !is_array($json) ){
                echo $result;
                // ERRORS:
                // Operation timed out after 0 milliseconds with 0 out of 0 bytes received-
                // Connection timed out after 5000 milliseconds
            }else{
                //print_r($json);
            }

    /*
            // Use each place as starting point and get all surrounding places
            $around = CrawlFacebookEvent::whereCountry($country)->last(1)->get();
            $lastAround = $around;
            while( count($around)>0 ){
                foreach($around as $place){
                    $lat = $place->lat;
                    $lon = $place->lon;
                    $result = Remote::get("https://graph.facebook.com/search?q={$keyword}&type=event&center={$lat},{$lon}&distance={$distance}&access_token={$access_token}");
                    $json = json_decode($result,true);
                    $data = $json['data'];

                    if( count($data)>0 ){
                        $has_more = true;
                        $place->crawled = 1;
                        $place->save();
                    }

                    while($has_more and is_array($json)){
                        $pc = count($data);
                        $nr = 0;
                        foreach($data as $place){
                            $place = CrawlFacebookEvent::import($place);

                            // mark first half or results as crawled
                            if( $pc>4 and ceil($pc/3*2)>$nr ){
                                $place->crawled = 1;
                                $place->save();
                            }
                            $nr++;

                            if( $place->is_new ){
                                $output->writeln("Found new ".$place->name);
                            }else if( $place->is_changed ){
                                $output->writeln("Changed ".$place->name);
                            }

                        }

                        
                        
                        if( !empty( $json['paging']['next']) ){
                            $wait = $wait_max_sec;
                            $output->write('Waiting '.$wait.'s ');
                            foreach(range(1,$wait) as $seconds){
                                sleep(1);
                                $output->write('.');
                            }

                            $result = Remote::get($json['paging']['next']);
                            $json = json_decode($result,true);
                            $data = $json['data'];
                            if( count($data)>0 ){
                                $has_more = true;
                            }

                        }else{
                            $has_more = false;
                        }
                        $output->writeln("");

                    }
                }
                $around = CrawlFacebookEvent::whereCountry($country)->last(1)->get();
                
                $crawled = CrawlFacebookEvent::whereCountry($country)->whereCrawled(1)->count();
                $total = CrawlFacebookEvent::whereCountry($country)->count();

                $output->writeln("Crawled: $crawled / Total in $country: $total");
                //$output->writeln('Memory: '.(memory_get_usage()/1024/1024));
                if( $crawled == $lastCrawled ){
                    $wait = 30;
                    $output->write('Waiting '.$wait.'s ');
                    foreach(range(1,$wait) as $seconds){
                        sleep(1);
                        $output->write('.');
                    }
                }
                $lastCrawled = $crawled;
            }
    */

            $wait = 5;
            $output->write('Waiting '.$wait.'s ');
            foreach(range(1,$wait) as $seconds){
                sleep(1);
                $output->write('.');
            }
            $output->writeln("");

            $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
            $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');

        }

    }


    public function getFacebookAccessToken($index=null)
    {
        if( !static::$tokens ){
            static::$tokens = config('facebook');
        }
        $tokens = static::$tokens;

        static::$fbi++;
        if( !is_null($index) ){
            static::$fbi = $index;
        }
        if( static::$fbi > count($tokens)-1 ){
            static::$fbi = 0;
        }
        $token = $tokens[static::$fbi];
        echo $token['name']."\n";

        return $token['fb_page_access_token'];

        /*
        $access_token = getenv('FB_PAGE_ACCESS_TOKEN');
        if( empty($access_token) ){
            $access_token = config('app.fb_page_access_token');
        }
        return $access_token;
        */
    }

}