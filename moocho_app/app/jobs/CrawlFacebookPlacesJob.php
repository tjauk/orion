<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion crawl:facebookplaces
class CrawlFacebookPlacesJob extends Command
{
    static $fbi = 0;
    static $tokens = null;

    protected function configure()
    {
        $this
            ->setName('crawl:facebookplaces')
            ->setDescription('Get all facebook places within country')
            ->addArgument(
                'country',
                InputArgument::REQUIRED,
                'Country'
            )
            ->addArgument(
                'address',
                InputArgument::OPTIONAL,
                'Starting address'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '64M');

        $country = $input->getArgument('country');
        $address = $input->getArgument('address');

        if( empty($address) ){
            $address = $country;
        }else{
            //$address = $address.' , '.$country;
        }

        $keyword = ''; // leave empty for all
        $distance = 50000; // metters
        
        $show_after = 5;
        $wait_max_sec = 4;
        $min_cycle_time_sec = 5;

        $point = Geo::code($address);

        $lat = $point['lat'];
        $lon = $point['lon'];

        $access_token = $access_token = $this->getFacebookAccessToken(0);

        $diff_sec = strtotime('now');

        $result = Remote::get("https://graph.facebook.com/v2.6/search?q={$keyword}&type=place&center={$lat},{$lon}&distance={$distance}&access_token={$access_token}&limit=100");


        $json = json_decode($result,true);
        $data = $json['data'];

        /*
        print_r($json);
        exit();
        */


        if( count($data)>0 ){
            $has_more = true;
        }

        while($has_more and is_array($json)){
            
            $has_more = false;
            
            foreach($data as $place){
                $place = CrawlFacebookPlace::import($place);
                /*
                print_r($place->toJson());
                exit();
                */
                if( $place->is_new ){
                    $output->writeln("Found new ".$place->name);
                }else if( $place->is_changed ){
                    //$output->writeln("Changed ".$place->name);
                }
            }

            $output->writeln('Memory: '.(memory_get_usage()/1024/1024));
            
            /*
            if( !empty( $json['paging']['next']) ){
                $wait = $wait_max_sec;
                $output->write('Waiting '.$wait.'s ');
                foreach(range(1,$wait) as $seconds){
                    sleep(1);
                    $output->write('.');
                }

                $result = Remote::get($json['paging']['next']);
                $json = json_decode($result,true);
                $data = $json['data'];
                if( count($data)>0 ){
                    //$has_more = true;
                    $output->writeln('has more');
                }

            }else{
                $has_more = false;
            }
            $output->writeln("");
            */
        }

        if( !is_array($json) ){
            echo $result;
            // ERRORS:
            // Operation timed out after 0 milliseconds with 0 out of 0 bytes received-
            // Connection timed out after 5000 milliseconds
        }else{
            //print_r($json);
        }

        $output->writeln("Check around");


        // Use each place as starting point and get all surrounding places
        $around = CrawlFacebookPlace::whereCountry($country)->random(1)->get();
        //$around = CrawlFacebookPlace::whereCountry($country)->next(1)->get();
        
        $lastAround = $around;
        while( count($around)>0 ){
            foreach($around as $place){
                $has_more = false;
                $lat = $place->lat;
                $lon = $place->lon;
                $access_token = $this->getFacebookAccessToken();

                $result = Remote::get("https://graph.facebook.com/v2.6/search?q={$keyword}&type=place&center={$lat},{$lon}&distance={$distance}&access_token={$access_token}&limit=100");
                $json = json_decode($result,true);
                $data = $json['data'];
                
                DB::beginTransaction();

                if( count($data)>0 ){
                    $has_more = true;
                    //$place->crawled = $place->crawled+1;
                    //$place->save();
                    $place->crawled = $place->crawled + 2;
                    $place->save();
                }else{
                    print_r($json);
                    if( isset($json['error']) ){
                        $on_error_wait = 300;
                        $wait = $on_error_wait;
                        $output->write('Waiting '.$wait.'s ');
                        foreach(range(1,$wait) as $seconds){
                            sleep(1);
                            $output->write('.');
                        }
                    }
                }

                while($has_more and is_array($json)){
                    $has_more = false;
                    $pc = count($data);
                    $nr = 0;
                    foreach($data as $place){
                        $place = CrawlFacebookPlace::import($place);

                        $nr++;

                        if( $place->is_new ){
                            $output->writeln("Found new ".$place->name);
                        }else if( $place->is_changed ){
                            //$output->writeln("Changed ".$place->name);
                        }

                    }

                    
                    if( !empty( $json['paging']['next']) ){
                        $wait = $wait_max_sec;
                        $output->write('Waiting '.$wait.'s ');
                        foreach(range(1,$wait) as $seconds){
                            sleep(1);
                            $output->write('.');
                        }
                        $output->writeln('');

                        $result = Remote::get($json['paging']['next']);
                        $json = json_decode($result,true);
                        $data = $json['data'];
                        if( count($data)>0 ){
                            $has_more = true;
                        }

                    }else{
                        $has_more = false;
                    }
                    /*
                    */
                    $output->writeln("");

                }

                DB::commit();
            
            }


            $around = CrawlFacebookPlace::whereCountry($country)->random(1)->get();
            
            $crawled = CrawlFacebookPlace::whereCountry($country)->where('crawled','>',2)->count();
            
            $show_after++;
            if( $show_after>5 ){
                $show_after=0;
                $total = CrawlFacebookPlace::whereCountry($country)->count();
                $gap = $total-$crawled;
                $output->writeln(date('Y-m-d H:i:s')." Crawled: $crawled / Gap: $gap / Total in $country: $total");
            }else{
                $gap = $total-$crawled;
                $output->writeln(date('Y-m-d H:i:s')." Crawled: $crawled / Gap: $gap");
            }

            $now = strtotime('now');
            $output->writeln("Cycle time: ".($now-$diff_sec)."s (et:".round(($total-$crawled)*($now-$diff_sec)/60/60)."h)");
            $diff_sec = strtotime('now');

            $wait=0;
            if( $diff_sec<$min_cycle_time_sec ){
                $wait = $min_cycle_time_sec-$diff_sec;
            }

            if( $crawled == $lastCrawled ){
                print_r($json);
                if( isset($json['errors']) ){
                    $on_error_wait = 300;
                    $wait = $on_error_wait;
                }else{
                    $wait = 5;
                }
            }
            
            if( $wait>0 ){
                $output->write('Waiting '.$wait.'s ');
                foreach(range(1,$wait) as $seconds){
                    sleep(1);
                    $output->write('.');
                }
            }

            $lastCrawled = $crawled;
        }

        $crawled = CrawlFacebookPlace::whereCountry($country)->where('crawled','>',2)->count();
        $total = CrawlFacebookPlace::whereCountry($country)->count();
        $output->writeln("Crawled: $crawled / Total in $country: $total");
        $output->writeln("----------------------------------");
        $output->writeln("");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');

    }


    public function getFacebookAccessToken($index=null)
    {
        if( !static::$tokens ){
            static::$tokens = config('facebook');
        }
        $tokens = static::$tokens;

        static::$fbi++;
        if( !is_null($index) ){
            static::$fbi = $index;
        }
        if( static::$fbi > count($tokens)-1 ){
            static::$fbi = 0;
        }
        $token = $tokens[static::$fbi];
        echo $token['name']."\n";

        return $token['fb_page_access_token'];

        /*
        $access_token = getenv('FB_PAGE_ACCESS_TOKEN');
        if( empty($access_token) ){
            $access_token = config('app.fb_page_access_token');
        }
        return $access_token;
        */
    }
}