<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion crawl:facebookplaces2
class CrawlFacebookPlacesOptJob extends Command
{
    static $fbi = 0;
    static $tokens = null;

    protected function configure()
    {
        $this
            ->setName('crawl:facebookplacesopt')
            ->setDescription('Crawl FB Places via optimized tables')
            ->addArgument(
                'country',
                InputArgument::REQUIRED,
                'Country'
            )
            ->addArgument(
                'address',
                InputArgument::OPTIONAL,
                'Starting address'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '128M');

        $country = $input->getArgument('country');
        $address = $input->getArgument('address');

        if( empty($address) ){
            $address = $country;
        }else{
            //$address = $address.' , '.$country;
        }

        $batch = 20;

        $keyword = ''; // leave empty for all
        $distance = 50000; // metters
        
        $show_after = 5;
        $wait_max_sec = 1;
        $min_cycle_time_sec = 3;

        $point = Geo::code($address);

        $lat = $point['lat'];
        $lon = $point['lon'];

        $access_token = $access_token = $this->getFacebookAccessToken();

        $diff_sec = strtotime('now');

        $result = Remote::bee("https://graph.facebook.com/v2.6/search?q={$keyword}&type=place&center={$lat},{$lon}&distance={$distance}&access_token={$access_token}&limit=100");
        $json = json_decode($result,true);
        $data = $json['data'];

        /*
        print_r($result);
        exit();
        */
        


        if( count($data)>0 ){
            $has_more = true;
        }

        while($has_more and is_array($json)){
            
            $has_more = false;
            
            foreach($data as $place){
                $place = CrawlFacebookPlace::import($place);
                if( $place->is_new ){
                    $output->writeln("Found new ".$place->name);
                }else if( $place->is_changed ){
                    //$output->writeln("Changed ".$place->name);
                }
            }

            $output->writeln('Memory: '.(memory_get_usage()/1024/1024));

        }

        if( !is_array($json) ){
            echo $result;
            // ERRORS:
            // Operation timed out after 0 milliseconds with 0 out of 0 bytes received-
            // Connection timed out after 5000 milliseconds
        }else{
            //print_r($json);
        }

        $output->writeln("Check around");


        // Use each place as starting point and get all surrounding places
        $around = CrawlFacebookPlace::whereCountry($country)->random($batch)->get();
        //$around = CrawlFacebookPlace::whereCountry($country)->next(1)->get();
        
        $lastAround = $around;
        while( count($around)>0 ){
            foreach($around as $place){
                $has_more = false;
                $lat = $place->lat;
                $lon = $place->lon;
                $access_token = $this->getFacebookAccessToken();

                $result = Remote::bee("https://graph.facebook.com/v2.6/search?q={$keyword}&type=place&center={$lat},{$lon}&distance={$distance}&access_token={$access_token}&limit=100");
                $json = json_decode($result,true);
                $data = $json['data'];
                
                DB::beginTransaction();

                if( count($data)>0 ){
                    $has_more = true;
                    //$place->crawled = $place->crawled+1;
                    //$place->save();
                    $place->crawled = $place->crawled + 2;
                    $place->save();
                }else{
                    print_r($json);
                    if( isset($json['error']) ){
                        $on_error_wait = 300;
                        $wait = $on_error_wait;
                        $output->write('Waiting '.$wait.'s ');
                        foreach(range(1,$wait) as $seconds){
                            sleep(1);
                            $output->write('.');
                        }
                    }
                }

                while($has_more and is_array($json)){
                    $has_more = false;
                    $pc = count($data);
                    $nr = 0;
                    foreach($data as $place){
                        $place = CrawlFacebookPlaceOpt::import($place);

                        $nr++;

                        if( $place->is_new ){
                            //$output->write('+');
                            //$output->writeln("Found new ".$place->name);
                        }else if( $place->is_changed ){
                            //$output->writeln("Changed ".$place->name);
                        }

                    }

                    //$output->writeln("");

                }

                DB::commit();
            
            }

            // move from opt to master
            $output->writeln(" ");
            $output->writeln(date('Y-m-d H:i:s'));
            $output->writeln("Moving data from opt to master table");
            
            // increment crawled on all
            $output->writeln("Incrementing crawled on master table");
            // update existing ?
            $fbids = collect(DB::select( DB::raw("
SELECT crawl_facebook_places_opt.fbid
FROM crawl_facebook_places 
JOIN crawl_facebook_places_opt ON crawl_facebook_places.fbid=crawl_facebook_places_opt.fbid
WHERE crawl_facebook_places_opt.country='$country'
") ) )->pluck('fbid')->toArray();
            CrawlFacebookPlace::whereIn('fbid',$fbids)->increment('crawled');


            // insert new and set crawled to 1
            $fbids = collect(DB::select( DB::raw("
SELECT crawl_facebook_places_opt.fbid 
FROM crawl_facebook_places_opt
WHERE
    crawl_facebook_places_opt.country='$country'
    AND
    crawl_facebook_places_opt.fbid NOT IN (SELECT crawl_facebook_places.fbid FROM crawl_facebook_places)     
") ) )->pluck('fbid')->toArray();

            $output->writeln("Copying ".count($fbids)." new places to master table");

            $optplaces = CrawlFacebookPlaceOpt::whereIn('fbid',$fbids)->get();
            foreach($optplaces as $optplace){
                CrawlFacebookPlace::import($optplace);
            }

            // empty the table
            DB::table('crawl_facebook_places_opt')->truncate();

            // get existing fbids
            // SELECT COUNT(crawl_facebook_places_opt.fbid) FROM crawl_facebook_places JOIN crawl_facebook_places_opt ON crawl_facebook_places.fbid=crawl_facebook_places_opt.fbid WHERE crawl_facebook_places_opt.country='Netherlands'


            $around = CrawlFacebookPlace::whereCountry($country)->random($batch)->get();
            
            $crawled = CrawlFacebookPlace::whereCountry($country)->where('crawled','>',2)->count();
            
            $total = CrawlFacebookPlace::whereCountry($country)->count();
            $gap = $total-$crawled;
            $output->writeln(date('Y-m-d H:i:s')." Crawled: $crawled / Gap: $gap / Total in $country: $total");


            $now = strtotime('now');
            $output->writeln("Cycle time: ".($now-$diff_sec)."s (et:".round($gap*($now-$diff_sec)/60/60/$batch)."h)");
            $diff_sec = strtotime('now');

            $wait=0;
            if( $diff_sec<$min_cycle_time_sec ){
                $wait = $min_cycle_time_sec-$diff_sec;
            }

            if( $crawled == $lastCrawled ){
                print_r($json);
                if( isset($json['errors']) ){
                    $on_error_wait = 300;
                    $wait = $on_error_wait;
                }else{
                    $wait = 5;
                }
            }
            
            if( $wait>0 ){
                $output->write('Waiting '.$wait.'s ');
                foreach(range(1,$wait) as $seconds){
                    sleep(1);
                    $output->write('.');
                }
            }

            $lastCrawled = $crawled;

            $output->writeln(" ");
        }

        $crawled = CrawlFacebookPlace::whereCountry($country)->where('crawled','>',2)->count();
        $total = CrawlFacebookPlace::whereCountry($country)->count();
        $output->writeln("Crawled: $crawled / Total in $country: $total");
        $output->writeln("----------------------------------");
        $output->writeln("");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');

    }


    public function getFacebookAccessToken($index=null)
    {
        if( !static::$tokens ){
            static::$tokens = config('facebook');
        }
        $tokens = static::$tokens;

        static::$fbi++;
        if( !is_null($index) ){
            static::$fbi = $index;
        }
        if( static::$fbi > count($tokens)-1 ){
            static::$fbi = 0;
        }
        $token = $tokens[static::$fbi];
        echo "T".static::$fbi." ";

        return $token['fb_page_access_token'];

        /*
        $access_token = getenv('FB_PAGE_ACCESS_TOKEN');
        if( empty($access_token) ){
            $access_token = config('app.fb_page_access_token');
        }
        return $access_token;
        */
    }
}