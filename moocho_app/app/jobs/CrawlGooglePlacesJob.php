<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion crawl:googleplaces
class CrawlGooglePlacesJob extends Command
{
    protected function configure()
    {
        $this
            ->setName('crawl:googleplaces')
            ->setDescription('Get all google places within country')
            ->addArgument(
                'country',
                InputArgument::REQUIRED,
                'Country'
            )
            ->addArgument(
                'address',
                InputArgument::REQUIRED,
                'Starting address'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '64M');

        $country = $input->getArgument('country');
        $address = $input->getArgument('address');
        

        //$address = 'Mandlova 1, Zagreb, Croatia'; // starting address
        //$keyword = 'baotić';// bar,restoran,trgovina,shop,bankomat
        $keyword = ''; // leave empty for all
        $radius = 50000; // metters

        $point = Geo::code($address);

        $lat = $point['lat'];
        $lon = $point['lon'];
        $GOOGLE_API_KEY = config('app.google_api_key');
        //https://maps.googleapis.com/maps/api/place/textsearch/json?query=$query&sensor=true&key=".API_KEY."&$nextStr";

        //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={$lat},{$lon}&radius={$radius}&type={$type}&key=YOUR_API_KEY

        //https://maps.googleapis.com/maps/api/place/nearbysearch/output?parameters
        //location=lat,lon
        //radius=50000
        //rankby=distance [must be used with type parametar]
        //type=[only one type may be specified]

        $types = [
    'accounting',
    'airport',
    'amusement_park',
    'aquarium',
    'art_gallery',
    'atm',
    'bakery',
    'bank',
    'bar',
    'beauty_salon',
    'bicycle_store',
    'book_store',
    'bowling_alley',
    'bus_station',
    'cafe',
    'campground',
    'car_dealer',
    'car_rental',
    'car_repair',
    'car_wash',
    'casino',
    'cemetery',
    'church',
    'city_hall',
    'clothing_store',
    'convenience_store',
    'courthouse',
    'dentist',
    'department_store',
    'doctor',
    'electrician',
    'electronics_store',
    'embassy',
    'finance',
    'fire_station',
    'florist',
    'food',
    'funeral_home',
    'furniture_store',
    'gas_station',
    'grocery_or_supermarket',
    'gym',
    'hair_care',
    'hardware_store',
    'health',
    'hindu_temple',
    'home_goods_store',
    'hospital',
    'insurance_agency',
    'jewelry_store',
    'laundry',
    'lawyer',
    'library',
    'liquor_store',
    'local_government_office',
    'locksmith',
    'lodging',
    'meal_delivery',
    'meal_takeaway',
    'mosque',
    'movie_rental',
    'movie_theater',
    'moving_company',
    'museum',
    'night_club',
    'painter',
    'park',
    'parking',
    'pet_store',
    'pharmacy',
    'physiotherapist',
    'plumber',
    'police',
    'post_office',
    'real_estate_agency',
    'restaurant',
    'roofing_contractor',
    'rv_park',
    'school',
    'shoe_store',
    'shopping_mall',
    'spa',
    'stadium',
    'storage',
    'store',
    'subway_station',
    'synagogue',
    'taxi_stand',
    'train_station',
    'transit_station',
    'travel_agency',
    'university',
    'veterinary_care',
    'zoo',
];
        echo count($types);
        //exit();
        $type = 'bank';

        $result = Remote::get("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={$lat},{$lon}&radius={$radius}&type={$type}&key={$GOOGLE_API_KEY}");
        $json = json_decode($result,true);

        $data = $json['results'];

        if( count($data)>0 ){
            $has_more = true;
        }

        while($has_more and is_array($json)){

            foreach($data as $place){
                $place = CrawlGooglePlace::import($place);
                if( $place->is_new ){
                    $output->writeln("Found new ".$place->name);
                }else if( $place->is_changed ){
                    $output->writeln("Changed ".$place->name);
                }
            }

            $output->writeln('Memory: '.(memory_get_usage()/1024/1024));
            
            
            $output->writeln("");

        }

        if( !is_array($json) ){
            echo $json;
            // ERRORS:
            // Operation timed out after 0 milliseconds with 0 out of 0 bytes received-
            // Connection timed out after 5000 milliseconds
        }else{
            //print_r($json);
        }

        // Use each place as starting point and get nearby places
        $around = CrawlGooglePlace::whereCountry($country)->last(1)->get();
        while( count($around)>0 ){
            foreach($around as $place){
                $lat = $place->lat;
                $lon = $place->lon;
                $result = Remote::get("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={$lat},{$lon}&radius={$radius}&type={$type}&key={$GOOGLE_API_KEY}");
                $json = json_decode($result,true);
                $data = $json['results'];

                if( count($data)>0 ){
                    $has_more = true;
                    $place->crawled = $place->crawled +1;
                    $place->save();
                }

                while($has_more and is_array($json)){
                    $pc = count($data);

                    $nr = 0;
                    foreach($data as $place){
                        $place = CrawlGooglePlace::import($place);

                        $nr++;

                        if( $place->is_new ){
                            $output->writeln("Found new ".$place->name);
                        }else if( $place->is_changed ){
                            $output->writeln("Changed ".$place->name);
                        }

                    }

                    $output->writeln("");

                }
            }
            $around = CrawlGooglePlace::whereCountry($country)->last(1)->get();

            
            $crawled = CrawlGooglePlace::whereCountry($country)->whereCrawled(1)->count();
            $total = CrawlGooglePlace::whereCountry($country)->count();

            $output->writeln("Crawled: $crawled / Total in $country: $total");
            $output->writeln('Memory: '.(memory_get_usage()/1024/1024));
        }


        $output->writeln("----------------------------------");
        $output->writeln("");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');

    }
}