<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use League\CLImate\CLImate;

use HiveBee as Bee;

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;


// Example: orion bee:get

class CrawlPropsJob extends Command
{
    protected function configure()
    {
        $this
            ->setName('crawl:props')
            ->setDescription('Crawl unclaimed properties')
            ->addArgument(
                'from',
                InputArgument::REQUIRED,
                'From'
            )
            ->addArgument(
                'to',
                InputArgument::REQUIRED,
                'To'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '256M');

        // test

        $base = "https://ucpi.sco.ca.gov/ucp/PropertyDetails.aspx?propertyRecID=1728567";

        // First, include Requests
//include('../vendor/library/Requests.php');
// Next, make sure Requests can load internal classes
Requests::register_autoloader();
// Set up our session
$session = new Requests_Session('https://ucpi.sco.ca.gov/ucp/PropertyDetails.aspx?propertyRecID=172856');
$session->headers['Accept'] = 'application/json';
$session->useragent = 'Awesomesauce';
// Now let's make a request!
$request = $session->get('/get');
// Check what we received
print_r($request);
// Let's check our user agent!
$request = $session->get('/user-agent');
// And check again
print_r($request);


        exit();

        $climate = new CLImate;

        $base = "https://ucpi.sco.ca.gov/ucp/PropertyDetails.aspx?propertyRecID=";
        $from = $input->getArgument('from');
        $to = $input->getArgument('to');

        foreach(range($from,$to) as $id){

            $url = $base.$id;

            // Initialize session and set URL.
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);

            // Set so curl_exec returns the result instead of outputting it.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CAINFO, ROOTDIR."/cacert.pem");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            
            curl_setopt($ch, CURLOPT_ENCODING, "");
            curl_setopt($ch, CURLOPT_AUTOREFERER, true);
            
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36');

            // Get the response and close the channel.
            $response = curl_exec($ch);
            curl_close($ch);

            $crawler = new Crawler($response);

            if( strpos($response,'tableNoResult')!==false ){

                $output->writeln("$id no match");
                PropertyClaim::insert(['NR'=>$id]);
                continue;

            }else{

                $crawler->filter('#tbl_HeaderInformation')->each(function ($node) {
                    print $node->text()."\n";
                });

                $data = [];
                $data['NR'] = $id;
                $data['Date'] = $crawler->filter('#tbl_HeaderInformation tr td span')->first()->text();
                $data['Source'] = $crawler->filter('#tbl_HeaderInformation tr td')->eq(1)->filter('span')->first()->text();
                $data['Property_ID'] = $crawler->filter('#tbl_HeaderInformation tr td')->eq(2)->filter('span')->first()->text();

                $data['Owners_Name'] = $crawler->filter('#OwnersNameData')->first()->text();
                $data['Reported_Address'] = $crawler->filter('#ReportedAddressData')->first()->text();
                $data['Property_Type'] = $crawler->filter('#PropertyTypeData')->first()->text();
                $data['Cash_Report'] = $crawler->filter('#ctl00_ContentPlaceHolder1_CashReportData')->first()->text();
                $data['Reported_By'] = $crawler->filter('#ReportedByData')->first()->text();

                $data = array_map(function($val){
                    return trim($val);
                },$data);                

                PropertyClaim::insert($data);
            
                $output->writeln("$id found and saved ");
                print_r($data);

            }
        }

        //print_r($crawler);



        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('Elapsed time: '.round(microtime(true)-ORION_STARTED,2).' seconds');
        $output->writeln("----------------------------------");

        $output->writeln('DONE');
    }
}
