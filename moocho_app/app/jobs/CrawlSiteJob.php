<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use League\CLImate\CLImate;

use HiveBee as Bee;

// Example: orion bee:get

class CrawlSiteJob extends Command
{
    protected function configure()
    {
        $this
            ->setName('crawl:site')
            ->setDescription('Crawl single web site')
            ->addArgument(
                'compress',
                InputArgument::REQUIRED,
                'Compress downloaded data (0/1)?'
            )
            ->addArgument(
                'url',
                InputArgument::REQUIRED,
                'Enter web site url?'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $climate = new CLImate;

        $compress = $input->getArgument('compress');
        CrawlPage::compress($compress);
        $url = $input->getArgument('url');

        $site = CrawlSite::register($url);

        // check if has links
        if( $site->pages->count()==0 ){
            // if no, create site url as first link
            CrawlPage::register($url);
        }

        // check for links not crawled
        $pages = CrawlPage::whereCrawlSiteId($site->id)->next(10)->get();
        foreach($pages as $page){
            $page->crawling=1;
            $page->save();
        }
        while( count($pages)>0 ){
            foreach($pages as $page){
                $output->write('Extracting links from '.$page->url);
                if( $page->matchSiteRules($page->url) ){
                    $page->extractLinks();
                    $output->writeln(' ... ok');
                }else{
                    $output->writeln(' ... ignored');
                }
            }
            // loop until no more links
            $pages = CrawlPage::whereCrawlSiteId($site->id)->next(10)->get();
            foreach($pages as $page){
                $page->crawling=1;
                $page->save();
            }
            $crawled = $site->getTotalCrawled();
            $total = $site->getTotalPages();
            $proc = round($crawled/$total*100,1);
            $output->writeln("Crawled $proc% ($crawled/$total) " );
        }

        //$result = Bee::remoteGet($site->url);
        //print_r($result);
        $output->writeln('DONE');
    }
}
