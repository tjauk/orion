<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use League\CLImate\CLImate;

// Example: orion test:redis

class TestRedisJob extends Command
{
    protected function configure()
    {
        $this
            ->setName('test:redis')
            ->setDescription('Test Redis')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $climate = new CLImate;

        $single_server = array(
            'host' => '127.0.0.1',
            'port' => 6379,
            'database' => 0,
        );


        Redis::put('library', 'predis');
        Redis::put('arr',[1,2,3,4,5]);

        print_r(Redis::get('arr'));

        print_r(Redis::get('library'));

        if( Redis::has('mykey') ){
            echo "Yes";
        }else{
            echo "No";
        }
        Redis::incr('mykey');

        echo Redis::get('mykey');


        // implement
        /*
        Redis::make()
        Redis::get($key,$default)
        Redis::put($key,$val)
        Redis::incr($key)
        Redis::decr($key)
        Redis::hset($key)
        Redis::expire($key)
        Redis::ttl($key)
        Redis::persist($key)
        */
        
        $output->writeln('DONE');
    }
}
