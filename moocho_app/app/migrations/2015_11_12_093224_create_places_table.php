<?php

class CreatePlacesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('status')->default('');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('name')->default('');
            $table->string('slug')->default('');
            $table->decimal('lat',10,8)->nullable();
            $table->decimal('lon',11,8)->nullable();
            $table->integer('radius')->default(1);
            $table->string('address')->default('');
            $table->string('city')->default('');
            $table->string('region')->default('');
            $table->integer('country_id')->unsigned()->nullable();
            $table->text('raw')->default('');
            $table->string('src')->default('');
            $table->string('src_uid')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('places');
    }

}
