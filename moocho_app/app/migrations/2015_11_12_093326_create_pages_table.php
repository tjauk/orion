<?php

class CreatePagesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('status')->default('');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('title')->default('');
            $table->string('slug')->default('');
            $table->text('summary')->default('');
            $table->text('content')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }

}
