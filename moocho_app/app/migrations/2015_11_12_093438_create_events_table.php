<?php

class CreateEventsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('status')->default('');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('title')->default('');
            $table->string('slug')->default('');
            $table->integer('place_id')->nullable();
            $table->dateTime('starts')->nullable();
            $table->dateTime('ends')->nullable();
            $table->tinyInteger('repeatable')->default(0);
            $table->string('repeat_mode')->default(''); // daily, weekly, monthly, yearly, date, special, custom
            $table->string('repeat_func')->default('');
            $table->string('src')->default('');
            $table->string('src_uid')->default('');
            $table->text('summary')->default('');
            $table->text('content')->default('');
            $table->text('photos')->default('');
            $table->text('cover')->default('');
            $table->text('flyer')->default('');
            $table->tinyInteger('ticketed')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }

}
