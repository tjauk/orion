<?php

class CreateBeaconsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beacons', function($table)
        {
            $table->increments('id');
            $table->timestamps();

            $table->integer('client_id')->unsigned()->nullable();
            
            $table->string('code')->default('');
            $table->integer('major');
            $table->integer('minor');

            $table->string('status')->default('');
            $table->text('note')->default('');

            $table->string('immediate')->default('');
            $table->string('near')->default('');
            $table->string('far')->default('');

            $table->decimal('lat',10,8)->nullable();
            $table->decimal('lon',11,8)->nullable();

            $table->string('address')->default('');
            $table->string('city')->default('');
            $table->string('region')->default('');
            $table->integer('country_id')->unsigned()->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('beacons');
    }

}
