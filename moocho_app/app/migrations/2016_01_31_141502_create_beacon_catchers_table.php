<?php

class CreateBeaconCatchersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beacon_catchers', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('uid')->nullable();
            $table->integer('minor')->nullable();
            $table->integer('major')->nullable();
            $table->float('distance')->nullable();
            $table->dateTime('time')->nullable();
            $table->string('action')->nullable();
            $table->string('mac')->nullable();
            $table->decimal('lat',10,8)->nullable();
            $table->decimal('lon',11,8)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('beacon_catchers');
    }

}
