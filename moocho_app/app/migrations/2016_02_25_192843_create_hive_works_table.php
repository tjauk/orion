<?php

class CreateHiveWorksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hive_works', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->default('');
            $table->text('description')->default('');
            $table->string('work')->default('');
            $table->string('work_type')->default('');
            $table->text('work_params')->nullable();
            $table->string('throttle')->default('');
            $table->dateTime('last_run')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hive_works');
    }

}
