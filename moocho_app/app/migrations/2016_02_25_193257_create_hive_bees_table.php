<?php

class CreateHiveBeesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hive_bees', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->default('');
            $table->string('endpoint')->default('');
            $table->string('ipaddress')->default('');
            $table->text('keys')->default('');
            $table->tinyInteger('is_proxy')->default(0);
            $table->text('capabilities')->default('');
            $table->dateTime('last_run')->nullable();
            $table->string('status')->default('');
            $table->float('version')->default(1.0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hive_bees');
    }

}
