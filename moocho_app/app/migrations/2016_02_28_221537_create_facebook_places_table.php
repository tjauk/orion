<?php

class CreateFacebookPlacesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facebook_places', function($table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('status')->default('');

            $table->string('name')->default('');
            $table->bigInteger('fb_id')->unsigned();
            $table->string('street')->default('');
            $table->string('city')->default('');
            $table->string('state')->default('');
            $table->string('zip')->default('');
            $table->string('country')->default('');
            $table->integer('country_id')->unsigned()->nullable();

            $table->decimal('lat',10,8)->nullable();
            $table->decimal('lon',11,8)->nullable();

            $table->string('category')->default('');
            $table->string('category_names')->default('');
            $table->text('category_list')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('facebook_places');
    }

}
