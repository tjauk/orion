<?php

class CreateCrawlSitesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crawl_sites', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->nullable();
            $table->string('url')->nullable();
            $table->text('rules')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('crawl_sites');
    }

}
