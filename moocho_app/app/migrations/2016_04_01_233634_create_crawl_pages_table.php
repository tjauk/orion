<?php

class CreateCrawlPagesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crawl_pages', function($table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('crawl_site_id')->unsigned();
            $table->string('url')->default('');
            $table->integer('crawling')->unsigned()->default(0);
            $table->tinyInteger('crawled')->unsigned()->default(0);
            $table->integer('status')->unsigned()->nullable();
            $table->text('headers')->nullable();
            $table->string('html')->nullable();
            $table->tinyInteger('analysed')->unsiged()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('crawl_pages');
    }

}
