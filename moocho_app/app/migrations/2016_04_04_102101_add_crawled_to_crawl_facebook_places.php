<?php

class AddCrawledToCrawlFacebookPlaces extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crawl_facebook_places', function($table)
        {
            $table->integer('crawled')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crawl_facebook_places', function($table)
        {
            $table->dropColumn('crawled');
        });
    }

}
