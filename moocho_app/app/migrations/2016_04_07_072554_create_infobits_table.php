<?php

class CreateInfobitsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infobits', function($table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('label',40)->default('value');
            $table->string('val')->default('');
        });
        Schema::create('infonets', function($table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('pair',50)->default('');
            $table->bigInteger('from')->unsigned()->default(0);
            $table->float('weight')->default(0.0);
            $table->bigInteger('to')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('infobits');
        Schema::drop('infonets');
    }

}
