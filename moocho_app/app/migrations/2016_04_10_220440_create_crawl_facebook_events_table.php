<?php

class CreateCrawlFacebookEventsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crawl_facebook_events', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('fbid')->unsigned()->unique();
            $table->string('name')->default('');
            $table->text('description')->default('');
            $table->bigInteger('fbplace_id')->unsigned()->unique();
            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('crawl_facebook_events');
    }

}
