<?php

class CreateCrawlGooglePlacesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crawl_google_places', function($table)
        {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('name')->default('');
            $table->string('icon')->default('');
            $table->string('types')->default(''); //array,set
            $table->string('google_id')->default('');
            $table->string('place_id')->unique();
            
            $table->decimal('lat',10,8)->nullable();
            $table->decimal('lon',11,8)->nullable();
            $table->string('vicinity')->nullable();

            $table->string('street')->default('');
            $table->string('city')->default('');
            $table->string('state')->default('');
            $table->string('zip')->default('');
            $table->string('country')->default('');
            $table->integer('country_id')->unsigned()->nullable();
            
            $table->float('rating')->nullable();
            $table->string('reference')->default('');

            $table->text('photos')->default('');//json

            $table->integer('crawled')->default(0);

            $table->text('json')->default('');
            $table->string('jsonhash')->default('');
            $table->dateTime('changed_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('crawl_google_places');
    }

}
