<?php

class AddDepthToCrawlPages extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crawl_pages', function($table)
        {
            $table->tinyInteger('depth')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crawl_pages', function($table)
        {
            $table->dropColumn('depth');
        });
    }

}
