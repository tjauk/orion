<?php

class AddJsonhashToCrawlFacebookEvents extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crawl_facebook_events', function($table)
        {
            $table->text('json')->default('');
            $table->string('jsonhash')->default('');
            $table->dateTime('changed_at');
            $table->integer('crawled')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crawl_facebook_events', function($table)
        {
            $table->dropColumn('json');
            $table->dropColumn('jsonhash');
            $table->dropColumn('changed_at');
            $table->dropColumn('crawled');
        });
    }

}
