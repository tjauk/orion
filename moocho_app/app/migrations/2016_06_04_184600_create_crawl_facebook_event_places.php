<?php

class CreateCrawlFacebookEventPlaces extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crawl_fb_event_places', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('status')->default('');

            $table->string('name')->nullable();
            $table->bigInteger('fbid')->unsigned();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('country')->nullable();
            $table->integer('country_id')->unsigned()->nullable();

            $table->decimal('lat',10,8)->nullable();
            $table->decimal('lon',11,8)->nullable();

            $table->string('category')->nullable();
            $table->string('category_names')->nullable();
            $table->text('category_list')->nullable();

            $table->text('json')->default('');
            $table->string('jsonhash')->default('');
            $table->dateTime('changed_at');
            $table->integer('crawled')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('crawl_fb_event_places');
    }

}
