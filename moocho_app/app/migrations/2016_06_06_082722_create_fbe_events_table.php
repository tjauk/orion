<?php

class CreateFbeEventsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fbe_events', function($table)
        {
            $table->increments('id');
            $table->timestamps();

            $table->bigInteger('fbid')->unsigned()->unique();

            $table->string('name')->default('');
            $table->text('description')->nullable();
            $table->string('category')->nullable();
            $table->text('tags')->default('');

            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();

            $table->string('timezone')->nullable();
            
            $table->string('start_utc')->nullable();
            $table->string('end_utc')->nullable();

            $table->integer('duplicate')->unsigned()->nullable();
            $table->integer('quality')->default(0);

            $table->string('place_name')->nullable();
            $table->bigInteger('place_fbid')->unsigned()->nullable();
            
            $table->string('address')->nullable();
            $table->string('zip')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();

            $table->decimal('lat',10,8)->nullable();
            $table->decimal('lon',11,8)->nullable();

            $table->integer('attending_count')->nullable();
            $table->integer('declined_count')->nullable();

            $table->string('cover')->nullable();

            $table->string('cover_fbid')->nullable();
            $table->string('cover_source')->nullable();
            $table->integer('cover_offset_x')->nullable();
            $table->integer('cover_offset_y')->nullable();

            $table->string('ticket_uri')->nullable();

            $table->text('json')->default('');
            $table->string('jsonhash')->default('');
            $table->dateTime('changed_at');

            $table->integer('crawled')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fbe_events');
    }

}
