<?php

class CreateFbePagesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fbe_pages', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->default('');

            $table->text('about')->default('');
            $table->text('description')->default('');

            $table->integer('likes')->default(0);

            $table->string('phone')->nullable();
            $table->string('website')->nullable();

            $table->text('json')->default('');
            $table->string('jsonhash')->default('');
            $table->dateTime('changed_at');
            $table->integer('crawled')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fbe_pages');
    }

}
