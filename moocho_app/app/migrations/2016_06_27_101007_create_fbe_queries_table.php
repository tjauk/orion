<?php

class CreateFbeQueriesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fbe_queries', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('country')->default('');
            $table->string('query')->default('');
            $table->dateTime('done')->nullable();
            $table->integer('found')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fbe_queries');
    }

}
