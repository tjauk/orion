<?php

class CreateFbeCitiesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fbe_cities', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('country')->default('');
            $table->string('region')->nullable();
            $table->string('name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fbe_cities');
    }

}
