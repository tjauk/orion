<?php

class AddFieldPhaseToFbeQueries extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fbe_queries', function($table)
        {
            $table->string('phase')->default('');
            $table->string('status')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fbe_queries', function($table)
        {
            $table->dropColumn('phase');
            $table->dropColumn('status');
        });
    }

}
