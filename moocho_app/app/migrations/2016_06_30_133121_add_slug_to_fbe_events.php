<?php

class AddSlugToFbeEvents extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fbe_events', function($table)
        {
            $table->string('slug')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fbe_events', function($table)
        {
            $table->dropColumn('slug');
        });
    }

}
