<?php

class AddActiveToBees extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hive_bees', function($table)
        {
            $table->tinyInteger('active')->unsigned()->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hive_bees', function($table)
        {
            $table->dropColumn('active');
        });
    }

}
