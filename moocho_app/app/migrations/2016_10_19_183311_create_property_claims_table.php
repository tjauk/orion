<?php

class CreatePropertyClaimsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_claims', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->integer('NR')->unsigned()->nullable();
            $table->string('Date')->default('');
            $table->string('Source')->default('');
            $table->string('Property_ID')->default('');
            $table->string('Owners_Name')->default('');
            $table->text('Reported_Address')->default('');
            $table->string('Property_Type')->default('');
            $table->string('Cash_Report')->default('');
            $table->string('Reported_By')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_claims');
    }

}
/*
(
    [Date] => 10/19/2016
    [Source] => INT
    [Property_ID] => 965376500
    [Owners_Name] => FORBES ROSE LOWRIE
    [Reported_Address] => 4900 ANGELES VISTA BLVD       LOS ANGELES                    CA 90043-1737
    [Property_Type] => Royalties
    [Cash_Report] => $0.01
    [Reported_By] => DEVON ENERGY PRODUCTION COMPANY LP
)
*/