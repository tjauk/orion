<?php

class CreateTablePeros extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peros', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->default('');
            $table->text('body');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('peros');
    }

}
