<?php
class BeaconCatcher extends Model
{
    protected $table = 'beacon_catchers';

    protected $fillable = [
		'uid',
		'minor',
		'major',
		'distance',
		'time',
		'action',
		'mac',
		'lat',
		'lon'
    ];

    /*
     * Relations
     */



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */


}
