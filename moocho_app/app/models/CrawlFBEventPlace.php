<?php
class CrawlFBEventPlace extends Model
{
    protected $table = 'crawl_fb_event_places';

    protected $fillable = [
		'status',
        'name',
        'fbid',
        'street',
		'city',
		'state',
        'zip',
        'country',
        'country_id',
		'lat',
		'lon',
        'category',
        'category_names',
        'category_list',
		'json',
		'jsonhash',
		'changed_at',
        'crawled'
    ];

    public $is_new = false;
    public $is_changed = false;


    /*
     * Scopes
     */
    public function scopeNext($query,$take=1)
    {
        $query->where('crawled', '<', 3)->take($take);
    }
    public function scopeLast($query,$take=1)
    {
        $query->where('crawled', '<', 3)->take($take)->orderBy('id',DESC);
    }
    public function scopeRandom($query,$take=1)
    {
        $query->where('crawled', '<', 3)->take($take)->orderBy(DB::raw('RAND()'));
    }


    /*
     * Importer
     */
    static function import($data)
    {
        $place = CrawlFBEventPlace::findByFbid($data['id']);
        $jsonhash = md5(json_encode($data));

        // update
        if( $place->id>0 ){

        }else{
            $place = new CrawlFBEventPlace;
            $place->is_new = true;
            $place->crawled = 0;
        }

        $place->category = '';
        $place->category_names = '';
        $place->category_list = '';

        $place->fbid = $data['id'];
        $place->name = $data['name'];
        $place->status = 'ok';
        if( !empty($data['category']) ){
            $place->category = $data['category'];
        }
        if( count($data['category_list'])>0 ){
            $place->category_id = $data['category_list'][0]['id'];
            $place->category_name = $data['category_list'][0]['name'];
            $tags = [];
            foreach($data['category_list'] as $cat){
                $tags[] = $cat['name'];
            }
            $place->category_tags = implode(',',$tags);
        }
        $place->address = $data['location']['street'];
        $place->zip     = preg_replace('/\s+/', '', $data['location']['zip']);
        $place->city    = $data['location']['city'];
        $place->state   = $data['location']['state'];
        $place->country = $data['location']['country'];
        $place->lat     = $data['location']['latitude'];
        $place->lon     = $data['location']['longitude'];

        if( $place->jsonhash!==$jsonhash ){
            $place->changed_at = date('Y-m-d H:i:s');
            $place->jsonhash = $jsonhash;
            $place->json = json_encode($data);
            $place->is_changed = true;
            $place->crawled = $place->crawled + 1;
            $place->save();
        }else{
            $place->increment('crawled');
        }

        return $place;
    }



    /*
     * Options
     */


    /*
     * Scopes
     */


    /*
     * Getters
     */


    /*
     * Setters
     */


}
