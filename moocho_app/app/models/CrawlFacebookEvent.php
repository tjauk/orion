<?php
class CrawlFacebookEvent extends Model
{
    protected $table = 'crawl_facebook_events';

    protected $fillable = [
		'fbid',
		'name',
		'description',
        'place_id',
		'start_time',
		'end_time',
		'json',
		'jsonhash',
		'changed_at',
        'crawled'
    ];

    public $is_new = false;
    public $is_changed = false;


    /*
     * Scopes
     */
    public function scopeNext($query,$take=1)
    {
        $query->where('crawled', '<', 3)->take($take);
    }
    public function scopeLast($query,$take=1)
    {
        $query->where('crawled', '<', 3)->take($take)->orderBy('id',DESC);
    }
    public function scopeRandom($query,$take=1)
    {
        $query->where('crawled', '<', 3)->take($take)->orderBy(DB::raw('RAND()'));
    }


    /*
     * Importer
     */
    static function import($data)
    {
        $event = CrawlFacebookEvent::findByFbid($data['id']);
        $jsonhash = md5(json_encode($data));

        // update
        if( $event->id>0 ){

        }else{
            $event = new CrawlFacebookevent;
            $event->is_new = true;
            $event->crawled = 0;
        }

        $event->fbid = $data['id'];
        $event->name = $data['name'];
        if( !empty($data['start_time']) ){
            $event->start_time = $data['start_time'];
        }
        if( !empty($data['end_time']) ){
            $event->end_time = $data['end_time'];
        }

        if( !empty($data['description']) ){
            $event->description = $data['description'];
        }else{
            $event->description = '';
        }
        if( isset($data['place']) and $data['place']['id']>0 ){
            $event->place_id = $data['place']['id'];
            $place = CrawlFBEventPlace::import($data['place']);
        }
        
        if( $event->jsonhash!==$jsonhash ){
            $event->changed_at = date('Y-m-d H:i:s');
            $event->jsonhash = $jsonhash;
            $event->json = json_encode($data);
            $event->is_changed = true;
            $event->crawled = $event->crawled + 1;
            $event->save();
        }else{
            $event->increment('crawled');
        }

        return $event;
    }



    /*
     * Options
     */


    /*
     * Scopes
     */


    /*
     * Getters
     */


    /*
     * Setters
     */


}
