<?php
class CrawlFacebookPlace extends Model
{
    public $table = 'crawl_facebook_places';

    protected $fillable = [
		'fbid',
		'name',
		'status',
        'page',
		'category',
		'category_id',
		'category_name',
		'category_tags',
		'address',
		'zip',
		'city',
		'state',
		'country',
		'lat',
		'lon',
		'json',
		'jsonhash',
		'changed_at',
        'crawled'
    ];

    public $is_new = false;
    public $is_changed = false;


    /*
     * Scopes
     */
    public function scopeNext($query,$take=1)
    {
        $query->where('crawled', '<', 3)->take($take);
    }
    public function scopeLast($query,$take=1)
    {
        $query->where('crawled', '<', 3)->take($take)->orderBy('id',DESC);
    }
    public function scopeRandom($query,$take=1)
    {
        $query->where('crawled', '<', 3)->take($take)->orderBy(DB::raw('RAND()'));
    }


    /*
     * Importer
     */
    static function import($data)
    {
        // insert from optimized table
        if( is_object($data) and $data instanceof CrawlFacebookPlaceOpt ){
            $place = new static;
            $place->is_new = true;
            $place->crawled = 1;

            $place->fbid            = $data->fbid;
            $place->name            = $data->name;
            $place->status          = $data->status;
            $place->page            = $data->page;
            $place->category        = $data->category;
            $place->category_id     = $data->category_id;
            $place->category_name   = $data->category_name;
            $place->category_tags   = $data->category_tags;
            $place->address         = $data->address;
            $place->zip             = $data->zip;
            $place->city            = $data->city;
            $place->state           = $data->state;
            $place->country         = $data->country;
            $place->lat             = $data->lat;
            $place->lon             = $data->lon;
            $place->json            = $data->json;
            $place->jsonhash        = $data->jsonhash;
            $place->changed_at      = $data->changed_at;

            $place->save();

        // regular import 
        }else{

            $place = static::findByFbid($data['id']);
            $jsonhash = md5(json_encode($data));

            // update
            if( $place->id>0 ){

            }else{
                $place = new static;
                $place->is_new = true;
                $place->crawled = 0;
            }

            $place->fbid = $data['id'];
            $place->name = $data['name'];
            $place->status = 'ok';
            $place->category = $data['category'];
            if( count($data['category_list'])>0 ){
                $place->category_id = $data['category_list'][0]['id'];
                $place->category_name = $data['category_list'][0]['name'];
                $tags = [];
                foreach($data['category_list'] as $cat){
                    $tags[] = $cat['name'];
                }
                $place->category_tags = implode(',',$tags);
            }
            $place->address = $data['location']['street'];
            $place->zip     = preg_replace('/\s+/', '', $data['location']['zip']);
            $place->city    = $data['location']['city'];
            $place->state   = $data['location']['state'];
            $place->country = $data['location']['country'];
            $place->lat     = $data['location']['latitude'];
            $place->lon     = $data['location']['longitude'];

            if( $place->jsonhash!==$jsonhash ){
                $place->changed_at = date('Y-m-d H:i:s');
                $place->jsonhash = $jsonhash;
                $place->json = json_encode($data);
                $place->is_changed = true;
                if( $place->crawled<4 ){
                    $place->crawled = $place->crawled + 1;
                    $place->save();
                }
            }else if( $place->crawled<3 ){
                $place->increment('crawled');
            }

            //$place->crawled = $place->crawled + 1;
            //$place->save();
        }

        return $place;
    }



    /*
     * Options
     */


    /*
     * Scopes
     */


    /*
     * Getters
     */


    /*
     * Setters
     */


}
