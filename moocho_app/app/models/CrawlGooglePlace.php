<?php
class CrawlGooglePlace extends Model
{
    protected $table = 'crawl_google_places';

    protected $fillable = [
		'name',
		'icon',
		'types',
        'google_id',
		'place_id',
		
        'lat',
		'lon',
        'vicinity',
        
        'street',
        'address',
        'zip',
        'city',
        'state',
        'country',
        'country_id',
        
        'rating',
        'reference',
        'photos',
        
        'json',
        'jsonhash',
        'changed_at',
        'crawled',
    ];

    public $is_new = false;
    public $is_changed = false;


    /*
     * Scopes
     */
    public function scopeNext($query,$take=1)
    {
        $query->where('crawled', '<', 3)->take($take);
    }
    public function scopeLast($query,$take=1)
    {
        $query->where('crawled', '<', 3)->take($take)->orderBy('id',DESC);
    }
    public function scopeType($query,$type='store')
    {
        $query->whereRaw("FIND_IN_SET('$type',types)");
    }


    /*
     * Importer
     */
    static function import($data)
    {
        $place = CrawlGooglePlace::findByPlaceId($data['place_id']);
        $jsonhash = md5(json_encode($data));

        // update
        if( $place->id>0 ){

        }else{
            $place = new CrawlGooglePlace;
            $place->is_new = true;
            $place->crawled = 0;
        }

        $place->status = 'ok';

        $place->name = $data['name'];
        $place->icon = $data['icon'];
        $place->types = implode(',',$data['types']);

        $place->google_id = $data['id'];
        $place->place_id = $data['place_id'];

        $place->address = '';
        $place->zip     = '';
        $place->city    = '';
        $place->state   = '';
        $place->country = '';

        $place->lat     = $data['geometry']['location']['lat'];
        $place->lon     = $data['geometry']['location']['lng'];
        $place->vicinity= $data['vicinity'];

        $place->rating = $data['rating'];
        $place->reference = $data['reference'];
        $place->photos = json_encode($data['photos']);
        
        if( $place->jsonhash!==$jsonhash ){
            $place->changed_at = date('Y-m-d H:i:s');
            $place->jsonhash = $jsonhash;
            $place->json = json_encode($data);
            $place->is_changed = true;
        }
        
        $place->crawled = $place->crawled + 1;

        $place->save();

        return $place;
    }



    /*
     * Options
     */


    /*
     * Scopes
     */


    /*
     * Getters
     */


    /*
     * Setters
     */


}
