<?php
use Symfony\Component\DomCrawler\Crawler;

class CrawlPage extends Model
{
    static $compress = 0;

    protected $table = 'crawl_pages';

    protected $fillable = [
		'crawl_site_id',
		'url',
		'crawling',
		'crawled',
        'status',
		'headers',
        'html',
        'depth',
		'analysed'
    ];


    public static function compress($compress)
    {
        static::$compress = $compress;
    }

    /*
     * Relations
     */
    public function site()
    {
        return $this->belongsTo('CrawlSite','crawl_site_id');
    }


    /*
     * Scopes
     */
    public function scopeNext($query,$take=10)
    {
        $time = strtotime('now -2 hours');
        $query->where('crawling', '<', $time)->where('crawled', '=', 0)->take($take);
    }


    /*
     * Register
     */
    static function register($url){

        $page = CrawlPage::findByUrl($url);
        if( $page->id>0 ){
            return $page;
        }
        
        $site = CrawlSite::register($url);

        $page = new CrawlPage();
        $page->crawl_site_id = $site->id;
        $page->url = $url;
        $page->crawling = 0;
        $page->crawled = 0;
        $page->headers = '';
        $page->html = '';
        $page->depth=0;
        $page->analysed = 0;
        $page->save();

        return $page;
    }


    /*
     * Methods
     */
    public function extractAllLinks()
    {
        $this->crawling = 1;
        $this->save();

        $depth = (int)$this->depth;
        $url = $this->url;

        $client = new GuzzleHttp\Client();

        try {
            $response = $client->request('GET', $url);
        } catch (\Exception $e) {
            echo 'Uh oh! '.$e->getMessage();
            $this->status = 404;
            $this->crawling = 0;
            $this->crawled = 1;
            $this->save();
            return false;
        }
        
        $status     = (int)$response->getStatusCode();
        $headers    = (array)$response->getHeaders();
        $body       = (string)$response->getBody();

        $this->status = $status;
        $this->headers = json_encode($headers);

        if( $this->status !== 200 ){
            $this->crawling = 0;
            $this->crawled = 1;
            $this->save();
            return false;
        }

        $html = new Crawler($body);
        $found = $html->filter('a')->each(function (Crawler $node, $i) {
            return $node->attr('href');
        });
       
        $links = [];
        foreach($found as $link){
            if( !empty($link) ){
                $link = $this->getAbsoluteLink($link);
                if( $this->matchSiteRules($link,['all']) ){
                    $links[] = $link;
                }
            }
        }

        $links = array_unique($links);

        // from text
        $text = strip_tags($body); 
        $textlinks = static::extractTextualLinks($text);
        foreach($textlinks as $link){
            $link = $this->getAbsoluteLink($link);
            if( $this->matchSiteRules($link,['all']) ){
                $links[] = $link;
            }
        }

        foreach ($links as $link) {
            $site = CrawlSite::register($link);
            if( $site->id>0 ){
                $page = CrawlPage::register($link);
                if( $page->crawl_site_id<>$this->crawl_site_id ){
                    $page->depth = 0;
                }else if($page->depth==0){
                    $page->depth = $depth + 1;
                }
                $page->save();
            }
        }
        
        $this->crawling = 0;
        $this->crawled = 1;
        $this->save();
    }

    public function extractLinks()
    {
        $this->crawling = 1;
        $this->save();

        $url = $this->url;

        $client = new GuzzleHttp\Client();

        try {
            $response = $client->request('GET', $url);
        } catch (\Exception $e) {
            echo 'Uh oh! '.$e->getMessage();
            $this->status = 404;
            $this->crawling = 0;
            $this->crawled = 1;
            $this->save();
            return false;
        }
        
        $status     = (int)$response->getStatusCode();
        $headers    = (array)$response->getHeaders();
        $body       = (string)$response->getBody();

        $this->status = $status;
        $this->headers = json_encode($headers);

        if( $this->status !== 200 ){
            $this->crawling = 0;
            $this->crawled = 1;
            $this->save();
            return false;
        }

        $filepath = ROOTDIR.'/webcache/'.$this->id.'.html';
        if( static::$compress==1 ){
            File::compress($filepath.'.gz',$body);
        }else{
            File::save($filepath,$body);
        }
        $this->html = $filepath;

        $html = new Crawler($body);
        $found = $html->filter('a')->each(function (Crawler $node, $i) {
            return $node->attr('href');
        });
       
        $links = [];
        foreach($found as $link){
            if( !empty($link) ){
                $link = $this->getAbsoluteLink($link);
                if( $this->matchSiteRules($link) ){
                    $links[] = $link;
                }
            }
        }

        $links = array_unique($links);

        foreach ($links as $link) {
            $page = CrawlPage::register($link);
            $page->depth = $this->depth + 1;
            $page->save();
        }
        
        $this->crawling = 0;
        $this->crawled = 1;
        $this->save();
    }

    static function extractTextualLinks($text)
    {
        $links = [];
        $regex_urls = "/(http|https)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        //http://www.vegicept.com/trgovine
        preg_match_all($regex_urls, $text, $urls);
        if( is_array($urls[0]) ){
            foreach($urls[0] as $link){
                $links[] = $link;
            }
        }

        $regex_urls = "/(www\.)[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        preg_match_all($regex_urls, $text, $urls);
        if( is_array($urls[0]) ){
            foreach($urls[0] as $link){
                $links[] = 'http://'.$link;
            }
        }

        return array_unique($links);
    }

    public function getAbsoluteLink($link){
        $info = parse_url($this->url);
        $scheme = $info['scheme'];
        $host = $info['host'];
        $home = "$scheme://$host";
        if( substr($link,0,1)=='/' ){
            return $home.$link;
        }
        return $link;
    }

    public function matchSiteRules($url,$rules=array())
    {
        if( empty($rules) ){
            $rules = $this->site->rules;
        }
        if( empty($rules) ){
            return true;
        }

        $match = false;

        if( in_array('all',$rules) ){
            $match = true;
        }

        // skip this site
        if( in_array('skip',$rules) ){
            return false;
        }

        // match only the same web host or domain
        if( in_array('samehost',$rules) ){
            $info = parse_url($url);
            $host = $info['host'];
            if( $this->site->host == $host ){
                $match = true;
            }
        }

        // ignore links containing query variables
        $info = parse_url($url);
        $query = $info['query'];
        if( strpos($query,'filter[')!==false ){
            $match = false;
        }
        if( strpos($url,'dodaj-u-kosaricu')!==false ){
            $match = false;
        }
        if( strpos($url,'#tagovi')!==false ){
            $match = false;
        }
        if( strpos($url,'#ocijenite')!==false ){
            $match = false;
        }
        if( strpos($url,'cgi-bin')!==false ){
            $match = false;
        }

        // check link file extensions
        $low = strtolower($url);
        if( in_array( substr($low,-4,4) , ['.jpg','.gif','.png','.bmp','.xml','.doc','.xls','.zip','.rar','.pdf'] ) ){
            $match = false;
        }
        if( in_array( substr($low,-5,5) , ['.jpeg','.docx','.xlsx','.json'] ) ){
            $match = false;
        }
        return $match;
    }

    public function done()
    {
        $this->crawling = 0;
        $this->crawled = 1;
        $this->save();
        return $this;
    }


    /*
     * Getters
     */


    /*
     * Setters
     */
    public function setCrawlingAttribute($val=0)
    {
        if( $val>0 ){
            $this->attributes['crawling'] = time();
        }else{
            $this->attributes['crawling'] = $val;
        }
    }

}
