<?php
class CrawlSite extends Model
{

    protected $table = 'crawl_sites';

    protected $fillable = [
		'name',
		'url',
		'rules'
    ];


    /*
     * Relations
     */
    public function pages()
    {
        return $this->hasMany('CrawlPage','crawl_site_id');
    }


    /*
     * Register
     */
    static function register($url){

        $info = parse_url($url);
        $scheme = $info['scheme'];
        $host = $info['host'];
        $home = "$scheme://$host";

        if( empty($host) ){
            return false;
        }

        $site = CrawlSite::findByUrl($home);
        if( $site->id>0 ){
            return $site;
        }
        
        $site = new CrawlSite();
        $site->name = $host;
        $site->url = $home;
        $site->rules = 'samehost';
        $site->save();

        // create first link to crawl
        CrawlPage::register($url);

        return $site;
    }


    /*
     * Options
     */


    /*
     * Scopes
     */


    /*
     * Getters
     */
    public function getRulesAttribute()
    {
        $rules = array_filter(explode("\n",str_replace("\r","",$this->attributes['rules'])));
        return $rules;
    }
    public function getHostAttribute()
    {
        $info = parse_url($this->url);
        $host = $info['host'];
        return $host;
    }
    public function getSchemeAttribute()
    {
        $info = parse_url($this->url);
        $scheme = $info['scheme'];
        return $scheme;
    }


    /*
     * Setters
     */


    /*
     * Counters
     */
    public function getTotalPages()
    {
        return CrawlPage::whereCrawlSiteId($this->id)->count();
    }
    public function getTotalCrawled()
    {
        return CrawlPage::whereCrawlSiteId($this->id)->whereCrawled(1)->count();
    }
    public function getTotalAnalysed()
    {
        return CrawlPage::whereCrawlSiteId($this->id)->whereAnalysed(1)->count();
    }


}
