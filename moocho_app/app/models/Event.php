<?php
class Event extends Model
{
    protected $table = 'events';

    protected $fillable = ['status','user_id','title','slug','place_id','starts','ends','repeatable','repeat_mode','repeat_func','src','src_uid','summary','content','photos','cover','flyer','ticketed'];

    /*
     * Relations
     */


    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */


}
