<?php
class FbeEvent extends Model
{
    protected $table = 'fbe_events';

    protected $fillable = [
		'fbid',

        'name',
        'slug',
        'description',
        'category',
        'tags',

        'start_time',
        'end_time',
        'timezone',
        'start_utc',
        'end_utc',

        'duplicate',
        'quality',

        'place_name',
        'place_fbid',

        'address',
        'zip',
        'city',
        'state',
        'country',

        'lat',
        'lon',

        'attending_count',
        'declined_count',

        'cover',
        
        'cover_fbid',
        'cover_source',
        'cover_offset_x',
        'cover_offset_y',
        
        'ticket_uri',
        'json',
        'jsonhash',
        'changed_at',
        'crawled',
    ];

    public $is_new = false;
    public $is_changed = false;

    /*
     * Relations
     */


    /*
     * Scopes
     */
    public function scopeActive($query)
    {
        $query->whereNotNull('cover')->whereNull('duplicate')->orderBy('start_time',ASC);
    }
    public function scopeNext($query,$take=1)
    {
        $query->where('crawled', '<', 3)->take($take);
    }
    public function scopeLast($query,$take=1)
    {
        $query->where('crawled', '<', 3)->take($take)->orderBy('id',DESC);
    }
    public function scopeRandom($query,$take=1)
    {
        $query->where('crawled', '<', 3)->take($take)->orderBy(DB::raw('RAND()'));
    }


    public function scopeTakeCovers($query,$take=1)
    {
        $query->whereNotNull('cover_source')->whereNull('cover')->take($take);
    }



    /*
     * Getters
     */
    public function getCoverThumbAttribute()
    {
        return Img::thumb($this->cover,'260x90xC')->url();
    }


    /*
     * Setters
     */
    public function setSlugAttribute($str)
    {
        $this->attributes['slug'] = str_slug($str);
    }
    public function calucalateQualityScore()
    {
        $q = 0;
        // non empty fields score as 1
        $fields = ['name','description','category','cover_fbid','cover','start_time','end_time','place_name','address','city','attending_count','declined_count','ticket_uri'];
        foreach($fields as $field){
            if( !empty($this->$field) ){
                $q++;
            }
        }
        $this->quality = $q;
    }



    /*
     * Importer
     */
    static function import($data)
    {

            $event = static::findByFbid($data['id']);
            $jsonhash = md5(json_encode($data));

            // update
            if( $event->id>0 ){

            }else{
                $event = new static;
                $event->is_new = true;
                $event->crawled = 0;
            }

            $event->fbid        = $data['id'];

            // basic info
            $event->name        = $data['name'];
            $event->slug        = $data['name'];
            $event->description = $data['description'];
            $event->category    = $data['category'];

            // time
            $event->start_utc  = $data['start_time'];
            $event->end_utc    = $data['end_time'];
            $event->timezone    = $data['timezone'];

            $event->start_time  = date('Y-m-d H:i:s',strtotime($data['start_time']));
            if( !empty($event->end_utc) ){
                $event->end_time    = date('Y-m-d H:i:s',strtotime($data['end_time']));
            }


            // place, venue, location
            $country = '';
            if( isset($data['place']) ){
                $place = $data['place'];
                $event->place_fbid     = $place['id'];
                $event->place_name  = $place['name'];

                if( isset($place['location']) ){
                    $event->address     = $place['location']['street'];
                    $event->zip         = $place['location']['zip'];
                    $event->city        = $place['location']['city'];
                    $country = $event->country     = $place['location']['country'];
                    $event->lat         = $place['location']['latitude'];
                    $event->lon         = $place['location']['longitude'];
                }

                // import place
                $place = FbePlace::import($place);
            }

            // extract words for dictionary
            if( $event->is_new and !empty($country) ){
                $replace = ['@','#','(',')','\\'];
                $words = str_replace($replace, ' ', $event->name);
                $words = ML::tokenizeWords($words,1);
                $words = array_filter($words,function($item){
                    if( mb_strlen($item)>3 ){
                        return true;
                    }
                    return false;
                });
                foreach($words as $word){
                    $dict = FbeDict::whereCountry($country)->whereWord($word)->first();
                    if( $dict->id>0 ){
                        $dict->increment('found');
                    }else{
                        $dict = FbeDict::create([
                                'country'   => $country,
                                'word'      => $word,
                                'found'     => 1
                            ]);
                    }
                }
            }

            // attending
            $event->attending_count  = $data['attending_count'];
            $event->declined_count    = $data['declined_count'];

            // cover
            if( isset($data['cover']) ){
                $event->cover_fbid      = $data['cover']['id'];
                $event->cover_source    = $data['cover']['source'];
                $event->cover_offset_x  = $data['cover']['offset_x'];
                $event->cover_offset_y  = $data['cover']['offset_y'];
            }

            // ticketing url if set
            $event->ticket_uri  = $data['ticket_uri'];

            // calculate quality score
            $event->calucalateQualityScore();

            // hash
            if( $event->jsonhash!==$jsonhash ){
                $event->changed_at = date('Y-m-d H:i:s');
                $event->jsonhash = $jsonhash;
                $event->json = json_encode($data);
                $event->is_changed = true;
                if( $event->crawled<4 ){
                    $event->crawled = $event->crawled + 1;
                    $event->save();
                }
            }else if( $event->crawled<3 ){
                $event->increment('crawled');
            }

        return $event;
    }



    public function downloadCover()
    {
        $output = '';

        if( !is_null($this->cover_source) and !empty($this->country) and is_null($this->cover) ){
            
            $number_of_retries_on_error = 3;

            $country = $this->country;
            $covers_folder = ROOTDIR.'/images/covers/'.str_slug($country);
            Folder::make($covers_folder);

            $parsed = parse_url($this->cover_source);
            $path = $parsed['path'];
            $ext = File::ext($path);
            $crc = substr($this->cover_fbid,-4,4);

            $filename =str_slug($this->name.'-'.$crc).'.'.$ext;
            $filepath = $covers_folder.'/'.$filename;

            $output.="Downloading $filename ...";

            if( !file_exists($filepath) ){

                $data = Remote::get($this->cover_source);

                File::save($filepath,$data);

                $filesize = filesize($filepath);

                if( $filesize>99 ){
                    $this->cover = '/images/covers/'.str_slug($country).'/'.$filename;
                    $this->save();
                    $output.=" OK";
                    $error_retry=0;
                }else{
                    unlink($filepath);
                    $output.=" ERROR";
                    $error_retry++;
                    if( $error_retry>$number_of_retries_on_error ){
                        $this->cover = '';
                        $this->save();
                    }
                }

            }else{
                $this->cover = '/images/covers/'.str_slug($country).'/'.$filename;
                $this->save();
                $output.=" ALREADY EXISTS";
                $error_retry=0;
            }

            $output.="\n";

        }

        return $output;
    }

}
