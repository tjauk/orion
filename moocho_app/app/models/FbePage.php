<?php
class FbePage extends Model
{
    protected $table = 'fbe_pages';

    protected $fillable = [
		'name',
		'about',
		'description',
        'likes',
		'phone',
		'website',
		'json',
		'jsonhash',
		'changed_at',
		'crawled'
    ];

    /*
     * Relations
     */



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */


    /*
     * Setters
     */


}
