<?php
class FbeQuery extends Model
{
    protected $table = 'fbe_queries';

    protected $fillable = [
		'country',
		'query',
		'done',
		'found',
        'phase',
        'status'
    ];

    /*
     * Relations
     */



    /*
     * Scopes
     */
    public function scopeNext($query,$country,$take=1)
    {
        $query->where( DB::raw('DATE(done)'), '<>', DB::raw('CURRENT_DATE()') )
                ->whereCountry($country)
                ->take($take);
    }

    public function scopeToday($query)
    {
        $query->where( DB::raw('DATE(done)'), '<', DB::raw('CURRENT_DATE()') );
    }


    /*
     * Getters
     */
    public function isRunToday()
    {
        $done = date('Y-m-d',strtotime($this->done));
        $today = date('Y-m-d',time());
        if( $done==$today ){
            return true;
        }
        return false;
    }


    /*
     * Setters
     */
    static function register($country,$query,$phase=0)
    {
        $item = static::whereCountry($country)->whereQuery($query)->first();
        if( $item->id > 0 ){
        
        }else{
            $item = new static;
            $item->country = $country;
            $item->query = $query;
            $item->phase = $phase;
            $item->status = 'new';
            $item->save();
        }
        return $item;
    }


}
