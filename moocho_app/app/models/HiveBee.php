<?php
class HiveBee extends Model
{
    protected $table = 'hive_bees';

    protected $fillable = [
		'name',
		'endpoint',
        'ipaddress',
		'keys',
		'is_proxy',
		'capabilities',
		'last_run',
		'status',
        'version',
        'active'
    ];

    static $last_name = '';

    public function scopeActive($query)
    {
        $query->whereActive(1);
    }

    /*
     * Remote methods
     */
    static function remoteGet($url)
    {
        $bee = static::active()->orderBy('last_run','ASC')->first();
        $bee->last_run = date('Y-m-d H:i:s',strtotime('now'));
        $bee->save();

        static::$last_name = $bee->name;

        $json = Remote::get( $bee->script.'?do=GET64&url='.base64_encode($url) );
        $data = json_decode( $json, true );
        return $data['result'];
    }


    /*
     * Relations
     */



    /*
     * Options
     */


    /*
     * Scopes
     */


    /*
     * Getters
     */
    public function getScriptAttribute()
    {
        return rtrim($this->endpoint,'/').'/proxybee.php';
    }


    /*
     * Setters
     */


}
