<?php
class HiveWork extends Model
{
    protected $table = 'hive_works';

    protected $fillable = [
		'name',
		'description',
		'work',
		'work_type',
		'work_params',
		'throttle',
		'last_run'
    ];

    /*
     * Relations
     */



    /*
     * Options
     */


    /*
     * Scopes
     */


    /*
     * Getters
     */


    /*
     * Setters
     */


}
