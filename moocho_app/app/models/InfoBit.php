<?php
class InfoBit extends Model
{
    protected $table = 'infobits';

    protected $fillable = ['label','val'];

    static $label = 'n/a';
    static function label($label){
        static::$label = $label;
    }

    static function add($val='',$label=null)
    {
    	if( is_null($label) ){
            $label = static::$label;
        }
        $val = trim($val);
    	
        // try to recognize (email,link,url,phone,country)

		$label = str_slug($label);
		$bit = InfoBit::where('label','=',$label)->where('val','=',$val)->first();
		if( $bit->id > 0 ){

		}else{
			$bit = new InfoBit();
			$bit->label = $label;
			$bit->val = $val;
			$bit->save();        
        }

		return $bit;
    }

}
