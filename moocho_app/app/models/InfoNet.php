<?php
class InfoNet extends Model
{
    protected $table = 'infonets';

    protected $fillable = ['key','low','weight','high'];

    static $label = 'n/a';

    static function label($label){
        static::$label = $label;
    }

    static function bidir($first,$second,$weight=0.5)
    {
        $one = static::vector($first,$second,$weight);
        $two = static::vector($second,$first,$weight);
        return [$one,$two];
    }

    static function vector( $from, $to, $weight=0.5 )
    {

        if( is_string($from) ){
            $from = InfoBit::add($from);
        }
        if( is_string($to) ){
            $to = InfoBit::add($to);
        }

        if( $from->id <= $to->id ){
            $low = $from->id;
            $high = $to->id;
        }elseif( $from->id > $to->id ){
            $low = $to->id;
            $high = $from->id;
        }
        $key = (string)$low.'-'.(string)$high;

        $net = InfoNet::whereFrom($from)->whereTo($to)->first();

        if( $net->id > 0 ){
            $weight = ((1-$net->weight)/2)+$net->weight;
            $net->weight = $weight;
            $net->save();
        }else{
            $net = new InfoNet();
            $net->key = $key;
            $net->from = $from;
            $net->weight = $weight;
            $net->to = $to;
            $net->save();
        }

        return $net;
    }

    static function search($term,$label,$depth=1)
    {
        return $results;
    }
}
