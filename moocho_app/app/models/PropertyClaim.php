<?php
class PropertyClaim extends Model
{
    protected $table = 'property_claims';

    protected $fillable = [
		'NR',
		'Date',
		'Source',
		'Property_ID',
		'Owners_Name',
		'Reported_Address',
		'Property_Type',
		'Cash_Report',
		'Reported_By'
    ];

    /*
     * Relations
     */



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */


    /*
     * Setters
     */


}
