<div class="col-md-12">
    <h1>{{item.name}}</h1>
    <img src="{{item.cover}}">
    <br>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Responsive Minimalist -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2672392764909585"
     data-ad-slot="4294046819"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
    <br>
    <h4>When?</h4>
    <b>Start time: {{item.start_time}} - End time: {{item.end_time}}</b>
    <br>
    <br>
    <h4>Where?</h4>
    <b>{{item.place_name}}</b><br>
    <em>{{item.address}} {{item.city}} in {{item.country}} (Q:{{item.quality}})</em>
    <br>
    <h4>About</h4>
    <p>
    {{item.description|nbr}}
    </p>
</div>