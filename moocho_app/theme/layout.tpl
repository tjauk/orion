<!--layout.tpl-->
<!DOCTYPE html>
<html lang="en">
  <head>

    <title>{ doc.title }</title>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    { doc.meta }

    <link rel="icon" href="/favicon.ico">

    <!-- UIkit core CSS -->
    <link href="{ THEMEURL }css/uikit.gradient.min.css" rel="stylesheet">
    <link href="{ THEMEURL }styles.css" rel="stylesheet">

    <link href="{ THEMEURL }css/nv.d3.css" rel="stylesheet">
    { doc.css }

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    @asset "jquery"
    @asset "underscore"
    
    { doc.js }
    <!-- UIkit core JS -->
    <script src="{ THEMEURL }js/uikit.min.js"></script>

    <script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/angular.js/1.1.1/angular.min.js"></script>
    <script src="{ THEMEURL }js/line-chart-master/dist/line-chart.min.js"></script>
    

    <!-- graphs -->


    <style type="text/css">

    </style>


  </head>

  <body>

    <div class="uk-container uk-container-center" style="background-color:#fff;">

      <!-- Normal navbar -->
      <nav class="uk-navbar uk-margin-top uk-margin-bottom">
        <a class="uk-navbar-brand uk-hidden-small" href="{ ROOTURL }" style="padding-top:4px;">Kalkulator uštede</a>
        <?php $menus = Navigation::make()->only_roots()->get(); ?>
        <ul class="uk-navbar-nav uk-hidden-small">
          @loop menus as menu
            <li><a href="{ menu.link|href }">{ menu.title }</a></li>
          @end
          <?php if( Auth::guest() ){ ?>
            <li><a href="{ "user/login"|href }"><i class="uk-icon-user"></i> Prijava</a></li>
          <?php }else{ ?>
            <li class="uk-parent" data-uk-dropdown >
              <a href="{ "user/profile"|href }"><i class="uk-icon-user"></i> <?php echo Auth::user()->c_name.' '.Auth::user()->c_surname; ?></a>
              <div class="uk-dropdown uk-dropdown-navbar" style="">
                <ul class="uk-nav uk-nav-navbar">
                  <li><a href="{ "order/list"|href }">Ponude</a></li>
                  <li><a href="{ "user/profile"|href }">Profil</a></li>
                  <li><a href="{ "user/logout"|href }">Odjava</a></li>
                </ul>
              </div>
            </li>
          <?php }; ?>
        </ul>
        <a href="#offmenu" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas="{target:'#offmenu'}"></a>
        <div class="uk-navbar-brand uk-navbar-center uk-visible-small">Kalkulator uštede</div>
      </nav>


      <div class="uk-width-1-1 uk-margin-bottom">
        { CONTENT }
      </div>

      <div class="uk-grid uk-margin-bottom">
        <div class="uk-width-1-1 uk-text-right">
          <p>Sfera PLUS d.o.o. &copy;{ ""|date "Y" } , developed by <a href="http://www.trinium-media.hr" target="_blank">Trinium</a></p>
        </div>
      </div>

    </div>

      <!-- Offcanvas navbar -->
      <div id="offmenu" class="uk-offcanvas">
        <div class="uk-offcanvas-bar">
          <ul class="uk-nav uk-nav-offcanvas">
            <?php reset($menus); ?>
            @loop menus as menu
              <li><a href="{ menu.link|href }">{ menu.title }</a></li>
            @end
            <?php if( Auth::guest() ){ ?>
              <li class="uk-active"><a href="{ "user/login"|href }">Prijava</a></li>
            <?php }else{ ?>
              <li class="uk-active"><a href="{ "user/logout"|href }">Odjava</a></li>
            <?php }; ?>
          </ul>
        </div>
      </div>

  </body>
</html>
