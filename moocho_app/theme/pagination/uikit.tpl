<ul class="uk-pagination">
@loop items as item
	@if item.flag = "FIRST"
		<li><a href="{ item.link }">Prva</a></li>
	@end
	@if item.flag = "PREV"
		<li><a href="{ item.link }">Prethodna</a></li>
	@end
	@if item.flag = "NUMBER"
		@if item.selected = 1
			<li class="uk-active"><span>{ item.number }</span></li>
		@else
			<li><a href="{ item.link }">{ item.number }</a></li>
		@end
	@end
	@if item.flag = "BETWEEN"
		<li><span>...</span></li>
	@end
	@if item.flag = "NEXT"
		<li><a href="{ item.link }">Sljedeća</a></li>
	@end
	@if item.flag = "LAST"
		<li><a href="{ item.link }">Zadnja</a></li>
	@end
@end
</ul>

<div style="clear:both;"></div>
