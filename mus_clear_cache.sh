#!/bin/bash

# set all folders
declare -a folders=(events_australia events_brazil events_canada events_newzealand events_philippines events_singapore events_usa)

# loop through folders and execute
for folder in "${folders[@]}"
do
	find /var/www/orion/$folder/cache/globals/*.same.* -type f -delete
	find /var/www/orion/$folder/cache/globals/city.* -type f -delete
	find /var/www/orion/$folder/cache/globals/home.* -type f -delete
done

echo "Cache cleared on MUS server"
