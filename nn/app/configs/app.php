<?php
// you can add functions before return array call and they will get executed

// add shared events folder / module
/*
Autoload::register( 'configs', 'events', realpath(ROOTDIR.'/../events/app/configs') );
Autoload::register( 'controllers', 'events', realpath(ROOTDIR.'/../events/app/controllers') );
Autoload::register( 'jobs', 'events', realpath(ROOTDIR.'/../events/app/jobs') );
Autoload::register( 'migrations', 'events', realpath(ROOTDIR.'/../events/app/migrations') );
Autoload::register( 'models', 'events', realpath(ROOTDIR.'/../events/app/models') );
Autoload::register( 'views', 'events', realpath(ROOTDIR.'/../events/app/views') );
Autoload::register( 'resources', 'events', realpath(ROOTDIR.'/../events/app/resources') );
*/
Autoload::registerNamespace('Api');

// config
return array(

	// site country
	'country' => "EU",

	// default application title
	'title'	=> "NN tenders analysis",

	// default charset
	'charset' => 'utf-8',

	// default controller
	'home'	=> 'HomeController',

	// default admin controller
	'admin.home'	=> 'Admin\\HomeController',

	// default theme folder
	'theme'	=> '/theme',

	// default document layout template
	'layout' => 'layouts/default',


	// default image if photo is not available
	'image.not_available'	=> '/images/no-photo.jpg',

	// Google API
	'google.api.key'	=> '',

	/*
	|--------------------------------------------------------------------------
	| Application URL
	|--------------------------------------------------------------------------
	|
	| This URL is used by the console to properly generate URLs when using
	| the Artisan command line tool. You should set this to the root of
	| your application so that it is used when running Artisan tasks.
	|
	*/

	'url' => 'https://www.nn-stats.hr',

	/*
	|--------------------------------------------------------------------------
	| Application Timezone
	|--------------------------------------------------------------------------
	|
	| Here you may specify the default timezone for your application, which
	| will be used by the PHP date and date-time functions. We have gone
	| ahead and set this to a sensible default for you out of the box.
	|
	*/

	'timezone' => 'Europe/Zagreb',

	'date_format' => 'd.m.Y',
	'datetime_format' => 'd.m.Y H:i',

	/*
	|--------------------------------------------------------------------------
	| Application Debug Mode
	|--------------------------------------------------------------------------
	|
	| When your application is in debug mode, detailed error messages with
	| stack traces will be shown on every error that occurs within your
	| application. If disabled, a simple generic error page is shown.
	|
	*/

	'debug' => true,

	/*
	|--------------------------------------------------------------------------
	| Application Prototype Mode
	|--------------------------------------------------------------------------
	|
	| When your application is in prototype mode, instead of throwing errors,
	| it will try to create database tables and columns,
	| models, controllers and views.
	|
	*/

	'prototype' => false,


	/*
	|--------------------------------------------------------------------------
	| Application Locale Configuration
	|--------------------------------------------------------------------------
	|
	| The application locale determines the default locale that will be used
	| by the translation service provider. You are free to set this value
	| to any of the locales which will be supported by the application.
	|
	*/

	'locale' => 'hr',

	/*
	|--------------------------------------------------------------------------
	| Application Fallback Locale
	|--------------------------------------------------------------------------
	|
	| The fallback locale determines the locale to use when the current one
	| is not available. You may change the value to correspond to any of
	| the language folders that are provided through your application.
	|
	*/

	'fallback_locale' => 'en',

	/*
	|--------------------------------------------------------------------------
	| Encryption Key
	|--------------------------------------------------------------------------
	|
	| This key is used by the Illuminate encrypter service and should be set
	| to a random, 32 character string, otherwise these encrypted strings
	| will not be safe. Please do this before deploying an application!
	|
	*/

	'key' => '6TKelstF5T5eKfr1Ckrifb2K9DsY63AA',

	'cipher' => MCRYPT_RIJNDAEL_128,

	/*
	|--------------------------------------------------------------------------
	| Service Provider Manifest
	|--------------------------------------------------------------------------
	|
	| The service provider manifest is used by Laravel to lazy load service
	| providers which are not needed for each request, as well to keep a
	| list of all of the services. Here, you may set its storage spot.
	|
	*/

	//'manifest' => storage_path().'/meta',

	/*
	|--------------------------------------------------------------------------
	| Class Aliases
	|--------------------------------------------------------------------------
	|
	| This array of class aliases will be registered when this application
	| is started. However, feel free to register as many as you wish as
	| the aliases are "lazy" loaded so they don't hinder performance.
	|
	*/

	'aliases' => array(
		'Carbon'          => 'Carbon\Carbon',
		'Sentry'          => 'Cartalyst\Sentry\Facades\Laravel\Sentry',
		'Notification' 	  => 'Krucas\Notification\Facades\Notification',
	),

);
