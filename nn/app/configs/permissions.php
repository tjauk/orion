<?php

return array(

    // rent app
    'menu.dashboard'		=> "Show Dashboard",
    'menu.contact'			=> "Show Contacts",
    'menu.deal'				=> "Show Deals",
    'menu.company'			=> "Show Companies",
    'menu.place'			=> "Show Places",
    'menu.region'			=> "Show Regions",
    'menu.country'			=> "Show Countries",
    'menu.promocode'		=> "Show Promo Codes",
    'menu.page'				=> "Show Pages",
    'menu.menu'             => "Show Menus",
    'access.api-log'             => "Access API logs",

);
