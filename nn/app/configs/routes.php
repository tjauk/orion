<?php
Route::get('shops','Shops@index');
Route::get('shop/{slug}','Shop@view');

Route::get('products','Products@index');
Route::get('product/{slug}','Product@view');

Route::get('brands','Brands@index');
Route::get('brand/{slug}','Brand@view');

Route::get('categories','Category@index');
Route::get('category/{slug}','Category@view');

Route::get('bag/{slug}','Product@view');

Route::get('top10','Home@top10');

Route::get('top-searches','Search@topSearches');

// static pages
Route::get('about',		'Pages@about');
Route::get('contact',	'Pages@contact');
Route::get('privacy-policy',	'Pages@privacypolicy');
Route::get('terms-of-use',		'Pages@termsofuse');

// SITEMAPS
Route::get('sitemap.xml', 			 'Sitemap@index');
Route::get('sitemap/schema.xsl', 	'Sitemap@schema');
Route::get('sitemap_misc.xml',		'Sitemap@misc');
Route::get('sitemap_events.xml',	'Sitemap@events');
Route::get('sitemap_cities.xml',	'Sitemap@cities');

return array();
