<?php

class HomeController extends Controller
{

    public function boot()
    {
        Doc::view('layouts/default');
    }

    /**
     * INDEX
     */
    function index()
    {
        return view('dashboard');
    }

}
