<?php

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


// Example: orion import:hnb {domain}
class ImportColorNamesJob extends Command
{

    static $limit = 10;

    static $wait_between = 1;
    static $wait_on_error = 5;

    public $output;

    protected function configure()
    {
        $this
            ->setName('import:colornames')
            ->setDescription('Import color names with hex codes from web')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $url = 'http://www.color-hex.com/color-names.html';
        $file = ROOTDIR.'/files/color-names.html';
        $output->writeln("Downloading $url");

        // ******************************************************
        $options = [
            CURLOPT_TIMEOUT => 45,
            CURLOPT_CONNECTTIMEOUT => 45,
        ];

        if( file_exists($file) ){
            $html = file_get_contents($file);
        }else{
            $html = Remote::get($url,$options);
        }

        $dom = \Trinium\Support\Dom::make($html);

        $rows = $dom->find('table.table tbody tr');
        unset($rows[0]);
        foreach ($rows as $row) {
            $td = $row->children;
            $name = $td[0]->innertext;
            $hex = $td[2]->text;
            echo "$name $hex\n";
            $color = ColorName::register($hex,$name);
        }
        
        
        // *********************************************************************

        $output->writeln("----------------------------------");
        $output->writeln("");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');
    }


}