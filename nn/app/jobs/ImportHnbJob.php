<?php

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


// Example: orion import:hnb {domain}
class ImportHnbJob extends Command
{

    static $limit = 10;

    static $wait_between = 1;
    static $wait_on_error = 5;

    public $output;

    protected function configure()
    {
        $this
            ->setName('import:hnb')
            ->setDescription('Import currency exchange rates from HNB')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $url = 'http://www.hnb.hr/tecajn/htecajn.htm';

        // ******************************************************

        $data = Remote::get($url);
        $rows = explode("\n",$data);
        unset($rows[0]); // timestamp
        foreach ($rows as $row) {
            // 840USD001       6,298448       6,317400       6,336352
            $col = explode("       ",$row);
            $code = substr($col[0],0,3);
            $mark = substr($col[0],3,3);
            $units = substr($col[0],9,3);
            $buying_rate = floatval(str_replace(',','.',$col[1]));
            $middle_rate = floatval(str_replace(',','.',$col[2]));
            $selling_rate = floatval(str_replace(',','.',$col[3]));
            print_r($col);
            echo $middle_rate."\n";

            $currency = Currency::findByMark($mark);
            if( $currency->id>0 ){
                $currency->code = $code;
                $currency->mark = $mark;
                $currency->buying_rate = $buying_rate;
                $currency->rate = $middle_rate;
                $currency->selling_rate = $selling_rate;
                $currency->save();
            }else{
                Currency::create([
                    'code'          => $code,
                    'mark'          => $mark,
                    'buying_rate'   => $buying_rate,
                    'rate'          => $middle_rate,
                    'selling_rate'  => $selling_rate,
                ]);
            }
        }
        
        
        // *********************************************************************

        $output->writeln("----------------------------------");
        $output->writeln("");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');
    }


}