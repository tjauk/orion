<?php

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


// Example: orion nn {from}
class NNJob extends Command
{

    static $limit = 10;

    static $wait_between = 1;
    static $wait_on_error = 5;

    public $output;

    protected function configure()
    {
        $this
            ->setName('nn')
            ->setDescription('Download htmls from NN')
            ->addArgument(
                'from',
                InputArgument::OPTIONAL,
                'From id'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $options = [
            CURLOPT_TIMEOUT => 15,
            CURLOPT_CONNECTTIMEOUT => 15,
        ];

        $from_id = intval($input->getArgument('from'));
        if( empty($from_id) ){
            $from_id = 1972380;
        }
        $to_id = 81937;

        $baseurl = 'https://eojn.nn.hr/SPIN/application/ipn/DocumentManagement/DokumentPodaciFrm.aspx?id=';
        $folder = 'F:/Datasets/NN/';

        for ($i=$from_id; $i > $to_id; $i--) {
            $url = $baseurl.$i;
            $file = $folder.$i.'.html';
            if( file_exists($file) ){
                $output->writeln("Skipping $url");
            }else{
                $output->writeln("Downloading $url");
                $html = Remote::get($url,$options);
                file_put_contents($file, $html);
            }
        }

        // *********************************************************************

        $output->writeln("----------------------------------");
        $output->writeln("");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');

        // ******************************************************

/*
        if( file_exists($file) ){
            $html = file_get_contents($file);
        }else{
            $html = Remote::get($url,$options);
        }

        $dom = \Trinium\Support\Dom::make($html);

        $rows = $dom->find('table.table tbody tr');
        unset($rows[0]);
        foreach ($rows as $row) {
            $td = $row->children;
            $name = $td[0]->innertext;
            $hex = $td[2]->text;
            echo "$name $hex\n";

        }
*/


    }


}
