<?php

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


// Example: orion nn {from}
class NNParseJob extends Command
{

    static $limit = 10;

    static $wait_between = 1;
    static $wait_on_error = 5;

    public $output;

    protected function configure()
    {
        $this
            ->setName('nn:parse')
            ->setDescription('Parse NN htmls')
            ->addArgument(
                'from',
                InputArgument::OPTIONAL,
                'From id'
            )
            ->addArgument(
                'to',
                InputArgument::OPTIONAL,
                'To id'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '2048M');

        // $from_id = 2161937;
        $from_id = intval($input->getArgument('from'));
        if( empty($from_id) ){
            $from_id = 2161937;
        }
        $to_id = intval($input->getArgument('to'));
        if( empty($to_id) ){
            $to_id = 90000;
        }

        $folder = 'F:/Datasets/NN/';

        // $from_id = $to_id = 2161936;
        $skip = [];

        for ($i=$from_id; $i >= $to_id; $i--) {
            if( in_array($i, $skip) ){
                $output->writeln("Skipped $i");
                continue;
            }
            $filename = $folder.$i.'.html';
            if( file_exists($filename) and filesize($filename)<3000 ){
                unlink($filename);
                $output->writeln("Deleted $filename");

            }else if( file_exists($filename) ){
                // $output->writeln("Parsing $filename");
                $html = file_get_contents($filename);

                $dom = \Trinium\Support\Dom::make($html);

                if( !is_string($dom) ){

                    $data = [];
                    $data['nnid'] = $i;
                    $data['naslov'] = $dom->first(".HeaderInner H3")->text;

                    $data['narucitelj'] = $dom->first('#uiDokumentPodaci_uiNarucitelj')->text;
                    $data['broj_objave'] = $dom->first('#uiDokumentPodaci_uiBrojObjave')->text;
                    $data['naziv'] = $dom->first('#uiDokumentPodaci_uiNaslov')->text;

                    $data['vrsta_objave'] = $dom->first('#uiDokumentPodaci_uiVrstaObjave')->text;
                    $data['vrsta_ugovora'] = $dom->first('#uiDokumentPodaci_uiVrstaUgovora')->text;

                    $data['cpv'] = $dom->first('#uiDokumentPodaci_uiCpv')->text;

                    $data['proc_vrijednost'] = Str::toFloat($dom->first('#uiDokumentPodaci_uiProcijenjenaVrijednost')->text);

                    $data['rok_za_dostavu'] = $dom->first('#uiDokumentPodaci_uiRokZaDostavuVrijeme')->text;
                    $data['datum_objave'] = $dom->first('#uiDokumentPodaci_uiDatumObjaveRow span.form-input-label')->text;
                    $data['datum_slanja'] = $dom->first('#uiDokumentPodaci_uiDatumSlanjaRow span.form-input-label')->text;

                    $data['verzija_zakona'] = $dom->first('#uiDokumentPodaci_uiVerzijaZakona')->text;
                    $data['e_dostava'] = $dom->first('#uiDokumentPodaci_uiEtendering')->text;

                    // extras
                    $cpv = $data['cpv'];
                    // 45233129-9
                    $data['cpv_code'] = trim(substr($cpv, 0 , 10));
                    // Usluge savjetovanja na području vođenja projekta
                    $data['cpv_name'] = trim(substr($cpv, 11));

                    // normaliziraj datume
                    $data['rok_za_dostavu'] = $this->convertToDate($data['rok_za_dostavu']);
                    $data['datum_objave'] = $this->convertToDate($data['datum_objave']);
                    $data['datum_slanja'] = $this->convertToDate($data['datum_slanja']);

                    $data = array_filter($data);
                    if( empty($data) ){
                        // $output->writeln("Empty $i");
                    } else {
                        // $tender = NNTender::register($data);
                        try {
                            $tender = NNTender::create($data);
                            $output->writeln("Inserted $i");
                        } catch (\Exception $e) {
                            $output->writeln("$i already exists");
                            $skip = (array)NNTender::where('nnid','<',$i)->where('nnid','>',($i-10000))->pluck('nnid')->flatten()->toArray();
                        }


                    }
                }

            }else{
                echo '.';
                // $output->writeln("Not found $i");
            }
        }

        // *********************************************************************

        $output->writeln("----------------------------------");
        $output->writeln("");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');

        // ******************************************************

/*
        if( file_exists($file) ){
            $html = file_get_contents($file);
        }else{
            $html = Remote::get($url,$options);
        }

        $dom = \Trinium\Support\Dom::make($html);

        $rows = $dom->find('table.table tbody tr');
        unset($rows[0]);
        foreach ($rows as $row) {
            $td = $row->children;
            $name = $td[0]->innertext;
            $hex = $td[2]->text;
            echo "$name $hex\n";

        }
*/


    }


    function convertToDate($str) {
        $str = trim($str);
        if( empty($str) ){
            return null;
        }
        return date('Y-m-d',strtotime($str));
    }


}
