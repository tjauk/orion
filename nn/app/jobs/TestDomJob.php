<?php

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\DomCrawler\Crawler;

//use Trinium\Dom;
use Trinium\Support\Dom;

// Example: orion test:dom {url}
class TestDomJob extends Command
{

    public $output;

    protected function configure()
    {
        $this
            ->setName('test:dom')
            ->setDescription('Test')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        ini_set('memory_limit', '256M');


        // ******************************************************
        
        $url = 'https://www.houseoffraser.co.uk/bags-and-luggage/ted-baker-ameliee-small-east-west-tote-bag/d784410.pd#264982101';
        $link = Link::register($url);
        $html = $link->loadBody();
/*
        $dom = Dom::make( $html );

        $hrefs = $dom->map('a',function($item){
            return $item->href;
        });
        print_r($hrefs);

        echo $dom->get('.product-title span.name');
        echo "\n";
        echo $dom->get('sdf.product-title span.name','nema toga');
        echo "\n";
        exit();        
*/
        // ******************************************************

        $html = '
            <html>
                <head>
                    <title>TITLE</title>
                    <meta charset="utf-8">
                </head>
                <body>
                    <h1>H1 CONTENT</h1>
                    <img src="main.jpg" data-atr="main" />
                    <div class="klasa1">
                        <ul>
                            <li><a href="/normal-page">Link 1</a></li>
                            <li class="active"><a href="/active-page">Link 2</a></li>
                            <li class="klasa2"><a href="/first-page">Link 3</a></li>
                            <li class="klasa2"><a href="/second-page">Link 4</a></li>
                        </ul>
                        <div class="slides">
                            <img src="1.jpg" data-atr="1" />
                            <img src="2.jpg" data-atr="2" />
                            <img src="3.jpg" data-atr="3" />
                            <img src="4.jpg" data-atr="4" />
                        </div>
                    </div>
                </body>
            </html>
        ';

        $dom = Dom::make( $html );

        // test 1 - find all links - expected count 4
        $result = count( $dom->getLinks() );
        $this->test( $result==4, "\$dom->getLinks()" );

        // test 2 - find all links where none exists - expected count 0
        $result = count( $dom->getLinks('head') );
        $this->test( $result==0, "\$dom->getLinks('head')" );

        // test 3 - find first image and get src - expected "main.jpg"
        $result = $dom->first('img')->attr('src');
        $this->test( $result=="main.jpg", "\$dom->first('img')->attr('src')" );

        // test 4 - find first image in .slides and get data-atr - expected "1"
        $result = $dom->first('.slides img')->attr('data-atr');
        $this->test( $result=="1", "\$dom->first('.slides img')->attr('data-atr')" );

        // test 5 - find image src inside '.klasa1 ul' - expected ""
        $result = $dom->first('.klasa1 ul img')->attr('src');
        $this->test( $result=="", "\$dom->first('.klasa1 ul img')->attr('src')" );

        // test 6 - find second link and get content - expected "Link 2"
        $result = $dom->find('a',1)->text;
        $this->test( $result=="Link 2", "\$dom->find('a',1)->text" );

        // test 7 - find all images inside .slides and extract data-atr - expected ["1","2","3","4"]
        $result = [];
        $imgs = $dom->find('.slides img');
        foreach ($imgs as $img) {
            $result[] = $img->attr('data-atr');
        }
        $this->test( $result==["1","2","3","4"], "\$dom->find('a',1)->text" );

        // test 8 - find nonexisting element src - expected ""
        $result = $dom->first('notag')->attr('src');
        $this->test( $result=="", "\$dom->first('notag')->attr('src')" );

        // test 9 - find all with map function
        $result = $dom->map('a',function($item){
            return $item->href;
        });
        $this->test( $result==['/normal-page','/active-page','/first-page','/second-page'], "\$dom->map('a',function(\$item){
            return \$item->href;
        })" );


        //print_r($dom->first('.slick-current .carousel-main-img')->attr('data-large'));
        exit();

        //print_r($dom->getLdJson('Product'));

        
        //print_r($dom->getLinks('.panel-body'));

        $price = $dom->get('.product-price .price');
//€375.00

        $number = $dom->getNumber('.product-price .price');
// 375

exit($number);

        // ******************************************************


        // *********************************************************************

        $output->writeln("----------------------------------");
        $output->writeln("");
        $output->writeln('Peak memory: '.(memory_get_peak_usage()/1024/1024));
        $output->writeln('DONE IN '.round(microtime(true)-ORION_STARTED,2).' seconds');
    }

    static $testnr = 0;
    public function test( $test, $expression="" ){
        echo (++static::$testnr).". ".$expression."\n";
        echo ($test ? "PASS" : "FAIL")."\n";
        echo "\n";
    }


}