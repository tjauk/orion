<?php

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
//use Trinium\Dom;
use Trinium\Support\Dom;

// Example: orion test:dom {url}
class TestLSDJob extends Command
{
    public $output;

    protected function configure()
    {
        $this
            ->setName('test:lsd')
            ->setDescription('Laravel Schema Designer')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '512M');

        // ******************************************************
        $links = [];
        $pages = range(1, 128);
        /*
                foreach ($pages as $page_number) {
                    $url = 'https://www.laravelsd.com/browse?page='.$page_number;
                    $html = Remote::get($url);

                    $dom = \Trinium\Support\Dom::make($html);
                    if ($dom) {
                        $rows = $dom->find('table.table tbody tr');
                        unset($rows[0]);
                        foreach ($rows as $row) {
                            $td = $row->children;
                            $cell = \Trinium\Support\Dom::make($td[0]->innertext);
                            $name = $cell->first('a')->text;
                            $link = $cell->first('a')->href;
                            echo "$name - $link \n";
                            $links[] = [
                                'name' => $name,
                                'link' => $link,
                            ];
                        }
                    }
                }

                file_put_contents('lsd.json', json_encode($links));
        */
        // ******************************************************

        $links = json_decode(file_get_contents('lsd.json'));
        $txt = '';
        foreach ($links as $link) {
            $txt .= $link->link."\n";
        }
        file_put_contents('links.txt', $txt);
        exit();
        print_r($links);
        foreach ($links as $link) {
            $zipfile = Remote::get($link->link.'/export/schema'); //model
            file_put_contents('dw/'.$link->name.'.zip', $zipfile);
            echo 'Saved '.'dw/'.$link->name.'.zip'."\n";
        }

        // *********************************************************************

        $output->writeln('----------------------------------');
        $output->writeln('');
        $output->writeln('Peak memory: '.(memory_get_peak_usage() / 1024 / 1024));
        $output->writeln('DONE IN '.round(microtime(true) - ORION_STARTED, 2).' seconds');
    }
}
