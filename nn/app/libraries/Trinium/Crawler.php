<?php
namespace Trinium;

use Symfony\Component\DomCrawler\Crawler as SymfonyCrawler;

class Crawler implements \Iterator, \Countable
{
    /**
     * Wrapped Symfony crawler object
     *
     * @var SymfonyCrawler
     */
    protected $symfonyCrawler;

    /**
     * Hold the URI as the Symfony Crawler does not provide
     * getters for it
     *
     * @var string
     */
    protected $uri;

    /**
     * Constructor.
     *
     * @param mixed  $node  A Node to use as the base for the crawling
     * @param string $uri   The current URI or the base href value
     */
    public function __construct($node = null, $uri = null)
    {
        $this->symfonyCrawler = new SymfonyCrawler($node, $uri);
        $this->uri = $uri;
    }

    /**
     * Magic methods proxy to the crawler
     *
     * @param string $name      method name
     * @param array  $arguments list of arguments
     *
     * @return mixed
     */
    public function __call($name, array $arguments = [])
    {
        $callable = [$this->symfonyCrawler, $name];
        if (!is_callable($callable)) {
            throw new \BadMethodCallException(sprintf(
                'Method %s is undefined!',
                $name
            ));
        }

        $temp = call_user_func_array($callable, $arguments);
        if (!$temp instanceof SymfonyCrawler) {
            return $temp;
        }

        $result = new self(null, $this->uri);
        foreach ($temp as $node) {
            $result->add($node);
        }

        return $result;
    }

    /**
     * Return the current element
     *
     * @return Crawler
     * @see http://php.net/manual/en/iterator.current.php
     */
    public function current()
    {
        $domNode = $this->symfonyCrawler->current();

        return new self($domNode, $this->uri);
    }

    /**
     * Return the key of the current element
     *
     * @return scalar
     * @see http://php.net/manual/en/iterator.key.php
     */
    public function key()
    {
        return $this->symfonyCrawler->key();
    }

    /**
     * Move forward to next element
     *
     * @return void
     * @see http://php.net/manual/en/iterator.next.php
     */
    public function next()
    {
        $this->symfonyCrawler->next();
    }

    /**
     * Rewind the Iterator to the first element
     *
     * @return void
     * @see http://php.net/manual/en/iterator.rewind.php
     */
    public function rewind()
    {
        $this->symfonyCrawler->rewind();
    }

    /**
     * Checks if current position is valid
     *
     * @return boolean
     * @see http://php.net/manual/en/iterator.valid.php
     */
    public function valid()
    {
        return $this->symfonyCrawler->valid();
    }

    /**
     * Count the items
     *
     * @return int
     * @see http://php.net/manual/en/class.countable.php
     */
    public function count()
    {
        return $this->symfonyCrawler->count();
    }
}