<?php
namespace Trinium\Crawler;

use \Trinium\Cache;

use \LayerShifter\TLDExtract\Extract;


/**
 * Remote
 *
 * A bit more specific implementation of curl wrapper
 *
 * @package Trinium
 * @author Tihomir Jauk
 * @copyright 2016
 * @version 2016-12-05
 * @access public
 * @uses \Trinium\Remote, curl
 */

/* ******************************************************************************* */

/* method list

Remote::get( $url, array $options = NULL )
...
The rest of methods are extended from \Trinium\Remote class

*/

/* ******************************************************************************* */

class Remote extends \Trinium\Remote
{

    /**
    * Default curl options
    */
    public static $default_options = array( 
        CURLOPT_USERAGENT => 'Mozilla/5.0 (compatible; Crawler +http://www.google.com/)',
        CURLOPT_CONNECTTIMEOUT => 10,
        CURLOPT_TIMEOUT => 10,
    );


    /**
     * Returns the output of a remote URL. Any [curl option](http://php.net/curl_setopt)
     * may be used.
     *
     * <code>
     *     // Do a simple GET request
     *     $data = Remote::get($url);
     *
     *     // Do a POST request
     *     $data = Remote::get($url, array(
     *         CURLOPT_POST       => TRUE,
     *         CURLOPT_POSTFIELDS => http_build_query($array),
     *     ));
     *     // Do a POST request v2
     *     $data = Remote::post($url, $keys_values);
     * </code>
     *
     * @param   string   remote URL
     * @param   array    curl options
     * @return  string
     */
    public static function get( $url, array $options = NULL )
    {
        if( is_string($url) ){
            $url = \URI::parse($url);
        }

        if( $options === NULL )
        {
            // Use default options
            $options = static::$default_options;
        } else {
            // Add default options
            $options = $options + static::$default_options;
        }
        

        // Initialize session and set URL.

        $start = microtime(TRUE);

        // Open a new remote connection
        $remote = curl_init( $url['original'] );


        // Setup headers - I used the same headers from Firefox version 2.0.0.6
        // below was split up because php.net said the line was too long. :/
        $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Keep-Alive: 300";
        $header[] = "Accept-Charset: ISO-8859-1,ISO-8859-2,utf-8;q=0.7,*;q=0.7";
        $header[] = "Accept-Language: en-us,en;q=0.5";
        $header[] = "Pragma: "; // browsers keep this blank.
        $options[CURLOPT_HTTPHEADER] = $header;

        $options[CURLOPT_USERAGENT] = 'Googlebot/2.1 (+http://www.google.com/bot.html)';
        $options[CURLOPT_REFERER] = 'http://www.google.com';
        $options[CURLOPT_AUTOREFERER] = TRUE;
        $options[CURLOPT_ENCODING] = 'gzip,deflate';
        $options[CURLOPT_REFERER] = 'http://www.google.com';

        // The transfer must always be returned
        $options[CURLOPT_RETURNTRANSFER] = TRUE;

        // Follow redirects
        $options[CURLOPT_FOLLOWLOCATION] = TRUE;

        // Get headers
        $options[CURLOPT_VERBOSE] = FALSE;
        $options[CURLOPT_HEADER] = TRUE;

        // Use Cookies
        $cookie_jar_file = CACHEDIR.'/globals/cookie_jar_'.$url['host'].'.txt';
        $options[CURLOPT_COOKIEJAR] = $cookie_jar_file;
        $options[CURLOPT_COOKIEFILE] = $cookie_jar_file;

        if( $url['scheme'] == 'https' ){
            // if this fails, you need to manually download certificate for site
            $options[CURLOPT_SSL_VERIFYPEER] = FALSE;
            $options[CURLOPT_SSL_VERIFYHOST] = 2;
            $certificate = APPDIR.'/certificates/'.$url['host'].'.crt';
            if( file_exists($certificate) ){
                $options[CURLOPT_CAINFO] = $certificate;
            }else{
                exit("No certificate for host {$url['host']}");
            }
        }

        // Set connection options
        if( !curl_setopt_array( $remote, $options ) ) {
            return false;
        }
            
        // Get the response and close the channel.
        $response = curl_exec($remote);

        // Get the http status code response information
        $code = curl_getinfo( $remote, CURLINFO_HTTP_CODE );

        $error = null;
        if( $code AND $code < 200 OR $code > 299 ) {
            $error = $response;

        } elseif( $response === FALSE ) {
            $error = curl_error( $remote );
        }

        // Get mime content type from headers
        $mime = curl_getinfo($remote, CURLINFO_CONTENT_TYPE);

        // Get all header information
        $header_size = curl_getinfo($remote, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);

        // Close the connection
        curl_close($remote);

        $time = microtime(TRUE)-$start;

        // TO-DO: Create response object
        $response = new \StdClass;
        $response->body     = $body;
        $response->status   = $code;
        $response->error    = $error;
        $response->headers  = $headers;
        $response->mime     = $mime;
        $response->time     = $time; // Load response time / TTLB + DW

        if( isset( $error ) ){
            $response->error  = $error;
        }

        return $response;
    }


    /**
     * Returns the output of a remote URL.
     *
     * Any [curl option](http://php.net/curl_setopt)
     * may be used.
     *
     * <code>
     *     // Do a POST request v2
     *     $data = Remote::post($url, $keys_values);
     * </code>
     *
     * @param   string   remote URL
     * @param   array    curl options
     * @return  string
     */
    public static function post( $url, $keys )
    {
        $data = \Trinium\Remote::get( $url, array( 
                        CURLOPT_POST => TRUE, 
                        CURLOPT_POSTFIELDS => http_build_query( $keys ),
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_FOLLOWLOCATION => TRUE
                    )
                );
        return $data;
    }


    /**
     * Returns phantomjs rendered page output of a remote URL.
     *
     * <code>
     *     $data = Remote::render($url);
     * </code>
     *
     * @param   string   remote URL
     * @param   array    curl options
     * @return  string
     */
    public static function render( $url )
    {
        $client = \JonnyW\PhantomJs\Client::getInstance();
        $client->getEngine()->setPath(realpath('C:\wamp64\bin\phantomjs\bin\phantomjs.exe'));
        
        $request = $client->getMessageFactory()->createRequest($url, 'GET');

        $response = $client->getMessageFactory()->createResponse();

        // Send the request
        $client->send($request, $response);

        //if($response->getStatus() === 200) {
            // Dump the requested page content
            return $response->getContent();
        //}
    }


    public static function rendercmd( $url, $keys=[] )
    {
        $cmd = [];
        $cmd[] = "phantomjs";
        $cmd[] = "\"" . realpath(SYSDIR.'/resources/savepage.js') . "\"";
        $cmd[] = "\"" . $url . "\"";
        //$cmd[] = ">";
        $cmd[] = "temp.html";
        /*
        */
        exec( implode( " ", $cmd ), $output, $result );

        //file_put_contents('temp.html', implode("\n",$output));
        return implode("\n",$output);
        return file_get_contents("temp.html");
    }

}