<?php
namespace Trinium\Crawler;

use \Trinium\Cache;
use \LayerShifter\TLDExtract\Extract;

class URL {

    public static $last_url;

    /* example:
    host http|https
    port 80
    path /folder/slug
    folder /folder/
    query q=123&key=val
    data [q=>123,key=>val]
    fragment home (from #home)
    domain test.com
    subdomain www
    tld com
    original http://www.test.com/folder/slug?q=123&key=val
    */
	static function parse( $str )
    {
        if( is_array($str) ){
            static::$last_url = $str;
            return $str;
        }

        $url = parse_url($str);
        $url['original'] = $str;

        if( !isset($url['host']) ){
            $url = parse_url('http://'.$str);
            $url['original'] = 'http://'.$str;
        }
        
        if( !isset($url['port']) ){
            $url['port'] = 80;
        }
        
        if( isset($url['host']) ){
            // https://github.com/layershifter/TLDExtract
            $cache_key = 'tldinfo-'.$url['host'].'.json';
            if( Cache::has($cache_key) ){
                list($domain,$subdomain,$tld) = Cache::get($cache_key);

            }else{
                $extract = new Extract();
                $result = $extract->parse($url['host']);
                
                $domain     = $result->getRegistrableDomain();
                $subdomain  = $result->subdomain;
                $tld        = $result->suffix;
                
                Cache::put($cache_key, [ $url['domain'], $url['subdomain'], $url['tld'] ] ,60*24*7);
            }

            $url['domain']      = $domain;
            $url['subdomain']   = $subdomain;
            $url['tld']         = $tld;
        }

        if( isset($url['query']) ){
            parse_str($url['query'], $url['data']);
        }
        
        $url['full'] = $url['original'];

        // folder
        if( !empty($url['path']) ){
            $arr = explode('/',$url['path']);
            unset($arr[count($arr)-1]);
            $url['folder'] = implode('/', $arr);
        }

        /*
        print_r($url);
        exit();
        */

        static::$last_url = $url;
        return $url;
    }


    static function normalize( $rel, $base )
    {
        $base = static::parse($base);
        
        $rel = trim($rel);

        $parts = explode(':',$rel);
        if( in_array(strtolower($parts[0]),['http','https','ftp']) ){
            return $rel;
        }

        if( substr($rel,0,2)=='//' ){
            $rel = $base['scheme'].':'.$rel;
        }else if( substr($rel,0,1)=='/' ){
            $rel = $base['scheme'].'://'.$base['host'].$rel;
        }
        return $rel;
    }


    static function hasSameProperty( $prop, $first, $second )
    {
        $first = static::parse($first);
        $second = static::parse($second);
        return ($first[$prop]==$second[$prop]) ? true : false;
    }


    static function hasSameScheme( $first, $second )
    {
        return static::hasSameProperty( 'scheme', $first, $second );
    }

    static function hasSameHost( $first, $second )
    {
        return static::hasSameProperty( 'host', $first, $second );
    }

    static function hasSamePath( $first, $second )
    {
        return static::hasSameProperty( 'path', $first, $second );
    }

    static function hasSameDomain( $first, $second )
    {
        return static::hasSameProperty( 'domain', $first, $second );
    }

    static function hasSamePort( $first, $second )
    {
        return static::hasSameProperty( 'port', $first, $second );
    }


    
}