<?php
namespace Trinium\Worker;

class Job
{

    public $args = [];
    public $data = [];
    public $result = [];

    public function args($args)
    {
        $this->args = $args;
        return $this;
    }

    public function data($data)
    {
        $this->data = $data;
        return $this;
    }
    
    public function run()
    {
        $this->result = true;
        return $this;
    }

    public function result($result=null)
    {
        if( !empty($result) ){
            $this->result = $result;
            return $this;
        }
        return $this->result;
    }

    /*
     * Helpers
     */

    public function some_help()
    {

    }

    public function write($str)
    {
        echo $str;
    }
    
    public function writeln($str)
    {
        echo $str."\n";
    }
    

}
