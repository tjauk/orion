<?php
namespace Trinium\Worker\Jobs;

use Trinium\Carbon;

use Trinium\Crawler\Remote;

use Symfony\Component\DomCrawler\Crawler;

use \URI as URI;
use \Site as Site;
use \Link as Link;


class CrawlUrls extends \Trinium\Worker\Job
{
	public $args = [];
    public $data = [];
    public $result = [];


    /*
     * RUN
     */
    public function run()
    {
    	$urls = $this->args;
        $this->writeln(" ");
        foreach ($urls as $url) {
            
            $this->writeln(date('Y-m-d H:i:s'));
            $this->writeln("GET $url");

            $skip = false;
            $hash = md5($url);
            
            $response = Remote::get($url);
            $ttfb = $response->time;
	        $status = $response->status;
	        $crawled_at = date('Y-m-d H:i:s');
            
            $msg = '';
            $body = '';

            if( (int)$response->status==0 ){
	            $msg = "No connection? Waiting 5 seconds...";
	            sleep(5);

	        }else if( (int)$response->status >=400 and (int)$response->status<600 ){
	            $msg = "Response status {$response->status}, saving and moving on";
	            //$skip = true;

	        }else if( $response->status !== 200 ){
	            $msg = "Response status {$response->status}, skipping";
	            //$skip = true;
	        }



	        if( empty($response->body) ){
	            $msg = "Empty response body, skipping";
	            $skip = true;
	        }


	        if( $skip==false ){
	            $body = $response->body;
                // extract urls
                // extract web links from [a href=]
                // TO-DO check for more attributes like "nofollow"
                $html = new Crawler($body);
                $hrefs = $html->filter('a')->each(function (Crawler $node, $i) {
                    return trim($node->attr('href'));
                });
                $hrefs = array_unique($hrefs);
                $length = strlen($body);

                /*
                // extract web links only for current domain
                foreach($hrefs as $index=>$href){
                    $remove = false;
                    if( !empty($href) ){
                    }
                }
                */
                $msg = "Response: $status  Length: $length  Time: $ttfb  Links: ".count($hrefs)." ";

                // save all
                $result = [
                    'url'       => $url,
                    'hash'      => $hash,
                    //'msg'       => $msg,
                    'status'    => $status,
                    'crawled_at'=> $crawled_at,
                    'ttfb'      => $ttfb,
                    'body'      => $body,
                    'hrefs'     => $hrefs,
                ];
                $this->result[] = $result;

                
	        }
            
            $this->writeln($msg);
        }
    }



    /*
     * POST PROCESS ON MASTER
     */
    public function postprocess()
    {
        $msg = '';
        $hrefs = [];

    	foreach($this->result as $result){

    		// find corresponding link
    		$link = Link::register($result['url'],null,false);

    		// save additional info
    		$link->status = $result['status'];
    		$link->crawled_at = $result['crawled_at'];
    		$link->ttfb = $result['ttfb'];
    		$link->save();

    		$site = $link->site;

    		// save body
    		$link->saveBody($result['body']);

    		// append hrefs
            if( is_array($result['hrefs']) and $link->status==200 ){
                foreach($result['hrefs'] as $href){
                    $abs = $link->toAbsoluteUrl($href); // can return false
                    $msg.= "ABS:".$abs."\n";
                    if( $abs ){
                        // apply site filters
                        if( $site->filterValid($abs) ){
                            $hrefs[] = $abs;
                        }else{
                            $msg.= "INVALID";
                        }
                    }
                }
            }
    	}

        // save new links
        $hrefs = array_unique($hrefs);
        $start_time = time();
        foreach($hrefs as $url){
            //$msg.= "NEW:".$url."\n";
            Link::register($url);
        }
        $exec_time = time()-$start_time;
        $msg.= "Written in $exec_time seconds"."\n";        
        $msg.= "Found ".count($hrefs)." unique links"."\n";
        return $msg;
    }

}
