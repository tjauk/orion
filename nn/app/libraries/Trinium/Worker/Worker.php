<?php
namespace Trinium\Worker;

use \Trinium\Crawler\Remote as Remote;

class Worker
{
    public $endpoint = 'http://hunt.app/comm';

    public $name = null;
    public $uid = 0;
    public $pid = 0;
    public $threads = 2;

    public $signal = null;
    public $status = 'created';// created,started,running,sending,idle,waiting,stoping,stopped

    public $job = null;
    public $data = [];

    public $queue = [];


    public function __construct($name=null)
    {
        if( !is_null($name) ){
            $this->name = $name;
        }
        if( empty($this->name) ){
            $this->name = 'w'.substr(rand(9999,99999),0,4);
        }
        $this->status = 'starting';
    }

    /*
     * Methods
     */


    // start worker, ask for job if none
    public function start()
    {
        $this->status = 'started';

        $response = Remote::get($this->endpoint.'/getjob/'.$worker);
        //print_r($response);
        if( $response->status == 200 ){
            $json = json_decode($response->body,true);
            if( !empty($json['action']) ){
                // prep job
                $action = $json['action']['name']; // CrawlUrls
                $class = "\\Trinium\\Worker\\Jobs\\".$action;
                $job = new $class;
                $job->args($json['action']['args']);
                // add to queue
                $this->queue[] = $job;

            }else{
                $json = [];
            }
        }
    }


    // run job
    public function run()
    {
        $this->status = 'running';

        //
        if( !empty($this->queue) ){
            $this->job = array_shift($this->queue);
            $this->job->run();
        }else{
            $this->job = null;
        }
        //

        $this->status = 'finished';
    }


    // send results to master unit
    public function send()
    {
        if( !is_null($this->job) ){
            $this->status = 'sending';
            $result = $this->job->result();
            $post = [
                'worker'    => $this->name,
                'job'       => 'CrawlUrls',
                'result'    => $result
            ];

            $url = $this->endpoint.'/jobdone/'.$this->name;

            echo "Sending results"."\n";

            $options[CURLOPT_RETURNTRANSFER] = TRUE;
            $options[CURLOPT_POST] = TRUE;
            $options[CURLOPT_POSTFIELDS] = http_build_query( $post );
            $options[CURLOPT_FOLLOWLOCATION] = TRUE;

            $remote = curl_init( $url );
            curl_setopt_array( $remote, $options );
            $response = curl_exec( $remote );
            $code = curl_getinfo( $remote, CURLINFO_HTTP_CODE );

            //$response = \Trinium\Remote::post($this->endpoint.'/workdone/'.$worker,$post);
            $json = json_decode($response,true);

            
            
            if( $json['success']==true ){
                $this->status = 'sent';
                echo substr($json['message'],0,50000)."\n";
            }else{
                $this->status = 'stopped';
            }
            print_r($response);

        }
    }


    // stop worker and close script
    public function stop()
    {
        $this->status = 'stopping';
    }


    // still running ?
    public function notStopped()
    {
        return !in_array($this->status, ['stopping','stopped']);
    }


    // internal log
    public function log()
    {

    }

}
