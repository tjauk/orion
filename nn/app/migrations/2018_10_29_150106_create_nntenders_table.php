<?php

class CreateNntendersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nntenders', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->integer('nnid')->unsigned();

            $table->string('naslov')->default('');
            $table->string('narucitelj')->default('');
            $table->string('broj_objave')->default('');
            $table->string('naziv')->default('');

            $table->string('vrsta_objave')->default('');
            $table->string('vrsta_ugovora')->default('');

            $table->string('cpv')->default('');

            $table->double('proc_vrijednost')->nullable();

            $table->string('rok_za_dostavu')->default('');
            $table->string('datum_objave')->default('');
            $table->string('datum_slanja')->default('');

            $table->string('verzija_zakona')->default('');

            $table->string('e_dostava')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nntenders');
    }

}
