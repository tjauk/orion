<?php

class AddFieldsToNntenders extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nntenders', function($table)
        {
            $table->string('cpv_code')->default('');
            $table->string('cpv_name')->default('');
            $table->string('cpv_root')->default('');
            $table->string('cpv_main')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nntenders', function($table)
        {
            $table->dropColumn('cpv_code');
            $table->dropColumn('cpv_name');
            $table->dropColumn('cpv_root');
            $table->dropColumn('cpv_main');
        });
    }

}
