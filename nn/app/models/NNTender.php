<?php
class NNTender extends Model
{
    protected $table = 'nntenders';

    protected $fillable = [
        'nnid',
        'naslov',
        'narucitelj',
        'broj_objave',
        'naziv',
        'vrsta_objave',
        'vrsta_ugovora',
        'cpv',
        'proc_vrijednost',
        'rok_za_dostavu',
        'datum_objave',
        'datum_slanja',
        'verzija_zakona',
        'e_dostava',
        'cpv_code',
        'cpv_name',
        'cpv_root',
        'cpv_main',
    ];

    /*
     * Relations
     */


    /*
     * Options
     */

    static function register($data)
    {
        $nnid = $data['nnid'];
        if( $nnid>0 ){
            $tender = static::findByNnid($nnid);
            if( $tender->id > 0 ){
                // skip, no need to update for now
            }else{
                $tender = static::insert($data);
            }
        }
        return $tender;
    }


    /*
     * Scopes
     */


    /*
     * Getters
     */


    /*
     * Setters
     */


}
