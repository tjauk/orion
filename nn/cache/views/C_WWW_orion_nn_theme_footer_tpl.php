<?php /* ver: 2018-10-29 19:35:33 */
?><footer>
  <div class="container">

    <div class="col-md-4">
      <ul>
        <li><a href="/about">About</a></li>
        <li><a href="/terms-of-use">Terms of Use</a></li>
        <li><a href="/contact">Contact</a></li>
        <li><a href="/privacy-policy">Privacy Policy</a></li>
      </ul>
    </div>

  </div>
</footer>
