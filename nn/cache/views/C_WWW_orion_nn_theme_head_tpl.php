<?php /* ver: 2018-10-29 19:30:16 */
?><script async src="https://www.googletagmanager.com/gtag/js?id=UA-119067227-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-119067227-1');
</script>

<?php $inc_1646812771=\Trinium\View::inc("hotjar");include("$inc_1646812771"); ?>

<title>{{doc.title}}</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php
/*
<!-- REMOVE BEFORE LAUNCH -->
<meta name="robots" content="noindex, nofollow">
<!-- REMOVE BEFORE LAUNCH -->
*/
?>

{{doc.meta}}

<link href="https://fonts.googleapis.com/css?family=Nunito:300,400&amp;subset=latin-ext" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link rel="stylesheet" href="/theme/css/bootstrap.min.css">
<link rel="stylesheet" href="/theme/font-awesome-4.7.0/css/font-awesome.min.css">

<?php \Trinium\Doc::less("styles.less"); ?>

<link rel="stylesheet" href="/theme/app.css">
<link rel="icon" type="image/png" href="/theme/favicon.png" />
{{doc.css}}

<?php jquery(); ?>
{{doc.js}}
