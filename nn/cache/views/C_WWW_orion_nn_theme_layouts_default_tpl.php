<?php /* ver: 2018-10-29 19:30:16 */
?><!DOCTYPE html>
<html>
<head>
<?php $inc_225336351=\Trinium\View::inc("head");include("$inc_225336351"); ?>
</head>
<body>

	<div id="app">

		<!-- header -->
<?php $inc_1077734687=\Trinium\View::inc("header");include("$inc_1077734687"); ?>

		<!-- main -->
	  <div id="content" class="container">
			<?php echo $CONTENT;?>
		</div>

		<!-- footer -->
<?php $inc_1573007174=\Trinium\View::inc("footer");include("$inc_1573007174"); ?>

	</div>

	<!-- end scripts -->
  <script type="text/javascript" src="/theme/app.js"></script>

<?php $inc_1423246249=\Trinium\View::inc("google-analytics");include("$inc_1423246249"); ?>

	{{doc.jsend}}
</body>
</html>
