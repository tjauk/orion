<?php /* ver: 2018-10-29 19:30:16 */
View::filter("e_filter");
?><?php
$query = Request::get('q');
?>
<form class="navbar-form search-top" role="search" action="/search">
  <div class="input-group" style="width: 100%">
<input name="q" id="top-search-input" type="text" class="form-control" autocomplete="off" placeholder="What are you shopping for?" value="<?php echo e_filter(strip_tags($query));?>">
    <div class="input-group-btn">
        <button class="btn btn-search" type="submit"><i class="fa fa-search"></i></button>
    </div>
  </div>
</form>
