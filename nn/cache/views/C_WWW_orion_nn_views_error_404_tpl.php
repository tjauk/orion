<?php /* ver: 2018-10-29 19:30:17 */
?><h1>Ooops! Page not found</h1>

<h3>
It looks like this page no longer exists.<br>
Would you like to go to <a href="/">homepage</a> instead?
</h3>

<small><a href="https://en.wikipedia.org/wiki/HTTP_404" target="_blank">Error 404 - Page not found</a></small>