@asset "ace"
<?php Doc::title('Crawler editor'); ?>

<div class="crawl editor row">

	<div class="col-md-2">
		<?php echo Form::select('parser')->values(['_urls','discover','product'])->addClass('form-control'); ?>
	</div>
	<div class="col-md-10">
		<input style="float:left;width:90%" type="text" name="url" class="form-control" value="{{url}}" placeholder="Enter URL">
		<a style="float:left;width:9%" onclick="openUrl()" id="open_url" target="_blank" class="btn btn-primary">Open URL</a>
	</div>

	<div class="col-md-12">
	</div>

</div>


<div class="row">
	<div id="col1" class="col-md-6">
		<button class="btn btn-primary" name="btnLoad">Load</button>
		<button class="btn btn-primary" name="btnSave">Save</button>
		<button class="btn btn-warning" name="btnBust">Bust</button>
		<button class="btn btn-warning" name="btnShow">Show Html</button>
		<pre id="editor" class="form-control" style="min-height: 640px"><?php echo htmlentities($code); ?></pre>
		<input type="hidden" name="code">
	</div>
	
	<div id="col3" class="col-md-3" style="height:100%">
		<button class="btn btn-primary" name="btnBody">Body</button>
		<iframe src="" id="iframe"></iframe>
	</div>

	<div id="col2" class="col-md-3">
		<button class="btn btn-primary" name="btnParse">Parse</button>
		<div id="messages"></div>
		<div id="result">
			<pre><code><?php pr($data);?></code></pre>
		</div>
	</div>


</div>


<script>
	var cols = [6,4,2];
	// init ace
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/twilight");
    editor.session.setMode("ace/mode/php");

    document.getElementById('editor').style.fontSize='14px';

    var heightUpdateFunction = function(){

        // http://stackoverflow.com/questions/11584061/
        var newHeight =
                  editor.getSession().getScreenLength()
                  * editor.renderer.lineHeight
                  + editor.renderer.scrollBar.getWidth();

        $('#editor').height(newHeight.toString() + "px");
        $('#editor-section').height(newHeight.toString() + "px");

        // This call is required for the editor to fix all of
        // its inner structure for adapting to a change in size
        editor.resize();
    };

    // Set initial size to match initial content
    //heightUpdateFunction();

    // Whenever a change happens inside the ACE editor, update
    // the size again
    //editor.getSession().on('change', heightUpdateFunction);


    editor.getSession().on('change', function(e) {
	    // e.type, etc
	    parse();
	});

    //

    (function($){
	    $.fn.extend({
	        getFullPath: function(stopAtBody){
	            stopAtBody = stopAtBody || false;
	            function traverseUp(el){
	                var result = el.tagName + ':eq(' + $(el).index() + ')',
	                    pare = $(el).parent()[0];
	                if (pare.tagName !== undefined && (!stopAtBody || pare.tagName !== 'BODY')){
	                    result = [traverseUp(pare), result].join(' ');
	                }                
	                return result;
	            };
	            return this.length > 0 ? traverseUp(this[0]) : '';
	        }
	    });
	})(jQuery);

	//
	function openInNewTab(url) {
	  var win = window.open(url, '_blank');
	  win.focus();
	}

	function openUrl(){
		var url = $('input[name=url]').val();
		$('#open_url').attr('src',url);
		openInNewTab(url);
	}

	function load(){
		var parser = $('select[name=parser]').val();
		var url = $('input[name=url]').val();
		$.ajax({
			url: '/admin/crawl/loadfx',
			type: 'POST',
			dataType: 'json',
			data: {url:url,parser:parser},
		})
		.done(function(json) {
			editor.setValue(json.code);
			parse();
		});
	}

	function save(){
		var parser = $('select[name=parser]').val();
		var url = $('input[name=url]').val();
		var code = editor.getValue();
		$.ajax({
			url: '/admin/crawl/savefx',
			type: 'POST',
			dataType: 'json',
			data: {url:url,parser:parser,code:code},
		})
		.done(function(json) {
			//editor.setValue(json.code);
		});
	}

	function useUrl(el){
		var url = $(el).attr('href');
		$('input[name=url]').val(url);
		$('#open_url').attr('src',url);
	}

	function linkify(text){
    if (text) {
      text = text.replace(
        /((https?\:\/\/)|(www\.))(\S+)(\w{2,4})(:[0-9]+)?(\/|\/([\w\d#!:.?+=&%@!\-\/]))?/gi,
        function(url){
          var full_url = url;
          if (!full_url.match('^https?:\/\/')) {
            full_url = 'http://' + full_url;
          }
          return '<a onclick="useUrl(this)" href="' + full_url + '">' + url + '</a>';
        }
      );
    }
    return text;
	}


	function parse(){
		var parser = $('select[name=parser]').val();
		var url = $('input[name=url]').val();
		var code = editor.getValue();
		$.ajax({
			url: '/admin/crawl/parse',
			type: 'POST',
			data: {url:url,parser:parser,code:code},
		})
		.done(function(json) {
			displayJson(json)
			//editor.setValue(json.code);
		});
	}

	function bust(){
		var url = $('input[name=url]').val();
		$.ajax({
			url: '/admin/crawl/bust',
			type: 'POST',
			data: {url:url},
		})
		.done(function(json) {
			displayJson(json);
		});
	}

	function showHtml(){
		var url = $('input[name=url]').val();
		$.ajax({
			url: '/admin/crawl/showhtml',
			type: 'POST',
			data: {url:url},
		})
		.done(function(json) {
			displayJson(json);
		});
	}

	function body(){
		var url = $('input[name=url]').val();
		var enc = encodeURIComponent(url);
		$('#iframe').attr('src','/admin/crawl/body/?url='+enc);
		/*
		$('#iframe').contents().find("body").delegate('a', 'click', function(e){ 
		  console.log(e);
		});
		*/
	}

	function syntaxHighlight(json) {
	    if (typeof json != 'string') {
	         json = JSON.stringify(json, undefined, 2);
	    }
	    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
	    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
	        var cls = 'number';
	        if (/^"/.test(match)) {
	            if (/:$/.test(match)) {
	                cls = 'key';
	            } else {
	                cls = 'string';
	            }
	        } else if (/true|false/.test(match)) {
	            cls = 'boolean';
	        } else if (/null/.test(match)) {
	            cls = 'null';
	        }
	        return '<span class="' + cls + '">' + match + '</span>';
	    });
	}

	$('body').on('click', '#a_url', function(event) {
		event.preventDefault();
		$(this).attr('href') = $('input[name=url]').val();
	});

	$('body').on('click', 'button[name=btnLoad]', function(event) {
		event.preventDefault();
		load();
	});

	$('body').on('click', 'button[name=btnSave]', function(event) {
		event.preventDefault();
		save();
	});

	$('body').on('click', 'button[name=btnBust]', function(event) {
		event.preventDefault();
		bust();
	});

	$('body').on('click', 'button[name=btnShow]', function(event) {
		event.preventDefault();
		showHtml();
	});

	var parseInterval = 0;
	$('body').on('click', 'button[name=btnParse]', function(event) {
		event.preventDefault();
		var parseDelay = 50;
		parseInterval = setInterval(function(){
			if( parseDelay<0 ){
				parse();
				clearInterval(parseInterval);
			}
			parseDelay--;
		}, 10);
	});

	$('body').on('click', 'button[name=btnBody]', function(event) {
		event.preventDefault();
		body();
	});

	//

	$('body').on('click', '#col1', function(event) {
		event.preventDefault();
		arrangeColumns(1);
	});
	$('body').on('click', '#col2', function(event) {
		event.preventDefault();
		arrangeColumns(2);
	});
	$('body').on('click', '#col3', function(event) {
		event.preventDefault();
		arrangeColumns(3);
	});


	function arrangeColumns(nr){
		if( nr==1 ){
			$('#col1').removeClass().addClass('col-md-6');
			$('#col2').removeClass().addClass('col-md-6');
			$('#col3').hide();
			return true;
		}
		for (var i = 1; i < 4; i++) {
			var id = '#col'+i;
			$(id).removeClass().addClass('col-md-3');
		}
		var id = '#col'+nr;
		$(id).removeClass().addClass('col-md-6');
		
		var ifr = $('#iframe');
		var ifp = $('#iframe').parent();
		var col1 = $('#col1');

		console.log(ifp.width(),col1.height());

		$(ifr).width(ifp.width());
		$(ifr).height(col1.height());

		
	}

	function displayJson(json){
		if( json.formated ){
			json.formated = linkify(json.formated);
			$('#result').html(json.formated);
		}else{
			$('#result').html(json);
		}

		$('#messages').html('');
		if( json.messages ){
			var out = '';
			for( i in json.messages ){
				if( json.messages[i].substring(0,7)=='Missing'){
					out+= '<span class="label label-danger" style="font-size:14px">'+json.messages[i]+'</span><br>';
				}else{
					out+= '<span class="label label-warning">'+json.messages[i]+'</span><br>';
				}
			}
			$('#messages').html(out);
		}
	}

	arrangeColumns(1);

</script>