<!-- Morris chart -->
@css "morris/morris.css"
<!-- Morris.js charts -->
@jsend "https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"
@jsend "morris/morris.min.js"
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<?php /*@jsend "pages/dashboard.js" */ ?>

  <!-- Content Header (Page header) -->
  <!--
        <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
  -->

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">

            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{Product:active()->count()}} / {{Product:count()}}</h3>
                  <p>Products ( active / total )</p>
                </div>
                <div class="icon">
                  <i class="fa fa-shopping-bag"></i>
                </div>
                <a href="/admin/product" class="small-box-footer">show more <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{Brand:active()->count()}} / {{Brand:count()}}</h3>
                  <p>Brands ( active / total )</p>
                </div>
                <div class="icon">
                  <i class="fa fa-trademark"></i>
                </div>
                <a href="/admin/brand" class="small-box-footer">show more <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?php
                    echo Link::whereTask('done')->count();
                    echo ' / ';
                    echo Link::whereTask('parse')->whereParser('product')->whereIn('status',[200,301,302])->count();
                    echo ' / ';
                    echo Link::count();
                    ?></h3>
                  <p>Links ( not crawled /not parsed / total )</p>
                </div>
                <div class="icon">
                  <i class="fa fa-globe"></i>
                </div>
                <a href="/admin/shop" class="small-box-footer">show more <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

          </div>

          <!-- Main row -->
          <div class="row">

            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{Category:active()->count()}} / {{Category:count()}}</h3>
                  <p>Categories ( active / total )</p>
                </div>
                <div class="icon">
                  <i class="fa fa-list"></i>
                </div>
                <a href="/admin/category" class="small-box-footer">show more <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{Material:count()}}</h3>
                  <p>Materials ( total )</p>
                </div>
                <div class="icon">
                  <i class="fa fa-battery-3"></i>
                </div>
                <a href="/admin/material" class="small-box-footer">show more <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->


          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
