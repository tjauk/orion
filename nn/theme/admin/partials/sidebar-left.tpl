<!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">GLAVNI IZBORNIK</li>

            @if Auth:can('menu.dashboard')
            <li>
              <a href="/admin/dashboard">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            @end

            <li>
              <a href="/admin/product">
                <i class="fa fa-shopping-bag"></i> <span>Products</span>
                <small class="label pull-right bg-green">{{Product:count()}}</small>
              </a>
            </li>

            <li>
              <a href="/admin/brand">
                <i class="fa fa-building-o"></i> <span>Brands</span>
                <small class="label pull-right bg-green">{{Brand:count()}}</small>
              </a>
            </li>

            <li>
              <a href="/admin/category">
                <i class="fa fa-list"></i> <span>Categories</span>
                <small class="label pull-right bg-green">{{Category:count()}}</small>
              </a>
            </li>

            <li>
              <a href="/admin/material">
                <i class="fa fa-battery-3"></i> <span>Materials</span>
                <small class="label pull-right bg-green">{{Material:count()}}</small>
              </a>
            </li>

            <li>
              <a href="/admin/colorname">
                <i class="fa fa-paint-brush"></i> <span>Colors</span>
                <small class="label pull-right bg-green">{{ColorName:count()}}</small>
              </a>
            </li>

            <li>
              <a href="/admin/site">
                <i class="fa fa-globe"></i> <span>Sites</span>
                <small class="label pull-right bg-green">{{Site:count()}}</small>
              </a>
            </li>

            <li>
              <a href="/admin/link">
                <i class="fa fa-globe"></i> <span>Links</span>
                <small class="label pull-right bg-green">{{Link:count()}}</small>
              </a>
            </li>

            <li>
              <a href="/admin/crawl/editor">
                <i class="fa fa-code"></i> <span>Crawl editor</span>
              </a>
            </li>

            <li>
              <a href="/admin/redirectlog">
                <i class="fa fa-bar-chart"></i> <span>Statistics</span>
                <small class="label pull-right bg-green">{{RedirectLog:today()->count()}}</small>
              </a>
            </li>

            <li>
              <a href="/admin/currency">
                <i class="fa fa-money"></i> <span>Exchange rates</span>
                <small class="label pull-right bg-green">{{Currency:count()}}</small>
              </a>
            </li>

            @if Auth:can('menu.user')
            <li>
              <a href="/admin/user">
                <i class="fa fa-user-plus"></i> <span>Users</span>
                <small class="label pull-right bg-gray">{{User:count()}}</small>
              </a>
            </li>
            @end

            @if Auth:can('menu.user')
            <li>
              @if Auth:can('manage.usergroups')
                <li><a href="/admin/usergroup"><i class="fa fa-circle-o"></i> User groups</a></li>
              @end
            </li>
            @end

            @if Auth:can('access.api-log')
            <li>
              <a href="/admin/api-log">
                <i class="fa fa-database"></i> <span>API logs</span>
              </a>
            </li>
            @end

            @if Auth:can('admin')
            <li>
              <a href="/admin/search-log">
                <i class="fa fa-search"></i> <span>Search log</span>
              </a>
            </li>
            @end
<!--
            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
-->
          </ul>
