<div class="homepage row">

  <div class="col-md-12" style="text-align:center;">
    <h1 style="margin-left:-20px;"><i class="fa fa-shopping-bag"></i> MyHandBags.eu</h1>
  </div>

  <div class="col-md-12" style="margin-top:40px;">
    <?php $query = Request::get('q'); ?>
    <form id="homepage-search" role="search" action="/search">
      <div class="input-group" style="width:100%;">
        <input name="q" id="top-search-input" type="text" class="form-control" autocomplete="off" placeholder="Find your dream bag" value="{{query|strip_tags|e}}">
        <div class="input-group-btn">
            <button class="btn btn-search" type="submit"><i class="fa fa-search"></i> Search</button>
        </div>
      </div>
    </form>
  </div>

  <div id="homepage-info" class="col-md-12">
      <p>
          <b>{{total_products}}+ bags</b> from <a href="/brands" style="color: inherit"><b>{{total_brands}}+ designers &amp; brands</b></a>
          with prices ranging from low <b>{{min_price|round}}€</b> to exclusive <b>{{max_price|round}}€</b>.
      </p>
      <p>
          All items are shipping from <b>European Union</b> so you can expect <b>low shipping rates</b> from all featured webshops.
      </p>
      <p>

      </p>
  </div>
</div>

<div class="row">
  <div  id="homepage-categories" class="col-md-12">
    <ul>
        <?php $categories = Category::active()->orderBy('name',ASC)->get(); ?>
        @foreach categories as category
          <?php
          $pc = Product::active()->whereCategoryId($category->id)->take(1)->first();
          // $icon = $pc->thumb('28x28xA');
          ?>
          <li><a href="/category/{{category.slug}}">{{category.name}}</a></li>
        @end
    </ul>
    <div class="clearfix"></div>
  </div>

  <div class="col-md-12">
    <h3>Featured</h3>
    @loop featured_products as item
        @inc "product/list-box"
    @end
  </div>

  <div class="col-md-12">
    <h3>On sale</h3>
    @loop products_on_action as item
        @inc "product/list-box"
    @end
  </div>

  <div class="col-md-12">
    <h3>Newest additions</h3>
    @loop newest_products as item
        @inc "product/list-box"
    @end
  </div>

</div>
