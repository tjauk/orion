<!DOCTYPE html>
<html>
<head>
	@include "head"
</head>
<body>

	<div id="app">

		<!-- header -->
	  @include "header"

		<!-- main -->
	  <div id="content" class="container">
			{{CONTENT}}
		</div>

		<!-- footer -->
	  @include "footer"

	</div>

	<!-- end scripts -->
  <script type="text/javascript" src="/theme/app.js"></script>

	@include "google-analytics"

	{{doc.jsend}}
</body>
</html>
