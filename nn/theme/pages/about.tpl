<?php
$total_brands = Brand::active()->count();
$total_products = Product::active()->count();
$total_categories = Category::active()->count();
?>

<style>
	#about p {
		font-size: 16px !important;
	}
</style>

<div id="about">

	<h1>About</h1>

	<p>
		Idea for myhandbags.eu site started as just another price compare guide at first.
	</p>
	<p>
		After a while we changed our mission and now we are on path to become the best and largest catalogue specialized in all kinds of bags.
	</p>

	<p>&nbsp;</p>

	<p>
		We just started and there are already {{total_products}} bags carefully categorized in {{total_categories}} categories.
	</p>
	<p>
		We are presenting to you {{total_brands}} brands and designers.
	<p>

	<p>&nbsp;</p>

</div>
