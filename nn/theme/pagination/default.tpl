<div class="col-md-12" style="text-align: center;margin-left:-10px;margin-right:-10px;">
<?php
// display how many pages around selected
$pages_around = 1;

//
$pages_total = count($items)-4;
$pages_max = 7;
$pages_show = range(1,$pages_total);

// << < 1 ... 4 5 6 [7] 8 9 10 ... 99 > >>
if( $pages_total>$pages_max ){
	// find current page
	foreach($items as $item){
		if( $item['selected']==1 ){
			$current = $item['number'];
			break;
		}
	}
	$left = $current-$pages_around;
	$right = $current+$pages_around;
	while($left<0){
		++$left; ++$right;
	}
	while($right>$pages_total){
		--$left; --$right;
	}
	$middle = range($left,$right);
	$pages_show = array_merge( array(1,$pages_total), $middle );
}
?>
	<ul class="pagination pagination-sm no-margin">
	<?php
	$prev_page = 0;
	foreach($items as $item){
	?>

		@if item.flag = "FIRST"
			<li class="hidden-xs pagination-first"><a href="{{item.link}}">«</a></li>
		@end
		@if item.flag = "PREV"
			<li class="pagination-prev"><a href="{{item.link}}">‹</a></li>
		@end
		<?php if( $item['flag']=="NUMBER" ){ ?>
			<?php if( in_array($item['number'], $pages_show) ){ ?>
				<?php if( $prev_page+1 < $item['number'] ){ ?>
					<li class="hidden-xs"><span>...</span></li>
				<?php }; ?>
				<?php $prev_page = $item['number']; ?>
				@if item.selected = 1
					<li class="active"><a href="{{item.link}}">{{item.number}}</a></li>
				@else
					<li><a href="{{item.link}}">{{item.number}}</a></li>
				@end
			<?php }; ?>
		<?php }; ?>
		@if item.flag = "BETWEEN"
			<li class="hidden-xs"><span>...</span></li>
		@end
		@if item.flag = "NEXT"
			<li class="pagination-next"><a href="{{item.link}}">›</a></li>
		@end
		@if item.flag = "LAST"
			<li class="hidden-xs pagination-last"><a href="{{item.link}}">»</a></li>
		@end
	<?php
		
	}; ?>
	</ul>
</div>