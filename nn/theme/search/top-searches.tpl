<!--BEGIN:search/top-searches-->
<?php Doc::title('Top searched - '.$items[0]->term); ?>
<h1>Top Searched Keywords</h1>

<ul style="list-style: url;font-size: 21px;">
	@loop items
	<li><a href="/search?q={{item..term}}">{{item..term}}</a></li>
	@end
</ul>

<!--END:search/top-searches-->
