<?php
$query = Request::get('q');
?>
<form class="navbar-form search-top" role="search" action="/search">
  <div class="input-group" style="width: 100%">
    <input name="q" id="top-search-input" type="text" class="form-control" autocomplete="off" placeholder="What are you shopping for?" value="{{query|strip_tags|e}}">
    <div class="input-group-btn">
        <button class="btn btn-search" type="submit"><i class="fa fa-search"></i></button>
    </div>
  </div>
</form>
