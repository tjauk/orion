<?php
class AdminBookController extends AdminController
{
    public $model = 'Book';
    public $baseurl = '/admin/book';

    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'title' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Per page');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');

        $table = Table::make()
                ->headings(['#','Created At','Updated At','Title','Description','Isbn','Year','Author','Cover','Back','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = $item->created_at;
            $data[] = $item->updated_at;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->title.'</a>';
            $data[] = $item->description;
            $data[] = $item->isbn;
            $data[] = $item->year;
            $data[] = $item->author;
            $data[] = $item->cover;
            $data[] = $item->back;
            $actions = UI::btnEdit($item->id,'book');
            $actions.= UI::btnDelete($item->id,'book');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('book');
        $form->label('Title');
        $form->input('title')->klass('form-control');

        $form->label('Description');
        $form->input('description')->klass('form-control');

        $form->label('Isbn');
        $form->input('isbn')->klass('form-control');

        $form->label('Year');
        $form->input('year')->klass('form-control');

        $form->label('Author');
        $form->input('author')->klass('form-control');

        $form->label('Cover');
        $form->input('cover')->klass('form-control');

        $form->label('Back');
        $form->input('back')->klass('form-control');


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/book')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
