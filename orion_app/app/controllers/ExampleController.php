<?php
class ExampleController extends Controller
{
    function index()
    {
        $ctrlname = str_replace('Controller','',__CLASS__);
        echo h3( $ctrlname );
        $actions = get_class_methods( __CLASS__ );
        sort( $actions );
        echo '<ul>';
        echo '<li>'.strtolower( $ctrlname ).'/'.'</li>';
            echo '<li><ul>';        
            foreach( $actions as $action ) {
                if( substr( $action, 0, 1 ) !== '_' and substr( $action, 0, 4 ) !== 'post' and substr( $action, 0, 6 ) !== 'widget' ) {
                          echo  '<li><a href="/'.strtolower( $ctrlname ).'/'.$action.'">'.$action.'</a></li>';
                }
            }
            echo '</ul></li>';        
        echo '</ul>';
    }


    function arr(){
        echo Html::pre('
$values = array( 1,2,3,4,5 );
$avg = Arr::avg( $values );
echo $avg; // returns 3
');
        $values = array( 1,2,3,4,5 );
        $avg = Arr::avg( $values );
        echo "RESULT=$avg"; // returns 3

        echo "<br>\n";
        echo "Class \Trinum\Arr methods<br>\n";
        pr(get_class_methods('\Trinium\Arr'));
    }


    function adminlte()
    {
        Doc::view('layouts/adminlte');

    }

    function auth()
    {
        pr(Session::get('auth.user'));
        Session::put('auth.user','');
        return '';
        $username = 'test';
        $password = 'tes2t';
        $credentials = ['username'=>$username,'password'=>$password];
        $logged = Session::get('auth.user');
        pr($logged->toArray());
        if( Auth::guest() ){
            echo "GUEST";
        }
        if( Auth::attempt($credentials) ){
            echo Auth::user()->first_name;
        }
    }

    
    function assets()
    {
        jquery();
        jsready("alert('ok');");

    }


    function autoload()
    {
        Autoload::dump();
    }


    function cache()
    {
        if( Cache::has('value') ){
            $value = Cache::get('value');
        }else{
            $value = Cache::put('value',5,1);
        }

        return $value;
    }


    function configs()
    {
    	echo Config::get('app.charset');
        Config::set('app.charset','latin');
        echo Config::get('app.charset');
        echo Config::get('cache.driver');
        Config::set('database.value','vec sam setao');
        echo Config::get('database.value','hello');
        pr( Config::$keys );
    }

    function cookie()
    {
/*
        $out = "TUSAM";
        $name = config('session.cookie');
        $out.= $name;
        $out.= Cookie::get('orion_session');
        */
        // solve new Request class first to get this working
        $val = Cookie::get('count',1);
        Cookie::put('count',$val+1);
        return $val;
    }

    function csv()
    {
        $writer = League\Csv\Writer::createFromFileObject(new SplTempFileObject); //the CSV file will be created into a temporary File
        $writer->setDelimiter("\t"); //the delimiter will be the tab character
        $writer->setNewline("\r\n"); //use windows line endings for compatibility with some csv libraries
        $writer->setEncodingFrom("utf-8");
        $headers = ["position" , "team", "played", "goals difference", "points"];
        $writer->insertOne($headers);
        $teams = [
            [1, "Chelsea", 26, 27, 57],
            [2, "Arsenal", 26, 22, 56],
            [3, "Manchester City", 25, 41, 54],
            [4, "Liverpool", 26, 34, 53],
            [5, "Tottenham", 26, 4, 50],
            [6, "Everton", 25, 11, 45],
            [7, "Manchester United", 26, 10, 42],
        ];
        $writer->insertAll($teams);

        echo $writer->toHTML('table-csv-data with-header');

        echo "CSV DONE!<br>";
    }

    function dbs()
    {

    }

    function doc()
    {
        Doc::title('My page title');
        asset('jquery');
        jsblock('
$(document).ready(function(){
    alert("ready");
});
        ');
        //Doc::js('http://orion.dev/theme/js/jquery/jquery.js');

        Doc::view('layouts/blank');
        echo "ECHOED";
        //return "TEST CONTENT";
        //Doc::dump();
    }

    function env()
    {
        echo config('app.timezone');
        // will return timezone from configs/app.php file, timezone var
        // or .env file, APP_TIMEZONE var
        // it just works ;)
    }

    function file()
    {
        $file = 'docs/overview.txt';
        $txt = File::mime($file);
        echo $txt;

        $txt = File::make($file)->size();
        echo $txt;
    }

    function fluid()
    {
        // if no such model
        // create default model class
        $book = new Book();
        $book->unfreeze();
        // create migration, database table named in plural, lower case => books
        // with basic columns like id, created_at and updated_at
        
        // add attribute and database column named title, guess column type
        $book->title = "My title";
        $book->author_id = 1;
        // create migration and add all missing column names and types

        // save
        $book->save();
        // saved to newly created database table ;)
    }

    function fluid2()
    {
        $place = new Place();
        //$place->unfreeze();

        $place->name = "My location";
        $place->lat = 46.001;
        $place->long = 16.001;
        $place->address = "Ilica 123";
        $place->city = "Zagreb";
        $place->country = "Hrvatska";
        $place->save();
        return $place;
    }

    function fluid3()
    {
        $recept = new Recepie();
        $recept->name = "Moj recept";
        $recept->save();
    }

    function gen()
    {
        Generate::model('MyModel');
    }

    function gen2()
    {
        $vehicle = new Vehicle();
        $vehicle->name = "Mustang";
        $vehicle->company = "Ford";
        $vehicle->model = "GT";
        $vehicle->year = 1992;
        $vehicle->save();

        $country = new Country();
        $country->name = "Croatia";
        $country->save();
        
    }

    function folder()
    {
        $folder = 'temp/test';
        Folder::make($folder);
        if( Folder::exists($folder) ){
            echo "Ima";
        }else{
            echo "Nema";
        }
    }

    function forms()
    {
        if( !empty($_POST) ){
            pr($_POST);
        }
        $form = new Forms();
        
        $form->open();
        
            $form->label('First name');
            $form->input('first_name')->required();
        
            $form->br();
            
            $form->label('Last name');
            $form->input('last_name');
            
            $form->br();

            $form->submit('Send');

        $form->close();
        
        return $form->render();
    }

    function hash()
    {
        $password = '1234';
        $hash = Hash::make($password);
        $valid = Hash::check($password,$hash);
        echo "P:$password , H:$hash, V:$valid";
    }

    function layout()
    {
        Layout::set('layouts/blank');
        echo h1("Hello world of layouts");
    }
    function less()
    {
        // http://lessphp.gpeasy.com/

        $parser = new Less_Parser();
        $parser->parse( '@color: #4D926F; #header { color: @color; } h2 { color: @color; }' );
        echo $parser->getCss();

        $parser = new Less_Parser();
        $parser->parseFile( ROOTDIR.'/theme/styles.less', 'http://example.com/mysite/' );
        $css = $parser->getCss();


    }

    function mails()
    {
        $mail = Mail::make();
        $mail->from('test@test.com');
        $mail->to('tihomir.jauk@gmail.com');
        $mail->subject('My subject');
        $mail->body('My body');
        $mail->send();

        if( $mail->sent() ){
            echo "Mail sent";
        }else{
            echo "Mail NOT sent";
        }
    }

    function makeuser()
    {
        $user = new User();
        $user->username = 'admin';
        $user->password = 'admin';
        $user->email = 'tihomir.jauk@gmail.com';
        $user->usergroup = 'admin';
        $user->save();

        return $user;
    }

    function models()
    {
        $articles = \Test\Article::all();
        return $articles;
    }

    function pagination()
    {

    }

    function remote()
    {
        return Remote::get('http://www.bug.hr');
    }

    // http://symfony.com/doc/current/components/http_foundation/introduction.html#request
    function request()
    {   
        pr( Request::cookies() );
        pr( Request::query() );
        pr( Request::post() );
        //exit("tusam u request action");
    }

    // http://symfony.com/doc/current/components/http_foundation/introduction.html#response
    function response()
    {
        return Response::make("Hello",404);
        return Response::json([1,2,3,4]);
    }

    function router()
    {

        //Route::any('books',['as'=>'book/index'])
        Router::$routes = ['books/'=>'BookController@index'];
        Router::$routes = ['books/'=>'BookController@index'];
        Router::$routes = ['book/{:int}'=>'book/view/{Book::id}'];

        /*
        Matching part
        path = string
        segments = array()
        patterns = {}
        method = ANY,GET,POST,PUT,PATCH,DELETE
         */
        
        /*
        Action part
        path
        controller
        action
        model
        view
        args
         */
        
        pr(Router::$routes);
        pr(Router::match('books/'));
        pr(Router::match('books/123'));
        /*
        pr(Router::match('book/index'));
        pr(Router::match('books'));

        $path = preg_quote('books/23/-test');
        echo htmlentities($path).'<br>';
        $pattern = '/books\/([0-9]+)\/(\\\-)([a-z]+)/';
        echo htmlentities($pattern).'<br>';
        
        if( preg_match($pattern, $path) ){
            echo "Match";
        }else{
            echo "Not found";
        }
        exit();

        */
    }

    function schemas()
    {
    	if( Schema::hasTable('myschema') ){
    		Schema::drop('myschema');
    	}
    	Schema::create('myschema',function($table){
    		$table->increments('id');
            $table->timestamps();
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->tinyInteger('active')->default(0);
    	});
    }

    function session()
    {

        // solve cookie first to get this working
        $val = Session::get('count',1);
        Session::put('count',$val+1);
        //Session::dump();
        /*
        $val = Session::get('count',1);
        $id = Session::id();
        Cookie::put(config('session.cookie'),$id);
        */
        return $val;
    }

    function table()
    {
        $t = Table::make();
        $t->headings('#','One','Two');
        $t->add_row([1,'bla','fsdf']);
        $t->out();

        $t = Table::make();
        $t->add_row(Article::find(1)->toArray());
        $t->out();

        pr(Article::find(1));
    }


    function views()
    {
        $items = range(1,5);
        View::make('example/view')
            ->put($items,'items')
            ->out();
        //return view('example.view')->put($items,'items');
    }

    function viewbody()
    {
        $items = range(1,7);
        return View::make()->body("
View body<ul>
@foreach items as item
    <li>{{item}}</li>
@end
</ul>
")->put($items,'items');
    }

    function view2()
    {

        $article = Article::find(1);
        $articles = Article::all();
        $arr = range(1,5);
        return View::make('test/test')
                ->put($article,'article')
                ->put($articles,'articles')
                ->put($arr,'arr');
    }



}
