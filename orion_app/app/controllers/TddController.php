<?php

class TddController extends Controller
{

    function index()
    {
        $actions = get_class_methods( __CLASS__ );
        foreach( $actions as $action ) {
            if( $action == 'index' or substr($action, 0, 2)=='__'  ){
            }else{
                echo $action.'...';
                try {
                    $this->$action();
                    echo "OK";
                } catch (Exception $e) {
                    echo "Failed";
                }
                echo '<br>'."\n";
            }
        }
    }

    function arr(){

    }

    function cache(){
        
    }

    function csv(){

    }

    function db(){

    }

    function eloquent(){
        return Article::all();
    }

    function fluid(){
        $book = new Book();
        $book->unfreeze();
        $book->title = "My title";
        $book->author = "TJ";
        $book->save();
    }

    function form(){
        Form::input('name');
    }

    function forms(){
        $f = Forms::make();
        $f->open('form1');
        $f->input('first_name');
        $f->input('last_name');
        $f->submit('Send');
        $f->out();
    }

    function redirect(){
        return Redirect::to('http://www.trinium-media.hr');
    }

    function remote(){
        Remote::get('http://www.trinium-media.hr');
    }

    function schema(){
        
    }

    function session(){
        $value = Session::get('value');
        Session::put('value',++$value);
    }

    function str(){
        if( Str::contains('ABC','b') ){
            echo "ABC contains b";
        }
    }

    function view(){
        view('tdd.list')->out();
        echo view('tdd.list')->render();
        View::make('tdd.list')->out();
        echo View::make('tdd.list')->render();
    }

}
