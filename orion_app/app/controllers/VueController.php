<?php
class VueController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('vue/todo');
    }

    public function getBudget(){
        $id = Request::get('id');
        return ['id'=>$id];
    }

    public function putBudget(){
        $id = Request::get('id');
        return ['id'=>$id];
    }


}
