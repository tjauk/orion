<?php

class CreateBooksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('title')->default('');
            $table->text('description')->default('');
            $table->string('isbn')->default('');
            $table->integer('year')->default(2015);
            $table->string('author')->default('');
            $table->string('cover')->default('');
            $table->string('back')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('books');
    }

}
