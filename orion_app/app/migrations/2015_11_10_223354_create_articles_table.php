<?php

class CreateArticlesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->dateTime('published_at')->nullable();
            $table->string('title')->default('');
            $table->string('slug')->default('');
            $table->text('summary');
            $table->text('body');
            $table->text('photos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }

}
