<?php

class CreateGoranTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gorans', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('firstname')->default('');
            $table->string('lastname')->default('');
            $table->integer('age')->unsigned();
            $table->text('opis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gorans');
    }

}
