<?php
class Article extends Model
{
    protected $table = 'articles';

    protected $fillable = ['published_at','title','slug','summary','body','photos'];

    /*
     * Relations
     */


    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */
    public function getLinkAttribute()
    {
        return '/article/view/'.$this->id;
    }

}
