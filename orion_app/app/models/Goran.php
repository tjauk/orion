<?php
class Goran extends Model
{
    protected $table = 'gorans';

    protected $fillable = [
		'firstname',
		'lastname',
		'age',
		'opis'
    ];

    protected $hidden = ['id', 'created_at', 'updated_at','age'];

    protected $appends = ['fullname'];

    /*
     * Relations
     */



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }



    /*
     * Getters
     */
    public function getFullnameAttribute()
    {
        return $this->firstname.' '.$this->lastname;
    }

}
