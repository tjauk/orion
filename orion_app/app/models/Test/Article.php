<?php
namespace Test;

use \Trinium\Model as Model;

class Article extends Model {

	protected $table	= 'articles';	
	protected $fillable = ['title'];

	public function getLinkAttribute()
	{
		return '/article/view/'.$this->id;
	}
}