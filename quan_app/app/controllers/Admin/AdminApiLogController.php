<?php
class AdminApiLogController extends AdminController
{
    public $model = 'ApiLog';
    public $baseurl = '/admin/api-log';


    /**
     * Index page
     *
     * @return View
     */
    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $filter = new SearchFilter;
        $filter->perpage = 100;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'method,url,params' );
        }

        $items = $items->orderBy('id',DESC);

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search')->search('keywords')->placeholder('Search');
        $form->label('Per page')->select('perpage')->values([10,20,50,100]);

        $table = Table::make()
                ->headings(['#','Time','IP','Request','Params','Device Uid','User','Region','Lat','Lon','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = $item->created_at;
            $data[] = $item->ip_address;
            $data[] = $item->method.' '.$item->url;
            $data[] = json_encode($item->params);
            $data[] = $item->device_uid;
            $data[] = $item->user_id;
            $data[] = $item->region_id;
            $data[] = $item->lat;
            $data[] = $item->lon;
            $actions = UI::btnEdit($item->id,'apilog');
            $actions.= UI::btnDelete($item->id,'apilog');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    /**
     * Edit page
     *
     * @return View
     */
    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('apilog');

        $form->label('Method')->input('method');
        $form->label('Url')->input('url');
        $form->label('Params')->text('params');
        $form->label('IP address')->input('ip_address');
        $form->label('Device Uid')->input('device_uid');
        $form->label('User Id')->input('user_id');
        $form->label('Region Id')->input('region_id');
        $form->label('Lat')->input('lat');
        $form->label('Lon')->input('lon');

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }


    /**
     * Delete
     *
     * @return Response
     */
    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/apilog')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
