<?php
class ApiController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        ApiLog::request();
    }

    /**
     * 
     *
     * @return Response
     */
    public function index()
    {
        Doc::set('layout/blank');
        Doc::title('API');
        $txt = "
POST api/results
";
        return str_replace("\n","<br>",$txt);
    }

    /**
     * 
     *
     * @return Response
     */
    public function receiver()
    {
        return [
            'success'   => false,
            'message'   => "Not implemented",
        ]

    }


}
