<?php
class ApiDeal extends Deal
{

    public function getPhotosAttribute()
    {
        $photos = array_filter(explode(';',$this->attributes['photos']));
        $items = [];
        foreach($photos as $item){
            if( !empty($item) ){
                $img = Img::make($item);
                if( $img->width()>1200 or $img->height()>1200 ){
                    $items[] = $img->thumb('1400x700xA')->url();
                }else{
                    $items[] = $img->url();
                }
            }
        }
        return $items;
    }

    public function getRatingAttribute()
    {
        $rating = $avg = Rating::average('Deal',$this->id);
        if( floatval($this->attribute['rating'])>0 ){
            $rating = ( $rating + floatval($this->attribute['rating']) ) / 2;
        }
        return $rating;
    }

}
