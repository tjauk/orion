<?php
class ApiPage extends Page
{

    protected $casts = [
        'featured'      => 'boolean',
        'menu_id'       => 'integer',
        'region_id'     => 'integer',
    ];
    
    public function getPhotosAttribute()
    {
        $photos = array_filter(explode(';',$this->attributes['photos']));
        $items = [];
        foreach($photos as $item){
            if( !empty($item) ){
                $img = Img::make($item);
                if( $img->width()>1200 or $img->height()>1200 or $img->filesize()>1000000 ){
                    $items[] = $img->thumb('1400x700xA')->url();
                }else{
                    $items[] = $img->url();
                }
            }
        }
        return $items;
    }

    public function getPlacesAttribute()
    {
        $ids = collect(array_filter(explode(',',$this->attributes['places'])));
        return $ids->map(function($item, $key){
            return ApiPlace::find($item);
        })->all();
    }

    public function getDealsAttribute()
    {
        $ids = collect(array_filter(explode(',',$this->attributes['deals'])));
        return $ids->map(function($item, $key){
            return ApiDeal::find($item);
        })->all();
    }

}
