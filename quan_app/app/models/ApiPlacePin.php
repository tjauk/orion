<?php
class ApiPlacePin extends ApiPlace
{
    protected $appends = [
    	'icon',
    	'phone',
    	'action',
    ];

    public function getIconAttribute()
    {
    	return "default";
    }

    public function getPhoneAttribute()
    {
    	$deals = Deal::wherePlaceId($this->id)->get(['id','info_phone']);
    	if( count($deals)>0 ){
    		$deal = $deals[0];
    		if( !empty($deal->info_phone) ){
    			return $deal->info_phone;
    		}
    	}
    	return '';
    }

    public function getActionAttribute()
    {
    	$deals = $this->deals;
    	if( count($deals)>0 ){
    		$command = "dealsMore\nid:".$deals[0];
    		return Action::parse($command);
    	}
    	$pages = $this->pages;
    	if( count($pages)>0 ){
    		$command = "page\nid:".$pages[0];
    		return Action::parse($command);
    	}
    	return null;
    }


    public function getPagesAttribute()
    {
    	$ids = [];
    	$pages = ApiPage::whereRaw('FIND_IN_SET('.$this->id.',places)')->get(['id']);
    	foreach($pages as $page){
    		$ids[] = $page->id;
    	}
    	return $ids;
    }

    public function getDealsAttribute()
    {
    	$ids = [];
    	$deals = Deal::wherePlaceId($this->id)->get(['id']);
    	foreach($deals as $deal){
    		$ids[] = $deal->id;
    	}
    	return $ids;
    }

}
