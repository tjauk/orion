<?php
class Deal extends Model
{

    use \Trinium\Traits\GetSetPhotosAttribute;

    protected $table = 'deals';

    protected $fillable = [
		'company_id',
		'title',
        'summary',
		'description',
		'rating',
		'tripadvisor',
		'discount',
		'discount_note',
        'place_id',
		'city_id',
        'region_id',
		'valid_from',
		'valid_to',
		'note',
        'tags',
        'photos',
        'info_web',
        'info_email',
        'info_phone',
    ];

    protected $hidden = [
        'created_at',
        //'updated_at',
        'place_lat',
        'place_lon',
        'company_id',
        'place_id',
        'city_id',
        'note'
    ];

    protected $casts = [
        'rating'    => 'float',
        'discount'  => 'integer'
    ];

    /*
     * Relations
     */
    public function company()
    {
        return $this->belongsTo('Company');
    }
    public function place()
    {
        return $this->belongsTo('Place');
    }
    public function city()
    {
        return $this->belongsTo('City');
    }
    public function region()
    {
        return $this->belongsTo('Region');
    }



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopeActive($query)
    {
        $query->where(function($query){
            $query->orWhereNull('valid_from');
            $query->orWhere('valid_from', '>=', \DB::raw('NOW()'));
        });
        $query->where(function($query){
            $query->orWhereNull('valid_to');
            $query->orWhere('valid_to', '<=', \DB::raw('NOW()'));
        });
    }


    /*
     * Getters
     */
    public function getTagsAttribute()
    {
        return array_filter(explode(',',$this->attributes['tags']));
    }



    /*
     * Setters
     */
    public function setTagsAttribute($tags)
    {
        if( is_array($tags) ){
            $tags = implode(',',$tags);
        }
        if( is_null($tags) ){
            $tags = '';
        }
        $this->attributes['tags'] = $tags;
    }

    public function setDiscountAttribute($str)
    {
        $this->attributes['discount'] = (int)$str;
    }

    public function setValidFromAttribute($str)
    {
        $val = date('Y-m-d',strtotime($str));
        if( empty($str) or $str=='0000-00-00' ){
            $val = null;
        }
        $this->attributes['valid_from'] = $val;
    }
    
    public function setValidToAttribute($str)
    {
        $val = date('Y-m-d',strtotime($str));
        if( empty($str) or $str=='0000-00-00' ){
            $val = null;
        }
        $this->attributes['valid_to'] = $val;
    }

}
