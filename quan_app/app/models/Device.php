<?php
class Device extends User
{

    protected $fillable = [
        'name', 
        'email',
        'usergroup',
        'first_name',
        'last_name',
        'blocked',
        'device_uid'
    ];

    /*
     * Register
     */
    static function register($device_uid)
    {
        if( !empty($device_uid) ){

            $user = User::findByDeviceUid($device_uid);

            if( !$user ){
                $user = new User();
                $user->username     = $device_uid;
                $user->device_uid   = $device_uid;
                $user->usergroup    = 'user';
                $user->last_seen = date('Y-m-d H:i:s');
                $user->save();
            }

            $user->saveLastPosition();

            return $user;
        }
        return false;
    }


    /*
     * Scopes
     */


    /*
     * Getters
     */


    /*
     * Setters
     */


}
