<?php
class Menu extends Model
{

    use \Trinium\Traits\GetSetIconsAttribute;
    use \Trinium\Traits\GetSetImagesAttribute;

    protected $table = 'menus';

    protected $fillable = [
		'name',
		'slug',
		'group',
        'pri',
		'images',
		'icons',
		'action',
		'active',
		'private',
        'region_id',
        'color'
    ];

    protected $hidden = [
        'created_at',
    ];

    protected $casts = [
        'pri'       => 'integer',
        'action'    => 'json',
    ];

    /*
     * Relations
     */
    public function region()
    {
        return $this->belongsTo('Region');
    }

    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::groupBy('group')->orderBy('name',ASC)->get();

        foreach($items as $item){
            $options[$item->group] = $item->group;
        };

        return $options;
    }

    static function optionsAll()
    {
        $options = [''=>'---'];
        $items = static::orderBy('region_id',ASC)->orderBy('pri',ASC)->get();

        foreach($items as $item){
            $options[$item->id] = $item->title;
        };

        return $options;
    }


    /*
     * Scopes
     */


    /*
     * Getters
     */
    public function getTitleAttribute()
    {
        if( $this->region_id>0 ){
            return $this->name.' ('.$this->region->name.')';
        }
        return $this->name.' ('.$this->group.')';
    }


    /*
     * Setters
     */


}
