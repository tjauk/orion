<?php
class Page extends Model
{

    use \Trinium\Traits\GetSetPhotosAttribute;

    protected $table = 'pages';

    protected $fillable = [
		'status',
		'published_at',
		'title',
		'slug',
		'excerpt',
		'content',
		'photos',
		'tags',
		'featured',
        'menu_id',
        'region_id',

        'deals',
        'places'
    ];

    static $statuses = [
        'draft',
        'published',
        'archived'
    ];

    protected $hidden = [
        'created_at',
    /*
        'updated_at',
    */
    ];

    protected $casts = [
        'featured'      => 'boolean',
        'menu_id'       => 'integer',
        'region_id'     => 'integer'
    ];

    /*
     * Relations
     */
    public function menu()
    {
        return $this->belongsTo('Menu');
    }
    public function region()
    {
        return $this->belongsTo('Region');
    }


    /*
     * Options
     */
    static function statusOptions()
    {
        return array_combine(static::$statuses, static::$statuses);
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->whereStatus('published');
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */
    public function getTagsAttribute()
    {
        return array_filter(explode(',',$this->attributes['tags']));
    }

    /*
     * Setters
     */
    public function setStatusAttribute($status)
    {
        if( !in_array($status,static::$statuses) ){
            $status = 'draft';
        }
        if( $status=='published' and is_null($this->published_at) ){
            $this->attributes['published_at'] = date('Y-m-d H:i:s');
        }
        $this->attributes['status'] = $status;
    }

    public function setTagsAttribute($tags)
    {
        if( is_array($tags) ){
            $tags = implode(',',$tags);
        }
        if( is_null($tags) ){
            $tags = '';
        }
        $this->attributes['tags'] = $tags;
    }
    
    public function setPublishedAtAttribute($str)
    {
        $val = date('Y-m-d H:i:s',strtotime($str));
        if( empty($str) or $str=='0000-00-00' ){
            $val = null;
        }
        $this->attributes['published_at'] = $val;
    }

}
