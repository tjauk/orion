<?php
class Place extends Model
{
    protected $table = 'places';

    protected $fillable = [
		'name',
		'slug',
		'address',
		'zip',
		'city',
		'country_id',
		'lat',
		'lon',
		'alt',
		'radius'
    ];

    protected $hidden = [
        'created_at',
        'slug',
        'alt',
        'radius',
    ];

    protected $casts = [
        'lat'   => 'double',
        'lon'   => 'double',
    ];

    protected $defaults = [
        'country_id' => 52
    ];


    /*
     * Relations
     */
    public function country()
    {
        return $this->belongsTo('Country');
    }

    public static function boot(){

        parent::boot();

        static::saving(function($model){
            if( empty($model->slug) ){
                $model->slug = str_slug($model->name);
            }
        });
    }


    /*
     * Methods
     */
    public function geoCodeByAddress($address=null)
    {
        if( is_null($address) ){
            $address = $this->address.', '.$this->zip.' '.$this->city;
        }

        $results = Geo::code($address);

        if( !empty($results['lat']) ){
            $this->lat = $results['lat'];
            $this->lon = $results['lon'];
        }

        return $results;
    }


    /*
     * Form
     */
    static function createForm()
    {
        $form = Forms::make()->format('EDIT_VERTICAL');
        $form->fill(['Place.country_id'=>52]);
        $form->label('Place Name')->input('name');
        $form->label('Address')->input('address');
        $form->label('Zip')->input('zip');
        $form->label('City')->input('city');
        $form->label('Country')->select('country_id')->options(Country::options());
        return $form;
    }


    /*
     * Getters
     */
    public function getLatitudeAttribute()
    {
        return $this->lat;
    }
    public function getLongitudeAttribute()
    {
        return $this->lon;
    }


    /*
     * Setters
     */
    public function setSlugAttribute($str)
    {
        if( $str=='' ){
            $str=$this->name;
        }
        $this->attributes['slug'] = str_slug($str);
    }
    public function setLatAttribute($str)
    {
        if( $str=='' ){
            $str=null;
        }
        $this->attributes['lat'] = $str;
    }
    public function setLonAttribute($str)
    {
        if( $str=='' ){
            $str=null;
        }
        $this->attributes['lon'] = $str;
    }
    public function setLatitudeAttribute($str)
    {
        $this->setLatAttribute($str);
    }
    public function setLongitudeAttribute($str)
    {
        $this->setLonAttribute($str);
    }



}
