<?php
class PromoCode extends Model
{
    protected $table = 'promo_codes';

    protected $fillable = [
		'code',
		'description',
		'discount',
		'discount_note',
		'valid_from',
		'valid_to',
		'max_usage',
		'used'
    ];

    /*
     * Relations
     */


    /*
     * Scopes
     */
    public function scopeValid($query)
    {
        $query->where('valid_from', '<', \DB::raw('NOW()'));
        $query->where('valid_to', '>', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */

    /*
     * Setters
     */
    public function setCodeAttribute($code)
    {
        $this->attributes['code'] = strtoupper($code);
    }
    public function setValidFromAttribute($str)
    {
        $val = date('Y-m-d',strtotime($str));
        if( empty($str) or $str=='0000-00-00' ){
            $val = null;
        }
        $this->attributes['valid_from'] = $val;
    }
    public function setValidToAttribute($str)
    {
        $val = date('Y-m-d',strtotime($str));
        if( empty($str) or $str=='0000-00-00' ){
            $val = null;
        }
        $this->attributes['valid_to'] = $val;
    }

}
