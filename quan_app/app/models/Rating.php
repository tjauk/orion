<?php
class Rating extends Model
{
    protected $table = 'ratings';

    protected $fillable = [
		'item',
		'item_id',
		'score',
		'device_uid'
    ];

    protected $casts = [
        'score' => 'float',
        'item_id' => 'integer',
    ];

    /*
     * Relations
     */
    public function item()
    {
        return $this->belongsTo( $this->item, 'item_id' );
    }



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Agregates
     */
    static function average($item,$item_id)
    {
        return round(
                DB::table('ratings')
                ->whereItem($item)
                ->whereItemId($item_id)
                ->avg('score')
                ,1);
    }


    /*
     * Getters
     */



    /*
     * Setters
     */


}
