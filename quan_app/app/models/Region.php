<?php
class Region extends Model
{
    protected $table = 'regions';

    protected $fillable = [
		'name',
		'country_id',
		'lat',
		'lon',
        'background',
    ];

    /*
     * Relations
     */
    public function country()
    {
        return $this->belongsTo('Country');
    }



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }



    /*
     * Getters
     */

    /*
     * Setters
     */
    public function setLatAttribute($str)
    {
        if( $str=='' ){
            $str=null;
        }
        $this->attributes['lat'] = $str;
    }
    public function setLonAttribute($str)
    {
        if( $str=='' ){
            $str=null;
        }
        $this->attributes['lon'] = $str;
    }

    public function getBackgroundAttribute()
    {
        return array_filter(explode(';',$this->attributes['background']));
    }

    public function setBackgroundAttribute($background)
    {
        if( is_array($background) ){
            $background = implode(';',array_filter($background));
        }
        if( is_null($background) ){
            $background = '';
        }
        $this->attributes['background'] = $background;
    }
}
