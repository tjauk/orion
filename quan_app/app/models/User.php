<?php 

class User extends Model {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $UserGroups = array();
	protected $Permissions = null;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 
		'email',
		'password',
		'usergroup',
		'first_name',
		'last_name',

		'device_uid',
		'is_paid',
		'paid_until',

		// last position
		'lat',
		'lon',
		'region_id',
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password'
	];

	public function can($permission){
		if( empty($this->UserGroups) ){
			$usergroups = explode(',',$this->usergroup);
			
			foreach($usergroups as $usergroup){
				$this->UserGroups[] = UserGroup::findByName($usergroup);
			}
		}
		$valid = false;
		foreach($this->UserGroups as $UserGroup){
			if( $UserGroup->has($permission) ){
				$valid = true;
			}
		}
		return $valid;
	}

	/*
	 * Options
	 */
	static function options()
	{
		$options = [''=>'---'];
		if( Auth::user()->can('admin') ){
			$items = static::orderBy('last_name')->get();
		}else{
			$items = [Auth::user()];
		}
		
		foreach($items as $item){
			if( !empty($item->email) ){
				$options[$item->id] = $item->full_title;
			}
		};
		return $options;
	}


	/*
	 * Get full_name
	 */
	public function getFullNameAttribute(){
		return $this->last_name.' '.$this->first_name;
	}

	/*
	 * Get full_title and (email)
	 */
	public function getFullTitleAttribute(){
		return $this->last_name.' '.$this->first_name.' ('.$this->email.')';
	}

    public function getPaidUntilAttribute()
    {
        if( empty($this->attributes['paid_until']) ){
            return '';
        }
        return date('Y-m-d',strtotime($this->attributes['paid_until']));
    }

	/*
	 * Setters
	 */
	public function setPasswordAttribute($password)
	{
		$this->attributes['password'] = Hash::make($password);
	}

    public function setPaidUntilAttribute($date)
    {
    	if( empty($date) ){
    		$date = null;
    	}else{
        	$date = date('Y-m-d',strtotime($date));
    	}
        $this->attributes['paid_until'] = $date;
    }


	/*
	 * Generate Unique User Token
	 */
	static function generateToken()
	{
		return md5(bin2hex(uniqid())).md5(rand(1,time()));
	}

    /*
     * Last position
     */
    function saveLastPosition()
    {
        $data = Request::all();
        $keys = ['lat','lon','region_id'];
        $changed = false;
        foreach ($keys as $key) {
            if( isset($data[$key]) and !empty($data[$key]) and $data[$key]!=="null" ){
                $this->$key = $data[$key];
                $changed = true;
            }
        }
        if( $changed ){
            $this->save();
        }
    }
	
}
