<div class="box box-primary">
	<div class="box-body">
		<ul class="products-list product-list-in-box">
		@loop items
		  	<li class="item" data-key="{{key}}" data-id="{{item..id}}" style="cursor: pointer;position:relative;">
          <i class="fa fa-close" style="position:absolute;top:10px;right:0;"></i>
		  		<!--
          <div class="product-img">
              
              <img alt="Product Image" src="/theme/images/default-50x50.gif">
          </div>

          <div class="product-info"> </div>
          -->
          <i class="fa fa-map-marker"></i> {{item..name}}, {{item..city}}
          
		  	</li>
		@end
		</ul>
	</div>
</div>