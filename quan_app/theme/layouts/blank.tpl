<!DOCTYPE html>
<html lang="en">
    <head>

        <title>{{doc.title}}</title>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="{{doc.description}}">
        <meta name="author" content="{{doc.author}}">
        {{doc.meta}}

        <link rel="icon" href="/favicon.ico">

        <link href="{{THEMEURL}}/styles.css" rel="stylesheet">

        {{doc.css}}

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        @asset "jquery"
        
        {{doc.js}}

        <style type="text/css">

        </style>


    </head>

    <body>
        <div class="wrapper">
            <div id="content">{{CONTENT}}</div>
        </div>
    </body>
    
</html>
