#!/bin/bash

cd /var/www/orion/

# set all folders
declare -a folders=( events_sweden events_belgium )

# loop through folders and execute
for folder in "${folders[@]}"
do
	cd $folder
	echo ""
	echo $folder
	php -f orion events:remap
	cd ..
done
