﻿# MySQL-Front 5.0  (Build 1.96)

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;


# Host: 3nium.com    Database: rent_app
# ------------------------------------------------------
# Server version 5.5.47-0ubuntu0.14.04.1

#
# Table structure for table audits
#

DROP TABLE IF EXISTS `audits`;
CREATE TABLE `audits` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `code` text COLLATE utf8_unicode_ci,
  `message` text COLLATE utf8_unicode_ci,
  `data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=854 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table audits
#
LOCK TABLES `audits` WRITE;
/*!40000 ALTER TABLE `audits` DISABLE KEYS */;

INSERT INTO `audits` VALUES (1,'2016-02-07 20:03:13','2016-02-07 20:03:13','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (2,'2016-02-07 20:06:17','2016-02-07 20:06:17','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (3,'2016-02-07 20:13:03','2016-02-07 20:13:03','user.logout','User admin logged out','{\"user_id\":1}');
INSERT INTO `audits` VALUES (4,'2016-02-07 20:13:05','2016-02-07 20:13:05','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (5,'2016-02-07 21:08:20','2016-02-07 21:08:20','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (6,'2016-02-08 06:44:16','2016-02-08 06:44:16','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (7,'2016-02-08 08:06:51','2016-02-08 08:06:51','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (8,'2016-02-08 10:19:12','2016-02-08 10:19:12','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (9,'2016-02-08 10:53:02','2016-02-08 10:53:02','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (10,'2016-02-08 11:28:45','2016-02-08 11:28:45','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (11,'2016-02-08 11:34:27','2016-02-08 11:34:27','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (12,'2016-02-08 13:56:22','2016-02-08 13:56:22','user.logout','User  logged out','{\"user_id\":null}');
INSERT INTO `audits` VALUES (13,'2016-02-09 08:17:58','2016-02-09 08:17:58','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (14,'2016-02-09 11:40:52','2016-02-09 11:40:52','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (15,'2016-02-09 12:53:52','2016-02-09 12:53:52','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (16,'2016-02-09 15:02:13','2016-02-09 15:02:13','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (17,'2016-02-09 15:10:26','2016-02-09 15:10:26','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (18,'2016-02-09 16:24:22','2016-02-09 16:24:22','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (19,'2016-02-09 20:33:03','2016-02-09 20:33:03','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (20,'2016-02-10 07:02:48','2016-02-10 07:02:48','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (21,'2016-02-10 07:13:54','2016-02-10 07:13:54','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (22,'2016-02-10 08:09:06','2016-02-10 08:09:06','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (23,'2016-02-10 10:21:48','2016-02-10 10:21:48','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (24,'2016-02-10 11:36:35','2016-02-10 11:36:35','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (25,'2016-02-10 12:25:11','2016-02-10 12:25:11','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (26,'2016-02-10 13:09:01','2016-02-10 13:09:01','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (27,'2016-02-10 13:54:48','2016-02-10 13:54:48','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (28,'2016-02-10 14:46:02','2016-02-10 14:46:02','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (29,'2016-02-11 08:19:37','2016-02-11 08:19:37','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (30,'2016-02-11 09:11:58','2016-02-11 09:11:58','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (31,'2016-02-11 10:31:31','2016-02-11 10:31:31','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (32,'2016-02-11 11:46:03','2016-02-11 11:46:03','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (33,'2016-02-11 14:54:48','2016-02-11 14:54:48','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (34,'2016-02-11 15:06:38','2016-02-11 15:06:38','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (35,'2016-02-11 18:34:41','2016-02-11 18:34:41','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (36,'2016-02-11 20:53:45','2016-02-11 20:53:45','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (37,'2016-02-12 07:01:34','2016-02-12 07:01:34','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (38,'2016-02-12 08:49:51','2016-02-12 08:49:51','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (39,'2016-02-12 08:59:16','2016-02-12 08:59:16','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (40,'2016-02-12 08:59:47','2016-02-12 08:59:47','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (41,'2016-02-12 09:00:57','2016-02-12 09:00:57','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (42,'2016-02-12 10:19:38','2016-02-12 10:19:38','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (43,'2016-02-12 11:12:30','2016-02-12 11:12:30','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (44,'2016-02-12 12:52:09','2016-02-12 12:52:09','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (45,'2016-02-13 08:13:24','2016-02-13 08:13:24','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (46,'2016-02-13 13:50:13','2016-02-13 13:50:13','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (47,'2016-02-13 14:03:00','2016-02-13 14:03:00','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (48,'2016-02-13 15:50:26','2016-02-13 15:50:26','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (49,'2016-02-15 07:05:13','2016-02-15 07:05:13','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (50,'2016-02-15 08:01:34','2016-02-15 08:01:34','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (51,'2016-02-15 08:31:54','2016-02-15 08:31:54','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (52,'2016-02-15 09:30:21','2016-02-15 09:30:21','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (53,'2016-02-15 10:46:38','2016-02-15 10:46:38','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (54,'2016-02-16 08:57:14','2016-02-16 08:57:14','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (55,'2016-02-16 11:22:31','2016-02-16 11:22:31','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (56,'2016-02-16 11:26:59','2016-02-16 11:26:59','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (57,'2016-02-16 12:54:41','2016-02-16 12:54:41','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (58,'2016-02-16 12:55:01','2016-02-16 12:55:01','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (59,'2016-02-16 12:58:52','2016-02-16 12:58:52','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (60,'2016-02-16 15:12:02','2016-02-16 15:12:02','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (61,'2016-02-17 08:29:22','2016-02-17 08:29:22','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (62,'2016-02-17 09:24:00','2016-02-17 09:24:00','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (63,'2016-02-17 12:30:05','2016-02-17 12:30:05','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (64,'2016-02-17 12:32:38','2016-02-17 12:32:38','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (65,'2016-02-17 16:00:57','2016-02-17 16:00:57','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (66,'2016-02-18 07:24:05','2016-02-18 07:24:05','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (67,'2016-02-18 09:28:01','2016-02-18 09:28:01','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (68,'2016-02-19 08:44:27','2016-02-19 08:44:27','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (69,'2016-02-19 10:16:34','2016-02-19 10:16:34','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (70,'2016-02-19 11:43:14','2016-02-19 11:43:14','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (71,'2016-02-19 13:03:07','2016-02-19 13:03:07','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (72,'2016-02-19 17:16:59','2016-02-19 17:16:59','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (73,'2016-02-19 18:17:40','2016-02-19 18:17:40','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (74,'2016-02-19 20:42:45','2016-02-19 20:42:45','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (75,'2016-02-20 07:12:02','2016-02-20 07:12:02','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (76,'2016-02-20 08:53:52','2016-02-20 08:53:52','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (77,'2016-02-20 17:37:28','2016-02-20 17:37:28','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (78,'2016-02-21 08:04:26','2016-02-21 08:04:26','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (79,'2016-02-21 08:39:01','2016-02-21 08:39:01','user.logout','User admin logged out','{\"user_id\":1}');
INSERT INTO `audits` VALUES (80,'2016-02-21 08:39:03','2016-02-21 08:39:03','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (81,'2016-02-21 08:40:26','2016-02-21 08:40:26','user.logout','User admin logged out','{\"user_id\":1}');
INSERT INTO `audits` VALUES (82,'2016-02-21 10:46:55','2016-02-21 10:46:55','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (83,'2016-02-21 16:50:44','2016-02-21 16:50:44','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (84,'2016-02-21 20:37:34','2016-02-21 20:37:34','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (85,'2016-02-22 08:37:12','2016-02-22 08:37:12','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (86,'2016-02-22 09:18:25','2016-02-22 09:18:25','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (87,'2016-02-22 10:44:25','2016-02-22 10:44:25','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (88,'2016-02-22 11:19:46','2016-02-22 11:19:46','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (89,'2016-02-22 11:39:29','2016-02-22 11:39:29','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (90,'2016-02-22 13:05:23','2016-02-22 13:05:23','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (91,'2016-02-22 13:17:35','2016-02-22 13:17:35','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (92,'2016-02-22 13:48:09','2016-02-22 13:48:09','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (93,'2016-02-22 15:03:48','2016-02-22 15:03:48','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (94,'2016-02-22 15:39:08','2016-02-22 15:39:08','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (95,'2016-02-22 16:34:51','2016-02-22 16:34:51','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (96,'2016-02-22 21:05:54','2016-02-22 21:05:54','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (97,'2016-02-23 06:29:55','2016-02-23 06:29:55','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (98,'2016-02-23 07:06:33','2016-02-23 07:06:33','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (99,'2016-02-23 07:43:00','2016-02-23 07:43:00','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (100,'2016-02-23 08:13:35','2016-02-23 08:13:35','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (101,'2016-02-23 09:18:16','2016-02-23 09:18:16','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (102,'2016-02-23 10:13:59','2016-02-23 10:13:59','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (103,'2016-02-23 11:01:53','2016-02-23 11:01:53','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (104,'2016-02-23 11:34:37','2016-02-23 11:34:37','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (105,'2016-02-23 13:27:00','2016-02-23 13:27:00','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (106,'2016-02-23 13:29:23','2016-02-23 13:29:23','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (107,'2016-02-23 13:46:53','2016-02-23 13:46:53','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (108,'2016-02-23 14:35:40','2016-02-23 14:35:40','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (109,'2016-02-23 14:38:28','2016-02-23 14:38:28','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (110,'2016-02-23 15:03:19','2016-02-23 15:03:19','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (111,'2016-02-23 15:23:38','2016-02-23 15:23:38','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (112,'2016-02-23 16:01:34','2016-02-23 16:01:34','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (113,'2016-02-23 20:00:55','2016-02-23 20:00:55','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (114,'2016-02-24 08:17:59','2016-02-24 08:17:59','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (115,'2016-02-24 08:52:33','2016-02-24 08:52:33','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (116,'2016-02-24 11:26:32','2016-02-24 11:26:32','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (117,'2016-02-24 13:19:23','2016-02-24 13:19:23','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (118,'2016-02-24 14:03:19','2016-02-24 14:03:19','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (119,'2016-02-24 15:35:45','2016-02-24 15:35:45','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (120,'2016-02-24 16:38:20','2016-02-24 16:38:20','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (121,'2016-02-24 21:23:03','2016-02-24 21:23:03','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (122,'2016-02-25 07:14:43','2016-02-25 07:14:43','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (123,'2016-02-25 09:02:48','2016-02-25 09:02:48','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (124,'2016-02-25 10:56:32','2016-02-25 10:56:32','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (125,'2016-02-25 12:12:05','2016-02-25 12:12:05','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (126,'2016-02-25 13:44:05','2016-02-25 13:44:05','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (127,'2016-02-25 15:28:08','2016-02-25 15:28:08','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (128,'2016-02-25 15:59:36','2016-02-25 15:59:36','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (129,'2016-02-25 16:28:45','2016-02-25 16:28:45','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (130,'2016-02-25 17:01:55','2016-02-25 17:01:55','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (131,'2016-02-25 20:29:26','2016-02-25 20:29:26','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (132,'2016-02-25 21:35:42','2016-02-25 21:35:42','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (133,'2016-02-26 06:22:41','2016-02-26 06:22:41','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (134,'2016-02-26 07:19:12','2016-02-26 07:19:12','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (135,'2016-02-26 08:10:05','2016-02-26 08:10:05','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (136,'2016-02-26 09:46:57','2016-02-26 09:46:57','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (137,'2016-02-26 11:56:28','2016-02-26 11:56:28','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (138,'2016-02-26 14:49:17','2016-02-26 14:49:17','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (139,'2016-02-26 15:57:38','2016-02-26 15:57:38','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (140,'2016-02-26 16:41:52','2016-02-26 16:41:52','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (141,'2016-02-26 17:44:09','2016-02-26 17:44:09','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (142,'2016-02-26 17:51:56','2016-02-26 17:51:56','user.logout','User admin logged out','{\"user_id\":1}');
INSERT INTO `audits` VALUES (143,'2016-02-26 20:02:28','2016-02-26 20:02:28','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (144,'2016-02-27 09:57:19','2016-02-27 09:57:19','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (145,'2016-02-27 11:15:50','2016-02-27 11:15:50','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (146,'2016-02-27 13:30:31','2016-02-27 13:30:31','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (147,'2016-02-27 13:36:53','2016-02-27 13:36:53','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (148,'2016-02-27 14:38:50','2016-02-27 14:38:50','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (149,'2016-02-27 16:12:34','2016-02-27 16:12:34','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (150,'2016-02-27 17:28:52','2016-02-27 17:28:52','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (151,'2016-02-28 09:51:47','2016-02-28 09:51:47','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (152,'2016-02-28 14:12:17','2016-02-28 14:12:17','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (153,'2016-02-28 14:12:28','2016-02-28 14:12:28','user.logout','User admin logged out','{\"user_id\":1}');
INSERT INTO `audits` VALUES (154,'2016-02-28 14:45:11','2016-02-28 14:45:11','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (155,'2016-02-29 07:02:22','2016-02-29 07:02:22','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (156,'2016-02-29 08:41:53','2016-02-29 08:41:53','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (157,'2016-02-29 09:38:01','2016-02-29 09:38:01','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (158,'2016-02-29 09:52:50','2016-02-29 09:52:50','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (159,'2016-02-29 10:44:43','2016-02-29 10:44:43','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (160,'2016-02-29 11:48:54','2016-02-29 11:48:54','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (161,'2016-02-29 13:15:09','2016-02-29 13:15:09','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (162,'2016-02-29 13:33:33','2016-02-29 13:33:33','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (163,'2016-02-29 14:04:01','2016-02-29 14:04:01','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (164,'2016-02-29 15:03:06','2016-02-29 15:03:06','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (165,'2016-02-29 17:02:42','2016-02-29 17:02:42','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (166,'2016-02-29 20:01:21','2016-02-29 20:01:21','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (167,'2016-03-01 06:46:41','2016-03-01 06:46:41','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (168,'2016-03-01 08:30:53','2016-03-01 08:30:53','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (169,'2016-03-01 09:02:19','2016-03-01 09:02:19','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (170,'2016-03-01 09:32:03','2016-03-01 09:32:03','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (171,'2016-03-01 10:31:40','2016-03-01 10:31:40','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (172,'2016-03-01 12:25:48','2016-03-01 12:25:48','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (173,'2016-03-01 14:41:10','2016-03-01 14:41:10','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (174,'2016-03-01 18:14:01','2016-03-01 18:14:01','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (175,'2016-03-01 19:08:10','2016-03-01 19:08:10','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (176,'2016-03-02 06:59:55','2016-03-02 06:59:55','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (177,'2016-03-02 07:26:39','2016-03-02 07:26:39','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (178,'2016-03-02 07:57:00','2016-03-02 07:57:00','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (179,'2016-03-02 08:10:18','2016-03-02 08:10:18','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (180,'2016-03-02 09:01:23','2016-03-02 09:01:23','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (181,'2016-03-02 11:26:28','2016-03-02 11:26:28','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (182,'2016-03-02 12:04:08','2016-03-02 12:04:08','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (183,'2016-03-02 13:16:53','2016-03-02 13:16:53','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (184,'2016-03-02 14:58:39','2016-03-02 14:58:39','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (185,'2016-03-03 08:24:47','2016-03-03 08:24:47','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (186,'2016-03-03 09:12:49','2016-03-03 09:12:49','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (187,'2016-03-03 09:35:45','2016-03-03 09:35:45','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (188,'2016-03-03 14:06:57','2016-03-03 14:06:57','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (189,'2016-03-03 17:46:31','2016-03-03 17:46:31','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (190,'2016-03-03 17:57:16','2016-03-03 17:57:16','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (191,'2016-03-03 20:39:54','2016-03-03 20:39:54','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (192,'2016-03-04 09:05:42','2016-03-04 09:05:42','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (193,'2016-03-04 11:06:09','2016-03-04 11:06:09','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (194,'2016-03-04 13:41:47','2016-03-04 13:41:47','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (195,'2016-03-04 15:01:27','2016-03-04 15:01:27','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (196,'2016-03-04 16:04:47','2016-03-04 16:04:47','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (197,'2016-03-04 17:09:05','2016-03-04 17:09:05','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (198,'2016-03-04 17:50:53','2016-03-04 17:50:53','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (199,'2016-03-04 18:01:29','2016-03-04 18:01:29','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (200,'2016-03-04 18:28:19','2016-03-04 18:28:19','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (201,'2016-03-04 18:35:33','2016-03-04 18:35:33','user.logout','User admin logged out','{\"user_id\":1}');
INSERT INTO `audits` VALUES (202,'2016-03-04 19:28:50','2016-03-04 19:28:50','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (203,'2016-03-04 20:12:44','2016-03-04 20:12:44','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (204,'2016-03-05 07:44:22','2016-03-05 07:44:22','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (205,'2016-03-05 20:09:20','2016-03-05 20:09:20','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (206,'2016-03-06 13:06:35','2016-03-06 13:06:35','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (207,'2016-03-06 14:19:13','2016-03-06 14:19:13','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (208,'2016-03-06 16:55:54','2016-03-06 16:55:54','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (209,'2016-03-06 17:48:38','2016-03-06 17:48:38','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (210,'2016-03-06 22:07:03','2016-03-06 22:07:03','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (211,'2016-03-07 06:44:07','2016-03-07 06:44:07','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (212,'2016-03-07 08:09:44','2016-03-07 08:09:44','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (213,'2016-03-07 09:09:59','2016-03-07 09:09:59','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (214,'2016-03-07 11:09:17','2016-03-07 11:09:17','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (215,'2016-03-07 12:08:12','2016-03-07 12:08:12','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (216,'2016-03-07 12:38:49','2016-03-07 12:38:49','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (217,'2016-03-07 13:06:30','2016-03-07 13:06:30','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (218,'2016-03-07 13:12:37','2016-03-07 13:12:37','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (219,'2016-03-07 13:16:22','2016-03-07 13:16:22','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (220,'2016-03-07 14:34:10','2016-03-07 14:34:10','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (221,'2016-03-07 15:56:11','2016-03-07 15:56:11','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (222,'2016-03-07 22:56:46','2016-03-07 22:56:46','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (223,'2016-03-08 07:25:37','2016-03-08 07:25:37','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (224,'2016-03-08 08:04:23','2016-03-08 08:04:23','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (225,'2016-03-08 09:53:43','2016-03-08 09:53:43','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (226,'2016-03-08 10:12:58','2016-03-08 10:12:58','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (227,'2016-03-08 11:29:29','2016-03-08 11:29:29','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (228,'2016-03-08 13:00:27','2016-03-08 13:00:27','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (229,'2016-03-08 13:01:31','2016-03-08 13:01:31','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (230,'2016-03-08 13:37:46','2016-03-08 13:37:46','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (231,'2016-03-08 13:38:56','2016-03-08 13:38:56','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (232,'2016-03-08 17:01:59','2016-03-08 17:01:59','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (233,'2016-03-09 06:31:04','2016-03-09 06:31:04','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (234,'2016-03-09 09:21:50','2016-03-09 09:21:50','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (235,'2016-03-09 10:28:51','2016-03-09 10:28:51','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (236,'2016-03-09 11:21:37','2016-03-09 11:21:37','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (237,'2016-03-09 13:13:06','2016-03-09 13:13:06','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (238,'2016-03-09 13:31:40','2016-03-09 13:31:40','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (239,'2016-03-09 16:13:29','2016-03-09 16:13:29','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (240,'2016-03-09 17:59:04','2016-03-09 17:59:04','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (241,'2016-03-09 20:12:55','2016-03-09 20:12:55','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (242,'2016-03-10 06:28:48','2016-03-10 06:28:48','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (243,'2016-03-10 06:58:52','2016-03-10 06:58:52','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (244,'2016-03-10 07:35:17','2016-03-10 07:35:17','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (245,'2016-03-10 08:13:29','2016-03-10 08:13:29','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (246,'2016-03-10 08:43:29','2016-03-10 08:43:29','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (247,'2016-03-10 10:47:02','2016-03-10 10:47:02','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (248,'2016-03-10 11:45:19','2016-03-10 11:45:19','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (249,'2016-03-10 13:16:37','2016-03-10 13:16:37','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (250,'2016-03-10 13:35:43','2016-03-10 13:35:43','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (251,'2016-03-10 14:36:39','2016-03-10 14:36:39','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (252,'2016-03-10 16:36:28','2016-03-10 16:36:28','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (253,'2016-03-10 17:33:07','2016-03-10 17:33:07','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (254,'2016-03-10 21:01:02','2016-03-10 21:01:02','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (255,'2016-03-11 06:07:37','2016-03-11 06:07:37','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (256,'2016-03-11 10:19:36','2016-03-11 10:19:36','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (257,'2016-03-11 11:01:00','2016-03-11 11:01:00','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (258,'2016-03-11 13:06:43','2016-03-11 13:06:43','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (259,'2016-03-11 13:45:00','2016-03-11 13:45:00','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (260,'2016-03-11 13:56:06','2016-03-11 13:56:06','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (261,'2016-03-11 16:28:03','2016-03-11 16:28:03','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (262,'2016-03-11 17:08:34','2016-03-11 17:08:34','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (263,'2016-03-11 18:35:55','2016-03-11 18:35:55','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (264,'2016-03-12 06:30:40','2016-03-12 06:30:40','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (265,'2016-03-12 08:15:59','2016-03-12 08:15:59','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (266,'2016-03-12 12:55:10','2016-03-12 12:55:10','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (267,'2016-03-12 17:54:01','2016-03-12 17:54:01','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (268,'2016-03-12 19:09:24','2016-03-12 19:09:24','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (269,'2016-03-13 07:15:19','2016-03-13 07:15:19','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (270,'2016-03-14 06:20:33','2016-03-14 06:20:33','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (271,'2016-03-14 08:15:18','2016-03-14 08:15:18','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (272,'2016-03-14 08:47:31','2016-03-14 08:47:31','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (273,'2016-03-14 09:31:12','2016-03-14 09:31:12','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (274,'2016-03-14 10:58:00','2016-03-14 10:58:00','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (275,'2016-03-14 11:02:56','2016-03-14 11:02:56','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (276,'2016-03-14 12:04:34','2016-03-14 12:04:34','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (277,'2016-03-14 14:12:06','2016-03-14 14:12:06','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (278,'2016-03-14 14:19:37','2016-03-14 14:19:37','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (279,'2016-03-14 15:00:47','2016-03-14 15:00:47','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (280,'2016-03-14 17:29:51','2016-03-14 17:29:51','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (281,'2016-03-14 19:52:05','2016-03-14 19:52:05','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (282,'2016-03-14 19:52:39','2016-03-14 19:52:39','user.logout','User admin logged out','{\"user_id\":1}');
INSERT INTO `audits` VALUES (283,'2016-03-14 20:18:45','2016-03-14 20:18:45','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (284,'2016-03-15 06:27:43','2016-03-15 06:27:43','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (285,'2016-03-15 07:20:44','2016-03-15 07:20:44','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (286,'2016-03-15 08:48:20','2016-03-15 08:48:20','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (287,'2016-03-15 10:29:04','2016-03-15 10:29:04','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (288,'2016-03-15 10:58:34','2016-03-15 10:58:34','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (289,'2016-03-15 11:14:49','2016-03-15 11:14:49','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (290,'2016-03-15 13:04:25','2016-03-15 13:04:25','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (291,'2016-03-15 13:14:05','2016-03-15 13:14:05','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (292,'2016-03-15 13:49:59','2016-03-15 13:49:59','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (293,'2016-03-15 14:14:53','2016-03-15 14:14:53','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (294,'2016-03-15 14:17:46','2016-03-15 14:17:46','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (295,'2016-03-15 15:09:52','2016-03-15 15:09:52','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (296,'2016-03-15 16:03:47','2016-03-15 16:03:47','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (297,'2016-03-15 17:16:47','2016-03-15 17:16:47','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (298,'2016-03-15 20:27:00','2016-03-15 20:27:00','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (299,'2016-03-16 06:22:31','2016-03-16 06:22:31','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (300,'2016-03-16 07:40:02','2016-03-16 07:40:02','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (301,'2016-03-16 08:15:10','2016-03-16 08:15:10','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (302,'2016-03-16 08:55:50','2016-03-16 08:55:50','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (303,'2016-03-16 10:39:24','2016-03-16 10:39:24','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (304,'2016-03-16 11:46:11','2016-03-16 11:46:11','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (305,'2016-03-16 13:37:48','2016-03-16 13:37:48','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (306,'2016-03-16 13:49:53','2016-03-16 13:49:53','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (307,'2016-03-16 13:53:25','2016-03-16 13:53:25','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (308,'2016-03-16 14:03:56','2016-03-16 14:03:56','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (309,'2016-03-16 14:31:05','2016-03-16 14:31:05','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (310,'2016-03-16 21:13:54','2016-03-16 21:13:54','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (311,'2016-03-17 08:10:41','2016-03-17 08:10:41','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (312,'2016-03-17 10:12:25','2016-03-17 10:12:25','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (313,'2016-03-17 10:25:26','2016-03-17 10:25:26','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (314,'2016-03-17 11:40:31','2016-03-17 11:40:31','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (315,'2016-03-17 11:42:20','2016-03-17 11:42:20','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (316,'2016-03-17 14:46:12','2016-03-17 14:46:12','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (317,'2016-03-17 16:18:59','2016-03-17 16:18:59','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (318,'2016-03-17 19:49:00','2016-03-17 19:49:00','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (319,'2016-03-18 08:03:28','2016-03-18 08:03:28','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (320,'2016-03-18 08:21:55','2016-03-18 08:21:55','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (321,'2016-03-18 09:08:32','2016-03-18 09:08:32','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (322,'2016-03-18 09:15:15','2016-03-18 09:15:15','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (323,'2016-03-18 10:25:00','2016-03-18 10:25:00','user.logout','User  logged out','{\"user_id\":null}');
INSERT INTO `audits` VALUES (324,'2016-03-18 10:38:29','2016-03-18 10:38:29','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (325,'2016-03-18 10:38:49','2016-03-18 10:38:49','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (326,'2016-03-18 10:42:42','2016-03-18 10:42:42','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (327,'2016-03-18 10:54:40','2016-03-18 10:54:40','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (328,'2016-03-18 10:55:33','2016-03-18 10:55:33','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (329,'2016-03-18 11:41:26','2016-03-18 11:41:26','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (330,'2016-03-18 11:41:43','2016-03-18 11:41:43','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (331,'2016-03-18 12:29:21','2016-03-18 12:29:21','user.logout','User  logged out','{\"user_id\":null}');
INSERT INTO `audits` VALUES (332,'2016-03-18 14:38:45','2016-03-18 14:38:45','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (333,'2016-03-19 06:26:53','2016-03-19 06:26:53','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (334,'2016-03-19 07:52:22','2016-03-19 07:52:22','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (335,'2016-03-19 08:34:58','2016-03-19 08:34:58','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (336,'2016-03-19 12:05:01','2016-03-19 12:05:01','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (337,'2016-03-19 13:08:14','2016-03-19 13:08:14','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (338,'2016-03-19 15:22:22','2016-03-19 15:22:22','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (339,'2016-03-19 20:39:49','2016-03-19 20:39:49','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (340,'2016-03-20 06:43:25','2016-03-20 06:43:25','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (341,'2016-03-20 10:45:12','2016-03-20 10:45:12','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (342,'2016-03-20 14:14:28','2016-03-20 14:14:28','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (343,'2016-03-20 16:02:12','2016-03-20 16:02:12','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (344,'2016-03-20 19:51:04','2016-03-20 19:51:04','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (345,'2016-03-20 21:01:55','2016-03-20 21:01:55','user.logout','User icukac logged out','{\"user_id\":2}');
INSERT INTO `audits` VALUES (346,'2016-03-20 21:02:05','2016-03-20 21:02:05','user.login','User luka logged in','{\"user_id\":6}');
INSERT INTO `audits` VALUES (347,'2016-03-20 21:02:40','2016-03-20 21:02:40','user.logout','User luka logged out','{\"user_id\":6}');
INSERT INTO `audits` VALUES (348,'2016-03-20 21:02:44','2016-03-20 21:02:44','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (349,'2016-03-21 07:16:44','2016-03-21 07:16:44','user.login','User luka logged in','{\"user_id\":6}');
INSERT INTO `audits` VALUES (350,'2016-03-21 07:48:55','2016-03-21 07:48:55','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (351,'2016-03-21 08:47:17','2016-03-21 08:47:17','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (352,'2016-03-21 08:51:03','2016-03-21 08:51:03','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (353,'2016-03-21 08:59:01','2016-03-21 08:59:01','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (354,'2016-03-21 09:33:29','2016-03-21 09:33:29','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (355,'2016-03-21 09:34:06','2016-03-21 09:34:06','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (356,'2016-03-21 09:55:32','2016-03-21 09:55:32','user.logout','User icukac logged out','{\"user_id\":2}');
INSERT INTO `audits` VALUES (357,'2016-03-21 09:55:45','2016-03-21 09:55:45','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (358,'2016-03-21 10:08:31','2016-03-21 10:08:31','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (359,'2016-03-21 10:46:10','2016-03-21 10:46:10','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (360,'2016-03-21 11:30:50','2016-03-21 11:30:50','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (361,'2016-03-21 11:53:01','2016-03-21 11:53:01','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (362,'2016-03-21 11:53:15','2016-03-21 11:53:15','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (363,'2016-03-21 12:11:26','2016-03-21 12:11:26','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (364,'2016-03-21 12:16:15','2016-03-21 12:16:15','user.logout','User icukac logged out','{\"user_id\":2}');
INSERT INTO `audits` VALUES (365,'2016-03-21 12:16:20','2016-03-21 12:16:20','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (366,'2016-03-21 12:21:28','2016-03-21 12:21:28','user.logout','User icukac logged out','{\"user_id\":2}');
INSERT INTO `audits` VALUES (367,'2016-03-21 12:21:35','2016-03-21 12:21:35','user.login','User luka logged in','{\"user_id\":7}');
INSERT INTO `audits` VALUES (368,'2016-03-21 12:21:56','2016-03-21 12:21:56','user.logout','User luka logged out','{\"user_id\":7}');
INSERT INTO `audits` VALUES (369,'2016-03-21 12:24:47','2016-03-21 12:24:47','user.login','User luka logged in','{\"user_id\":7}');
INSERT INTO `audits` VALUES (370,'2016-03-21 12:25:13','2016-03-21 12:25:13','user.logout','User luka logged out','{\"user_id\":7}');
INSERT INTO `audits` VALUES (371,'2016-03-21 14:55:19','2016-03-21 14:55:19','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (372,'2016-03-21 16:49:20','2016-03-21 16:49:20','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (373,'2016-03-21 17:08:57','2016-03-21 17:08:57','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (374,'2016-03-21 17:22:17','2016-03-21 17:22:17','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (375,'2016-03-21 21:14:13','2016-03-21 21:14:13','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (376,'2016-03-22 06:23:58','2016-03-22 06:23:58','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (377,'2016-03-22 08:15:48','2016-03-22 08:15:48','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (378,'2016-03-22 08:23:06','2016-03-22 08:23:06','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (379,'2016-03-22 09:17:50','2016-03-22 09:17:50','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (380,'2016-03-22 10:00:11','2016-03-22 10:00:11','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (381,'2016-03-22 10:34:21','2016-03-22 10:34:21','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (382,'2016-03-22 10:41:28','2016-03-22 10:41:28','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (383,'2016-03-22 10:45:40','2016-03-22 10:45:40','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (384,'2016-03-22 11:46:04','2016-03-22 11:46:04','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (385,'2016-03-22 12:15:28','2016-03-22 12:15:28','user.login','User luka logged in','{\"user_id\":7}');
INSERT INTO `audits` VALUES (386,'2016-03-22 12:22:19','2016-03-22 12:22:19','user.logout','User luka logged out','{\"user_id\":7}');
INSERT INTO `audits` VALUES (387,'2016-03-22 12:26:43','2016-03-22 12:26:43','user.login','User luka logged in','{\"user_id\":7}');
INSERT INTO `audits` VALUES (388,'2016-03-22 12:27:55','2016-03-22 12:27:55','user.logout','User luka logged out','{\"user_id\":7}');
INSERT INTO `audits` VALUES (389,'2016-03-22 12:41:52','2016-03-22 12:41:52','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (390,'2016-03-22 13:24:53','2016-03-22 13:24:53','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (391,'2016-03-22 13:25:32','2016-03-22 13:25:32','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (392,'2016-03-22 13:46:21','2016-03-22 13:46:21','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (393,'2016-03-22 13:50:13','2016-03-22 13:50:13','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (394,'2016-03-22 14:36:05','2016-03-22 14:36:05','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (395,'2016-03-22 15:00:57','2016-03-22 15:00:57','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (396,'2016-03-22 16:00:26','2016-03-22 16:00:26','user.login','User luka logged in','{\"user_id\":7}');
INSERT INTO `audits` VALUES (397,'2016-03-22 16:02:06','2016-03-22 16:02:06','user.logout','User luka logged out','{\"user_id\":7}');
INSERT INTO `audits` VALUES (398,'2016-03-22 17:07:41','2016-03-22 17:07:41','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (399,'2016-03-22 18:46:03','2016-03-22 18:46:03','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (400,'2016-03-22 20:19:22','2016-03-22 20:19:22','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (401,'2016-03-23 06:04:45','2016-03-23 06:04:45','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (402,'2016-03-23 06:56:40','2016-03-23 06:56:40','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (403,'2016-03-23 07:43:07','2016-03-23 07:43:07','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (404,'2016-03-23 08:33:18','2016-03-23 08:33:18','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (405,'2016-03-23 09:02:42','2016-03-23 09:02:42','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (406,'2016-03-23 09:47:17','2016-03-23 09:47:17','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (407,'2016-03-23 10:40:20','2016-03-23 10:40:20','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (408,'2016-03-23 11:24:18','2016-03-23 11:24:18','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (409,'2016-03-23 12:19:43','2016-03-23 12:19:43','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (410,'2016-03-23 12:59:06','2016-03-23 12:59:06','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (411,'2016-03-23 13:19:46','2016-03-23 13:19:46','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (412,'2016-03-23 14:06:39','2016-03-23 14:06:39','user.logout','User  logged out','{\"user_id\":null}');
INSERT INTO `audits` VALUES (413,'2016-03-23 16:00:52','2016-03-23 16:00:52','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (414,'2016-03-23 18:41:17','2016-03-23 18:41:17','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (415,'2016-03-23 19:05:06','2016-03-23 19:05:06','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (416,'2016-03-23 19:23:24','2016-03-23 19:23:24','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (417,'2016-03-23 20:47:18','2016-03-23 20:47:18','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (418,'2016-03-23 20:47:48','2016-03-23 20:47:48','user.logout','User icukac logged out','{\"user_id\":2}');
INSERT INTO `audits` VALUES (419,'2016-03-23 20:47:53','2016-03-23 20:47:53','user.login','User luka logged in','{\"user_id\":7}');
INSERT INTO `audits` VALUES (420,'2016-03-24 06:01:58','2016-03-24 06:01:58','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (421,'2016-03-24 06:23:59','2016-03-24 06:23:59','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (422,'2016-03-24 07:31:51','2016-03-24 07:31:51','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (423,'2016-03-24 08:47:16','2016-03-24 08:47:16','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (424,'2016-03-24 09:35:24','2016-03-24 09:35:24','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (425,'2016-03-24 10:19:32','2016-03-24 10:19:32','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (426,'2016-03-24 11:14:35','2016-03-24 11:14:35','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (427,'2016-03-24 11:21:23','2016-03-24 11:21:23','user.login','User luka logged in','{\"user_id\":7}');
INSERT INTO `audits` VALUES (428,'2016-03-24 12:58:00','2016-03-24 12:58:00','user.login','User luka logged in','{\"user_id\":7}');
INSERT INTO `audits` VALUES (429,'2016-03-24 13:09:51','2016-03-24 13:09:51','user.logout','User luka logged out','{\"user_id\":7}');
INSERT INTO `audits` VALUES (430,'2016-03-24 13:09:58','2016-03-24 13:09:58','user.logout','User  logged out','{\"user_id\":null}');
INSERT INTO `audits` VALUES (431,'2016-03-24 13:40:09','2016-03-24 13:40:09','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (432,'2016-03-24 15:11:38','2016-03-24 15:11:38','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (433,'2016-03-25 06:36:37','2016-03-25 06:36:37','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (434,'2016-03-25 07:22:03','2016-03-25 07:22:03','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (435,'2016-03-25 10:02:04','2016-03-25 10:02:04','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (436,'2016-03-25 10:19:21','2016-03-25 10:19:21','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (437,'2016-03-25 10:36:46','2016-03-25 10:36:46','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (438,'2016-03-25 10:36:49','2016-03-25 10:36:49','user.logout','User admin logged out','{\"user_id\":1}');
INSERT INTO `audits` VALUES (439,'2016-03-25 11:56:19','2016-03-25 11:56:19','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (440,'2016-03-25 12:42:04','2016-03-25 12:42:04','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (441,'2016-03-25 12:47:23','2016-03-25 12:47:23','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (442,'2016-03-25 13:03:08','2016-03-25 13:03:08','user.login','User luka logged in','{\"user_id\":7}');
INSERT INTO `audits` VALUES (443,'2016-03-25 13:04:30','2016-03-25 13:04:30','user.logout','User luka logged out','{\"user_id\":7}');
INSERT INTO `audits` VALUES (444,'2016-03-25 13:19:44','2016-03-25 13:19:44','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (445,'2016-03-25 13:59:11','2016-03-25 13:59:11','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (446,'2016-03-25 14:53:56','2016-03-25 14:53:56','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (447,'2016-03-25 18:38:15','2016-03-25 18:38:15','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (448,'2016-03-25 20:27:54','2016-03-25 20:27:54','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (449,'2016-03-25 20:28:15','2016-03-25 20:28:15','user.login','User luka logged in','{\"user_id\":7}');
INSERT INTO `audits` VALUES (450,'2016-03-25 20:38:36','2016-03-25 20:38:36','user.logout','User luka logged out','{\"user_id\":7}');
INSERT INTO `audits` VALUES (451,'2016-03-25 21:05:46','2016-03-25 21:05:46','user.login','User luka logged in','{\"user_id\":7}');
INSERT INTO `audits` VALUES (452,'2016-03-25 21:06:49','2016-03-25 21:06:49','user.logout','User luka logged out','{\"user_id\":7}');
INSERT INTO `audits` VALUES (453,'2016-03-25 21:34:45','2016-03-25 21:34:45','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (454,'2016-03-26 06:57:42','2016-03-26 06:57:42','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (455,'2016-03-26 08:04:32','2016-03-26 08:04:32','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (456,'2016-03-26 08:29:37','2016-03-26 08:29:37','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (457,'2016-03-26 09:03:53','2016-03-26 09:03:53','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (458,'2016-03-26 10:26:17','2016-03-26 10:26:17','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (459,'2016-03-26 12:28:57','2016-03-26 12:28:57','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (460,'2016-03-26 15:16:44','2016-03-26 15:16:44','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (461,'2016-03-26 16:43:51','2016-03-26 16:43:51','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (462,'2016-03-26 18:57:15','2016-03-26 18:57:15','user.login','User luka logged in','{\"user_id\":7}');
INSERT INTO `audits` VALUES (463,'2016-03-26 18:58:17','2016-03-26 18:58:17','user.logout','User luka logged out','{\"user_id\":7}');
INSERT INTO `audits` VALUES (464,'2016-03-26 18:59:40','2016-03-26 18:59:40','user.login','User luka logged in','{\"user_id\":7}');
INSERT INTO `audits` VALUES (465,'2016-03-26 19:21:05','2016-03-26 19:21:05','user.logout','User luka logged out','{\"user_id\":7}');
INSERT INTO `audits` VALUES (466,'2016-03-26 19:21:08','2016-03-26 19:21:08','user.login','User luka logged in','{\"user_id\":7}');
INSERT INTO `audits` VALUES (467,'2016-03-26 19:27:55','2016-03-26 19:27:55','user.logout','User luka logged out','{\"user_id\":7}');
INSERT INTO `audits` VALUES (468,'2016-03-27 07:34:44','2016-03-27 07:34:44','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (469,'2016-03-28 07:38:14','2016-03-28 07:38:14','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (470,'2016-03-28 10:04:23','2016-03-28 10:04:23','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (471,'2016-03-28 17:49:25','2016-03-28 17:49:25','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (472,'2016-03-29 06:25:58','2016-03-29 06:25:58','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (473,'2016-03-29 06:36:51','2016-03-29 06:36:51','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (474,'2016-03-29 07:16:24','2016-03-29 07:16:24','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (475,'2016-03-29 07:53:14','2016-03-29 07:53:14','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (476,'2016-03-29 08:14:26','2016-03-29 08:14:26','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (477,'2016-03-29 09:06:25','2016-03-29 09:06:25','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (478,'2016-03-29 09:49:39','2016-03-29 09:49:39','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (479,'2016-03-29 10:44:48','2016-03-29 10:44:48','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (480,'2016-03-29 10:45:09','2016-03-29 10:45:09','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (481,'2016-03-29 11:12:39','2016-03-29 11:12:39','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (482,'2016-03-29 11:46:02','2016-03-29 11:46:02','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (483,'2016-03-29 14:01:37','2016-03-29 14:01:37','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (484,'2016-03-29 14:06:07','2016-03-29 14:06:07','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (485,'2016-03-29 14:06:07','2016-03-29 14:06:07','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (486,'2016-03-29 14:11:50','2016-03-29 14:11:50','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (487,'2016-03-29 14:45:28','2016-03-29 14:45:28','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (488,'2016-03-29 18:24:49','2016-03-29 18:24:49','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (489,'2016-03-29 19:54:43','2016-03-29 19:54:43','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (490,'2016-03-29 21:21:40','2016-03-29 21:21:40','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (491,'2016-03-30 07:16:50','2016-03-30 07:16:50','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (492,'2016-03-30 07:30:23','2016-03-30 07:30:23','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (493,'2016-03-30 09:31:27','2016-03-30 09:31:27','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (494,'2016-03-30 10:34:48','2016-03-30 10:34:48','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (495,'2016-03-30 10:36:06','2016-03-30 10:36:06','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (496,'2016-03-30 11:23:09','2016-03-30 11:23:09','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (497,'2016-03-30 11:24:47','2016-03-30 11:24:47','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (498,'2016-03-30 12:01:13','2016-03-30 12:01:13','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (499,'2016-03-30 12:20:15','2016-03-30 12:20:15','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (500,'2016-03-30 12:29:18','2016-03-30 12:29:18','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (501,'2016-03-30 14:51:49','2016-03-30 14:51:49','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (502,'2016-03-30 15:06:37','2016-03-30 15:06:37','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (503,'2016-03-30 20:14:16','2016-03-30 20:14:16','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (504,'2016-03-30 21:23:35','2016-03-30 21:23:35','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (505,'2016-03-31 06:26:00','2016-03-31 06:26:00','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (506,'2016-03-31 08:18:01','2016-03-31 08:18:01','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (507,'2016-03-31 08:55:07','2016-03-31 08:55:07','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (508,'2016-03-31 10:01:11','2016-03-31 10:01:11','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (509,'2016-03-31 11:01:52','2016-03-31 11:01:52','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (510,'2016-03-31 12:07:27','2016-03-31 12:07:27','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (511,'2016-03-31 12:16:04','2016-03-31 12:16:04','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (512,'2016-03-31 12:29:12','2016-03-31 12:29:12','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (513,'2016-03-31 12:39:57','2016-03-31 12:39:57','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (514,'2016-03-31 12:45:29','2016-03-31 12:45:29','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (515,'2016-03-31 14:38:06','2016-03-31 14:38:06','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (516,'2016-03-31 15:14:31','2016-03-31 15:14:31','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (517,'2016-03-31 19:12:07','2016-03-31 19:12:07','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (518,'2016-04-01 06:38:02','2016-04-01 06:38:02','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (519,'2016-04-01 07:56:41','2016-04-01 07:56:41','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (520,'2016-04-01 08:11:53','2016-04-01 08:11:53','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (521,'2016-04-01 08:20:49','2016-04-01 08:20:49','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (522,'2016-04-01 09:04:52','2016-04-01 09:04:52','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (523,'2016-04-01 09:19:34','2016-04-01 09:19:34','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (524,'2016-04-01 09:44:25','2016-04-01 09:44:25','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (525,'2016-04-01 12:05:03','2016-04-01 12:05:03','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (526,'2016-04-01 14:18:00','2016-04-01 14:18:00','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (527,'2016-04-01 16:05:15','2016-04-01 16:05:15','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (528,'2016-04-01 18:44:32','2016-04-01 18:44:32','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (529,'2016-04-01 20:03:14','2016-04-01 20:03:14','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (530,'2016-04-02 07:10:49','2016-04-02 07:10:49','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (531,'2016-04-02 07:52:52','2016-04-02 07:52:52','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (532,'2016-04-02 09:18:53','2016-04-02 09:18:53','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (533,'2016-04-02 14:27:21','2016-04-02 14:27:21','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (534,'2016-04-02 15:34:06','2016-04-02 15:34:06','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (535,'2016-04-03 09:02:27','2016-04-03 09:02:27','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (536,'2016-04-03 11:56:18','2016-04-03 11:56:18','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (537,'2016-04-03 13:12:07','2016-04-03 13:12:07','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (538,'2016-04-03 15:48:52','2016-04-03 15:48:52','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (539,'2016-04-03 21:00:36','2016-04-03 21:00:36','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (540,'2016-04-04 06:24:01','2016-04-04 06:24:01','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (541,'2016-04-04 07:49:06','2016-04-04 07:49:06','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (542,'2016-04-04 08:21:20','2016-04-04 08:21:20','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (543,'2016-04-04 09:18:57','2016-04-04 09:18:57','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (544,'2016-04-04 09:49:44','2016-04-04 09:49:44','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (545,'2016-04-04 09:53:29','2016-04-04 09:53:29','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (546,'2016-04-04 10:03:20','2016-04-04 10:03:20','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (547,'2016-04-04 10:24:02','2016-04-04 10:24:02','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (548,'2016-04-04 11:06:20','2016-04-04 11:06:20','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (549,'2016-04-04 13:04:34','2016-04-04 13:04:34','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (550,'2016-04-04 13:44:38','2016-04-04 13:44:38','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (551,'2016-04-04 15:02:25','2016-04-04 15:02:25','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (552,'2016-04-04 15:38:07','2016-04-04 15:38:07','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (553,'2016-04-04 19:27:33','2016-04-04 19:27:33','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (554,'2016-04-05 06:10:47','2016-04-05 06:10:47','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (555,'2016-04-05 06:54:41','2016-04-05 06:54:41','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (556,'2016-04-05 08:20:32','2016-04-05 08:20:32','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (557,'2016-04-05 10:27:13','2016-04-05 10:27:13','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (558,'2016-04-05 10:59:41','2016-04-05 10:59:41','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (559,'2016-04-05 13:39:53','2016-04-05 13:39:53','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (560,'2016-04-05 13:59:49','2016-04-05 13:59:49','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (561,'2016-04-05 14:33:03','2016-04-05 14:33:03','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (562,'2016-04-05 14:46:02','2016-04-05 14:46:02','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (563,'2016-04-05 15:21:37','2016-04-05 15:21:37','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (564,'2016-04-05 16:27:43','2016-04-05 16:27:43','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (565,'2016-04-05 16:27:50','2016-04-05 16:27:50','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (566,'2016-04-06 06:17:43','2016-04-06 06:17:43','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (567,'2016-04-06 07:10:18','2016-04-06 07:10:18','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (568,'2016-04-06 07:49:07','2016-04-06 07:49:07','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (569,'2016-04-06 08:26:59','2016-04-06 08:26:59','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (570,'2016-04-06 09:27:43','2016-04-06 09:27:43','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (571,'2016-04-06 09:35:19','2016-04-06 09:35:19','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (572,'2016-04-06 12:12:19','2016-04-06 12:12:19','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (573,'2016-04-06 13:19:25','2016-04-06 13:19:25','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (574,'2016-04-06 22:22:01','2016-04-06 22:22:01','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (575,'2016-04-07 08:45:46','2016-04-07 08:45:46','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (576,'2016-04-07 10:00:12','2016-04-07 10:00:12','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (577,'2016-04-07 10:40:43','2016-04-07 10:40:43','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (578,'2016-04-07 11:47:41','2016-04-07 11:47:41','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (579,'2016-04-07 14:15:35','2016-04-07 14:15:35','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (580,'2016-04-07 15:40:17','2016-04-07 15:40:17','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (581,'2016-04-07 17:17:06','2016-04-07 17:17:06','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (582,'2016-04-07 20:30:31','2016-04-07 20:30:31','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (583,'2016-04-08 06:23:56','2016-04-08 06:23:56','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (584,'2016-04-08 08:25:59','2016-04-08 08:25:59','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (585,'2016-04-08 12:58:28','2016-04-08 12:58:28','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (586,'2016-04-08 13:36:22','2016-04-08 13:36:22','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (587,'2016-04-08 14:17:27','2016-04-08 14:17:27','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (588,'2016-04-09 07:01:46','2016-04-09 07:01:46','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (589,'2016-04-09 08:35:30','2016-04-09 08:35:30','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (590,'2016-04-09 09:03:16','2016-04-09 09:03:16','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (591,'2016-04-09 09:18:08','2016-04-09 09:18:08','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (592,'2016-04-09 10:42:55','2016-04-09 10:42:55','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (593,'2016-04-09 15:54:58','2016-04-09 15:54:58','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (594,'2016-04-10 10:52:36','2016-04-10 10:52:36','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (595,'2016-04-10 14:57:08','2016-04-10 14:57:08','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (596,'2016-04-11 08:04:55','2016-04-11 08:04:55','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (597,'2016-04-11 08:17:53','2016-04-11 08:17:53','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (598,'2016-04-11 08:40:43','2016-04-11 08:40:43','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (599,'2016-04-11 08:42:33','2016-04-11 08:42:33','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (600,'2016-04-11 10:11:03','2016-04-11 10:11:03','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (601,'2016-04-11 10:13:32','2016-04-11 10:13:32','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (602,'2016-04-11 10:21:19','2016-04-11 10:21:19','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (603,'2016-04-11 11:58:17','2016-04-11 11:58:17','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (604,'2016-04-11 12:21:28','2016-04-11 12:21:28','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (605,'2016-04-11 13:16:31','2016-04-11 13:16:31','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (606,'2016-04-11 13:20:17','2016-04-11 13:20:17','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (607,'2016-04-11 13:20:34','2016-04-11 13:20:34','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (608,'2016-04-11 13:22:27','2016-04-11 13:22:27','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (609,'2016-04-11 16:01:24','2016-04-11 16:01:24','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (610,'2016-04-11 18:26:34','2016-04-11 18:26:34','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (611,'2016-04-11 19:35:36','2016-04-11 19:35:36','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (612,'2016-04-11 20:45:40','2016-04-11 20:45:40','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (613,'2016-04-12 07:38:10','2016-04-12 07:38:10','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (614,'2016-04-12 07:45:37','2016-04-12 07:45:37','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (615,'2016-04-12 10:07:30','2016-04-12 10:07:30','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (616,'2016-04-12 12:31:33','2016-04-12 12:31:33','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (617,'2016-04-12 14:08:33','2016-04-12 14:08:33','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (618,'2016-04-12 21:15:45','2016-04-12 21:15:45','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (619,'2016-04-13 06:20:33','2016-04-13 06:20:33','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (620,'2016-04-13 08:01:46','2016-04-13 08:01:46','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (621,'2016-04-13 08:54:09','2016-04-13 08:54:09','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (622,'2016-04-13 09:31:15','2016-04-13 09:31:15','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (623,'2016-04-13 12:22:43','2016-04-13 12:22:43','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (624,'2016-04-13 12:43:11','2016-04-13 12:43:11','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (625,'2016-04-13 13:26:04','2016-04-13 13:26:04','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (626,'2016-04-13 14:21:07','2016-04-13 14:21:07','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (627,'2016-04-13 14:42:36','2016-04-13 14:42:36','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (628,'2016-04-13 14:53:18','2016-04-13 14:53:18','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (629,'2016-04-13 15:19:55','2016-04-13 15:19:55','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (630,'2016-04-13 18:23:41','2016-04-13 18:23:41','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (631,'2016-04-13 20:54:24','2016-04-13 20:54:24','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (632,'2016-04-14 07:11:01','2016-04-14 07:11:01','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (633,'2016-04-14 07:42:42','2016-04-14 07:42:42','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (634,'2016-04-14 07:47:07','2016-04-14 07:47:07','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (635,'2016-04-14 07:58:39','2016-04-14 07:58:39','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (636,'2016-04-14 09:17:35','2016-04-14 09:17:35','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (637,'2016-04-14 09:29:51','2016-04-14 09:29:51','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (638,'2016-04-14 09:35:39','2016-04-14 09:35:39','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (639,'2016-04-14 09:55:53','2016-04-14 09:55:53','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (640,'2016-04-14 10:28:31','2016-04-14 10:28:31','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (641,'2016-04-14 11:41:01','2016-04-14 11:41:01','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (642,'2016-04-14 11:42:27','2016-04-14 11:42:27','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (643,'2016-04-14 12:30:26','2016-04-14 12:30:26','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (644,'2016-04-15 06:18:20','2016-04-15 06:18:20','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (645,'2016-04-15 07:19:08','2016-04-15 07:19:08','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (646,'2016-04-15 08:09:24','2016-04-15 08:09:24','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (647,'2016-04-15 08:29:14','2016-04-15 08:29:14','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (648,'2016-04-15 09:02:01','2016-04-15 09:02:01','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (649,'2016-04-15 10:38:17','2016-04-15 10:38:17','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (650,'2016-04-15 11:38:45','2016-04-15 11:38:45','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (651,'2016-04-15 12:21:15','2016-04-15 12:21:15','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (652,'2016-04-18 06:17:48','2016-04-18 06:17:48','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (653,'2016-04-18 08:26:59','2016-04-18 08:26:59','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (654,'2016-04-18 08:31:12','2016-04-18 08:31:12','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (655,'2016-04-18 08:45:31','2016-04-18 08:45:31','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (656,'2016-04-18 09:02:44','2016-04-18 09:02:44','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (657,'2016-04-18 09:37:09','2016-04-18 09:37:09','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (658,'2016-04-18 09:57:30','2016-04-18 09:57:30','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (659,'2016-04-18 10:38:13','2016-04-18 10:38:13','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (660,'2016-04-18 10:42:24','2016-04-18 10:42:24','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (661,'2016-04-18 11:49:00','2016-04-18 11:49:00','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (662,'2016-04-18 12:13:02','2016-04-18 12:13:02','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (663,'2016-04-18 13:05:13','2016-04-18 13:05:13','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (664,'2016-04-18 13:16:39','2016-04-18 13:16:39','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (665,'2016-04-18 14:35:34','2016-04-18 14:35:34','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (666,'2016-04-18 15:42:27','2016-04-18 15:42:27','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (667,'2016-04-19 06:52:17','2016-04-19 06:52:17','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (668,'2016-04-19 08:00:47','2016-04-19 08:00:47','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (669,'2016-04-19 08:38:20','2016-04-19 08:38:20','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (670,'2016-04-19 09:27:03','2016-04-19 09:27:03','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (671,'2016-04-19 09:59:17','2016-04-19 09:59:17','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (672,'2016-04-19 11:05:05','2016-04-19 11:05:05','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (673,'2016-04-19 11:36:54','2016-04-19 11:36:54','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (674,'2016-04-19 13:45:12','2016-04-19 13:45:12','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (675,'2016-04-19 13:45:24','2016-04-19 13:45:24','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (676,'2016-04-19 14:06:50','2016-04-19 14:06:50','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (677,'2016-04-19 14:16:16','2016-04-19 14:16:16','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (678,'2016-04-19 14:44:29','2016-04-19 14:44:29','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (679,'2016-04-19 16:08:03','2016-04-19 16:08:03','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (680,'2016-04-20 08:01:53','2016-04-20 08:01:53','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (681,'2016-04-20 11:03:27','2016-04-20 11:03:27','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (682,'2016-04-20 11:19:29','2016-04-20 11:19:29','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (683,'2016-04-20 11:25:19','2016-04-20 11:25:19','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (684,'2016-04-20 11:50:40','2016-04-20 11:50:40','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (685,'2016-04-20 11:55:45','2016-04-20 11:55:45','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (686,'2016-04-20 12:48:35','2016-04-20 12:48:35','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (687,'2016-04-20 13:59:18','2016-04-20 13:59:18','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (688,'2016-04-20 14:55:03','2016-04-20 14:55:03','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (689,'2016-04-20 17:07:33','2016-04-20 17:07:33','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (690,'2016-04-20 19:20:27','2016-04-20 19:20:27','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (691,'2016-04-21 07:49:31','2016-04-21 07:49:31','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (692,'2016-04-21 08:43:18','2016-04-21 08:43:18','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (693,'2016-04-21 10:58:57','2016-04-21 10:58:57','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (694,'2016-04-21 11:49:28','2016-04-21 11:49:28','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (695,'2016-04-21 12:58:58','2016-04-21 12:58:58','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (696,'2016-04-21 13:23:43','2016-04-21 13:23:43','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (697,'2016-04-21 13:27:06','2016-04-21 13:27:06','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (698,'2016-04-21 13:32:16','2016-04-21 13:32:16','user.logout','User admin logged out','{\"user_id\":1}');
INSERT INTO `audits` VALUES (699,'2016-04-21 19:00:33','2016-04-21 19:00:33','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (700,'2016-04-21 23:08:36','2016-04-21 23:08:36','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (701,'2016-04-22 06:46:06','2016-04-22 06:46:06','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (702,'2016-04-22 07:47:54','2016-04-22 07:47:54','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (703,'2016-04-22 09:46:45','2016-04-22 09:46:45','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (704,'2016-04-22 11:04:16','2016-04-22 11:04:16','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (705,'2016-04-22 11:11:42','2016-04-22 11:11:42','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (706,'2016-04-22 11:14:23','2016-04-22 11:14:23','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (707,'2016-04-22 11:17:26','2016-04-22 11:17:26','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (708,'2016-04-22 13:06:50','2016-04-22 13:06:50','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (709,'2016-04-22 14:04:11','2016-04-22 14:04:11','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (710,'2016-04-22 14:30:12','2016-04-22 14:30:12','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (711,'2016-04-22 18:15:25','2016-04-22 18:15:25','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (712,'2016-04-22 20:24:11','2016-04-22 20:24:11','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (713,'2016-04-22 22:10:11','2016-04-22 22:10:11','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (714,'2016-04-23 06:28:35','2016-04-23 06:28:35','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (715,'2016-04-23 07:18:58','2016-04-23 07:18:58','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (716,'2016-04-23 09:09:02','2016-04-23 09:09:02','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (717,'2016-04-23 12:50:07','2016-04-23 12:50:07','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (718,'2016-04-23 15:27:02','2016-04-23 15:27:02','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (719,'2016-04-23 16:21:25','2016-04-23 16:21:25','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (720,'2016-04-24 09:18:16','2016-04-24 09:18:16','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (721,'2016-04-25 07:22:31','2016-04-25 07:22:31','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (722,'2016-04-25 07:27:26','2016-04-25 07:27:26','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (723,'2016-04-25 08:08:30','2016-04-25 08:08:30','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (724,'2016-04-25 08:10:32','2016-04-25 08:10:32','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (725,'2016-04-25 08:17:09','2016-04-25 08:17:09','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (726,'2016-04-25 08:42:08','2016-04-25 08:42:08','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (727,'2016-04-25 10:32:16','2016-04-25 10:32:16','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (728,'2016-04-25 10:50:22','2016-04-25 10:50:22','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (729,'2016-04-25 10:52:42','2016-04-25 10:52:42','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (730,'2016-04-25 12:01:55','2016-04-25 12:01:55','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (731,'2016-04-25 13:23:52','2016-04-25 13:23:52','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (732,'2016-04-25 13:24:54','2016-04-25 13:24:54','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (733,'2016-04-25 13:44:33','2016-04-25 13:44:33','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (734,'2016-04-25 14:11:21','2016-04-25 14:11:21','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (735,'2016-04-25 14:42:47','2016-04-25 14:42:47','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (736,'2016-04-25 15:09:37','2016-04-25 15:09:37','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (737,'2016-04-25 15:58:27','2016-04-25 15:58:27','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (738,'2016-04-26 07:47:32','2016-04-26 07:47:32','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (739,'2016-04-26 08:11:47','2016-04-26 08:11:47','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (740,'2016-04-26 09:23:48','2016-04-26 09:23:48','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (741,'2016-04-26 09:39:48','2016-04-26 09:39:48','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (742,'2016-04-26 10:10:32','2016-04-26 10:10:32','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (743,'2016-04-26 10:24:44','2016-04-26 10:24:44','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (744,'2016-04-26 13:11:14','2016-04-26 13:11:14','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (745,'2016-04-26 14:31:10','2016-04-26 14:31:10','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (746,'2016-04-26 14:40:02','2016-04-26 14:40:02','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (747,'2016-04-26 15:49:25','2016-04-26 15:49:25','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (748,'2016-04-27 06:33:38','2016-04-27 06:33:38','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (749,'2016-04-27 07:40:35','2016-04-27 07:40:35','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (750,'2016-04-27 08:28:08','2016-04-27 08:28:08','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (751,'2016-04-27 09:07:05','2016-04-27 09:07:05','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (752,'2016-04-27 09:29:53','2016-04-27 09:29:53','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (753,'2016-04-27 10:11:44','2016-04-27 10:11:44','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (754,'2016-04-27 10:32:33','2016-04-27 10:32:33','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (755,'2016-04-27 12:04:27','2016-04-27 12:04:27','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (756,'2016-04-27 12:46:05','2016-04-27 12:46:05','user.logout','User  logged out','{\"user_id\":null}');
INSERT INTO `audits` VALUES (757,'2016-04-27 12:54:12','2016-04-27 12:54:12','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (758,'2016-04-27 14:34:50','2016-04-27 14:34:50','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (759,'2016-04-27 14:35:09','2016-04-27 14:35:09','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (760,'2016-04-27 14:38:13','2016-04-27 14:38:13','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (761,'2016-04-27 14:49:26','2016-04-27 14:49:26','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (762,'2016-04-27 17:33:54','2016-04-27 17:33:54','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (763,'2016-04-27 20:50:44','2016-04-27 20:50:44','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (764,'2016-04-28 06:48:03','2016-04-28 06:48:03','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (765,'2016-04-28 07:51:22','2016-04-28 07:51:22','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (766,'2016-04-28 07:51:37','2016-04-28 07:51:37','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (767,'2016-04-28 08:16:11','2016-04-28 08:16:11','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (768,'2016-04-28 08:57:01','2016-04-28 08:57:01','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (769,'2016-04-28 09:04:55','2016-04-28 09:04:55','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (770,'2016-04-28 09:55:34','2016-04-28 09:55:34','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (771,'2016-04-28 10:59:57','2016-04-28 10:59:57','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (772,'2016-04-28 11:18:46','2016-04-28 11:18:46','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (773,'2016-04-28 11:20:34','2016-04-28 11:20:34','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (774,'2016-04-28 11:46:33','2016-04-28 11:46:33','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (775,'2016-04-28 13:07:39','2016-04-28 13:07:39','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (776,'2016-04-28 13:07:52','2016-04-28 13:07:52','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (777,'2016-04-28 13:09:36','2016-04-28 13:09:36','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (778,'2016-04-28 13:19:39','2016-04-28 13:19:39','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (779,'2016-04-28 17:50:47','2016-04-28 17:50:47','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (780,'2016-04-28 20:15:19','2016-04-28 20:15:19','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (781,'2016-04-28 20:25:10','2016-04-28 20:25:10','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (782,'2016-04-29 06:35:29','2016-04-29 06:35:29','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (783,'2016-04-29 07:22:04','2016-04-29 07:22:04','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (784,'2016-04-29 08:33:44','2016-04-29 08:33:44','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (785,'2016-04-29 09:11:29','2016-04-29 09:11:29','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (786,'2016-04-29 10:00:27','2016-04-29 10:00:27','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (787,'2016-04-29 10:07:33','2016-04-29 10:07:33','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (788,'2016-04-29 10:16:39','2016-04-29 10:16:39','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (789,'2016-04-29 11:53:34','2016-04-29 11:53:34','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (790,'2016-04-29 14:43:04','2016-04-29 14:43:04','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (791,'2016-04-29 14:46:24','2016-04-29 14:46:24','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (792,'2016-04-29 15:50:01','2016-04-29 15:50:01','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (793,'2016-04-29 16:44:45','2016-04-29 16:44:45','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (794,'2016-04-29 17:33:05','2016-04-29 17:33:05','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (795,'2016-04-29 18:07:56','2016-04-29 18:07:56','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (796,'2016-04-30 07:14:47','2016-04-30 07:14:47','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (797,'2016-04-30 09:02:59','2016-04-30 09:02:59','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (798,'2016-04-30 09:41:38','2016-04-30 09:41:38','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (799,'2016-04-30 09:52:37','2016-04-30 09:52:37','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (800,'2016-05-01 09:23:30','2016-05-01 09:23:30','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (801,'2016-05-02 06:45:21','2016-05-02 06:45:21','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (802,'2016-05-02 07:18:52','2016-05-02 07:18:52','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (803,'2016-05-02 07:31:48','2016-05-02 07:31:48','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (804,'2016-05-02 08:31:53','2016-05-02 08:31:53','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (805,'2016-05-02 08:43:00','2016-05-02 08:43:00','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (806,'2016-05-02 09:29:24','2016-05-02 09:29:24','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (807,'2016-05-02 10:55:38','2016-05-02 10:55:38','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (808,'2016-05-02 11:33:16','2016-05-02 11:33:16','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (809,'2016-05-02 11:57:03','2016-05-02 11:57:03','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (810,'2016-05-02 12:34:26','2016-05-02 12:34:26','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (811,'2016-05-02 13:20:40','2016-05-02 13:20:40','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (812,'2016-05-02 13:34:04','2016-05-02 13:34:04','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (813,'2016-05-03 06:26:07','2016-05-03 06:26:07','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (814,'2016-05-03 09:42:04','2016-05-03 09:42:04','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (815,'2016-05-03 10:38:38','2016-05-03 10:38:38','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (816,'2016-05-03 11:41:08','2016-05-03 11:41:08','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (817,'2016-05-03 11:43:31','2016-05-03 11:43:31','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (818,'2016-05-03 11:53:26','2016-05-03 11:53:26','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (819,'2016-05-03 12:54:22','2016-05-03 12:54:22','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (820,'2016-05-03 14:01:21','2016-05-03 14:01:21','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (821,'2016-05-03 14:44:53','2016-05-03 14:44:53','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (822,'2016-05-03 14:45:05','2016-05-03 14:45:05','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (823,'2016-05-03 14:54:41','2016-05-03 14:54:41','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (824,'2016-05-03 15:14:07','2016-05-03 15:14:07','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (825,'2016-05-03 15:39:15','2016-05-03 15:39:15','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (826,'2016-05-03 16:51:39','2016-05-03 16:51:39','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (827,'2016-05-03 20:37:28','2016-05-03 20:37:28','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (828,'2016-05-04 06:45:18','2016-05-04 06:45:18','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (829,'2016-05-04 07:18:28','2016-05-04 07:18:28','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (830,'2016-05-04 08:07:25','2016-05-04 08:07:25','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (831,'2016-05-04 08:12:26','2016-05-04 08:12:26','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (832,'2016-05-04 09:49:44','2016-05-04 09:49:44','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (833,'2016-05-04 10:09:05','2016-05-04 10:09:05','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (834,'2016-05-04 11:59:09','2016-05-04 11:59:09','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (835,'2016-05-04 12:05:13','2016-05-04 12:05:13','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (836,'2016-05-04 12:56:52','2016-05-04 12:56:52','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (837,'2016-05-04 13:43:02','2016-05-04 13:43:02','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (838,'2016-05-04 14:22:20','2016-05-04 14:22:20','user.login','User boris logged in','{\"user_id\":4}');
INSERT INTO `audits` VALUES (839,'2016-05-04 14:49:29','2016-05-04 14:49:29','user.logout','User boris logged out','{\"user_id\":4}');
INSERT INTO `audits` VALUES (840,'2016-05-04 16:11:06','2016-05-04 16:11:06','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (841,'2016-05-04 16:41:18','2016-05-04 16:41:18','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (842,'2016-05-04 17:07:33','2016-05-04 17:07:33','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (843,'2016-05-04 19:14:15','2016-05-04 19:14:15','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (844,'2016-05-04 19:47:41','2016-05-04 19:47:41','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (845,'2016-05-04 20:18:28','2016-05-04 20:18:28','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (846,'2016-05-04 20:43:38','2016-05-04 20:43:38','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (847,'2016-05-05 06:51:32','2016-05-05 06:51:32','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (848,'2016-05-05 07:10:46','2016-05-05 07:10:46','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (849,'2016-05-05 07:52:48','2016-05-05 07:52:48','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (850,'2016-05-05 08:24:55','2016-05-05 08:24:55','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (851,'2016-05-05 09:01:00','2016-05-05 09:01:00','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (852,'2016-05-05 10:21:24','2016-05-05 10:21:24','user.login','User icukac logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (853,'2016-05-05 10:40:24','2016-05-05 10:40:24','user.login','User icukac logged in','{\"user_id\":2}');
/*!40000 ALTER TABLE `audits` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table bookings
#

DROP TABLE IF EXISTS `bookings`;
CREATE TABLE `bookings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `booked_by` int(10) unsigned NOT NULL,
  `from` datetime DEFAULT NULL,
  `to` datetime DEFAULT NULL,
  `vehicle_id` int(10) unsigned NOT NULL,
  `km_start` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `km_end` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `price` double(8,2) NOT NULL,
  `client_id` int(10) unsigned DEFAULT NULL,
  `partner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `paid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `related_doc` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `partial_amount` double(8,2) NOT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT '0',
  `service_type` tinyint(4) NOT NULL DEFAULT '10',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table bookings
#
LOCK TABLES `bookings` WRITE;
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;

INSERT INTO `bookings` VALUES (1,'2015-12-17 20:16:03','2015-12-28 13:12:21',0,'2015-12-19 08:00:00','2015-12-21 08:00:00',3,'119194','119654',975,3,'1','','',20,'2015482',0,2,10);
INSERT INTO `bookings` VALUES (2,'2015-12-17 20:20:08','2015-12-28 12:18:53',0,'2015-12-22 08:00:00','2015-12-23 08:00:00',15,'','',478,4,'1','','Od Baotića-Gratis',20,'2015481',0,2,10);
INSERT INTO `bookings` VALUES (3,'2015-12-18 11:08:59','2016-01-09 09:37:55',0,'2015-12-09 12:00:00','2015-12-31 12:00:00',6,'28000','30374',6897,9,'1','','- vozilo vraćeno 30.12.2015 u 17.00\r\n',20,'2015470',0,2,10);
INSERT INTO `bookings` VALUES (4,'2015-12-18 11:19:46','2016-01-12 20:00:03',0,'2016-01-02 08:00:00','2016-01-11 08:00:00',5,'86944','89431',3240,10,'2','','',20,'Bez računa',0,2,10);
INSERT INTO `bookings` VALUES (5,'2015-12-18 11:30:57','2016-01-10 07:54:27',0,'2015-12-11 08:00:00','2016-01-10 07:00:00',7,'','',9843,11,'1','','',20,'2015456',0,2,10);
INSERT INTO `bookings` VALUES (6,'2015-12-18 11:31:47','2016-04-28 11:04:26',0,'2016-01-10 08:00:00','2016-02-09 07:00:00',7,'229130','230755',9843,11,'1','','',1,'2016010',0,2,10);
INSERT INTO `bookings` VALUES (7,'2015-12-18 11:32:45','2016-01-10 07:54:16',0,'2015-12-11 07:00:00','2016-01-10 07:00:00',10,'','',9843,11,'1','','',20,'2015456',0,2,10);
INSERT INTO `bookings` VALUES (8,'2015-12-18 11:34:34','2016-02-08 08:07:41',0,'2016-01-10 08:00:00','2016-02-09 07:00:00',10,'118889','119637',9843,11,'1','','',20,'2016009',0,2,10);
INSERT INTO `bookings` VALUES (9,'2015-12-18 11:35:25','2016-01-26 07:31:41',0,'2015-12-26 08:00:00','2016-01-25 07:00:00',9,'114123','121678',9843,11,'1','','',20,'2015457',0,2,10);
INSERT INTO `bookings` VALUES (10,'2015-12-18 11:37:17','2016-02-24 21:24:13',0,'2016-01-25 08:00:00','2016-02-24 07:00:00',9,'121678','124058',9843,11,'1','','',1,'2016011',0,2,10);
INSERT INTO `bookings` VALUES (11,'2015-12-18 11:39:47','2015-12-28 12:18:39',0,'2015-12-15 08:00:00','2015-12-19 08:00:00',8,'','',1347,12,'1','','Na istoj ponudi \r\ni najam u 1.mj.2016',20,'2015474',0,2,10);
INSERT INTO `bookings` VALUES (12,'2015-12-18 11:43:59','2016-01-29 13:00:01',0,'2016-01-09 08:00:00','2016-01-31 07:00:00',8,'220134','223021',7218,12,'1','','',20,'2015474',0,2,10);
INSERT INTO `bookings` VALUES (14,'2015-12-18 11:50:09','2016-01-02 10:01:48',0,'2015-12-27 12:00:00','2016-01-02 10:00:00',8,'219761','220134',2580,13,'2','','',20,'2015299',0,2,10);
INSERT INTO `bookings` VALUES (15,'2015-12-18 11:54:26','2016-01-26 07:32:45',0,'2016-01-01 08:00:00','2016-01-11 08:00:00',11,'10','1941',4875,15,'1','','',20,'2015459',0,2,10);
INSERT INTO `bookings` VALUES (16,'2015-12-18 11:56:00','2016-01-18 08:38:51',0,'2016-01-15 07:00:00','2016-01-18 08:00:00',11,'1941','3200',1500,14,'2','','',20,'Bez računa',0,2,10);
INSERT INTO `bookings` VALUES (17,'2015-12-18 12:05:17','2016-03-01 06:47:06',0,'2016-02-01 08:00:00','2016-03-01 07:00:00',8,'223021','228560',9515,12,'1','','',20,'2015475',0,2,10);
INSERT INTO `bookings` VALUES (18,'2015-12-18 12:10:49','2015-12-30 16:19:23',0,'2015-12-01 09:00:00','2015-12-31 08:00:00',5,'83000','86944',9375,16,'1','','- vračen 30.12.2015 u 16.15',20,'239-1-1',0,2,10);
INSERT INTO `bookings` VALUES (19,'2015-12-19 13:28:23','2015-12-29 14:53:33',0,'2015-12-22 07:30:00','2015-12-25 09:00:00',3,'119654','120946',1375,17,'2','','\r\n\r\n',20,'2015302',500,2,10);
INSERT INTO `bookings` VALUES (20,'2015-12-22 14:08:56','2016-02-02 23:10:44',0,'2015-12-22 10:00:00','2015-12-22 11:00:00',2,'280001','',3072,741,'1','','',20,'2015486',0,2,30);
INSERT INTO `bookings` VALUES (22,'2015-12-23 14:54:13','2016-01-07 08:41:33',0,'2015-12-29 12:00:00','2016-01-06 11:05:00',3,'120982','122930',3000,1334,'2','','- povratak 7.1. u 8.00',20,'2015303',0,2,10);
INSERT INTO `bookings` VALUES (26,'2015-12-28 11:52:18','2015-12-29 11:51:33',0,'2015-12-28 13:00:00','2015-12-29 10:00:00',3,'120946','120982',487,1338,'1','','',20,'2015487',0,2,10);
INSERT INTO `bookings` VALUES (27,'2015-12-29 12:57:15','2016-01-26 07:32:25',0,'2016-01-10 07:00:00','2016-01-15 13:00:00',6,'30931','33832',13199,1340,'2','','',20,'2015305',1000,2,20);
INSERT INTO `bookings` VALUES (28,'2015-12-30 12:13:04','2016-01-18 07:00:59',0,'2016-01-04 08:00:00','2016-01-18 08:00:00',16,'','',5250,12,'1','','- Blu Sun Alias 3500kn',20,'2015488',0,2,10);
INSERT INTO `bookings` VALUES (29,'2016-01-04 13:17:19','2016-01-04 13:17:19',0,'2016-01-04 01:00:00','1970-01-01 01:00:00',2,'','',0,0,'1','','prodaja vozila',1,'',0,1,30);
INSERT INTO `bookings` VALUES (30,'2016-01-04 13:21:26','2016-01-08 15:51:45',0,'2016-01-04 01:00:00','0015-01-05 01:00:00',2,'','',0,0,'1','','Prodaja vozila',1,'',0,1,30);
INSERT INTO `bookings` VALUES (31,'2016-01-04 13:25:23','2016-02-02 23:11:18',0,'2016-01-04 01:00:00','2015-01-05 01:00:00',2,'','',38250,1342,'1','','Prodaja vozila',20,'',0,2,30);
INSERT INTO `bookings` VALUES (32,'2016-01-04 16:15:07','2016-01-12 19:59:26',0,'2016-01-05 08:00:00','2016-01-05 20:00:00',6,'30374','30931',2000,1343,'1','','',20,'2016002',0,2,20);
INSERT INTO `bookings` VALUES (33,'2016-01-06 18:24:40','2016-01-12 13:03:38',0,'2016-01-07 09:00:00','2016-01-09 09:00:00',3,'122930','123101',810,1344,'2','','',20,'2016001',0,2,10);
INSERT INTO `bookings` VALUES (34,'2016-01-08 10:44:08','2016-01-08 10:44:08',0,'2015-09-18 12:00:00','2015-09-19 08:00:00',0,'','',500,145,'1','','',1,'',0,2,10);
INSERT INTO `bookings` VALUES (35,'2016-01-08 10:45:21','2016-04-14 10:39:17',0,'2015-09-18 12:00:00','2015-09-19 08:00:00',10,'','',500,145,'1','','',20,'',0,2,10);
INSERT INTO `bookings` VALUES (36,'2016-01-08 10:47:24','2016-04-14 10:39:37',0,'2015-09-26 14:00:00','2015-09-27 14:00:00',9,'','',500,145,'1','','',20,'',0,2,10);
INSERT INTO `bookings` VALUES (37,'2016-01-08 10:55:59','2016-02-03 14:38:48',0,'2015-11-11 01:00:00','2015-12-11 01:00:00',10,'','',19687.99,11,'1','','',20,'2015436',0,2,10);
INSERT INTO `bookings` VALUES (38,'2016-01-08 11:00:00','2016-02-03 14:39:38',0,'2015-11-26 01:00:00','2015-12-26 01:00:00',9,'','',9843,11,'1','','',20,'2015446',0,2,10);
INSERT INTO `bookings` VALUES (39,'2016-01-08 12:29:03','2016-01-24 12:01:56',0,'2016-01-22 10:30:00','2016-01-24 10:00:00',3,'124527','125420',1095,1346,'2','','',20,'2016004',0,2,10);
INSERT INTO `bookings` VALUES (40,'2016-01-08 15:44:44','2016-02-08 10:19:57',0,'2016-02-02 08:00:00','2016-02-08 08:00:00',3,'126046','126969',2850,1347,'1','','',20,'2016012',0,2,10);
INSERT INTO `bookings` VALUES (41,'2016-01-08 18:59:02','2016-01-15 15:18:01',0,'2016-01-11 08:00:00','2016-01-12 08:00:00',5,'89431','89837',500,1348,'2','','',20,'2016005',0,2,10);
INSERT INTO `bookings` VALUES (42,'2016-01-13 09:08:14','2016-01-14 13:03:43',0,'2016-01-13 08:00:00','2016-01-14 08:00:00',5,'89837','90055',405,1350,'2','','',20,'2016007',0,2,10);
INSERT INTO `bookings` VALUES (43,'2016-01-14 07:22:20','2016-02-06 07:57:12',0,'2016-02-06 10:00:00','2016-02-07 10:00:00',16,'','',470,1351,'2','','- Lovigo 350kn nasi zauzeti',20,'2016008',0,2,10);
INSERT INTO `bookings` VALUES (44,'2016-01-14 10:55:48','2016-01-21 12:56:27',0,'2016-01-18 12:00:00','2016-01-20 09:00:00',16,'','',750,12,'1','','-Blue sun',20,'2016015',0,2,10);
INSERT INTO `bookings` VALUES (45,'2016-01-14 14:58:25','2016-02-01 13:44:09',0,'2016-01-23 12:00:00','2016-01-29 13:00:00',4,'208518','211934',3800,1352,'2','','- platio 250 eura a u ponedaljek uplaćuje 1900 na račun.',20,'2016013',1900,2,10);
INSERT INTO `bookings` VALUES (46,'2016-01-18 12:42:32','2016-01-20 15:25:21',0,'2016-01-18 13:00:00','2016-01-20 12:00:00',3,'123201','124527',1375,1353,'1','','',20,'2016020',0,2,10);
INSERT INTO `bookings` VALUES (47,'2016-01-19 15:08:40','2016-01-24 09:51:24',0,'2016-01-21 18:00:00','2016-01-23 18:00:00',5,'90175','91509',1500,360,'1','','',20,'2016016',0,2,10);
INSERT INTO `bookings` VALUES (48,'2016-01-20 11:08:04','2016-01-26 13:07:20',0,'2016-01-22 10:00:00','2016-01-26 10:00:00',6,'34000','35135',2100,1354,'1','','',20,'2016021',0,2,10);
INSERT INTO `bookings` VALUES (49,'2016-01-21 13:03:39','2016-01-28 07:58:45',0,'2016-01-26 08:00:00','2016-01-28 08:00:00',3,'125497','125945',975,1355,'1','','',20,'2016022',0,2,10);
INSERT INTO `bookings` VALUES (50,'2016-01-22 09:00:18','2016-01-22 15:06:58',0,'2016-01-22 10:00:00','2016-01-23 08:00:00',4,'207318','207518',400,1356,'2','','',20,'bez računa',0,2,10);
INSERT INTO `bookings` VALUES (52,'2016-01-25 10:38:09','2016-01-27 07:26:25',0,'2016-01-26 07:00:00','2016-01-27 07:00:00',5,'91509','91824',483,795,'1','','',20,'2016025',0,2,10);
INSERT INTO `bookings` VALUES (53,'2016-01-25 10:52:10','2016-02-03 15:22:51',0,'2016-01-29 18:00:00','2016-01-30 17:00:00',3,'125945','126046',607,1358,'2','','',20,'2016015',0,2,10);
INSERT INTO `bookings` VALUES (54,'2016-01-25 13:40:23','2016-02-16 12:56:01',0,'2016-02-01 08:00:00','2016-02-04 08:00:00',16,'10','1200',1875,1359,'1','','- Besttravel- Ferara',20,'2016028',0,2,10);
INSERT INTO `bookings` VALUES (55,'2016-01-25 13:55:10','2016-03-22 13:41:48',0,'2016-01-25 12:00:00','2016-01-25 12:00:00',10,'','',407,1323,'1','','Javili 7.3 da su platili',20,'2016027',0,2,30);
INSERT INTO `bookings` VALUES (56,'2016-01-26 12:52:43','2016-01-29 16:16:19',0,'2016-01-27 07:15:00','2016-01-28 07:00:00',11,'3200','3600',500,1361,'2','','',20,'Bez računa',0,2,10);
INSERT INTO `bookings` VALUES (57,'2016-01-26 12:53:48','2016-02-07 20:08:06',0,'2016-01-26 13:00:00','2016-01-30 09:00:00',6,'35335','38341',2700,1360,'2','','',20,'Bez računa',0,2,10);
INSERT INTO `bookings` VALUES (58,'2016-01-26 14:45:49','2016-01-28 08:00:41',0,'2016-01-27 08:00:00','2016-01-28 08:00:00',5,'91824','91905',400,1362,'2','','Zagreb locco',20,'Bez računa',0,2,10);
INSERT INTO `bookings` VALUES (59,'2016-01-27 09:43:32','2016-02-03 14:06:09',0,'2016-01-28 08:00:00','2016-01-29 08:00:00',5,'91905','92300',455,1363,'2','','- plaća do petka, veza BIJUK HPC',20,'2016019',0,2,10);
INSERT INTO `bookings` VALUES (61,'2016-01-27 10:24:11','2016-02-07 19:41:38',0,'2016-02-07 08:00:00','2016-02-08 08:00:00',6,'38341','38461',485,1358,'2','','',20,'2016020',0,2,10);
INSERT INTO `bookings` VALUES (62,'2016-01-28 10:28:39','2016-01-28 10:52:32',0,'2016-01-28 10:00:00','2016-01-28 10:00:00',11,'','',1000,1361,'2','','- franšiza ',20,'Bez računa',0,2,30);
INSERT INTO `bookings` VALUES (65,'2016-02-01 12:11:48','2016-02-02 14:12:37',0,'2016-02-02 08:00:00','2016-02-02 10:45:00',4,'211934','212032',362.5,1355,'1','','',20,'2016032',0,2,10);
INSERT INTO `bookings` VALUES (66,'2016-02-02 11:19:31','2016-02-02 11:19:31',0,'1970-01-01 01:00:00','1970-01-01 01:00:00',0,'','',12150,88,'1','','DPK-Osijek',1,'',0,0,30);
INSERT INTO `bookings` VALUES (67,'2016-02-02 11:22:41','2016-02-02 11:22:41',0,'1970-01-01 01:00:00','1970-01-01 01:00:00',0,'','',12150,88,'1','','DPK-Osijek',1,'',0,2,30);
INSERT INTO `bookings` VALUES (68,'2016-02-02 13:19:14','2016-02-02 13:19:14',0,'1970-01-01 01:00:00','1970-01-01 01:00:00',0,'','',122,88,'1','','DPK-Osijek',1,'',0,2,30);
INSERT INTO `bookings` VALUES (69,'2016-02-02 13:20:13','2016-02-02 13:20:13',0,'2016-02-02 08:00:00','2016-02-02 12:00:00',0,'','',122,88,'1','','DPK-Osijek',1,'',0,2,30);
INSERT INTO `bookings` VALUES (70,'2016-02-02 13:21:50','2016-02-10 12:13:11',0,'2016-02-02 08:00:00','2016-02-02 12:00:00',15,'','',122,88,'1','','DPK-Osijek',20,'2016034',0,2,30);
INSERT INTO `bookings` VALUES (71,'2016-02-02 13:53:13','2016-03-11 13:55:38',0,'2016-02-02 10:00:00','2016-02-02 12:00:00',16,'','',5716,1365,'1','','Popravak AUDI A6\r\nCroatia osiguranje uplatilo 4.572,30 kn',20,'',0,2,30);
INSERT INTO `bookings` VALUES (72,'2016-02-03 12:33:15','2016-03-10 07:36:14',0,'2016-02-09 10:00:00','2016-03-10 01:00:00',7,'230755','232255',9843,11,'1','','',1,'2016036',0,2,10);
INSERT INTO `bookings` VALUES (73,'2016-02-03 12:37:44','2016-03-10 07:35:47',0,'2016-02-09 10:00:00','2016-03-10 10:00:00',10,'119637','121100',9843,11,'1','','',1,'2016035',0,2,10);
INSERT INTO `bookings` VALUES (74,'2016-02-04 10:17:39','2016-02-25 20:42:55',0,'2016-02-05 08:00:00','2016-02-20 08:00:00',16,'','',5437.5,16,'1','','- dio najma pokrivamo sa Blu Sun \r\n- na istoj ponudi i trafic od 20.02\r\n- 30 dana odgoda, plaćanje po računu\r\n- 3750 Blu Sun kad anama sjedne',20,'2016033',0,2,10);
INSERT INTO `bookings` VALUES (75,'2016-02-05 08:34:38','2016-03-06 14:19:39',0,'2016-02-20 08:00:00','2016-03-06 08:00:00',11,'3769','5430',5437.5,16,'1','','- na istoj ponudi i partnerski kombi od \r\n05.2./20.02.2016',20,'2016033',0,2,10);
INSERT INTO `bookings` VALUES (76,'2016-02-06 14:51:16','2016-02-15 08:05:36',0,'2016-02-13 08:00:00','2016-02-15 08:00:00',3,'127082','128335',1160,1366,'2','','',20,'2016033',0,2,10);
INSERT INTO `bookings` VALUES (77,'2016-02-09 11:44:53','2016-02-16 11:23:14',0,'2016-02-11 09:00:00','2016-02-13 07:00:00',3,'126969','127082',978.75,1367,'1','','',20,'2016039',0,2,10);
INSERT INTO `bookings` VALUES (78,'2016-02-09 13:24:46','2016-03-03 18:00:48',0,'2016-02-22 08:00:00','2016-03-03 08:00:00',6,'38520','39960',4331.25,1369,'1','','',20,'2016037',0,2,10);
INSERT INTO `bookings` VALUES (79,'2016-02-10 07:05:02','2016-03-22 10:52:29',0,'2016-03-15 09:00:00','2016-03-22 09:00:00',3,'134470','137729',13064.4,1340,'2','','- prijevoz Toulouse + 4 dana čekanja vozi Rimay\r\n- ZABA 8000kn (900,00eur+1.133,00kn)+400,00kn gotovina\r\n- svi troškovi + Rimay = 8.400,00kn',20,'2016035',1000,2,20);
INSERT INTO `bookings` VALUES (80,'2016-02-10 13:58:44','2016-02-13 14:03:47',0,'2016-02-13 08:00:00','2016-02-13 14:00:00',6,'38461','38521',320,1370,'2','','6 sati max 100km',20,'2016028',0,2,10);
INSERT INTO `bookings` VALUES (81,'2016-02-11 09:37:32','2016-02-29 11:49:20',0,'2016-02-23 08:00:00','2016-02-29 08:00:00',3,'129305','129986',2868.75,1347,'1','','',20,'2016040',0,2,10);
INSERT INTO `bookings` VALUES (82,'2016-02-11 15:10:45','2016-03-31 06:31:24',0,'2016-03-01 08:00:00','2016-03-31 08:00:00',8,'228560','231757',9843.75,12,'1','','',20,'2016041',0,2,10);
INSERT INTO `bookings` VALUES (83,'2016-02-11 15:14:11','2016-04-29 08:00:48',0,'2016-03-30 08:00:00','2016-04-25 08:00:00',16,'','',9843.75,12,'1','','- Blu sun 25x200kn\r\n- STrojo promet plati 30 dana, a bio 25. \r\nRaziliku ćemo im umanjiti kod sljedećeg najma.\r\n',20,'2016042',0,2,10);
INSERT INTO `bookings` VALUES (86,'2016-02-15 08:35:19','2016-02-16 08:58:02',0,'2016-02-15 09:00:00','2016-02-16 09:00:00',5,'92369','92807',460,1372,'2','','',20,'Bez računa',0,2,10);
INSERT INTO `bookings` VALUES (87,'2016-02-15 10:57:09','2016-02-17 09:24:50',0,'2016-02-16 08:00:00','2016-02-17 08:00:00',4,'','',504,1373,'2','','- preko Igora Šameca',20,'2016040',0,2,10);
INSERT INTO `bookings` VALUES (88,'2016-02-19 12:04:55','2016-03-21 08:59:33',0,'2016-03-19 08:00:00','2016-03-21 08:00:00',16,'','',1072,1374,'2','','-lovigo usluge 900kn',20,'2016044',0,2,10);
INSERT INTO `bookings` VALUES (89,'2016-02-19 12:06:08','2016-03-21 08:59:18',0,'2016-03-19 08:00:00','2016-03-21 08:00:00',16,'','',1072,1374,'2','','Uzima dva kombija\r\nLovigo usluge - 900kn',20,'2016043',0,2,10);
INSERT INTO `bookings` VALUES (90,'2016-02-22 09:22:08','2016-02-29 08:47:49',0,'2016-02-26 08:00:00','2016-02-29 08:00:00',16,'','',1908,1375,'2','','- Blu Sun 1.600,00kn-plaćeno ',20,'2016045',0,2,10);
INSERT INTO `bookings` VALUES (91,'2016-02-22 11:41:31','2016-03-21 07:50:29',0,'2016-03-18 08:00:00','2016-03-21 08:00:00',5,'97138','98832',1500,1376,'2','','',20,'bez računa',0,2,10);
INSERT INTO `bookings` VALUES (92,'2016-02-22 13:06:55','2016-02-24 08:23:09',0,'2016-02-23 08:00:00','2016-02-24 08:00:00',5,'92807','93333',583.75,68,'1','','',20,'2015051',0,2,10);
INSERT INTO `bookings` VALUES (93,'2016-02-23 09:23:52','2016-03-25 07:05:27',0,'2016-03-02 18:00:00','2016-03-07 08:00:00',5,'95075','97021',2282.5,1377,'1','','',20,'2016050',0,2,10);
INSERT INTO `bookings` VALUES (94,'2016-02-24 09:02:22','2016-03-24 07:56:31',0,'2016-02-24 08:00:00','2016-03-25 08:00:00',9,'124058','126824',9843.75,11,'1','','',20,'2016054',0,2,10);
INSERT INTO `bookings` VALUES (95,'2016-02-24 14:05:47','2016-03-01 10:58:14',0,'2016-02-25 12:00:00','2016-02-27 12:00:00',5,'93333','93485',967.5,1067,'1','','',20,'2016055',0,2,10);
INSERT INTO `bookings` VALUES (96,'2016-02-25 11:20:43','2016-02-29 11:17:56',0,'2016-02-26 08:00:00','2016-02-26 08:00:00',11,'','',572,14,'2','','',20,'Bez računa',0,2,30);
INSERT INTO `bookings` VALUES (97,'2016-02-25 13:10:43','2016-02-29 13:16:02',0,'2016-02-29 09:30:00','2016-02-29 14:30:00',5,'93552','93583',297,1378,'2','','',20,'2015050',0,2,10);
INSERT INTO `bookings` VALUES (98,'2016-02-26 14:56:01','2016-03-07 14:34:52',0,'2016-02-26 16:00:00','2016-03-07 16:00:00',4,'212200','213877',4235,1379,'1','','',20,'2015057',0,2,10);
INSERT INTO `bookings` VALUES (101,'2016-02-27 13:42:35','2016-02-29 08:54:13',0,'2016-02-27 13:00:00','2016-02-28 12:00:00',5,'93486','93552',390,1381,'2','','',20,'Bez računa',0,2,10);
INSERT INTO `bookings` VALUES (102,'2016-02-29 11:27:10','2016-03-01 19:09:22',0,'2016-02-29 13:30:00','2016-03-01 13:30:00',5,'93583','94075',500,1382,'2','','- veza Sennna',20,'Bez računa',0,2,10);
INSERT INTO `bookings` VALUES (104,'2016-02-29 12:18:15','2016-04-11 11:59:35',0,'2016-03-10 08:00:00','2016-04-09 08:00:00',7,'232255','234125',9843.75,11,'1','','',1,'2016071',0,2,10);
INSERT INTO `bookings` VALUES (105,'2016-02-29 12:19:37','2016-04-11 11:59:57',0,'2016-03-10 08:00:00','2016-04-09 08:00:00',10,'121100','122272',9843.75,11,'1','','',1,'2016070',0,2,10);
INSERT INTO `bookings` VALUES (106,'2016-02-29 20:05:40','2016-03-04 17:54:49',0,'2016-03-02 08:00:00','2016-03-04 08:00:00',3,'130200','130861',978.75,1355,'1','','',20,'2016059',0,2,10);
INSERT INTO `bookings` VALUES (107,'2016-03-01 12:47:50','2016-03-14 06:21:44',0,'2016-03-12 08:00:00','2016-03-14 08:00:00',16,'','',1140,1383,'2','','- veza Igor Lekić\r\n- lovigo usluge',20,'2016056',0,2,10);
INSERT INTO `bookings` VALUES (108,'2016-03-02 09:15:08','2016-04-04 10:03:51',0,'2016-03-06 08:00:00','2016-04-06 08:00:00',11,'5430','8409',10875,16,'1','','',20,'2016069',0,2,10);
INSERT INTO `bookings` VALUES (109,'2016-03-02 15:15:56','2016-03-15 11:15:14',0,'2016-03-03 19:00:00','2016-03-04 19:00:00',6,'39960','40690',504,1373,'2','','',20,'2016060',0,2,10);
INSERT INTO `bookings` VALUES (110,'2016-03-04 15:04:54','2016-03-07 08:11:51',0,'2016-03-05 08:00:00','2016-03-06 08:00:00',3,'130861','131452',500,1384,'2','','Preko Pribanića Baotić',20,'Bez računa',0,2,10);
INSERT INTO `bookings` VALUES (111,'2016-03-04 15:06:21','2016-03-07 13:13:07',0,'2016-03-05 08:00:00','2016-03-05 14:00:00',6,'40690','40740',324,1385,'2','','',20,'2016061',0,2,10);
INSERT INTO `bookings` VALUES (112,'2016-03-07 12:09:46','2016-03-11 13:07:59',0,'2016-03-07 16:00:00','2016-03-10 16:00:00',4,'213877','214550',1270.5,1379,'1','','',20,'2016073',0,2,10);
INSERT INTO `bookings` VALUES (113,'2016-03-07 12:10:42','2016-03-08 13:38:37',0,'2016-03-07 16:00:00','2016-03-07 08:00:00',4,'','',1250,1379,'1','','Franšiza',20,'2016074',0,2,30);
INSERT INTO `bookings` VALUES (114,'2016-03-08 09:59:23','2016-03-09 09:23:05',0,'2016-03-08 10:00:00','2016-03-09 10:00:00',3,'131452','131680',483.75,1387,'1','','KOMPENZACIJA ZA MAJCE\r\n1856,25',20,'2016077',0,2,10);
INSERT INTO `bookings` VALUES (115,'2016-03-08 10:22:41','2016-04-04 07:50:47',0,'2016-04-01 08:00:00','2016-04-04 08:00:00',16,'','',1590,870,'2','','Lovigo promet 1350kn\r\n- imali mali sudar na parkingu Lovigo rjesava po osiguranju',20,'2016063',0,2,10);
INSERT INTO `bookings` VALUES (116,'2016-03-08 17:06:22','2016-03-20 06:43:58',0,'2016-03-15 18:00:00','2016-03-20 18:00:00',16,'','',3750,466,'1','','Libertas Regis \r\n3.125,00kn',20,'2016066',0,2,10);
INSERT INTO `bookings` VALUES (117,'2016-03-08 17:22:27','2016-03-14 11:20:21',0,'2016-03-10 08:00:00','2016-03-14 08:00:00',3,'131680','134394',2435,1389,'1','','',20,'2016080',0,2,10);
INSERT INTO `bookings` VALUES (118,'2016-03-09 16:16:57','2016-03-20 06:45:52',0,'2016-03-14 09:00:00','2016-03-18 09:00:00',4,'214729','216223',1584,1390,'2','','Murter, vraca kombi 17.03 u 17sati.',20,'2016062',0,2,10);
INSERT INTO `bookings` VALUES (119,'2016-03-10 08:47:23','2016-03-15 13:53:06',0,'2016-03-11 08:00:00','2016-03-12 08:00:00',5,'97021','97100',489.38,1367,'1','','',20,'2016082',0,2,10);
INSERT INTO `bookings` VALUES (120,'2016-03-10 14:39:55','2016-03-14 14:13:04',0,'2016-03-12 08:00:00','2016-03-14 08:00:00',16,'','',1200,1392,'2','','Blu Sun ALias 1000kn',20,'2016067',0,2,10);
INSERT INTO `bookings` VALUES (121,'2016-03-11 11:02:58','2016-03-12 12:55:35',0,'2016-03-10 16:00:00','2016-03-12 16:00:00',4,'214550','214729',818.13,1379,'1','','',20,'2016086',0,2,10);
INSERT INTO `bookings` VALUES (122,'2016-03-11 16:36:05','2016-03-22 14:05:18',0,'2016-03-14 08:00:00','2016-03-22 08:00:00',16,'','',5862.5,1393,'1','','Libertas Regis \r\n5.250,00kn\r\n',20,'2016088',0,2,20);
INSERT INTO `bookings` VALUES (123,'2016-03-12 13:00:08','2016-03-14 15:01:42',0,'2016-03-14 10:00:00','2016-03-14 14:00:00',3,'134394','134470',300,1394,'1','','',20,'2016090',0,2,10);
INSERT INTO `bookings` VALUES (124,'2016-03-14 08:21:34','2016-03-15 15:21:14',0,'2016-06-05 17:00:00','2016-06-10 18:00:00',10,'','',2385,1383,'2','','Rezervirao 14.03',1,'2016074',0,0,10);
INSERT INTO `bookings` VALUES (125,'2016-03-14 08:27:12','2016-04-29 07:49:35',0,'2016-04-01 08:00:00','2016-05-01 08:00:00',8,'231757','',9843.75,12,'1','','',20,'2016109',0,2,10);
INSERT INTO `bookings` VALUES (126,'2016-03-14 11:21:13','2016-03-16 13:48:02',0,'2016-03-14 08:00:00','2016-03-14 08:00:00',3,'','',257,1389,'1','','Dodatni kilometri',20,'2016091',0,2,30);
INSERT INTO `bookings` VALUES (127,'2016-03-14 12:31:03','2016-03-18 11:25:10',0,'2016-03-14 15:00:00','2016-03-19 15:00:00',16,'','',4687.5,251,'1','','Blue sun alias 650x5',20,'2016092',0,2,10);
INSERT INTO `bookings` VALUES (128,'2016-03-15 09:00:13','2016-03-17 14:46:45',0,'2016-03-16 08:00:00','2016-03-18 08:00:00',6,'40740','41172',967.5,1395,'1','','',20,'2016087',0,2,10);
INSERT INTO `bookings` VALUES (129,'2016-03-15 15:11:39','2016-03-19 08:09:09',0,'2016-03-18 08:00:00','2016-03-20 08:00:00',6,'41172','41653',978.75,1355,'1','','',20,'2015095',0,2,10);
INSERT INTO `bookings` VALUES (130,'2016-03-15 15:17:25','2016-04-22 11:06:54',0,'2016-06-16 08:00:00','2016-06-23 08:00:00',10,'','',2800,1396,'2','','25.03. uplata 1 dio\r\n21.04. uplata 2 dio',20,'2016068',1600,0,10);
INSERT INTO `bookings` VALUES (131,'2016-03-15 15:59:47','2016-03-17 10:47:22',0,'2016-03-17 08:00:00','2016-03-18 08:00:00',5,'97110','97138',489.38,1338,'1','','',20,'2016096',0,2,10);
INSERT INTO `bookings` VALUES (132,'2016-03-16 14:59:00','2016-03-21 11:31:20',0,'2016-03-18 12:00:00','2016-03-21 12:00:00',4,'216223','216323',1468.13,1397,'1','','',20,'2016097',0,2,10);
INSERT INTO `bookings` VALUES (133,'2016-03-18 11:24:41','2016-03-28 07:38:38',0,'2016-03-19 08:00:00','2016-03-26 08:00:00',16,'','',3465,251,'1','','Blu SUna alias 2100',20,'2016100',0,2,10);
INSERT INTO `bookings` VALUES (134,'2016-03-18 15:05:33','2016-03-26 08:05:09',0,'2016-03-22 08:00:00','2016-03-26 08:00:00',5,'98980','101070',1886,1398,'2','','',20,'2016081',0,2,10);
INSERT INTO `bookings` VALUES (135,'2016-03-19 12:32:09','2016-03-20 14:14:48',0,'2016-03-19 12:00:00','2016-03-20 08:00:00',6,'41670','41775',400,1381,'2','','',20,'Bez racuna',0,2,10);
INSERT INTO `bookings` VALUES (137,'2016-03-21 11:03:17','2016-03-31 12:28:52',0,'2016-03-23 07:00:00','2016-03-24 08:00:00',3,'137830','138371',639.38,88,'1','','',20,'2016101',0,2,10);
INSERT INTO `bookings` VALUES (138,'2016-03-21 11:36:52','2016-03-22 17:08:16',0,'2016-03-22 08:00:00','2016-03-23 08:00:00',4,'216323','216400',489.38,1367,'1','','',20,'2016102',0,2,10);
INSERT INTO `bookings` VALUES (139,'2016-03-21 11:54:35','2016-03-22 13:54:01',0,'2016-03-22 11:00:00','2016-03-23 07:00:00',3,'137729','137800',391.5,1378,'2','','',20,'2016083',0,2,10);
INSERT INTO `bookings` VALUES (140,'2016-03-21 15:12:14','2016-03-25 11:56:59',0,'2016-03-22 12:00:00','2016-03-25 12:00:00',6,'41795','43069',1618.13,1400,'1','','',20,'2016104',0,2,10);
INSERT INTO `bookings` VALUES (142,'2016-03-23 08:54:09','2016-03-23 08:54:57',0,'2016-03-23 08:00:00','2016-03-23 08:00:00',8,'','',437.5,12,'1','','kazna Land Oberosterreich\r\nrn.br.091010492275',1,'2016107',0,2,30);
INSERT INTO `bookings` VALUES (143,'2016-03-23 11:32:54','2016-03-25 20:34:24',0,'2016-03-24 07:00:00','2016-03-26 08:00:00',4,'216510','217861',1215,1401,'2','','',20,'2016086',0,2,10);
INSERT INTO `bookings` VALUES (144,'2016-03-23 16:06:22','2016-04-01 07:57:24',0,'2016-03-29 08:00:00','2016-04-01 08:00:00',6,'43069','44275',1500,1402,'1','','',20,'2015108',0,2,10);
INSERT INTO `bookings` VALUES (146,'2016-03-23 16:21:15','2016-04-06 10:08:01',0,'2016-03-31 08:00:00','2016-04-06 08:00:00',5,'101070','104102',2930,1403,'2','','',20,'2016031',0,2,10);
INSERT INTO `bookings` VALUES (147,'2016-03-23 18:47:00','2016-04-18 10:40:05',0,'2016-03-25 08:00:00','2016-04-24 08:00:00',9,'126824','132916',9843.75,11,'1','','',1,'2016110',0,2,10);
INSERT INTO `bookings` VALUES (148,'2016-03-24 13:42:37','2016-04-04 07:49:49',0,'2016-03-30 08:00:00','2016-04-04 08:00:00',4,'217861','219332',2100,1407,'2','','Preko 6 sigma',1,'2016094',0,2,10);
INSERT INTO `bookings` VALUES (149,'2016-03-25 07:02:17','2016-03-25 07:02:17',0,'2016-03-21 08:00:00','2016-03-21 08:00:00',5,'','',100,1376,'2','','- sitno oštećenje desnih vrata',20,'Bez računa',0,2,30);
INSERT INTO `bookings` VALUES (150,'2016-03-25 07:07:00','2016-03-25 07:07:00',0,'2016-03-07 08:00:00','2016-03-07 12:00:00',5,'','',200,1377,'1','','-  kašnjenje u povratu',20,'Bez računa',0,2,10);
INSERT INTO `bookings` VALUES (151,'2016-03-25 12:13:21','2016-03-29 14:09:10',0,'2016-03-25 08:00:00','2016-03-25 08:00:00',6,'','',1250,1400,'1','','Fransiza',20,'2016113',0,2,30);
INSERT INTO `bookings` VALUES (153,'2016-03-25 20:32:49','2016-03-26 19:00:25',0,'2016-03-26 08:00:00','2016-03-26 20:00:00',3,'138371','138548',0,1404,'2','','',99,'',0,2,10);
INSERT INTO `bookings` VALUES (156,'2016-03-29 20:05:39','2016-04-01 09:06:59',0,'2016-03-31 09:00:00','2016-04-01 09:00:00',3,'138578','138997',566.56,1406,'1','','',20,'2016115',0,2,10);
INSERT INTO `bookings` VALUES (157,'2016-03-30 12:09:35','2016-05-03 09:42:42',0,'2016-04-04 08:00:00','2016-05-04 08:00:00',3,'139220','140878',7821,1408,'2','','- na istoj ponudi i RI-654-UP\r\n- došli su preko Konta\r\n- vjerovatno više mjeseci. 29.4 ih zvati\r\n- na kraju najma unjeti kilometražu i ubaciti je u sljedeći mjesec',20,'2016087',0,2,10);
INSERT INTO `bookings` VALUES (159,'2016-03-30 12:13:46','2016-05-04 06:46:03',0,'2016-04-04 08:00:00','2016-05-04 08:00:00',4,'219352','',7029,1408,'2','','- na istoj ponudi i RI-629-UP\r\n- došli su preko Konta\r\n- vjerovatno više mjeseci. 29.4 ih zvati\r\n- na kraju najma unijeti kilometražu i ubaciti je u sljedeći mjesec',20,'2016087',0,2,10);
INSERT INTO `bookings` VALUES (160,'2016-03-31 06:38:24','2016-04-01 18:45:20',0,'2016-04-01 08:00:00','2016-04-02 08:00:00',6,'44275','44418',405,1409,'2','','',20,'2016095',0,2,10);
INSERT INTO `bookings` VALUES (161,'2016-03-31 12:10:21','2016-03-31 12:11:47',0,'2016-06-16 15:00:00','2016-06-19 15:00:00',7,'','',2200,1410,'2','','- 1200kn gotovina\r\n-1000kn će odraditi na webu',1,'bez računa',0,0,10);
INSERT INTO `bookings` VALUES (162,'2016-03-31 14:50:25','2016-04-02 09:19:40',0,'2016-04-01 09:00:00','2016-04-02 09:00:00',3,'138997','139200',489.38,1411,'1','','',20,'2016118',0,2,10);
INSERT INTO `bookings` VALUES (163,'2016-04-01 16:07:57','2016-04-05 15:22:10',0,'2016-04-02 09:00:00','2016-04-03 09:00:00',6,'44418','44653',480,1412,'2','','450-10% (300km/dan)\r\n+ 75 preuzimanje nedeljom',20,'2016100',0,2,10);
INSERT INTO `bookings` VALUES (164,'2016-04-01 16:14:54','2016-04-29 10:02:39',0,'2016-04-07 08:00:00','2016-04-08 08:00:00',11,'8409','8989',2137.5,1171,'1','','-treba poslati račun za uplatu\r\n- 15 dana odgoda.\r\n- 792kune ukupni trošak sa Rimayem',20,'2016117',0,2,20);
INSERT INTO `bookings` VALUES (165,'2016-04-01 16:16:17','2016-04-29 10:02:52',0,'2016-04-07 08:00:00','2016-04-08 08:00:00',16,'','',2137.5,1171,'1','','- vozi partner\r\n- na istoj ponudi i naš Trafic\r\n- treba napisati i poslati račun\r\n- Ferrara-1500+pdv',20,'2016117',0,2,20);
INSERT INTO `bookings` VALUES (166,'2016-04-01 20:07:21','2016-05-03 14:03:57',0,'2016-05-18 08:00:00','2016-05-29 08:00:00',16,'','',4125,1413,'2','','- ima dva kombija (2016103)\r\n- plaća 375kn rezervaciju do 15.04\r\n- ostalo plaća gotovinom u eurima \r\n+ ostavlja polog 1000kn\r\n3500 Blue Sun',10,'2016101',375,0,10);
INSERT INTO `bookings` VALUES (167,'2016-04-01 20:09:26','2016-04-11 10:13:22',0,'2016-05-18 08:00:00','2016-05-29 08:00:00',10,'','',4125,1413,'2','','- ima dva kombija (2016101)\r\n- plaća 375kn rezervaciju do 15.04\r\n- ostalo plaća gotovinom u eurima \r\n+ ostavlja polog 1000kn',10,'2016103',375,0,10);
INSERT INTO `bookings` VALUES (168,'2016-04-01 20:20:40','2016-04-07 08:48:32',0,'2016-04-04 08:00:00','2016-04-07 08:00:00',6,'44653','47740',2600,365,'2','','- bili par puta i platili nešto više gotovinom\r\n- razbijen ljevi far placaju oni',20,'2016096',0,2,10);
INSERT INTO `bookings` VALUES (169,'2016-04-04 10:25:43','2016-04-07 08:52:00',0,'2016-04-06 09:00:00','2016-04-07 09:00:00',5,'104102','104176',489.38,1394,'1','','435kn -10% x 1 dan (300km/dan)',20,'2016119',0,2,10);
INSERT INTO `bookings` VALUES (170,'2016-04-04 13:08:24','2016-04-18 11:06:03',0,'2016-04-15 12:00:00','2016-04-18 12:00:00',5,'106430','108050',1630,1414,'2','','450kn x 3  (+700km)\r\n+ 1130kuna plaćeno u gotovini\r\n+ 100eura ostavljen polog',20,'2016105',500,2,10);
INSERT INTO `bookings` VALUES (171,'2016-04-05 08:26:35','2016-04-26 07:49:25',0,'2016-04-25 08:00:00','2016-04-26 08:00:00',11,'11084','11810',898.5,1415,'1','','510kn-10% x 1 + 400km + 100kn preuzimanje u 5.30',20,'2016121',0,2,10);
INSERT INTO `bookings` VALUES (172,'2016-04-05 10:32:08','2016-04-05 10:32:08',0,'2016-05-14 07:00:00','2016-05-15 08:00:00',10,'','',2256.25,1416,'1','','- prijevoz',1,'2016043',0,0,20);
INSERT INTO `bookings` VALUES (174,'2016-04-05 13:44:57','2016-04-05 14:01:11',0,'2016-06-03 08:00:00','2016-06-11 08:00:00',11,'','',3384,1418,'2','','465kn-10% x 8 dana (300km/dan)+navigacija',1,'2016084',0,0,10);
INSERT INTO `bookings` VALUES (175,'2016-04-06 09:55:17','2016-04-06 09:55:17',0,'2016-04-06 08:00:00','2016-04-06 08:00:00',5,'','',2000,1403,'2','','- oštećenje kombija',20,'Bez računa',0,2,30);
INSERT INTO `bookings` VALUES (176,'2016-04-07 10:49:28','2016-04-21 11:03:58',0,'2016-07-05 12:00:00','2016-07-15 12:00:00',11,'','',4690.63,1420,'1','','395-5% x 10 dana/250km',10,'2016061',500,0,10);
INSERT INTO `bookings` VALUES (177,'2016-04-07 11:13:35','2016-04-11 08:05:34',0,'2016-04-09 08:00:00','2016-04-11 08:00:00',5,'104441','105408',1000,1362,'2','','500km x 2 dana',20,'Bez računa',0,2,10);
INSERT INTO `bookings` VALUES (178,'2016-04-07 14:17:35','2016-04-10 10:53:05',0,'2016-04-09 09:00:00','2016-04-10 09:00:00',6,'47760','48070',500,1422,'2','','430kn/ x 1 dan/300km + 70kn nedelja',20,'bez računa',0,2,10);
INSERT INTO `bookings` VALUES (179,'2016-04-08 08:44:49','2016-05-04 19:16:56',0,'2016-07-26 08:00:00','2016-08-04 08:00:00',7,'','',3645,1421,'2','','- plaća rezervaciju 500kn do 15.05.2016',1,'2016003',0,0,10);
INSERT INTO `bookings` VALUES (181,'2016-04-09 07:07:07','2016-04-27 21:00:46',0,'2016-04-09 08:00:00','2016-05-10 08:00:00',10,'122272','',10500,11,'1','','',1,'2016123',0,1,10);
INSERT INTO `bookings` VALUES (182,'2016-04-09 07:08:42','2016-04-28 11:04:14',0,'2016-04-09 08:00:00','2016-04-30 08:00:00',7,'234125','237103',6890.63,11,'1','','',1,'2016124',0,2,10);
INSERT INTO `bookings` VALUES (184,'2016-04-11 16:04:26','2016-05-02 07:19:41',0,'2016-04-30 14:00:00','2016-05-01 14:00:00',7,'236523','236882',715.5,870,'2','','530kn x -10% x 1.5',20,'2016118',0,2,10);
INSERT INTO `bookings` VALUES (185,'2016-04-11 19:44:24','2016-04-18 12:23:13',0,'2016-04-12 08:00:00','2016-04-17 08:00:00',6,'48134','50705',2896.88,1425,'1','','435kn-10% x 5dana (300km)+900km\r\n- preko informatičara Baotić',20,'2016129',0,2,10);
INSERT INTO `bookings` VALUES (186,'2016-04-12 12:34:45','2016-04-13 12:43:58',0,'2016-04-12 13:00:00','2016-04-13 13:00:00',5,'105498','106057',550,1426,'2','','550kn/600km/Biograd',20,'Bez računa',0,2,10);
INSERT INTO `bookings` VALUES (187,'2016-04-12 14:17:03','2016-05-01 09:23:56',0,'2016-04-27 08:00:00','2016-05-01 08:00:00',16,'','',2707.5,1427,'1','','510kn-10% x 4 dana/300km\r\n+700km + 50kn povrat nedelja\r\n- Lovigo 2000kuna',20,'2016132',0,2,10);
INSERT INTO `bookings` VALUES (188,'2016-04-13 13:29:02','2016-04-15 09:07:50',0,'2016-04-14 08:00:00','2016-04-15 08:00:00',5,'106057','106420',494.81,1428,'1','','453kn -9% x 1dan/300km\r\n- 30kuna za vise kilometara placeno gotovinim',20,'2016133',0,2,10);
INSERT INTO `bookings` VALUES (189,'2016-04-14 09:31:11','2016-04-18 08:28:11',0,'2016-04-14 08:00:00','2016-04-14 20:00:00',6,'','',343,1368,'1','','-kazna Austrija\r\n-Land Salzburg\r\nrn.br.30206-369/16043-2016',20,'2016135',0,2,30);
INSERT INTO `bookings` VALUES (190,'2016-04-18 06:19:58','2016-04-18 12:23:40',0,'2016-04-17 08:00:00','2016-04-18 08:00:00',6,'50705','50706',462.19,1425,'1','','-produžetak najma',1,'2016137',0,2,10);
INSERT INTO `bookings` VALUES (191,'2016-04-18 11:57:15','2016-04-20 08:25:29',0,'2016-04-19 07:00:00','2016-04-20 07:00:00',5,'108050','108788',639.38,1430,'1','','Osijek\r\n435-10% x 1 dan(300km)+ 300km\r\n+50kuna u gotovini za kilometre',20,'2016139',0,2,10);
INSERT INTO `bookings` VALUES (192,'2016-04-18 12:30:19','2016-04-25 10:57:48',0,'2016-04-22 17:00:00','2016-04-24 17:00:00',5,'109351','109912',910,1431,'2','','450 - 10% x 2 dana (300km) + 100kn nedelja',20,'2016133',0,2,10);
INSERT INTO `bookings` VALUES (193,'2016-04-18 13:29:00','2016-04-23 08:01:16',0,'2016-04-19 09:00:00','2016-04-23 09:00:00',6,'50706','51861',1620,17,'2','','450-10% x 4 dana(300km)\r\n',1,'2016134',0,2,10);
INSERT INTO `bookings` VALUES (194,'2016-04-18 15:46:54','2016-05-04 16:43:23',0,'2016-05-05 08:00:00','2016-05-07 08:00:00',16,'','',1198.5,1432,'1','','Lovigo',20,'2016142',0,0,10);
INSERT INTO `bookings` VALUES (195,'2016-04-19 10:21:34','2016-04-28 13:08:17',0,'2016-03-19 08:00:00','2016-03-19 08:00:00',4,'','',92.5,1397,'1','','ZGB Holding kazna parking',20,'2016143',0,2,30);
INSERT INTO `bookings` VALUES (196,'2016-04-20 12:50:36','2016-04-21 07:50:28',0,'2016-04-20 12:00:00','2016-04-21 08:00:00',5,'108788','108888',410,1362,'2','','Zagreb',20,'Bez računa',0,2,10);
INSERT INTO `bookings` VALUES (197,'2016-04-20 15:00:01','2016-05-02 08:33:24',0,'2016-04-23 09:00:00','2016-04-25 08:00:00',6,'51861','52251',810,1433,'2','','450kn-10%  x 2 dana (300km/dan)\r\nUplata preko Internet Instituta',20,'2016138',0,2,10);
INSERT INTO `bookings` VALUES (198,'2016-04-20 17:11:46','2016-04-22 07:48:18',0,'2016-04-21 11:00:00','2016-04-22 11:00:00',5,'108888','109351',500,1434,'2','','- preko Frakoprom (Ljubljana)\r\n450knx1dan(300km)+125km',20,'Bez računa',0,2,10);
INSERT INTO `bookings` VALUES (199,'2016-04-20 19:37:38','2016-05-04 19:16:43',0,'2016-07-05 08:00:00','2016-07-19 08:00:00',7,'','',5403.3,1435,'2','','415kn-7% x 14 dana(200km)\r\n- 500kuna uplaćuje rezervaciju, ostalo ćemo u gotovini uzeti\r\n- preuzima Gregov vozi njegova rodbina iz Australije',10,'2016130',500,0,10);
INSERT INTO `bookings` VALUES (201,'2016-04-21 11:24:55','2016-05-03 14:45:34',0,'2016-04-26 12:00:00','2016-04-30 12:00:00',8,'234720','236523',2088,1437,'2','','- plaćaju u eurima iz Italije 278,35eura',20,'2016141',0,2,10);
INSERT INTO `bookings` VALUES (202,'2016-04-21 13:01:56','2016-04-25 09:36:37',0,'2016-04-23 07:00:00','2016-04-25 08:00:00',8,'233888','234720',1100,1439,'2','','500kn x 2 dana 300km + 250km\r\n- veza Zdenko Belko\r\n- dati kombi u petak naveče\r\n- plaćanje na povratku',20,'belko',0,2,10);
INSERT INTO `bookings` VALUES (203,'2016-04-21 19:03:25','2016-04-29 07:49:19',0,'2016-05-01 08:00:00','2016-05-31 08:00:00',9,'132916','',9843.75,12,'1','','- poslana ponuda 27.04.\r\n- na kraju najma zatražiti kilometražu',1,'2016155',0,1,10);
INSERT INTO `bookings` VALUES (204,'2016-04-22 13:08:32','2016-04-25 10:58:28',0,'2016-04-25 08:00:00','2016-05-07 08:00:00',5,'109912','',5414.06,365,'1','','',1,'2016151',0,1,10);
INSERT INTO `bookings` VALUES (205,'2016-04-25 08:58:15','2016-05-04 16:42:34',0,'2016-05-08 08:00:00','2016-05-31 08:00:00',7,'','',6665.63,12,'1','','- 23 dana najma. ali smo im umanjili\r\nnaplatu za 4 jer su u travnju imali plaćeno\r\n30 dana a koristili 25 dana',1,'2016156',0,0,10);
INSERT INTO `bookings` VALUES (206,'2016-04-25 10:48:30','2016-04-26 09:24:14',0,'2016-03-23 08:00:00','2016-03-24 08:00:00',3,'','',121.5,88,'1','','Parking kazna Osijek',20,'2016159',0,2,30);
INSERT INTO `bookings` VALUES (207,'2016-04-26 10:18:09','2016-05-03 11:45:01',0,'2016-05-02 10:00:00','2016-05-03 10:00:00',15,'','',489.38,1441,'1','','435-10% x 1 dan(150km)\r\n- Baotićev kombi  350kn\r\n(Račun 158-PP1-364)',20,'2016161',0,2,10);
INSERT INTO `bookings` VALUES (208,'2016-04-26 13:42:55','2016-04-26 13:42:55',0,'2016-05-11 08:00:00','2016-05-14 07:00:00',10,'','',1816.88,1442,'1','','510kn -5% x 3 dana(300km)\r\n- vidjeti da ga vrate ranije da Rimay može ići na put',1,'2016116',0,0,10);
INSERT INTO `bookings` VALUES (209,'2016-04-26 13:53:39','2016-04-28 17:52:29',0,'2016-04-27 18:00:00','2016-04-28 18:00:00',6,'52324','52704',427.5,1443,'2','','450kn-5% x 1 dan(300km)\r\n+ 40kn u kilometrima bez racuna',20,'2016158',0,2,10);
INSERT INTO `bookings` VALUES (210,'2016-04-26 14:18:08','2016-04-28 13:37:39',0,'2016-04-28 08:00:00','2016-05-08 08:00:00',11,'11810','',4300,1444,'2','','- 430kn x 10 dana\r\n- ostavljen polog 1000kn\r\n',20,'Bez računa',500,1,10);
INSERT INTO `bookings` VALUES (211,'2016-04-26 14:20:24','2016-04-27 17:34:47',0,'2016-04-27 08:00:00','2016-04-27 12:00:00',6,'52251','52324',240,1445,'2','','',20,'Bez računa',0,2,10);
INSERT INTO `bookings` VALUES (212,'2016-04-26 15:53:55','2016-05-03 20:45:51',0,'2016-05-25 08:00:00','2016-05-31 08:00:00',11,'','',3442.5,400,'1','','- do 4.5. plaćaju 500kn rezervacije\r\n- ostalo do dana preuzimanja\r\n- 510kn - 10% x 6 dana(300km)\r\n500,00kn uplaceno 29.04.2016',10,'2016165',500,0,10);
INSERT INTO `bookings` VALUES (213,'2016-04-26 15:56:29','2016-05-03 20:46:18',0,'2016-05-25 08:00:00','2016-05-31 08:00:00',16,'','',3442.5,400,'1','','- do 4.5. plaćaju 500kn rezervacije\r\n- ostalo do dana preuzimanja\r\n- 510kn - 10% x 6 dana(300km)\r\n- lovigo usluge 2400kn /400x6\r\n500,00kn uplaceno 29.04.2016',10,'2016166',500,0,10);
INSERT INTO `bookings` VALUES (214,'2016-04-27 14:52:49','2016-05-03 14:45:54',0,'2016-04-29 17:30:00','2016-05-01 17:30:00',6,'52781','53412',945,1447,'2','','450kn-5% x 2 dana(300km)\r\n+90kn povrat nedelja\r\n-vrača u ponedeljak u 7.45',20,'2016139',0,2,10);
INSERT INTO `bookings` VALUES (215,'2016-04-27 15:28:38','2016-05-05 09:10:11',0,'2016-05-04 08:00:00','2016-06-03 08:00:00',4,'','',8786.25,1408,'1','','- zvao Igor 5.5 i rekli su da je plaćado 4.5. iza 3 sata',1,'2016157',0,1,10);
INSERT INTO `bookings` VALUES (217,'2016-04-28 20:16:44','2016-04-29 16:45:15',0,'2016-04-29 07:00:00','2016-04-29 17:00:00',6,'52704','52781',489.38,1367,'1','','435-10% x 1 dan(300km)\r\n- treba provjeriti da li je napravljen povrat',20,'2016174',0,2,10);
INSERT INTO `bookings` VALUES (218,'2016-04-29 08:36:21','2016-05-02 13:37:34',0,'2016-05-02 11:00:00','2016-05-02 15:00:00',15,'','',300,1448,'1','','4 sata x 60kn (1 sat/15km)\r\n- lovigo',20,'2016175',0,1,10);
INSERT INTO `bookings` VALUES (219,'2016-04-29 09:25:31','2016-05-04 08:13:16',0,'2016-05-02 08:00:00','2016-05-04 08:00:00',6,'53412','53962',989.63,1449,'1','','435kn -9% x 2 dana/300km',20,'2016154',0,2,10);
INSERT INTO `bookings` VALUES (220,'2016-05-02 09:59:21','2016-05-04 12:57:26',0,'2016-05-04 10:00:00','2016-05-04 14:00:00',6,'53962','54039',240,1450,'2','','Kašina- Rugvica',20,'nema',100,2,10);
INSERT INTO `bookings` VALUES (221,'2016-05-02 12:45:33','2016-05-02 12:45:33',0,'2016-05-02 08:00:00','2016-05-02 08:00:00',16,'','',1800,3,'2','','- provizija',20,'bez računa',0,2,10);
INSERT INTO `bookings` VALUES (222,'2016-05-02 13:35:19','2016-05-02 13:36:14',0,'2016-05-02 08:00:00','2016-05-02 08:00:00',16,'','',300,1427,'2','','- kemijkso- 200kn Lovigo',20,'Bez računa',0,2,10);
INSERT INTO `bookings` VALUES (223,'2016-05-03 14:06:18','2016-05-05 09:40:09',0,'2016-05-05 08:00:00','2016-06-04 08:00:00',8,'237103','',9975,1451,'1','','- 5 mjeseci\r\n- na kraju najma tražiti kilometražu i upisati u slj.mjesec',20,'2016179',0,1,10);
INSERT INTO `bookings` VALUES (224,'2016-05-03 14:08:05','2016-05-04 16:42:10',0,'2016-06-04 08:00:00','2016-07-04 08:00:00',8,'','',9975,1451,'1','','- upisati početnu kiloemtražu\r\n- 01.06.2016 poslati ponudu za plaćanje\r\n- upisati završnu kilometražu',1,'Treba poslati ponudu',0,0,10);
INSERT INTO `bookings` VALUES (225,'2016-05-04 19:51:23','2016-05-04 19:51:23',0,'2016-05-13 16:00:00','2016-05-16 08:00:00',5,'','',1001.25,1453,'2','','Beč',1,'2016116',0,0,10);
INSERT INTO `bookings` VALUES (226,'2016-05-05 09:33:34','2016-05-05 10:40:47',0,'2016-05-05 10:30:00','2016-05-05 14:30:00',3,'','',300,1309,'1','','- Zagreb locco',1,'2016181',0,1,10);
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table checklists
#

DROP TABLE IF EXISTS `checklists`;
CREATE TABLE `checklists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `check_date` date NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table checklists
#
LOCK TABLES `checklists` WRITE;
/*!40000 ALTER TABLE `checklists` DISABLE KEYS */;

INSERT INTO `checklists` VALUES (2,'2015-12-29 12:30:22','2016-01-04 13:49:37','2015-12-29',3,'{\"kilometraza_vozila\":\"120980\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"+\",\"tekucina_serva\":\"+\",\"tekucina_kocnica\":\"+\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (3,'2015-12-30 16:23:55','2015-12-30 16:23:55','2015-12-30',5,'{\"kilometraza_vozila\":\"86944\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"napomena\":\"Malo napuknuto ljevo stop svjetlo, ne smeta. Tako je kupljen.\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (4,'2015-12-30 16:47:17','2015-12-30 16:47:17','2015-12-30',6,'{\"kilometraza_vozila\":\"30374\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"+\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (5,'2016-01-09 09:03:33','2016-01-09 09:03:33','2016-01-09',3,'{\"kilometraza_vozila\":\"123101\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (6,'2016-01-12 20:33:00','2016-01-12 20:33:00','2016-01-11',11,'{\"kilometraza_vozila\":\"1941\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (7,'2016-01-18 08:40:02','2016-01-18 08:40:02','2016-01-18',11,'{\"kilometraza_vozila\":\"3200\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (8,'2016-01-18 10:10:46','2016-01-18 10:10:46','2016-01-18',6,'{\"kilometraza_vozila\":\"33832\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"+\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"napomena\":\"dolili ulje i teku\\u0107inu za stakla\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (9,'2016-01-20 08:36:41','2016-01-20 08:36:41','2016-01-20',3,'{\"kilometraza_vozila\":\"124527\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"+\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (10,'2016-01-24 09:55:51','2016-01-24 09:55:51','2016-01-24',5,'{\"kilometraza_vozila\":\"91509\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"+\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"napomena\":\"Sve ok\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (11,'2016-01-25 09:21:17','2016-01-25 09:21:17','2016-01-25',3,'{\"kilometraza_vozila\":\"125475\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (12,'2016-01-29 12:16:08','2016-01-29 12:16:08','2016-01-29',4,'{\"kilometraza_vozila\":\"211934\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (13,'2016-02-08 08:39:51','2016-02-08 08:39:51','2016-02-08',6,'{\"kilometraza_vozila\":\"38461\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"+\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (14,'2016-02-08 08:49:50','2016-02-08 08:52:48','2016-02-08',5,'{\"kilometraza_vozila\":\"92370\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"Napuknuto stop ljevi.\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (15,'2016-02-16 09:05:55','2016-02-16 09:05:55','2016-02-16',5,'{\"kilometraza_vozila\":\"92807\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (16,'2016-03-07 08:15:46','2016-03-07 08:15:46','2016-03-07',3,'{\"kilometraza_vozila\":\"131452\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"+\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (17,'2016-03-07 08:17:00','2016-03-07 08:17:00','2016-03-07',6,'{\"kilometraza_vozila\":\"40747\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"+\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (18,'2016-03-19 08:15:58','2016-03-19 08:15:58','2016-03-19',6,'{\"kilometraza_vozila\":\"41653\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"+\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (20,'2016-03-22 10:47:04','2016-03-22 10:47:04','2016-03-22',3,'{\"kilometraza_vozila\":\"137729\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"+\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (21,'2016-03-24 07:37:30','2016-03-26 19:05:08','2016-03-24',3,'{\"kilometraza_vozila\":\"138371\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"\",\"rad_ventilacije_brzine\":\"\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (22,'2016-04-01 08:00:45','2016-04-01 08:00:45','2016-04-01',6,'{\"kilometraza_vozila\":\"44275\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"+\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (23,'2016-04-12 07:40:44','2016-04-12 07:40:44','2016-04-12',6,'{\"kilometraza_vozila\":\"48134\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"+\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"+\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (24,'2016-04-12 08:00:39','2016-04-12 08:00:39','2016-04-12',5,'{\"kilometraza_vozila\":\"105444\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"+\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (25,'2016-04-22 07:50:36','2016-04-22 07:50:36','2016-04-22',5,'{\"kilometraza_vozila\":\"109351\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"+\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (26,'2016-04-25 08:20:27','2016-04-25 08:20:27','2016-04-25',6,'{\"kilometraza_vozila\":\"52251\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"+\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (27,'2016-04-25 08:33:11','2016-04-25 08:33:11','2016-04-25',8,'{\"kilometraza_vozila\":\"234720\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"+\",\"tekucina_serva\":\"+\",\"tekucina_kocnica\":\"+\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"+\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
INSERT INTO `checklists` VALUES (28,'2016-05-04 08:17:07','2016-05-04 08:17:07','2016-05-04',6,'{\"kilometraza_vozila\":\"53962\",\"brisaci\":\"+\",\"kratka_svjetla\":\"+\",\"duga_svjetla\":\"+\",\"zmigavci\":\"+\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"+\",\"tekucina_serva\":\"\",\"tekucina_kocnica\":\"\",\"gume_tlak_stanje\":\"+\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"\",\"btnSave\":\"\"}');
/*!40000 ALTER TABLE `checklists` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table contacts
#

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `company` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `company_long` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `oib` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mb` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address_2` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `zip` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `country_id` int(11) NOT NULL DEFAULT '52',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobile` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fax` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `web` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `groups` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tags` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1455 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table contacts
#
LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;

INSERT INTO `contacts` VALUES (1,'2015-12-17 19:57:41','2015-12-18 11:30:30','','','KOMET STANDARD d.o.o.','KOMET STANDARD d.o.o.','05029171760','','Aleja Blaža Jurišića 9','Ulica Dragutina Mandla 1','10040','Zagreb',52,'info@komet-prijevoz.hr','00385981756002','0038517888123','','http://flexirent.hr','','Pravna osoba,Partner,Najmodavac','','','');
INSERT INTO `contacts` VALUES (2,'2015-12-17 19:59:37','2015-12-17 20:21:25','','','FLEXI RENT j.d.o.o.','FLEXI RENT j.d.o.o.','21113779642','04302028 ','Aleja Blaža Jurišića 9','Ulica Dragutina Mandla 1','10040','Zagreb',52,'info@flexirent.hr','00385981756002','0038517888123','','http://flexirent.hr','','Pravna osoba,Partner,Najmodavac','','','');
INSERT INTO `contacts` VALUES (3,'2015-12-17 20:09:49','2015-12-21 13:09:30','','','6 SIGMA d.o.o.','6 SIGMA d.o.o.','89359809737','','BUNIĆEVA 7 ','','10000','Zagreb',52,'culjat13@gmail.com','00385 99 2720212','','','','Firma ima najam kombija, uzimali kod nas a mi možemo i kod njih.\r\nImaju Opel Vivare 2013 ili mlađe.','Pravna osoba,Dobavljač','','','');
INSERT INTO `contacts` VALUES (4,'2015-12-17 20:13:00','2015-12-17 20:13:00','','','ACNielsen d.o.o.','ACNielsen d.o.o.','77114819074','01479776','Budmanijeva ulica  1','','10000','',52,'marina.leko@nielsen.com','','','','','','Pravna osoba,Partner','','','');
INSERT INTO `contacts` VALUES (5,'2015-12-18 10:54:53','2016-03-09 10:32:24','','','AUTODUBRAVA d.o.o.','AUTODUBRAVA d.o.o.','82053451114','','Ulica Dragutina Mandal 1','','10040','Zagreb',52,'branka.glojnaric@baotic.hr','00385 1 2900191','00385 1 2900191','','','','Pravna osoba,Dobavljač,Servisni centar','','','auto servis,servis,vulkanizer,auto električar');
INSERT INTO `contacts` VALUES (6,'2015-12-18 10:58:34','2016-03-09 10:31:40','','','AUTO ART j.d.o.o.','AUTO ART j.d.o.o.','93130677665','','Bjelovarska 123','','10370 ','Dugo Selo',52,'autoart@net.amis.hr','00385 98 330176','','','','- auto servis\r\n- nije u PDV-u, račune tražiti na FLEXI RENT\r\n- radi kompletan servis i gume se čuvaju kod njega','Pravna osoba,Dobavljač,Servisni centar','Roman Jurić','00385 98 330176','auto servis,servis,auto električar,vulkanizer,gume');
INSERT INTO `contacts` VALUES (7,'2015-12-18 10:59:51','2016-03-24 07:26:45','Mario','Bečaj','Limar','Mario Limar','','','','','','',52,'','00385 99 6374958','','','','- limarija, servis\r\n- za štete koje ne plaćamo preko firmi','Fizička osoba,Dobavljač,Servisni centar','Mario Bečaj','00385 99 6374958','auto,limarija,servis');
INSERT INTO `contacts` VALUES (8,'2015-12-18 11:03:42','2016-03-09 10:38:00','','','LONTRA d.o.o.','LONTRA d.o.o.','','','Bože Huzanića 72 A','','10370','Dugo Selo',52,'lontradv@gmail.com','00385 98 640094','','','','Rade sve oko limarije i vučna služba. Može preko kaska ili gotovina.\r\n','Pravna osoba,Dobavljač,Servisni centar','Dražen','00385 98 640094','kasko,vučna služba,limarija,auto,pomoć na cesti');
INSERT INTO `contacts` VALUES (9,'2015-12-18 11:13:11','2015-12-23 16:13:36','','','AURO DOMUS d.o.o.','AURO DOMUS','46688992812','','Tometići 1/D','','51000','Kastav',52,'snjezana.traunkar@aurodomus.com','00385 91 4201044','','','','Firma se bavi otkupom zlata.','Pravna osoba','Snježana Traunkar','00385 91 4201044','');
INSERT INTO `contacts` VALUES (10,'2015-12-18 11:14:49','2015-12-24 20:07:02','Igor','Vlahek','','','','','','','','',52,'','','','','','Borisov kolega','Fizička osoba','Igor Vlahek','00385922469484','');
INSERT INTO `contacts` VALUES (11,'2015-12-18 11:26:56','2015-12-23 16:09:36','','','Zagreb Montaža d.o.o.','Zagreb Montaža d.o.o.','06588149401','','Roberta Frangeša Mihanovića 9','','10000','Zagreb',52,'dmartinovic@zagreb-montaza.hr','00385 98 485735','','','','- kasne sa plaćanjem\r\n- Damir je uredu osoba\r\n','Pravna osoba','Damir Martinović','00385 98 485735','');
INSERT INTO `contacts` VALUES (12,'2015-12-18 11:28:52','2015-12-23 16:12:33','','','STROJOPROMET-ZAGREB d.o.o.','STROJOPROMET-ZAGREB d.o.o.','97994010225','','Zagrebačka 6','','10292','Šenkovec',52,'denis.sakanovic@strojopromet.hr','00 385 99 2531 884','','','','- uredan platiša\r\n- bave se izgradnjom','Pravna osoba','Denis Šakanović','00 385 99 2531 884','');
INSERT INTO `contacts` VALUES (13,'2015-12-18 11:41:13','2015-12-23 13:07:52','Thomas','Farnell','','','','','','','','',52,'thomas.farnell@gmail.com','','','','','','Fizička osoba','Thomas Farnell','00385 99 2948658','');
INSERT INTO `contacts` VALUES (14,'2015-12-18 11:42:10','2015-12-24 20:08:47','Nikola','Jambrešić','','','','','','','','',52,'jambresic5@gmail.com','','','','','','Fizička osoba','Nikola Jambrešić','00385992000650','');
INSERT INTO `contacts` VALUES (15,'2015-12-18 11:52:44','2015-12-18 11:59:05','','','PLINOSERVIS KUZMAN d.o.o.','PLINOSERVIS KUZMAN d.o.o.','53047211759','','Mrzopoljska 2','','10000','',52,'info@plin-grijanje-kuzman.hr','00385 98 248 256','','','','-kontakt Joško iz Baotića','Pravna osoba','','','');
INSERT INTO `contacts` VALUES (16,'2015-12-18 12:09:40','2016-02-05 08:21:00','','','HRVATSKI CRVENI KRIŽ','HRVATSKI CRVENI KRIŽ','72527253659','','Ulica Crvenog križa 14','','10000','Zagreb',52,'','','00385 1 4655 814','','','Usmena komunikacija, nema mail.','Pravna osoba','Tomislav Žurkula','+38598216631','');
INSERT INTO `contacts` VALUES (17,'2015-12-19 13:29:22','2016-04-22 18:17:21','Fran','Martić','','','','','','','','',52,'marticfran@gmail.com','00385  91 7303596','','','','','Fizička osoba','Fran Martić','00385  91 7303596','');
INSERT INTO `contacts` VALUES (18,'2015-12-21 09:47:44','2015-12-23 11:53:27','','','ALLIANZ OSIGURANJE','','23759810849','','Heinzelova 70','','10000','Zagreb',52,'stern@impuls-alpha.hr','','','','','- kasko\r\n- kasko polica\r\n- mogu biti na rate','Pravna osoba,Dobavljač','Irena Štern','00385 99 2720212','');
INSERT INTO `contacts` VALUES (25,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','N.S. DUBRAVA','','47521323377','','Cerska 1','','10040','Zagreb',52,'racunovodstvo@ns-dubrava.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (26,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Martina Poletto','','','','Kupac građanin','','','',52,'teapoletto@virgilio.it','','','','','','','','','');
INSERT INTO `contacts` VALUES (27,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Veleposlanstvo NR Kine','','84022382972','','Mlinovi 132','','10000',' Zagreb',52,'veleposlanstvo.nr.kine1@zg.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (28,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','EUROCOP d.o.o.','','48400313356','','Zvečajska 5','','10000','Zagreb',52,'ivan.curic@eurocop.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (29,'0000-00-00 00:00:00','2015-12-22 08:53:40','','','SPEGRA INŽINJERING d.o.o.','','33172675964','',' Ante Petravića 23','','21000 ','Split',52,'boris.pavic@spegra.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (30,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Agencija za znanost i visoko obrazovanje','','83358955356','','Donje Svetice 38/5','','10000','Zagreb',52,'josip.supukovic@azvo.hr','','+385 1 6274 895','','','','','','','');
INSERT INTO `contacts` VALUES (31,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MEDICUS COFFEA MM AK','','','','','','','',52,'mcoffea.mmak@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (32,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','AUTOTRANS d.o.o','','19819724166','','Žabica 1','','51000','Rijeka',52,'ivanka.gerbus@autotrans.hr','','+385-91-3090920','','','','','','','');
INSERT INTO `contacts` VALUES (33,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','KAUFLAND HRVATSKA','','','','Vile Velebita','','10000','Zagreb',52,'martina.matic@kaufland.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (34,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','LOGISTIKA VIOLETA d.o.o.','','62874063131','','Obrež Zelinski 55','','10380','Sv.Ivan Zelina',52,'josipa.knezovic@violeta.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (35,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','HRVATSKI CURLING SAVEZ','','','','','','','',52,'drazen.cutic@curling.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (36,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ZAGREBAČKO KAZALIŠTE MLADIH','','13254939546','','Teslina 7','','10000','Zagreb',52,'kresimir.juric@zekaem.hr','','+385-91-487-2577','','','','','','','');
INSERT INTO `contacts` VALUES (39,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','test','','','','','','','',52,'igor.cukac@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (40,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Bijuk HPC d.o.o','','61373622132','','Vrbovec Samoborski 1/A','','10430','Samobor',52,'jasminka.vrancic@bijuk-hpc.hr','','+385 1 3335 800','','','','','','','');
INSERT INTO `contacts` VALUES (44,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','HŠSG','','59944829246','','Maksimirska 51','','10000','Zagreb',52,'hss.gluhih@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (45,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','E-PORT SOLUTIONS SISTEM d.o.o.','','20122571244','','Maksimirska cesta','','10000','Zagreb',52,'tamara@eport.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (48,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','BET-ARTIS d.o.o.','','73551968549','','Selčinska 27','','10360','Sesvete',52,'t.rukavina@bet-artis.hr','','','','','2015126 - kombi ide u Istru','','','','');
INSERT INTO `contacts` VALUES (53,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','HRVATSKO DRUŠTVO KARIKATURISTA','','15978402700','','Savska 100','','10000','Zagreb',52,'trgovcevic@tzv-gredelj.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (56,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','IVAMONT d.o.o.','','86660343460','','VULINČEVA 24','','10310','Ivanić Grad',52,'tomislav.vukojevic@ivamont.hr','','+385-91-2882-533','','','','','','','');
INSERT INTO `contacts` VALUES (57,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','KOJAN d.o.o.','','53448760343','','Kokoti 3','','20215','Gruda',52,'info@kojankoral.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (58,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ELEKTRO SERVIS GB','','53029252370','','Ulica Hrvatskog proljeća 36','','10040','Zagreb',52,'servisgb@hotmail.com','','00385 98 415 401','','','','','','','');
INSERT INTO `contacts` VALUES (59,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Končar D&ST d.d','','49214559889','','J. Mokrovica 8; p.p. 100','','10090','Zagreb',52,'neno.preglej@koncar-dst.hr','','+385 99 264 8840','','','','','','','');
INSERT INTO `contacts` VALUES (60,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Qatar Airways','','','','','','','',52,'mherceg@hr.qatarairways.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (61,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Goran Zecevic','','','','Kupac gradanin','','','',52,'goran.zecevic@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (65,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Miroslav Cipic','','','','Kupac građanin','','','',52,'miroslav.cipri@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (66,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','EMOS d.o.o.','','','','','','','',52,'emos@emoszg.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (67,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DK DUBRAVA','','','','','','','',52,'sanja.tisljaric@kazalistedubrava.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (68,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','QUADRATA m2 d.o.o.','','61672319010','','Jadranska avenija b.b.','','10000','Zagreb',52,'zlatko@quadrata-m2.hr','','+385-916443555','','','','','','','');
INSERT INTO `contacts` VALUES (69,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Denis Lukačin','','','','Kupac građanin','','','',52,'','','','','','','','','','');
INSERT INTO `contacts` VALUES (70,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','IRO','','88292527040','','Preradovićeva 33/I ','','10000','Zagreb',52,'daracic@iro.hr','','+385 1 4817195','','','','','','','');
INSERT INTO `contacts` VALUES (71,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','SARGON d.o.o.','','17128515960','','KRALJA TOMISLAVA 98','','35410','Nova Kapela',52,'info@kombi-rent.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (72,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Tea Božičević','','','','Kupac građanin','','','',52,'tea.bozicevic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (73,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','KLETT VERLAG d.o.o.','','04776093937','','Domagojeva 15','','10000','Zagreb',52,'ivan@klett.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (74,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','FRESENIUS KABI HRVATSKA d.o.o','','43007220940','','Trg J.F.Kennedyja 6/B','','10000','Zagreb',52,'linda.franicevic@fresenius-kabi.com','','+385 98 641 412','','','','','','','');
INSERT INTO `contacts` VALUES (75,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','PAPAGENO TOURS KELE d.o.o.','','92540258836','','Gomboševa 4','','10000','Zagreb',52,'papageno-tours@zg.htnet.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (76,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Neven Cegnar','','','','Kupac građanin','','','',52,'ncegnar@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (81,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','EXPRO d.o.o.','','05891754957','','Jelenovac 38/6','','10000','Zagreb',52,'zeljko@hidalgo.hr','','+385993047555','','','','','','','');
INSERT INTO `contacts` VALUES (82,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Damir Petrić','','','','-kupac građanin','','','',52,'tomislav.stipanovic@xnet.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (83,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','UNIQA OSIGURANJE d.d.','','75665455333','','Savska 106','','10000','Zagreb',52,'miroslav.beslija@uniqa.hr','','+385-91-6886473','','','','','','','');
INSERT INTO `contacts` VALUES (84,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Auto-Sport','','','','','','','',52,'adrianasekulic@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (85,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ALIA d.o.o.','','','','','','','',52,'dzaje14@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (86,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','T.S. ELEKTROTIM ','','','','TAVANKUTSKA 2','','10000','Zagreb',52,'tomislav.supina@xnet.hr','','+385-95-9102697','','','','','','','');
INSERT INTO `contacts` VALUES (87,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Orhideja praonica rublja j.d.o.o','','25975336177','','Mostarska 1C, Soblinec','','10360','Sesvete',52,'babic.mato@googlemail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (88,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','DOKUMENTA d.o.o.','','00414682067','','Rudeška cesta 99','','10000','Zagreb',52,'josip.dadic@dokumenta.hr','','+385-993322381','','','2015200 - Zagreb - max 50km (29.04.2015 pokupili stvari iz IKEA-e za Pašman)','','','','');
INSERT INTO `contacts` VALUES (93,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','NEO ORBIS d.o.o.','','45109449461','','Pokornoga 81','','10000','Zagreb',52,'darko@pentax.hr','','+386-91-1524859','','','','','','','');
INSERT INTO `contacts` VALUES (94,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ILIJA KOVAČEVIĆ','','','','KUPAC GRAĐANIN','','','',52,'','','','','','','','','','');
INSERT INTO `contacts` VALUES (95,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Romano Miljković','','','','Benka Benkovića 3a','','23000','Zadar',52,'romano.miljkovic@gmail.com','','','','','popust rani booking','','','','');
INSERT INTO `contacts` VALUES (96,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Luka De Marco','','','','Kupac gradanin','','','',52,'demarco.lka@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (97,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','SLOBOĐANAC d.o.o.','','57865866839','','Kozari bok IV.odvojak 6','','10000','Zagreb',52,'slobodjanac1@zg.t-com.hr','','+385-91-316-2300','','','Korisnik ostvaruje popust 15%. Višekratno korištenje','','','','');
INSERT INTO `contacts` VALUES (98,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mladen Rajković','','','','Kupac građanin','','','',52,'mladenrajkovic@hotmail.com','','','','','Modest preporuka','','','','');
INSERT INTO `contacts` VALUES (107,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','STRUKTURA d.o.o.','','','','','','','',52,'darko@struktura.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (108,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','MEDI PRODUCT d.o.o.','','77224327384','','Mesnička 26','','10000','Zagreb',52,'arsov.dancho@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (109,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Josip Pende','','','','Kupac gradanin','','','',52,'jpende5@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (110,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GLAS ZIVOTINJA','','','','','','','',52,'ttabak37@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (112,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Tomislav Jech','','','','Kupac gradanin','','','',52,'tomislav@terraneofestival.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (117,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Agencija za znanost i visoko  obrazovanje','','83358955356','','Donje Svetice 38/5','','10000','Zagreb',52,'jadranka.zimbrek@azvo.hr','','+385 1 6274 895','','','','','','','');
INSERT INTO `contacts` VALUES (119,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Scrinium d.o.o.','','','','B. Adžije 34 ','','10000','Zagreb',52,'info@scrinium-tours.hr','','+38513646 751','','','','','','','');
INSERT INTO `contacts` VALUES (121,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivan Trgo','','','','Kupac građanin','','','',52,'ivantrgo@net.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (122,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ACCESSUS d.o.o.','','94661221794','','Hruševečka 1','','10000','Zagreb',52,'v.belina@retailsolutions.hr','','+385 91 6658 188','','','','','','','');
INSERT INTO `contacts` VALUES (123,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Dejan','','','','Kupac građanin','','','',52,'mdejan@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (124,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mladen Rutalj','','','','Kupac građanin','','','',52,'mrutalj@mup.hr','','','','','Fali kategorija7pa smo dlai 15% popusta.','','','','');
INSERT INTO `contacts` VALUES (125,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Bio Pharm Vet d.o.o','','59417050556','','Medvedgradska 1c','','10000','Zagreb',52,'a.klasic@bio-pharm-vet.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (126,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','STYROTHERM d.o.o','','55985296493','','Stupničke Šipkovine 3','','10250','Donji Stupnik',52,'styrotherm@bih.net.ba','','','','','Popust 15% jer fali veći kombi.','','','','');
INSERT INTO `contacts` VALUES (127,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','FOTO SERVIS d.o.o.','','65187328485','','Nova cesta 171','','10000','Zagreb',52,'fotoservisdoo@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (128,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','HRVATSKO KASKADERSKO DRUŠTVO','','38953235314','','Čikoševa 2','','10000','Zagreb',52,'stunt.toni.bobeta@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (129,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','INTESA SANPAOLO CARD D.O.O.','','63558150971','','Radnička cesta 50','','10000','Zagreb',52,'denita.burcar@intesasanpaolocard.com','','+385-99-730-2201','','','','','','','');
INSERT INTO `contacts` VALUES (130,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KOŠĆAL d.o.o.','','','','','','','',52,'marko@koscal.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (131,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ETNOGRAFSKI MUZEJ GRADA ZAGREBA','','99544354954','','Trg Ivana Mažuranića 14 ','','10000','Zagreb',52,'tajnistvo@emz.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (138,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Nino Klobas','','','','','','','',52,'nino.klobas@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (139,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ZU Ljekarna Pablo d.o.o.','','23197705042','','Šarengradska 6','','10000','Zagreb',52,'igor.milak@ljekarna-pablo.hr','','+385-99-2563765','','','18% popusta zbog ranijeg vračanja. 7 sati ranije','','','','');
INSERT INTO `contacts` VALUES (140,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivana Sadrija','','','','Kupac građanin','','','',52,'ivana.sadrija@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (141,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','AMPEU','','','','','','','',52,'stjepan.juricic@mobilnost.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (142,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','PRIJEVOZNIČKI OBRT HONESTA','','6577321233','','Đakovačka 23','','10000','Zagreb',52,'honesta@net.hr','','+385-95-7413751','','','Popust-modest preporuka','','','','');
INSERT INTO `contacts` VALUES (143,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','BEKAMENT','','','','','','','',52,'info@bekament.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (144,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SV Group d.o.o.','','','','','','','',52,'drazen.petrovic@svgroup.hr','','','','','Kratki najam i fali vozilo','','','','');
INSERT INTO `contacts` VALUES (145,'0000-00-00 00:00:00','2015-12-24 20:04:35','','','PLAYBOX GRUPA D.O.O.','','25553634985','','Zagrebačka cesta 156D','','10000','Zagreb',52,'manuel.stiglic@playboxgrupa.hr','','','','','','Pravna osoba','Manuel Štiglić','','');
INSERT INTO `contacts` VALUES (146,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Ordinacija Bastaić','','','','Domjanićeva 5','','10000','Zagreb',52,'ljubisa.bastaic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (147,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','INSig2 d.o.o.','','33116950552','','Buzinska cesta 58','','10000','Zagreb',52,'danijel.blazevic@insig2.eu','','','','','2015231 - Osijek','','','','');
INSERT INTO `contacts` VALUES (148,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','FIL.B.IS Projekt d.o.o.','','737027026','','Osječka 34','','10000','Zagreb',52,'info@filbis.hr','','','','','2015198 - Ljubljana  2015201 - produžek najma iz 2015198','','','','');
INSERT INTO `contacts` VALUES (153,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mislav Kasalo','','','','-kupac građanin','','','',52,'','','','','','','','','','');
INSERT INTO `contacts` VALUES (159,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','AVAC d.o.o.','','02814749529','','Kesten Brijeg 5','','10000','Zagreb',52,'avac@avac.hr','','+385 1 4580275','','','','','','','');
INSERT INTO `contacts` VALUES (160,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Franjo Cavar','','','','','','','',52,'pansion.cavar@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (162,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ante Šprljan','','','','','','','',52,'asprljan@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (163,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','SUTON GRAF d.o.o.','','18277198474','','Ožujska 2','','10000','Zagreb',52,'boris.golubic@sutongraf.hr','','+385-99-5263111','','','','','','','');
INSERT INTO `contacts` VALUES (164,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','TEHNIČKA ŠKOLA RUĐERA BOŠKOVIĆA','','49811265576','','Getaldičeva 4','','10000','Zagreb',52,'res.ipaprojekt@gmail.com','','+385-1-2371061','','','','','','','');
INSERT INTO `contacts` VALUES (165,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Zeljko Savic','','','','','','','',52,'zsavic1974@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (173,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','PILANA SIMUNIC','','','','','','','',52,'pilanasimunic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (174,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hrvoje Bradica','','','','','','','',52,'hbradica@gmail.com','','','','','Popust zbog ranog bookinga','','','','');
INSERT INTO `contacts` VALUES (175,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Simo','','','','','','','',52,'simo1000@net.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (176,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','udruga puz','','','','','','','',52,'puz@udrugapuz.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (177,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','NILA MEDIA GRUPA D.O.O.','','83572273882','','Ivana Lozice 3','','21000','Split',52,'nilamediagrupa@gmail.com','','+385-91-319-18-99','','','','','','','');
INSERT INTO `contacts` VALUES (178,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Zvonko Zagrajski','','','','','','','',52,'zvonko.zagrajski@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (179,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','SUPER PROGRAM J D.O.O.','','98717070183','','Travanjska 16','','10000','Zagreb',52,'bozo.kvesic@superprogram.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (180,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','FRIMANTLE MEDIA ','','','','Oporovecka 12','','10000','Zagreb',52,'valentina.klasic@fremantle.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (181,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GKP STEFANJE d.o.o.','','64324403899','','','','','',52,'denis@stefanje.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (183,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','LjubišaBastaić','','-kupac građanin','','Domjanićeva 5','','10000','Zagreb',52,'ljubisa.bastaic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (187,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','GIP STUDIO D.O.O.','','91468164660','','Savska cesta 56','','10000','Zagreb',52,'intermedo@gmail.com','','+385-1-6177-119','','','GIP STUDIO D.O.O.','','','','');
INSERT INTO `contacts` VALUES (188,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ATM d.o.o.','','','','Slavoska avenija 22e','','10000','Zagreb',52,'vlatka.kink@atm.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (189,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','AGROPLAST SISAK d.o.o.','','70023893910','','Lipa ulica 20','','44000','Sisak',52,'agroplast01@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (190,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ENERGOSPEKTAR d.o.o','','40966163739','','Alberta Fortisa 1','','10000','Zagreb',52,'milodrag.gadze@energospektar.hr','','+385-98-5263113','','','','','','','');
INSERT INTO `contacts` VALUES (191,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Biserka Tešević','','','','','','','',52,'biba.tesevic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (192,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','FremantleMedia Hrvatska d.o.o.','','','','','','','',52,'damir.silov@fremantle.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (193,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Zlatko Vlajčić','','','','','','','',52,'zlatkovlajcic@yahoo.com','','+385-98-607760','','','','','','','');
INSERT INTO `contacts` VALUES (194,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ZEM NADZOR d.o.o.','','','','','','','',52,'elamihovilovic@hotmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (195,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','NEMETH-PROJEKT d.o.o.','','88463041378','','Radnička 57','','10000','Zagreb',52,'boran.petljak@nemeth-projekt.hr','','+385-99-4683445','','','','','','','');
INSERT INTO `contacts` VALUES (196,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DELTRON d.o.o.','','','','','','','',52,'antonija.gabela@deltron.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (203,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','AUREL-MED d.o.o.','','16597482939','','Ive Paraća  6  ','','10360',' Sesvete  ',52,'zlatkovlajcic@yahoo.com','','+385-98-607760','','','','','','','');
INSERT INTO `contacts` VALUES (207,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mario Turkalj','','','','Kupac gradanin','','','',52,'turkaljmario@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (208,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Imas viska','','','','','','','',52,'miro.lujic@rtl.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (209,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','GRAFIČKI URED VL.MARIO ANIČIĆ','','99033755649','','Mesnička 35','','10000','Zagreb',52,'mario.ured@gmail.com','','+385--98-384-895','','','','','','','');
INSERT INTO `contacts` VALUES (210,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Marko Konjevod','','','','Kupac građanin','','','',52,'marko@easeofmove.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (211,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MENORAH FILM','','','','','','','',52,'fjp@menorah-film.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (212,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','UDRUGA BOSANSKIH HRVATA PRSTEN','','92653606230','','Grada Vukovara 235','','10000','Zagreb',52,'biskicfilip@gmail.com','','','','','humanitarna pomoć','','','','');
INSERT INTO `contacts` VALUES (221,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ADRIA MEDIA ZAGREB d.o.o.','','30421345069','','Radnička cesta 39','','10000','Zagreb',52,'t.rakos@adriamedia.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (223,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','BIJELA HARMONIJA d.o.o.','','31022857153','','Samoborska cesta 266','','10000','Zagreb',52,'prodaja@harmonija-usluge.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (227,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Gordana Matijević','','','','Kupac građanin','','','',52,'gmatijevic0807@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (228,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','CONTING d.o.o.','','58276100989','','Vučnik 92','','51300','Delnice',52,'conting@ri.t-com.hr','','+385-98-9834788','','','','','','','');
INSERT INTO `contacts` VALUES (229,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ŽEPOH d.o.o.','','','','','','','',52,'zeljko.bosnar@zepoh.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (230,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ALFA TRANS','','','','','','','',52,'alfatrans0@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (231,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','KUHN-HRVATSKA d.o.o.','','04057188037','','Rakitnica 4','','10000','Zagreb',52,'danko.weber@kuhn.hr','','+385-99-3101555','','','','','','','');
INSERT INTO `contacts` VALUES (232,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','UniCredit Leasing Croatia d.o.o.','','','','Centar Bundek – D. T. Gavrana 17','','10000','Zagreb',52,'ivan.spoljar@unicreditleasing.hr','','','','','popust 20% ako je rezervacija izvšena do23.07.2013','','','','');
INSERT INTO `contacts` VALUES (233,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','HRVATSKI POLJOPRIVREDNI ZADRUŽNI SAVEZ','','','','','','','',52,'bozo.volic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (237,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivana Sladić','','','','Kupac građanin','','','',52,'ivana.lenic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (238,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Nikola Salinovic','','','','','','','',52,'johnny53ster@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (239,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ZAGREBAČKA UMJETNIČKA GIMNAZIJA','','','','','','','',52,'vanda.koludrovic@umjetnicka-gimnazija.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (243,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Maja Vujnovac','','','','','','','',52,'maja37@windowslive.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (244,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Ured za suzbijanje zlouporabe droga Vlade Republike Hrvatske','','20706369236','','Preobraženska 4/II','','10000','Zagreb',52,'marina.fulir@uredzadroge.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (245,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Međunarodna komisija za sliv rijeke Save','','','','Kneza Branimira 29','','10000','Zagreb',52,'isrbc@savacommission.org','','','','','2015161 - rani booking, treći najam, Ljubljana- Beaograd maraton','','','','');
INSERT INTO `contacts` VALUES (246,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Marko Stokov','','','','','','','',52,'marko.stokov@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (247,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hrvatsko društvo za robotiku','','','','','','','',52,'jelka4@net.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (248,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Vanja Šikić','','','','','','','',52,'vanja.sikic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (250,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ATLAS d.d.','','','','Izidora Kršnjavoga 1','','10000','Zagreb',52,'zdravko.krsnik@atlas.hr','','+385 1 4698 015','','','','','','','');
INSERT INTO `contacts` VALUES (251,'0000-00-00 00:00:00','2015-12-28 08:53:44','','','SINAGO d.o.o.','','64020845944','','Mokrice 180D','','','Oroslavje',52,'proizvodnja@sinago.hr','','+385 1 4550615','','','- bave se stolarijom','Pravna osoba','Mario Pilat','00385 91 6005 206','');
INSERT INTO `contacts` VALUES (252,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Lidl Hrvatska d.o.o k.d.','','','','','','','',52,'mislav.gajinov@lidl.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (253,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Martina Golić','','','','','','','',52,'golic.martina@hotmail.fr','','','','','','','','','');
INSERT INTO `contacts` VALUES (254,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ADRIATIKA DRVO d.o.o.','','','','','','','',52,'adriatika.drvo@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (255,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Danijel Vezmarović','','','','Kupac građanin','','','',52,'vezmarovic.danijel2@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (258,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Goran Šamec','','','','','','','',52,'','','','','','','','','','');
INSERT INTO `contacts` VALUES (259,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Saša Šafranič','','','','Kupac građanin','','','',52,'safranic@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (260,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Dasović','','','','','','','',52,'dasovic@msn.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (261,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','AZONPRINTER d.o.o.','','76288894727','','Matije Jandrića 20','','10000','Zagreb',52,'anamarija@azonprinter.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (262,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Peter Churdo','','','','','','','',52,'pchurdo@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (263,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','PLIVA HRVATSKA d.o.o.','','','','Prilaz baruna Filipovića 25','','10000','Zagreb',52,'maja.mihoci@pliva.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (264,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','DTR d.d.','','29787128314','','Preradovićeva 20','','10000','Zagreb',52,'vprerad@deteer.hr','','+385 91 518 3135','','','','','','','');
INSERT INTO `contacts` VALUES (265,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Zrinka Milošev','','','','Kupac građanin','','','',52,'zrinka.milosev@gmail.com','','','','','15.03 ---- 4 dana----','','','','');
INSERT INTO `contacts` VALUES (266,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hrvoje ','','','','','','','',52,'htroselj@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (267,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','PERIVAN USLUGE d.o.o.','','','','','','','',52,'ivpercic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (270,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','KUĆA PRODAJE d.o.o.','','50539738333','','Ulica grada Vukovara 269d','','10000','Zagreb',52,'cimbur55@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (271,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Sandra Mavračić','','','','','','','',52,'sandramavracic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (272,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Maja Šilhard','','','','Kupac građanin','','','',52,'majasilhard@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (275,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','UGO GRUPA d.o.o.','','','','Savska cesta 165','','10000','Zagreb',52,'uprava@ugo-grupa.hr','','00385-99-204-5025','','','','','','','');
INSERT INTO `contacts` VALUES (276,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','KOVINOPOJASAR d.o.o.','','01102530839','','Šarengradska 11','','10000','Zagreb',52,'kovinopojasar-dpb@zg.t-com.hr','','+385-98-9851-882','','','','','','','');
INSERT INTO `contacts` VALUES (277,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Vjekoslav Ćavar','','','','Kupac građanin','','','',52,'vcavar@net.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (278,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Damir Šimić','','','','Kupac građanin','','','',52,'sime.damir@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (279,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Krešo Kristo','','','','Kupac građanin','','','',52,'kreso.kristo@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (280,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Branko Ducato','','','','','','','',52,'branko.ducato@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (281,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','FRENCH CONNECTION d.o.o.','','08494126774','','Rimski jarak 24','','10000','Zagreb',52,'frenchconnection.hr@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (282,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Marina Leko','','','','','','','',52,'romana.zidak@nielsen.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (285,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ŽRK Samobor','','93078403397','','Perkovčeva 90','','10430','Samobor ',52,'htroselj@gmail.com','','+385-995355723','','','','','','','');
INSERT INTO `contacts` VALUES (286,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Štefica Vincek','','','','Kupac građanin','','','',52,'stefica.vincek@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (287,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Domagoj Matašić','','','','Kupac građanin','','','',52,'dmatasic@hotmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (288,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Smiljan Prpić','','','','Kupac građanin','','','',52,'smiljan48@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (297,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','TEHNO-VENT D.O.O.','','','','','','','',52,'tehno-vent@zg.t-com.hr','','','','','TEHNO-VENT@ZG.T-COM.HR','','','','');
INSERT INTO `contacts` VALUES (298,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Pero Posavi','','','','Kupac građanin','','','',52,'perodani@net.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (300,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','MEĐUNARODNI SAJAM SERVIS d.o.o.','','39683805636','','Rijeznica 66','','10000','Zagreb',52,'alen.huskic@if-services.net','','','','','','','','','');
INSERT INTO `contacts` VALUES (301,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','NUM','','','','','','','',52,'mario@num.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (302,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','INTERMEZZO UDRUGA','','','','','','','',52,'intermezzo.kultura@gmail.com','','','','','Vozili prošle godine','','','','');
INSERT INTO `contacts` VALUES (303,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','INTERIJER PROJEKT d.o.o.','','','','','','','',52,'info@interijerprojekt.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (304,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','DESANO STUDIO d.o.o.','','28348688204','',' Petrovaradinska 69','','10000','Zagreb',52,'sanja@desano-studio.hr','','','','','2015159 - Poreč','','','','');
INSERT INTO `contacts` VALUES (305,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Jelens Stanin','','','','Kupac građanin','','','',52,'jelena.stanin@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (306,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','RECRO','','','','Avenija Većeslava Holjevca 40','','10000','Zagreb',52,'nina.vujicic@recro-net.hr','','+385 99 3030 652','','','','','','','');
INSERT INTO `contacts` VALUES (307,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Ministarstvo graditeljstva i prostornog uređenja','','','','Ulica Republike Austrije 20','','10000','Zagreb',52,'ivan.derek@mgipu.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (308,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Nikšić','','','','Kupac građanin','','','',52,'vniksic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (309,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','HRVATSKI MUZEJ TURIZMA','','47076735780','','Park Angiolina 1','','51410','Opatija',52,'marin.pintur@hrmt.hr','','','','','Veza ETnografski muzej','','','','');
INSERT INTO `contacts` VALUES (310,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Dario Radović','','','','Kupac građanin','','','',52,'milica.radovic@infodom.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (311,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','DALEKOVOD PROIZVODNJA','','','','Vukomerička 9','','10410','Velika Gorica',52,'dubravko.mestrovic@dalekovod.hr','','','','','već bili','','','','');
INSERT INTO `contacts` VALUES (314,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','BELVISIO d.o.o.','','19736265288','','Rakovčeva 16','','10000','Zagreb',52,'iprgomet1@net.amis.hr','','','','','igor','','','','');
INSERT INTO `contacts` VALUES (315,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Studentski zbor Sveučilišta u Rijeci','','','','','','','',52,'medunarodna@sz.uniri.hr','','0998855652','','','Popust 15% ako plate rezervaciju do 8.4','','','','');
INSERT INTO `contacts` VALUES (316,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','TEHNO DOM d.o.o.','','','','','','','',52,'ivan.miksa@tehno-dom.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (318,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','IPA projekta FERTEO','','','','','','','',52,'andrija.petrovic@zagreb.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (320,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Vladimir Nikšić','','','','-kupac građanin','','','',52,'vniksic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (323,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivo Uvodić','','','','','','','',52,'iuvodic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (324,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Goran Karlović','','','','Kupac građanin','','','',52,'kgogoo@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (325,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ORGANIZACIJA MLADIH STATUS: M','','96065912144','','Zinke Kunc 3','','10000','Zagreb',52,'mislav.mandir@status-m.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (326,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','DIALOG KOMUNIKACIJE','','','','Šoštarićeva 10','','10000','Zagreb',52,'veronika.slogar@dialog-komunikacije.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (327,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Vijeće bošnjačke nacionalne manjine Grada Zagreba','','44926257293','','Trnjanska cesta 35','','10000','Zagreb',52,'vbnmgz@vbnmgz.hr','','01 631 41 09','','','','','','','');
INSERT INTO `contacts` VALUES (328,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','UDRUGA INOVATORA HRVATSKE','','','','','','','',52,'idujmovic@inovatorstvo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (329,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Domagoj Miletić','','','','','','','',52,'do.miletic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (330,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','NETGEN d.o.o.','','56703914419','','Ante Mike Tripala 3','','10000','Zagreb',52,'davorka@netgen.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (331,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','HESET d.o.o.','','04135878306','','Mandlova 3','','10000','Zagreb',52,'dijana.bello@heset.hr','','+385-95-1972121','','','','','','','');
INSERT INTO `contacts` VALUES (337,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Vicko Fiorenini','','','','Kupac građanin','','','',52,'vicko.fiorenini@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (338,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mirjana Strbac','','','','Kupac građanin','','','',52,'mirjanastrbac@yahoo.de','','','','','','','','','');
INSERT INTO `contacts` VALUES (339,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Matija Bahorski','','','','Kupac građanin','','','',52,'matija.bahorski@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (340,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','HSA-SF','','','','ivana lučića 5','','10000','zagreb',52,'filip.merdic@gmail.com','','','','','-7 dana...7.7.2012 10..','','','','');
INSERT INTO `contacts` VALUES (341,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','FUČKALA d.o.o.','','','','','','','',52,'fuckala@fuckala.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (342,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Elsa Lindgreen','','','','Kupac građanin','','','',52,'elsalehen@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (343,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Vedran Zoričić','','','','Kupac građanin','','','',52,'vedran.zoricic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (344,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Nikolina Radočaj','','','','Kupac građanin','','','',52,'nikolina.radocaj@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (345,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','INETEC','','69707113052','','Dolenica 28','','10250','Lučko/Zagreb',52,'marina.bedran@inetec.hr','','','','','2015107 - 03.05.2015- dali nedelju gratis, 4000km ukupno po kombiju Rumunjska','','','','');
INSERT INTO `contacts` VALUES (346,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Marion Špehar','','','','','','','',52,'marion.spehar@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (349,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','STUDIO DENTALNE MEDICINE  FRNTIĆ','','51719765777','','Mlinovi 159a','','10000','Zagreb',52,'info@studiodental-frntic.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (350,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','NOVALIJA KOMPAS','','','','','','','',52,'novalija-kompas@gs.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (351,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ŠKOLSKA KNJIGA','','','','','','','',52,'renata.sadzak@skolskaknjiga.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (352,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DHC','','','','','','','',52,'info@dhc.com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (353,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','MEDIAVAL d.o.o.','','','','Budmanijeva 5','','10000','Zagreb',52,'zdravko.ivic@media-val.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (354,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','REDOX d.o.o.','','36138701402','','Lanište 24','','10000','Zagreb',52,'redox@redox.hr','','+385-98-274981','','','','','','','');
INSERT INTO `contacts` VALUES (355,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','MAJER-PROM d.o.o.','','33412382465','','Kriška 32','','10000','Zagreb',52,'kristina.juricic@ad-promet.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (356,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','SPEKTAR PUTOVANJA d.o.o.','','39672837472','','Tkalčićeva 15','','10000','Zagreb',52,'glodic.goran@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (358,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','FLUID SISAK d.o.o.','','83678016096','','A.Starčevića 1A','','43000','Sisak',52,'cafefluid@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (359,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Tihomir Trikvić','','','','Kupac građanin','','','',52,'ttihomir@hotmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (360,'0000-00-00 00:00:00','2016-01-21 11:28:02','','','HORTILAB d.o.o.','','01384523625','','Sisačka 4','','10410','Velika Gorica',52,'hortilab.info@gmail.com','','','','','','Pravna osoba','Vilim Elez','00385 95 2029892','');
INSERT INTO `contacts` VALUES (361,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MULTIMATIKA','','','','','','','',52,'multimatika@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (362,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivica Srncevic','','','','Kupac građanin','','','',52,'srna1@windowslive.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (363,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ORBICO','','','','','','','',52,'darko.krnic@orbico.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (365,'0000-00-00 00:00:00','2016-04-01 20:22:25','','','INTERIER MONT d.o.o.','','35036017877','','Želježnička 41','','32227','Borovo',52,'zarko@interier-mont.hr','','','','','','Pravna osoba','Žarko Aleksić','00385981361979','');
INSERT INTO `contacts` VALUES (374,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','FALKO d.o.o.','','29928371299','','Krvarić 76 C ','','10000','Zagreb',52,'ttihomir@hotmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (376,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SPLITSKI SPORTSKI SAVEZ GLUHIH','','','','','','','',52,'mirek2001hr@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (377,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','PERO oplate i skele','','','','','','','',52,'danka.pamic@peri.com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (379,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','shaun  underwood','','','','','','','',52,'julia@iasnikatravel.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (380,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','DAMONT INTERIJERI d.o.o.','','27473530124','','Lukačićeva 7','','','Samobor',52,'persicj@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (381,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Daniel Kempgen','','','','','','','',52,'Daniel.Kempgen@statravel.de','','','','','','','','','');
INSERT INTO `contacts` VALUES (387,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SAKI&SAKI d.o.o.','','','','','','','',52,'sadik.rakovic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (388,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Hr CČP','','','','Savska c. 41/IV','','10000','Zagreb',52,'ivana.ivicic@cro-cpc.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (389,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','GENERA LIJEKOVI d.o.o.','',' 98483262055','','Svetonedeljska 2','','10436','Rakov Potok',52,'DPisonic@genera.hr','','+385 99 78 88 623','','','','','','','');
INSERT INTO `contacts` VALUES (390,'0000-00-00 00:00:00','2015-12-22 09:21:51','','','UDRUGA ŠAPICA','','30697626511','','Kanadska 3','','10290','Zaprešić',52,'vukasovi@unhcr.org','','','','','','','','','');
INSERT INTO `contacts` VALUES (391,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GDi','','','','','','','',52,'tomislav.svilicic@gdi.net','','','','','','','','','');
INSERT INTO `contacts` VALUES (392,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Kathrin Kaufhold','','','','','','','',52,'kaufhold@isw-gmbh.de','','','','','','','','','');
INSERT INTO `contacts` VALUES (393,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SMELT INŽINJERING d.o.o.','','','','','','','',52,'instalacije.mamic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (394,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','RODIN LET D.O.O.','','33704349594','','Čanićeva 14','','10000','Zagreb',52,'tijana@roda.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (395,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','KERAMIKA MODUS d.o.o.','','93756665118','','Vladimira Nazora 67','','33515','Orahovica',52,'ghorvat@keramika-modus.com','','+385-98-205-681','','','','','','','');
INSERT INTO `contacts` VALUES (396,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KOCKICA','','','','','','','',52,'tajnik@kockice.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (397,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','GEBRUDER WEISS d.o.o.','','05216322294','','Jankomir 25','','10000','Zagreb',52,'marija.jaksic@gw-world.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (398,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','MULTI FORMA d.o.o.','','69146219847','','Jakuševečka 15','','10000','Zagreb',52,'multi-forma@zg.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (399,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ante Prizmić','','','','','','','',52,'tonci94@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (400,'0000-00-00 00:00:00','2016-04-26 16:02:40','','','HRVATSKO UDRUŽENJE STUDENATA BRODOGRADNJE','','74463657147','','Ivana Lučića 5','','10000','Zagreb',52,'tomich.denis@gmail.com','','','','','','Pravna osoba','Franko Kovačević','00385 98 9591514','');
INSERT INTO `contacts` VALUES (415,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Grdović','','','','','','','',52,'jgrdovic@zv.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (416,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','SVEUČILIŠTE U RIJECI TEHNIČKI FAKULTET','','46319717480','','Vukovarska 58','','51000','Rijeka',52,'tonik@riteh.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (417,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','LOGISTIKA VIOLETA','','62874063131','','Obrež Zelinksi 55','','10380','Sv.Ivan Zelina',52,'dijana.bilic@violeta.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (418,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Damir Marijan','','','','','','','',52,'damirmarijan@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (419,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ALFA TRANS d.o.o.','','','','','','','',52,'dejan.buic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (420,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Miroslav Bukovšćak','','','','Kupac građanin','','','',52,'miroslav.bukovscak@zg.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (421,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','POSILOVIĆ D.ZAŠTITA k.d..','','63051259879','','Ljudevita Posavskog 14','','10000','Zagreb',52,'sanja.subotnik@posilovic-zastita.hr','','','','','skračeno i redovito','','','','');
INSERT INTO `contacts` VALUES (422,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','FORTUNA KOMERS d.o.o.','','','','','','','',52,'martina.mihelj@fortuna-digital.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (423,'0000-00-00 00:00:00','2015-12-28 08:56:32','','','OBRT ZA TRGOVINU, PRIJEVOZ I IZNAJMLJIVANJE ZLATNA CIPELICA , VL. ANKICA GOJEVIĆ','','75651976730','','Okučanska 11','','10040','Zagreb',52,'ankica_gojevic@hotmail.com','','','','','najam kombija, partner iznajmljivač, rent a car ','Pravna osoba,Dobavljač','Petar Gojević','','');
INSERT INTO `contacts` VALUES (425,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','BACAČI SJENKI','','75226535695','','Bosanska ulica 10','','10040','Zagreb',52,'bacaci.sjenki@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (426,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mislav Šajnović','','','','','','','',52,'sajnovic166@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (427,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','RODA','','','','','','','',52,'ured@roda.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (428,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','E-DIZAJN uslužni obrt i putnička agencija','','13863801781','','Škorpikova 11','','10000','Zagreb',52,'karlo@e-dizajn.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (429,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Damir','','','','','','','',52,'dapola@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (430,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ADMETAM BUSINESS CONSULTANTS d.o.o.','','93922704282','','Rapska 46','','10000','Zagreb',52,'s.curic@admetam.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (431,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ichtis d.o.o.','','','','','','','',52,'operations@ichtisonline.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (438,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','MEĐ. SAJAM SERVIS d.o.o.','','39683805636','','Rijeznica 66','','10000','Zagreb',52,'Ifs@if-services.net','','','','','','','','','');
INSERT INTO `contacts` VALUES (439,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ILOV','','','','','','','',52,'info@ilov.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (445,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Centar za građanske inicijative Poreč','','11593010193','','Partizanska 2d','','52440','Poreč',52,'cgiporec@cgiporec.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (446,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ECOINA d.o.o.','','','','','','','',52,'etudic@ecoina.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (447,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ZLARING d.o.o.','','97665390982','','Crnčićeva 9','','10000','Zagreb',52,'zlaring@zlaring.hr','','+385 1 2303937','','','','','','','');
INSERT INTO `contacts` VALUES (448,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','MEDIA REGIS d.o.o.','','49012352778','','Matice Hrvatske 7','','10410','Velika Gorica',52,'vedran@mediaregis.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (449,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','JK - NAUTIKA','','00648383346','','Jagodno 105b','','10415','Novo Čiče',52,'info@jk-nautika.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (450,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','THERMIA d.o.o.','','81433864455','','Vukovarska 142','','31540','Donji Miholjac',52,'luka.kovacevic@thermia.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (451,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','LS TURISTIČKA AGENCIJA','','','','Ante Starčevića 71','','21300','Makarska',52,'','','+385 21 678 566 ','','','','','','','');
INSERT INTO `contacts` VALUES (452,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GEO ISTRAŽIVANJE','','','','','','','',52,'geoistrazivanje@zg.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (453,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Luka Despot','','','','Kupac građanin','','','',52,'ldespot@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (454,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','FOTOGRAFICA','','','','','','','',52,'info@fotografica.biz','','','','','','','','','');
INSERT INTO `contacts` VALUES (455,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','z@corkx.co.uk','','','','','','','',52,'z@corkx.co.uk','','','','','','','','','');
INSERT INTO `contacts` VALUES (456,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','NATURA ISA d.o.o.','','33041190079','','Bulvanova 14','','10000','Zagreb',52,'zoranh@naturaisa.com','+385-95-8000464','','','','','','','','');
INSERT INTO `contacts` VALUES (457,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Koraljka Stazić','','','','Kupac građanin','','','',52,'koraljka.stazic@zg.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (458,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','BAYER d.o.o.','',' 56386591827','','Radnička cesta 80','','10000','Zagreb',52,'kresimir.simicic@bayer.com','','','','','15.05','','','','');
INSERT INTO `contacts` VALUES (466,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','TEHNOKOM d.o.o.','','','','','','','',52,'renato.gecan@tehnokom.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (467,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Bonfanti d.o.o','','','','','','','',52,'vickovic@meinl.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (468,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Bojan Kuscevic','','','','','','','',52,'bojan.kuscevic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (469,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','PROMO IDEJA d.o.o.','','','','','','','',52,'info@promologic.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (470,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','GAMBOR d.o.o.','','88521601653','','Šenova 4','','10000','Zagreb',52,'flanjka@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (474,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Tomislav Vučković','','','','Kupac građanin','','','',52,'t.vuchkovich@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (475,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','MORIS d.o.o.','','46480621776','','Belostenčeva 3','','10000','Zagreb',52,'nadabenko@gmail.com','+385-98-452775','','','','','','','','');
INSERT INTO `contacts` VALUES (476,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','HORECA TRGOVINA d.o.o','','69500958846','','Čulinečka cesta 152','','10040','Zagreb',52,'sasa@horeca-trgovina.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (477,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','VETERINARSKA STANICA VRBOVEC d.o.o.','','43025336094','','Kolodvorska 68','','10340','Vrbovec',52,'vujevic.vet@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (478,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Kazalište, vizualne umjetnosti i kultura Gluhih - DLAN','','','','','','','',52,'udruga.dlan.zagreb@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (479,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','TECUS d.o.o.','','49889862881','','Radnička cesta 48,','','10000','Zagreb',52,'ambalaza@ambalaza.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (480,'0000-00-00 00:00:00','2015-12-22 09:37:46','','','Intercon Dubrovnik tur. I pom.agencija d.o.o.','','13369950936','','Dr.A.Starčevića 69','','20000','Dubrovnik',52,'intercon@intercon.hr','','+385-98-393008','','','','','','','');
INSERT INTO `contacts` VALUES (481,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','IVAS GRUPA d.o.o.','','22926614320','','Donje Svetice 40','','10000','Zagreb',52,'andreja.tot@ivas.hr','','','','','2015237 - Ljubljana   2015254 - Vodice','','','','');
INSERT INTO `contacts` VALUES (482,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','UDRUGA STUDENATA MEĐIMURJA','','28396279066','','Ilica 10/3','','10000','Zagreb',52,'udruga.studenata.medjimurja@gmail.com','+385-98-763-658','','','','','','','','');
INSERT INTO `contacts` VALUES (493,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','WAWA d.o.o.','','59785180614','','Zagrebačka 26','','10430','Samobor',52,'ivan.jezic@wawa.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (494,'0000-00-00 00:00:00','2015-12-22 09:21:51','','','Športsko društvo gluhih \"Silent\"','','09942019810','','Ulica grada Vukovara 284/5','','10000','Zagreb',52,'hss.gluhih@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (495,'0000-00-00 00:00:00','2015-12-22 09:21:51','','','MIRT INTERIJER d.o.o. ','','28491086971','','1. novački odvojak 15','','10437','Bestovje',52,'mirt.interijer.d.o.o@zg.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (499,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','UGOSTITELJSKI OBRT CAFFE BAR \"BOOGIE JUNGLE\"','','','','','','','',52,'boogiejungle@net.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (500,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','SCHENKER d.o.o., Croatia','','','','Slavonska avenija 52i','','10000','Zagreb',52,'marina.baron@dbschenker.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (501,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ANKA PRANJIĆ','','','','','','','',52,'spektra.tim@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (504,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','INGRA-M.E. d.o.o.','','48123751191','','Humboldta Alexandera 4/b','','10000','Zagreb',52,'milan.cicvara@ingra.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (505,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','OPT Tržnica','','','','','','','',52,'niko.pervan@otp-trznca.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (509,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Javna ustanova RERA SD','','','','Domovinskog rata 2','','21000','Split',52,'marija.vucica@rera.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (510,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','PRESSCUT d.o.o.','','34672089688','','Domagojeva 2','','10000','Zagreb',52,'marko.poljak@presscut.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (511,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Srečko Pačić','','','','','','','',52,'srecko.pacic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (512,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','AQUA PURITAS','','','','','','','',52,'aqua.puritas@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (513,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Goran Ivezić','','','','Kupac građanin','','','',52,'goran.ivezic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (514,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','EBIT','','','','','','','',52,'michael@ebit.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (515,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ZVONA USLUGE d.o.o.','','99421577215','','Busovačka 19','','10000','Zagreb',52,'tomislav@zvonacatering.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (516,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','METRO CONSILIUM d.o.o.','','07078076353','','Milana Butkovića 4','','51000','Rijeka',52,'mladen@colledani.hr','','+385994426967','','','','','','','');
INSERT INTO `contacts` VALUES (517,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','HRVATSKI ŠPORTSKO RIBOLOVNI SAVEZ','','','','Krešimira Čosića 11','','10000','Zagreb',52,'rentacar.modest@gmail.com','','','','','19.07. do 29.07. 540,00 ','','','','');
INSERT INTO `contacts` VALUES (518,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','KRAS GRUPA d.o.o.','','53200971578','','26 DIVIZIJE 8','','51415','LOVRAN',52,'info@kras-grupa.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (519,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','PRINT GRUPA d.o.o.','','07275785182','','Brezjanski put 33','','10431','Sveta Nedjelja',52,'valentina@printgrupa.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (520,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','FESB','','','','','','','',52,'dbalic@fesb.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (521,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','MI-STAR d.o.o.','','44816778493','','Novska ulica 24','','10000','Zagreb',52,'mario@mistar.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (522,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Sveučilište u Zagrebu Agronomski fakultet','','76023745044','','Svetošimunska c. 25','','10000','Zagreb',52,'dpreiner@agr.hr','','+385 98 391836','','','','','','','');
INSERT INTO `contacts` VALUES (523,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','SABRINA DOMINUS d.o.o.','','25414636119','','4.Luka 14','','10040','Zagreb',52,'parfumerija.dominus@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (524,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Nenad Crnica','','','','Kupac građanin','','','',52,'neno@sampos.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (525,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','G.K. MAKSIMIR','','26735791870','','Ulica grada Vukovara 284','','10000','Zagreb',52,'sanja.samec@hotmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (526,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Zlatko Čule','','','','Kupac građanin','','','',52,'zlatko.cule@ri.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (527,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','HUCK FINN d.o.o.','','66802959608','','Vukovarska 271','','10000','Zagreb',52,'bruno@huck-finn.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (536,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','INGRA ME d.o.o.','','48123751191','','Alexandera Humboldta 4B','','10000','Zagreb',52,'milan.cicvara@ingra.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (540,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','HRVATSKI SAVEZ SJEDEĆE ODBOJKE','','72382180496','',' K o l a r o v a 18','','10000','Zagreb',52,'tajnik.hsso@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (541,'0000-00-00 00:00:00','2015-12-22 09:21:51','','','GRAD DUBROVNIK - UPRAVNI ODJEL ZA KULTURU','','21712494719','','Branitelja Dubrovnika 7','','20000','Dubrovnik',52,'ssimunovic@dura.hr','','+ 385 99 52 000 46','','','','','','','');
INSERT INTO `contacts` VALUES (542,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','HULAHOP d.o.o.','','00595398836','','Vlaška 72a','','10000','Zagreb',52,'nino@animafest.hr','','+385-91-1765636','','','','','','','');
INSERT INTO `contacts` VALUES (551,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Marin Bartolić','','','','Kupac građanin','','','',52,'fertoll@post.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (555,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Tanja Pajk','','','','Kupac građanin','','','',52,'tanja.pajk@zagreb.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (556,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','CONUS INŽENJERING d.o.o.','','15242307029','','Žrtava fašizma 1B','','','Umag',52,'info@conus-ing.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (557,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','VIKAPOTA','','','','','','','',52,'info@vikapota.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (558,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','AKTERPUBLIC','','','','Remetinečka cesta 9d ','','10000','Zagreb',52,'ivan.bartolovic@akterpublic.hr','','+385 91 5858 769','','','','','','','');
INSERT INTO `contacts` VALUES (559,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Sanja Žugaj','','','','','','','',52,'udut.zagreb@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (569,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Krešimir Vuković','','','','Kupac građanin','','','',52,'vukovic.kresimir@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (570,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mate Dragičević','','','','Kupac građanin','','','',52,'kardumzrinka10@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (573,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','PDR d.o.o.','','42222541140','','Trg Žrtava fašizma 6','','10290','Zaprešić',52,'info@pdr.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (574,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','UDRUGA BUDISTIČKO DRUŠTVO SHECHEN','',' 14856368987','','Antuna Mihića 10','','',' 51410 Opatija',52,'shechencroatia@gmail.com','','+385-98-267-427 ','','','','','','','');
INSERT INTO `contacts` VALUES (575,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','SARGON j.d.o.o.','','58114974989','',' Kralja Tomislava 98','','35410','Nova Kapela',52,'info@kombi-rent.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (576,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivan Kljaić','','','','Kupac građanin','','','',52,'ivan.kljaic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (577,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','PROMONDIS d.o.o.','','92796174019','','Slovenska 13','','10437','Bestovje',52,'ana.profaca@promondis.hr','','','','','2015112 - na drugo vozilo dodatnih 5% jer idu u paru  2015233 - Požega  2015235 - Vukovar','','','','');
INSERT INTO `contacts` VALUES (578,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','POU Mato Došen','','','','','','','',52,'dora.markuz@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (584,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','KUD \"ZORA\"','','57615715265','','Kneza Adama 1,','','10363','Belovar',52,'ivansikic@net.hr','','+385-095-5919 627','','','','','','','');
INSERT INTO `contacts` VALUES (585,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Dubravka Ciglenecki','','','','Kupac građanin','','','',52,'dubravkaciglenecki@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (591,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Slaven Gašparić','','','','Kupac građanin','','','',52,'','','','','','','','','','');
INSERT INTO `contacts` VALUES (592,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GORAN HANŽEK','','','','','','','',52,'marin@meneghetti.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (593,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','VEKTOR GRUPA d.o.o.','','40485772360','','Joha 3','','10040','Zagreb',52,'antonio@vektorgrupa.com','','+385-99-2437900','','','','','','','');
INSERT INTO `contacts` VALUES (594,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hrvatska škola Outward Bound','','','','','','','',52,'dubravka.simunec@outwardbound.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (595,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','DADO SISAK d.o.o.','','79282509413','',' Petrinjska 156','','','Sisak',52,'dado1sisak@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (596,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','B.N.S. GRADNJA d.o.o.','','65325497806','','Polanjščak 6','','10000','Zagreb',52,'bbrzic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (597,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','POU AMC Nova Gradiška','','','','KOARSKA 2','','35400','Nova Gradiška',52,'gbakunic@pou-amc.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (598,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','OMEGA GRUPA d.o.o.','','34767079413','','Tomislavova 11/2','','10000','Zagreb',52,'boris@omega-grupa.hr','','','','','DARKO KOVAČ','','','','');
INSERT INTO `contacts` VALUES (599,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','FESB SPLIT','','','','','','','',52,'ups.fesb@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (600,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Smiljan Berišić','','','','Kupac građanin','','','',52,'stjohnspark2014@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (601,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','HIDRAULIKA SERVISI d.o.o.','','18117849564','','Žitnjak b.b.','','10000','Zagreb',52,'info@hidraulika-servisi.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (602,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Klaudio Skok','','','','Kupac građanin','','','',52,'klaudio.skok1@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (613,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Marko Kramarić','','','','','','','',52,'marko.kramaric@fero-term.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (614,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Hrvatska komora dentalne medicine','','','','Kurelčeva 3','','10000','Zagreb',52,'ivana.staresinic@hkdm.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (615,'0000-00-00 00:00:00','2016-03-09 10:33:36','','','KOTURIĆ d.o.o.','','01928398713','','Čulinečka Cesta 221/e','','10000','Zagreb',52,'koturic@koturic.hr','00385 99 2944 048','','','','Firma se bavi prodajom i montažom guma.','Pravna osoba,Dobavljač,Servisni centar','Hrvoje Koturić','00385 99 2944 048','gume,vulkanizer');
INSERT INTO `contacts` VALUES (616,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','GRAFIK.NET d.o.o.','','25187588059','','Odranska 1/1','','10000','Zagreb',52,'mirko@grafiknet.hr','+385-99-3097-836','','','','','','','','');
INSERT INTO `contacts` VALUES (617,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','NEBESKI BALONI d.o.o.','','18577584563','','Savska cesta 50','','10000','Zagreb',52,'info@nebeskibaloni.hr','','+385-91-6930307','','','','','','','');
INSERT INTO `contacts` VALUES (624,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Krešimir Gagulić','','','','Kupac građanin','','','',52,'kresimir.gagulic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (625,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MIROSLAV VIDOVIĆ','','','','','','','',52,'info@morris-studio.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (626,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','RHINO PRODUKCIJA d.o.o.','','40826829003','','Brodska 7','','10000','Zagreb',52,'mladen@rhino.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (632,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Darko Brajdić','','','','Kupac građanin','','','',52,'sandra@brajdic.de','','','','','','','','','');
INSERT INTO `contacts` VALUES (634,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ELLABO d.o.o.','','','','Kamenarka 24','','10000','Zagreb',52,'andrea.simic@ellabo.hr','','+385 1 6611115','','','','','','','');
INSERT INTO `contacts` VALUES (635,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Zlata Jauk','','','','Kupac građanin','','','',52,'zlata.jauk2@h-1.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (636,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','PRECI','','','','','','','',52,'precibh@gmail.cim','','','','','','','','','');
INSERT INTO `contacts` VALUES (637,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','TEHNOMEDIKA doo','','','','','','','',52,'zoran.krtinic@tehnomedika.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (638,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','TADEJA KETEKOZARY','','','','','','','',52,'tadeja.ketekozary@hep.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (639,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Bruno Lalić','','','','','','','',52,'bruno.lalic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (640,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Jelena Šerić','','','','Kupac građanin','','','',52,'','','','','','','','','','');
INSERT INTO `contacts` VALUES (641,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ZAKLADANI STAVEB a.s.','','66304376486','','II Cvjetno Naselje 19','','10000','Zagreb',52,'zakladani@zakladani.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (642,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Denis Bajraktarević','','','','Kupac građanin','','','',52,'dinopromet@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (643,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','SUNCE ZA VAS d.o.o.','','84185112238','','Šubičeva 64','','10000','Zagreb',52,'www-sun-for-you@zg.t-com.hr','','+385-98-754142','','','','','','','');
INSERT INTO `contacts` VALUES (644,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','EKO LIBERTAS','','','','','','','',52,'d.simovic3@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (645,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Blitz film i video distribucija d.o.o.','','69856063967','','Kamenarka 1','','10000','Zagreb',52,'maja.pavlinusic@blitz.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (646,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MAGURO d.o.o.','','','','','','','',52,'sasa@maguro-fishing.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (661,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Marina Pavičić','','','','Kupac građanin','','','',52,'marinap@phy.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (662,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ETRANET GRUPA d.o.o.','','36044044039','','Ulica Frana Folnegovića 1B,','','10000','Zagreb',52,'marin.ljoljic@etranet.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (663,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Darka Zimić','','','','Kupac građanin','','','',52,'dzimic@hotmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (665,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','BLITZ FILM','','','','','','','',52,'maja.pavlinusic@blitz.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (670,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','COBRA SISAK d.o.o.','','45195887063','','I. Gundulića 30','','44000','Sisak',52,'iv.volaric@gmail.com','','+385-91-5543580','','','','','','','');
INSERT INTO `contacts` VALUES (671,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','UNICEF','','18282095247','','Radnička cesta 41','','10000','Zagreb',52,'dgorski@unicef.org','','+385-1-2329 606','','','','','','','');
INSERT INTO `contacts` VALUES (672,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','LJEPOTICA d.o.o.','','','','Jakova Gotovca 17/1','','10360','Sesvete',52,'komercijala.ljepotica@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (675,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Ured UNICEF-a u RH','','18282095247','','Radnička cesta 41/VII','','10000','Zagreb',52,'dgorski@unicef.org','','+385-1-2329 606','','','','','','','');
INSERT INTO `contacts` VALUES (676,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','URBANA GALERIJA ','','','','','','','',52,'marijana@urbana-galerija.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (677,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','VODOPRIVREDA ZAGREB d.d.','','','','','','','',52,'vodoprivreda02@vzg.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (678,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','VERGL','','','','','','','',52,'vergl@vergl.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (679,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','OPG Milica Kuzmić','','16076556994','','Negrijeva 4','','52100','Pula',52,'milan.kuzmic@pu.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (680,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Filip Buzuk','','','','Kupac građanin','','','',52,'filip.buzuk@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (684,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','TRGOVAČKO USLUŽNI OBRT WOX Vl.Božo Medić','','20256467728','','ZAGREBAČKA 13','','10410','Velika Gorica',52,'wox2@net.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (685,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Janković','','','','','','','',52,'jankovic@sfzg.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (686,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Nino Barčanac','','','','Kupac građanin','','','',52,'barcanac@fkit.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (687,'0000-00-00 00:00:00','0000-00-00 00:00:00','','',' Ivan Greganić','','','','Kupac građanin','','','',52,'ivan.greganic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (693,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SELECTI AREA d.o.o.','','','','','','','',52,'tomasic.mario@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (694,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','FERROSTIL MONT d.o.o.','','','','','','','',52,'boris.sajko@ferrostilmont.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (695,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','AEROTEH d.o.o.','','','','','','','',52,'josipa.tausan@aeroteh.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (702,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','BOR TRANS vl.Marijana Boršić','','30659765058','','Grletinec 47/2','','49231','Hum na Sutli',52,'ivica@bortrans.hr','','+385 (91) 340 5840','','','','','','','');
INSERT INTO `contacts` VALUES (705,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','BEVEL d.o.o.','','60271794480','','Narodnog oslobođenja 9','','51306','Čabar',52,'info@bevel.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (706,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','MANIFEST MEDIA','','','','Hebrangova 11','','10000','Zagreb',52,'dario@manifest.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (707,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Maja Tolić','','','','Kupac građanin','','','',52,'mayacr7@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (708,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','UABHDR','','','','Kneza Mutimira 5','','10000','Zagreb',52,'uabhdr@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (710,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MULTI CONNECTO d.o.o.','','','','','','','',52,'igor@multiconnecto.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (712,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','WHITE PEARL d.o.o.','','99388954115','','Trepčanka 21','','10040','Zagreb',52,'ured.visnjan@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (713,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','ESTARE CULTO d.o.o.','','91151135167','','Folnegovićeva 1C','','10000','Zagreb',52,'sasakondic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (714,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Dijana Martine','','','','Kupac građanin','','','',52,'dijana55@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (715,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Gordan Barić','','','','','','','',52,'baricgordan@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (716,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ON-AIR sistemi d.o.o.','','','','','','','',52,'miroslav.jeras@broadstream.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (719,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','STAR PRODUKCIJA d.o.o.','','89276507815','','Čire Truhelke 21','','10000','Zagreb',52,'elfpecunia@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (720,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Velimir Kukas','','','','Kupac građanin','','','',52,'velimirkukas@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (721,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','MIRIAM','','','','Kotrmanova 9','','10410','Velika Gorica',52,'info@miriam-dg.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (722,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','METAMORFOZA d.o.o.','','24880192958','','Bukovačka cesta 174','','10000','Zagreb',52,'vedran@metamorfoza.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (723,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','OK VIHOR','','','','','','','',52,'vihor@vihor.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (727,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','LIVADA PRODUKCIJA d.o.o.','','65708682409','','Zavrtnica 17','','10000','Zagreb',52,'toni@livada-produkcija.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (728,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','SENNA d.o.o.','','60437320326','','Negovečka 6','','10000','Zagreb',52,'sennam@gmail.com','','+385-91-2011056','','','','','','','');
INSERT INTO `contacts` VALUES (729,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','MAJO COMMERCE d.o.o.','','81876879825','','Tina Ujevića 13','','10000','Zagreb',52,'majo-commerce@xnet.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (730,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','SPORT CONCEPT d.o.o.','','','','Ravnica b.b.','','10000','Zagreb',52,'ana@sportconcept.hr','','','','','2015118 - traže dostavni auto za Begrad. Zato je putnički tako jeftin','','','','');
INSERT INTO `contacts` VALUES (731,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','MIRIAM LOGISTIKA d.o.o.','','86060931675','','Kotrmanova 9','','10410','Velika Gorica',52,'info@miriam-dg.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (733,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Siniša Bajšić','','','','Kupac građanin','','','',52,'sbajsic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (734,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GROMEL d.o.o.','','','','','','','',52,'jlackovic@gromel.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (738,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ENIKON PROJEKTIRANJE','','','','','','','',52,'boris.petrovic@enikon.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (739,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GAM INTERIJER d.o.o.','','','','','','','',52,'ognjen@gam-interijer.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (740,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ENDORA d.o.o.','','','','','','','',52,'goran@endora.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (741,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','NE-KOM OBRT','','92629689944','','Braće Ribara 33','','10437','Bestovje',52,'neno2905@net.hr','','','','','u 4mj popravak RI-645-UP - u Vodicama - korisnik platio račun  u 5.mj 1250 umanjili račin za 1250kuna  u 4mj ri-815-ua - sajba oštećena','','','','');
INSERT INTO `contacts` VALUES (742,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Miljenko Žeger','','','','Kupac građanin','','','',52,'mzeger@net.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (743,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Josip Saraga','','','','Kupac građanin','','','',52,'josip.saraga@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (744,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Krešimir Burnać','','','','Kupac građanin','','','',52,'k.burnac@hotmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (745,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hlupić','','','','','','','',52,'','','','','','','','','','');
INSERT INTO `contacts` VALUES (746,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MARTINGRAD d.o.o.','','','','','','','',52,'martin.grad@optinet.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (754,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Boris Vižintin','','','','Kupac građanin','','','',52,'k.burnac@hotmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (755,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Damon Ronald Antičević','','','','','','','',52,'zeljkaanticevic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (756,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KLAROM DELICIJE d.o.o.','','','','','','','',52,'klarom-delicije@zg.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (757,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','TERAD d.o.o.','','22078329702','','Gojlanska ulica 41','','10040','Zagreb',52,'kresimir.karacic@terad.hr','','+385-98386297','','','','','','','');
INSERT INTO `contacts` VALUES (758,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mario KESER','','','','Kupac građanin','','','',52,'mariok2306@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (759,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Heinirch Boell Stiftung','','73690702958','','Preobraženska 2','','10000','Zagreb',52,'Maja.BucicGrabic@hr.boell.org','','','','','','','','','');
INSERT INTO `contacts` VALUES (760,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','HRABRI TELEFON','','','','','','','',52,'ella@hrabritelefon.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (761,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Mototrip d.o.o.','','81732611777','','A. Mihanovića 14','','10450','Jastrebarsko',52,'info@mototrip-tours.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (765,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Vatroslava Mišetić','','','','Kupac građanin','','','',52,'vatroslava.misetic@zg.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (767,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SING','','','','','','','',52,'miroslav.cukovic@sing.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (768,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Michael Dagostin','','','','','','','',52,'m.dagostin@inet.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (769,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','HRVATSKO DRUŠTVO LIKOVNIH UMJETNIKA','','','','','','','',52,'luka.hrvoj@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (772,'0000-00-00 00:00:00','2015-12-22 09:38:43','','','DEMA Karlovac','','5465023912','','Varaždinska 1','','47000','Karlovac',52,'info@dema.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (774,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Julia Underwood','','','','','','','',52,'underwood.j@iasnikatravel.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (776,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Zagrebački ronilački klub','','37596479831','','Hanamanova 1 A','','10000','Zagreb',52,'velimirkukas@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (780,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Milenium promocija d.o.o.','','','','','','','',52,'mihaela.miljkovic@mpr.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (781,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','OMP obrada metala i trgovina','','93831820829','','Gubaševo 3','','49210','Zabok',52,'plesko@plesko.hr','','+385 98 351153','','','','','','','');
INSERT INTO `contacts` VALUES (782,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KUD PRLJAVAC','','','','','','','',52,'prljavac@live.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (783,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Martina Kontošić','','','','','','','',52,'martina.kont@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (784,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Ivan i Marko d.o.o.','','62909566494','','Gornji Bukovac 101','','10000','Zagreb',52,'marko.kontek@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (785,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Martina Bezan','','','','','','','',52,'martina_bezan@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (786,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hrvoje Grubać','','','','Kupac građanin','','','',52,'hrvojegrubac@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (787,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','REAL CARGO CROATIA','','77758714505','','Radnička cesta 39','','10000','Zagreb',52,'tboris65@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (788,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','Tip putovanja-turistička agencija d.o.o.','','16520487009','','Vončinina 2/1','','10000','Zagreb',52,'maja@tiptours.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (789,'0000-00-00 00:00:00','2015-12-22 09:21:25','','','DRUGI PLAN D.O.O.','','','','Heinzelova 62a','','10000','Zagreb',52,'sanja.nikic@gmail.com','','+385 99 3563 272','','','','','','','');
INSERT INTO `contacts` VALUES (794,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ANTE SKORUP','','','','','','','',52,'anteskorup@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (795,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','APPLICON D.O.O','','76817252728','','Prisavlje 2','','10000','Zagreb',52,'anteb@pandopad.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (796,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Savez društava multiple skleroze Hrvatske','','','','','','','',52,'sdms_hrvatske@sdmsh.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (797,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DRAGICA BIKIĆ','','','','','','','',52,'dbgagi@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (798,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KRISTIAN KRALJ','','','','','','','',52,'kristiankralj@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (799,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','HROK D.O.O.','','','','','','','',52,'ured@hrok.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (800,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','ZIT d.o.o','','58721577676','','Rakitnica 2','','10000','Zagreb',52,'mario.radujkovic@zit-zg.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (801,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Krunoslav Milatović','','','','','','','',52,'kruno.milatovic@stipic.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (802,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','SALOMON d.o.o.','','94022363843','',' Rudolfa Bičanića 28','','10000','Zagreb',52,'denis@falizmaj.org','','','','','','','','','');
INSERT INTO `contacts` VALUES (803,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Zaklada \"Hrvatska kuća srca\"','','','','Ilica 5/II','','10000','Zagreb',52,'tea.lovric@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (804,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Daniel Fekter','','','','Kupac građanin','','','',52,'daniel.fekter@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (805,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Marko Planinić','','','','-kupac građanin','','','',52,'marinap@phy.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (809,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Kristan Herwing Kralj','','','','-kupac građanin','','','',52,'kristiankralj@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (816,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ŽELJKO ČAVLEK','','','','','','','',52,'zeljko.cavlek@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (817,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivo Žinić','','','','','','','',52,'isostari@zsem.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (818,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ilija Mikušić','','','','Kupac građanin','','','',52,'imisukic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (819,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','PRVO PLINARSKO DRUŠTVO d.o.o.','','58292277611','','Kardinala Alojzija Stepinca 27','','10000','Zagreb',52,'antonija.glavas@ppd.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (820,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MIRTA PRODUKCIJA','','','','','','','',52,'mirta.jusic.d@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (821,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Test za ponude','','','','','','','',52,'testzaponude@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (822,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ADVENA d.o.o.','','','','','','','',52,'advena@advena.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (827,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Matija Gikić','','','','','','','',52,'matija@gikic.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (828,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Lidija Vrbanek','','','','Kupac građanin','','','',52,'lidija.vrbanek@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (830,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Hyperborea d.o.o.','','87800945941','','Stepinčeva 38','','21311','Stobreč',52,'hyperborea.hr@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (831,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','APIN SUSTAVI d.o.o.','','22659472331','','Ožujska 8','','10000','Zagreb',52,'goran.krizanac@apin.hr','','','','','0','','','','');
INSERT INTO `contacts` VALUES (832,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Maja Komljenović','','','','Kupac građanin','','','',52,'maja_komljenovic@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (833,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','IVAN FILIPČIĆ','','','','','','','',52,'ivanfilipcic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (834,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Danijel Slišković','','','','','','','',52,'danijel.sliskovic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (835,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Krešimir Terzić','','','','Kupac građanin','','','',52,'kresimir.terzic@genera.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (841,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mrakec','','','','','','','',52,'mrakec@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (846,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Nora Šoda','','','','-kupac građanin','','','',52,'danijel.sliskovic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (847,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','SIGET – DIZALA d.o.o.','','19841995263','','Prečko 61','','10000','Zagreb',52,'tomislav.boskovic@hotmail.com','','+38598 9812760','','','','','','','');
INSERT INTO `contacts` VALUES (848,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Edison Krammer','','','','Kupac građanin','','','',52,'edison.krammer@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (850,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Sarfi Elvis','','','','Kupac građanin','','','',52,'elvis.sarfi@strabag.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (851,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ana Vučković','','','','Kupac građanin','','','',52,'ekipavuckici@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (852,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Branumir Borojević','','','','Kupac građanin','','','',52,'etsongo@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (853,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DENTAL PRACTICE ŠVAJHLER','','','','','','','',52,'ivan.svajhler@dent-med.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (855,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Oskar Rugas','','','','Kupac građanin','','','',52,'orugas@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (856,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','HNS','','','','','','','',52,'helena.puskar@hns-cff.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (857,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KROKO TRADE','','','','','','','',52,'krokotradeeu@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (858,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Antonio Skorjanec','','','','Kupac građanin','','','',52,'askorjanec@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (866,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hrvoje Bartolić','','','','','','','',52,'zglogistic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (867,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','MAPEI d.o.o.','','89471789472','','Purgarija 14, Kerestinec','','10431','Sveta Nedelja',52,'majda.lukic@mapei.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (868,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','INSTITUT TRADICIONALNOG TAEKWON-DOA','','80363051470','','Ivana Gundulića 3','','35000','Slavonski Brod',52,'itf.croatia@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (870,'0000-00-00 00:00:00','2016-03-08 10:20:21','','','Obrt za elektronsko mjerenje vremena CHAMPSTAT TIMING','','69853710977','','Matije Skurjenija 153','','10290','Zaprešić',52,'davorka@champstat.hr','','','','','','Pravna osoba','Davorka Borić','','');
INSERT INTO `contacts` VALUES (876,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DiArt d.o.o.','','','','','','','',52,'ivica.diart@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (877,'0000-00-00 00:00:00','2015-12-22 09:39:33','','','KOMIS d.o.o.','','','',' Pavla bb','','21212','Kaštel Sućurac',52,'ivan@komis.hr','','+385-91-531-6070','','','','Pravna osoba','','','');
INSERT INTO `contacts` VALUES (878,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mladen Šimić','','','','','','','',52,'mladensimic@ymail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (879,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Krešimir Vodopivec','','','','','','','',52,'kresimir.vodopivec@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (880,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','AKADEMIJA DRAMSKIH UMJETNOSTI','','','','','','','',52,'dapetkovic@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (881,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Odrčić','','','','','','','',52,'ajodorcic@live.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (882,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','POS SERVISI d.o.o.','','30421345069','','Čista velika I 82','','','Čista velika',52,'bruno.brckovic@pos-servis.eu','','+385 91 4456450','','','2014114 - 01.04.2015 - ima jednu paletu tonera, a mi nemamo teretnih kombija','','','','');
INSERT INTO `contacts` VALUES (883,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ninoslav Andrašević','','','','Kupac građanin','','','',52,'ninoslav.andrasevic1@zg.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (887,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Miroslav Zemljić','','','','','','','',52,'miroslav@bakina-kuca.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (888,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Hrvatsko etnološko društvo','','','',' Ivana Lučića 3','','10000','Zagreb',52,'zorany@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (889,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Via Tours','','','','','','','',52,'i.merkas@via-tours.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (890,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Kosta Uremović','','','','','','','',52,'kostaurumovic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (893,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','M San Grupa d.d.','','','','Buzinski prilaz','','10000','Zagreb',52,'zlatan.macan@msan.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (894,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ZMRP','','','','','','','',52,'jjuracak@agr.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (895,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ŽENE U DOMOVINSKO RATU','','','','','','','',52,'ruzica.posao@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (896,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','MMP - AUTOMATSKA VRATA d.o.o. ','','49261752334','','Božidara Kunca 1/IV','','10000','Zagreb',52,'info@mmp-automatskavrata.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (897,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivan Ketović','','','','Kupac građanin','','','',52,'','','','','','','','','','');
INSERT INTO `contacts` VALUES (898,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','POS-SERVIS d.o.o.','','30421345069','','Čista Velika 1-82','','',' 22214 Čista Velika',52,'bruno.brckovic@pos-servis.eu','','','','','','','','','');
INSERT INTO `contacts` VALUES (905,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Vlatko Brčić','','','','Kupac građanin','','','',52,'vlatko.brcic@hgi-cgs.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (906,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Miroslav Bučanac','','','','','','','',52,'mbucanac@net.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (907,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Tomislav Kralj','','','','','','','',52,'kraljtomislav21@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (908,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ŽKK NOVI ZAGREB','','','','','','','',52,'kiki13jaga@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (909,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Montažstroj','','19673322859','','Vojnovićeva 22','','10000','Zagreb',52,'petra.prsa@montazstroj.hr','','','','','2015170 - 30% - u najavi Berlin, Rijeka, Crna gora..','','','','');
INSERT INTO `contacts` VALUES (910,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Jurica Vincelj','','','','','','','',52,'juvincelj@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (911,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SALONA d.o.o.','','','','','','','',52,'r.renata97@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (912,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Tomislav Šumski','','','','','','','',52,'tsumski.vt@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (913,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Ivan Banožić','','','','','','','',52,'ivan.banozic007@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (914,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Vesna Spoza','','','','','','','',52,'rpretkovic@vesnasposa.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (924,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','PUČKI PRAVOBRANITELJ','','08026537914','','Trg hrvatskih velikana 6','','10000','Zagreb',52,'jelena.music@ombudsman.hr','','+385 1 4851855','','','','','','','');
INSERT INTO `contacts` VALUES (925,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','INŽINJERING MTK d.o.o.','','','','','','','',52,'mtk@mtk.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (926,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','FZOEU','','85828625994','','Radnička 80','','10000','Zagreb',52,'ojdana.ra@fzoeu.hr','','099 4382 920','','','','','','','');
INSERT INTO `contacts` VALUES (928,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','ROMSKO NACIONALNO VIJEĆE','','','','Poljana J.Andrassyja 9','','10000','Zagreb',52,'beganovic1992@hotmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (929,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','PERI OPLATE I SKELE d.o.o.','','','','','','','',52,'danka.pamic@peri.com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (934,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SKIT','','','','','','','',52,'kresimir.selci@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (940,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Amatersko dramsko kazalište','','','','','','','',52,'adksv.kriz.zacretje@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (941,'0000-00-00 00:00:00','0000-00-00 00:00:00','','',' Kineziološki fakultet Zagreb','','','','','','','',52,'dodosh8@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (942,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','JAGMA d.o.o.','','','','','','','',52,'marijo.rakic@jagma.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (943,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','NICRO','','','','','','','',52,'helga.cubric@nicro.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (944,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','FREUND j.d.o.o.','','12519744471','','Kutjevačka 3','','10000','Zagreb',52,'bnmcentar@gmail.com','','+385-99-6631004','','','','','','','');
INSERT INTO `contacts` VALUES (945,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','AUTO ODAK','','','','','','','',52,'odak@globalnet.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (946,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','MAJOR INTERNATIONAL d.o.o.','','49604933226','','Zagrebačka avenija 100A','','10000','Zagreb',52,'damir.hasakovic@major.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (948,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','MONTAŽSTROJ d.o.o.','','89876133223','','Vojnovićeva 22','','10000','Zagreb',52,'zdravko.popovic@montazstroj.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (950,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','TIP TOURS - PUTNIČKA AGENCIJA d.o.o.','','83011422157','','Nova Ves 69','','10000','Zagreb',52,'maja@tiptours.hr','','','','','1','','','','');
INSERT INTO `contacts` VALUES (953,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KV knjigovodstvo i marketing','','','','','','','',52,'kv.knjigovodstvo@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (954,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','NICRO d.o.o.','','71179651211','','Marijana Čavića 9','','10000','Zagreb',52,'igor.matisic@nicro.hr','','+385 1 2450 009','','','','','','','');
INSERT INTO `contacts` VALUES (955,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Studio Moderna - TV Prodaja d.o.o.','','','','Zadarska 80','','10000','Zagreb',52,'ivan.buzanic@studio-moderna.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (968,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','OBRT KISKO','','','','','','','',52,'branko.tumpa@zg.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (973,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ENERGOMETALI E.P.','','','','','','','',52,'energometal.ep@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (980,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Bravo putovanja d.o.o.','','92714688296','','Draškovićeva 55','','10000','Zagreb',52,'denis.cetusic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (981,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','VIT TRANSLOGISTIKA d.o.o.','','','','','','','',52,'vit-translogistika@email.ht.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (988,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','ZATON GRUPA D.O.O.','','36264929889','','POLJANA 36','','23232','Zaton',52,'','','','','','','','','','');
INSERT INTO `contacts` VALUES (991,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','ČISTA VODA d.o.o.','','42375187043','','Kovinska 4a','','10000','Zagreb',52,'stefica.greblicki@clearwater.hr','','+385 1 3463 430','','','','','','','');
INSERT INTO `contacts` VALUES (992,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','STOMATOLOŠKA ORDINACIJA DOKTORICE UDILJAK','','','','','','','',52,'udiljak.iva@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (993,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','ORGINAL KORČULA TRAVEL ','',' 36535173505','','Trg Sv. Justine 8 ','','20260','Korčula',52,'info@korcula-travel.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (994,'0000-00-00 00:00:00','0000-00-00 00:00:00','','',' BIOMEDICA DIJAGNOSTIKA d.o.o.','','','','','','','',52,'zlatko.svacov@bmgrp.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (995,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','AQUAMONT d.o.o.','','85058985197','','Sobolski put 16','','10000','Zagreb',52,'ante.matic@aquamont.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (999,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','24sata d.o.o. ','','78093047651','','Oreškovićeva 6H/1','','10000','Zagreb',52,'krunoslav.bagaric@24sata.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1000,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Centar za organizaciju građenja','','51521331536','','Gračanska cesta 39','','10000','Zagreb',52,'miroslavzemlji294@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1001,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','J.Britvec','','','','','','','',52,'j.britvec@brucha.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1002,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Teatar EXIT','','','','Ilica 208','','10000','Zagreb',52,'exit@teatarexit.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1003,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','TRAVABLED TURISTIČKA AGENCIJA J.D.O.O. ','','69431968517','','Antuna Bauera 19','','10000','Zagreb',52,'skekic7@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1004,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','CETIS d.o.o.','','','','','','','',52,'cetis@zg.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1005,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','MILENIJ HOTELI d.o.o.','','78796880101','','Ulica Viktora Cara Emina','','51000','Opatija',52,'skekic7@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1008,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','METALVAR d.o.o.','','','','','','','',52,'metalvar@metalvar.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1009,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Economic Chamber of Macedonia','','','','','','','',52,'snezana@mchamber.mk','','','','','','','','','');
INSERT INTO `contacts` VALUES (1010,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','CENTROTRANS','','','','','','','',52,'d.tolic@centrotrans.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1011,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','TRIPLEJUMP d.o.o.','','35875278809','','Selska 34','','10000','Zagreb',52,'dalibor.fustar@triple-jump.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1017,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','KLAPŠEC INTERIJERI d.o.o.','','46070195460','','Žuti breg 119','','10000','Zagreb',52,'klapsecinterijeri@gmail.com','','+385 91 2801801','','','','','','','');
INSERT INTO `contacts` VALUES (1018,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','OMNIS POWER j.d.o.o.','','','','','','','',52,'poweromnis@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1029,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','TRGOVAČKA ŠKOLA','','56064164023','','Trg J. F. Kennedyja br. 4','','10000','Zagreb',52,'visnja.birus@trgovacka-skola.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1030,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SEUL RESTORAN','','','','','','','',52,'seul.restoran@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1032,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MAK DIZAJN I SAVJETOVANJA','','','','','','','',52,'mak.dizajn.savjet@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1037,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Odvjetnik Nataša Tatarić','','34528292763','','Donji Prčac 23','','10000','Zagreb',52,'odvjetnik.natasa.tataric@zg.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1038,'0000-00-00 00:00:00','2015-12-22 09:21:26','','',' Nacionalni centar za vanjsko vrednovanje obrazovanja ','','','','Petračićeva 4','','10000','Zagreb',52,'vesna.vukovic@ncvvo.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1039,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Stype CS d.o.o ','','','','Velikopoljska 32','','10010','Zagreb',52,'np@stypegrip.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1040,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','AG,Matas d.o.o','','','','3.Cvjetno naselje 23','','10000','Zagreb',52,'agmatas1@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1041,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','VIVAT FINA VINA d.o.o.','','','','Frana Folnegovića 1b','','10000','Zagreb',52,'morana.petricevic@vivat-finavina.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1042,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GALERIJA PRICA','','','','','','','',52,'martina_kalle@net.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1046,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','PIK Vinkovci d.d.','','','','Matije Gupca 130','','32100','Vinkovci',52,'','','','','','','','','','');
INSERT INTO `contacts` VALUES (1047,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','EUROINSPEKT CROATIAKONTROLA d.o.o.','','50024748563','','Karlovačka cesta 4L','','10000','Zagreb',52,'info@croatiakontrola.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1048,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hsk-Adriatic d.o.o.','','19436019856','','Rusevje 11 ','','','',52,'sasa.popovic@hsk-adriatic.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1049,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Goethe-Institut Kroatien','','','','Ulica grada Vukovara 64','','10000','Zagreb',52,'doroteja.jakovic@zagreb.goethe.org','','','','','','','','','');
INSERT INTO `contacts` VALUES (1058,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','NATURPRODUKT d.o.o.','','','','','','','',52,'robert.milek@naturprodukt.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1059,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','TE-MARINE d.o.o.','','58003854845','','Šoštarićeva 4','','10000','Zagreb',52,'info@temarin.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1060,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Ustanova Zoološki vrt grada Zagreba ','','','','Maksimirski perivoj bb','','10000','Zagreb',52,'andrea@zoo.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1061,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Intercars d.o.o','','','','','','','',52,'kristijan.pajur@intercars.eu','','','','','','','','','');
INSERT INTO `contacts` VALUES (1062,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','M.T.F. d.o.o.','','','','Hondlova 2/2','','10000','Zagreb',52,'m.mestric@mtf.hr','','','','','tri vozila budmipešta','','','','');
INSERT INTO `contacts` VALUES (1063,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','HUDEK ZAGREB d.o.o.','','43013193376','','Sunekova 145','','10000','Zagreb',52,'hudek@hudek.hr','','','','','-popust 10.03 je zato jer je kombi 2 dana vani ne tri cijela.uzima utorak 20 sati/povrat petak u 8.00','','','','');
INSERT INTO `contacts` VALUES (1067,'0000-00-00 00:00:00','2015-12-23 16:15:22','','','KATAPULT d.o.o','','48566967897','',' Ulica Gordana Lederera 4 ','','10000','Zagreb',52,'kresimir@SHOOSTER.HR','','','','','Shooster','Pravna osoba','Krešimir Savić','00385 98 268464','');
INSERT INTO `contacts` VALUES (1068,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SRETNO PAŠKO SUNCE d.o.o.','','','','','','','',52,'sretno-pasko-sunce@hotmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1069,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Sindikat PPDIV','','','','','','','',52,'ivan.klakocer@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1070,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Media net d.o.o.','',' 60259436947','','Petrinjska 81','','10000','Zagreb',52,'ivana.rudelic@presscut.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1071,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Krka-farma d.o.o.','','','','Radnička cesta 48/II','','10000','Zgareb',52,'darija.tabulov@krka.biz','','','','','','','','','');
INSERT INTO `contacts` VALUES (1079,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Fox International Channels','','','','','','','',52,'Milena.Marinic@fox.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1080,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','TURISTICKA AGENCIJA OMEGA GRUPA','','','','Tomislavova 11/2','','10000','Zagreb',52,'boris@omega-grupa.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1081,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','RAČAN ELEKTRONIKA d.o.o.','','52795324928','','Klekova 9','','10000','Zagreb',52,'velimir.racan@zg.t-com.hr','','+385 98 282894','','','23.03. - ide u Dubrovnik','','','','');
INSERT INTO `contacts` VALUES (1082,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','AS Sesvete-Bus d.o.o.','','38774034343','','Ljudevita Posavskog 3','','10360','Sesvete',52,'as.sesvetebus@gmail.com','','','','','16.04- put u Beograd i locco Beograd sa turistima','','','','');
INSERT INTO `contacts` VALUES (1083,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','REHAU d.o.o.','','','','Franje Lučića 34','','10000','Zagreb',52,'stanislava.gasparevic-relic@rehau.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1084,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','VITKOVIĆ PROM d.o.o.','','50705099618','','Bocakova ul. 48','','','10380, Sveti Ivan Zelina',52,'nevitkovic@net.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1086,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Smart industrial services d.o.o.','','','','','','','',52,'marko.zubcic@smartservices.hr','','','','','6.4 - nizozemska radnici','','','','');
INSERT INTO `contacts` VALUES (1087,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','MG design d.o.o.','',' 30833969357','','Poduzetnička cesta b.b.','','10314','Križ',52,'kr.kamenscak@mgdesign.hr','','','','','2015111 - 27.03-putuje u Banja Luku','','','','');
INSERT INTO `contacts` VALUES (1093,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','NOVOGRADNJA ZAGORKA','','','','','','','',52,'tomasic@novogradnja-zagorka.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1097,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','CUNAE j.d.o.o.','','44676497343','','R. Cimermana 68','','10000','Zagreb',52,'info@noona.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1098,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','DB TRANS d.o.o.','','25245842783','','Stjepana Fabijančića 135','','10410','Velika Gorica',52,'info@vg-dbtrans.eu','','+385-91-1556666','','','2015120 - LJUBLJANA','','','','');
INSERT INTO `contacts` VALUES (1099,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Abrakadabra  integrirane komunikacije d.o.o.','','','','Metalčeva 5/VIII','','10000','Zagreb',52,'nina.remenar@abrakadabra.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1102,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','NORINGRADNJA','','','','','','','',52,'noringradnja@gmail.com','','','','','2015122 - Plitvice','','','','');
INSERT INTO `contacts` VALUES (1105,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Mirko Hanzlić','','01628828415','','J.J.Strossmayera 13a','','31500','Našice',52,'','','','','','','','','','');
INSERT INTO `contacts` VALUES (1107,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ATALIAN d.o.o.','','','','','','','',52,'ivanka.cevak@atalian.hr','','','','','2015123 - 3 vozila- Rijeka- jednokratno 30% popusta','','','','');
INSERT INTO `contacts` VALUES (1108,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','FAKULTET ELEKTROTEHNIKE I. RAČUNARSTVA','','57029260362','','Unska 3','','10000','Zagreb',52,'kristian.jambrosic@fer.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1109,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ANITA BOŠNJAK','','','','','','','',52,'anita.bosnjak1001@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1110,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','LIBERTAS','','','','','','','',52,'snjezana.baban@os.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1111,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ELKOM d.o.o.','','','','','','','',52,'elkom@bj.t-com.hr','','','','','-jednokratan poposi 23% - mali km','','','','');
INSERT INTO `contacts` VALUES (1112,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ŽUPANISKO DRŽAVNO ODVJETNIŠTVO','','','','','','','',52,'vlatka.durak@zdozg.dorh.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1119,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','HRVATSKI SABOR KULTURE','','45263394181','','Ulica kralja Zvonimira 17','','10000','Zagreb',52,'kazaliste@hrsk.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1120,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MAsaprint d.o.o.','','','','','','','',52,'josipa@masaprint.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1121,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','LAVITO usluge d.o.o. ','','96202705185','','Maksimirska 19','','10000','Zagreb',52,'kristina.juricic@ad-promet.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1123,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DOMOTEH d.o.o.','','','','','','','',52,'darko.tomljenovic@domoteh.hr','','','','','2014143 - zagreb - max 150km','','','','');
INSERT INTO `contacts` VALUES (1124,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','CONTRES PROJEKTI D.O.O. ','','','','','','','',52,'marina@contres.hr','','','','','2014144 - Bled-rezervacija 3 tjedan prije najma','','','','');
INSERT INTO `contacts` VALUES (1125,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Akademija za politički razvoj','','','','Makančeva 16','','10000','Zagreb',52,'jakov.zizic@politicka-akademija.org','','','','','','','','','');
INSERT INTO `contacts` VALUES (1126,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','KUPOLE - bolje od šatora d.o.o.','','77581043464','',' Lučko 47 B','','','Lučko',52,'mia@kupole.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1132,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','AUTO BENUSSI d.o.o.','','96262119913','',' Industrijska ulica 2/D','','','Pula',52,'','','','','','','','','','');
INSERT INTO `contacts` VALUES (1133,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Triumph International d.o.o.','','','','','','','',52,'hrvojeselinger@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1134,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MEP RIJEKA D.O.O.','','','','','','','',52,'ante@mep.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1137,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','BIZZ putovanja d.o.o.','','','','II Ravnice 1A','','10000','Zagreb',52,'tamara@bizztravel.biz','',' +385 1 4111 522','','','','','','','');
INSERT INTO `contacts` VALUES (1138,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Terme Tuhelj d.o.o.','','56566580479','','Ljudevita Gaja 4','','49215','Tuhelj',52,'sanja.popovic@terme-tuhelj.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1139,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Electus DGS d.o.o.','','','','Hondlova 2/11','','10000','Zagreb',52,'ana.horvat@electus.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1140,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','STATIPROM d.o.o.','','','','','','','',52,'statiprom@email.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1141,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','OPG MRAOVIĆ','','','','','','','',52,'kontakt@staridud.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1142,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KUNEŠTRA, obrt za trgovinu, vl. Nikolina Vrtodušić','','91120329117','',' Istarska 18','','','Novalja',52,'kunestra@gmail.com','','','','','2 kombija na 3 mjeseca ','','','','');
INSERT INTO `contacts` VALUES (1143,'0000-00-00 00:00:00','2015-12-22 09:21:26','','',' AS INTERIJERI d.o.o.','','60694690625','','Resnički put 55','','10000','Zagreb',52,'as.interijeri@zg.t-com.hr','','+385-91-4405070','','','2015163 - Rijeka - 2 kombija','','','','');
INSERT INTO `contacts` VALUES (1144,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','INSTITUT IGH d.d.','','79766124714','','Janka Rakuše 1','','10000','Zagreb',52,'ivan.mavar@igh.hr','','+385-98-418748','','','2015164 - TIRANA  2015172 - TIRANA PRODUŽETEK  2015191 - ZAGRBE LOCCO /MAX 200KM DAN','','','','');
INSERT INTO `contacts` VALUES (1146,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Veleposlanstvo Japana','','65169155509','','Boškovićeva 2','','10000','Zagreb',52,'prvonozac@zr.mofa.go.jp','','','','','','','','','');
INSERT INTO `contacts` VALUES (1147,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','ME TE ME d.o.o.','','','','','','','',52,'ana@meetme.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1148,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','DGS Ltd. Maritime Crewing Agency','','','','Milutina Baraca 7','','51000','Rijeka',52,'filip@dgs.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1149,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KFK TEHNIKA d.o.o.','','','','','','','',52,'andrea.bartolic@kfk.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1150,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','BIO NATURAL MED','','','','','','','',52,'bnmcentar@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1152,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','ALCON FARMACEUTIKA d.o.o','','97660092353','','Avenija Dubrovnik 16','','10000','Zagreb',52,'andres.arandia-kresic@alcon.com','','+385-91-4841426','','','','','','','');
INSERT INTO `contacts` VALUES (1158,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','DGS d.o.o.','','96250701832','','Milutina Barača 7','','51000','Rijeka',52,'filip@dgs.hr','','','','','1','','','','');
INSERT INTO `contacts` VALUES (1160,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Amali Doare Production','','','','Stare Gajnice 3','','10000','Zagreb',52,'uprava@topdestinacije.hr','','+385 95 8042 320','','','','','','','');
INSERT INTO `contacts` VALUES (1170,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','TEMPERO VERO','','','','','','','',52,'tempero.vero@gmail.com','','','','','slovenija','','','','');
INSERT INTO `contacts` VALUES (1171,'0000-00-00 00:00:00','2016-04-01 14:19:01','','','Fond za financiranje razgradnje NEK','','','','Radnička cesta 47','','10000','Zagreb',52,'sanja.miscevic@fond-nek.hr','','099/2717-174','','','','Pravna osoba','Sanja Miščević','00385 99 2717174','');
INSERT INTO `contacts` VALUES (1172,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Entrio tehnologije d.o.o.','','30513194761','','Ul.grada Vukovara 237A / I','','10000','Zagreb',52,'barbara@entrio.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1177,'0000-00-00 00:00:00','2015-12-22 09:21:51','','','BAUERELEKTRO d.o.o.','','32896907245','','Nikole Tesle 111','','10410','Velika Gorica',52,'bauelektro@bauelektro.hr','','+385-1-6251-826','','','','','','','');
INSERT INTO `contacts` VALUES (1182,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','EXPLANTA d.o.o.','','','','Kralja Tomislava 41/A','','10312','Kloštar Ivanić',52,'info@explanta.hr','','+385 1 2892 645','','','2015205 - Pula','','','','');
INSERT INTO `contacts` VALUES (1183,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','SUIDUM j.d.o.o.','','04179791202','','Dragutina Domjanića 25','','10410','Velika Gorica',52,'info@mondays.hr','','','','','2015206 - Berlin-Zagreb','','','','');
INSERT INTO `contacts` VALUES (1184,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Muzej za umjetnost i obrt','','','','','','','',52,'miroslav.gasparovic@muo.hr','','','','','2015207-Rim','','','','');
INSERT INTO `contacts` VALUES (1186,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Adriatic Escape','','','','','','','',52,'maja@adriaticescape.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1187,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','MAJOR INTERNACIONAL d.o.o.','','49604933226','','Zagrebačka avenija 100a','','10090','Zagreb',52,'nives.futac@major.hr','','+385 99 3177249','','','2015211 - Čakovec max 300km','','','','');
INSERT INTO `contacts` VALUES (1193,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Državni zavod za statistiku','','','','Branimirova 19','','10000','Zagreb',52,'brumnica@dzs.hr','','','','','2015220 - Beć','','','','');
INSERT INTO `contacts` VALUES (1194,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MAKRO EKO','','','','','','','',52,'info@makroeko.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1196,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','ĐURĐEVIĆ d.o.o.','','96560811477','','Brače Domany 8','','10000','Zagreb',52,'durdevic@zg.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1202,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Veleposlanstvo Islamske Republike Iran u Zagrebu','','00960251459','','Pantovčak 125c','','10000','Zagreb',52,'vbnmgz@vbnmgz.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1203,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','AQUILONIS','','','','','','','',52,'vesna.zivkovic@aquilonis.hr','','','','','2015228 - najam sa vozačem Zagreb-Čnomelj','','','','');
INSERT INTO `contacts` VALUES (1204,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Hen-Mar Proces d.o.o.','','','','','','','',52,'hen-mar@hen-mar.hr','','','','','2015232 - Banja luka','','','','');
INSERT INTO `contacts` VALUES (1210,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','AQUILONIS D.O.O.','',' 62288271688','','Vankina 14','','10000','Zagreb',52,'vesna.zivkovic@aquilonis.hr','','','','','2015228 - najam sa vozačem Zagreb-Čnomelj','','','','');
INSERT INTO `contacts` VALUES (1211,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Genesis d.o.o.','','','','','','','',52,'goran.kezic@genesis.hr','','','','','2015234 - Banja Luka','','','','');
INSERT INTO `contacts` VALUES (1217,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','PRIMALAB d.o.o.','','83028109264','','Matije Gupca 12a','','49210','Zabok',52,'marko.posavec@primalab.hr','','+385 99 807 3292','','','2015249 - Zabok selidba','','','','');
INSERT INTO `contacts` VALUES (1223,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','FAKULTET ELEKTROTEHNIKE I RAČUNARSTVA','','57029260362','','Unska 3','','10000','Zagreb',52,'kristian.jambrosic@fer.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1224,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Branko Boras','','','','','','','',52,'branko.boras@konzum.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1225,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DECATHLON','','','','','','','',52,'antun.petrusa@decathlon.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1232,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GENOS','','','','','','','',52,'glauc@genos.hr','','','','','2015265-Zagreb-Rijeka -Zaostrog','','','','');
INSERT INTO `contacts` VALUES (1233,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MONOTIP D.O.O','','','','','','','',52,'veselina.harabajs@monotip.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1234,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','COLLERS GRUPA d.o.o.','','59683724378','','Gustava Krkleca 6','','10090','Zagreb',52,'topfishing.zagreb@gmail.com','','+385-95-5755977','','','','','','','');
INSERT INTO `contacts` VALUES (1235,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','INTEGRA d.o.o.','','13405669320','','Trg kralja Tomislava 4','','42000','Varaždin',52,'zagreb@ivora.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1243,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','SOLLERS GRUPA d.o.o.','','59683724378','','Gustava Krkleca 6','','10090','Zagreb',52,'topfishing.zagreb@gmail.com','','+385-95-5755977','','','','','','','');
INSERT INTO `contacts` VALUES (1245,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','BroadStream Solutions d.o.o.','','01777360796','','Horvatova 82','','10010','Zagreb',52,'miroslav.jeras@broadstream.com','','+385 98 265 059','','','','','','','');
INSERT INTO `contacts` VALUES (1246,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','STYPE CS d.o.o.','','','','','','','',52,'robert.r@stypegrip.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1247,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','INDURESCO j.d.o.o.','','74454322085','','Prisavlje 2','','10000','Zagreb',52,'gallery.tempera@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1248,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','Specijalistička ordinacija obiteljske medicine Vesna Bajer ','','92437420910','','Trg Antuna Mihanovića 1','','42223','Varaždinske toplice',52,'viktor.domislovic@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1249,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','DENT OPREMA d.o.o.','','','','','','','',52,'katica.spehar@zg.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1257,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SINMAR d.o.o.','','','','','','','',52,'info@asap.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1258,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Davor Ivanov','','','','','','','',52,'davor.ivanov83@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1259,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','int','','','','','','','',52,'','','','','','','','','','');
INSERT INTO `contacts` VALUES (1260,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Kazalište Oberon','','','','','','','',52,'damirmadaric@hotmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1261,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','HORIZONT film j.d.o.o.','','30909739307','','Nad tunelom 9','','10000','Zagreb',52,'branko@horizont-production.eu','','+385 98 250 255','','','','','','','');
INSERT INTO `contacts` VALUES (1262,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','TORBO COLOR d.o.o.','','','','','','','',52,'farbo.interijeri@zg.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1263,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','BAJKMONT d.o.o.','','','','Svetomatejska 12','','10360','Sesvete',52,'miroslav@bajkmont.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1264,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SEKA VARAŽDIN','','','','','','','',52,'seka33@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1265,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','S1 d.o.o.','','10020122641','','Stjepana Širole 8','','10000','Zagreb',52,'s-1@inet.hr','','+385 95 5619324','','','','','','','');
INSERT INTO `contacts` VALUES (1266,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','LIMEA TRAVEL ZAGREB d.o.o.','','','','','','','',52,'limea.travelzg@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1267,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','VLADO ANDRAŠEVIĆ','','','','','','','',52,'vlado_and@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1274,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','NORMAN GRUPA d.o.o.','','','','','','','',52,'ante.odak@norman-grupa.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1277,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Mateja Nemet Andrasevic','','','','-kupac građanin','','','',52,'vlado_and@yahoo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1279,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SUMME SENSUS d.o.o.','','','','','','','',52,'tina@summesensus.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1280,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','PRO OPTIMUS','','','','Augusta Brauna 3','','71000','Sarajevo BIH',52,'pco@pro-optimus.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1281,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Euro-logistične usluge d.o.o.','','','','','','','',52,'eurologu@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1282,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','Synthesis d.o.o. ','','','','','','','',52,'jurica@synthesis.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1284,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','JEDINSTVO D.D.','','','','','','','',52,'jmihalic@jedinstvo.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1285,'0000-00-00 00:00:00','0000-00-00 00:00:00','','',' CENTAR ZA AUTIZAM','','','','','','','',52,'nada.bolont@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1286,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','V.D.P d.o.o.','','','','','','','',52,'stipcic.vinko@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1288,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','GALKO d.o.o.','','','','','','','',52,'marin@galko.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1289,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KMAG d.o.o.','','','','','','','',52,'iviana.penic@kmag.net','','','','','','','','','');
INSERT INTO `contacts` VALUES (1290,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','MLC','','','','','','','',52,'zdravko.babic@mlc.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1291,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','HETA d.o.o.','','157962295615','','Vlaška 62','','10000','Zagreb',52,'eniz@heta.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1292,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','SUPER ŠANSA d.o.o.','','','','','','','',52,'bozena.bicanic@stanleybet.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1293,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','PRIMOTRONIC d.o.o.','','01358353636','','Savska cesta 118','','10000','Zagreb',52,'sinisa.jorgic@primotronic.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1294,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','ŠUMARSKI FAKULTET','','07699719217','','Sveto Šimunska 25','','10000','Zagreb',52,'smikac@gmail.com','+385 99 3133795','','','','STJEPAN MIKAC - KONTAKT','','','','');
INSERT INTO `contacts` VALUES (1295,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','AGENTURA d.o.o.','','68817739468','','Rubeši 79','','51215','Kastav',52,'agentura@ri.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1296,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','HUBITK','','','','Trnjanska 140','','10000','Zagreb',52,'matea.uhlik-vidovic@hubitk.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1297,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','UNITAS d.d.','','','','','','','',52,'prodaja@unitas.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1298,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','STROJOGRADNJA ŠIVAK d.o.o.','','79517833007','','Demerska 9','','10251','Hrvatski Leskovac',52,'veljko.sivak@zg.t-com.hr','','+385 1 6578400','','','','','','','');
INSERT INTO `contacts` VALUES (1307,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','NOVA rent a car','','','','','','','',52,'josko@rentacarsplit.net','','','','','','','','','');
INSERT INTO `contacts` VALUES (1308,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','HUSZPO','','50729905264','','Berislavićeva 6','','10000','Zagreb',52,'huszpo@huszpo.hr','','+385 1 611 48 67','','','','','','','');
INSERT INTO `contacts` VALUES (1309,'0000-00-00 00:00:00','2016-05-05 09:32:38','','','SYSTEM ONE d.o.o.','','43566773819','','Ul. Grada Vukovara 237 D','','10000','Zagreb',52,'tomas.blaha@s1see.com','','','','','','Pravna osoba','Tomas Blaha','00385 99 6590991','');
INSERT INTO `contacts` VALUES (1310,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','PUŠIĆ d.o.o.','','','','','','','',52,'administracija@pusic.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1311,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','INVENTA d.o.o.','','99213847706','','K. A. Stepinca 7','','32000','Vukovar',52,'ana.krstic@inventa.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1312,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','3d studio','','','','','','','',52,'info@3dstudio.hr','','','','',',','','','','');
INSERT INTO `contacts` VALUES (1314,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','START KONGRESI','','','','','','','',52,'start@start-kongresi.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1315,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','TEST MAIL','','','','','','','',52,'igor.cukac@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1320,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','EMPE BIRO D.O.O.','','98617709143','','','','10360','Sesvete',52,'mandaric.srecko@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1321,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','SELES ZASTUPSTVA  d.o.o.','','18163429716','','Varaždinska 23','','10360','Sesvete',52,'mario.kovac@seles.hr','','+385 98 9824655','','','','','','','');
INSERT INTO `contacts` VALUES (1322,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','KB Dubrava Klinika za plastičnu kirurgiju','','','','','','','',52,'zstanec@kbd.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1323,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','ŠPREM-AMARENA d.o.o.','','90196496457','',' Ivana Gundulića 18','','42240','Ivanec',52,'sprem-amarena@vz.t-com.hr','','','','','','','','','');
INSERT INTO `contacts` VALUES (1324,'0000-00-00 00:00:00','0000-00-00 00:00:00','','','RIPINA d.o.o.','','','','','','','',52,'ripina.info@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1325,'0000-00-00 00:00:00','2015-12-22 09:21:26','','','HERMES INTERNATIONAL D.O.O','','81810672657','',' Nova ulica 5','','42204','Turčin',52,'mladen.ivanus1@gmail.com','','','','','','','','','');
INSERT INTO `contacts` VALUES (1326,'2015-12-23 09:14:40','2015-12-23 11:51:58','','','IMPULS-LEASING d.o.o.','','65918029671','','Velimira Škorpika 24/1','','10090 ','Zagreb',52,'pesut@impuls-leasing.hr','','','','','- leasing\r\n- osiguranje','Pravna osoba,Dobavljač','Goran Pešut','00385 099 2271 374','');
INSERT INTO `contacts` VALUES (1327,'2015-12-23 09:17:07','2016-03-09 10:36:15','','','Euroherc osiguranje d.d','','22694857747','','Ulica grada Vukovara 282','','10000','Zagreb',52,'milica.misic@euroherc.hr','','','','','- može ići na rate','Pravna osoba,Dobavljač','Milica Mišić','00385 98 416 920','kasko,osiguranje,auto');
INSERT INTO `contacts` VALUES (1328,'2015-12-23 09:19:59','2015-12-23 09:19:59','','','Auto Kuća Kovačević d.o.o.','','84485812058','','Bukovac gornji 1a','','10000','Zagreb',52,'','01 2396 925','','','','Prije bio kontakt Marinko Perković koji više ne radi.','Pravna osoba,Dobavljač,Servisni centar','','','');
INSERT INTO `contacts` VALUES (1329,'2015-12-23 09:21:36','2015-12-23 15:33:34','','','AUTO CENTAR ŠIBENIK d.o.o.','','','','Put bioca 15b','','22000 ','Šibenik',52,'servis@ac-sibenik.hr','','+385 22 330 800','','','','Pravna osoba,Dobavljač,Servisni centar','Goran Perica','00385 91 3377 488','');
INSERT INTO `contacts` VALUES (1330,'2015-12-23 09:27:58','2016-03-09 10:30:17','Amir','','','','','','','','','',52,'','00385 5639659','','','','Trenutno radi samo za gotovinu bez računa.','Fizička osoba,Dobavljač,Servisni centar','Amir','00385 91 5639659','servis,auto električar,amir');
INSERT INTO `contacts` VALUES (1331,'2015-12-23 09:32:41','2016-03-09 10:34:50','','','AUTOKUĆA BAOTIĆ d.o.o.','','86807475866','','Maksimirska cesta 282','','10040','Zagreb',52,'josko.stefanovic@baotic.hr','','+385 1 29 00 013','','','\r\nLimarija:\r\nRobert vrhovec 01 290 0079\r\n\r\nProcjena kasko:\r\nTihomir (nemamo broj)\r\n','Pravna osoba,Dobavljač,Servisni centar','Joško Štefanović','00385 91 29 00 023','auto servis,auto električar,procjena kasko,vulkanizer,limarija');
INSERT INTO `contacts` VALUES (1332,'2015-12-23 11:58:47','2015-12-23 11:58:47','','','UNIQA osiguranje d.d.','','','','Planinska 13A','','10000','Zagreb',52,'Vesna.KRAUTH@uniqa.hr','','00 3851 6323 612','','','- osigurnje\r\n- polica osigurnja može na rate','Pravna osoba,Dobavljač','Vesna Krauth','00385 95 8996 661','');
INSERT INTO `contacts` VALUES (1333,'2015-12-23 12:36:49','2015-12-23 12:36:49','Josip','Taši','','','','','','','','Zagreb',52,'centar@centarsvijesti.com','','','','','- bio 2013 sve uredu','Fizička osoba','Josip Taši','00385 95 7771009','');
INSERT INTO `contacts` VALUES (1334,'2015-12-23 14:52:43','2015-12-23 14:52:43','Miljenko ','Crnić','','','','','','','','',52,'miljenko.crnic@gmail.com','','','','','','Fizička osoba','Miljenko Crnić','00385 91 502 1480','');
INSERT INTO `contacts` VALUES (1336,'2015-12-28 08:47:42','2015-12-28 08:52:13','','','BLUE SUN ALIAS j.d.o.o.','','73736154486','','Franje 38','','10252',' Kupinečki Kraljevec ',52,'marko.gregoras@gmail.com','','00385 91 4623801','','','najam kombija, partner iznajmljivač, rent a car\r\n- nije u sustavu Pdv-a, tražiti račun na FLEXI RENT','Pravna osoba,Dobavljač','Marko Gregoraš','00385 91 4623801','');
INSERT INTO `contacts` VALUES (1337,'2015-12-28 08:50:17','2015-12-28 08:51:51','','','LOVIGO PROMET d.o.o.','','59274150602','','Blage Zadre 7A','','10360','Sesvete',52,'lovigopromet@gmail.com','','00385 95 553 47 53','','','najam kombija, partner iznajmljivač, rent a car\r\n- nije u sustavu Pdv-a, tražiti račun na FLEXI RENT','Pravna osoba,Dobavljač','Goran Ponjević','00385 95 553 47 53','');
INSERT INTO `contacts` VALUES (1338,'2015-12-28 11:51:45','2015-12-28 11:51:45','','','TecomTrade d.o.o.','','10572256030','','Hrvatskog proljeća 20A','','10040','Zagreb',52,'bhadzic@tecomtrade.hr','','00385 98 703307','','','','Pravna osoba','Boris Hađić','00385 98 703307','');
INSERT INTO `contacts` VALUES (1339,'2015-12-29 11:42:53','2015-12-29 11:42:53','','','SELECTIO KADROVI d.o.o.','','18518491403','','Strojarska cesta 20','','10000','Zagreb',52,'kontakt@selectio.hr','','01 6065 255','01 6065 256','','','Pravna osoba','','','');
INSERT INTO `contacts` VALUES (1340,'2015-12-29 12:55:55','2016-01-07 11:55:58','','','Umjetnička organizacija Radio Kanza','','16931758580','','Sitnice 9','','10000','Zagreb',52,'ttupajic@gmail.com','','','','','Preko ZKM-a klijentica.','Pravna osoba','Tea Tupajić','00385 131 3655','');
INSERT INTO `contacts` VALUES (1341,'2016-01-01 22:30:43','2016-01-01 22:30:43','','','VATROMEHANIKA-DUBRAVA d.o.o.','','','','Dubrava 64','','10040','Zagreb',52,'vatromehanikadubrava@zg.t-com.hr  ','00385098410453  ','','','','Protupožarni aparat, pp aparat, vatrogasni aparati','Pravna osoba,Dobavljač,Servisni centar','','00385098410453','');
INSERT INTO `contacts` VALUES (1342,'2016-01-04 13:23:58','2016-01-04 13:23:58','','','INSIDO d.o.o.','','19045534881','','Mate Vizmara 47','','44318 ','Voloder',52,'insido1@gmail.com','','','','','','Pravna osoba','Mario','0916504567','');
INSERT INTO `contacts` VALUES (1343,'2016-01-04 16:16:55','2016-01-04 16:16:55','','','Artisan d.o.o.','','88030194076','','Radnička cesta 1A','','10000','Zagreb',52,'silva@artisan.ba','00385 99 8442232','','','','','Pravna osoba','Silva','00385 99 8442232','');
INSERT INTO `contacts` VALUES (1344,'2016-01-06 18:20:26','2016-01-06 18:20:26','','','Danijel Čavić','','','','','','','',52,'danijelcavic8@gmail.com  ','','','','','','Fizička osoba','Danijel Čavić','+385923023030','');
INSERT INTO `contacts` VALUES (1345,'2016-01-07 17:12:40','2016-01-07 17:12:40','','','HEXO d.o.o.','','36366086105','','Joze Laurenčića 8','','10000','Zagreb',52,'roman.murr@yahoo.com','','+385 1 4852050','','','','Pravna osoba',' Roman Murr','','');
INSERT INTO `contacts` VALUES (1346,'2016-01-08 12:26:21','2016-01-22 10:00:51','','','MARIJO METZNER','','','','','','','',52,'danijel.vuckovic@hep.hr','00385 99 3128755','','','','- bio preko ROKERI MOTORI j.d.o.o.','Fizička osoba','Marijo','00385 99 549699','');
INSERT INTO `contacts` VALUES (1347,'2016-01-08 15:47:29','2016-02-19 11:53:12','','','PRO-TRADE d.o.o.','','20429317317','','Frankopanska 22','','10000','Zagreb',52,'maslac@protrade.hr','00385 99 3962 881','','','','HAMA','Pravna osoba','Gordan Maslać','00385 99 3962 881','');
INSERT INTO `contacts` VALUES (1348,'2016-01-08 18:57:00','2016-01-08 18:57:00','','','Jakov Ivković','','','','','','','',52,'ivkovic.jakov@gmail.com','','','','','','Fizička osoba','Jakov Ivković','00385 95 8550170','');
INSERT INTO `contacts` VALUES (1349,'2016-01-12 12:43:47','2016-01-12 12:43:47','','','TRINIUM MEDIA d.o.o.','','83445447856','','Radićeva 38','','10000','Zagreb',52,'tihomir.jauk@gmail.com','','','','','aplikacija, web, programiranje, program','Pravna osoba,Dobavljač','Tihomir Jauk','+385 95 9119947','');
INSERT INTO `contacts` VALUES (1350,'2016-01-13 09:06:15','2016-01-13 09:06:15','','','VRTNI CENTAR KLJAJIĆ vl.Jela Kljajić','','47325784669','','III Poljanice 5','','10040','Zagreb',52,'','','','','','','Pravna osoba','Josip Kljajić','00385 95 2922686','');
INSERT INTO `contacts` VALUES (1351,'2016-01-14 07:20:34','2016-01-14 07:20:34','','','Renato Meštrović','','','','','','','',52,'renato.mestrovic@gmail.com','','','','','','Fizička osoba','Renato Meštrović','','');
INSERT INTO `contacts` VALUES (1352,'2016-01-14 14:57:10','2016-01-14 14:57:10','','','Miroslav Navratil','','','','','','','',52,'','','','','','','Fizička osoba','Miroslav Navratil','00385 99 1904997','');
INSERT INTO `contacts` VALUES (1353,'2016-01-18 12:40:15','2016-01-18 12:40:15','','','I.B.JAZBINA d.o.o.','','77265033209','','Jazbina 151','','10000','zAGREB',52,'','','','','','','Pravna osoba','Branko Radoević','00385 91 2341201','');
INSERT INTO `contacts` VALUES (1354,'2016-01-20 11:04:57','2016-01-20 11:04:57','','','VINA ROTA d.o.o.','','14494127250','','Kuna 12','','20243','Kuna Peljaška',52,'vinarota2013@gmail.com','','','','','','Pravna osoba','','','');
INSERT INTO `contacts` VALUES (1355,'2016-01-21 13:00:47','2016-01-21 13:00:47','','','ClusterTech Internationa d.o.o.','','78877542963','','Jakova Gotovca 1','','10000','Zagreb',52,'b.subotic@cti.hr','','','','','','Pravna osoba','Boriša Subotić','00385 95 1711 099','');
INSERT INTO `contacts` VALUES (1356,'2016-01-22 08:57:34','2016-01-22 08:57:34','','','MOZAIK PARKET-HOKMAN','','44685299453','','Presečno 34 D','','42220','Presečno',52,'ilija.hokman971@gmail.com','','','','','','Pravna osoba','Ilija Hokman','00385 98 9710787','');
INSERT INTO `contacts` VALUES (1357,'2016-01-25 10:36:58','2016-01-25 10:36:58','','','APPLICON D.O.O','','76817252728','','Prisavlje 2','','10000','Zagreb',52,'anteb@pandopad.com','','','','','','Pravna osoba','Ante','00385 99 7332033','');
INSERT INTO `contacts` VALUES (1358,'2016-01-25 10:50:07','2016-01-29 08:18:34','','','Turistička zajednica Grada Samobora','','87714603967','','Trg kralja Tomislava 5','','10430','Samobor',52,'antica.bracanov@gmail.com','','','','','','Pravna osoba','Antica  Bračanov','00385 99 2531835','');
INSERT INTO `contacts` VALUES (1359,'2016-01-25 13:37:13','2016-01-25 13:37:13','','','JASIKA d.o.o.','','62815184072','','Remetinečka cesta 115','','10000','Zagreb',52,'vesna.bogdanic@jasika.hr','','','','','','Pravna osoba','Vesna Bogdanić','00385 91 2254 472','');
INSERT INTO `contacts` VALUES (1360,'2016-01-26 11:53:33','2016-01-26 11:53:33','','','Marko Ćatić','','','','','','','',52,'catic.marko@gmail.com','','','','','','Fizička osoba','Marko Ćatić','00385 92 3414971','');
INSERT INTO `contacts` VALUES (1361,'2016-01-26 12:51:05','2016-01-26 12:51:05','','','Mladen Labaš','','','','Stjepana Vojnovića 30','','10000 ','Zagreb',52,'','','','','','','Fizička osoba','Mladen Labaš','00385 98 1837021','');
INSERT INTO `contacts` VALUES (1362,'2016-01-26 14:44:19','2016-01-26 14:44:19','','','Lukša DOminiković','','','','','','10000','Zagreb',52,'','','','','','','Fizička osoba','Lukša Dominiković','00385 924 9327','');
INSERT INTO `contacts` VALUES (1363,'2016-01-27 09:34:06','2016-01-27 09:41:51','','','HRVATSKA UDRUGA VOZAČA','','66378400616','','Obrtnička ulica 5','','10430','Samobor',52,'','','','','','- veza BIJUK HPC','Pravna osoba','Tomislav Šoštarić ','00385 99 2122922','');
INSERT INTO `contacts` VALUES (1364,'2016-01-29 12:28:19','2016-01-29 12:28:19','','','Mobi Optika d.o.o.','','58769076755','','Črešnjevec 4c','','10000','Zagreb',52,'andrea@mobioptika.hr','','','','','','Pravna osoba','Andrea Bosnar','00385 95 20 20 552','');
INSERT INTO `contacts` VALUES (1365,'2016-02-02 13:51:01','2016-02-02 13:51:43','OSIGURANJE d.o.o.','CROATIA','','','','','','','','',52,'','','','','','','Pravna osoba','','','');
INSERT INTO `contacts` VALUES (1366,'2016-02-06 14:48:57','2016-02-06 14:48:57','','','IVAN BERTOVIĆ','','','','','','','',52,'bertovic.ivan@gmail.com','','','','','','Fizička osoba','Ivan Bertović','00385 91 4564210','');
INSERT INTO `contacts` VALUES (1367,'2016-02-09 11:43:16','2016-02-11 08:20:17','','','SUSTAV JAVNIH BICIKALA d.o.o.','','97795935846','','Maceljska 4','','10000','Zagreb',52,'kresimir.dvorski@nextbike.hr','','','','','','Pravna osoba','Krešimir Dvorski','00385 91 6300836','');
INSERT INTO `contacts` VALUES (1368,'2016-02-09 13:22:47','2016-02-09 13:22:47','','','PRO d.o.o.','','79967404744','','II Surepci 1','','10000','Zagreb',52,'bakir@pro-laser.hr','','','','','','Pravna osoba','Martin Broz','00385 98 320414','');
INSERT INTO `contacts` VALUES (1369,'2016-02-09 13:25:52','2016-02-09 13:25:52','','','PRO d.o.o.','','','','II Surepci 1','','10000','Zagreb',52,'bakir@pro-laser.hr','','','','','','Pravna osoba','Martin Broz','00385 98 320414','');
INSERT INTO `contacts` VALUES (1370,'2016-02-10 13:56:46','2016-02-10 13:56:46','','','Željko Riha','','','','','','','',52,'zeljko.riha@gmail.com','','','','','','Fizička osoba','Željko Riha','0038599 44 41 122','');
INSERT INTO `contacts` VALUES (1371,'2016-02-12 09:03:27','2016-02-12 09:13:24','','',' Moding junior d.o.o.','','25867440119','','Valica 7','','52100','Pula',52,'zagreb@moding-junior.hr','',' +385 52 500 590','','','','Pravna osoba','Moding','00385 91 444 0777','');
INSERT INTO `contacts` VALUES (1372,'2016-02-15 08:33:35','2016-02-15 08:33:35','','','Davor Lončarić','','','','','','','',52,'','','','','','','Fizička osoba','Davor Lončarić','00385 98 755892','');
INSERT INTO `contacts` VALUES (1373,'2016-02-15 10:50:25','2016-02-15 10:50:25','','','COFFEE TEAM d.o.o.','','26149575711','','Turčićeva 12','','10040','Zagreb',52,'mario@coffe-team.hr','','','','','','Pravna osoba','Tomislav','00385 95 1994300','');
INSERT INTO `contacts` VALUES (1374,'2016-02-19 12:02:36','2016-03-01 12:27:45','','','Klapa Dišpet','','92014037503','','S. Batušića 19','','10000','Zagreb',52,'lovorka.srsen@gmail.com','','','','','','Pravna osoba','Lovorka Sršen','00385 91 789 3787','');
INSERT INTO `contacts` VALUES (1375,'2016-02-22 09:20:09','2016-02-22 09:20:09','','','Igor Lekić','','','','','','','',52,'tilekic@gmail.com','','','','','','Fizička osoba','Igor Lekić',' 00385 98 9627127','');
INSERT INTO `contacts` VALUES (1376,'2016-02-22 11:40:18','2016-02-22 11:40:18','Tihomir','Špruk','','','','','','','','',52,'','','','','','','Fizička osoba','Tihomir Špruk','098474779','');
INSERT INTO `contacts` VALUES (1377,'2016-02-23 09:22:11','2016-02-23 09:22:11','','','MAGIC-MONT d.o.o','','67483547256','','Bana Jelačića 14','','43280','Garešnica-RH',52,'zarko@magic-mont.hr','','','','','','Pravna osoba','Žarko Međugorac','00385 98 319 495','');
INSERT INTO `contacts` VALUES (1378,'2016-02-25 13:08:54','2016-02-25 13:08:54','','','Zrinka Veža','','','','Josipa Pupačića 1','','10000','Zagreb',52,'zrinka.veza@gmail.com','','','','','','Fizička osoba','Zrinka Veža','00385 97 7679334','');
INSERT INTO `contacts` VALUES (1379,'2016-02-26 14:52:07','2016-02-26 14:52:07','','','LAURA MONTAŽA d.o.o.','','29851768971','','Dr. Franje Tuđmana 39/A','','10430','Sveta Nedelja',52,'daniel@lauramontaza.hr','','','','','','Pravna osoba','Daniel','00385 95 1969480','');
INSERT INTO `contacts` VALUES (1380,'2016-02-26 20:07:21','2016-02-26 20:07:21','','','Udruga inovatora Hrvatske','','69872404259','','Dalmatinska 12','','10000','Zagreb',52,'damir.gornik@inovatorstvo.com','','','','','','Pravna osoba','Damir Gornik','00385 91 15 17 592.','');
INSERT INTO `contacts` VALUES (1381,'2016-02-27 13:39:32','2016-02-27 13:39:32','','','Manuel Štiglić','','40593356527','','Krasica 302 B','','51224','Krasica',52,'','','','','','','Fizička osoba','Manuel Štigić','00385 99 2345608','');
INSERT INTO `contacts` VALUES (1382,'2016-02-29 11:23:14','2016-02-29 11:23:14','','','Donald Debeljak','','73520907093','','Vida Noršića Čekulja 20','','10291','Zdenci Brdovečki',52,'','','','','','','Fizička osoba','Dobald Debeljak ','00385 91 5253746','');
INSERT INTO `contacts` VALUES (1383,'2016-03-01 12:46:34','2016-03-11 10:31:14','','','Zdenko Belko','','00364559214','','Albrechova 22','','10000','Zagreb',52,'belko.zdenko@gmail.com','','','','','','Fizička osoba','Zdenko Belko','00385 239511','');
INSERT INTO `contacts` VALUES (1384,'2016-03-04 15:03:46','2016-03-04 15:03:46','','','Zoran Lalić','','68771610038','','Ul. kralja Tomislava 36a','','21260','Imotski',52,'','','','','','','Fizička osoba','','00385 95 354 0607','');
INSERT INTO `contacts` VALUES (1385,'2016-03-04 15:05:32','2016-03-04 15:05:32','','','Adrian Špoljar','','','','','','','',52,'adrian.spoljar@infodom.hr','','','','','','Fizička osoba','Adrian Špoljar','','');
INSERT INTO `contacts` VALUES (1386,'2016-03-08 08:06:53','2016-03-08 08:06:53','','','TOKIĆ d.o.o.','','74867487620','','AV. DUBRAVA 145A','','10049','Zagreb',52,'','','0038513033999','','','','Dobavljač,Servisni centar','','','');
INSERT INTO `contacts` VALUES (1387,'2016-03-08 09:55:52','2016-03-08 09:55:52','','','MARINES d.o.o.','','93707372357','','Radnicka cesta 32','','10000','Zagreb',52,'skladiste@marines.hr','+385 1 6055 625','','','','','Pravna osoba','Damir','00385 98  828818','');
INSERT INTO `contacts` VALUES (1388,'2016-03-08 17:04:19','2016-03-08 17:04:19','','','TEHNOKOM d.o.o.','','26987865935','','Radnička cesta 228','','10000','Zagreb',52,'mario.lekic@tehnokom.hr','','','','','','Pravna osoba','Mario Lekić','','');
INSERT INTO `contacts` VALUES (1389,'2016-03-08 17:19:42','2016-03-08 17:19:42','','','ŽIVOTINJSKI SVIJET d.o.o.','','15527740514','','Matice hrvatske 4','','21000','SPlit',52,'zoobatuda@gmail.com','','','','','','Pravna osoba','Perišić','00385 98 9272790','');
INSERT INTO `contacts` VALUES (1390,'2016-03-09 16:14:41','2016-03-09 16:14:41','','','Ana Galović','','','','','','','',52,'ana.galovic@gmail.com','','','','','','Fizička osoba','Ana Galović','00385 99 272 4771','');
INSERT INTO `contacts` VALUES (1391,'2016-03-10 09:36:11','2016-03-10 09:36:11','','','LEMA TRANS d.o.o.','','67149833423','',' Rusnica 63/2 ','','49231','Hum na Sutli',52,'b.zavrski@lema-trans.hr','','','','','','Pravna osoba','Barbara','00385 98 378447','');
INSERT INTO `contacts` VALUES (1392,'2016-03-10 14:38:58','2016-03-10 14:38:58','','','Nives Košarkašice','','','','','','','',52,'nives2512@gmail.com','','','','','','Fizička osoba','Nives','00385 95 527 9262','');
INSERT INTO `contacts` VALUES (1393,'2016-03-11 16:30:34','2016-03-11 16:30:34','','','REAL GRUPA d.o.o.','','57210097686','','Ljudevita Posavskog 31','','10000','Zagreb',52,'slavica@realgrupa.com','','','','','','Pravna osoba','Slavica Markulin','00385 91 4650 070','');
INSERT INTO `contacts` VALUES (1394,'2016-03-12 12:58:03','2016-03-12 12:58:03','','','HEMARK d.o.o.','','21126244830','','Podgorska 6','','10000','Zagreb',52,'drazen@hemark.hr','','','','','','Pravna osoba','Dražen Nikolić','00385 98 50 50 60','');
INSERT INTO `contacts` VALUES (1395,'2016-03-15 08:57:16','2016-03-15 08:57:16','','','ZAGREBAČKI PROMETNI ZAVOD','','68849995685','',' Ljubljanska avenija 1','','10000 ','Zagreb',52,'development@zpz.hr','','','','','','Pravna osoba','Davorko Barušić ','00385 91 5639526','');
INSERT INTO `contacts` VALUES (1396,'2016-03-15 15:14:35','2016-03-15 15:14:35','','','Zoran Hanžir','','','','','','','',52,'zhanzir@gmail.com','','','','','veza Denis STrojopromet','Fizička osoba','Zoran Hanžir','','');
INSERT INTO `contacts` VALUES (1397,'2016-03-16 14:57:59','2016-03-16 15:01:57','','','Atletski klub “CRO-RUN”','','57104276518','','Maksimirska cesta 112 A','','10000','Zagreb',52,'sinisa.ergotic@btravel.pro','','','','','','Pravna osoba','Siniša Ergotić','00385 99 492 6685','');
INSERT INTO `contacts` VALUES (1398,'2016-03-18 14:39:41','2016-03-18 14:39:41','','','Damir Bobinac','','','','','','','',52,'damirbobinac@gmail.com','','','','','','Fizička osoba','Damir Bobinac','00385 98 9525215','');
INSERT INTO `contacts` VALUES (1399,'2016-03-21 10:13:18','2016-03-21 10:13:18','Luka ','Čukac','','','5474547484','','Jurišića','','10040','Zagreb',52,'hhh@gki.lo','0038555373663','','','','','Fizička osoba','Luka Čukac','098383773','');
INSERT INTO `contacts` VALUES (1400,'2016-03-21 14:58:34','2016-03-21 14:58:34','','','SANITARIA DENTAL d.o.o. ','','30023990959','','Korčulanska 4-6 ','','10000','Zagreb',52,'dbaotic@sanitaria.hr','','','','','','Pravna osoba','Dražen Baotić','00385 99 258 48 69 ','');
INSERT INTO `contacts` VALUES (1401,'2016-03-23 11:29:00','2016-03-23 11:29:00','Marko','Kordiš','','','','','','','','',52,'markordis@gmail.com','0914000516','','','','','Fizička osoba','Marko Kordiš','0914000516','');
INSERT INTO `contacts` VALUES (1402,'2016-03-23 16:04:50','2016-03-23 16:04:50','','','PRACTICUM VITA d.o.o.','','97401494525','',' Ul. Fausta Vrančića 2','','10000','Zagreb',52,'practicumvita@gmail.com','00385 95 9112785','','','','','Pravna osoba,Dobavljač','Goran Sučević','00385 95 9112785','');
INSERT INTO `contacts` VALUES (1403,'2016-03-23 16:18:38','2016-03-23 16:18:38','','','Miljenko Sladović','','','','','','','',52,'msladovic@europe.com','','','','','','Fizička osoba','Miljenko Sladović','00385 98 237094','');
INSERT INTO `contacts` VALUES (1404,'2016-03-25 20:31:38','2016-03-25 20:31:38','Zlatko','Makar','','','105429157','','A.B.Jurišić 9','','10040','Zagreb',52,'','','','','','','Fizička osoba','','','');
INSERT INTO `contacts` VALUES (1405,'2016-03-29 14:03:00','2016-03-29 14:03:00','','','Alan Jelovečki','','','','','','','',52,'ajelovecki@gmail.com','','','','','','Fizička osoba','Alan Jelovečki','','');
INSERT INTO `contacts` VALUES (1406,'2016-03-29 20:02:37','2016-03-29 20:02:37','','','GWT d.o.o.','','05626170569','','Voćarska 48','','10000','Zagreb-RH',52,'gwt@gwt.hr','','','','','','Pravna osoba','Leon Tomislav Gregurić','00385 98 233 140','');
INSERT INTO `contacts` VALUES (1407,'2016-03-30 07:22:17','2016-03-30 07:22:17','','','STUDIO ARENA d.o.o.','','28869851978','','Zagrebačka 175','','10000','Zagreb',52,'','','','','','','Pravna osoba','','','');
INSERT INTO `contacts` VALUES (1408,'2016-03-30 12:01:44','2016-04-01 10:30:18','','','Centar Zdravlja d.o.o.','','29902184926','','Virovska 15','','10040','Zagreb',52,'centarzdravljahr@gmail.com','','','','','','Pravna osoba','Centar','0916038268','');
INSERT INTO `contacts` VALUES (1409,'2016-03-31 06:35:41','2016-03-31 06:35:41','','','Dean Konjević','','','','','','','',52,'dean.konjevic@vef.hr','','','','','','Fizička osoba','Dean Konjević','','');
INSERT INTO `contacts` VALUES (1410,'2016-03-31 12:08:48','2016-03-31 12:08:48','','','Pavle Vidaković','','','','','','','',52,'','','','','','','Fizička osoba','','','');
INSERT INTO `contacts` VALUES (1411,'2016-03-31 14:40:08','2016-04-01 08:34:39','','','RONDO MONDO d.o.o.','','50082788552','','Štefanovečka cesta 10','','10040','Zagreb',52,'dario@rondoshop.hr','','','','','','Pravna osoba','Dario Šehović','00385 98 9124469','');
INSERT INTO `contacts` VALUES (1412,'2016-04-01 16:06:15','2016-04-01 16:06:15','','','Domagoj Raić','','','','','','','',52,'domagojraic@gmail.com','','','','','','Fizička osoba','DOmagoj Raić','00385 91 5142030','');
INSERT INTO `contacts` VALUES (1413,'2016-04-01 20:05:40','2016-04-01 20:05:40','','','Žarko Anđelić','','','','','','','',52,'durmitorvisit@gmail.com','','','','','','Fizička osoba','Žarko Anđelić','+381641313490','');
INSERT INTO `contacts` VALUES (1414,'2016-04-04 13:06:16','2016-04-04 13:06:16','','','Dalibor Ević','','','','','','','',52,'daliborevic23@gmail.com','','','','','','Fizička osoba','Dalibor Ević','00385 91 603 9174','');
INSERT INTO `contacts` VALUES (1415,'2016-04-05 08:23:53','2016-04-05 08:23:53','','','ARTEC S.P. d.o.o.','','70439021788','','Pantovčak  7','','10000','Zagreb',52,'sinisa.prpic@zg.t-com.hr','','','','','','Pravna osoba','SIniša Prpić','00385 98 276 015','');
INSERT INTO `contacts` VALUES (1416,'2016-04-05 10:29:45','2016-04-06 08:42:54','','','Odsjek za etnologiju HAZU','','06872053793','','Andrije Hebranga 1','','10000 ','Zagreb',52,'etnologija@hazu.hr','','00385 1 4895 149','','','','Pravna osoba','Klementina Batina','00385 91 1649 694','');
INSERT INTO `contacts` VALUES (1417,'2016-04-05 10:38:17','2016-04-05 10:38:17','','','Nikola Ostojić','','','','','','','',52,'nostojic@outlook.com','','','','','','Fizička osoba','','','');
INSERT INTO `contacts` VALUES (1418,'2016-04-05 13:42:50','2016-04-05 13:42:50','','','KOMPAS CELJE d.d.','','','','Peričeva 23','','1000','Ljubljana',191,'polona.jelocnik@kompas-celje.si','','','','','','Pravna osoba','Polona Jeločnik','00386 1 280 3600','');
INSERT INTO `contacts` VALUES (1419,'2016-04-06 10:07:32','2016-04-06 10:07:32','','','KLARA GARDENING d.o.o.','','58189471296','','Josipa Jelačića 87','','10430','Samobor',52,'danko@klara-gardening.com','','','','','','Pravna osoba','Danko Crljen','+385 98 48 08 28','');
INSERT INTO `contacts` VALUES (1420,'2016-04-07 10:42:12','2016-04-07 10:50:14','','','GRAFOL ','','20874864877','','Slavenskog 1','','10000','Zagreb',52,'janko.kozina@voyager.hr','00385 91 5105848','','','','','Pravna osoba','Janko Kozina','00385 91 5105848','');
INSERT INTO `contacts` VALUES (1421,'2016-04-08 08:37:40','2016-04-08 08:37:40','','','Rudi Nahtigal','','','','','','','',52,'nahtigal.rudi@gmail.com','','','','','','Fizička osoba','Rudi Nahtigal','','');
INSERT INTO `contacts` VALUES (1422,'2016-04-09 08:36:14','2016-04-09 08:36:14','','','Antonio Obadić','','','','','','','',52,'','','','','','','Fizička osoba','','00385 99 6749666','');
INSERT INTO `contacts` VALUES (1423,'2016-04-09 09:07:13','2016-04-09 09:07:13','','','ŽUMBERAČKA FARMA d.o.o.','','29751866778','','Stankovo 1B','','10450','Gornji Desinec ',52,'zuberackafarma@gmail.com','','','','','','Pravna osoba',' VJEKOSLAV RAŠIĆ','00385 99 4417388','');
INSERT INTO `contacts` VALUES (1424,'2016-04-11 13:47:10','2016-04-11 13:47:10','','','VETERINARSKI FAKULTET','','36389528408','','Heinzlova 55','','10000','Zagreb',52,'mperharic@vef.hr','','','','','','Pravna osoba','Matrko Perharić','00385 91 539 0370','');
INSERT INTO `contacts` VALUES (1425,'2016-04-11 19:41:03','2016-04-11 19:41:03','','','Integrirani poslovni sustavi d.o.o.','','66238130370','',' III. Cvjetno naselje 18','','10000 ','Zagreb',52,'hrvoje@remaris.com','','','','','','Pravna osoba','Hrvoje Habjanec ','00385 95 1994 288','');
INSERT INTO `contacts` VALUES (1426,'2016-04-12 12:32:10','2016-04-12 12:45:43','','','Danijel Ivanović','','99230965884','','Novoselska 54A','','10000','Zagreb',52,'','','','','','','Fizička osoba','Danijel Ivanović','00385 95 8792073','');
INSERT INTO `contacts` VALUES (1427,'2016-04-12 14:14:52','2016-04-18 13:24:32','','','Odsjek za indologiju i dalekoistočne studije Filozofski fakultet','','','','Ivana Lučića 3','','10000','Zagreb',52,'kkrnic@ffzg.hr','','','','','','Pravna osoba','Krešimir Krnić','00385  98 476160','');
INSERT INTO `contacts` VALUES (1428,'2016-04-13 10:30:06','2016-04-13 13:26:42','','','NESTOR INŽENJERING d.o.o.','','25351911375','','Pokupska 32','','10410','Velika Gorica',52,'nestor.ing@hotmail.com','','','','','','Pravna osoba','Radivoj Petrak','099 6666 355','');
INSERT INTO `contacts` VALUES (1429,'2016-04-13 12:24:33','2016-04-13 12:27:16','','','P.S.F oprema d.o.o. ','','15535848079','','Spinčići 180','','51215 ','Kastav',52,'josip@marelja.hr','','','','','','Pravna osoba','Josip Marelja','00385 91 4201100','');
INSERT INTO `contacts` VALUES (1430,'2016-04-18 10:58:55','2016-04-18 10:58:55','','','FRANKOPROM j.d.o.o.','','11696288578','','Kneza Ljudevita Posavskog 102','',' 32254','Vrbanja',52,'frankoprom@gmail.com','','','','','','Pravna osoba','Ivica Doknjaš ','00385 91 186 4400','');
INSERT INTO `contacts` VALUES (1431,'2016-04-18 11:49:42','2016-04-19 13:48:12','','','Saša Sabo','','','','','','','',52,'ssabo@croz.net','','','','','','Fizička osoba','Saša Sabo','003885912263531','');
INSERT INTO `contacts` VALUES (1432,'2016-04-18 15:44:06','2016-04-18 15:44:06','','','Sveučilište u Zagrebu – Hrvatski studiji','','36612267447','','Borongajska cesta 83d','','10000','Zagreb',52,'uredvoditelja@hrstud.hr','','','','','','Pravna osoba','Lidija Zorić','+ 385 1 24 57 600','');
INSERT INTO `contacts` VALUES (1433,'2016-04-20 14:56:35','2016-04-23 06:37:06','','','ITI - INTERNET INSTITUT','','34343208149','','Matije Gupca 63/I','','49000 ','Krapina',52,'natalija.gojkovic@gmail.com','','','','','','Pravna osoba','Natalija Gojković','00385 91 3444 825','');
INSERT INTO `contacts` VALUES (1434,'2016-04-20 17:10:38','2016-04-20 17:10:38','','','Marko Grgić','','','','Putine 15A','','10090','Zagreb',52,'marko.grgic55@gmail.com','','','','','','Fizička osoba','Marko Grgić','00385 98 423616','');
INSERT INTO `contacts` VALUES (1435,'2016-04-20 19:35:23','2016-04-20 19:35:23','','','Hrvoje Gregov','','','','','','','',52,'hgregov@yahoo.com','','','','','','Fizička osoba','Hrvoje Gregov','00385 98 964 8340','');
INSERT INTO `contacts` VALUES (1436,'2016-04-21 11:01:04','2016-04-21 11:01:04','','','Jadranka Čižmak','','','','','','','',52,'kristian.jadranka@gmail.com','','','','','','Fizička osoba','Kristian  Čižmak','00385 98 965 7940','');
INSERT INTO `contacts` VALUES (1437,'2016-04-21 11:22:56','2016-04-21 11:22:56','','','FAE Group S.p.a.','','','','Zona produttiva, 18 ','','38013 Fondo ','Fondo (TN)',104,'goretic@minotehnika.hr','','','','','','Pravna osoba','Goran Goretić','+385-91-252-4985','');
INSERT INTO `contacts` VALUES (1438,'2016-04-21 11:55:26','2016-04-21 11:55:26','','','UDRUGA „GRAĐANSKA INICIJATIVA STARI PAPIR ZA NOVI OSMIJEH“','','04191489016','','Avenija Većeslava Holjevca 62','','10000','Zagreb',52,'staripapirzanoviosmijeh@gmail.com','','','','','','Pravna osoba','Tomislav Pavlović','','');
INSERT INTO `contacts` VALUES (1439,'2016-04-21 13:00:05','2016-04-21 13:00:05','','','Ivana Ozana Prah','','','','','','','',52,'','','','','','','Fizička osoba','Ivana Ozana Prah','+385917540062','');
INSERT INTO `contacts` VALUES (1441,'2016-04-26 10:16:44','2016-04-26 10:16:44','','','Global Connect ','','09499228291','','Srebrnjak 126','','10000 ','Zagreb',52,'info@globalconnect.hr','','','','','','Pravna osoba',' Deni Pavić','','');
INSERT INTO `contacts` VALUES (1442,'2016-04-26 13:40:28','2016-04-26 13:40:28','','','CENTAR ZA PROFESIONALNU REHABILITACIJU \"ZAGREB\"','','69410598395','','Ilica 29','','10000','Zagreb',52,'info@cprz.hr','','','','','','Pravna osoba','Tomo Knežević','00385 91 6202241','');
INSERT INTO `contacts` VALUES (1443,'2016-04-26 13:52:07','2016-04-26 13:52:07','','','Hrvoje Tretinjak','','','','','','','',52,'hrvoje.tretinjak@gmail.com','','','','','','Fizička osoba','','','');
INSERT INTO `contacts` VALUES (1444,'2016-04-26 14:15:46','2016-04-28 13:27:24','','','HUANG WEIDONG','','46484662616','','Ulica Antuna Šoljana 26','','10000','Zagreb',52,'','','','','','','Fizička osoba','HUANG WEIDONG','00385 98 283 505','');
INSERT INTO `contacts` VALUES (1445,'2016-04-26 14:19:42','2016-04-26 14:19:42','','','Dejan Hribar','','','','Grančarska ulica Vodvojak 7 A','','10257','Brezovica',52,'','','','','','','Fizička osoba','Dejan Hribar','00385 99 4240323','');
INSERT INTO `contacts` VALUES (1446,'2016-04-27 09:31:07','2016-04-27 09:31:07','','','SIMULTANA d.o.o.','','18633413281','','Horvaćanska 160','','10000','Zagreb',52,'simultana@simultana.com','','','','','','Pravna osoba','Željko Pokasić','','');
INSERT INTO `contacts` VALUES (1447,'2016-04-27 14:50:53','2016-04-27 14:50:53','','','Domagoj Kralj','','','','Baranjska 3','','10410','Velika Gorica',52,'domagoj.kralj.hr@gmail.com','','','','','','Fizička osoba','Domagoj Kralj','00385 99 6042558','');
INSERT INTO `contacts` VALUES (1448,'2016-04-29 08:35:13','2016-04-29 08:35:13','','','Ekonomsko propagandni program d.o.o.','','92099286194','','Svete Ane 9','','10430','Samobor',52,'miroslav.jurkovic@epepe.hr','','','','','','Pravna osoba','Miroslav Jurković','00385 99 3325 850','');
INSERT INTO `contacts` VALUES (1449,'2016-04-29 09:19:33','2016-04-29 09:46:49','','','EKO ELEKTRO VOD d.o.o.','','35955468622','','P. Štoosa 10 A','','10360 Sesvete','Sesvete',52,'ekoelektrovod@gmail.com','','','','','','Pravna osoba','Ivo Andrić','00385 98 389 797','');
INSERT INTO `contacts` VALUES (1450,'2016-05-02 09:58:16','2016-05-02 09:58:16','','','Stjepan Vranek','','04388885880','','IV Luka 13','','10040','Zagreb',52,'','','','','','','Fizička osoba','Stjepan Vranek','00385 98 1625061','');
INSERT INTO `contacts` VALUES (1451,'2016-05-03 12:56:00','2016-05-03 12:56:00','','','Tehnika d.d.','','73037001250','','Ulica grada Vukovara 274','','10000','Zagreb',52,'','','','','','','Pravna osoba','','+385 (0)1 6301 111 ','');
INSERT INTO `contacts` VALUES (1452,'2016-05-04 14:04:07','2016-05-04 14:04:07','','','HRT ŠARIĆ','','76454212077','','Zagrebačka 217','','10370','Dugo Selo',52,'info@hrt-saric.hr','','','','','','Dobavljač,Servisni centar','Centrala','00385012759511','');
INSERT INTO `contacts` VALUES (1453,'2016-05-04 19:48:26','2016-05-04 19:48:26','','','Branka Dörr-Županić','','','','','','','',52,'brandozup@gmail.com','','','','','','Fizička osoba','Branka Dörr-Županić','','');
INSERT INTO `contacts` VALUES (1454,'2016-05-04 20:13:51','2016-05-04 20:13:51','','','AGL d.o.o.','','96612633807','','Anina ulica 7','','32100 ','Vinkovci',52,'markopongrac0@gmail.com','','','','','','Pravna osoba','Marko Pongrac','00385 91 2007993','');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table contract_templates
#

DROP TABLE IF EXISTS `contract_templates`;
CREATE TABLE `contract_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table contract_templates
#
LOCK TABLES `contract_templates` WRITE;
/*!40000 ALTER TABLE `contract_templates` DISABLE KEYS */;

/*!40000 ALTER TABLE `contract_templates` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table contracts
#

DROP TABLE IF EXISTS `contracts`;
CREATE TABLE `contracts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `pdf` text COLLATE utf8_unicode_ci NOT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `dated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table contracts
#
LOCK TABLES `contracts` WRITE;
/*!40000 ALTER TABLE `contracts` DISABLE KEYS */;

INSERT INTO `contracts` VALUES (9,'2016-02-25 12:15:20','2016-02-25 12:50:23','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"KATAPULT d.o.o\",\"adresa\":\" Ulica Gordana Lederera 4 \",\"oib\":\"48566967897\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"datum_preuzimanja\":\"25.02.2016\",\"hour\":\"13\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"13:00\",\"datum_povratka\":\"27.02.2016\",\"vrijeme_povratka\":\"13:00\",\"cijena_po_danu\":\"430\",\"ukupno_dana\":\"2\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"Filip Martinovi\\u0107\",\"vozac_adresa\":\"Sel\\u010dinska 15\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"234450\",\"vozac_telefon\":\"0913476911\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',0,5,1067,'2016-02-25');
INSERT INTO `contracts` VALUES (11,'2016-02-26 07:24:55','2016-02-26 15:19:20','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Igor Leki\\u0107\",\"adresa\":\"Ulica Slavoljuba Bulvana 12\",\"oib\":\"86267398779\",\"registracija_vozila\":\"Partner - putni\\u010dki\",\"datum_preuzimanja\":\"26.02.2016\",\"hour\":\"09\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"07:00\",\"datum_povratka\":\"29.02.2016\",\"vrijeme_povratka\":\"09:00\",\"cijena_po_danu\":\"530\",\"ukupno_dana\":\"4\",\"dnevna_kilometraza\":\"400\",\"popust\":\"10\",\"vozac\":\"Igor Leki\\u0107\",\"vozac_adresa\":\"Ulica Slavoljuba Bulivana 12\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"112273503\",\"vozac_telefon\":\"00385 98 962 7127\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"Nema\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',0,16,1375,'2016-02-26');
INSERT INTO `contracts` VALUES (12,'2016-02-26 15:13:45','2016-02-26 16:00:31','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"LAURA MONTA\\u017dA d.o.o.\",\"adresa\":\"Dr. Franje Tu\\u0111mana 39\\/A\",\"oib\":\"29851768971\",\"registracija_vozila\":\"Fiat Ducato RI-654-UP\",\"datum_preuzimanja\":\"26.02.2016\",\"hour\":\"16\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"16:00\",\"datum_povratka\":\"07.03.2016\",\"vrijeme_povratka\":\"16:00\",\"cijena_po_danu\":\"385\",\"ukupno_dana\":\"10\",\"dnevna_kilometraza\":\"250\",\"popust\":\"12\",\"vozac\":\"Goran Jur\\u010devi\\u0107\",\"vozac_adresa\":\"\\u017deljezni\\u010dka 12\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0482301\",\"vozac_telefon\":\"00385 95 196 9480\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"Nema\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',0,4,1379,'2016-02-26');
INSERT INTO `contracts` VALUES (13,'2016-02-27 13:44:45','2016-02-27 13:44:45','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Manuel \\u0160tigli\\u0107\",\"adresa\":\"Krasica 302 B\",\"oib\":\"40593356527\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"datum_preuzimanja\":\"27.02.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"13:00\",\"datum_povratka\":\"28.02.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"430\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"Manuel \\u0160tigli\\u0107\",\"vozac_adresa\":\"Krasica 302B\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"1047355254\",\"vozac_telefon\":\"00385 99 2345608\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"Nema\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',0,5,1381,'2016-02-27');
INSERT INTO `contracts` VALUES (14,'2016-02-29 08:44:15','2016-02-29 09:45:19','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Zrinka Ve\\u017ea\",\"adresa\":\"Josipa Pupa\\u010di\\u0107a 1\",\"oib\":\"07181783249\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"datum_preuzimanja\":\"29.02.2016\",\"hour\":\"13\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"09:00\",\"datum_povratka\":\"29.02.2016\",\"vrijeme_povratka\":\"13:00\",\"cijena_po_danu\":\"297\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"100\",\"popust\":\"0\",\"vozac\":\"HEVOJE \\u0160ALA\",\"vozac_adresa\":\"KA\\u010cI\\u0106EVA 6\",\"vozac_zemlja\":\"HRVATSKA\",\"vozac_broj_vozacke\":\"105775965\",\"vozac_telefon\":\"0915057352\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"Nema.\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',0,5,1378,'2016-02-29');
INSERT INTO `contracts` VALUES (15,'2016-02-29 11:42:58','2016-02-29 13:35:11','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Donald Debeljak\",\"adresa\":\"Vida Nor\\u0161i\\u0107a \\u010cekulja 20\",\"oib\":\"73520907093\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"datum_preuzimanja\":\"29.02.2016\",\"hour\":\"13\",\"minute\":\"45\",\"vrijeme_preuzimanja\":\"13:45\",\"datum_povratka\":\"01.03.2016\",\"vrijeme_povratka\":\"13:45\",\"cijena_po_danu\":\"500\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"500\",\"popust\":\"0\",\"vozac\":\"Donald Debeljak\",\"vozac_adresa\":\"Vida Nor\\u0161i\\u0107a \\u010cekulja 20\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"10238363\",\"vozac_telefon\":\"00385 91 525 3746\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"Nema\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',0,5,1382,'2016-02-29');
INSERT INTO `contracts` VALUES (16,'2016-03-02 07:28:26','2016-03-02 08:11:22','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"ClusterTech Internationa d.o.o.\",\"adresa\":\"Jakova Gotovca 1\",\"oib\":\"78877542963\",\"registracija_vozila\":\"Renault Master RI-629-UP\",\"datum_preuzimanja\":\"02.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"04.03.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"2\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"Bori\\u0161a Suboti\\u0107\",\"vozac_adresa\":\"Po\\u017ee\\u0161ka 6\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"229654\",\"vozac_telefon\":\"00385 95 17111099\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"NEMA\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',0,3,1355,'2016-03-02');
INSERT INTO `contracts` VALUES (17,'2016-03-02 12:07:58','2016-03-02 12:07:58','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"MAGIC-MONT d.o.o\",\"adresa\":\"Bana Jela\\u010di\\u0107a 14\",\"oib\":\"67483547256\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"datum_preuzimanja\":\"02.03.2016\",\"hour\":\"18\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"18:00\",\"datum_povratka\":\"06.03.2016\",\"vrijeme_povratka\":\"18:00\",\"cijena_po_danu\":\"550\",\"ukupno_dana\":\"4\",\"dnevna_kilometraza\":\"\",\"popust\":\"17\",\"vozac\":\"Darko Me\\u0111ugorac\",\"vozac_adresa\":\"Bana Jela\\u010di\\u0107a 14\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0203519\",\"vozac_telefon\":\"0038598 319 495\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',0,5,1377,'2016-03-02');
INSERT INTO `contracts` VALUES (18,'2016-03-03 09:40:50','2016-03-03 09:40:50','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"COFFEE TEAM d.o.o.\",\"adresa\":\"Tur\\u010di\\u0107eva 12\",\"oib\":\"26149575711\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"datum_preuzimanja\":\"03.03.2016\",\"hour\":\"18\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"18:00\",\"datum_povratka\":\"04.03.2016\",\"vrijeme_povratka\":\"18:00\",\"cijena_po_danu\":\"600\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"500\",\"popust\":\"16\",\"vozac\":\" Mario \\u017dug\\u010di\\u0107\",\"vozac_adresa\":\"Tur\\u010di\\u0107eva 12\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"10139565\",\"vozac_telefon\":\"00385 95 1994300\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"NEMA\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',0,6,1373,'2016-03-03');
INSERT INTO `contracts` VALUES (19,'2016-03-04 17:52:48','2016-03-04 17:52:48','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Zoran Lali\\u0107\",\"adresa\":\"Ul. kralja Tomislava 36a\",\"oib\":\"68771610038\",\"registracija_vozila\":\"Renault Master RI-629-UP\",\"datum_preuzimanja\":\"05.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"06.03.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"500\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"500\",\"popust\":\"0\",\"vozac\":\"Zoran Lali\\u0107\",\"vozac_adresa\":\"Ul. kralja Tomislava 36a\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"346353\",\"vozac_telefon\":\"00385 95 3540607\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"Kombi preuzet 4.3. u 18 00\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',0,3,1384,'2016-03-04');
INSERT INTO `contracts` VALUES (20,'2016-03-05 07:46:44','2016-03-05 08:03:16','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Adrian \\u0160poljar\",\"adresa\":\"NASELJE \\u010cA\\u0160LJA IV 9\",\"oib\":\"10047340906\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"datum_preuzimanja\":\"05.03.2016\",\"hour\":\"14\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"05.03.2016\",\"vrijeme_povratka\":\"14:00\",\"cijena_po_danu\":\"648\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"60\",\"popust\":\"50\",\"vozac\":\"DRA\\u017dENKA JUKI\\u0106\",\"vozac_adresa\":\"MATE IVANI\\u0160EVI\\u0106A 3\",\"vozac_zemlja\":\"Hrvstaka\",\"vozac_broj_vozacke\":\"10273963\",\"vozac_telefon\":\"00385 99 2517157\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"SATNI NAJAM\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',0,6,1385,'2016-03-05');
INSERT INTO `contracts` VALUES (21,'2016-03-07 12:15:30','2016-03-07 12:16:35','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"LAURA MONTA\\u017dA d.o.o.\",\"adresa\":\"Dr. Franje Tu\\u0111mana 39\\/A\",\"oib\":\"29851768971\",\"registracija_vozila\":\"Fiat Ducato RI-654-UP\",\"datum_preuzimanja\":\"07.03.2016\",\"hour\":\"16\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"16:00\",\"datum_povratka\":\"10.03.2016\",\"vrijeme_povratka\":\"16:00\",\"cijena_po_danu\":\"385\",\"ukupno_dana\":\"3\",\"dnevna_kilometraza\":\"250\",\"popust\":\"12\",\"vozac\":\"Goran Jur\\u010devi\\u0107\",\"vozac_adresa\":\"\\u017deljezni\\u010dka 12\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"0482301\",\"vozac_telefon\":\"00385 95 196 9480\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"PRODU\\u017dETAK NAJMA\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',0,4,1379,'2016-03-07');
INSERT INTO `contracts` VALUES (22,'2016-03-08 10:05:32','2016-03-08 10:05:32','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"MARINES d.o.o.\",\"adresa\":\"Radnicka cesta 32\",\"oib\":\"93707372357\",\"registracija_vozila\":\"Renault Master RI-629-UP\",\"datum_preuzimanja\":\"08.03.2016\",\"hour\":\"10\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"10:00\",\"datum_povratka\":\"09.03.2016\",\"vrijeme_povratka\":\"10:00\",\"cijena_po_danu\":\"430\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"DAMIR PAVLOVI\\u0106\",\"vozac_adresa\":\"II Kanalski put 6 odvojak br.10\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0532354\",\"vozac_telefon\":\"00385 98 828818\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"KOMPENZACIJA PO RA\\u0106UNU MARINES  2015_16-300-000086\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',0,3,1387,'2016-03-08');
INSERT INTO `contracts` VALUES (23,'2016-03-10 06:30:49','2016-03-10 08:07:39','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"\\u017dIVOTINJSKI SVIJET d.o.o.\",\"adresa\":\"Matice hrvatske 4\",\"oib\":\"15527740514\",\"registracija_vozila\":\"Renault Master RI-629-UP\",\"datum_preuzimanja\":\"10.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"14.03.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"430\",\"ukupno_dana\":\"4\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"DORIS PERI\\u0160\",\"vozac_adresa\":\"VIDILICA 1\",\"vozac_zemlja\":\"HRVATSKA\",\"vozac_broj_vozacke\":\"250728\",\"vozac_telefon\":\"00385 98 9272790\",\"dodatni_kilometri\":\"1000\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"Nema\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',0,3,1389,'2016-03-10');
INSERT INTO `contracts` VALUES (24,'2016-03-11 10:21:53','2016-03-11 10:21:53','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"SUSTAV JAVNIH BICIKALA d.o.o.\",\"adresa\":\"Maceljska 4\",\"oib\":\"97795935846\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"datum_preuzimanja\":\"11.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"12.03.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"Ivan Celrjak\",\"vozac_adresa\":\"OSJELKA 2\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"10519279\",\"vozac_telefon\":\"00385 99 861 6168\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',119,5,1367,'2016-03-11');
INSERT INTO `contracts` VALUES (25,'2016-03-11 10:34:08','2016-03-11 10:35:33','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Zdenko Belko\",\"adresa\":\"Albrechova 22\",\"oib\":\"00364559214\",\"registracija_vozila\":\"OPEL VIVARO ZG-9937-FS\",\"datum_preuzimanja\":\"12.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"14.03.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"530\",\"ukupno_dana\":\"2\",\"dnevna_kilometraza\":\"300\",\"popust\":\"0\",\"vozac\":\"Zdenko Belko\",\"vozac_adresa\":\"Albrechtova 22\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"0009213\",\"vozac_telefon\":\"00385 98 239 511\",\"dodatni_kilometri\":\"200\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',107,16,1383,'2016-03-11');
INSERT INTO `contracts` VALUES (26,'2016-03-11 11:03:21','2016-03-11 11:04:56','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"LAURA MONTA\\u017dA d.o.o.\",\"adresa\":\"Dr. Franje Tu\\u0111mana 39\\/A\",\"oib\":\"29851768971\",\"registracija_vozila\":\"Fiat Ducato RI-654-UP\",\"datum_preuzimanja\":\"10.03.2016\",\"hour\":\"16\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"16:00\",\"datum_povratka\":\"12.03.2016\",\"vrijeme_povratka\":\"16:00\",\"cijena_po_danu\":\"385\",\"ukupno_dana\":\"2\",\"dnevna_kilometraza\":\"300\",\"popust\":\"15\",\"vozac\":\"Goran Jur\\u010devi\\u0107\",\"vozac_adresa\":\"\\u017deljezni\\u010dka 12\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"0482301\",\"vozac_telefon\":\"00385 95 196 9480\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',121,4,1379,'2016-03-11');
INSERT INTO `contracts` VALUES (28,'2016-03-14 08:16:49','2016-03-14 09:33:24','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Ivan Galovi\\u0107\",\"adresa\":\"Savka Lowyja 7\",\"oib\":\"2711958311401\",\"registracija_vozila\":\"Fiat Ducato RI-654-UP\",\"datum_preuzimanja\":\"14.03.2016\",\"hour\":\"09\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"09:00\",\"datum_povratka\":\"18.03.2016\",\"vrijeme_povratka\":\"09:00\",\"cijena_po_danu\":\"440\",\"ukupno_dana\":\"4\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"Ivan Galovi\\u0107\",\"vozac_adresa\":\"Slavka Lowyja 7\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0201742\",\"vozac_telefon\":\"00385 99 2724771\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"NEMA\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',0,4,1390,'2016-03-14');
INSERT INTO `contracts` VALUES (30,'2016-03-14 11:02:21','2016-03-14 11:02:21','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"SUSTAV JAVNIH BICIKALA d.o.o.\",\"adresa\":\"Maceljska 4\",\"oib\":\"97795935846\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"datum_preuzimanja\":\"11.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"12.03.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"\",\"ukupno_dana\":\"\",\"dnevna_kilometraza\":\"\",\"popust\":\"\",\"vozac\":\"\",\"vozac_adresa\":\"\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"\",\"vozac_telefon\":\"\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',119,5,1367,'2016-03-14');
INSERT INTO `contracts` VALUES (31,'2016-03-14 11:05:52','2016-03-14 11:05:52','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"HEMARK d.o.o.\",\"adresa\":\"Podgorska 6\",\"oib\":\"21126244830\",\"registracija_vozila\":\"Renault Master RI-629-UP\",\"datum_preuzimanja\":\"14.03.2016\",\"hour\":\"14\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"10:00\",\"datum_povratka\":\"14.03.2016\",\"vrijeme_povratka\":\"14:00\",\"cijena_po_danu\":\"480\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"100\",\"popust\":\"50\",\"vozac\":\"Dra\\u017een Nikoli\\u0107\",\"vozac_adresa\":\"Goranec Ro\\u017ei\\u0107i 7\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0229296\",\"vozac_telefon\":\"00385 98 505060\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',123,3,1394,'2016-03-14');
INSERT INTO `contracts` VALUES (32,'2016-03-16 08:58:10','2016-03-16 08:58:10','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"ZAGREBA\\u010cKI PROMETNI ZAVOD\",\"adresa\":\" Ljubljanska avenija 1\",\"oib\":\"68849995685\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"datum_preuzimanja\":\"16.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"18.03.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"430\",\"ukupno_dana\":\"2\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"DAVORKO BARU\\u0160I\\u0106\",\"vozac_adresa\":\"\\u0160IBENSKA ULICA 5\",\"vozac_zemlja\":\"HRVATSKA\",\"vozac_broj_vozacke\":\"6709\",\"vozac_telefon\":\"00385 91 5639526\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',128,6,1395,'2016-03-16');
INSERT INTO `contracts` VALUES (33,'2016-03-17 08:25:46','2016-03-17 08:30:49','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"TecomTrade d.o.o.\",\"adresa\":\"Hrvatskog prolje\\u0107a 20A\",\"oib\":\"10572256030\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"datum_preuzimanja\":\"17.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"18.03.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"Tomislav Antunovi\\u0107\",\"vozac_adresa\":\"Klanac 6\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0409461\",\"vozac_telefon\":\"00385 98 703307\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"NEMA\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',131,5,1338,'2016-03-17');
INSERT INTO `contracts` VALUES (34,'2016-03-18 08:05:58','2016-03-18 08:05:58','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"ClusterTech Internationa d.o.o.\",\"adresa\":\"Jakova Gotovca 1\",\"oib\":\"78877542963\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"datum_preuzimanja\":\"18.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"20.03.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"2\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"BORI\\u0160A SUBOTI\\u0106\",\"vozac_adresa\":\"PO\\u017dE\\u0160KA ULICA 6\",\"vozac_zemlja\":\"HRVATSKA\",\"vozac_broj_vozacke\":\"229654\",\"vozac_telefon\":\"00385 95 1711099\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"NEMA\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',129,6,1355,'2016-03-18');
INSERT INTO `contracts` VALUES (35,'2016-03-18 08:14:23','2016-03-18 08:14:23','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"\\u0160pruk Tihomir\",\"adresa\":\"STANKA VRAZA 42D\",\"oib\":\"96905611875\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"datum_preuzimanja\":\"18.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"21.03.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"500\",\"ukupno_dana\":\"3\",\"dnevna_kilometraza\":\"500\",\"popust\":\"0\",\"vozac\":\"TIHOMIR \\u0160PRUK\",\"vozac_adresa\":\"STANKA VRAZA 42D\",\"vozac_zemlja\":\"HRVATSKA\",\"vozac_broj_vozacke\":\"10069181\",\"vozac_telefon\":\"00385 98 474779\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"NEMA\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',91,5,1376,'2016-03-18');
INSERT INTO `contracts` VALUES (36,'2016-03-18 10:59:54','2016-03-18 10:59:54','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"Atletski klub \\u201cCRO-RUN\\u201d\",\"adresa\":\"Maksimirska cesta 112 A\",\"oib\":\"57104276518\",\"registracija_vozila\":\"Fiat Ducato RI-654-UP\",\"datum_preuzimanja\":\"18.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"21.03.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"3\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"Sini\\u0161a Ergoti\\u0107\",\"vozac_adresa\":\"Slavka Batu\\u0161i\\u0107a 27\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0270252\",\"vozac_telefon\":\"00385 99 4926685\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"NEMA\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',132,4,1397,'2016-03-18');
INSERT INTO `contracts` VALUES (37,'2016-03-19 13:27:13','2016-03-19 15:23:39','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Manuel \\u0160tigli\\u0107\",\"adresa\":\"Krasica 302 B\",\"oib\":\"40593356527\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"datum_preuzimanja\":\"19.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"12:00\",\"datum_povratka\":\"20.03.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"400\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"\",\"vozac\":\"Manuel \\u0160tigli\\u0107\",\"vozac_adresa\":\"Krasica 302 B\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"435443\",\"vozac_telefon\":\"00385 99 2345 028\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',135,6,1381,'2016-03-19');
INSERT INTO `contracts` VALUES (39,'2016-03-21 17:12:30','2016-03-21 17:12:30','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Damir Bobinac\",\"adresa\":\"Ivana Rabara 1\",\"oib\":\"54686427840\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"datum_preuzimanja\":\"21.03.2016\",\"hour\":\"17\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"17:00\",\"datum_povratka\":\"25.03.2016\",\"vrijeme_povratka\":\"17:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"4\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"Damir Bobinac\",\"vozac_adresa\":\"Ivana Rabara 1\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0541295\",\"vozac_telefon\":\"00385 98 9525215\",\"dodatni_kilometri\":\"800\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"KORISNIK MO\\u017dDA PRODU\\u017dI NAJAM, JAVITI \\u0106E TELEFONSKI\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',134,5,1398,'2016-03-21');
INSERT INTO `contracts` VALUES (40,'2016-03-22 08:17:11','2016-03-22 08:17:11','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"SUSTAV JAVNIH BICIKALA d.o.o.\",\"adresa\":\"Maceljska 4\",\"oib\":\"97795935846\",\"registracija_vozila\":\"Fiat Ducato RI-654-UP\",\"datum_preuzimanja\":\"22.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"23.03.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"Ivan Cerjak\",\"vozac_adresa\":\"Fra Filipa Grabovca 10\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"10519279\",\"vozac_telefon\":\"00385 99 8616168\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"0\",\"napomena2\":\"0\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',138,4,1367,'2016-03-22');
INSERT INTO `contracts` VALUES (42,'2016-03-22 11:26:02','2016-03-22 11:26:02','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Zrinka Ve\\u017ea\",\"adresa\":\"Josipa Pupa\\u010di\\u0107a 1\",\"oib\":\"102530141\",\"registracija_vozila\":\"Renault Master RI-629-UP\",\"datum_preuzimanja\":\"22.03.2016\",\"hour\":\"07\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"11:00\",\"datum_povratka\":\"23.03.2016\",\"vrijeme_povratka\":\"07:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"Stjepan \\u0160ala\",\"vozac_adresa\":\"Ka\\u010di\\u0107eva 6\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0233007\",\"vozac_telefon\":\"0913770589\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',139,3,1378,'2016-03-22');
INSERT INTO `contracts` VALUES (44,'2016-03-22 15:15:02','2016-03-22 15:15:02','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"SANITARIA DENTAL d.o.o. \",\"adresa\":\"Kor\\u010dulanska 4-6 \",\"oib\":\"30023990959\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"datum_preuzimanja\":\"22.03.2016\",\"hour\":\"12\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"12:00\",\"datum_povratka\":\"25.03.2016\",\"vrijeme_povratka\":\"12:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"3\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"DRa\\u017een Baoti\\u0107\",\"vozac_adresa\":\"Gornji Bukovac 3 \\/ 11\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"0215831\",\"vozac_telefon\":\"00385 99 258 4869\",\"dodatni_kilometri\":\"300\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',140,6,1400,'2016-03-22');
INSERT INTO `contracts` VALUES (45,'2016-03-23 07:01:35','2016-03-23 07:01:35','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"DOKUMENTA d.o.o.\",\"adresa\":\"Rude\\u0161ka cesta 99\",\"oib\":\"00414682067\",\"registracija_vozila\":\"Renault Master RI-629-UP\",\"datum_preuzimanja\":\"23.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"07:00\",\"datum_povratka\":\"24.03.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"Vanja Novak\",\"vozac_adresa\":\"Brezdani\\u010dka 17\",\"vozac_zemlja\":\"Hrvtska\",\"vozac_broj_vozacke\":\"10282506\",\"vozac_telefon\":\"00385 99 2899399\",\"dodatni_kilometri\":\"300\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',137,3,88,'2016-03-23');
INSERT INTO `contracts` VALUES (50,'2016-03-25 20:35:19','2016-03-25 20:35:19','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Makar Zlatko\",\"adresa\":\"A.B.Juri\\u0161i\\u0107 9\",\"oib\":\"105429157\",\"registracija_vozila\":\"Renault Master RI-629-UP\",\"datum_preuzimanja\":\"26.03.2016\",\"hour\":\"20\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"26.03.2016\",\"vrijeme_povratka\":\"20:00\",\"cijena_po_danu\":\"\",\"ukupno_dana\":\"\",\"dnevna_kilometraza\":\"\",\"popust\":\"\",\"vozac\":\"\",\"vozac_adresa\":\"\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"\",\"vozac_telefon\":\"\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',153,3,1404,'2016-03-25');
INSERT INTO `contracts` VALUES (52,'2016-03-29 08:16:58','2016-03-29 08:16:58','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"PRACTICUM VITA d.o.o.\",\"adresa\":\" Ul. Fausta Vran\\u010di\\u0107a 2\",\"oib\":\"97401494525\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"datum_preuzimanja\":\"29.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"01.04.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"400\",\"ukupno_dana\":\"3\",\"dnevna_kilometraza\":\"300\",\"popust\":\"0\",\"vozac\":\"Hrvoje Cvitkovi\\u0107\",\"vozac_adresa\":\"Horvatnica 92\",\"vozac_zemlja\":\"Hrvstska\",\"vozac_broj_vozacke\":\"0389886\",\"vozac_telefon\":\"00385 91 5889131\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"NEMA\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',144,6,1402,'2016-03-29');
INSERT INTO `contracts` VALUES (53,'2016-03-30 07:25:16','2016-03-30 07:25:16','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"STUDIO ARENA d.o.o.\",\"adresa\":\"Zagreba\\u010dka 175\",\"oib\":\"28869851978\",\"registracija_vozila\":\"Fiat Ducato RI-654-UP\",\"datum_preuzimanja\":\"30.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"04.04.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"420\",\"ukupno_dana\":\"5\",\"dnevna_kilometraza\":\"300\",\"popust\":\"0\",\"vozac\":\"Dra\\u017een \\u010culjat\",\"vozac_adresa\":\"Banjol 174\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"200671\",\"vozac_telefon\":\"00385 91 7282801\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',148,4,1407,'2016-03-30');
INSERT INTO `contracts` VALUES (54,'2016-03-30 09:34:08','2016-03-30 09:34:08','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"STROJOPROMET-ZAGREB d.o.o.\",\"adresa\":\"Zagreba\\u010dka 6\",\"oib\":\"97994010225\",\"registracija_vozila\":\"Partner - putni\\u010dki\",\"datum_preuzimanja\":\"30.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"30.04.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"\",\"ukupno_dana\":\"\",\"dnevna_kilometraza\":\"\",\"popust\":\"\",\"vozac\":\"MISLAV \\u0160TRPS\",\"vozac_adresa\":\"GLOGOVI\\u0106EVA 9\",\"vozac_zemlja\":\"HRVATSKA\",\"vozac_broj_vozacke\":\"10326641\",\"vozac_telefon\":\"00385 91 1815784\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"cijena ugovorena po ponudi 2016042\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',83,16,12,'2016-03-30');
INSERT INTO `contracts` VALUES (55,'2016-03-30 09:38:33','2016-03-30 09:38:33','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"STROJOPROMET-ZAGREB d.o.o.\",\"adresa\":\"Zagreba\\u010dka 6\",\"oib\":\"97994010225\",\"registracija_vozila\":\"Opel Vivaro RI-165-UA\",\"datum_preuzimanja\":\"01.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"01.05.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"\",\"ukupno_dana\":\"\",\"dnevna_kilometraza\":\"\",\"popust\":\"\",\"vozac\":\"STJEPAN CRNKOVI\\u0106\",\"vozac_adresa\":\"JE\\u017dEVSKA 5\",\"vozac_zemlja\":\"HRVATSKA\",\"vozac_broj_vozacke\":\"9543865\",\"vozac_telefon\":\"00385 91 5515558\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"CIJENA UGOVORENA PO PONUDI 2016109\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',125,8,12,'2016-03-30');
INSERT INTO `contracts` VALUES (57,'2016-03-30 14:59:38','2016-03-30 14:59:38','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Miljenko Sladovi\\u0107\",\"adresa\":\"Bartoli\\u0107i 21\",\"oib\":\"br.os.103029780\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"datum_preuzimanja\":\"31.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"06.04.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"450\",\"ukupno_dana\":\"6\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"vozac\":\"Miljenko Sladovi\\u0107\",\"vozac_adresa\":\"Bartoli\\u0107i 21\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"10187773\",\"vozac_telefon\":\"00385 98 237094\",\"dodatni_kilometri\":\"1200\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"Vozilo preuzeto 30.03.2016 u 15.00. bez naplate. Ostavljen 1000kuna gotovinskog pologa.\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',146,5,1403,'2016-03-30');
INSERT INTO `contracts` VALUES (58,'2016-03-31 08:19:49','2016-03-31 08:45:52','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"GWT d.o.o.\",\"adresa\":\"Vo\\u0107arska 48\",\"oib\":\"05626170569\",\"registracija_vozila\":\"Renault Master RI-629-UP\",\"datum_preuzimanja\":\"31.03.2016\",\"hour\":\"09\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"09:00\",\"datum_povratka\":\"01.04.2016\",\"vrijeme_povratka\":\"09:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"5\",\"vozac\":\"LEON-TOMISLAV GREGURI\\u0106\",\"vozac_adresa\":\"Vo\\u0107arska 48\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"117163\",\"vozac_telefon\":\"00385 98 233140\",\"dodatni_kilometri\":\"100\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',156,3,1406,'2016-03-31');
INSERT INTO `contracts` VALUES (59,'2016-04-01 08:36:23','2016-04-01 10:05:24','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"RONDO MONDO d.o.o.\",\"adresa\":\"\\u0160tefanove\\u010dka cesta 10\",\"oib\":\"50082788552\",\"registracija_vozila\":\"Renault Master RI-629-UP\",\"vozac\":\"Dario \\u0160ehovi\\u0107\",\"vozac_adresa\":\"Jarnovi\\u0107eva 17F\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0454710\",\"vozac_telefon\":\"00385 98 9124469\",\"datum_preuzimanja\":\"01.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"02.04.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"NEMA\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',0,3,1411,'2016-04-01');
INSERT INTO `contracts` VALUES (61,'2016-04-01 09:01:30','2016-04-01 09:01:30','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Dean Konjevi\\u0107\",\"adresa\":\"Nikole Kramari\\u0107a 2B\",\"oib\":\"48599308205\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"vozac\":\"Dean Konjevi\\u0107\",\"vozac_adresa\":\"Nikole Kramari\\u0107a 2B\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"10372914\",\"vozac_telefon\":\"00385 98 1912084\",\"datum_preuzimanja\":\"01.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"02.04.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"450\",\"ukupno_dana\":\"10\",\"dnevna_kilometraza\":\"300\",\"popust\":\"\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnSave\":\"\"}','',160,6,1409,'2016-04-01');
INSERT INTO `contracts` VALUES (62,'2016-04-01 10:16:46','2016-04-01 10:16:46','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Obrt za elektronsko mjerenje vremena CHAMPSTAT TIMING\",\"adresa\":\"Matije Skurjenija 153\",\"oib\":\"69853710977\",\"registracija_vozila\":\"Partner - putni\\u010dki\",\"vozac\":\"Ivan Micudaj\",\"vozac_adresa\":\"Kunti\\u0107i 11\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0541711\",\"vozac_telefon\":\"00385 97 7532157\",\"datum_preuzimanja\":\"01.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"04.04.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"530\",\"ukupno_dana\":\"3\",\"dnevna_kilometraza\":\"300\",\"popust\":\"0\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',115,16,870,'2016-04-01');
INSERT INTO `contracts` VALUES (63,'2016-04-01 10:40:32','2016-04-01 10:40:32','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Centar Zdravlja d.o.o.\",\"adresa\":\"Virovska 15\",\"oib\":\"29902184926\",\"registracija_vozila\":\"Renault Master RI-629-UP\",\"vozac\":\"po nalogu korisnika\",\"vozac_adresa\":\"po nalogu korisnika\",\"vozac_zemlja\":\"po nalogu korisnika\",\"vozac_broj_vozacke\":\"po nalogu korisnika\",\"vozac_telefon\":\" 0916038268\",\"datum_preuzimanja\":\"04.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"04.05.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"0\",\"ukupno_dana\":\"\",\"dnevna_kilometraza\":\"175\",\"popust\":\"\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"CIJENA JE UGOVORENA PO PONUDI 2016087\\r\\n- NAJAM \\u0106E BITI PRODU\\u017dIVAN PO POTREBI\\r\\n\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',0,3,1408,'2016-04-01');
INSERT INTO `contracts` VALUES (64,'2016-04-01 10:41:39','2016-04-01 10:41:58','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Centar Zdravlja d.o.o.\",\"adresa\":\"Virovska 15\",\"oib\":\"29902184926\",\"registracija_vozila\":\"Fiat Ducato RI-654-UP\",\"vozac\":\"po nalogu korisnika\",\"vozac_adresa\":\"po nalogu korisnika\",\"vozac_zemlja\":\"po nalogu korisnika\",\"vozac_broj_vozacke\":\"po nalogu korisnika\",\"vozac_telefon\":\" 0916038268\",\"datum_preuzimanja\":\"04.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"04.05.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"0\",\"ukupno_dana\":\"\",\"dnevna_kilometraza\":\"175\",\"popust\":\"\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"CIJENA JE UGOVORENA PO PONUDI 2016087\\r\\n- NAJAM \\u0106E BITI PRODU\\u017dIVAN PO POTREBI\\r\\n\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',0,4,1408,'2016-04-01');
INSERT INTO `contracts` VALUES (65,'2016-04-02 09:22:12','2016-04-02 09:22:12','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Domagoj Rai\\u0107\",\"adresa\":\"Augusta \\u0160enoe\",\"oib\":\"189097711378\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"vozac\":\"Domagoj Rai\\u0107\",\"vozac_adresa\":\"Augusta \\u0160enoe\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"0498929\",\"vozac_telefon\":\"00385 91 5142030\",\"datum_preuzimanja\":\"02.04.2016\",\"hour\":\"09\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"09:00\",\"datum_povratka\":\"03.04.2016\",\"vrijeme_povratka\":\"09:00\",\"cijena_po_danu\":\"450\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"75\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',163,6,1412,'2016-04-02');
INSERT INTO `contracts` VALUES (66,'2016-04-03 11:58:57','2016-04-03 11:58:57','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"INTERIER MONT d.o.o.\",\"adresa\":\"\\u017delje\\u017eni\\u010dka 41\",\"oib\":\"35036017877\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"vozac\":\"Dragan Kr\\u010di\\u0107\",\"vozac_adresa\":\"Be\\u010daj Republiska 189\",\"vozac_zemlja\":\"Srbija\",\"vozac_broj_vozacke\":\"20414\",\"vozac_telefon\":\"0038 95 849 6917\",\"datum_preuzimanja\":\"04.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"09.04.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"520\",\"ukupno_dana\":\"5\",\"dnevna_kilometraza\":\"550\",\"popust\":\"0\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"NEMA\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',168,6,365,'2016-04-03');
INSERT INTO `contracts` VALUES (67,'2016-04-06 09:37:15','2016-04-06 09:37:15','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"HEMARK d.o.o.\",\"adresa\":\"Podgorska 6\",\"oib\":\"21126244830\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"vozac\":\"Dra\\u017een Nikoli\\u0107\",\"vozac_adresa\":\"Goranec Ro\\u017ei\\u0107i 7\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0229296\",\"vozac_telefon\":\"00385 98 505060\",\"datum_preuzimanja\":\"06.04.2016\",\"hour\":\"09\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"09:00\",\"datum_povratka\":\"07.04.2016\",\"vrijeme_povratka\":\"09:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',169,5,1394,'2016-04-06');
INSERT INTO `contracts` VALUES (68,'2016-04-07 11:51:29','2016-04-07 11:51:29','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"\\u017deljko Bo\\u017eovi\\u0107\",\"adresa\":\"Juruja Gagarina 134\",\"oib\":\"br.put.008701052\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"vozac\":\"\\u017deljko Bo\\u017eovi\\u0107\",\"vozac_adresa\":\"Juruja Gagarina 134\",\"vozac_zemlja\":\"Srbija\",\"vozac_broj_vozacke\":\"1089132\",\"vozac_telefon\":\"00385 97 6737948\",\"datum_preuzimanja\":\"07.04.2016\",\"hour\":\"15\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"15:00\",\"datum_povratka\":\"08.04.2016\",\"vrijeme_povratka\":\"15:00\",\"cijena_po_danu\":\"450\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"5\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',173,5,1417,'2016-04-07');
INSERT INTO `contracts` VALUES (69,'2016-04-08 13:58:27','2016-04-08 13:58:27','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Luk\\u0161a DOminikovi\\u0107\",\"adresa\":\"Matije Gupca 12\",\"oib\":\"95052052983\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"vozac\":\"Luk\\u0161a DOminikovi\\u0107\",\"vozac_adresa\":\"Matije Gupca 12\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"0205699\",\"vozac_telefon\":\"00385 91 9249327\",\"datum_preuzimanja\":\"09.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"11.04.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"500\",\"ukupno_dana\":\"2\",\"dnevna_kilometraza\":\"500\",\"popust\":\"0\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',177,5,1362,'2016-04-08');
INSERT INTO `contracts` VALUES (70,'2016-04-09 08:39:34','2016-04-09 08:39:34','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Antonio Obadi\\u0107\",\"adresa\":\"Ulica Ivana Lackovi\\u0107a 16\",\"oib\":\"13800661661\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"vozac\":\"Antonio Obadi\\u0107\",\"vozac_adresa\":\"Ulica Ivana Lackovi\\u0107a 16\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0216444\",\"vozac_telefon\":\"00385 99 6749666\",\"datum_preuzimanja\":\"09.04.2016\",\"hour\":\"09\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"09:00\",\"datum_povratka\":\"10.04.2016\",\"vrijeme_povratka\":\"09:00\",\"cijena_po_danu\":\"500\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"400\",\"popust\":\"0\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"NEMA\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',178,6,1422,'2016-04-09');
INSERT INTO `contracts` VALUES (71,'2016-04-12 07:48:00','2016-04-12 07:48:00','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"Integrirani poslovni sustavi d.o.o.\",\"adresa\":\" III. Cvjetno naselje 18\",\"oib\":\"66238130370\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"vozac\":\"Dalibor Kotarinin\",\"vozac_adresa\":\"Maljevac 76\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0202244\",\"vozac_telefon\":\"00385 91 2541942\",\"datum_preuzimanja\":\"12.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"13.04.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"dodatni_kilometri\":\"570\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',185,6,1425,'2016-04-12');
INSERT INTO `contracts` VALUES (72,'2016-04-12 12:47:17','2016-04-12 12:47:17','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Danijel Ivanovi\\u0107\",\"adresa\":\"Novoselska 54A\",\"oib\":\"99230965884\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"vozac\":\"Danijel Ivanovi\\u0107\",\"vozac_adresa\":\"Novoselska 54A\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"0223619\",\"vozac_telefon\":\"00385 95 879 2073\",\"datum_preuzimanja\":\"12.04.2016\",\"hour\":\"13\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"13:00\",\"datum_povratka\":\"13.04.2016\",\"vrijeme_povratka\":\"13:00\",\"cijena_po_danu\":\"430\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"0\",\"dodatni_kilometri\":\"300\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',186,5,1426,'2016-04-12');
INSERT INTO `contracts` VALUES (73,'2016-04-14 08:14:59','2016-04-14 08:14:59','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"NESTOR IN\\u017dENJERING d.o.o.\",\"adresa\":\"Pokupska 32\",\"oib\":\"25351911375\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"vozac\":\"Petrak Radivoj\",\"vozac_adresa\":\"Miroslav Krle\\u017ea 8\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0208984\",\"vozac_telefon\":\"00385 \",\"datum_preuzimanja\":\"14.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"15.04.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"9\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',188,5,1428,'2016-04-14');
INSERT INTO `contracts` VALUES (74,'2016-04-15 11:43:40','2016-04-15 11:43:40','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Dalibor Evi\\u0107\",\"adresa\":\"Mali Erjavec 30A\",\"oib\":\"47922891922\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"vozac\":\"Dalibor Evi\\u0107\",\"vozac_adresa\":\"Mali Erjavec 30A\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"0228522\",\"vozac_telefon\":\"00385 91 6039174\",\"datum_preuzimanja\":\"15.04.2016\",\"hour\":\"12\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"12:00\",\"datum_povratka\":\"18.04.2016\",\"vrijeme_povratka\":\"12:00\",\"cijena_po_danu\":\"450\",\"ukupno_dana\":\"3\",\"dnevna_kilometraza\":\"300\",\"popust\":\"0\",\"dodatni_kilometri\":\"700\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',170,5,1414,'2016-04-15');
INSERT INTO `contracts` VALUES (75,'2016-04-19 08:04:18','2016-04-19 08:04:18','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"FRANKOPROM j.d.o.o.\",\"adresa\":\"Kneza Ljudevita Posavskog 102\",\"oib\":\"11696288578\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"vozac\":\"Mario Bo\\u0161njak\",\"vozac_adresa\":\"Kneza Ljudevuita OPosavskog 102\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"10329444\",\"vozac_telefon\":\"00385 91 1864400\",\"datum_preuzimanja\":\"19.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"20.04.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"dodatni_kilometri\":\"300\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"Prikazuje se servisna lampica  za ko\\u010dnice . Nije resetirana.\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',191,5,1430,'2016-04-19');
INSERT INTO `contracts` VALUES (76,'2016-04-19 09:32:21','2016-04-19 09:32:21','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Marti\\u0107 Fran\",\"adresa\":\"Vitasovi\\u0107eva Poljana 1\",\"oib\":\"104654339\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"vozac\":\"Fran Marti\\u0107\",\"vozac_adresa\":\"Vitasovi\\u0107eva Poljana 1\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"00445497\",\"vozac_telefon\":\"00385 91 7303596\",\"datum_preuzimanja\":\"19.04.2016\",\"hour\":\"09\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"09:00\",\"datum_povratka\":\"23.04.2016\",\"vrijeme_povratka\":\"09:00\",\"cijena_po_danu\":\"450\",\"ukupno_dana\":\"4\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',193,6,17,'2016-04-19');
INSERT INTO `contracts` VALUES (77,'2016-04-20 12:52:28','2016-04-20 12:52:28','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Luk\\u0161a DOminikovi\\u0107\",\"adresa\":\"Matije Gupca 12\",\"oib\":\"95952052983\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"vozac\":\"Luk\\u0161a Dominikovi\\u0107\",\"vozac_adresa\":\"Matije Gupca 12\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0205699\",\"vozac_telefon\":\"00385 91 9249327\",\"datum_preuzimanja\":\"20.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"12:00\",\"datum_povratka\":\"21.04.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"450\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',196,5,1362,'2016-04-20');
INSERT INTO `contracts` VALUES (78,'2016-04-21 08:44:58','2016-04-21 08:44:58','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Marko Grgi\\u0107\",\"adresa\":\"Putine 15A\",\"oib\":\"55566959040\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"vozac\":\"Marko Grgi\\u0107\",\"vozac_adresa\":\"Putine 15A\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"0523032\",\"vozac_telefon\":\"00385 98 423616\",\"datum_preuzimanja\":\"21.04.2016\",\"hour\":\"11\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"11:00\",\"datum_povratka\":\"22.04.2016\",\"vrijeme_povratka\":\"11:00\",\"cijena_po_danu\":\"450\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"0\",\"dodatni_kilometri\":\"123\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',198,5,1434,'2016-04-21');
INSERT INTO `contracts` VALUES (79,'2016-04-22 13:10:41','2016-04-22 13:10:41','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"INTERIER MONT d.o.o.\",\"adresa\":\"\\u017delje\\u017eni\\u010dka 41\",\"oib\":\"35036017877\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"vozac\":\"Slobodan Aleksi\\u0107\",\"vozac_adresa\":\"\\u017delje\\u017eni\\u010dka 41\",\"vozac_zemlja\":\"35036017877\",\"vozac_broj_vozacke\":\"983738\",\"vozac_telefon\":\"00385 95 8496917\",\"datum_preuzimanja\":\"25.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"07.05.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"385\",\"ukupno_dana\":\"12\",\"dnevna_kilometraza\":\"235\",\"popust\":\"10\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"0\",\"napomena2\":\"0\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',204,6,365,'2016-04-22');
INSERT INTO `contracts` VALUES (80,'2016-04-22 13:16:42','2016-04-22 13:16:42','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"ARTEC S.P. d.o.o.\",\"adresa\":\"Pantov\\u010dak  7\",\"oib\":\"70439021788\",\"registracija_vozila\":\"Renault Trafic DA-760-DL\",\"vozac\":\"SIni\\u0161a Prpi\\u0107\",\"vozac_adresa\":\"Pantov\\u010dak 7\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"00385 98 276 015\",\"vozac_telefon\":\"00385 98 276 015\",\"datum_preuzimanja\":\"25.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"26.04.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"510\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"dodatni_kilometri\":\"400\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"100\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"0\",\"napomena2\":\"0\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',171,11,1415,'2016-04-22');
INSERT INTO `contracts` VALUES (81,'2016-04-22 14:06:21','2016-04-22 14:06:21','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Sa\\u0161a Sabo\",\"adresa\":\"Vladimira Vladi\\u010daka 13\",\"oib\":\"2711957330097\",\"registracija_vozila\":\"Renault Master RI-838-VH\",\"vozac\":\"Sa\\u0161a Sabo\",\"vozac_adresa\":\"Vladimira Vladi\\u010daka 13\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"14275\",\"vozac_telefon\":\"00385 91 2263531\",\"datum_preuzimanja\":\"22.04.2016\",\"hour\":\"17\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"17:00\",\"datum_povratka\":\"24.04.2016\",\"vrijeme_povratka\":\"17:00\",\"cijena_po_danu\":\"450\",\"ukupno_dana\":\"2\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"100\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',192,5,1431,'2016-04-22');
INSERT INTO `contracts` VALUES (82,'2016-04-22 14:20:55','2016-04-22 14:20:55','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Ivana Ozana Prah\",\"adresa\":\"FRA FILIPA GRABOVCA 18\",\"oib\":\"04576802350\",\"registracija_vozila\":\"Opel Vivaro RI-165-UA\",\"vozac\":\"Ivana Ozana Prah\",\"vozac_adresa\":\"FRA FILIPA GRABOVCA 18\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"0412332\",\"vozac_telefon\":\"00385 91 754 0062\",\"datum_preuzimanja\":\"23.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"07:00\",\"datum_povratka\":\"25.04.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"500\",\"ukupno_dana\":\"2\",\"dnevna_kilometraza\":\"300\",\"popust\":\"0\",\"dodatni_kilometri\":\"500\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"0\",\"napomena2\":\"0\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',202,8,1439,'2016-04-22');
INSERT INTO `contracts` VALUES (83,'2016-04-23 07:20:40','2016-04-23 07:20:40','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"ITI - INTERNET INSTITUT\",\"adresa\":\"Matije Gupca 63\\/I\",\"oib\":\"34343208149\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"vozac\":\"Natalija Gojkovi\\u0107\",\"vozac_adresa\":\"Matije Gupca 63\\/I\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"08149\",\"vozac_telefon\":\"00385 91 3444 825\",\"datum_preuzimanja\":\"23.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"09:00\",\"datum_povratka\":\"25.04.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"450\",\"ukupno_dana\":\"2\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',197,6,1433,'2016-04-23');
INSERT INTO `contracts` VALUES (84,'2016-04-26 08:13:24','2016-04-26 13:13:02','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"FAE Group S.p.a.\",\"adresa\":\"Zona produttiva, 18 \",\"oib\":\"IT01942570225\",\"registracija_vozila\":\"Opel Vivaro RI-165-UA\",\"vozac\":\"Goran Goreti\\u0107\",\"vozac_adresa\":\"Ilica 425\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0291179\",\"vozac_telefon\":\"00385 91 2524985\",\"datum_preuzimanja\":\"26.04.2016\",\"hour\":\"12\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"12:00\",\"datum_povratka\":\"30.04.2016\",\"vrijeme_povratka\":\"12:00\",\"cijena_po_danu\":\"530\",\"ukupno_dana\":\"4\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"dodatni_kilometri\":\"450\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"NEMA\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',201,8,1437,'2016-04-26');
INSERT INTO `contracts` VALUES (85,'2016-04-27 08:30:51','2016-04-27 08:30:51','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"Odsjek za indologiju i dalekoisto\\u010dne studije Filozofski fakultet\",\"adresa\":\"Ivana Lu\\u010di\\u0107a 3\",\"oib\":\"90633715804\",\"registracija_vozila\":\"Partner - putni\\u010dki\",\"vozac\":\"Kre\\u0161imir Krni\\u0107\",\"vozac_adresa\":\"4 Ravnice 25\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"4669\",\"vozac_telefon\":\"00385 98 476160\",\"datum_preuzimanja\":\"27.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"01.05.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"510\",\"ukupno_dana\":\"4\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"dodatni_kilometri\":\"700\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"0\",\"napomena2\":\"0\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',187,16,1427,'2016-04-27');
INSERT INTO `contracts` VALUES (86,'2016-04-27 08:47:18','2016-04-27 08:47:18','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Dejan Hribar\",\"adresa\":\"Gran\\u010darska ulica Vodvojak 7 A\",\"oib\":\"80164009375\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"vozac\":\"Dejan Hribar\",\"vozac_adresa\":\"Gran\\u010darska ulica Vodvojak 7 A\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"0111980500019\",\"vozac_telefon\":\"00385 99 4240323\",\"datum_preuzimanja\":\"27.04.2016\",\"hour\":\"12\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"27.04.2016\",\"vrijeme_povratka\":\"12:00\",\"cijena_po_danu\":\"\",\"ukupno_dana\":\"\",\"dnevna_kilometraza\":\"60\",\"popust\":\"\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"240\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',211,6,1445,'2016-04-27');
INSERT INTO `contracts` VALUES (87,'2016-04-27 17:36:48','2016-04-27 17:36:48','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Hrvoje Tretinjak\",\"adresa\":\"Usorska 33\",\"oib\":\"88478572727\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"vozac\":\"Hrvoje Tretinjak\",\"vozac_adresa\":\"Usorska 33\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"10237789\",\"vozac_telefon\":\"00385 95 5208850\",\"datum_preuzimanja\":\"27.04.2016\",\"hour\":\"18\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"18:00\",\"datum_povratka\":\"28.04.2016\",\"vrijeme_povratka\":\"18:00\",\"cijena_po_danu\":\"450\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"5\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',209,6,1443,'2016-04-27');
INSERT INTO `contracts` VALUES (88,'2016-04-28 13:30:40','2016-04-28 13:30:40','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"HUANG WEIDONG\",\"adresa\":\"Ulica Antuna \\u0160oljana 26\",\"oib\":\"46484662616\",\"registracija_vozila\":\"Renault Trafic DA-760-DL\",\"vozac\":\"HUANG WEIDONG\",\"vozac_adresa\":\"Ulica Antuna \\u0160oljana 26\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"10247270\",\"vozac_telefon\":\"00385 98 283 505\",\"datum_preuzimanja\":\"28.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"08.05.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"430\",\"ukupno_dana\":\"10\",\"dnevna_kilometraza\":\"2300\",\"popust\":\"\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',210,11,1444,'2016-04-28');
INSERT INTO `contracts` VALUES (89,'2016-04-29 07:24:33','2016-04-29 07:24:33','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"SUSTAV JAVNIH BICIKALA d.o.o.\",\"adresa\":\"Maceljska 4\",\"oib\":\"97795935846\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"vozac\":\"Ivan Cerjak\",\"vozac_adresa\":\"Fra Filipa Grabovca 10\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"10519279\",\"vozac_telefon\":\"00385 99 8616168\",\"datum_preuzimanja\":\"29.04.2016\",\"hour\":\"17\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"07:00\",\"datum_povratka\":\"29.04.2016\",\"vrijeme_povratka\":\"17:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"10\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"Nema\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',217,6,1367,'2016-04-29');
INSERT INTO `contracts` VALUES (90,'2016-04-29 07:46:04','2016-04-29 07:46:04','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Centar Zdravlja d.o.o.\",\"adresa\":\"Virovska 15\",\"oib\":\"29902184926\",\"registracija_vozila\":\"Opel Vivaro RI-121-SR\",\"vozac\":\"Po nalogu korisnika\",\"vozac_adresa\":\"Po nalog korisnika\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"Po nalogu korisnika\",\"vozac_telefon\":\"Po nalogu korisnika\",\"datum_preuzimanja\":\"04.05.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"04.06.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"330\",\"ukupno_dana\":\"30\",\"dnevna_kilometraza\":\"170\",\"popust\":\"21\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"NEMA\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',216,7,1408,'2016-04-29');
INSERT INTO `contracts` VALUES (91,'2016-04-29 07:51:19','2016-04-29 07:51:19','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"Centar Zdravlja d.o.o.\",\"adresa\":\"Virovska 15\",\"oib\":\"29902184926\",\"registracija_vozila\":\"Fiat Ducato RI-654-UP\",\"vozac\":\"Po nalogu korisnika\",\"vozac_adresa\":\"Po nalogu korisnika\",\"vozac_zemlja\":\"Po nalogu korisnika\",\"vozac_broj_vozacke\":\"Po nalogu korisnika\",\"vozac_telefon\":\"Po nalogu korisnika\",\"datum_preuzimanja\":\"04.05.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"04.06.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"330\",\"ukupno_dana\":\"30\",\"dnevna_kilometraza\":\"170\",\"popust\":\"29\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',215,4,1408,'2016-04-29');
INSERT INTO `contracts` VALUES (93,'2016-04-29 11:55:57','2016-05-02 07:35:29','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"EKO ELEKTRO VOD d.o.o.\",\"adresa\":\"P. \\u0160toosa 10 A\",\"oib\":\"35955468622\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"vozac\":\"Ivo Andri\\u0107\",\"vozac_adresa\":\"P.\\u0160toosa 10 a\",\"vozac_zemlja\":\"10360\",\"vozac_broj_vozacke\":\"0216448\",\"vozac_telefon\":\"00385 98 389797\",\"datum_preuzimanja\":\"02.05.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"04.05.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"2\",\"dnevna_kilometraza\":\"300\",\"popust\":\"9\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"0\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"0\",\"napomena2\":\"0\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',219,6,1449,'2016-04-29');
INSERT INTO `contracts` VALUES (94,'2016-04-29 17:36:03','2016-04-29 17:36:03','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Domagoj Kralj\",\"adresa\":\"Baranjska 3\",\"oib\":\"75218921871\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"vozac\":\"Domagoj Kralj\",\"vozac_adresa\":\"Baranjska 3 V.G\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"0214744\",\"vozac_telefon\":\"00385 99 6042558\",\"datum_preuzimanja\":\"29.04.2016\",\"hour\":\"17\",\"minute\":\"30\",\"vrijeme_preuzimanja\":\"17:30\",\"datum_povratka\":\"01.05.2016\",\"vrijeme_povratka\":\"17:30\",\"cijena_po_danu\":\"435\",\"ukupno_dana\":\"2\",\"dnevna_kilometraza\":\"300\",\"popust\":\"5\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"100\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"Povrat vozila 02.05.2016 u 7.45\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',214,6,1447,'2016-04-29');
INSERT INTO `contracts` VALUES (95,'2016-04-30 09:06:43','2016-04-30 09:06:43','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Obrt za elektronsko mjerenje vremena CHAMPSTAT TIMING\",\"adresa\":\"Matije Skurjenija 153\",\"oib\":\"69853710977\",\"registracija_vozila\":\"Opel Vivaro RI-121-SR\",\"vozac\":\"Ivan Micudaj\",\"vozac_adresa\":\"Kunti\\u0107i 11\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0541711\",\"vozac_telefon\":\"00385 97 7532157\",\"datum_preuzimanja\":\"30.04.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"14:00\",\"datum_povratka\":\"02.05.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"715\",\"ukupno_dana\":\"1\",\"dnevna_kilometraza\":\"300\",\"popust\":\"\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',184,7,870,'2016-04-30');
INSERT INTO `contracts` VALUES (96,'2016-05-02 10:58:28','2016-05-02 10:58:28','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"Ekonomsko propagandni program d.o.o.\",\"adresa\":\"Svete Ane 9\",\"oib\":\"92099286194\",\"registracija_vozila\":\"Partner - teretni\",\"vozac\":\"\",\"vozac_adresa\":\"\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"\",\"vozac_telefon\":\"\",\"datum_preuzimanja\":\"02.05.2016\",\"hour\":\"15\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"11:00\",\"datum_povratka\":\"02.05.2016\",\"vrijeme_povratka\":\"15:00\",\"cijena_po_danu\":\"0\",\"ukupno_dana\":\"0\",\"dnevna_kilometraza\":\"0\",\"popust\":\"0\",\"dodatni_kilometri\":\"60\",\"dodatni_sat_najma\":\"240\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',218,15,1448,'2016-05-02');
INSERT INTO `contracts` VALUES (97,'2016-05-02 10:58:50','2016-05-02 10:58:50','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"Ekonomsko propagandni program d.o.o.\",\"adresa\":\"Svete Ane 9\",\"oib\":\"92099286194\",\"registracija_vozila\":\"Partner - teretni\",\"vozac\":\"\",\"vozac_adresa\":\"\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"\",\"vozac_telefon\":\"\",\"datum_preuzimanja\":\"02.05.2016\",\"hour\":\"15\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"11:00\",\"datum_povratka\":\"02.05.2016\",\"vrijeme_povratka\":\"15:00\",\"cijena_po_danu\":\"0\",\"ukupno_dana\":\"0\",\"dnevna_kilometraza\":\"0\",\"popust\":\"0\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"240\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',218,15,1448,'2016-05-02');
INSERT INTO `contracts` VALUES (98,'2016-05-03 12:56:57','2016-05-03 12:56:57','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"Tehnika d.d.\",\"adresa\":\"Ulica grada Vukovara 274\",\"oib\":\"73037001250\",\"registracija_vozila\":\"Opel Vivaro RI-121-SR\",\"vozac\":\"\",\"vozac_adresa\":\"\",\"vozac_zemlja\":\"\",\"vozac_broj_vozacke\":\"\",\"vozac_telefon\":\"\",\"datum_preuzimanja\":\"05.05.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"05.06.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"350\",\"ukupno_dana\":\"30\",\"dnevna_kilometraza\":\"150\",\"popust\":\"24\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',0,7,1451,'2016-05-03');
INSERT INTO `contracts` VALUES (99,'2016-05-04 09:52:35','2016-05-04 09:52:35','','','{\"najmodavac\":\"2\",\"najmoprimac\":\"Stjepan Vranek\",\"adresa\":\"IV Luka 13\",\"oib\":\"04388885880\",\"registracija_vozila\":\"Renault Master RI-540-VN\",\"vozac\":\"Stjepan Vranek\",\"vozac_adresa\":\"IV Luka 13\",\"vozac_zemlja\":\"04388885880\",\"vozac_broj_vozacke\":\"0018306\",\"vozac_telefon\":\"00385 98 1625061\",\"datum_preuzimanja\":\"04.05.2016\",\"hour\":\"14\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"10:00\",\"datum_povratka\":\"04.05.2016\",\"vrijeme_povratka\":\"14:00\",\"cijena_po_danu\":\"0\",\"ukupno_dana\":\"0\",\"dnevna_kilometraza\":\"\",\"popust\":\"\",\"dodatni_kilometri\":\"80\",\"dodatni_sat_najma\":\"208\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',220,6,1450,'2016-05-04');
INSERT INTO `contracts` VALUES (100,'2016-05-05 08:27:07','2016-05-05 08:27:07','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"Tehnika d.d.\",\"adresa\":\"Ulica grada Vukovara 274\",\"oib\":\"73037001250\",\"registracija_vozila\":\"Opel Vivaro RI-165-UA\",\"vozac\":\"Josip Sambol\",\"vozac_adresa\":\"Hum Bistri\\u010dki 82G\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"1714\",\"vozac_telefon\":\"00385 98 1849898\",\"datum_preuzimanja\":\"05.05.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"04.06.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"350\",\"ukupno_dana\":\"30\",\"dnevna_kilometraza\":\"150\",\"popust\":\"24\",\"dodatni_kilometri\":\"\",\"dodatni_sat_najma\":\"\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"NAJM \\u0106E TRAJATI DO 5.10.2016 \",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',223,8,1451,'2016-05-05');
INSERT INTO `contracts` VALUES (101,'2016-05-05 10:42:09','2016-05-05 10:42:09','','','{\"najmodavac\":\"1\",\"najmoprimac\":\"SYSTEM ONE d.o.o.\",\"adresa\":\"Ul. Grada Vukovara 237 D\",\"oib\":\"43566773819\",\"registracija_vozila\":\"Renault Master RI-629-UP\",\"vozac\":\"TOMA\\u0160 BLAHA\",\"vozac_adresa\":\"De\\u017eanovac 259\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"0205260\",\"vozac_telefon\":\"00385 99 6590991\",\"datum_preuzimanja\":\"05.05.2016\",\"hour\":\"14\",\"minute\":\"30\",\"vrijeme_preuzimanja\":\"10:30\",\"datum_povratka\":\"05.05.2016\",\"vrijeme_povratka\":\"14:30\",\"cijena_po_danu\":\"0\",\"ukupno_dana\":\"0\",\"dnevna_kilometraza\":\"0\",\"popust\":\"0\",\"dodatni_kilometri\":\"0\",\"dodatni_sat_najma\":\"240\",\"preuzimanje_izvan_rv\":\"\",\"dostava_vozila_na_adresu\":\"\",\"napomena\":\"\",\"napomena2\":\"\",\"ugovor_napisao\":\"Igor \\u010cukac\",\"btnPrint\":\"\"}','',226,3,1309,'2016-05-05');
/*!40000 ALTER TABLE `contracts` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table countries
#

DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pri` smallint(6) NOT NULL DEFAULT '0',
  `iso3166` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `iso3166a3` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table countries
#
LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;

INSERT INTO `countries` VALUES (1,'Afghanistan','af',2,'af','');
INSERT INTO `countries` VALUES (2,'Albania','al',3,'al','');
INSERT INTO `countries` VALUES (3,'Algeria','dz',4,'dz','');
INSERT INTO `countries` VALUES (4,'American Samoa','as',5,'as','');
INSERT INTO `countries` VALUES (5,'Andorra','ad',6,'ad','');
INSERT INTO `countries` VALUES (6,'Angola','ao',7,'ao','');
INSERT INTO `countries` VALUES (7,'Anguilla','ai',8,'ai','');
INSERT INTO `countries` VALUES (8,'Antarctica','aq',9,'aq','');
INSERT INTO `countries` VALUES (9,'Antigua and Barbuda','ag',10,'ag','');
INSERT INTO `countries` VALUES (10,'Argentina','ar',11,'ar','');
INSERT INTO `countries` VALUES (11,'Armenia','am',12,'am','');
INSERT INTO `countries` VALUES (12,'Aruba','aw',13,'aw','');
INSERT INTO `countries` VALUES (13,'Australia','au',14,'au','');
INSERT INTO `countries` VALUES (14,'Austria','at',15,'at','');
INSERT INTO `countries` VALUES (15,'Azerbaijan','az',16,'az','');
INSERT INTO `countries` VALUES (16,'Bahamas','bs',17,'bs','');
INSERT INTO `countries` VALUES (17,'Bahrain','bh',18,'bh','');
INSERT INTO `countries` VALUES (18,'Bangladesh','bd',19,'bd','');
INSERT INTO `countries` VALUES (19,'Barbados','bb',20,'bb','');
INSERT INTO `countries` VALUES (20,'Belarus','by',21,'by','');
INSERT INTO `countries` VALUES (21,'Belgium','be',22,'be','');
INSERT INTO `countries` VALUES (22,'Belize','bz',23,'bz','');
INSERT INTO `countries` VALUES (23,'Benin','bj',24,'bj','');
INSERT INTO `countries` VALUES (24,'Bermuda','bm',25,'bm','');
INSERT INTO `countries` VALUES (25,'Bhutan','bt',26,'bt','');
INSERT INTO `countries` VALUES (26,'Bolivia','bo',27,'bo','');
INSERT INTO `countries` VALUES (27,'Bosnia and Herzegovina','ba',28,'ba','');
INSERT INTO `countries` VALUES (28,'Botswana','bw',29,'bw','');
INSERT INTO `countries` VALUES (29,'Bouvet Island','bv',30,'bv','');
INSERT INTO `countries` VALUES (30,'Brazil','br',31,'br','');
INSERT INTO `countries` VALUES (31,'British Indian Ocean Territory','io',32,'io','');
INSERT INTO `countries` VALUES (32,'Brunei darussalam','bn',33,'bn','');
INSERT INTO `countries` VALUES (33,'Bulgaria','bg',34,'bg','');
INSERT INTO `countries` VALUES (34,'Burkina Faso','bf',35,'bf','');
INSERT INTO `countries` VALUES (35,'Burundi','bi',36,'bi','');
INSERT INTO `countries` VALUES (36,'Cambodia','kh',37,'kh','');
INSERT INTO `countries` VALUES (37,'Cameroon','cm',38,'cm','');
INSERT INTO `countries` VALUES (38,'Canada','ca',39,'ca','');
INSERT INTO `countries` VALUES (39,'Cape Verde','cv',40,'cv','');
INSERT INTO `countries` VALUES (40,'Cayman Island','ky',41,'ky','');
INSERT INTO `countries` VALUES (41,'Central African Republic','cf',42,'cf','');
INSERT INTO `countries` VALUES (42,'Chad','td',43,'td','');
INSERT INTO `countries` VALUES (43,'Chile','cl',44,'cl','');
INSERT INTO `countries` VALUES (44,'China','cn',45,'cn','');
INSERT INTO `countries` VALUES (45,'Christmas Island','cx',46,'cx','');
INSERT INTO `countries` VALUES (46,'Cocos (Keeling) Island','cc',47,'cc','');
INSERT INTO `countries` VALUES (47,'Colombia','co',48,'co','');
INSERT INTO `countries` VALUES (48,'Comoros','km',49,'km','');
INSERT INTO `countries` VALUES (49,'Congo','cg',50,'cg','');
INSERT INTO `countries` VALUES (50,'Cook Island','ck',51,'ck','');
INSERT INTO `countries` VALUES (51,'Costa Rica','cr',52,'cr','');
INSERT INTO `countries` VALUES (52,'Hrvatska (Croatia)','hr',1,'hr','');
INSERT INTO `countries` VALUES (53,'Cuba','cu',54,'cu','');
INSERT INTO `countries` VALUES (54,'Cyprus','cy',55,'cy','');
INSERT INTO `countries` VALUES (55,'Czech Republic','cz',56,'cz','');
INSERT INTO `countries` VALUES (56,'Denmark','dk',57,'dk','');
INSERT INTO `countries` VALUES (57,'Djibouti','dj',58,'dj','');
INSERT INTO `countries` VALUES (58,'Dominica','dm',59,'dm','');
INSERT INTO `countries` VALUES (59,'Dominican Republic','do',60,'do','');
INSERT INTO `countries` VALUES (61,'Ecuador','ec',62,'ec','');
INSERT INTO `countries` VALUES (62,'Egypt','eg',63,'eg','');
INSERT INTO `countries` VALUES (63,'El Salvador','sv',64,'sv','');
INSERT INTO `countries` VALUES (64,'Equatorial Guinea','gq',65,'gq','');
INSERT INTO `countries` VALUES (65,'Eritrea','er',66,'er','');
INSERT INTO `countries` VALUES (66,'Estonia','ee',67,'ee','');
INSERT INTO `countries` VALUES (67,'Ethiopia','et',68,'et','');
INSERT INTO `countries` VALUES (68,'Falkland Islands (Malvinas)','fk',69,'fk','');
INSERT INTO `countries` VALUES (69,'Faroe Islands','fo',70,'fo','');
INSERT INTO `countries` VALUES (70,'Fiji','fj',71,'fj','');
INSERT INTO `countries` VALUES (71,'Finland','fi',72,'fi','');
INSERT INTO `countries` VALUES (72,'France','fr',73,'fr','');
INSERT INTO `countries` VALUES (74,'French Guiana','gf',75,'gf','');
INSERT INTO `countries` VALUES (75,'French Polynesia','pf',76,'pf','');
INSERT INTO `countries` VALUES (76,'French Southern Territories','tf',77,'tf','');
INSERT INTO `countries` VALUES (77,'Gabon','ga',78,'ga','');
INSERT INTO `countries` VALUES (78,'Gambia','gm',79,'gm','');
INSERT INTO `countries` VALUES (79,'Georgia','ge',80,'ge','');
INSERT INTO `countries` VALUES (80,'Germany','de',81,'de','');
INSERT INTO `countries` VALUES (81,'Ghana','gh',82,'gh','');
INSERT INTO `countries` VALUES (82,'Gibraltar','gi',83,'gi','');
INSERT INTO `countries` VALUES (83,'Greece','gr',84,'gr','');
INSERT INTO `countries` VALUES (84,'Greenland','gl',85,'gl','');
INSERT INTO `countries` VALUES (85,'Grenada','gd',86,'gd','');
INSERT INTO `countries` VALUES (86,'Guadeloupe','gp',87,'gp','');
INSERT INTO `countries` VALUES (87,'Guam','gu',88,'gu','');
INSERT INTO `countries` VALUES (88,'Guatemala','gt',89,'gt','');
INSERT INTO `countries` VALUES (89,'Guinea','gn',90,'gn','');
INSERT INTO `countries` VALUES (90,'Guinea Bissau','gw',91,'gw','');
INSERT INTO `countries` VALUES (91,'Guyana','gy',92,'gy','');
INSERT INTO `countries` VALUES (92,'Haiti','ht',93,'ht','');
INSERT INTO `countries` VALUES (93,'Heard Island and Mc Donald Islands','hm',94,'hm','');
INSERT INTO `countries` VALUES (94,'Honduras','hn',95,'hn','');
INSERT INTO `countries` VALUES (95,'Hong Kong','hk',96,'hk','');
INSERT INTO `countries` VALUES (96,'Hungary','hu',97,'hu','');
INSERT INTO `countries` VALUES (97,'Iceland','is',98,'is','');
INSERT INTO `countries` VALUES (98,'India','in',99,'in','');
INSERT INTO `countries` VALUES (99,'Indonesia','id',100,'id','');
INSERT INTO `countries` VALUES (100,'Iran (Islamic republic of)','ir',101,'ir','');
INSERT INTO `countries` VALUES (101,'Iraq','iq',102,'iq','');
INSERT INTO `countries` VALUES (102,'Ireland','ie',103,'ie','');
INSERT INTO `countries` VALUES (103,'Israel','il',104,'il','');
INSERT INTO `countries` VALUES (104,'Italy','it',105,'it','');
INSERT INTO `countries` VALUES (106,'Jamaica','jm',107,'jm','');
INSERT INTO `countries` VALUES (107,'Japan','jp',108,'jp','');
INSERT INTO `countries` VALUES (108,'Jordan','jo',109,'jo','');
INSERT INTO `countries` VALUES (109,'Kazakhstan','kz',110,'kz','');
INSERT INTO `countries` VALUES (110,'Kenya','ke',111,'ke','');
INSERT INTO `countries` VALUES (111,'Kiribati','ki',112,'ki','');
INSERT INTO `countries` VALUES (112,'Korea, Democratic people´s Republic of','kp',113,'kp','');
INSERT INTO `countries` VALUES (113,'Korea, Republic of','kr',114,'kr','');
INSERT INTO `countries` VALUES (114,'Kuwait','kw',115,'kw','');
INSERT INTO `countries` VALUES (115,'Kyrgyzstan','kg',116,'kg','');
INSERT INTO `countries` VALUES (116,'Lao people´s democratic republic','la',117,'la','');
INSERT INTO `countries` VALUES (117,'Latvia','lv',118,'lv','');
INSERT INTO `countries` VALUES (118,'Lebanon','lb',119,'lb','');
INSERT INTO `countries` VALUES (119,'Lesotho','ls',120,'ls','');
INSERT INTO `countries` VALUES (120,'Liberia','lr',121,'lr','');
INSERT INTO `countries` VALUES (121,'Libyan Arab Jamahiriya','ly',122,'ly','');
INSERT INTO `countries` VALUES (122,'Liechtenstein','li',123,'li','');
INSERT INTO `countries` VALUES (123,'Lithuania','lt',124,'lt','');
INSERT INTO `countries` VALUES (124,'Luxembourg','lu',125,'lu','');
INSERT INTO `countries` VALUES (125,'Macau','mo',126,'mo','');
INSERT INTO `countries` VALUES (126,'Madagascar','mg',127,'mg','');
INSERT INTO `countries` VALUES (127,'Malawi','mw',128,'mw','');
INSERT INTO `countries` VALUES (128,'Malaysia','my',129,'my','');
INSERT INTO `countries` VALUES (129,'Maldives','mv',130,'mv','');
INSERT INTO `countries` VALUES (130,'Mali','ml',131,'ml','');
INSERT INTO `countries` VALUES (131,'Malta','mt',132,'mt','');
INSERT INTO `countries` VALUES (132,'Marshall islands','mh',133,'mh','');
INSERT INTO `countries` VALUES (133,'Martinique','mq',134,'mq','');
INSERT INTO `countries` VALUES (134,'Mauritania','mr',135,'mr','');
INSERT INTO `countries` VALUES (135,'Mayotte','yt',136,'yt','');
INSERT INTO `countries` VALUES (136,'Mexico','mx',137,'mx','');
INSERT INTO `countries` VALUES (137,'Micronesia (Federated States of)','fm',138,'fm','');
INSERT INTO `countries` VALUES (138,'Moldova, Republic of','md',139,'md','');
INSERT INTO `countries` VALUES (139,'Monaco','mc',140,'mc','');
INSERT INTO `countries` VALUES (140,'Mongolia','mn',141,'mn','');
INSERT INTO `countries` VALUES (141,'Monserrat','ms',142,'ms','');
INSERT INTO `countries` VALUES (142,'Morocco','ma',143,'ma','');
INSERT INTO `countries` VALUES (143,'Mozambigue','mz',144,'mz','');
INSERT INTO `countries` VALUES (144,'Myanmar','mm',145,'mm','');
INSERT INTO `countries` VALUES (145,'Namibia','na',146,'na','');
INSERT INTO `countries` VALUES (146,'Nauru','nr',147,'nr','');
INSERT INTO `countries` VALUES (147,'Nepal','np',148,'np','');
INSERT INTO `countries` VALUES (148,'Netherlands','nl',149,'nl','');
INSERT INTO `countries` VALUES (149,'Netherlands Antilles','an',150,'an','');
INSERT INTO `countries` VALUES (150,'New Caledonia','nc',151,'nc','');
INSERT INTO `countries` VALUES (151,'New Zealand','nz',152,'nz','');
INSERT INTO `countries` VALUES (152,'Nicaragua','ni',153,'ni','');
INSERT INTO `countries` VALUES (153,'Niger','ne',154,'ne','');
INSERT INTO `countries` VALUES (154,'Nigeria','ng',155,'ng','');
INSERT INTO `countries` VALUES (155,'Niue','nu',156,'nu','');
INSERT INTO `countries` VALUES (156,'Norfolk Islands','nf',157,'nf','');
INSERT INTO `countries` VALUES (157,'Northern Mariana Islands','mp',158,'mp','');
INSERT INTO `countries` VALUES (158,'Norway','no',159,'no','');
INSERT INTO `countries` VALUES (159,'Oman','om',160,'om','');
INSERT INTO `countries` VALUES (160,'Pakistan','pk',161,'pk','');
INSERT INTO `countries` VALUES (161,'Palau','pw',162,'pw','');
INSERT INTO `countries` VALUES (162,'Panama','pa',163,'pa','');
INSERT INTO `countries` VALUES (163,'Papua New Guinea','pg',164,'pg','');
INSERT INTO `countries` VALUES (164,'Paraguay','py',165,'py','');
INSERT INTO `countries` VALUES (165,'Peru','pe',166,'pe','');
INSERT INTO `countries` VALUES (166,'Philippines','ph',167,'ph','');
INSERT INTO `countries` VALUES (167,'Pitcairn','pn',168,'pn','');
INSERT INTO `countries` VALUES (168,'Poland','pl',169,'pl','');
INSERT INTO `countries` VALUES (169,'Portugal','pt',170,'pt','');
INSERT INTO `countries` VALUES (170,'Puerto Rico','pr',171,'pr','');
INSERT INTO `countries` VALUES (171,'Qatar','qa',172,'qa','');
INSERT INTO `countries` VALUES (172,'Reunion','re',173,'re','');
INSERT INTO `countries` VALUES (173,'Romania','ro',174,'ro','');
INSERT INTO `countries` VALUES (174,'Russia Federation','ru',175,'ru','');
INSERT INTO `countries` VALUES (175,'Rwanda','rw',176,'rw','');
INSERT INTO `countries` VALUES (176,'Saint Helena','sh',177,'sh','');
INSERT INTO `countries` VALUES (177,'Saint Kitts and Nevis','kn',178,'kn','');
INSERT INTO `countries` VALUES (178,'Saint Lucia','lc',179,'lc','');
INSERT INTO `countries` VALUES (179,'Saint Pierre et Miquelon','pm',180,'pm','');
INSERT INTO `countries` VALUES (180,'Saint Vincent and the Grenadines','vc',181,'vc','');
INSERT INTO `countries` VALUES (181,'Samoa','ws',182,'ws','');
INSERT INTO `countries` VALUES (182,'San Marino','sm',183,'sm','');
INSERT INTO `countries` VALUES (183,'Sao Tome and Principe','st',184,'st','');
INSERT INTO `countries` VALUES (184,'Saudi Arabia','sa',185,'sa','');
INSERT INTO `countries` VALUES (185,'Senegal','sn',186,'sn','');
INSERT INTO `countries` VALUES (186,'Serbia','rs',187,'rs','');
INSERT INTO `countries` VALUES (187,'Seychelles','sc',188,'sc','');
INSERT INTO `countries` VALUES (188,'Sierra Leone','sl',189,'sl','');
INSERT INTO `countries` VALUES (189,'Singapore','sg',190,'sg','');
INSERT INTO `countries` VALUES (190,'Slovakia','sk',191,'sk','');
INSERT INTO `countries` VALUES (191,'Slovenia','si',192,'si','');
INSERT INTO `countries` VALUES (192,'Solomon Islands','sb',193,'sb','');
INSERT INTO `countries` VALUES (193,'Somalia','so',194,'so','');
INSERT INTO `countries` VALUES (194,'South Africa','za',195,'za','');
INSERT INTO `countries` VALUES (195,'Spain','es',196,'es','');
INSERT INTO `countries` VALUES (196,'Sri Lanka','lk',197,'lk','');
INSERT INTO `countries` VALUES (197,'Sudan','sd',198,'sd','');
INSERT INTO `countries` VALUES (198,'Suriname','sr',199,'sr','');
INSERT INTO `countries` VALUES (199,'Svalbard and Jan Mayen','sj',200,'sj','');
INSERT INTO `countries` VALUES (200,'Swaziland','sz',201,'sz','');
INSERT INTO `countries` VALUES (201,'Sweden','se',202,'se','');
INSERT INTO `countries` VALUES (202,'Switzerland','ch',203,'ch','');
INSERT INTO `countries` VALUES (203,'Syria Arab Republic','sy',204,'sy','');
INSERT INTO `countries` VALUES (204,'Taiwan, Province of China','tw',205,'tw','');
INSERT INTO `countries` VALUES (205,'Tajikistan','tj',206,'tj','');
INSERT INTO `countries` VALUES (206,'Tanzania, United Republic Of','tz',207,'tz','');
INSERT INTO `countries` VALUES (207,'Thailand','th',208,'th','');
INSERT INTO `countries` VALUES (208,'Togo','tg',209,'tg','');
INSERT INTO `countries` VALUES (209,'Tokelau','tk',210,'tk','');
INSERT INTO `countries` VALUES (210,'Tonga','to',211,'to','');
INSERT INTO `countries` VALUES (211,'Trinidad and Tobago','tt',212,'tt','');
INSERT INTO `countries` VALUES (212,'Tunisia','tn',213,'tn','');
INSERT INTO `countries` VALUES (213,'Turkey','tr',214,'tr','');
INSERT INTO `countries` VALUES (214,'Turkmenistan','tm',215,'tm','');
INSERT INTO `countries` VALUES (215,'Turks and Caicos Islands','tc',216,'tc','');
INSERT INTO `countries` VALUES (216,'Tuvalu','tv',217,'tv','');
INSERT INTO `countries` VALUES (217,'Uganda','ug',218,'ug','');
INSERT INTO `countries` VALUES (218,'Ukraine','ua',219,'ua','');
INSERT INTO `countries` VALUES (219,'United Arab Emirates','ae',220,'ae','');
INSERT INTO `countries` VALUES (220,'United Kingdom','gb',221,'gb','');
INSERT INTO `countries` VALUES (221,'United States','us',222,'us','');
INSERT INTO `countries` VALUES (222,'United States Minor Outlying Islands','um',223,'um','');
INSERT INTO `countries` VALUES (223,'Uruguay','uy',224,'uy','');
INSERT INTO `countries` VALUES (224,'Uzbekistan','uz',225,'uz','');
INSERT INTO `countries` VALUES (225,'Vanuatu','vu',226,'vu','');
INSERT INTO `countries` VALUES (226,'Vatican City State (Holy See)','va',227,'va','');
INSERT INTO `countries` VALUES (227,'Venezuela','ve',228,'ve','');
INSERT INTO `countries` VALUES (228,'Vietnam','vn',229,'vn','');
INSERT INTO `countries` VALUES (229,'Virgin Islands (British)','vg',230,'vg','');
INSERT INTO `countries` VALUES (230,'Virgin Islands (U.S.)','vi',231,'vi','');
INSERT INTO `countries` VALUES (231,'Wallis and Futuna Islands','wf',232,'wf','');
INSERT INTO `countries` VALUES (232,'Western Sahara','eh',233,'eh','');
INSERT INTO `countries` VALUES (233,'Yemen','ye',234,'ye','');
INSERT INTO `countries` VALUES (236,'Zambia','zm',237,'zm','');
INSERT INTO `countries` VALUES (237,'Zimbabwe','zw',238,'zw','');
INSERT INTO `countries` VALUES (238,'Aland Islands','ax',240,'ax','');
INSERT INTO `countries` VALUES (239,'Confo, The Democratic Republic of thec','cd',240,'cd','');
INSERT INTO `countries` VALUES (240,'Cote D\'ivoire','ci',240,'ci','');
INSERT INTO `countries` VALUES (241,'Guernsey','gg',240,'gg','');
INSERT INTO `countries` VALUES (243,'Isle of Man','im',240,'im','');
INSERT INTO `countries` VALUES (244,'Jersey','je',240,'je','');
INSERT INTO `countries` VALUES (245,'Macedonia (The Former Yugoslav Republic of)','mk',240,'mk','');
INSERT INTO `countries` VALUES (246,'Mauritius','mu',240,'mu','');
INSERT INTO `countries` VALUES (247,'Montenegro','me',240,'me','');
INSERT INTO `countries` VALUES (248,'Palestinian Territory, Occupied','ps',240,'ps','');
INSERT INTO `countries` VALUES (249,'Saint Barthelemy','bl',240,'bl','');
INSERT INTO `countries` VALUES (250,'Saint Martin','mf',240,'mf','');
INSERT INTO `countries` VALUES (251,'South Georgia and the South Sandwich Islands','gs',240,'gs','');
INSERT INTO `countries` VALUES (252,'Timor-Leste','tl',240,'tl','');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table damages
#

DROP TABLE IF EXISTS `damages`;
CREATE TABLE `damages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `vehicle_id` int(11) NOT NULL DEFAULT '0',
  `vehicle_km` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `date` date DEFAULT NULL,
  `where` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table damages
#
LOCK TABLES `damages` WRITE;
/*!40000 ALTER TABLE `damages` DISABLE KEYS */;

INSERT INTO `damages` VALUES (1,'2015-12-18 12:12:58','2016-04-06 09:56:21','Oštećenje desne strane',7,'250000','2015-07-22','Novalje','1','U najmu kod KUNEŠTE oštećena desna stranica (30cm ogrebotina)','- prijavljeno kasku\r\n- naplaćena franšiza 1.250,00kuna\r\n- popravak nakon povratka iz Zagreb Montaže','');
INSERT INTO `damages` VALUES (2,'2015-12-18 12:15:51','2016-01-18 08:40:28','Desna stranica',4,'201003','2015-10-06','Zagreb','2','Oštećena desna stranica kod NEKOM OBRT\r\n','- prijavljeno po kasko\r\n- franšizu prebili popravkom kuplunga u Šibeniku iz 7.mj.2015\r\n- popravak dogovoren u Baotiću kada se vrati vozilo iz najma\r\n- naknadno ćemo urediti sitna oštećenje na ljevoj stranici\r\npreko limara iz Baotića 22.12.2015 kombi uzeo Mario limar\r\nnjegov posao 1000kuna(NEKOM PLATIO 3000kn)\r\n- u Baotiću ostavljen na popravku 5.1.2016','');
INSERT INTO `damages` VALUES (3,'2016-01-28 09:47:03','2016-02-22 14:02:59','Desna vrata',11,'3400','2016-01-27','Gajec, Sesvete','2','- Kod ulaska u dvoriste ostecena desna klizna vrata\r\n','- naplacena fransiza 1000kn\r\n- prijavljeno po kasku u Baoticu\r\n- broj procjene KA01046/16/1\r\n- ostavljen kombi na popravku','68,69');
INSERT INTO `damages` VALUES (4,'2016-03-07 12:25:17','2016-03-25 12:23:54','Ogrebotina',4,'','2016-03-07','Zagreb-Skladište LAURA MONTAŽA','2','Laura Montaža u skladištu ogrebala desnu stranu na paleti.','Popravljeno','72,73');
INSERT INTO `damages` VALUES (5,'2016-03-21 08:12:05','2016-03-21 16:50:01','Udubina',5,'','2016-03-21','Karlovac','2','Udubina na vratima 0.5cm','Mario pooravlja za 100kn i ako treba sitnice polirati.','');
INSERT INTO `damages` VALUES (6,'2016-03-25 12:07:05','2016-03-26 10:27:36','Desna zadnja strana',6,'','2016-03-25','Dubrovnik','2','Sanitaria Dental u Dubravniku zapela za kontenjer sa zadnjom desnom stranom. Plastika i ogrebotina.\r\n','Naplata fransize 1250kuna sanitaria dental\r\n- popravlja Mario','79');
INSERT INTO `damages` VALUES (8,'2016-04-06 09:54:27','2016-04-15 08:11:39','Šteta Miljenko Sladović',5,'104102','2016-04-06','Brisel','1','- prednji branik i blatobran\r\n- zadnja ljeva stranica','- naplaćeno 2000kuna šteta\r\n- Mario Bećaj će ga popravljati 1500kn\r\n- vozilo dano 11.04 na popravak\r\n- dio porpravljen ostalo lakiranje 18.04','82,83');
INSERT INTO `damages` VALUES (9,'2016-04-07 10:02:06','2016-04-28 08:27:59','Ljevi far',6,'','2016-04-07','Francuska, Interier Mont','2','Ljevim farom udarili ženu ispred sebe\r\nCroatia osiguranje skida popravak sa police','Placaju far i popravak direktno Mariju','84,85');
/*!40000 ALTER TABLE `damages` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table documents
#

DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uploaded_by` int(10) unsigned NOT NULL DEFAULT '0',
  `shared` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ext` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `size` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table documents
#
LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;

INSERT INTO `documents` VALUES (1,'2015-12-15 09:53:11','2015-12-15 09:53:11',1,0,'Capture.PNG','PNG',12340,'/files/Capture.PNG');
INSERT INTO `documents` VALUES (2,'2015-12-21 08:08:07','2015-12-21 08:08:07',2,0,'OTPLATNI_PLAN_Master_RI-815-UA.pdf','pdf',91041,'/files/OTPLATNI_PLAN_Master_RI-815-UA.pdf');
INSERT INTO `documents` VALUES (3,'2015-12-21 08:08:10','2015-12-21 08:08:10',2,0,'AO_2015_RI-815-UA.pdf','pdf',139541,'/files/AO_2015_RI-815-UA.pdf');
INSERT INTO `documents` VALUES (4,'2015-12-21 08:08:25','2015-12-21 08:08:25',2,0,'KASKO_2015_RI-815-UA.pdf','pdf',1311459,'/files/KASKO_2015_RI-815-UA.pdf');
INSERT INTO `documents` VALUES (5,'2015-12-21 08:59:11','2015-12-21 08:59:11',2,0,'AO_RI-629-UP_2015.pdf','pdf',133763,'/files/AO_RI-629-UP_2015.pdf');
INSERT INTO `documents` VALUES (6,'2015-12-21 08:59:12','2015-12-21 08:59:12',2,0,'OTPLATNI_PLAN__Master_RI-629-UP.pdf','pdf',72249,'/files/OTPLATNI_PLAN__Master_RI-629-UP.pdf');
INSERT INTO `documents` VALUES (7,'2015-12-21 08:59:17','2015-12-21 08:59:17',2,0,'KASKO_2015.pdf','pdf',1326561,'/files/KASKO_2015.pdf');
INSERT INTO `documents` VALUES (8,'2015-12-21 10:20:24','2015-12-21 10:20:24',1,0,'unnamed.png','png',230909,'/files/unnamed.png');
INSERT INTO `documents` VALUES (9,'2015-12-21 11:00:38','2015-12-21 11:00:38',2,0,'KASKO_2015.pdf','pdf',151863,'/files/KASKO_2015.pdf');
INSERT INTO `documents` VALUES (10,'2015-12-21 11:00:38','2015-12-21 11:00:38',2,0,'OTPLATNI_PLAN__Master_RI-838-VH.pdf','pdf',83376,'/files/OTPLATNI_PLAN__Master_RI-838-VH.pdf');
INSERT INTO `documents` VALUES (11,'2015-12-21 11:07:15','2015-12-21 11:07:15',2,0,'OTPLATNI_PLAN__Master_RI-838-VH.pdf','pdf',83376,'/files/OTPLATNI_PLAN__Master_RI-838-VH.pdf');
INSERT INTO `documents` VALUES (12,'2015-12-21 11:15:07','2015-12-21 11:15:07',2,0,'AO_2015.pdf','pdf',141470,'/files/AO_2015.pdf');
INSERT INTO `documents` VALUES (13,'2015-12-21 11:15:07','2015-12-21 11:15:07',2,0,'OTPLATNI_PLAN__Vivaro_RI-165-UA.pdf','pdf',92603,'/files/OTPLATNI_PLAN__Vivaro_RI-165-UA.pdf');
INSERT INTO `documents` VALUES (14,'2015-12-21 11:15:54','2015-12-21 11:15:54',2,0,'RI-165-UA.doc','doc',172544,'/files/RI-165-UA.doc');
INSERT INTO `documents` VALUES (15,'2015-12-21 20:29:16','2015-12-21 20:29:16',2,0,'RI-815-UA_Prometna.compressed.pdf','pdf',432252,'/files/RI-815-UA_Prometna.compressed.pdf');
INSERT INTO `documents` VALUES (16,'2015-12-21 20:29:46','2015-12-21 20:29:46',2,0,'AO_2015_RI-815-UA.pdf','pdf',139541,'/files/AO_2015_RI-815-UA.pdf');
INSERT INTO `documents` VALUES (17,'2015-12-21 20:29:46','2015-12-21 20:29:46',2,0,'OTPLATNI_PLAN_Master_RI-815-UA.pdf','pdf',91041,'/files/OTPLATNI_PLAN_Master_RI-815-UA.pdf');
INSERT INTO `documents` VALUES (18,'2015-12-21 20:29:58','2015-12-21 20:29:58',2,0,'KASKO_2015_RI-815-UA.pdf','pdf',1311459,'/files/KASKO_2015_RI-815-UA.pdf');
INSERT INTO `documents` VALUES (19,'2015-12-21 20:33:16','2015-12-21 20:33:16',2,0,'OTPLATNI_PLAN__Master_RI-629-UP.pdf','pdf',72249,'/files/OTPLATNI_PLAN__Master_RI-629-UP.pdf');
INSERT INTO `documents` VALUES (20,'2015-12-21 20:33:18','2015-12-21 20:33:18',2,0,'AO_RI-629-UP_2015.pdf','pdf',133763,'/files/AO_RI-629-UP_2015.pdf');
INSERT INTO `documents` VALUES (21,'2015-12-21 20:33:36','2015-12-21 20:33:36',2,0,'KASKO_2015.compressed.pdf','pdf',140798,'/files/KASKO_2015.compressed.pdf');
INSERT INTO `documents` VALUES (22,'2015-12-21 20:33:40','2015-12-21 20:33:40',2,0,'RI-629-UP_Prometna.compressed.pdf','pdf',420331,'/files/RI-629-UP_Prometna.compressed.pdf');
INSERT INTO `documents` VALUES (23,'2015-12-21 20:36:20','2015-12-21 20:36:20',2,0,'RI-654-UP_Prometna.compressed.pdf','pdf',265239,'/files/RI-654-UP_Prometna.compressed.pdf');
INSERT INTO `documents` VALUES (24,'2015-12-21 20:36:38','2015-12-21 20:36:38',2,0,'OTPLATNI_PLAN__Ducato_RI-654-UP.pdf','pdf',90383,'/files/OTPLATNI_PLAN__Ducato_RI-654-UP.pdf');
INSERT INTO `documents` VALUES (25,'2015-12-21 20:36:40','2015-12-21 20:36:40',2,0,'AO_2015.pdf','pdf',137891,'/files/AO_2015.pdf');
INSERT INTO `documents` VALUES (26,'2015-12-21 20:36:40','2015-12-21 20:36:40',2,0,'KASKO_2014.pdf','pdf',119596,'/files/KASKO_2014.pdf');
INSERT INTO `documents` VALUES (27,'2015-12-21 20:40:55','2015-12-21 20:40:55',2,0,'KASKO_2015.pdf','pdf',151863,'/files/KASKO_2015.pdf');
INSERT INTO `documents` VALUES (28,'2015-12-21 20:40:55','2015-12-21 20:40:55',2,0,'OTPLATNI_PLAN__Master_RI-838-VH.pdf','pdf',83376,'/files/OTPLATNI_PLAN__Master_RI-838-VH.pdf');
INSERT INTO `documents` VALUES (29,'2015-12-21 20:41:16','2015-12-21 20:41:16',2,0,'KASKO_2015.compressed.pdf','pdf',140798,'/files/KASKO_2015.compressed.pdf');
INSERT INTO `documents` VALUES (30,'2015-12-21 20:41:20','2015-12-21 20:41:20',2,0,'RI-838-VH_Prometna.compressed.pdf','pdf',474585,'/files/RI-838-VH_Prometna.compressed.pdf');
INSERT INTO `documents` VALUES (31,'2015-12-21 20:42:27','2015-12-21 20:42:27',2,0,'RI-838-VH_AO_2015.compressed.pdf','pdf',97324,'/files/RI-838-VH_AO_2015.compressed.pdf');
INSERT INTO `documents` VALUES (32,'2015-12-21 20:47:06','2015-12-21 20:47:06',2,0,'Ugovor_o_leasingu.pdf','pdf',78743,'/files/Ugovor_o_leasingu.pdf');
INSERT INTO `documents` VALUES (33,'2015-12-21 20:47:12','2015-12-21 20:47:12',2,0,'AO_2015.pdf','pdf',760326,'/files/AO_2015.pdf');
INSERT INTO `documents` VALUES (34,'2015-12-21 20:47:28','2015-12-21 20:47:28',2,0,'Kasko_2015.compressed.pdf','pdf',297187,'/files/Kasko_2015.compressed.pdf');
INSERT INTO `documents` VALUES (35,'2015-12-21 20:47:29','2015-12-21 20:47:29',2,0,'Prometna_RI-540-VN.compressed.pdf','pdf',348275,'/files/Prometna_RI-540-VN.compressed.pdf');
INSERT INTO `documents` VALUES (36,'2015-12-21 20:52:44','2015-12-21 20:52:44',2,0,'KASKO_2015_RI-121-SR.compressed.pdf','pdf',130576,'/files/KASKO_2015_RI-121-SR.compressed.pdf');
INSERT INTO `documents` VALUES (37,'2015-12-21 20:52:44','2015-12-21 20:52:44',2,0,'Prometna_RI-121-SR.compressed.pdf','pdf',171871,'/files/Prometna_RI-121-SR.compressed.pdf');
INSERT INTO `documents` VALUES (38,'2015-12-21 20:53:01','2015-12-21 20:53:01',2,0,'AO_2015_RI-121-SR.pdf','pdf',139647,'/files/AO_2015_RI-121-SR.pdf');
INSERT INTO `documents` VALUES (39,'2015-12-21 20:53:01','2015-12-21 20:53:01',2,0,'OTPLATNI_PLAN__Vivaro_RI-121-SR.pdf','pdf',92350,'/files/OTPLATNI_PLAN__Vivaro_RI-121-SR.pdf');
INSERT INTO `documents` VALUES (40,'2015-12-21 20:57:08','2015-12-21 20:57:08',2,0,'OTPLATNI_PLAN__Vivaro_RI-165-UA.pdf','pdf',92603,'/files/OTPLATNI_PLAN__Vivaro_RI-165-UA.pdf');
INSERT INTO `documents` VALUES (41,'2015-12-21 20:57:08','2015-12-21 20:57:08',2,0,'AO_2015.pdf','pdf',141470,'/files/AO_2015.pdf');
INSERT INTO `documents` VALUES (42,'2015-12-21 20:57:59','2015-12-21 20:57:59',2,0,'Prometna_RI-165-UA.compressed.pdf','pdf',591545,'/files/Prometna_RI-165-UA.compressed.pdf');
INSERT INTO `documents` VALUES (43,'2015-12-21 21:06:16','2015-12-21 21:06:16',2,0,'AO_2015_RI-668-TC.compressed.pdf','pdf',96294,'/files/AO_2015_RI-668-TC.compressed.pdf');
INSERT INTO `documents` VALUES (44,'2015-12-21 21:06:20','2015-12-21 21:06:20',2,0,'KASKO_2014.compressed.pdf','pdf',249963,'/files/KASKO_2014.compressed.pdf');
INSERT INTO `documents` VALUES (45,'2015-12-21 21:06:22','2015-12-21 21:06:22',2,0,'Prometna_RI-668-TC.compressed.pdf','pdf',346400,'/files/Prometna_RI-668-TC.compressed.pdf');
INSERT INTO `documents` VALUES (46,'2015-12-21 21:06:39','2015-12-21 21:06:39',2,0,'OTPLATNI_PLAN__Transporter_RI-668-TC.pdf','pdf',90119,'/files/OTPLATNI_PLAN__Transporter_RI-668-TC.pdf');
INSERT INTO `documents` VALUES (47,'2015-12-22 08:40:04','2015-12-22 08:40:04',1,0,'data.xls','xls',6329856,'/files/data.xls');
INSERT INTO `documents` VALUES (48,'2015-12-22 08:40:29','2015-12-22 08:40:29',2,0,'AO_2015_RI-353-UE.compressed.pdf','pdf',108136,'/files/AO_2015_RI-353-UE.compressed.pdf');
INSERT INTO `documents` VALUES (49,'2015-12-22 08:40:32','2015-12-22 08:40:32',2,0,'Kasko_2015_sporazum_RI-353-UE.compressed.pdf','pdf',289024,'/files/Kasko_2015_sporazum_RI-353-UE.compressed.pdf');
INSERT INTO `documents` VALUES (50,'2015-12-22 08:40:33','2015-12-22 08:40:33',2,0,'Prometna_dozvola_RI-353-UE.compressed.pdf','pdf',279724,'/files/Prometna_dozvola_RI-353-UE.compressed.pdf');
INSERT INTO `documents` VALUES (51,'2015-12-22 08:40:48','2015-12-22 08:40:48',2,0,'Otplatni_plan_RI-353-UE.pdf','pdf',84251,'/files/Otplatni_plan_RI-353-UE.pdf');
INSERT INTO `documents` VALUES (52,'2015-12-22 08:55:53','2015-12-22 08:55:53',2,0,'KASKO_2015_RI-107-UO.compressed.pdf','pdf',212143,'/files/KASKO_2015_RI-107-UO.compressed.pdf');
INSERT INTO `documents` VALUES (53,'2015-12-22 08:55:54','2015-12-22 08:55:54',2,0,'Prometna_RI-107-UO.compressed.pdf','pdf',593634,'/files/Prometna_RI-107-UO.compressed.pdf');
INSERT INTO `documents` VALUES (54,'2015-12-22 08:56:27','2015-12-22 08:56:27',2,0,'AO_2015.pdf','pdf',138481,'/files/AO_2015.pdf');
INSERT INTO `documents` VALUES (55,'2015-12-22 08:56:27','2015-12-22 08:56:27',2,0,'OTPLATNI_PLAN_RI-107-UO.pdf','pdf',81948,'/files/OTPLATNI_PLAN_RI-107-UO.pdf');
INSERT INTO `documents` VALUES (56,'2015-12-22 09:18:31','2015-12-22 09:18:31',2,0,'OTPLATNI_PLAN_RI-107-UO.pdf','pdf',81948,'/files/OTPLATNI_PLAN_RI-107-UO.pdf');
INSERT INTO `documents` VALUES (57,'2015-12-22 09:18:32','2015-12-22 09:18:32',2,0,'AO_2015.pdf','pdf',138481,'/files/AO_2015.pdf');
INSERT INTO `documents` VALUES (58,'2015-12-22 09:19:30','2015-12-22 09:19:30',2,0,'Promenta_RI-747-VJ.compressed.pdf','pdf',0,'/files/Promenta_RI-747-VJ.compressed.pdf');
INSERT INTO `documents` VALUES (59,'2015-12-22 09:19:39','2015-12-22 09:19:39',2,0,'KASKO_2015_RI-107-UO.pdf','pdf',1347439,'/files/KASKO_2015_RI-107-UO.pdf');
INSERT INTO `documents` VALUES (60,'2015-12-22 09:19:57','2015-12-22 09:19:57',2,0,'Prometna_RI-107-UO.pdf','pdf',4600786,'/files/Prometna_RI-107-UO.pdf');
INSERT INTO `documents` VALUES (61,'2015-12-23 09:23:16','2015-12-23 09:23:16',2,0,'auftrag_2015005965_1.151222-09143118307.pdf','pdf',87601,'/files/auftrag_2015005965_1.151222-09143118307.pdf');
INSERT INTO `documents` VALUES (62,'2015-12-28 08:22:44','2015-12-28 08:22:44',2,0,'AO_2015_Renault_Trafic.pdf','pdf',4446807,'/files/AO_2015_Renault_Trafic.pdf');
INSERT INTO `documents` VALUES (63,'2015-12-28 08:26:55','2015-12-28 08:26:55',2,0,'Ugovor_leasing_Renault_Trafic_23498.pdf','pdf',6994437,'/files/Ugovor_leasing_Renault_Trafic_23498.pdf');
INSERT INTO `documents` VALUES (64,'2015-12-28 08:31:38','2015-12-28 08:31:38',2,0,'Kasko_2015_Renault_Trafic.pdf','pdf',121362,'/files/Kasko_2015_Renault_Trafic.pdf');
INSERT INTO `documents` VALUES (65,'2015-12-30 12:04:07','2015-12-30 12:04:07',2,0,'Prometna_DA-760-DL.pdf','pdf',6423028,'/files/Prometna_DA-760-DL.pdf');
INSERT INTO `documents` VALUES (66,'2016-01-05 09:15:48','2016-01-05 09:15:48',2,0,'Kasko_2015_RI-838-VH.pdf','pdf',3843461,'/files/Kasko_2015_RI-838-VH.pdf');
INSERT INTO `documents` VALUES (67,'2016-01-28 11:26:29','2016-01-28 11:26:29',1,0,'1453976773959.jpg','jpg',3798445,'/files/1453976773959.jpg');
INSERT INTO `documents` VALUES (68,'2016-01-28 11:27:12','2016-01-28 11:27:12',2,0,'20160128_093308.jpg','jpg',2028583,'/files/20160128_093308.jpg');
INSERT INTO `documents` VALUES (69,'2016-01-29 16:17:08','2016-01-29 16:17:08',2,0,'KA01046_16_-_izvid_stete.pdf','pdf',54745,'/files/KA01046_16_-_izvid_stete.pdf');
INSERT INTO `documents` VALUES (70,'2016-02-02 07:34:28','2016-02-02 07:34:28',2,0,'Otplatni_plan_DA_760_DL.pdf','pdf',1025884,'/files/Otplatni_plan_DA_760_DL.pdf');
INSERT INTO `documents` VALUES (71,'2016-03-01 09:03:08','2016-03-01 09:03:08',2,0,'AO_2016_-_RI-107-UO.pdf','pdf',963634,'/files/AO_2016_-_RI-107-UO.pdf');
INSERT INTO `documents` VALUES (72,'2016-03-07 13:09:32','2016-03-07 13:09:32',2,0,'ducato_2_laura.jpg','jpg',53822,'/files/ducato_2_laura.jpg');
INSERT INTO `documents` VALUES (73,'2016-03-07 13:09:32','2016-03-07 13:09:32',2,0,'Ducato_laura.jpg','jpg',49296,'/files/Ducato_laura.jpg');
INSERT INTO `documents` VALUES (74,'2016-03-15 11:03:59','2016-03-15 11:03:59',2,0,'RI747VJ_2015_Zeleni_karton.PDF','PDF',10954,'/files/RI747VJ_2015_Zeleni_karton.PDF');
INSERT INTO `documents` VALUES (75,'2016-03-15 11:04:00','2016-03-15 11:04:00',2,0,'RI747VJ_AO_2015.pdf','pdf',139608,'/files/RI747VJ_AO_2015.pdf');
INSERT INTO `documents` VALUES (76,'2016-03-15 11:04:45','2016-03-15 11:04:45',2,0,'RI-747-VJ_2015_Kasko.pdf','pdf',1908910,'/files/RI-747-VJ_2015_Kasko.pdf');
INSERT INTO `documents` VALUES (77,'2016-03-15 11:04:47','2016-03-15 11:04:47',2,0,'RI-747-VJ_2015_Kasko.pdf','pdf',1908910,'/files/RI-747-VJ_2015_Kasko.pdf');
INSERT INTO `documents` VALUES (78,'2016-03-22 10:57:36','2016-03-22 10:57:36',2,0,'1458640635846353042578.jpg','jpg',1465838,'/files/1458640635846353042578.jpg');
INSERT INTO `documents` VALUES (79,'2016-03-25 12:06:41','2016-03-25 12:06:41',2,0,'1458903977507-1088647805.jpg','jpg',1479403,'/files/1458903977507-1088647805.jpg');
INSERT INTO `documents` VALUES (80,'2016-03-30 07:31:30','2016-03-30 07:31:30',2,0,'1459315874864-1103857567.jpg','jpg',1135643,'/files/1459315874864-1103857567.jpg');
INSERT INTO `documents` VALUES (81,'2016-04-01 12:06:26','2016-04-01 12:06:26',2,0,'AO_RI-654-UP_2016.pdf','pdf',599172,'/files/AO_RI-654-UP_2016.pdf');
INSERT INTO `documents` VALUES (82,'2016-04-06 09:53:04','2016-04-06 09:53:04',2,0,'steta_2.jpg','jpg',86584,'/files/steta_2.jpg');
INSERT INTO `documents` VALUES (83,'2016-04-06 09:53:09','2016-04-06 09:53:09',2,0,'stets_1.jpg','jpg',314736,'/files/stets_1.jpg');
INSERT INTO `documents` VALUES (84,'2016-04-07 11:35:06','2016-04-07 11:35:06',2,0,'20160407_084308_HDR.jpg','jpg',138745,'/files/20160407_084308_HDR.jpg');
INSERT INTO `documents` VALUES (85,'2016-04-28 08:26:48','2016-04-28 08:26:48',2,0,'d.jpg','jpg',203766,'/files/d.jpg');
INSERT INTO `documents` VALUES (86,'2016-04-28 08:28:32','2016-04-28 08:28:32',2,0,'a.jpg','jpg',361554,'/files/a.jpg');
INSERT INTO `documents` VALUES (87,'2016-05-02 13:21:00','2016-05-02 13:21:00',2,0,'AO_2016_RI-629-UP.pdf','pdf',505048,'/files/AO_2016_RI-629-UP.pdf');
INSERT INTO `documents` VALUES (88,'2016-05-02 13:22:46','2016-05-02 13:22:46',2,0,'AO_2016_RI-540-VN.pdf','pdf',467919,'/files/AO_2016_RI-540-VN.pdf');
INSERT INTO `documents` VALUES (89,'2016-05-04 12:13:45','2016-05-04 12:13:45',2,0,'AO_2016_RI-121-SR.pdf','pdf',520370,'/files/AO_2016_RI-121-SR.pdf');
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table migrations
#

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `migrations_migration_unique` (`migration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table migrations
#
LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` VALUES ('2015_11_03_071430_create_user_groups_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_04_194404_create_users_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_05_071833_create_password_resets_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_05_072457_create_countries_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_155533_create_vehicles_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_155741_create_vehicle_services_tables.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_161025_create_damages_tables.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_164321_create_tasks_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_165212_create_bookings_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_165314_create_contacts_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_14_084224_create_contract_templates_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_14_084433_create_contracts_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_19_091720_create_translation_tags_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_19_091923_create_translations_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_21_143830_add_registration_columns_to_vehicle.php',1);
INSERT INTO `migrations` VALUES ('2015_11_24_171819_add_pp_columns_to_vehicles.php',1);
INSERT INTO `migrations` VALUES ('2015_11_24_172626_add_km_column_to_vehicle_services.php',1);
INSERT INTO `migrations` VALUES ('2015_11_24_175001_add_groups_column_to_contacts.php',1);
INSERT INTO `migrations` VALUES ('2015_11_29_121909_create_documents_table.php',1);
INSERT INTO `migrations` VALUES ('2015_12_01_193345_add_columns_to_vehicles.php',1);
INSERT INTO `migrations` VALUES ('2015_12_09_194008_add_docs_to_damages_and_bookings.php',1);
INSERT INTO `migrations` VALUES ('2015_12_10_091238_create_checklists_table.php',1);
INSERT INTO `migrations` VALUES ('2015_12_15_203702_add_column_km_to_vehicles.php',2);
INSERT INTO `migrations` VALUES ('2015_12_15_205229_add_column_service_km_to_vehicles.php',3);
INSERT INTO `migrations` VALUES ('2015_12_20_131141_switch_docs_and_note_fields.php',4);
INSERT INTO `migrations` VALUES ('2015_12_23_095550_add_extra_fields_to_contacts.php',5);
INSERT INTO `migrations` VALUES ('2015_12_23_102014_add_extra_fields_to_bookings.php',5);
INSERT INTO `migrations` VALUES ('2015_12_24_083322_create_reminders_table.php',6);
INSERT INTO `migrations` VALUES ('2015_12_28_113013_add_column_booking_state.php',6);
INSERT INTO `migrations` VALUES ('2015_12_28_232621_add_columns_to_reminders.php',7);
INSERT INTO `migrations` VALUES ('2015_12_29_115401_add_service_type_to_bookings.php',8);
INSERT INTO `migrations` VALUES ('2015_12_31_093015_add_fields_to_vehicle_services.php',9);
INSERT INTO `migrations` VALUES ('2016_01_04_123959_alter_services_from_to_nullable.php',10);
INSERT INTO `migrations` VALUES ('2016_01_12_121543_add_show_on_calendar_to_vehicles.php',11);
INSERT INTO `migrations` VALUES ('2016_01_19_114804_create_vehicle_history_kms_table.php',12);
INSERT INTO `migrations` VALUES ('2016_02_06_210557_create_audits_table.php',13);
INSERT INTO `migrations` VALUES ('2016_02_19_102325_add_booking_id_to_contracts.php',14);
INSERT INTO `migrations` VALUES ('2016_02_19_195717_add_field_dated_at_to_contracts.php',14);
INSERT INTO `migrations` VALUES ('2016_02_19_202120_remove_client_field_from_contracts.php',14);
INSERT INTO `migrations` VALUES ('2016_03_08_105149_add_tags_to_contacts.php',15);
INSERT INTO `migrations` VALUES ('2016_03_25_094218_create_user_throttle_table.php',16);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table password_resets
#

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  UNIQUE KEY `password_resets_email_unique` (`email`),
  UNIQUE KEY `password_resets_token_unique` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table password_resets
#
LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;

/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table reminders
#

DROP TABLE IF EXISTS `reminders`;
CREATE TABLE `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `assigned_to` int(10) unsigned NOT NULL DEFAULT '0',
  `followers` text COLLATE utf8_unicode_ci NOT NULL,
  `done` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `done_at` datetime NOT NULL,
  `done_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table reminders
#
LOCK TABLES `reminders` WRITE;
/*!40000 ALTER TABLE `reminders` DISABLE KEYS */;

/*!40000 ALTER TABLE `reminders` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table tasks
#

DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start` datetime NOT NULL,
  `due` datetime NOT NULL,
  `finish` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table tasks
#
LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;

/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table translation_tags
#

DROP TABLE IF EXISTS `translation_tags`;
CREATE TABLE `translation_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scope` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table translation_tags
#
LOCK TABLES `translation_tags` WRITE;
/*!40000 ALTER TABLE `translation_tags` DISABLE KEYS */;

INSERT INTO `translation_tags` VALUES (1,'2015-12-15 02:21:15','2015-12-15 02:21:15','','Pretraga');
INSERT INTO `translation_tags` VALUES (2,'2015-12-15 02:21:15','2015-12-15 02:21:15','','Vozilo');
INSERT INTO `translation_tags` VALUES (3,'2015-12-15 02:21:15','2015-12-15 02:21:15','','Per page');
INSERT INTO `translation_tags` VALUES (4,'2015-12-15 02:21:15','2015-12-15 02:21:15','','No matching results found.');
INSERT INTO `translation_tags` VALUES (5,'2015-12-15 02:21:19','2015-12-15 02:21:19','','Po stranici');
INSERT INTO `translation_tags` VALUES (6,'2015-12-15 02:21:36','2015-12-15 02:21:36','','Mjesto');
INSERT INTO `translation_tags` VALUES (7,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Registarska tablica');
INSERT INTO `translation_tags` VALUES (8,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Naziv vozila');
INSERT INTO `translation_tags` VALUES (9,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Marka vozila');
INSERT INTO `translation_tags` VALUES (10,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Model');
INSERT INTO `translation_tags` VALUES (11,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Tip');
INSERT INTO `translation_tags` VALUES (12,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Godina proizvodnje');
INSERT INTO `translation_tags` VALUES (13,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Veličina guma');
INSERT INTO `translation_tags` VALUES (14,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Boja');
INSERT INTO `translation_tags` VALUES (15,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Boja (kod)');
INSERT INTO `translation_tags` VALUES (16,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Broj šasije');
INSERT INTO `translation_tags` VALUES (17,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Partner');
INSERT INTO `translation_tags` VALUES (18,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Leasing kuća');
INSERT INTO `translation_tags` VALUES (19,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Rata leasinga');
INSERT INTO `translation_tags` VALUES (20,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Registriran do');
INSERT INTO `translation_tags` VALUES (21,'2015-12-15 09:00:53','2015-12-15 09:00:53','','AO osiguranje vrijedi do');
INSERT INTO `translation_tags` VALUES (22,'2015-12-15 09:00:53','2015-12-15 09:00:53','','AO osg.kuća');
INSERT INTO `translation_tags` VALUES (23,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Kasko vrijedi do');
INSERT INTO `translation_tags` VALUES (24,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Kasko osg.kuća');
INSERT INTO `translation_tags` VALUES (25,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Periodički pregled vrijedi do');
INSERT INTO `translation_tags` VALUES (26,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Protupožarni aparat vrijedi do');
INSERT INTO `translation_tags` VALUES (27,'2015-12-15 09:00:53','2015-12-15 09:00:53','','Dokumenti');
INSERT INTO `translation_tags` VALUES (28,'2015-12-15 09:20:42','2015-12-15 09:20:42','','Name');
INSERT INTO `translation_tags` VALUES (29,'2015-12-15 09:20:42','2015-12-15 09:20:42','','Status');
INSERT INTO `translation_tags` VALUES (30,'2015-12-15 09:20:42','2015-12-15 09:20:42','','Client');
INSERT INTO `translation_tags` VALUES (31,'2015-12-15 09:20:42','2015-12-15 09:20:42','','Data');
INSERT INTO `translation_tags` VALUES (32,'2015-12-15 09:20:42','2015-12-15 09:20:42','','Pdf');
INSERT INTO `translation_tags` VALUES (33,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Ime');
INSERT INTO `translation_tags` VALUES (34,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Prezime');
INSERT INTO `translation_tags` VALUES (35,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Tvrtka (kratki naziv)');
INSERT INTO `translation_tags` VALUES (36,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Tvrtka (dugi naziv)');
INSERT INTO `translation_tags` VALUES (37,'2015-12-15 09:20:49','2015-12-15 09:20:49','','OIB');
INSERT INTO `translation_tags` VALUES (38,'2015-12-15 09:20:49','2015-12-15 09:20:49','','MB');
INSERT INTO `translation_tags` VALUES (39,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Adresa');
INSERT INTO `translation_tags` VALUES (40,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Adresa 2');
INSERT INTO `translation_tags` VALUES (41,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Poštanski broj');
INSERT INTO `translation_tags` VALUES (42,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Država');
INSERT INTO `translation_tags` VALUES (43,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Tip kontakta');
INSERT INTO `translation_tags` VALUES (44,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Email');
INSERT INTO `translation_tags` VALUES (45,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Mobilni tel.');
INSERT INTO `translation_tags` VALUES (46,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Tel.');
INSERT INTO `translation_tags` VALUES (47,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Fax');
INSERT INTO `translation_tags` VALUES (48,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Web');
INSERT INTO `translation_tags` VALUES (49,'2015-12-15 09:20:49','2015-12-15 09:20:49','','Napomena');
INSERT INTO `translation_tags` VALUES (50,'2015-12-15 09:52:53','2015-12-15 09:52:53','','Tip servisa');
INSERT INTO `translation_tags` VALUES (51,'2015-12-15 09:52:53','2015-12-15 09:52:53','','Početni datum servisa');
INSERT INTO `translation_tags` VALUES (52,'2015-12-15 09:52:53','2015-12-15 09:52:53','','Završni datum servisa');
INSERT INTO `translation_tags` VALUES (53,'2015-12-15 09:52:53','2015-12-15 09:52:53','','Kilometraža');
INSERT INTO `translation_tags` VALUES (54,'2015-12-15 09:52:53','2015-12-15 09:52:53','','Servisni centar');
INSERT INTO `translation_tags` VALUES (55,'2015-12-15 09:52:53','2015-12-15 09:52:53','','Cijena servisa');
INSERT INTO `translation_tags` VALUES (56,'2015-12-15 09:52:53','2015-12-15 09:52:53','','Cijena dijelova');
INSERT INTO `translation_tags` VALUES (57,'2015-12-15 09:54:20','2015-12-15 09:54:20','','Search');
INSERT INTO `translation_tags` VALUES (58,'2015-12-15 09:54:20','2015-12-15 09:54:20','','Usergroup');
INSERT INTO `translation_tags` VALUES (59,'2015-12-15 09:54:28','2015-12-15 09:54:28','','Username');
INSERT INTO `translation_tags` VALUES (60,'2015-12-15 09:54:28','2015-12-15 09:54:28','','Password');
INSERT INTO `translation_tags` VALUES (61,'2015-12-15 09:54:28','2015-12-15 09:54:28','','First Name');
INSERT INTO `translation_tags` VALUES (62,'2015-12-15 09:54:28','2015-12-15 09:54:28','','Last Name');
INSERT INTO `translation_tags` VALUES (63,'2015-12-17 18:37:00','2015-12-17 18:37:00','','Prešao kilometara');
INSERT INTO `translation_tags` VALUES (64,'2015-12-17 18:37:00','2015-12-17 18:37:00','','Redoviti servis svakih (km)');
INSERT INTO `translation_tags` VALUES (65,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Početni datum');
INSERT INTO `translation_tags` VALUES (66,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Početno vrijeme');
INSERT INTO `translation_tags` VALUES (67,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Završni datum');
INSERT INTO `translation_tags` VALUES (68,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Završno vrijeme');
INSERT INTO `translation_tags` VALUES (69,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Početna kilometraža');
INSERT INTO `translation_tags` VALUES (70,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Završna kilometraža');
INSERT INTO `translation_tags` VALUES (71,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Ukupna cijena');
INSERT INTO `translation_tags` VALUES (72,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Klijent');
INSERT INTO `translation_tags` VALUES (73,'2015-12-17 18:41:46','2015-12-17 18:41:46','','Preko koje firme');
INSERT INTO `translation_tags` VALUES (74,'2015-12-17 18:42:25','2015-12-17 18:42:25','','Naslov');
INSERT INTO `translation_tags` VALUES (75,'2015-12-17 18:42:25','2015-12-17 18:42:25','','Datum događaja');
INSERT INTO `translation_tags` VALUES (76,'2015-12-17 18:42:25','2015-12-17 18:42:25','','Mjesto događaja');
INSERT INTO `translation_tags` VALUES (77,'2015-12-17 18:42:25','2015-12-17 18:42:25','','Km na vozilu');
INSERT INTO `translation_tags` VALUES (78,'2015-12-17 18:42:25','2015-12-17 18:42:25','','Opis');
INSERT INTO `translation_tags` VALUES (79,'2015-12-17 18:42:25','2015-12-17 18:42:25','','Tko je oštetio vozilo');
INSERT INTO `translation_tags` VALUES (80,'2015-12-18 12:52:15','2015-12-18 12:52:15','','Tvrtka');
INSERT INTO `translation_tags` VALUES (81,'2015-12-18 12:58:09','2015-12-18 12:58:09','','Kontakt osoba');
INSERT INTO `translation_tags` VALUES (82,'2015-12-18 12:58:10','2015-12-18 12:58:10','','Kontakt telefon');
INSERT INTO `translation_tags` VALUES (83,'2015-12-18 13:00:45','2015-12-18 13:00:45','','Kontakt mobitel');
INSERT INTO `translation_tags` VALUES (84,'2015-12-18 13:05:48','2015-12-18 13:05:48','','Mobitel');
INSERT INTO `translation_tags` VALUES (85,'2015-12-20 13:46:31','2015-12-20 13:46:31','','Language');
INSERT INTO `translation_tags` VALUES (86,'2015-12-20 13:46:31','2015-12-20 13:46:31','','Translation');
INSERT INTO `translation_tags` VALUES (87,'2015-12-20 13:46:32','2015-12-20 13:46:32','','Actions');
INSERT INTO `translation_tags` VALUES (88,'2015-12-20 13:46:48','2015-12-20 13:46:48','','Tag');
INSERT INTO `translation_tags` VALUES (89,'2015-12-20 13:46:49','2015-12-20 13:46:49','','Auto');
INSERT INTO `translation_tags` VALUES (90,'2015-12-21 10:29:54','2015-12-21 10:29:54','','EDIT');
INSERT INTO `translation_tags` VALUES (91,'2015-12-21 10:29:54','2015-12-21 10:29:54','','DELETE');
INSERT INTO `translation_tags` VALUES (92,'2015-12-21 10:49:01','2015-12-21 10:49:01','','Are you sure?');
INSERT INTO `translation_tags` VALUES (93,'2015-12-21 11:43:32','2015-12-21 11:43:32','','Status plaćanja');
INSERT INTO `translation_tags` VALUES (94,'2015-12-21 12:10:20','2015-12-21 12:10:20','','Najmodavac');
INSERT INTO `translation_tags` VALUES (95,'2015-12-21 16:12:46','2015-12-21 16:12:46','','Profile');
INSERT INTO `translation_tags` VALUES (96,'2015-12-21 16:12:46','2015-12-21 16:12:46','','Sign out');
INSERT INTO `translation_tags` VALUES (97,'2015-12-22 08:31:14','2015-12-22 08:31:14','','Save');
INSERT INTO `translation_tags` VALUES (98,'2015-12-22 12:42:28','2015-12-22 12:42:28','','Od datuma');
INSERT INTO `translation_tags` VALUES (99,'2015-12-22 12:42:28','2015-12-22 12:42:28','','Do datuma');
INSERT INTO `translation_tags` VALUES (100,'2015-12-23 11:08:54','2015-12-23 11:08:54','','Vezani dokument');
INSERT INTO `translation_tags` VALUES (101,'2015-12-24 11:25:26','2015-12-24 11:25:26','','Prati datum');
INSERT INTO `translation_tags` VALUES (102,'2015-12-26 10:58:42','2015-12-26 10:58:42','','Upiši pregled');
INSERT INTO `translation_tags` VALUES (103,'2015-12-28 10:52:41','2015-12-28 10:52:41','','Reset filters');
INSERT INTO `translation_tags` VALUES (104,'2015-12-28 12:16:07','2015-12-28 12:16:07','','Uplaćeni iznos');
INSERT INTO `translation_tags` VALUES (105,'2015-12-28 12:16:07','2015-12-28 12:16:07','','Status najma');
INSERT INTO `translation_tags` VALUES (106,'2015-12-29 08:59:22','2015-12-29 08:59:22','','Zapamti provjeru');
INSERT INTO `translation_tags` VALUES (107,'2015-12-29 08:59:22','2015-12-29 08:59:22','','<i class=\'fa fa-save\'></i> Zapamti provjeru');
INSERT INTO `translation_tags` VALUES (108,'2015-12-29 12:39:39','2015-12-29 12:39:39','','Vrsta usluge');
INSERT INTO `translation_tags` VALUES (109,'2015-12-31 10:27:49','2015-12-31 10:27:49','','Status servisa');
INSERT INTO `translation_tags` VALUES (110,'2015-12-31 12:22:52','2015-12-31 12:22:52','','Odvesti na kilometara');
INSERT INTO `translation_tags` VALUES (111,'2016-01-07 10:32:46','2016-01-07 10:32:46','','Title');
INSERT INTO `translation_tags` VALUES (112,'2016-01-07 10:32:46','2016-01-07 10:32:46','','Permissions');
INSERT INTO `translation_tags` VALUES (113,'2016-01-12 13:04:47','2016-01-12 13:04:47','','Prikaži ovo vozilo u rasporedu');
INSERT INTO `translation_tags` VALUES (114,'2016-02-04 14:35:19','2016-02-04 14:35:19','','Tip vozila');
INSERT INTO `translation_tags` VALUES (115,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Datum ugovora');
INSERT INTO `translation_tags` VALUES (116,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Najmoprimac');
INSERT INTO `translation_tags` VALUES (117,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Adresa sjedišta');
INSERT INTO `translation_tags` VALUES (118,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Registracija vozila');
INSERT INTO `translation_tags` VALUES (119,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Datum preuzimanja');
INSERT INTO `translation_tags` VALUES (120,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Vrijeme preuzimanja');
INSERT INTO `translation_tags` VALUES (121,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Datum povratka');
INSERT INTO `translation_tags` VALUES (122,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Vrijeme povratka');
INSERT INTO `translation_tags` VALUES (123,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Cijena po danu');
INSERT INTO `translation_tags` VALUES (124,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Ukupno dana');
INSERT INTO `translation_tags` VALUES (125,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Dnevna kilometraža');
INSERT INTO `translation_tags` VALUES (126,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Popust (%)');
INSERT INTO `translation_tags` VALUES (127,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Vozač');
INSERT INTO `translation_tags` VALUES (128,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Adresa vozača');
INSERT INTO `translation_tags` VALUES (129,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Zemlja vozača');
INSERT INTO `translation_tags` VALUES (130,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Broj vozačke');
INSERT INTO `translation_tags` VALUES (131,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Broj telefona vozača');
INSERT INTO `translation_tags` VALUES (132,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Dodatni kilometri (km)');
INSERT INTO `translation_tags` VALUES (133,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Dodatni sat najma (cijena)');
INSERT INTO `translation_tags` VALUES (134,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Preuzimanje izvan radnog vremena');
INSERT INTO `translation_tags` VALUES (135,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Dostava vozila na adresu');
INSERT INTO `translation_tags` VALUES (136,'2016-02-06 13:01:21','2016-02-06 13:01:21','','Ugovor napisao');
INSERT INTO `translation_tags` VALUES (137,'2016-02-11 10:31:42','2016-02-11 10:31:42','','Boja za prikaz');
INSERT INTO `translation_tags` VALUES (138,'2016-02-19 08:44:33','2016-02-19 08:44:33','','Last seen');
INSERT INTO `translation_tags` VALUES (139,'2016-02-19 08:44:33','2016-02-19 08:44:33','','Blocked');
INSERT INTO `translation_tags` VALUES (140,'2016-02-19 12:15:44','2016-02-19 12:15:44','','Izdaje');
INSERT INTO `translation_tags` VALUES (141,'2016-02-19 12:15:44','2016-02-19 12:15:44','','Ime ili naziv Najmoprimca');
INSERT INTO `translation_tags` VALUES (142,'2016-02-19 12:15:44','2016-02-19 12:15:44','','Tip i registracija vozila');
INSERT INTO `translation_tags` VALUES (143,'2016-02-19 12:15:44','2016-02-19 12:15:44','','Dnevna kilometraža (km)');
INSERT INTO `translation_tags` VALUES (144,'2016-02-19 12:15:44','2016-02-19 12:15:44','','Dodatni sat najma (kn)');
INSERT INTO `translation_tags` VALUES (145,'2016-02-19 12:15:44','2016-02-19 12:15:44','','Preuzimanje izvan radnog vremena (kn)');
INSERT INTO `translation_tags` VALUES (146,'2016-02-19 12:15:44','2016-02-19 12:15:44','','Dostava vozila na adresu (kn)');
INSERT INTO `translation_tags` VALUES (147,'2016-02-19 12:15:44','2016-02-19 12:15:44','','Dodatna napomena');
INSERT INTO `translation_tags` VALUES (148,'2016-02-22 12:18:20','2016-02-22 12:18:20','','Dodatna napomena (kod preuzimanja)');
INSERT INTO `translation_tags` VALUES (149,'2016-02-22 12:18:20','2016-02-22 12:18:20','','Dodatna napomena (kod vraćanja)');
INSERT INTO `translation_tags` VALUES (150,'2016-02-22 13:49:44','2016-02-22 13:49:44','','ISPRINTAJ');
INSERT INTO `translation_tags` VALUES (151,'2016-02-24 13:19:33','2016-02-24 13:19:33','','');
INSERT INTO `translation_tags` VALUES (152,'2016-02-25 09:03:09','2016-02-25 09:03:09','','');
INSERT INTO `translation_tags` VALUES (153,'2016-02-25 10:56:56','2016-02-25 10:56:56','','');
INSERT INTO `translation_tags` VALUES (154,'2016-02-25 12:13:37','2016-02-25 12:13:37','','');
INSERT INTO `translation_tags` VALUES (155,'2016-02-25 12:14:12','2016-02-25 12:14:12','','');
INSERT INTO `translation_tags` VALUES (156,'2016-02-25 12:46:52','2016-02-25 12:46:52','','');
INSERT INTO `translation_tags` VALUES (157,'2016-02-25 21:36:36','2016-02-25 21:36:36','','');
INSERT INTO `translation_tags` VALUES (158,'2016-02-26 07:19:22','2016-02-26 07:19:22','','');
INSERT INTO `translation_tags` VALUES (159,'2016-02-26 15:11:19','2016-02-26 15:11:19','','');
INSERT INTO `translation_tags` VALUES (160,'2016-02-26 15:15:57','2016-02-26 15:15:57','','');
INSERT INTO `translation_tags` VALUES (161,'2016-02-26 15:18:55','2016-02-26 15:18:55','','');
INSERT INTO `translation_tags` VALUES (162,'2016-02-26 15:58:13','2016-02-26 15:58:13','','');
INSERT INTO `translation_tags` VALUES (163,'2016-02-27 13:42:43','2016-02-27 13:42:43','','');
INSERT INTO `translation_tags` VALUES (164,'2016-02-29 08:42:39','2016-02-29 08:42:39','','');
INSERT INTO `translation_tags` VALUES (165,'2016-02-29 09:43:49','2016-02-29 09:43:49','','');
INSERT INTO `translation_tags` VALUES (166,'2016-02-29 09:44:50','2016-02-29 09:44:50','','');
INSERT INTO `translation_tags` VALUES (167,'2016-02-29 09:57:01','2016-02-29 09:57:01','','');
INSERT INTO `translation_tags` VALUES (168,'2016-02-29 11:39:58','2016-02-29 11:39:58','','');
INSERT INTO `translation_tags` VALUES (169,'2016-02-29 13:34:40','2016-02-29 13:34:40','','');
INSERT INTO `translation_tags` VALUES (170,'2016-03-02 07:26:49','2016-03-02 07:26:49','','');
INSERT INTO `translation_tags` VALUES (171,'2016-03-02 08:10:26','2016-03-02 08:10:26','','');
INSERT INTO `translation_tags` VALUES (172,'2016-03-02 12:04:19','2016-03-02 12:04:19','','');
INSERT INTO `translation_tags` VALUES (173,'2016-03-03 09:36:29','2016-03-03 09:36:29','','');
INSERT INTO `translation_tags` VALUES (174,'2016-03-03 09:45:07','2016-03-03 09:45:07','','');
INSERT INTO `translation_tags` VALUES (175,'2016-03-04 17:51:02','2016-03-04 17:51:02','','');
INSERT INTO `translation_tags` VALUES (176,'2016-03-04 18:34:47','2016-03-04 18:34:47','','');
INSERT INTO `translation_tags` VALUES (177,'2016-03-05 07:44:50','2016-03-05 07:44:50','','');
INSERT INTO `translation_tags` VALUES (178,'2016-03-05 07:47:10','2016-03-05 07:47:10','','');
INSERT INTO `translation_tags` VALUES (179,'2016-03-05 07:48:24','2016-03-05 07:48:24','','');
INSERT INTO `translation_tags` VALUES (180,'2016-03-05 08:00:42','2016-03-05 08:00:42','','');
INSERT INTO `translation_tags` VALUES (181,'2016-03-07 12:14:22','2016-03-07 12:14:22','','Cijena po danu (bez PDV-a)');
INSERT INTO `translation_tags` VALUES (182,'2016-03-08 17:02:45','2016-03-08 17:02:45','','Ključne riječi');
/*!40000 ALTER TABLE `translation_tags` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table translations
#

DROP TABLE IF EXISTS `translations`;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `translation_tag_id` int(10) unsigned NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `translation` text COLLATE utf8_unicode_ci NOT NULL,
  `auto` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=184 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table translations
#
LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;

INSERT INTO `translations` VALUES (1,'2015-12-15 02:21:15','2015-12-20 13:48:02',57,'hr','Traži',0);
INSERT INTO `translations` VALUES (2,'2015-12-15 02:21:15','2015-12-15 02:21:15',2,'hr','Vozilo',1);
INSERT INTO `translations` VALUES (3,'2015-12-15 02:21:15','2015-12-20 13:46:59',3,'hr','Po stranici',0);
INSERT INTO `translations` VALUES (4,'2015-12-15 02:21:15','2015-12-22 12:17:05',4,'hr','Nema rezultata.',0);
INSERT INTO `translations` VALUES (5,'2015-12-15 02:21:19','2015-12-15 02:21:19',5,'hr','Po stranici',1);
INSERT INTO `translations` VALUES (6,'2015-12-15 02:21:36','2015-12-15 02:21:36',6,'hr','Mjesto',1);
INSERT INTO `translations` VALUES (7,'2015-12-15 09:00:53','2015-12-15 09:00:53',7,'hr','Registarska tablica',1);
INSERT INTO `translations` VALUES (8,'2015-12-15 09:00:53','2015-12-15 09:00:53',8,'hr','Naziv vozila',1);
INSERT INTO `translations` VALUES (9,'2015-12-15 09:00:53','2015-12-15 09:00:53',9,'hr','Marka vozila',1);
INSERT INTO `translations` VALUES (10,'2015-12-15 09:00:53','2015-12-15 09:00:53',10,'hr','Model',1);
INSERT INTO `translations` VALUES (11,'2015-12-15 09:00:53','2015-12-15 09:00:53',11,'hr','Tip',1);
INSERT INTO `translations` VALUES (12,'2015-12-15 09:00:53','2015-12-15 09:00:53',12,'hr','Godina proizvodnje',1);
INSERT INTO `translations` VALUES (13,'2015-12-15 09:00:53','2015-12-15 09:00:53',13,'hr','Veličina guma',1);
INSERT INTO `translations` VALUES (14,'2015-12-15 09:00:53','2015-12-15 09:00:53',14,'hr','Boja',1);
INSERT INTO `translations` VALUES (15,'2015-12-15 09:00:53','2015-12-15 09:00:53',15,'hr','Boja (kod)',1);
INSERT INTO `translations` VALUES (16,'2015-12-15 09:00:53','2015-12-15 09:00:53',16,'hr','Broj šasije',1);
INSERT INTO `translations` VALUES (17,'2015-12-15 09:00:53','2015-12-15 09:00:53',17,'hr','Partner',1);
INSERT INTO `translations` VALUES (18,'2015-12-15 09:00:53','2015-12-15 09:00:53',18,'hr','Leasing kuća',1);
INSERT INTO `translations` VALUES (19,'2015-12-15 09:00:53','2015-12-15 09:00:53',19,'hr','Rata leasinga',1);
INSERT INTO `translations` VALUES (20,'2015-12-15 09:00:53','2015-12-15 09:00:53',20,'hr','Registriran do',1);
INSERT INTO `translations` VALUES (21,'2015-12-15 09:00:53','2015-12-15 09:00:53',21,'hr','AO osiguranje vrijedi do',1);
INSERT INTO `translations` VALUES (22,'2015-12-15 09:00:53','2015-12-15 09:00:53',22,'hr','AO osg.kuća',1);
INSERT INTO `translations` VALUES (23,'2015-12-15 09:00:53','2015-12-15 09:00:53',23,'hr','Kasko vrijedi do',1);
INSERT INTO `translations` VALUES (24,'2015-12-15 09:00:53','2015-12-15 09:00:53',24,'hr','Kasko osg.kuća',1);
INSERT INTO `translations` VALUES (25,'2015-12-15 09:00:53','2015-12-15 09:00:53',25,'hr','Periodički pregled vrijedi do',1);
INSERT INTO `translations` VALUES (26,'2015-12-15 09:00:53','2015-12-15 09:00:53',26,'hr','Protupožarni aparat vrijedi do',1);
INSERT INTO `translations` VALUES (27,'2015-12-15 09:00:53','2015-12-15 09:00:53',27,'hr','Dokumenti',1);
INSERT INTO `translations` VALUES (28,'2015-12-15 09:20:42','2015-12-15 09:20:42',28,'hr','Name',1);
INSERT INTO `translations` VALUES (29,'2015-12-15 09:20:42','2015-12-15 09:20:42',29,'hr','Status',1);
INSERT INTO `translations` VALUES (30,'2015-12-15 09:20:42','2015-12-15 09:20:42',30,'hr','Client',1);
INSERT INTO `translations` VALUES (31,'2015-12-15 09:20:42','2015-12-15 09:20:42',31,'hr','Data',1);
INSERT INTO `translations` VALUES (32,'2015-12-15 09:20:42','2015-12-15 09:20:42',32,'hr','Pdf',1);
INSERT INTO `translations` VALUES (33,'2015-12-15 09:20:49','2015-12-15 09:20:49',33,'hr','Ime',1);
INSERT INTO `translations` VALUES (34,'2015-12-15 09:20:49','2015-12-15 09:20:49',34,'hr','Prezime',1);
INSERT INTO `translations` VALUES (35,'2015-12-15 09:20:49','2015-12-15 09:20:49',35,'hr','Tvrtka (kratki naziv)',1);
INSERT INTO `translations` VALUES (36,'2015-12-15 09:20:49','2015-12-15 09:20:49',36,'hr','Tvrtka (dugi naziv)',1);
INSERT INTO `translations` VALUES (37,'2015-12-15 09:20:49','2015-12-15 09:20:49',37,'hr','OIB',1);
INSERT INTO `translations` VALUES (38,'2015-12-15 09:20:49','2015-12-15 09:20:49',38,'hr','MB',1);
INSERT INTO `translations` VALUES (39,'2015-12-15 09:20:49','2015-12-15 09:20:49',39,'hr','Adresa',1);
INSERT INTO `translations` VALUES (40,'2015-12-15 09:20:49','2015-12-15 09:20:49',40,'hr','Adresa 2',1);
INSERT INTO `translations` VALUES (41,'2015-12-15 09:20:49','2015-12-15 09:20:49',41,'hr','Poštanski broj',1);
INSERT INTO `translations` VALUES (42,'2015-12-15 09:20:49','2015-12-15 09:20:49',42,'hr','Država',1);
INSERT INTO `translations` VALUES (43,'2015-12-15 09:20:49','2015-12-15 09:20:49',43,'hr','Tip kontakta',1);
INSERT INTO `translations` VALUES (44,'2015-12-15 09:20:49','2015-12-15 09:20:49',44,'hr','Email',1);
INSERT INTO `translations` VALUES (45,'2015-12-15 09:20:49','2015-12-15 09:20:49',45,'hr','Mobilni tel.',1);
INSERT INTO `translations` VALUES (46,'2015-12-15 09:20:49','2015-12-15 09:20:49',46,'hr','Tel.',1);
INSERT INTO `translations` VALUES (47,'2015-12-15 09:20:49','2015-12-15 09:20:49',47,'hr','Fax',1);
INSERT INTO `translations` VALUES (48,'2015-12-15 09:20:49','2015-12-15 09:20:49',48,'hr','Web',1);
INSERT INTO `translations` VALUES (49,'2015-12-15 09:20:49','2015-12-15 09:20:49',49,'hr','Napomena',1);
INSERT INTO `translations` VALUES (50,'2015-12-15 09:52:53','2015-12-15 09:52:53',50,'hr','Tip servisa',1);
INSERT INTO `translations` VALUES (51,'2015-12-15 09:52:53','2015-12-15 09:52:53',51,'hr','Početni datum servisa',1);
INSERT INTO `translations` VALUES (52,'2015-12-15 09:52:53','2015-12-15 09:52:53',52,'hr','Završni datum servisa',1);
INSERT INTO `translations` VALUES (53,'2015-12-15 09:52:53','2015-12-15 09:52:53',53,'hr','Kilometraža',1);
INSERT INTO `translations` VALUES (54,'2015-12-15 09:52:53','2015-12-15 09:52:53',54,'hr','Servisni centar',1);
INSERT INTO `translations` VALUES (55,'2015-12-15 09:52:53','2015-12-15 09:52:53',55,'hr','Cijena servisa',1);
INSERT INTO `translations` VALUES (56,'2015-12-15 09:52:53','2015-12-15 09:52:53',56,'hr','Cijena dijelova',1);
INSERT INTO `translations` VALUES (57,'2015-12-15 09:54:20','2015-12-15 09:54:20',57,'hr','Search',1);
INSERT INTO `translations` VALUES (58,'2015-12-15 09:54:20','2015-12-15 09:54:20',58,'hr','Usergroup',1);
INSERT INTO `translations` VALUES (59,'2015-12-15 09:54:28','2015-12-15 09:54:28',59,'hr','Username',1);
INSERT INTO `translations` VALUES (60,'2015-12-15 09:54:28','2015-12-15 09:54:28',60,'hr','Password',1);
INSERT INTO `translations` VALUES (61,'2015-12-15 09:54:28','2015-12-15 09:54:28',61,'hr','First Name',1);
INSERT INTO `translations` VALUES (62,'2015-12-15 09:54:28','2015-12-15 09:54:28',62,'hr','Last Name',1);
INSERT INTO `translations` VALUES (63,'2015-12-17 18:37:00','2015-12-17 18:37:00',63,'hr','Prešao kilometara',1);
INSERT INTO `translations` VALUES (64,'2015-12-17 18:37:00','2015-12-17 18:37:00',64,'hr','Redoviti servis svakih (km)',1);
INSERT INTO `translations` VALUES (65,'2015-12-17 18:41:46','2015-12-17 18:41:46',65,'hr','Početni datum',1);
INSERT INTO `translations` VALUES (66,'2015-12-17 18:41:46','2015-12-17 18:41:46',66,'hr','Početno vrijeme',1);
INSERT INTO `translations` VALUES (67,'2015-12-17 18:41:46','2015-12-17 18:41:46',67,'hr','Završni datum',1);
INSERT INTO `translations` VALUES (68,'2015-12-17 18:41:46','2015-12-17 18:41:46',68,'hr','Završno vrijeme',1);
INSERT INTO `translations` VALUES (69,'2015-12-17 18:41:46','2015-12-17 18:41:46',69,'hr','Početna kilometraža',1);
INSERT INTO `translations` VALUES (70,'2015-12-17 18:41:46','2015-12-17 18:41:46',70,'hr','Završna kilometraža',1);
INSERT INTO `translations` VALUES (71,'2015-12-17 18:41:46','2015-12-17 18:41:46',71,'hr','Ukupna cijena',1);
INSERT INTO `translations` VALUES (72,'2015-12-17 18:41:46','2015-12-17 18:41:46',72,'hr','Klijent',1);
INSERT INTO `translations` VALUES (73,'2015-12-17 18:41:46','2015-12-17 18:41:46',73,'hr','Preko koje firme',1);
INSERT INTO `translations` VALUES (74,'2015-12-17 18:42:25','2015-12-17 18:42:25',74,'hr','Naslov',1);
INSERT INTO `translations` VALUES (75,'2015-12-17 18:42:25','2015-12-17 18:42:25',75,'hr','Datum događaja',1);
INSERT INTO `translations` VALUES (76,'2015-12-17 18:42:25','2015-12-17 18:42:25',76,'hr','Mjesto događaja',1);
INSERT INTO `translations` VALUES (77,'2015-12-17 18:42:25','2015-12-17 18:42:25',77,'hr','Km na vozilu',1);
INSERT INTO `translations` VALUES (78,'2015-12-17 18:42:25','2015-12-17 18:42:25',78,'hr','Opis',1);
INSERT INTO `translations` VALUES (79,'2015-12-17 18:42:25','2015-12-17 18:42:25',79,'hr','Tko je oštetio vozilo',1);
INSERT INTO `translations` VALUES (80,'2015-12-18 12:52:15','2015-12-18 12:52:15',80,'hr','Tvrtka',1);
INSERT INTO `translations` VALUES (81,'2015-12-18 12:58:09','2015-12-18 12:58:09',81,'hr','Kontakt osoba',1);
INSERT INTO `translations` VALUES (82,'2015-12-18 12:58:10','2015-12-18 12:58:10',82,'hr','Kontakt telefon',1);
INSERT INTO `translations` VALUES (83,'2015-12-18 13:00:45','2015-12-18 13:00:45',83,'hr','Kontakt mobitel',1);
INSERT INTO `translations` VALUES (84,'2015-12-18 13:05:48','2015-12-18 13:05:48',84,'hr','Mobitel',1);
INSERT INTO `translations` VALUES (85,'2015-12-20 13:46:31','2015-12-20 13:46:31',85,'hr','Language',1);
INSERT INTO `translations` VALUES (86,'2015-12-20 13:46:31','2015-12-20 13:46:31',86,'hr','Translation',1);
INSERT INTO `translations` VALUES (87,'2015-12-20 13:46:32','2015-12-20 13:46:32',87,'hr','Actions',1);
INSERT INTO `translations` VALUES (88,'2015-12-20 13:46:49','2015-12-20 13:46:49',88,'hr','Tag',1);
INSERT INTO `translations` VALUES (89,'2015-12-20 13:46:49','2015-12-20 13:46:49',89,'hr','Auto',1);
INSERT INTO `translations` VALUES (90,'2015-12-20 13:48:14','2015-12-20 13:48:14',1,'hr','Pretraga',1);
INSERT INTO `translations` VALUES (91,'2015-12-21 10:29:54','2015-12-21 10:34:03',90,'hr','PROMIJENI',0);
INSERT INTO `translations` VALUES (92,'2015-12-21 10:29:54','2015-12-21 10:34:23',91,'hr','OBRIŠI',0);
INSERT INTO `translations` VALUES (93,'2015-12-21 10:49:02','2015-12-21 10:49:28',92,'hr','Jeste li sigurni?',0);
INSERT INTO `translations` VALUES (94,'2015-12-21 11:43:32','2015-12-21 11:43:32',93,'hr','Status plaćanja',1);
INSERT INTO `translations` VALUES (95,'2015-12-21 12:10:20','2015-12-21 12:10:20',94,'hr','Najmodavac',1);
INSERT INTO `translations` VALUES (96,'2015-12-21 16:12:46','2015-12-21 16:12:46',95,'hr','Profile',1);
INSERT INTO `translations` VALUES (97,'2015-12-21 16:12:46','2015-12-21 16:12:46',96,'hr','Sign out',1);
INSERT INTO `translations` VALUES (98,'2015-12-22 08:31:14','2015-12-22 08:31:58',97,'hr','Zapamti',0);
INSERT INTO `translations` VALUES (99,'2015-12-22 12:42:28','2015-12-22 12:42:28',98,'hr','Od datuma',1);
INSERT INTO `translations` VALUES (100,'2015-12-22 12:42:28','2015-12-22 12:42:28',99,'hr','Do datuma',1);
INSERT INTO `translations` VALUES (101,'2015-12-23 11:08:54','2015-12-23 11:08:54',100,'hr','Vezani dokument',1);
INSERT INTO `translations` VALUES (102,'2015-12-24 11:25:26','2015-12-24 11:25:26',101,'hr','Prati datum',1);
INSERT INTO `translations` VALUES (103,'2015-12-26 10:58:42','2015-12-26 10:58:42',102,'hr','Upiši pregled',1);
INSERT INTO `translations` VALUES (104,'2015-12-28 10:52:41','2015-12-28 13:02:05',103,'hr','Resetiraj sve filtere',0);
INSERT INTO `translations` VALUES (105,'2015-12-28 12:16:07','2015-12-28 12:16:07',104,'hr','Uplaćeni iznos',1);
INSERT INTO `translations` VALUES (106,'2015-12-28 12:16:07','2015-12-28 12:16:07',105,'hr','Status najma',1);
INSERT INTO `translations` VALUES (107,'2015-12-29 08:59:22','2015-12-29 08:59:22',106,'hr','Zapamti provjeru',1);
INSERT INTO `translations` VALUES (108,'2015-12-29 08:59:22','2015-12-29 08:59:22',107,'hr','<i class=\'fa fa-save\'></i> Zapamti provjeru',1);
INSERT INTO `translations` VALUES (109,'2015-12-29 12:39:39','2015-12-29 12:39:39',108,'hr','Vrsta usluge',1);
INSERT INTO `translations` VALUES (110,'2015-12-31 10:27:49','2015-12-31 10:27:49',109,'hr','Status servisa',1);
INSERT INTO `translations` VALUES (111,'2015-12-31 12:22:52','2015-12-31 12:22:52',110,'hr','Odvesti na kilometara',1);
INSERT INTO `translations` VALUES (112,'2016-01-07 10:32:46','2016-01-07 10:32:46',111,'hr','Title',1);
INSERT INTO `translations` VALUES (113,'2016-01-07 10:32:46','2016-01-07 10:32:46',112,'hr','Permissions',1);
INSERT INTO `translations` VALUES (114,'2016-01-12 13:04:47','2016-01-12 13:04:47',113,'hr','Prikaži ovo vozilo u rasporedu',1);
INSERT INTO `translations` VALUES (115,'2016-02-04 14:35:19','2016-02-04 14:35:19',114,'hr','Tip vozila',1);
INSERT INTO `translations` VALUES (116,'2016-02-06 13:01:21','2016-02-06 13:01:21',115,'hr','Datum ugovora',1);
INSERT INTO `translations` VALUES (117,'2016-02-06 13:01:21','2016-02-06 13:01:21',116,'hr','Najmoprimac',1);
INSERT INTO `translations` VALUES (118,'2016-02-06 13:01:21','2016-02-06 13:01:21',117,'hr','Adresa sjedišta',1);
INSERT INTO `translations` VALUES (119,'2016-02-06 13:01:21','2016-02-06 13:01:21',118,'hr','Registracija vozila',1);
INSERT INTO `translations` VALUES (120,'2016-02-06 13:01:21','2016-02-06 13:01:21',119,'hr','Datum preuzimanja',1);
INSERT INTO `translations` VALUES (121,'2016-02-06 13:01:21','2016-02-06 13:01:21',120,'hr','Vrijeme preuzimanja',1);
INSERT INTO `translations` VALUES (122,'2016-02-06 13:01:21','2016-02-06 13:01:21',121,'hr','Datum povratka',1);
INSERT INTO `translations` VALUES (123,'2016-02-06 13:01:21','2016-02-06 13:01:21',122,'hr','Vrijeme povratka',1);
INSERT INTO `translations` VALUES (124,'2016-02-06 13:01:21','2016-02-06 13:01:21',123,'hr','Cijena po danu',1);
INSERT INTO `translations` VALUES (125,'2016-02-06 13:01:21','2016-02-06 13:01:21',124,'hr','Ukupno dana',1);
INSERT INTO `translations` VALUES (126,'2016-02-06 13:01:21','2016-02-06 13:01:21',125,'hr','Dnevna kilometraža',1);
INSERT INTO `translations` VALUES (127,'2016-02-06 13:01:21','2016-02-06 13:01:21',126,'hr','Popust (%)',1);
INSERT INTO `translations` VALUES (128,'2016-02-06 13:01:21','2016-02-06 13:01:21',127,'hr','Vozač',1);
INSERT INTO `translations` VALUES (129,'2016-02-06 13:01:21','2016-02-06 13:01:21',128,'hr','Adresa vozača',1);
INSERT INTO `translations` VALUES (130,'2016-02-06 13:01:21','2016-02-06 13:01:21',129,'hr','Zemlja vozača',1);
INSERT INTO `translations` VALUES (131,'2016-02-06 13:01:21','2016-02-06 13:01:21',130,'hr','Broj vozačke',1);
INSERT INTO `translations` VALUES (132,'2016-02-06 13:01:21','2016-02-06 13:01:21',131,'hr','Broj telefona vozača',1);
INSERT INTO `translations` VALUES (133,'2016-02-06 13:01:21','2016-02-06 13:01:21',132,'hr','Dodatni kilometri (km)',1);
INSERT INTO `translations` VALUES (134,'2016-02-06 13:01:21','2016-02-06 13:01:21',133,'hr','Dodatni sat najma (cijena)',1);
INSERT INTO `translations` VALUES (135,'2016-02-06 13:01:21','2016-02-06 13:01:21',134,'hr','Preuzimanje izvan radnog vremena',1);
INSERT INTO `translations` VALUES (136,'2016-02-06 13:01:21','2016-02-06 13:01:21',135,'hr','Dostava vozila na adresu',1);
INSERT INTO `translations` VALUES (137,'2016-02-06 13:01:21','2016-02-06 13:01:21',136,'hr','Ugovor napisao',1);
INSERT INTO `translations` VALUES (138,'2016-02-11 10:31:42','2016-02-11 10:31:42',137,'hr','Boja za prikaz',1);
INSERT INTO `translations` VALUES (139,'2016-02-19 08:44:33','2016-02-19 08:44:33',138,'hr','Last seen',1);
INSERT INTO `translations` VALUES (140,'2016-02-19 08:44:33','2016-02-19 08:44:33',139,'hr','Blocked',1);
INSERT INTO `translations` VALUES (141,'2016-02-19 12:15:44','2016-02-19 12:15:44',140,'hr','Izdaje',1);
INSERT INTO `translations` VALUES (142,'2016-02-19 12:15:44','2016-02-19 12:15:44',141,'hr','Ime ili naziv Najmoprimca',1);
INSERT INTO `translations` VALUES (143,'2016-02-19 12:15:44','2016-02-19 12:15:44',142,'hr','Tip i registracija vozila',1);
INSERT INTO `translations` VALUES (144,'2016-02-19 12:15:44','2016-02-19 12:15:44',143,'hr','Dnevna kilometraža (km)',1);
INSERT INTO `translations` VALUES (145,'2016-02-19 12:15:44','2016-02-19 12:15:44',144,'hr','Dodatni sat najma (kn)',1);
INSERT INTO `translations` VALUES (146,'2016-02-19 12:15:44','2016-02-19 12:15:44',145,'hr','Preuzimanje izvan radnog vremena (kn)',1);
INSERT INTO `translations` VALUES (147,'2016-02-19 12:15:44','2016-02-19 12:15:44',146,'hr','Dostava vozila na adresu (kn)',1);
INSERT INTO `translations` VALUES (148,'2016-02-19 12:15:44','2016-02-19 12:15:44',147,'hr','Dodatna napomena',1);
INSERT INTO `translations` VALUES (149,'2016-02-22 12:18:20','2016-02-22 12:18:20',148,'hr','Dodatna napomena (kod preuzimanja)',1);
INSERT INTO `translations` VALUES (150,'2016-02-22 12:18:20','2016-02-22 12:18:20',149,'hr','Dodatna napomena (kod vraćanja)',1);
INSERT INTO `translations` VALUES (151,'2016-02-22 13:49:44','2016-02-22 13:49:44',150,'hr','ISPRINTAJ',1);
INSERT INTO `translations` VALUES (152,'2016-02-24 13:19:33','2016-02-24 13:19:33',0,'en','',1);
INSERT INTO `translations` VALUES (153,'2016-02-25 09:03:09','2016-02-25 09:03:09',0,'en','',1);
INSERT INTO `translations` VALUES (154,'2016-02-25 10:56:56','2016-02-25 10:56:56',0,'en','',1);
INSERT INTO `translations` VALUES (155,'2016-02-25 12:13:37','2016-02-25 12:13:37',0,'en','',1);
INSERT INTO `translations` VALUES (156,'2016-02-25 12:14:12','2016-02-25 12:14:12',0,'en','',1);
INSERT INTO `translations` VALUES (157,'2016-02-25 12:46:52','2016-02-25 12:46:52',0,'en','',1);
INSERT INTO `translations` VALUES (158,'2016-02-25 21:36:36','2016-02-25 21:36:36',0,'en','',1);
INSERT INTO `translations` VALUES (159,'2016-02-26 07:19:22','2016-02-26 07:19:22',0,'en','',1);
INSERT INTO `translations` VALUES (160,'2016-02-26 15:11:19','2016-02-26 15:11:19',0,'en','',1);
INSERT INTO `translations` VALUES (161,'2016-02-26 15:15:57','2016-02-26 15:15:57',0,'en','',1);
INSERT INTO `translations` VALUES (162,'2016-02-26 15:18:55','2016-02-26 15:18:55',0,'en','',1);
INSERT INTO `translations` VALUES (163,'2016-02-26 15:58:13','2016-02-26 15:58:13',0,'en','',1);
INSERT INTO `translations` VALUES (164,'2016-02-27 13:42:43','2016-02-27 13:42:43',0,'en','',1);
INSERT INTO `translations` VALUES (165,'2016-02-29 08:42:39','2016-02-29 08:42:39',0,'en','',1);
INSERT INTO `translations` VALUES (166,'2016-02-29 09:43:49','2016-02-29 09:43:49',0,'en','',1);
INSERT INTO `translations` VALUES (167,'2016-02-29 09:44:50','2016-02-29 09:44:50',0,'en','',1);
INSERT INTO `translations` VALUES (168,'2016-02-29 09:57:01','2016-02-29 09:57:01',0,'en','',1);
INSERT INTO `translations` VALUES (169,'2016-02-29 11:39:58','2016-02-29 11:39:58',0,'en','',1);
INSERT INTO `translations` VALUES (170,'2016-02-29 13:34:40','2016-02-29 13:34:40',0,'en','',1);
INSERT INTO `translations` VALUES (171,'2016-03-02 07:26:49','2016-03-02 07:26:49',0,'en','',1);
INSERT INTO `translations` VALUES (172,'2016-03-02 08:10:26','2016-03-02 08:10:26',0,'en','',1);
INSERT INTO `translations` VALUES (173,'2016-03-02 12:04:19','2016-03-02 12:04:19',0,'en','',1);
INSERT INTO `translations` VALUES (174,'2016-03-03 09:36:29','2016-03-03 09:36:29',0,'en','',1);
INSERT INTO `translations` VALUES (175,'2016-03-03 09:45:07','2016-03-03 09:45:07',0,'en','',1);
INSERT INTO `translations` VALUES (176,'2016-03-04 17:51:02','2016-03-04 17:51:02',0,'en','',1);
INSERT INTO `translations` VALUES (177,'2016-03-04 18:34:47','2016-03-04 18:34:47',0,'en','',1);
INSERT INTO `translations` VALUES (178,'2016-03-05 07:44:50','2016-03-05 07:44:50',0,'en','',1);
INSERT INTO `translations` VALUES (179,'2016-03-05 07:47:10','2016-03-05 07:47:10',0,'en','',1);
INSERT INTO `translations` VALUES (180,'2016-03-05 07:48:24','2016-03-05 07:48:24',0,'en','',1);
INSERT INTO `translations` VALUES (181,'2016-03-05 08:00:42','2016-03-05 08:00:42',0,'en','',1);
INSERT INTO `translations` VALUES (182,'2016-03-07 12:14:22','2016-03-07 12:14:22',181,'hr','Cijena po danu (bez PDV-a)',1);
INSERT INTO `translations` VALUES (183,'2016-03-08 17:02:45','2016-03-08 17:02:45',182,'hr','Ključne riječi',1);
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table user_groups
#

DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE `user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table user_groups
#
LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;

INSERT INTO `user_groups` VALUES (1,'2015-12-13 14:30:54','2016-03-21 10:08:53','Administrator','admin','access.admin,admin,menu.user,user.create,user.switchto,user.ownpassword,manage.usergroups,menu.dashboard,menu.calendar,menu.booking,menu.contact,menu.vehicle,menu.vehicle-service,menu.damage,menu.checklist,show.totals');
INSERT INTO `user_groups` VALUES (4,'2016-01-11 09:50:11','2016-01-12 12:54:58','Vanjski korisnik','vanjski','access.admin,menu.vehicle-service,menu.checklist');
INSERT INTO `user_groups` VALUES (5,'2016-01-26 20:39:23','2016-01-26 20:39:23','Marketing','marketing','access.admin,menu.user,menu.dashboard,menu.calendar,menu.booking,menu.contact,menu.vehicle,menu.vehicle-service,menu.damage,menu.checklist');
INSERT INTO `user_groups` VALUES (6,'2016-03-21 12:20:41','2016-03-21 12:20:41','Zaposlenik','zaposlenik','access.admin,user.ownpassword,menu.dashboard,menu.calendar,menu.booking,menu.contact,menu.vehicle,menu.vehicle-service,menu.damage,menu.checklist');
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table user_throttle
#

DROP TABLE IF EXISTS `user_throttle`;
CREATE TABLE `user_throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ip_address` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `invalid_tries` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `data` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table user_throttle
#
LOCK TABLES `user_throttle` WRITE;
/*!40000 ALTER TABLE `user_throttle` DISABLE KEYS */;

/*!40000 ALTER TABLE `user_throttle` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table users
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `usergroup` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `blocked` tinyint(3) unsigned DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table users
#
LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` VALUES (1,'2015-12-13 14:30:54','2016-04-21 13:27:06','tihomir.jauk@gmail.com','admin','$2y$10$7Bz.2Pjvx2BgsOWB.a.9IOUvgm1swWIyB3v/MBbejM51n3.dsUJpW','admin','Tihomir','Jauk','',0,'2016-04-21 13:27:06');
INSERT INTO `users` VALUES (2,'2015-12-15 09:56:54','2016-05-05 10:40:24','info@komet-prijevoz.hr','icukac','$2y$10$89EKdijusI9UyV.alObs/eDnA3m6Hrs7Xd5PFQg.c1kDh6P3xHcze','admin','Igor','Čukac','',NULL,'2016-05-05 10:40:24');
INSERT INTO `users` VALUES (4,'2015-12-23 12:04:24','2016-05-04 14:22:20','info@komet-prijevoz.hr','boris','$2y$10$upSmSliufEZWqmn9vODXmuANq6iqyEV4nfUDnwquFwxONsvN2jB72','admin','Boris','Čukac','',NULL,'2016-05-04 14:22:20');
INSERT INTO `users` VALUES (7,'2016-03-21 12:21:21','2016-03-26 19:21:08','','luka','$2y$10$19M0Q3V8b.ugmQWsoww4seAaTR3PD8KqRQh98TrsNvgBGlq1bNrTC','zaposlenik','Luka','Čukac','',0,'2016-03-26 19:21:08');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table vehicle_kms
#

DROP TABLE IF EXISTS `vehicle_kms`;
CREATE TABLE `vehicle_kms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `vehicle_id` int(10) unsigned NOT NULL,
  `km` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table vehicle_kms
#
LOCK TABLES `vehicle_kms` WRITE;
/*!40000 ALTER TABLE `vehicle_kms` DISABLE KEYS */;

INSERT INTO `vehicle_kms` VALUES (1,'2016-01-19 12:51:14','2016-01-19 12:51:14',12,15600);
INSERT INTO `vehicle_kms` VALUES (2,'2016-01-20 07:41:04','2016-01-20 07:41:04',6,30931);
INSERT INTO `vehicle_kms` VALUES (3,'2016-01-20 07:41:04','2016-01-20 07:41:04',6,33832);
INSERT INTO `vehicle_kms` VALUES (4,'2016-01-20 08:30:44','2016-01-20 08:30:44',3,123201);
INSERT INTO `vehicle_kms` VALUES (5,'2016-01-20 08:30:44','2016-01-20 08:30:44',3,124527);
INSERT INTO `vehicle_kms` VALUES (6,'2016-01-21 13:39:41','2016-01-21 13:39:41',5,90175);
INSERT INTO `vehicle_kms` VALUES (7,'2016-01-22 09:00:18','2016-01-22 09:00:18',4,207318);
INSERT INTO `vehicle_kms` VALUES (8,'2016-01-22 15:06:58','2016-01-22 15:06:58',4,207518);
INSERT INTO `vehicle_kms` VALUES (9,'2016-01-24 07:31:18','2016-01-24 07:31:18',9,114123);
INSERT INTO `vehicle_kms` VALUES (10,'2016-01-24 09:51:24','2016-01-24 09:51:24',5,91509);
INSERT INTO `vehicle_kms` VALUES (11,'2016-01-24 10:30:11','2016-01-24 10:30:11',3,125357);
INSERT INTO `vehicle_kms` VALUES (12,'2016-01-24 10:34:30','2016-01-24 10:34:30',3,125497);
INSERT INTO `vehicle_kms` VALUES (13,'2016-01-26 07:28:57','2016-01-26 07:28:57',9,121678);
INSERT INTO `vehicle_kms` VALUES (14,'2016-01-26 07:32:45','2016-01-26 07:32:45',11,10);
INSERT INTO `vehicle_kms` VALUES (15,'2016-01-26 07:32:45','2016-01-26 07:32:45',11,1941);
INSERT INTO `vehicle_kms` VALUES (16,'2016-01-26 07:33:51','2016-01-26 07:33:51',7,229130);
INSERT INTO `vehicle_kms` VALUES (17,'2016-01-26 12:52:43','2016-01-26 12:52:43',11,3200);
INSERT INTO `vehicle_kms` VALUES (18,'2016-01-26 13:07:20','2016-01-26 13:07:20',6,34000);
INSERT INTO `vehicle_kms` VALUES (19,'2016-01-26 13:07:20','2016-01-26 13:07:20',6,35135);
INSERT INTO `vehicle_kms` VALUES (20,'2016-01-27 07:26:25','2016-01-27 07:26:25',5,91824);
INSERT INTO `vehicle_kms` VALUES (21,'2016-01-28 07:58:45','2016-01-28 07:58:45',3,125945);
INSERT INTO `vehicle_kms` VALUES (22,'2016-01-28 08:00:41','2016-01-28 08:00:41',5,91905);
INSERT INTO `vehicle_kms` VALUES (23,'2016-01-28 10:24:27','2016-01-28 10:24:27',11,3400);
INSERT INTO `vehicle_kms` VALUES (24,'2016-01-29 07:29:26','2016-01-29 07:29:26',5,92369);
INSERT INTO `vehicle_kms` VALUES (25,'2016-01-29 12:07:57','2016-01-29 12:07:57',4,208518);
INSERT INTO `vehicle_kms` VALUES (26,'2016-01-29 12:07:57','2016-01-29 12:07:57',4,211934);
INSERT INTO `vehicle_kms` VALUES (27,'2016-01-29 13:00:01','2016-01-29 13:00:01',8,220134);
INSERT INTO `vehicle_kms` VALUES (28,'2016-01-29 13:00:01','2016-01-29 13:00:01',8,223021);
INSERT INTO `vehicle_kms` VALUES (29,'2016-01-29 16:16:19','2016-01-29 16:16:19',11,3600);
INSERT INTO `vehicle_kms` VALUES (30,'2016-01-30 08:29:30','2016-01-30 08:29:30',6,35335);
INSERT INTO `vehicle_kms` VALUES (31,'2016-01-30 08:29:30','2016-01-30 08:29:30',6,38341);
INSERT INTO `vehicle_kms` VALUES (32,'2016-02-01 07:55:27','2016-02-01 07:55:27',3,126046);
INSERT INTO `vehicle_kms` VALUES (33,'2016-02-02 10:36:34','2016-02-02 10:36:34',4,212032);
INSERT INTO `vehicle_kms` VALUES (34,'2016-02-02 23:10:44','2016-02-02 23:10:44',2,280001);
INSERT INTO `vehicle_kms` VALUES (35,'2016-02-03 14:41:05','2016-02-03 14:41:05',10,118889);
INSERT INTO `vehicle_kms` VALUES (36,'2016-02-04 09:14:16','2016-02-04 09:14:16',16,10);
INSERT INTO `vehicle_kms` VALUES (37,'2016-02-04 09:14:16','2016-02-04 09:14:16',16,1200);
INSERT INTO `vehicle_kms` VALUES (38,'2016-02-07 19:41:38','2016-02-07 19:41:38',6,38461);
INSERT INTO `vehicle_kms` VALUES (39,'2016-02-08 08:07:41','2016-02-08 08:07:41',10,119637);
INSERT INTO `vehicle_kms` VALUES (40,'2016-02-08 08:08:36','2016-02-08 08:08:36',7,230755);
INSERT INTO `vehicle_kms` VALUES (41,'2016-02-08 10:19:57','2016-02-08 10:19:57',3,126969);
INSERT INTO `vehicle_kms` VALUES (42,'2016-02-11 10:32:06','2016-02-11 10:32:06',2,280002);
INSERT INTO `vehicle_kms` VALUES (43,'2016-02-11 10:34:57','2016-02-11 10:34:57',15,0);
INSERT INTO `vehicle_kms` VALUES (44,'2016-02-12 11:13:13','2016-02-12 11:13:13',3,127082);
INSERT INTO `vehicle_kms` VALUES (45,'2016-02-13 14:03:47','2016-02-13 14:03:47',6,38521);
INSERT INTO `vehicle_kms` VALUES (46,'2016-02-15 08:05:36','2016-02-15 08:05:36',3,128335);
INSERT INTO `vehicle_kms` VALUES (47,'2016-02-16 08:58:02','2016-02-16 08:58:02',5,92807);
INSERT INTO `vehicle_kms` VALUES (48,'2016-02-19 13:04:33','2016-02-19 13:04:33',11,3769);
INSERT INTO `vehicle_kms` VALUES (49,'2016-02-22 10:45:04','2016-02-22 10:45:04',3,129305);
INSERT INTO `vehicle_kms` VALUES (50,'2016-02-24 08:23:09','2016-02-24 08:23:09',0,0);
INSERT INTO `vehicle_kms` VALUES (51,'2016-02-24 21:24:13','2016-02-24 21:24:13',0,0);
INSERT INTO `vehicle_kms` VALUES (52,'2016-02-24 21:25:07','2016-02-24 21:25:07',0,0);
INSERT INTO `vehicle_kms` VALUES (53,'2016-02-25 07:15:44','2016-02-25 07:15:44',0,0);
INSERT INTO `vehicle_kms` VALUES (54,'2016-02-25 12:53:45','2016-02-25 12:53:45',0,0);
INSERT INTO `vehicle_kms` VALUES (55,'2016-02-26 15:21:09','2016-02-26 15:21:09',0,0);
INSERT INTO `vehicle_kms` VALUES (56,'2016-02-26 16:42:30','2016-02-26 16:42:30',0,0);
INSERT INTO `vehicle_kms` VALUES (57,'2016-02-26 20:17:34','2016-02-26 20:17:34',0,0);
INSERT INTO `vehicle_kms` VALUES (58,'2016-02-26 20:18:46','2016-02-26 20:18:46',0,0);
INSERT INTO `vehicle_kms` VALUES (59,'2016-02-27 09:57:53','2016-02-27 09:57:53',0,0);
INSERT INTO `vehicle_kms` VALUES (60,'2016-02-27 09:57:53','2016-02-27 09:57:53',0,0);
INSERT INTO `vehicle_kms` VALUES (61,'2016-02-27 13:31:01','2016-02-27 13:31:01',0,0);
INSERT INTO `vehicle_kms` VALUES (62,'2016-02-27 13:31:01','2016-02-27 13:31:01',0,0);
INSERT INTO `vehicle_kms` VALUES (63,'2016-02-27 13:42:20','2016-02-27 13:42:20',0,0);
INSERT INTO `vehicle_kms` VALUES (64,'2016-02-27 13:42:35','2016-02-27 13:42:35',0,0);
INSERT INTO `vehicle_kms` VALUES (65,'2016-02-28 14:45:38','2016-02-28 14:45:38',0,0);
INSERT INTO `vehicle_kms` VALUES (66,'2016-02-29 08:54:13','2016-02-29 08:54:13',0,0);
INSERT INTO `vehicle_kms` VALUES (67,'2016-02-29 08:54:13','2016-02-29 08:54:13',0,0);
INSERT INTO `vehicle_kms` VALUES (68,'2016-02-29 10:05:29','2016-02-29 10:05:29',0,0);
INSERT INTO `vehicle_kms` VALUES (69,'2016-02-29 11:49:20','2016-02-29 11:49:20',0,0);
INSERT INTO `vehicle_kms` VALUES (70,'2016-02-29 11:49:54','2016-02-29 11:49:54',0,0);
INSERT INTO `vehicle_kms` VALUES (71,'2016-02-29 12:23:22','2016-02-29 12:23:22',0,0);
INSERT INTO `vehicle_kms` VALUES (72,'2016-02-29 13:16:02','2016-02-29 13:16:02',0,0);
INSERT INTO `vehicle_kms` VALUES (73,'2016-02-29 13:16:02','2016-02-29 13:16:02',0,0);
INSERT INTO `vehicle_kms` VALUES (74,'2016-02-29 13:34:04','2016-02-29 13:34:04',0,0);
INSERT INTO `vehicle_kms` VALUES (75,'2016-02-29 13:34:04','2016-02-29 13:34:04',0,0);
INSERT INTO `vehicle_kms` VALUES (76,'2016-02-29 13:34:27','2016-02-29 13:34:27',0,0);
INSERT INTO `vehicle_kms` VALUES (77,'2016-03-01 06:47:06','2016-03-01 06:47:06',0,0);
INSERT INTO `vehicle_kms` VALUES (78,'2016-03-01 06:47:26','2016-03-01 06:47:26',0,0);
INSERT INTO `vehicle_kms` VALUES (79,'2016-03-01 10:58:14','2016-03-01 10:58:14',0,0);
INSERT INTO `vehicle_kms` VALUES (80,'2016-03-01 10:58:14','2016-03-01 10:58:14',0,0);
INSERT INTO `vehicle_kms` VALUES (81,'2016-03-01 10:58:28','2016-03-01 10:58:28',0,0);
INSERT INTO `vehicle_kms` VALUES (82,'2016-03-01 10:58:44','2016-03-01 10:58:44',0,0);
INSERT INTO `vehicle_kms` VALUES (83,'2016-03-01 19:09:22','2016-03-01 19:09:22',0,0);
INSERT INTO `vehicle_kms` VALUES (84,'2016-03-01 19:09:22','2016-03-01 19:09:22',0,0);
INSERT INTO `vehicle_kms` VALUES (85,'2016-03-02 07:01:23','2016-03-02 07:01:23',0,0);
INSERT INTO `vehicle_kms` VALUES (86,'2016-03-02 07:01:53','2016-03-02 07:01:53',0,0);
INSERT INTO `vehicle_kms` VALUES (87,'2016-03-02 08:15:09','2016-03-02 08:15:09',0,0);
INSERT INTO `vehicle_kms` VALUES (88,'2016-03-02 08:20:27','2016-03-02 08:20:27',0,0);
INSERT INTO `vehicle_kms` VALUES (89,'2016-03-02 13:20:04','2016-03-02 13:20:04',0,0);
INSERT INTO `vehicle_kms` VALUES (90,'2016-03-03 09:17:11','2016-03-03 09:17:11',0,0);
INSERT INTO `vehicle_kms` VALUES (91,'2016-03-03 18:00:48','2016-03-03 18:00:48',0,0);
INSERT INTO `vehicle_kms` VALUES (92,'2016-03-03 18:01:24','2016-03-03 18:01:24',0,0);
INSERT INTO `vehicle_kms` VALUES (93,'2016-03-03 20:42:12','2016-03-03 20:42:12',0,0);
INSERT INTO `vehicle_kms` VALUES (94,'2016-03-03 20:43:36','2016-03-03 20:43:36',0,0);
INSERT INTO `vehicle_kms` VALUES (95,'2016-03-04 09:06:30','2016-03-04 09:06:30',0,0);
INSERT INTO `vehicle_kms` VALUES (96,'2016-03-04 09:06:30','2016-03-04 09:06:30',0,0);
INSERT INTO `vehicle_kms` VALUES (97,'2016-03-04 17:54:49','2016-03-04 17:54:49',0,0);
INSERT INTO `vehicle_kms` VALUES (98,'2016-03-04 17:54:49','2016-03-04 17:54:49',0,0);
INSERT INTO `vehicle_kms` VALUES (99,'2016-03-04 17:57:32','2016-03-04 17:57:32',0,0);
INSERT INTO `vehicle_kms` VALUES (100,'2016-03-04 17:57:32','2016-03-04 17:57:32',0,0);
INSERT INTO `vehicle_kms` VALUES (101,'2016-03-04 17:58:00','2016-03-04 17:58:00',0,0);
INSERT INTO `vehicle_kms` VALUES (102,'2016-03-04 17:58:00','2016-03-04 17:58:00',0,0);
INSERT INTO `vehicle_kms` VALUES (103,'2016-03-04 17:59:04','2016-03-04 17:59:04',0,0);
INSERT INTO `vehicle_kms` VALUES (104,'2016-03-04 17:59:58','2016-03-04 17:59:58',0,0);
INSERT INTO `vehicle_kms` VALUES (105,'2016-03-04 18:01:12','2016-03-04 18:01:12',0,0);
INSERT INTO `vehicle_kms` VALUES (106,'2016-03-05 08:28:00','2016-03-05 08:28:00',0,0);
INSERT INTO `vehicle_kms` VALUES (107,'2016-03-05 20:09:55','2016-03-05 20:09:55',0,0);
INSERT INTO `vehicle_kms` VALUES (108,'2016-03-05 20:09:55','2016-03-05 20:09:55',0,0);
INSERT INTO `vehicle_kms` VALUES (109,'2016-03-06 13:07:08','2016-03-06 13:07:08',0,0);
INSERT INTO `vehicle_kms` VALUES (110,'2016-03-06 14:19:39','2016-03-06 14:19:39',11,5430);
INSERT INTO `vehicle_kms` VALUES (111,'2016-03-06 14:20:38','2016-03-06 14:20:38',3,130861);
INSERT INTO `vehicle_kms` VALUES (112,'2016-03-06 17:49:25','2016-03-06 17:49:25',5,94075);
INSERT INTO `vehicle_kms` VALUES (113,'2016-03-07 06:45:01','2016-03-07 06:45:01',5,95075);
INSERT INTO `vehicle_kms` VALUES (114,'2016-03-07 08:11:51','2016-03-07 08:11:51',3,131452);
INSERT INTO `vehicle_kms` VALUES (115,'2016-03-07 08:21:57','2016-03-07 08:21:57',5,97021);
INSERT INTO `vehicle_kms` VALUES (116,'2016-03-07 13:13:07','2016-03-07 13:13:07',6,40690);
INSERT INTO `vehicle_kms` VALUES (117,'2016-03-07 13:13:07','2016-03-07 13:13:07',6,40740);
INSERT INTO `vehicle_kms` VALUES (118,'2016-03-07 14:34:52','2016-03-07 14:34:52',4,212200);
INSERT INTO `vehicle_kms` VALUES (119,'2016-03-07 14:34:52','2016-03-07 14:34:52',4,213877);
INSERT INTO `vehicle_kms` VALUES (120,'2016-03-09 09:23:05','2016-03-09 09:23:05',3,131680);
INSERT INTO `vehicle_kms` VALUES (121,'2016-03-10 07:35:47','2016-03-10 07:35:47',10,121100);
INSERT INTO `vehicle_kms` VALUES (122,'2016-03-10 07:36:14','2016-03-10 07:36:14',7,232255);
INSERT INTO `vehicle_kms` VALUES (123,'2016-03-11 13:07:59','2016-03-11 13:07:59',4,214550);
INSERT INTO `vehicle_kms` VALUES (124,'2016-03-11 17:09:05','2016-03-11 17:09:05',5,97100);
INSERT INTO `vehicle_kms` VALUES (125,'2016-03-12 12:55:35','2016-03-12 12:55:35',4,214729);
INSERT INTO `vehicle_kms` VALUES (126,'2016-03-14 08:48:28','2016-03-14 08:48:28',3,134394);
INSERT INTO `vehicle_kms` VALUES (127,'2016-03-14 15:01:42','2016-03-14 15:01:42',3,134470);
INSERT INTO `vehicle_kms` VALUES (128,'2016-03-15 09:09:52','2016-03-15 09:09:52',9,124058);
INSERT INTO `vehicle_kms` VALUES (129,'2016-03-15 11:05:11','2016-03-15 11:05:11',13,15000);
INSERT INTO `vehicle_kms` VALUES (130,'2016-03-16 07:40:32','2016-03-16 07:40:32',5,97110);
INSERT INTO `vehicle_kms` VALUES (131,'2016-03-17 10:47:22','2016-03-17 10:47:22',5,97138);
INSERT INTO `vehicle_kms` VALUES (132,'2016-03-17 14:46:45','2016-03-17 14:46:45',6,41172);
INSERT INTO `vehicle_kms` VALUES (133,'2016-03-17 19:50:53','2016-03-17 19:50:53',4,216223);
INSERT INTO `vehicle_kms` VALUES (134,'2016-03-19 08:09:09','2016-03-19 08:09:09',6,41653);
INSERT INTO `vehicle_kms` VALUES (135,'2016-03-19 13:26:43','2016-03-19 13:26:43',6,41670);
INSERT INTO `vehicle_kms` VALUES (136,'2016-03-20 14:14:48','2016-03-20 14:14:48',6,41775);
INSERT INTO `vehicle_kms` VALUES (137,'2016-03-21 07:50:29','2016-03-21 07:50:29',5,98832);
INSERT INTO `vehicle_kms` VALUES (138,'2016-03-21 11:31:20','2016-03-21 11:31:20',4,216323);
INSERT INTO `vehicle_kms` VALUES (139,'2016-03-21 15:12:14','2016-03-21 15:12:14',6,41795);
INSERT INTO `vehicle_kms` VALUES (140,'2016-03-21 17:43:54','2016-03-21 17:43:54',5,98840);
INSERT INTO `vehicle_kms` VALUES (141,'2016-03-22 10:52:29','2016-03-22 10:52:29',3,137729);
INSERT INTO `vehicle_kms` VALUES (142,'2016-03-22 13:54:01','2016-03-22 13:54:01',3,137800);
INSERT INTO `vehicle_kms` VALUES (143,'2016-03-22 17:08:16','2016-03-22 17:08:16',4,216400);
INSERT INTO `vehicle_kms` VALUES (144,'2016-03-23 06:57:41','2016-03-23 06:57:41',3,137830);
INSERT INTO `vehicle_kms` VALUES (145,'2016-03-24 06:25:42','2016-03-24 06:25:42',4,216410);
INSERT INTO `vehicle_kms` VALUES (146,'2016-03-24 07:32:19','2016-03-24 07:32:19',3,138371);
INSERT INTO `vehicle_kms` VALUES (147,'2016-03-24 07:56:09','2016-03-24 07:56:09',9,126824);
INSERT INTO `vehicle_kms` VALUES (148,'2016-03-24 07:58:34','2016-03-24 07:58:34',4,216510);
INSERT INTO `vehicle_kms` VALUES (149,'2016-03-25 06:47:48','2016-03-25 06:47:48',8,228560);
INSERT INTO `vehicle_kms` VALUES (150,'2016-03-25 11:56:59','2016-03-25 11:56:59',6,43069);
INSERT INTO `vehicle_kms` VALUES (151,'2016-03-25 19:07:00','2016-03-25 19:07:00',4,217861);
INSERT INTO `vehicle_kms` VALUES (152,'2016-03-26 06:58:54','2016-03-26 06:58:54',5,98980);
INSERT INTO `vehicle_kms` VALUES (153,'2016-03-26 08:05:09','2016-03-26 08:05:09',5,101070);
INSERT INTO `vehicle_kms` VALUES (154,'2016-03-26 19:00:25','2016-03-26 19:00:25',3,138548);
INSERT INTO `vehicle_kms` VALUES (155,'2016-03-29 09:07:19','2016-03-29 09:07:19',8,231757);
INSERT INTO `vehicle_kms` VALUES (156,'2016-03-31 08:18:47','2016-03-31 08:18:47',3,138578);
INSERT INTO `vehicle_kms` VALUES (157,'2016-04-01 07:57:24','2016-04-01 07:57:24',6,44275);
INSERT INTO `vehicle_kms` VALUES (158,'2016-04-01 09:05:20','2016-04-01 09:05:20',3,138997);
INSERT INTO `vehicle_kms` VALUES (159,'2016-04-01 18:45:20','2016-04-01 18:45:20',6,44418);
INSERT INTO `vehicle_kms` VALUES (160,'2016-04-02 09:19:40','2016-04-02 09:19:40',3,139200);
INSERT INTO `vehicle_kms` VALUES (161,'2016-04-03 09:03:04','2016-04-03 09:03:04',6,44653);
INSERT INTO `vehicle_kms` VALUES (162,'2016-04-04 07:49:49','2016-04-04 07:49:49',4,219332);
INSERT INTO `vehicle_kms` VALUES (163,'2016-04-04 08:51:24','2016-04-04 08:51:24',3,139220);
INSERT INTO `vehicle_kms` VALUES (164,'2016-04-04 08:52:05','2016-04-04 08:52:05',4,219352);
INSERT INTO `vehicle_kms` VALUES (165,'2016-04-04 10:03:51','2016-04-04 10:03:51',11,8409);
INSERT INTO `vehicle_kms` VALUES (166,'2016-04-06 09:29:32','2016-04-06 09:29:32',5,104102);
INSERT INTO `vehicle_kms` VALUES (167,'2016-04-06 09:56:03','2016-04-06 09:56:03',7,250000);
INSERT INTO `vehicle_kms` VALUES (168,'2016-04-07 08:48:32','2016-04-07 08:48:32',6,47740);
INSERT INTO `vehicle_kms` VALUES (169,'2016-04-07 08:52:00','2016-04-07 08:52:00',5,104176);
INSERT INTO `vehicle_kms` VALUES (170,'2016-04-07 11:49:03','2016-04-07 11:49:03',5,104177);
INSERT INTO `vehicle_kms` VALUES (171,'2016-04-08 08:40:48','2016-04-08 08:40:48',11,8989);
INSERT INTO `vehicle_kms` VALUES (172,'2016-04-08 14:17:56','2016-04-08 14:17:56',5,104441);
INSERT INTO `vehicle_kms` VALUES (173,'2016-04-09 08:37:51','2016-04-09 08:37:51',6,47760);
INSERT INTO `vehicle_kms` VALUES (174,'2016-04-10 10:53:05','2016-04-10 10:53:05',6,48070);
INSERT INTO `vehicle_kms` VALUES (175,'2016-04-11 08:05:34','2016-04-11 08:05:34',5,105408);
INSERT INTO `vehicle_kms` VALUES (176,'2016-04-11 11:59:57','2016-04-11 11:59:57',10,122272);
INSERT INTO `vehicle_kms` VALUES (177,'2016-04-12 07:46:12','2016-04-12 07:46:12',6,48134);
INSERT INTO `vehicle_kms` VALUES (178,'2016-04-13 09:57:20','2016-04-13 09:57:20',6,48434);
INSERT INTO `vehicle_kms` VALUES (179,'2016-04-13 12:43:58','2016-04-13 12:43:58',5,105498);
INSERT INTO `vehicle_kms` VALUES (180,'2016-04-13 12:43:58','2016-04-13 12:43:58',5,106057);
INSERT INTO `vehicle_kms` VALUES (181,'2016-04-15 09:03:58','2016-04-15 09:03:58',5,106420);
INSERT INTO `vehicle_kms` VALUES (182,'2016-04-15 11:41:40','2016-04-15 11:41:40',5,106430);
INSERT INTO `vehicle_kms` VALUES (183,'2016-04-18 10:40:05','2016-04-18 10:40:05',9,132916);
INSERT INTO `vehicle_kms` VALUES (184,'2016-04-18 11:02:35','2016-04-18 11:02:35',5,1080050);
INSERT INTO `vehicle_kms` VALUES (185,'2016-04-18 12:23:13','2016-04-18 12:23:13',6,50705);
INSERT INTO `vehicle_kms` VALUES (186,'2016-04-18 12:23:40','2016-04-18 12:23:40',6,50706);
INSERT INTO `vehicle_kms` VALUES (187,'2016-04-22 14:41:01','2016-04-22 14:41:01',8,233888);
INSERT INTO `vehicle_kms` VALUES (188,'2016-04-23 08:01:16','2016-04-23 08:01:16',6,51861);
INSERT INTO `vehicle_kms` VALUES (189,'2016-04-23 08:02:26','2016-04-23 08:02:26',11,11084);
INSERT INTO `vehicle_kms` VALUES (190,'2016-04-25 08:17:40','2016-04-25 08:17:40',6,52251);
INSERT INTO `vehicle_kms` VALUES (191,'2016-04-25 08:25:00','2016-04-25 08:25:00',8,234720);
INSERT INTO `vehicle_kms` VALUES (192,'2016-04-26 07:49:25','2016-04-26 07:49:25',11,11810);
INSERT INTO `vehicle_kms` VALUES (193,'2016-04-27 17:34:47','2016-04-27 17:34:47',6,52324);
INSERT INTO `vehicle_kms` VALUES (194,'2016-04-28 17:52:07','2016-04-28 17:52:07',6,52704);
INSERT INTO `vehicle_kms` VALUES (195,'2016-04-29 16:45:15','2016-04-29 16:45:15',6,52781);
INSERT INTO `vehicle_kms` VALUES (196,'2016-04-30 09:53:34','2016-04-30 09:53:34',8,236523);
INSERT INTO `vehicle_kms` VALUES (197,'2016-05-02 07:24:34','2016-05-02 07:24:34',6,53412);
INSERT INTO `vehicle_kms` VALUES (198,'2016-05-03 09:42:42','2016-05-03 09:42:42',3,140878);
INSERT INTO `vehicle_kms` VALUES (199,'2016-05-04 08:13:16','2016-05-04 08:13:16',6,53962);
INSERT INTO `vehicle_kms` VALUES (200,'2016-05-04 12:57:26','2016-05-04 12:57:26',6,54039);
INSERT INTO `vehicle_kms` VALUES (201,'2016-05-05 09:40:09','2016-05-05 09:40:09',8,237103);
/*!40000 ALTER TABLE `vehicle_kms` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table vehicle_services
#

DROP TABLE IF EXISTS `vehicle_services`;
CREATE TABLE `vehicle_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `vehicle_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Redovan',
  `from` date DEFAULT NULL,
  `to` date DEFAULT NULL,
  `service_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parts_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `vehicle_km` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `service_id` int(11) DEFAULT NULL,
  `service_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table vehicle_services
#
LOCK TABLES `vehicle_services` WRITE;
/*!40000 ALTER TABLE `vehicle_services` DISABLE KEYS */;

INSERT INTO `vehicle_services` VALUES (3,'2015-12-18 10:48:41','2015-12-31 12:24:28',3,'Redovan','2015-11-10','2015-11-11','','','1','','- redoviti servis napravljen ranije jer je vozilo išlo u dugu rentu','116000',5,3);
INSERT INTO `vehicle_services` VALUES (4,'2015-12-21 11:18:54','2015-12-31 12:23:45',8,'Redovan','2015-12-21','2015-12-22','755','','1','','- redoviti servis \r\n- kugla, ulje, filter ulja\r\n- servis i djelovi zajedno','219761',5,3);
INSERT INTO `vehicle_services` VALUES (5,'2015-12-21 11:25:45','2015-12-31 12:24:48',5,'Redovan','2015-11-10','2015-11-10','','','1','','- redoviti servis','80687',5,3);
INSERT INTO `vehicle_services` VALUES (6,'2015-12-21 11:28:12','2015-12-31 12:24:05',9,'Redovan','2015-11-20','2015-11-21','','','1','','- reoviti servis i montaža guma','111396',5,3);
INSERT INTO `vehicle_services` VALUES (7,'2015-12-23 09:24:04','2015-12-31 12:23:02',9,'Izvanredni','2015-12-22','2015-12-22','3938','','1','61','Vozilo dao na popravak djelatnik ZG Montaže,\r\nhlanjak ispušnih plinova. Plaćanje kada nam\r\nuplati ZG Montaža.','114121',1329,3);
INSERT INTO `vehicle_services` VALUES (8,'2015-12-23 11:28:52','2016-01-12 12:59:34',2,'Pregled','2015-12-22','2015-12-29','175','','1','','- pregled nakon NEKOM OBRTA\r\n- promjenili žarulje, nadolili ulje, komplet pregled\r\n- nakon pregleda ide Mariju limaru\r\nna poliranje i sređivanje par oštećenja','280002',5,3);
INSERT INTO `vehicle_services` VALUES (9,'2015-12-23 11:30:22','2016-04-13 14:42:30',4,'Izvanredni','2015-12-23','2016-01-05','2500','','1','','- poliranje lijeva strana\r\n- prednji kvadrat izbočenja\r\n- oblaganje stranica unutarnji\r\n- poliranje haube od kamančića\r\n- NEKOMU naplaćen popravak\r\n- nakon njega ide preko kaska u\r\nAUTOKUĆU BAOTIĆ','207300',7,3);
INSERT INTO `vehicle_services` VALUES (10,'2015-12-25 10:42:42','2016-01-29 12:52:44',3,'Pregled','2016-01-11','2016-01-13','0','','1','','- pregled GRATS','123101',1331,3);
INSERT INTO `vehicle_services` VALUES (14,'2015-12-31 14:37:41','2016-01-14 09:20:02',4,'Izvanredni','2016-01-05','2016-01-12','0','0','1','','- popravak po kasku\r\n- gotov','207318',1331,3);
INSERT INTO `vehicle_services` VALUES (15,'2016-01-01 22:33:08','2016-01-05 14:52:38',8,'Izvanredni','2016-01-05','2016-01-05','69','','1','','- plaćeno gotovinom','',1341,3);
INSERT INTO `vehicle_services` VALUES (17,'2016-01-10 13:58:13','2016-04-13 14:41:48',4,'Pregled','2016-01-14','2016-01-19','0','','1','','- pregled GRATIS \r\n-Rus kava\r\n','207318',1331,3);
INSERT INTO `vehicle_services` VALUES (19,'2016-01-19 08:54:56','2016-02-17 08:39:47',4,'Izvanredni','2016-01-19','2016-01-19','353','0','1','','Zamjena kugle','207318',1331,3);
INSERT INTO `vehicle_services` VALUES (20,'2016-01-19 12:48:45','2016-01-19 12:48:45',12,'Redovan','2015-07-20','2015-07-21','803','','1','','sve ok','10800',1331,3);
INSERT INTO `vehicle_services` VALUES (21,'2016-01-19 12:50:18','2016-01-19 12:51:56',12,'Pregled','2015-11-19','2015-11-19','0','0','1','','- pregled kočnica i tekućina\r\n- sve ok\r\n- nisu htjeli naplatiti','13500',1331,3);
INSERT INTO `vehicle_services` VALUES (22,'2016-01-19 13:05:41','2016-01-19 13:05:41',10,'Pregled','2015-10-09','2015-10-09','62','0','1','','- pregled prije Zagreb Montaže','110008',5,3);
INSERT INTO `vehicle_services` VALUES (23,'2016-01-24 11:04:42','2016-01-24 11:04:42',6,'Redovan','2015-05-19','2015-05-19','0','0','1','','- odkonzerviravanje vozila','1',1331,3);
INSERT INTO `vehicle_services` VALUES (24,'2016-01-24 11:05:35','2016-01-24 11:05:35',11,'Redovan','2015-12-30','2015-12-30','0','0','1','','- odkonzerviravanje vozila','7',1331,3);
INSERT INTO `vehicle_services` VALUES (25,'2016-01-24 11:06:20','2016-01-24 11:06:20',11,'Pregled','2015-12-30','2015-12-30','0','0','1','','- vozilo pregledano kod kupnje','7',1331,3);
INSERT INTO `vehicle_services` VALUES (26,'2016-01-25 09:26:14','2016-01-25 16:54:07',3,'Izvanredni','2016-01-25','2016-01-25','0','0','1','','Kut zadnjeg desnog branika ogreban, popravak platio najmoprimac Mario Netzer','',7,3);
INSERT INTO `vehicle_services` VALUES (28,'2016-01-27 08:42:18','2016-02-12 09:00:53',11,'Izvanredni','2016-01-27','2016-01-27','138','','1','','- krpanje zadnje ljeve gume','',1331,3);
INSERT INTO `vehicle_services` VALUES (29,'2016-01-28 09:48:34','2016-02-19 10:17:04',11,'Izvanredni','2016-01-28','2016-02-12','0','0','1','','Popravak kliznih vrata po kasku.','',1331,3);
INSERT INTO `vehicle_services` VALUES (30,'2016-02-03 14:40:29','2016-04-11 13:21:46',8,'Izvanredni','2016-02-03','2016-02-04','405','467.40','1','','- promjena ležaja','',1331,3);
INSERT INTO `vehicle_services` VALUES (31,'2016-02-06 08:01:34','2016-02-06 08:01:34',5,'Pregled','2016-01-20','2016-01-20','0','0','1','','- pregled gratis ','90050',1331,3);
INSERT INTO `vehicle_services` VALUES (32,'2016-02-09 08:20:12','2016-02-10 08:10:03',3,'Izvanredni','2016-02-09','2016-02-10','75','0','1','','- atestiranje\r\n- placeno gotovinom R1','',1341,3);
INSERT INTO `vehicle_services` VALUES (33,'2016-02-16 09:10:48','2016-04-11 13:22:22',5,'Izvanredni','2016-02-16','2016-02-17','87.5','106.25','1','','- pregled i ev.zamjena kočnica\r\n- sve ok. Zamjenske plocice pa senzor ne radi\r\n- dolili litru ulja','',1331,3);
INSERT INTO `vehicle_services` VALUES (34,'2016-02-21 20:39:19','2016-04-27 14:38:06',6,'Redovan','2016-03-03','2016-03-03','403.32','1417.83','1','','-  redovan servis u Baotić\r\n','39960',1331,3);
INSERT INTO `vehicle_services` VALUES (36,'2016-02-23 10:16:43','2016-04-09 10:47:49',11,'Izvanredni','2016-04-04','2016-04-04','','','1','','- pregled po uputama Renaulta','',1331,3);
INSERT INTO `vehicle_services` VALUES (37,'2016-02-29 09:25:04','2016-04-27 14:37:04',5,'Pregled','2016-03-07','2016-03-07','100','','1','','Pregled','97021',1331,3);
INSERT INTO `vehicle_services` VALUES (38,'2016-03-03 09:23:48','2016-03-03 17:58:18',6,'Pregled','2016-03-03','2016-03-03','0','0','1','','-pregled uz redoviti servis','39960',1331,3);
INSERT INTO `vehicle_services` VALUES (39,'2016-03-04 20:19:42','2016-04-27 14:36:29',3,'Pregled','2016-03-07','2016-03-07','100','','1','','Pregled','131452',1331,3);
INSERT INTO `vehicle_services` VALUES (40,'2016-03-08 08:08:18','2016-03-31 10:07:17',4,'Izvanredni','2016-03-08','2016-03-08','','387.96','1','','1 x 5 litara ulja\r\n4 x 4 litre ulja\r\n6 x h7 zarulje','',1386,3);
INSERT INTO `vehicle_services` VALUES (41,'2016-03-18 08:17:10','2016-03-24 07:27:27',4,'Izvanredni','2016-03-22','2016-03-23','1000','','1','','- popravka oštećenja Laura montaža\r\n- platio Igor gotovinom\r\n','',7,3);
INSERT INTO `vehicle_services` VALUES (42,'2016-03-21 08:13:30','2016-03-24 07:28:06',5,'Izvanredni','2016-03-21','2016-03-21','200','','1','','- popravak udubine kod desnih vrata\r\n- popravak naplatili od Tihomira Špruka\r\n+poliranje gornje stranice sitno\r\n- platio Igor gotovinom','',7,3);
INSERT INTO `vehicle_services` VALUES (43,'2016-03-21 09:59:43','2016-03-24 07:28:31',6,'Izvanredni','2016-03-21','2016-03-22','200','','1','','- poliranje sitnih ogrebotina\r\n- platio Igor gotovinom','',7,3);
INSERT INTO `vehicle_services` VALUES (44,'2016-03-23 13:00:52','2016-04-27 14:37:24',4,'Redovan','2016-03-29','2016-03-29','237.25','166.07','1','','-pregled vozila\r\n-uz racun izdali potvrdu o pregledu','217875',1331,3);
INSERT INTO `vehicle_services` VALUES (45,'2016-03-23 13:01:48','2016-03-29 11:13:42',4,'Pregled','2016-03-29','2016-03-29','','','1','','- pregled se treba upisati kada i redoviti servis\r\njer tako prati program','217875',1331,3);
INSERT INTO `vehicle_services` VALUES (46,'2016-03-24 08:48:10','2016-03-24 13:40:46',13,'Izvanredni','2016-03-24','2016-03-24','60','','1','','PP aparat atestranje, plaćeno gotovinom','',1341,3);
INSERT INTO `vehicle_services` VALUES (47,'2016-03-24 11:52:41','2016-03-24 11:52:41',4,'Izvanredni','2016-03-24','2016-03-24','','534','1','','Ulje i filteri za redoviti servis','',1386,3);
INSERT INTO `vehicle_services` VALUES (48,'2016-03-25 12:09:37','2016-03-29 11:51:20',6,'Izvanredni',NULL,'2016-03-25','400','','1','','- popravak ostećenja Sanitaria Dental\r\n- platio Igor','',7,3);
INSERT INTO `vehicle_services` VALUES (50,'2016-03-25 19:13:11','2016-03-28 07:44:10',4,'Izvanredni','2016-03-25','2016-03-29','100','','1','','Ostavio Igoru Šamecu da kemijsko sjedala.','',0,3);
INSERT INTO `vehicle_services` VALUES (51,'2016-03-29 09:14:35','2016-04-25 15:10:27',8,'Pregled','2016-03-29','2016-03-29','0','','1','','- montaža guma i pregled\r\n- Rimay 100kn\r\n- poništena greška erg ventil koja se povremeno pojavljuje','231757',6,3);
INSERT INTO `vehicle_services` VALUES (52,'2016-03-30 20:29:45','2016-04-25 15:11:06',8,'Izvanredni','2016-04-11','2016-04-13','600','','0','','- gume\r\n- erg ventil\r\n- vrata desna\r\n- sjedalo\r\n- retrovizor\r\n- prednji diskovi i ploćice','',6,3);
INSERT INTO `vehicle_services` VALUES (54,'2016-04-06 10:02:04','2016-04-25 14:43:06',5,'Izvanredni','2016-05-09','2016-05-10','1700','','1','','- popravak limarije što je napravio Miljenko Sladović\r\n- dio popravljen ostalo 18.04. nakonpregleda u Baotiću','',7,1);
INSERT INTO `vehicle_services` VALUES (55,'2016-04-07 10:03:36','2016-04-09 07:02:37',6,'Izvanredni','2016-04-07','2016-04-08','','','1','','Zamjena ljevog fara, popravak plaća direktno Interier Mont.','',7,3);
INSERT INTO `vehicle_services` VALUES (56,'2016-04-09 10:43:46','2016-04-18 09:38:13',6,'Izvanredni','2016-04-10','2016-04-10','100','','1','','- kemijsko kabine zbog flekavih sjedala\r\n- radi Ivan iz praone doma','',0,3);
INSERT INTO `vehicle_services` VALUES (57,'2016-04-11 10:22:29','2016-04-11 10:22:29',6,'Izvanredni','2016-04-11','2016-04-11','333.59','','1','','Promjena guma i čuvanje','',615,3);
INSERT INTO `vehicle_services` VALUES (58,'2016-04-11 10:24:02','2016-04-11 10:24:02',6,'Izvanredni','2016-04-11','2016-04-11','','2278','1','','- Ljetne gume\r\n- Plaćeno Amexom na 6 rata','',615,3);
INSERT INTO `vehicle_services` VALUES (59,'2016-04-11 10:25:27','2016-04-11 10:25:27',8,'Izvanredni','2016-04-11','2016-04-11','','2167.50','1','','- ljetne gume\r\n- plaćeno Amexom na 6 rata','',615,3);
INSERT INTO `vehicle_services` VALUES (60,'2016-04-11 10:26:35','2016-04-11 10:26:35',7,'Izvanredni','2016-04-11','2016-04-11','','2167.5','1','','- ljetne gume\r\n- plaćeno Amexom na 6 rata','',615,3);
INSERT INTO `vehicle_services` VALUES (61,'2016-04-11 10:28:03','2016-04-11 10:28:03',10,'Izvanredni','2016-04-11','2016-04-11','','2167.5','1','','- ljetne gume\r\n- plaćeno Amexom na 6 rata','',615,3);
INSERT INTO `vehicle_services` VALUES (62,'2016-04-11 10:28:58','2016-04-11 10:28:58',9,'Izvanredni','2016-04-11','2016-04-11','','2167.5','1','','- ljetne gume\r\n- plaćeno Amexom na 6 rata','',615,3);
INSERT INTO `vehicle_services` VALUES (63,'2016-04-11 12:22:48','2016-04-11 12:22:48',8,'Izvanredni','2016-04-11','2016-04-11','','800','1','','Placen gotovinom ','',6,3);
INSERT INTO `vehicle_services` VALUES (64,'2016-04-14 08:02:39','2016-05-04 14:26:22',5,'Pregled','2016-04-18','2016-04-18','242.35','128.09','0','','- redoviti pregled+punjenje klime 500grama','108050',1331,1);
INSERT INTO `vehicle_services` VALUES (65,'2016-04-14 08:04:20','2016-04-18 13:40:47',5,'Izvanredni','2016-04-18','2016-04-18','75','','1','','- atestiranje  PP aparata','',1341,3);
INSERT INTO `vehicle_services` VALUES (66,'2016-04-15 09:39:22','2016-04-15 10:41:11',5,'Izvanredni','2016-04-15','2016-04-15','333.59','','1','','- montaža i čuvanje guma','',615,3);
INSERT INTO `vehicle_services` VALUES (67,'2016-04-15 12:23:06','2016-04-15 12:23:06',8,'Izvanredni','2016-04-15','2016-04-15','','','0','','- diskovi i disk ploćice je uzeo Jurić na naš revers\r\n- oko 1500kuna je račun','',1386,3);
INSERT INTO `vehicle_services` VALUES (68,'2016-04-18 10:43:43','2016-04-20 08:02:33',9,'Izvanredni','2016-04-18','2016-04-20','250','','1','','- kemisko\r\n- radi Ivan iz praona\r\n- nakon toga ide na pregled','',0,3);
INSERT INTO `vehicle_services` VALUES (69,'2016-04-18 10:44:31','2016-04-28 10:00:58',9,'Izvanredni','2016-04-19','2016-04-19','333.59','','1','','- montaža ljetnih guma + hotel za zimske','132916',615,3);
INSERT INTO `vehicle_services` VALUES (70,'2016-04-19 06:54:05','2016-04-19 06:54:05',6,'Pregled','2016-04-18','2016-04-18','','','1','','- pregled, bez plačanja sve uredu','50000',1331,3);
INSERT INTO `vehicle_services` VALUES (71,'2016-04-19 16:15:36','2016-05-05 06:53:16',9,'Pregled','2016-04-20','2016-04-22','','','0','','- redoviti pregled nakon ZM\r\n- nije naplatio pregled','132950',6,3);
INSERT INTO `vehicle_services` VALUES (72,'2016-04-21 13:04:02','2016-04-22 07:55:32',8,'Izvanredni','2016-04-21','2016-04-22','200','','1','','Kemisko, radi Ivek iz praone doma.','',0,3);
INSERT INTO `vehicle_services` VALUES (73,'2016-04-27 21:09:05','2016-04-27 21:09:05',10,'Izvanredni','2016-05-09','2016-05-09','','','','','- kemisko, nakon toga odmah u Auroart','',0,1);
INSERT INTO `vehicle_services` VALUES (74,'2016-04-27 21:10:41','2016-04-27 21:10:41',10,'Redovan','2016-05-10','2016-05-10','','','','','- redoviti servis + pregled klime\r\n- nakon servisa u Koturić staviti gume','',6,1);
INSERT INTO `vehicle_services` VALUES (75,'2016-04-28 08:46:42','2016-04-28 10:00:12',12,'Izvanredni','2016-04-28','2016-04-28','289.97','','1','','- montaža ljetnih guma\r\n- skladištenje zimskih','',615,3);
INSERT INTO `vehicle_services` VALUES (76,'2016-04-28 11:01:34','2016-04-28 13:21:20',7,'Izvanredni','2016-04-28','2016-04-28','333.59','','0','','- montaža ljetnih guma\r\n- skladišetnje zimskih','237103',615,3);
INSERT INTO `vehicle_services` VALUES (77,'2016-04-28 11:02:40','2016-05-04 16:47:36',7,'Redovan','2016-04-28','2016-05-04','1200','','0','','- redoviti servis\r\n- popravak onog ispod mjenjača\r\n- pregled klime\r\n- i ostale stvari\r\n- sajle mjenjača ','237103',6,3);
INSERT INTO `vehicle_services` VALUES (78,'2016-04-28 13:23:11','2016-05-03 20:41:49',7,'Pregled','2016-04-28','2016-05-04','0','0','1','','- cijena pregleda bio kada i redoviti servis ','237103',6,3);
INSERT INTO `vehicle_services` VALUES (79,'2016-04-29 16:15:47','2016-04-29 18:09:14',7,'Izvanredni','2016-04-29','2016-04-29','2850','','0','','- Jurić uzeo na revers za redoviti servis\r\n+ diskovi i ploćice\r\n+ zatezac remena\r\nOkvirna je cijena','',1386,3);
INSERT INTO `vehicle_services` VALUES (80,'2016-05-03 11:09:31','2016-05-03 20:39:52',3,'Pregled','2016-05-03','2016-05-03','','','1','','- pregled u cijeni kada su mjenjane ploćice i gume','140900',6,3);
INSERT INTO `vehicle_services` VALUES (81,'2016-05-03 15:15:10','2016-05-04 16:46:20',3,'Izvanredni','2016-05-03','2016-05-03','360','','0','','- zadnje ploćice promjena\r\n- pregled vozila\r\n- zamjena zimskih guma','',6,3);
INSERT INTO `vehicle_services` VALUES (82,'2016-05-03 15:15:57','2016-05-03 16:52:49',3,'Izvanredni','2016-05-03','2016-05-03','','257','0','','Ploćice zadnje uzeo Jurić','',1386,3);
INSERT INTO `vehicle_services` VALUES (84,'2016-05-04 14:04:57','2016-05-05 06:52:38',7,'Izvanredni','2016-05-04','2016-05-04','85','','1','','- Pp aparat pregled\r\n- plaćeno gotovinom','',1452,3);
/*!40000 ALTER TABLE `vehicle_services` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table vehicles
#

DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `brand` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `partner` tinyint(4) NOT NULL DEFAULT '0',
  `year` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'White',
  `color_hex` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#FFFFFF',
  `chassis_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `leasing` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `leasing_amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tire_size` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pp_expires` date DEFAULT NULL,
  `casco_expires` date DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `licence_plate` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `licence_expires` date DEFAULT NULL,
  `ppa_expires` date DEFAULT NULL,
  `ao_expires` date DEFAULT NULL,
  `ao_house` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `kasko_expires` date DEFAULT NULL,
  `kasko_house` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `km` int(10) unsigned NOT NULL DEFAULT '0',
  `service_km` int(10) unsigned NOT NULL DEFAULT '50000',
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  `show_on_calendar` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table vehicles
#
LOCK TABLES `vehicles` WRITE;
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;

INSERT INTO `vehicles` VALUES (3,'2015-12-17 20:05:57','2016-05-03 10:39:02','Renault Master RI-629-UP','Renault','Master','Teretni',1,'2014','Bijela','#aa15c4','VF1MAF4DE49720410','IMPULS LEASING','3090','225/65/16c','2016-11-06',NULL,'-provjereni podatci\r\n- operativni leasing\r\n- EURO+ asistencija\r\n- kasko se automatski obnavlja na rate\r\n- ljetne gume u AUTODUBRAVI d.o.o.','RI-629-UP','2017-05-06','2017-02-15','2017-05-06','UNIQA OSIGURANJE','2017-05-06','ALLIANZ OSIGURANJE',140878,0,'19,20,21,22,87',1);
INSERT INTO `vehicles` VALUES (4,'2015-12-18 10:08:28','2016-04-04 08:52:05','Fiat Ducato RI-654-UP','Fiat','Ducato','Teretni',1,'2011','Bijela','#dc82e5','ZFA25000001961707','IMPULS LEASING','2524','215/70/15c','2016-10-20',NULL,'- provjereni podatci\r\n- financijski leasing\r\n- EURO+ asistencija\r\n- kasko se automatski obnavlja na rate\r\n- ljetne gume u AUTODUBRAVI d.o.o.','RI-654-UP','2017-04-20','2016-08-31','2017-04-20','UNIQA OSIGURANJE','2016-09-24','ALLIANZ OSIGURANJE',219352,0,'23,24,25,26,81',1);
INSERT INTO `vehicles` VALUES (5,'2015-12-18 10:15:19','2016-04-25 10:57:48','Renault Master RI-838-VH','Renault ','Master','Teretni',1,'2013','Bijela','#b77993','VF1MAF4DE49002123','IMPULS LEASING','2830','225/65/16c','2016-09-29',NULL,'- svi podatci provjereni\r\n- financijski leasing\r\n- EURO+ asistencija\r\n- kasko se automatski obnavlja na rate\r\n- zimske gume kod Koturića','RI-838-VH','2016-09-29','2017-04-28','2016-09-29','UNIQA OSIGURANJE','2017-01-26','ALLIANZ OSIGURANJE',109912,40000,'28,30,31,66',1);
INSERT INTO `vehicles` VALUES (6,'2015-12-18 10:20:02','2016-05-04 12:57:26','Renault Master RI-540-VN','Renault','Master','Teretni',1,'2015','Bijela','#421744','VF1MAF4SE51411871','IMPULS LEASING','3197','225/65/16c','2016-11-26',NULL,'- svi podatci provjereni\r\n- financijski leasing\r\n - EURO+ asistencija\r\n - kasko se automatski obnavlja na rate\r\n- zimske gume kod Koturića','RI-540-VN','2016-05-27','2016-05-26','2017-05-26','UNIQA OSIGURANJE','2017-05-10','ALLIANZ OSIGURANJE',54039,40000,'32,33,34,35,88',1);
INSERT INTO `vehicles` VALUES (7,'2015-12-18 10:27:13','2016-05-04 14:05:33','Opel Vivaro RI-121-SR','Opel','Vivaro','Putnički',1,'2011','Met.siva','#12d300','W0LJ7BHBSBV643848','IMPULS LEASING','2103','215/65/16c','2016-06-30',NULL,'- finacijski leasing\r\n- kasko u Eurohercu treba\r\nsvake godine tražiti na rate\r\nkontaktirati Milicu Mišić','RI-121-SR','2016-06-30','2017-05-05','2017-05-04','UNIQA OSIGURANJE','2016-08-30','EUROHERC OSIGURANJE',237103,40000,'36,37,38,39,89',1);
INSERT INTO `vehicles` VALUES (8,'2015-12-18 10:31:35','2016-05-05 09:40:09','Opel Vivaro RI-165-UA','Opel','Vivaro','Putnički',1,'2011','Met.siva','#1f910c','W0LJ7BHB6BV623525','IMPULS LEASING','2333','215/65/16c','2016-06-14',NULL,'-svi podatci provjereni\r\n- financijski leasing\r\n- EURO+ asistencija\r\n- kasko se automatski sam obnavlja\r\n- ljetne gume u AUTODUBRAVI d.o.o.','RI-165-UA','2016-06-14','2017-01-04','2016-06-14','CROATIA OSIGURANJE d.d.','2017-04-18','ALLIANZ OSIGURANJE',237103,40000,'40,41,42',1);
INSERT INTO `vehicles` VALUES (9,'2015-12-18 10:37:48','2016-04-21 13:29:35','VW Transporter T5 RI-668-TC','Volkswagen','Transporter T5','Putnički',1,'2011','Bijela','#095108','WV2ZZZ7HZCH029963','IMPULS LEASING','2830','215/65/16c','2017-03-28',NULL,'- svi podatci provjereni\r\n-financijski leasing\r\n- EURO+ asistencija\r\n- kasko se automatski\r\nobnavlja na rate\r\n- zimske gume u Koturić d.o.o.','RI-668-TC','2016-09-28','2016-09-06','2016-09-28','UNIQA OSIGURANJE','2016-07-15','ALLIANZ OSIGURANJE',132916,30000,'43,44,45,46',1);
INSERT INTO `vehicles` VALUES (10,'2015-12-18 10:42:55','2016-04-11 11:59:57','Opel Vivaro RI-353-UE','Opel','Vivaro','Putnički',1,'2013','Met.siva','#4cef66','W0LJ7B7BSCV639987','IMPULS LEASING','2830','215/65/16c','2016-05-18',NULL,'- podatci provjereni\r\n- finansijski leasing\r\n- EURO+ asistencija\r\n- kasko se automatski\r\nobnavlja na rate','RI-353-UE','2016-07-15','2015-05-01','2016-07-15','UNIQA OSIGURANJE','2016-06-18','ALLIANZ OSIGURANJE',122272,40000,'48,49,50,51',1);
INSERT INTO `vehicles` VALUES (11,'2015-12-18 11:48:40','2016-04-26 07:49:25','Renault Trafic DA-760-DL','Renault','Trafic','Putnički',1,'2015','Bijela','#449142','VF1JL000853624368','IMPULS LEASING','3850','2015/65/16c','2016-12-30',NULL,'- provjereni svi podatci\r\n- financiski leasing\r\n- EURO+ asistencija\r\n- kasko ugovoren do 17.12.2018','','2016-12-30','2016-12-30','2016-12-28','ALLIANZ OSIGURANJE','2018-12-17','ALLINAZ OSIGURANJE',11810,40000,'62,63,64,65,70',1);
INSERT INTO `vehicles` VALUES (12,'2015-12-22 08:56:31','2016-04-11 10:32:20','Nissan Qashqai RI-107-UO','Nissan','Qashqai','Putnički',0,'2014','Crna','#ffffff','SJNFEAJ11U1014189','IMPULS LEASING','2486','215/65/17','2017-02-18',NULL,'-provjerieni podatci\r\n- operativni leasing\r\n- EURO+ asistencija\r\n- kasko se automatski \r\nobnavlja na rate\r\n- zimske gume kod Koturića','RI-107-UO','2017-03-02','2016-08-20','2017-03-02','CROATIA OSIGURANJE','2017-02-18','ALLIANZ OSIGURANJE',15600,30000,'52,53,54,55,71',0);
INSERT INTO `vehicles` VALUES (13,'2015-12-22 09:20:29','2016-03-23 19:10:27','VW Polo RI-747-VJ','Volksagen','Polo','Putnički',0,'2014','Bijela','#ffffff','WVWZZZ6RZFY035073','IMPULS LEASING','1840','215/45/R15   Zima 185/60/R15(stavljene 12/2015 na 15200km)','2015-04-13',NULL,'- provjereni podatci\r\n- financijski leasing\r\n-EURO+ asistencija\r\n- kasko se automatski \r\nobnavlja svake godine','RI-747-VJ','2016-06-09','2017-03-18','2016-06-09','CROATIA OSIGURANJE','2020-04-13','ALLIANZ OSIGURANJE',15000,0,'58,74,75,76',0);
INSERT INTO `vehicles` VALUES (14,'2015-12-22 09:29:27','2016-01-15 14:04:21','Audi A3 ZG-8325-FG','Audi','A3','Putnički',0,'2003','Siva -s efektom','#FFFFFF','WAUZZZ8P84A056591','American card','1823','225/45/R17',NULL,NULL,'Boris će ubaciti podatke','ZG-8325-FG','2016-07-07','2016-09-01',NULL,'Uniqa',NULL,'',180000,0,'',0);
INSERT INTO `vehicles` VALUES (15,'2015-12-22 09:33:11','2016-02-11 10:34:57','Partner - teretni','','','Partnerski teretni',0,'1990','','#37e07b','','','','',NULL,NULL,'','',NULL,NULL,NULL,'',NULL,'',0,0,'',1);
INSERT INTO `vehicles` VALUES (16,'2015-12-22 09:35:04','2016-02-11 10:35:11','Partner - putnički','','','Partnerski putnički',0,'1990','','#2cad20','','','','',NULL,NULL,'','',NULL,NULL,NULL,'',NULL,'',1200,0,'',1);
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;
UNLOCK TABLES;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
