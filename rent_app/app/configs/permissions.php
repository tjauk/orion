<?php

return array(

    // rent app
    'menu.dashboard'		=> "Show Dashboard page",
    'menu.calendar'			=> "Show Calendar page",
    'menu.booking'			=> "Show Bookings page",
    'menu.contact'			=> "Show Contacts page",
    'menu.vehicle'			=> "Show Vehicles page",
    'menu.vehicle-service'	=> "Show Vehicle Services page",
    'manage.vehicle-service'=> "Manage Vehicle Services",
    'menu.damage'			=> "Show Vehicle Damages page",
    'manage.damage'         => "Manage Vehicle Damages",
    'menu.checklist'		=> "Show Vehicle Checklists page",
    'menu.report'		    => "Show Reports page",
    'show.totals'			=> "Show Money Totals",

);
