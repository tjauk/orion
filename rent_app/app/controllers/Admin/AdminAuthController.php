<?php

class AdminAuthController extends Controller
{


    function index()
    {
        return $this->getLogin();
    }

    function postIndex()
    {
        return $this->postLogin();
    }

    function getLogin()
    {
        if( Auth::guest() ){
            Doc::view('admin/user/login');
        }else{
            return redirect('/');
        }
    }

    function postLogin()
    {
        $post = Request::post();
        if( empty($post['username']) or empty($post['password']) ){
            return redirect('admin/auth')->msg(t('auth.credentials_empty'));
        }
        $credentials = ['username'=>$post['username'],'password'=>$post['password']];
        if( Auth::attempt($credentials) ){
            if( Auth::user()->usergroup=='serviser' ){
                return redirect('admin/vehicle-service')->msg(t('auth.welcome'));
            }
            if( Auth::user()->usergroup=='mehanicar' ){
                return redirect('admin/damage')->msg(t('auth.welcome'));
            }
            $intended = Session::get('intended');
            if( !empty($intended) ){
                Session::put('intended',null);
                return redirect($intended);
            }
            return redirect('admin/dashboard')->msg(t('auth.welcome'));
        }else{
            return redirect('admin/auth')->msg(t('auth.invalid_credentials'));
        }
        return redirect('admin/auth')->msg(t('auth.welcome'));
    }

    function logout()
    {
        Auth::logout();
        return redirect('admin/auth/login');
    }

}
