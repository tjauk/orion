<?php
class AdminChecklistController extends AdminController
{

    public $model = 'Checklist';
    public $baseurl = '/admin/checklist';


    function index()
    {
        $model = $this->model;
        
        Doc::title("Provjera vozila");

        $items = new $model;

        $buttons = Forms::make();
        $buttons->btnAdd('Nova provjera',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'from,to' );
        }
        // search vehicle
        if( $filter->notEmpty('vehicle_id') ){
            $items = $items->whereVehicleId( $filter->vehicle_id );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Pretraga');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('po ključnim riječima');
        $form->label('Vozilo');
        $form->select2('vehicle_id')->options(Vehicle::options());
        $form->label('Per page');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');

        $items = $items->orderBy('check_date','DESC');
        $items = $filter->paginate( $items )->get();

        $table = Table::make()
                ->headings([
                    'Datum provjere',
                    'Vozilo',
                    'Trenutna kilometraža',
                    'Pregledano na km',
                    'Checked',
                    'Akcije']);

        foreach($items as $item){
            $item->unpackData();
            $data = [];
            $data[] = UI::date($item->check_date);
            $data[] = $item->vehicle->title;
            $data[] = $item->vehicle->km.' km';
            $data[] = $item->kilometraza_vozila.' km';
            $data[] = $item->countNotChecked().' / '.$item->countCheckOK().' / '.$item->countCheckNotOK();
            $actions = UI::btnEdit($item->id,'checklist');
            $actions.= UI::btnDelete($item->id,'checklist');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title("Provjera vozila");

        $item = $model::firstOrNew(['id'=>$id]);

        $data = Request::post();
        if( $data ){
            $item->fill($data);
            $item->data = $data;
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }else{
            $data = Request::get();
            if( !empty($data) ){
                $item->fill($data);
            }
        }

        echo Form::open('checklist_form')->method('POST');

        $form = Forms::make();
        $all = array_merge($item->toArray(),(array)$item->data);

        $form->fill($all);
        //$form->open('checklist_form')->method('POST');

        $opt2 = ['+'=>'Ima','-'=>'Nema'];
        $opt3 = [''=>'<span style="color:#444">Nije gledano</span>&nbsp;&nbsp;','+'=>'<span style="color:#090;font-weight:bold;">OK</span>&nbsp;&nbsp;','-'=>'<span style="color:#a00;font-weight:bold;">Nije OK</span>'];

        $table = Table::make();
        if( empty($item->check_date) ){
            $item->check_date = date('Y-m-d');
        }

        $table->addRow(['Datum provjere',               $form->datePicker('check_date')->addClass('form-control') ])->readonly();
        
        $table->addRow(['Vozilo',                       $form->select('vehicle_id')->options(Vehicle::options())->addClass('form-control') ]);
        
        $table->addRow(['Kilometraža vozila',           $form->number('kilometraza_vozila')->addClass('form-control') ]);
        
        $table->addRow(['Brisači',                      $form->radioGroup('brisaci')->options($opt3) ]);
        $table->addRow(['Kratka svjetla',               $form->radioGroup('kratka_svjetla')->options($opt3) ]);
        $table->addRow(['Duga svjetla',                 $form->radioGroup('duga_svjetla')->options($opt3) ]);
        $table->addRow(['Žmigavci',                     $form->radioGroup('zmigavci')->options($opt3) ]);

        $table->addRow(['Svjetlo registracije',         $form->radioGroup('svjetlo_registracije')->options($opt3) ]);
        $table->addRow(['Svjetlo kočnice',              $form->radioGroup('svjetlo_kocnice')->options($opt3) ]);
        $table->addRow(['Svjetlo rikverc',              $form->radioGroup('svjetlo_rikverc')->options($opt3) ]);
        $table->addRow(['Svj.instrument ploče',         $form->radioGroup('instrument_ploca')->options($opt3) ]);

        $table->addRow(['Tekućina za stakla',           $form->radioGroup('tekucina_za_stakla')->options($opt3) ]);
        $table->addRow(['Tekućina za hlađenje',         $form->radioGroup('tekucina_za_hladenje')->options($opt3) ]);
        $table->addRow(['Tekućina serva',               $form->radioGroup('tekucina_serva')->options($opt3) ]);
        $table->addRow(['Tekućina kočnica',             $form->radioGroup('tekucina_kocnica')->options($opt3) ]);

        $table->addRow(['Gume - tlak i stanje',         $form->radioGroup('gume_tlak_stanje')->options($opt3) ]);
        $table->addRow(['Rad ventilacije brzine',       $form->radioGroup('rad_ventilacije_brzine')->options($opt3) ]);
        $table->addRow(['Rad klima uređaja',            $form->radioGroup('rad_klima_uredaja')->options($opt3) ]);

        $table->addRow(['Stanje sjedala',               $form->radioGroup('stanje_sjedala')->options($opt3) ]);
        $table->addRow(['Stanje ut.prostora',           $form->radioGroup('stanje_ut_prostora')->options($opt3) ]);
        $table->addRow(['PP aparat ispravnost',         $form->radioGroup('pp_aparat_ispravnost')->options($opt3) ]);

        $table->addRow(['Rezervna guma',                $form->radioGroup('rezervna_guma')->options($opt3) ]);
        $table->addRow(['Dizalica',                     $form->radioGroup('dizalica')->options($opt3) ]);
        $table->addRow(['Prva pomoć',                   $form->radioGroup('prva_pomoc')->options($opt3) ]);
        $table->addRow(['Žarulje',                      $form->radioGroup('zarulje')->options($opt3) ]);
        $table->addRow(['Prsluk',                       $form->radioGroup('prsluk')->options($opt3) ]);

        $table->addRow(['Knjižica vozila',              $form->radioGroup('knjizica_vozila')->options($opt3) ]);
        $table->addRow(['Polica auto odgovornost',      $form->radioGroup('polica_auto_odgovornosti')->options($opt3) ]);
        $table->addRow(['Zeleni karton',                $form->radioGroup('zeleni_karton')->options($opt3) ]);
        $table->addRow(['EU izvještaj štete',           $form->radioGroup('eu_izvjestaj_stete')->options($opt3) ]);

        $table->addRow(['Napomena',                     $form->text('napomena')->autogrow()->addClass('form-control') ]);

        $table->addRow(['&nbsp;',                       $form->btnSave('Zapamti provjeru') ]);

        // trigger form fill with data
        $form->render();

        $table->out('admin/table/grid');

        echo Form::close();
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/checklist')->with('msg',"{$this->model} #{$item->id} deleted");
    }


}
