<?php
class AdminContractArchiveController extends AdminController
{
    public $model = 'ContractArchive';
    public $baseurl = '/admin/contractarchive';

    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Dodaj Ugovor',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('Search');
        $form->label('Per page');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');

        $table = Table::make()
                ->headings(['#','Created At','Updated At','Name','Status','Client','Data','Pdf','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = $item->created_at;
            $data[] = $item->updated_at;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->name.'</a>';
            $data[] = $item->status;
            $data[] = $item->client;
            $data[] = $item->data;
            $data[] = $item->pdf;
            $actions = UI::btnEdit($item->id,'contractarchive');
            $actions.= UI::btnDelete($item->id,'contractarchive');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('contractarchive');
        $form->label('Name');
        $form->input('name')->klass('form-control');

        $form->label('Status');
        $form->input('status')->klass('form-control');

        $form->label('Client');
        $form->input('client')->klass('form-control');

        $form->label('Data');
        $form->input('data')->klass('form-control');

        $form->label('Pdf');
        $form->input('pdf')->klass('form-control');


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/contractarchive')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
