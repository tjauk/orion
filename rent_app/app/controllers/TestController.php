<?php
use Symfony\Component\HttpFoundation\Response as Response;
use Illuminate\Database\Schema\Blueprint as Blueprint;

class TestController extends Controller
{
    function __construct(){
        Doc::none();
    }

    function index()
    {
        $ctrlname = str_replace('Controller','',__CLASS__);
        echo h3( $ctrlname );
        $actions = get_class_methods( __CLASS__ );
        sort( $actions );
        foreach( $actions as $action ) {
            if( substr( $action, 0, 1 ) !== '_' and substr( $action, 0, 4 ) !== 'post' and substr( $action, 0, 6 ) !== 'widget' ) {
                    $path = strtolower( $ctrlname ).'/'.$action;
                    echo  '<a href="/'.$path.'">'.$path.'</a><br>';
            }
        }
    }

    /**
     * ONE
     */
    function one(){
        return $_REQUEST;
    }

    function two(){
        return $_REQUEST;
    }

    function three(){
        return $_REQUEST;
    }

    function view($id=0){
        return $id;
    }

    function byTitle($id=0){
        return Article::findByTitle('test');
    }
    function izbroji_rijeci($tekst){
        $lista = explode(" ", $tekst);
        foreach ($lista as $index => $rijec) {
            if( $rijec=="" ){
                unset($lista[$index]);
            }
        }
        return count($lista);
    }
    function all(){
        return Article::all();
    }

    function getShow(){
        return __METHOD__;
    }

    function server(){
        return $_SERVER;
    }

    function env($name){
        return getenv($name);
    }

    function session(){
        return $_SESSION;
    }

    function json(){
    	return ['key'=>"val"];
    }

    function response(){
        $response = new Response();
        $response->setContent('My response');
        $response->setStatusCode(Response::HTTP_NOT_FOUND);
        return $response;
    }
    
    function schema(){
        if( !Schema::hasTable('users') ){
            Schema::create('users', function(Blueprint $table)
            {
                $table->increments('id');
                $table->timestamps();
                $table->string('username')->unique();
                $table->string('email')->unique();
                $table->string('password', 60);
                $table->string('first_name')->default('');
                $table->string('last_name')->default('');
                $table->rememberToken();
            });
        }
        if( !Schema::hasTable('password_resets') ){
            Schema::create('password_resets', function(Blueprint $table)
            {
                $table->timestamps();
                $table->string('email')->index();
                $table->string('token')->index();
            });
        }
        return true;
    }
    function drop(){
        if(Schema::hasTable('users') ){
            Schema::drop('users');
        }
        if(Schema::hasTable('password_resets') ){
            Schema::drop('password_resets');
        }
        return true;
    }

    function query()
    {
        return DB::table("articles")->get();
    }

    function columns()
    {
        return Schema::getColumnListing("articles");
    }

    function select()
    {
        //return get_class_methods(Capsule::$instance->connection());
        return DB::select("SELECT * FROM articles LIMIT 1",[]);
    }

    function fill(){
        $id = DB::table('articles')->insertGetId(['title'=>"Test"]);
        return $id;
    }

    function fluid(){
        // create this using fluid model concept as in previous tests
        $book = new Book();
        $book->title = 'My title';
        $book->author = 'B.H.';
        $book->save();

    }


    function helena()
    {
        $broj = 10;
        $drugi = 456;
        //
        $tekst = 'Neki tekst';
        $drugi = ' bla';
        $dugi = 'Temeljem strateškog sporazuma o suradnji Visokog učilišta Algebra i Griffith Collegea iz Dublina, sadašnjim i budućim studentima računarstva i multimedije omogućeno je korištenje studentske mobilnosti i studiranje na dvije lokacije, uz mogućnost stjecanja hrvatske i irske prvostupničke diplome u području računarstva. Riječ je o načinu studiranju koji kroz studentsku mobilnost implementira sve prednosti bolonjskog procesa te je u inozemstvu vrlo rasprostranjen.

Studenti koji se prilikom upisa odluče koristiti ovu mogućnost završavaju prve dvije godine studija na zagrebačkom kampusu Visokog učilišta Algebra, dok cijelu treću studijsku godinu kroz studentsku mobilnost pohađaju na Griffith College kampusu Dublin, gdje pripremaju i završni projekt uz potporu hrvatskih i irskih mentora. Ovisno o upisanom studijskom smjeru, studenti koji se odluče na stjecanje dvije diplome će za vrijeme studija u Zagrebu pohađati i polagati i dodatne kolegije, te će po povratku iz Irske polagati i završni ispit kako bi zadovoljili sve uvjete za dobivanje diplome Visokog učilišta Algebra.

Ovakvom prekograničnom suradnjom mladi će imati priliku steći vrijedno multikulturalno iskustvo u Dublinu, europskoj „Silicijskoj dolini“.';
        
        $ukupno = 0;
        $duzina = strlen($dugi);
        echo $duzina.'<br>';
        for ($i=0; $i < $duzina; $i=$i+1) { 
            $provjeravamo = substr($dugi, $i,1);
            if( strtolower($provjeravamo)=='a' ){
                $ukupno = $ukupno + 1;
            }
        }

        $output = "Koliko ima slova a u tekstu?<br>";
        $output.= "<h2>".$ukupno."</h2>";
        return  $output;
    }



        function rijeci()
    {

        $tekst = 'Temeljem        strateškog sporazuma o suradnji Visokog učilišta Algebra i Griffith Collegea iz Dublina, sadašnjim i budućim studentima računarstva i multimedije omogućeno je korištenje studentske mobilnosti i studiranje na dvije lokacije, uz mogućnost stjecanja hrvatske i irske prvostupničke diplome u području računarstva. Riječ je o načinu studiranju koji kroz studentsku mobilnost implementira sve prednosti bolonjskog procesa te je u inozemstvu vrlo rasprostranjen.

Studenti koji se prilikom upisa odluče koristiti ovu mogućnost završavaju prve dvije godine studija na zagrebačkom kampusu Visokog učilišta Algebra, dok cijelu treću studijsku godinu kroz studentsku mobilnost pohađaju na Griffith College kampusu Dublin, gdje pripremaju i završni projekt uz potporu hrvatskih i irskih mentora. Ovisno o upisanom studijskom smjeru, studenti koji se odluče na stjecanje dvije diplome će za vrijeme studija u Zagrebu pohađati i polagati i dodatne kolegije, te će po povratku iz Irske polagati i završni ispit kako bi zadovoljili sve uvjete za dobivanje diplome Visokog učilišta Algebra.

Ovakvom prekograničnom suradnjom mladi će imati priliku steći vrijedno multikulturalno iskustvo u Dublinu, europskoj „Silicijskoj dolini“.';
        
        return $this->izbroji_rijeci($tekst);
    }



}
