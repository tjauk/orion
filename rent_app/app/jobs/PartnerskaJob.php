<?php

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

// Example: orion partnerska

class PartnerskaJob extends Command
{
    protected function configure()
    {
        $this
            ->setName('partnerska')
            ->setDescription('Partner vehicles booked per day')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // *********************************************************************
        echo "Od 01.01.2019. do 15.12.2019.\n";
        $vehicles = Vehicle::where('name', 'LIKE', 'Partner%')->get();
        foreach ($vehicles as $vehicle) {
            echo $vehicle->name."\n";
            $this->getStatsForVehicle($vehicle->id);
        }
        // *********************************************************************
        $output->writeln('----------------------------------');
        $output->writeln('Peak memory: '.(memory_get_peak_usage() / 1024 / 1024));
        $output->writeln('DONE IN '.round(microtime(true) - ORION_STARTED, 2).' seconds');
    }

    public function getStatsForVehicle($vehicle_id)
    {
        $dates = [];
        $max = 0;
        for ($i = 0; $i < 10000; ++$i) {
            $ts = strtotime("now -$i days");
            $date = date('Y-m-d', $ts);

            if (strtotime('2019-01-01') <= $ts and $ts <= strtotime('2019-12-15')) {
                // echo $date."\n";
                $res = DB::select(DB::raw("SELECT COUNT(*) AS c FROM bookings WHERE vehicle_id=$vehicle_id AND DATE(`from`)<=DATE('$date') AND DATE(`to`)>DATE('$date')"));
                $c = $res[0]->c;
                if ($c > $max) {
                    $max = $c;
                }
                if ($c > 0) {
                    $dates[$date] = $c;
                }
            }
        }

        // print_r($dates);

        echo 'Max = '.$max."\n";

        if (count($dates) > 0) {
            $average = array_sum($dates) / count($dates);
            echo 'Avg = '.$average."\n";
        }
        echo "\n";
    }
}
