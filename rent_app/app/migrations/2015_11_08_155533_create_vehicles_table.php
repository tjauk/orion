<?php

class CreateVehiclesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->default('');
            $table->string('brand')->default('');
            $table->string('model')->default('');
            $table->string('type')->default('');
            $table->tinyInteger('partner')->default(0);
            $table->string('year')->default('');
            $table->string('color')->default('White');
            $table->string('color_hex')->default('#FFFFFF');
            $table->string('chassis_number')->default('');
            $table->string('leasing')->default('');
            $table->string('leasing_amount')->default('');
            $table->string('tire_size')->default('');
            $table->date('pp_expires')->nullable();
            $table->date('casco_expires')->nullable();
            $table->text('docs')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicles');
    }

}
