<?php

class CreateVehicleServicesTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_services', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->integer('vehicle_id');
            $table->string('type')->default('Redovan');
            $table->date('from');
            $table->date('to')->nullable();
            $table->string('service_price')->default('');
            $table->string('parts_price')->default('');
            $table->string('status')->default('');
            $table->text('docs')->default('');
            $table->text('note')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicle_services');
    }

}
