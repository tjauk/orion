<?php

class CreateTasksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->integer('created_by');
            $table->integer('assigned_to')->nullable();
            $table->string('title')->default('');
            $table->text('description')->default('');
            $table->string('status');
            $table->dateTime('start');
            $table->dateTime('due');
            $table->dateTime('finish');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasks');
    }

}
