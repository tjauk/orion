<?php

class CreateBookingsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->integer('booked_by')->unsigned();
            $table->dateTime('from')->nullable();
            $table->dateTime('to')->nullable();
            $table->integer('vehicle_id')->unsigned();
            $table->string('km_start')->default('');
            $table->string('km_end')->default('');
            $table->float('price');
            $table->integer('client_id')->unsigned()->nullable();
            $table->string('partner')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookings');
    }

}
