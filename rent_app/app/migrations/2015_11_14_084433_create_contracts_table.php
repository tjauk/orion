<?php

class CreateContractsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->default('');
            $table->string('status')->default('');
            $table->string('client')->default('');
            $table->text('data')->default('');
            $table->text('pdf')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contracts');
    }

}
