<?php

class AddRegistrationColumnsToVehicle extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicles', function($table)
        {
            $table->string('licence_plate')->default('');
            $table->date('licence_expires')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function($table)
        {
            $table->dropColumn('licence_plate');
            $table->dropColumn('licence_expires');
        });
    }

}
