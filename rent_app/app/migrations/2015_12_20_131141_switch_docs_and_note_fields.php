<?php

class SwitchDocsAndNoteFields extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicles', function($table)
        {
            $table->renameColumn('docs', 'note');
            //$table->text('docs')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function($table)
        {
            $table->renameColumn('note', 'docs');
            //$table->dropColumn('docs');
        });
    }

}
