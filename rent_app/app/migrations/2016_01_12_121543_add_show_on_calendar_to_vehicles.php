<?php

class AddShowOnCalendarToVehicles extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicles', function($table)
        {
            $table->integer('show_on_calendar')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function($table)
        {
            $table->dropColumn('show_on_calendar');
        });    }

}
