<?php

class CreateVehicleHistoryKmsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_kms', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->integer('vehicle_id')->unsigned();
            $table->integer('km')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicle_kms');
    }

}
