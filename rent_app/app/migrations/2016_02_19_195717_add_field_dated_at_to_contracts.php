<?php

class AddFieldDatedAtToContracts extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts', function($table)
        {
            $table->date('dated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts', function($table)
        {
            $table->dropColumn('dated_at');
        });
    }

}
