<?php

class AddContractedKmToBookings extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function($table)
        {
            $table->string('contracted_km')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function($table)
        {
            $table->dropColumn('contracted_km');
        });
    }

}
