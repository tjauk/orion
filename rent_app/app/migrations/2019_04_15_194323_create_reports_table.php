<?php

class CreateReportsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->integer('created_by')->unsigned()->nullable();
            $table->date('dated_at')->nullable();
            $table->date('from')->nullable();
            $table->date('to')->nullable();
            $table->string('period')->default('');

            $table->string('name')->default('');
            $table->string('status')->default('');
            $table->text('note')->nullable();

            $table->longText('data')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports');
    }

}
