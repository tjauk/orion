<?php

class ConvertHrkToEurValues extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        bookings.price
        bookings.partial_amount
        bookings.partner_price
        bookings.our_provision

        vehicles.leasing_amount
        vehicles.leasing_stake
        vehicles.ao_amount
        vehicles.kasko_amount

        vehicle_services.service_price
        vehicle_services.parts_price
        */

        DB::statement('
            UPDATE `bookings`
            SET
                price=round(price/7.5345,2),
                partial_amount=round(partial_amount/7.5345,2),
                partner_price=round(partner_price/7.5345,2),
                our_provision=round(our_provision/7.5345,2)                    
            ;
        ');

        DB::statement('
            UPDATE `vehicles`
            SET
                leasing_amount=round(leasing_amount/7.5345),
                leasing_stake=round(leasing_stake/7.5345),
                ao_amount=round(ao_amount/7.5345),
                kasko_amount=round(kasko_amount/7.5345)                    
            ;
        ');

        DB::statement('
            UPDATE `vehicle_services`
            SET
                service_price=round(service_price/7.5345),
                parts_price=round(parts_price/7.5345)                 
            ;
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::drop('convert_hrk_to_eur_values');
    }
}
