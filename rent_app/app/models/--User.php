<?php
class User extends Model
{
    protected $table = 'users';
    protected $fillable = ['username','email','password','first_name','last_name','remember_token'];
}
