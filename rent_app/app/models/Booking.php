<?php

class Booking extends Model
{
    protected $table = 'bookings';

    protected $fillable = [
        'booked_by',
        'from',
        'to',
        'vehicle_id',
        'km_start',
        'km_end',
        'contracted_km',
        'price',
        'partner_price',
        'our_provision',
        'paid',
        'partial_amount',
        'payable_until',
        'related_doc',
        'client_id',
        'partner',
        'service_type',
        'docs',
        'note',
        'status',
    ];

    protected $appends = [
        'from_date',
        'from_time',
        'to_date',
        'to_time',
    ];

    public static function paid_options()
    {
        return [
            '1' => 'Nije plaćeno',
            '10' => 'Djelomično plaćeno',
            '20' => 'Plaćeno u cijelosti',
            '99' => 'Bez naplate',
        ];
    }

    public static function status_options()
    {
        return [
            0 => 'Nije počeo',
            1 => 'Traje',
            2 => 'Završio',
        ];
    }

    public static function service_type_options()
    {
        return [
            10 => 'Najam',
            20 => 'Prijevoz',
            30 => 'Ostalo',
        ];
    }

    public static function company_options($insert_empty = false)
    {
        $options = [];
        if ($insert_empty) {
            $options[''] = '---';
        }
        $options['1'] = 'Komet';
        $options['2'] = 'Flexirent';
        $options['2318'] = 'Sistem';

        return $options;
    }

    /*
     * Relations
     */
    public function vehicle()
    {
        return $this->belongsTo('Vehicle');
    }

    public function client()
    {
        return $this->belongsTo('Contact');
    }

    /*
        public function partner()
        {
            return $this->belongsTo('Contact','id','partner');
        }
    */

    /*
     * Boot, register events
     */
    public static function boot()
    {
        static::saving(function ($object) {
            Vehicle::updateKm($object->vehicle_id, $object->km_start);
            Vehicle::updateKm($object->vehicle_id, $object->km_end);
        });
    }

    /*
     * Options
     */
    public static function options()
    {
        $options = ['' => '---'];
        $items = static::all();

        foreach ($items as $item) {
            $options[$item->id] = UI::dateTime($item->from).' - '.UI::dateTime($item->to).' / '.$item->vehicle->title;
        }

        return $options;
    }

    /*
     * Scopes
     */
    public function scopeActive($query)
    {
        $query->where('vehicle_id', '>', 0);
    }

    public function scopeStartsToday($query)
    {
        $query->where(\DB::raw('DATE(`from`)'), '=', \DB::raw('DATE(NOW())'));
    }

    public function scopeStartsThisWeek($query)
    {
        $start_date = date('Y-m-d');
        $week_later = date('Y-m-d', strtotime('now +7days'));
        $query->where(DB::raw('DATE(`from`)'), '>=', $start_date);
        $query->where(DB::raw('DATE(`from`)'), '<=', $week_later);
    }

    public function scopeEndsToday($query)
    {
        $query->where(\DB::raw('DATE(`to`)'), '=', \DB::raw('DATE(NOW())'));
    }

    public function scopeEndsThisWeek($query)
    {
        $start_date = date('Y-m-d');
        $week_later = date('Y-m-d', strtotime('now +7days'));
        $query->where(DB::raw('DATE(`to`)'), '>=', $start_date);
        $query->where(DB::raw('DATE(`to`)'), '<=', $week_later);
    }

    public function scopeInPeriod($query, $from = null, $to = null)
    {
        if (is_null($from)) {
            $from = date('Y-m-d');
        }
        if (is_null($to)) {
            $to = date('Y-m-d');
        }
        $query->whereRaw("(DATE(`from`)<=DATE('{$to}') OR DATE(`to`)>=DATE('{$from}'))");
    }

    public function scopeInMonth($query, $month, $year = null)
    {
        if (is_null($year)) {
            $year = date('Y');
        }
        $month_start = $year.'-'.$month.'-01';
        $month_start = date('Y-m-d', strtotime($year.'-01-01 +'.$month.'month'));
        $month_end = date('Y-m-t', strtotime($month_start));

        $query->where(DB::raw('DATE(`from`)'), '>=', $month_start);
        $query->where(DB::raw('DATE(`from`)'), '<=', $month_end);
    }

    public function scopeWithoutOtherServices($query)
    {
        $query->where('service_type', '<>', 30);
    }

    /*
     * Getters
     */
    public function getTitleAttribute()
    {
        return $this->vehicle->title.'<br>( '.\UI::dateTime($this->from).' - '.\UI::dateTime($this->to).' )';
    }

    public function getFromDateAttribute()
    {
        if (empty($this->attributes['from'])) {
            return '';
        }

        return date('Y-m-d', strtotime($this->attributes['from']));
    }

    public function getFromTimeAttribute()
    {
        if (empty($this->attributes['from'])) {
            return '';
        }

        return date('H:i', strtotime($this->attributes['from']));
    }

    public function getToDateAttribute()
    {
        if (empty($this->attributes['to'])) {
            return '';
        }

        return date('Y-m-d', strtotime($this->attributes['to']));
    }

    public function getToTimeAttribute()
    {
        if (empty($this->attributes['to'])) {
            return '';
        }

        return date('H:i', strtotime($this->attributes['to']));
    }

    /*
     * Setters
     */
    public function setBookedByAttribute($date)
    {
        $this->attribute['booked_by'] = Auth::user()->id;
    }

    public function setFromAttribute($datetime)
    {
        $this->attributes['from'] = date('Y-m-d H:i:s', strtotime($datetime));
    }

    public function setToAttribute($datetime)
    {
        $this->attributes['to'] = date('Y-m-d H:i:s', strtotime($datetime));
    }

    public function setFromDateAttribute($date)
    {
        $date = date('Y-m-d', strtotime($date));
        $time = date('H:i:00', strtotime($this->attributes['from']));
        $this->attributes['from'] = $date.' '.$time;
    }

    public function setFromTimeAttribute($time)
    {
        $date = date('Y-m-d', strtotime($this->attributes['from']));
        $time = date('H:i:00', strtotime($time));
        //dd($time);
        $this->attributes['from'] = $date.' '.$time;
    }

    public function setToDateAttribute($date)
    {
        $date = date('Y-m-d', strtotime($date));
        $time = date('H:i:00', strtotime($this->attributes['to']));
        $this->attributes['to'] = $date.' '.$time;
    }

    public function setToTimeAttribute($time)
    {
        $date = date('Y-m-d', strtotime($this->attributes['to']));
        $time = date('H:i:00', strtotime($time));
        $this->attributes['to'] = $date.' '.$time;
    }

    public function setPriceAttribute($price)
    {
        $price = str_replace(',', '.', $price);
        $this->attributes['price'] = $price;
    }
}
