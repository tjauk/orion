<?php
class Contact extends Model
{
    protected $table = 'contacts';

    protected $fillable = [
        'groups',
        'first_name',
        'last_name',
        'company',
        'company_long',
        'oib',
        'mb',
        'address',
        'address_2',
        'zip',
        'city',
        'country_id',
        'email',
        'mobile',
        'phone',
        'fax',
        'web',
        'note',
        'tags',
        'contact_person',
        'contact_phone'
    ];

    protected $appends = ['title'];

    /*
     * Relations
     */
    public function country()
    {
        return $this->belongsTo('Country','country_id');
    }


    /*
     * Options
     */
    static function options($group=null)
    {
        $options = [''=>'---'];
        if( is_null($group) ){
            $items = static::all();
        }else{
            $items = static::whereRaw("FIND_IN_SET('$group',groups)")->get();
        }

        foreach($items as $item){
            $options[$item->id] = $item->title;
        };

        return $options;
    }

    static function groups()
    {
        return [
            'Fizička osoba'     => "Fizička osoba",
            'Pravna osoba'      => "Pravna osoba",
            'Dobavljač'           => "Dobavljač",
            //'Partner'           => "Partner",
            'Servisni centar'   => "Servisni centar",
            //'Najmodavac'        => "Najmodavac"
        ];
    }


    /*
     * Scopes
     */


    /*
     * Getters
     */
    public function getTitleAttribute()
    {
        if( empty($this->company) ){
            return $this->last_name.' '.$this->first_name;
        }
        if( empty($this->first_name) and empty($this->last_name) ){
            return $this->company;
        }
        return $this->last_name.' '.$this->first_name.' ('.$this->company.')';
    }

    public function getGroupsAttribute()
    {
        // add function to clean up empty elements ?
        return explode(',',$this->attributes['groups']);
    }

    public function getTagsAttribute()
    {
        return array_filter(explode(',',$this->attributes['tags']));
    }

    /*
     * Setters
     */
    public function setGroupsAttribute($groups)
    {
        $this->attributes['groups'] = implode(',',$groups);
    }
    public function setTagsAttribute($tags)
    {
        if( is_array($tags) ){
            $tags = implode(',',$tags);
        }
        if( is_null($tags) ){
            $tags = '';
        }
        $this->attributes['tags'] = $tags;
    }

    // trim fields
    public function setCompanyAttribute($str)
    {
        $this->attributes['company'] = trim($str);        
    }
    public function setAddressAttribute($str)
    {
        $this->attributes['address'] = trim($str);        
    }
    public function setZipAttribute($str)
    {
        $this->attributes['zip'] = trim($str);        
    }
    public function setCityAttribute($str)
    {
        $this->attributes['city'] = trim($str);        
    }

    public function setWebAttribute($str)
    {
        $this->attributes['web'] = trim($str);        
    }

}
