<?php
class Damage extends Model
{
    protected $table = 'damages';

    protected $fillable = [
        'name',
        'vehicle_id',
        'vehicle_km',
        'date',
        'where',
        'status',
        'description',
        'note',
        'docs'
    ];

    static $statuses = ['0'=>"Nije popravljen",'1'=>"U procesu",'2'=>"Popravljen"];

    /*
     * Relations
     */
    public function vehicle()
    {
        return $this->belongsTo('Vehicle');
    }

   /*
     * Boot, register events
     */
    static function boot(){

        Damage::saving(function($object){
            Vehicle::updateKm($object->vehicle_id,$object->vehicle_km);
        });

    }

    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters / Setters
     */
    function setDateAttribute($str)
    {
        $val = date('Y-m-d',strtotime($str));
        if( empty($str) or $str=='0000-00-00' ){
            $val = null;
        }
        $this->attributes['date'] = $val;
    }

}
