<?php
class Docs extends Model
{
    protected $table = 'documents';

    protected $fillable = [];

    /*
     * Relations
     */


    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */


    /*
     * Getters
     */


}
