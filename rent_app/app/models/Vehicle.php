<?php

class Vehicle extends Model
{
    protected $table = 'vehicles';

    protected $fillable = [
        'name',
        'brand',
        'model',
        'type',
        'partner',
        'year',
        'km',
        'service_km',
        'service_amount',
        'color',
        'color_hex',
        'chassis_number',
        'leasing',
        'leasing_stake',
        'leasing_amount',
        'tire_size',
        'tire_type',
        'pp_expires',
        'ppa_expires',
        'ao_expires',
        'ao_amount',
        'ao_house',
        'kasko_expires',
        'kasko_amount',
        'kasko_house',
        'licence_plate',
        'licence_expires',
        'show_on_calendar',
        'docs',
        'note',
        'active',
    ];

    public static function types()
    {
        return [
            'Putnički kombi',
            'Teretni kombi',
            'Osobni automobil',
            'Osobni 5 sjedala N1',
            'Dostavni automobil',
            'Partnerski putnički kombi',
            'Partnerski teretni kombi',
            'Partnerski automobil',
            'Partnerski osobni 5 sjedala N1',
            'Partnerski dostavni automobil',
        ];
        //return ['Teretni','Putnički','Partnerski teretni','Partnerski putnički'];
    }

    public static function ordered_by_types()
    {
        return [
            "'Putnički kombi'",
            "'Teretni kombi'",
            "'Osobni automobil'",
            "'Osobni 5 sjedala N1'",
            "'Dostavni automobil'",
            "'Partnerski putnički kombi'",
            "'Partnerski teretni kombi'",
            "'Partnerski automobil'",
            "'Partnerski dostavni automobil'",
            "'Partnerski osobni 5 sjedala N1'",
        ];
    }

    public static function tire_types()
    {
        return [
            'summer' => 'Ljetne',
            'winter' => 'Zimske',
        ];
    }

    /*
     * Relations
     */
    public function partnervozila()
    {
        return $this->belongsTo('Contact', 'partner');
    }

    public function next_service_temp()
    {
        // prvo pronalazi servis sa upisanim pocetnim datumom i statusom treba ići,
        // nakon toga nađi servise s treba ici i kilometražom većom od trenutne na vozilu
        // return VehicelService models (this should be scope)
        return null;
    }

    /*
     * Boot, register events
     */
    public static function boot()
    {
        static::saving(function ($next) {
            if ($next->id > 0) {
                $prev = Vehicle::find($next->id);
                if (json_encode($prev->getAOFields()) !== json_encode($next->getAOFields())) {
                    $next->updateAOHistory();
                }
                if (json_encode($prev->getKaskoFields()) !== json_encode($next->getKaskoFields())) {
                    $next->updateKaskoHistory();
                }
            }
        });
    }

    /*
     * Options
     */
    public static function options()
    {
        $options = ['' => '---'];
        $items = static::active()->get();

        foreach ($items as $item) {
            $options[$item->id] = $item->title;
        }

        return $options;
    }

    public static function options_with_km()
    {
        $options = ['' => '---'];
        $items = static::active()->get();

        foreach ($items as $item) {
            $options[$item->id] = $item->title . ' ( ' . $item->km . ' km )';
        }

        return $options;
    }

    /*
     * Scopes
     */
    public function scopeActive($query)
    {
        $query->where('active', '=', 1);
    }

    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }

    /*
     * Getters
     */
    public function getTitleAttribute()
    {
        return $this->name; //.' ('.$this->brand.' '.$this->model.', '.$this->color.')'
    }

    /*
     * Setters
     */
    public function setPpExpiresAttribute($str)
    {
        $val = date('Y-m-d', strtotime($str));
        if (empty($str) or $str == '0000-00-00') {
            $val = null;
        }
        $this->attributes['pp_expires'] = $val;
    }

    public function setPpaExpiresAttribute($str)
    {
        $val = date('Y-m-d', strtotime($str));
        if (empty($str) or $str == '0000-00-00') {
            $val = null;
        }
        $this->attributes['ppa_expires'] = $val;
    }

    public function setAoExpiresAttribute($str)
    {
        $val = date('Y-m-d', strtotime($str));
        if (empty($str) or $str == '0000-00-00') {
            $val = null;
        }
        $this->attributes['ao_expires'] = $val;
    }

    public function setKaskoExpiresAttribute($str)
    {
        $val = date('Y-m-d', strtotime($str));
        if (empty($str) or $str == '0000-00-00') {
            $val = null;
        }
        $this->attributes['kasko_expires'] = $val;
    }

    public function setLicenceExpiresAttribute($str)
    {
        $val = date('Y-m-d', strtotime($str));
        if (empty($str) or $str == '0000-00-00') {
            $val = null;
        }
        $this->attributes['licence_expires'] = $val;
    }

    /*
     * Update Km if greater then prev value
     */
    public static function updateKm($vehicle_id, $km)
    {
        if ($vehicle_id > 0) {
            $vehicle = Vehicle::find($vehicle_id);
            if ($vehicle) {
                if ($km > $vehicle->km) {
                    $vehicle->km = $km;
                    $vehicle->save();
                }
            }
            VehicleKm::register($vehicle->id, $km);
        }
    }

    /*
     * Reporting
     */
    public function getStats()
    {
    }

    /*
     * Expirations between dates
     */
    public function scopeExpiresBetween($query, $from = null, $to = null, $field = 'licence_expires')
    {
        if (is_null($from)) {
            $from = date('Y-m-d');
        }
        if (is_null($to)) {
            $to = $from;
        }
        $query->whereRaw("( DATE(`$field`)>=DATE('{$from}') AND DATE(`$field`)<=DATE('{$to}') )");
    }

    public function scopePpExpires($query, $from = null, $to = null)
    {
        return $this->scopeExpiresBetween($query, $from, $to, 'pp_expires');
    }

    public function scopePpaExpires($query, $from = null, $to = null)
    {
        return $this->scopeExpiresBetween($query, $from, $to, 'ppa_expires');
    }

    public function scopeAoExpires($query, $from = null, $to = null)
    {
        return $this->scopeExpiresBetween($query, $from, $to, 'ao_expires');
    }

    public function scopeKaskoExpires($query, $from = null, $to = null)
    {
        return $this->scopeExpiresBetween($query, $from, $to, 'kasko_expires');
    }

    public function scopeLicenceExpires($query, $from = null, $to = null)
    {
        return $this->scopeExpiresBetween($query, $from, $to, 'licence_expires');
    }

    /*
     * Future expirations of licence, kasko, pp, ppa
     */
    public static function itemsExpiringSoon($days = 15)
    {
        $items = DB::select(
            "
            SELECT * FROM
            (
            (
            SELECT id,name,pp_expires as datum,'Periodički pregled' as item
            FROM vehicles
            WHERE pp_expires BETWEEN CURDATE() AND DATE_ADD(CURDATE(),INTERVAL $days DAY)
            )
            UNION
            (
            SELECT id,name,ppa_expires as datum,'Protupožarni aparat' as item
            FROM vehicles
            WHERE ppa_expires BETWEEN CURDATE() AND DATE_ADD(CURDATE(),INTERVAL $days DAY)
            )
            UNION
            (
            SELECT id,name,licence_expires as datum,'Registracija' as item
            FROM vehicles
            WHERE licence_expires BETWEEN CURDATE() AND DATE_ADD(CURDATE(),INTERVAL $days DAY)
            )
            UNION
            (
            SELECT id,name,kasko_expires as datum,'Kasko' as item
            FROM vehicles
            WHERE kasko_expires BETWEEN CURDATE() AND DATE_ADD(CURDATE(),INTERVAL $days DAY)
            )
            ) as t
            ORDER BY datum ASC"
        );

        return $items;
    }

    public static function countActiveOwn()
    {
        return static::where('active', '=', 1)->where('type', 'NOT LIKE', '%Partner%')->count();
    }

    public function getAOFields()
    {
        return [
            'ao_expires' => $this->ao_expires,
            'ao_amount' => $this->ao_amount,
            'ao_house' => $this->ao_house,
        ];
    }

    public function updateAOHistory()
    {
        $arr = json_decode($this->ao_history);
        if (!is_array($arr)) {
            $arr = [];
        }
        $arr[] = $this->getAOFields();
        $this->ao_history = json_encode($arr);
    }

    public function getKaskoFields()
    {
        return [
            'kasko_expires' => $this->kasko_expires,
            'kasko_amount' => $this->kasko_amount,
            'kasko_house' => $this->kasko_house,
        ];
    }

    public function updateKaskoHistory()
    {
        $arr = json_decode($this->kasko_history);
        if (!is_array($arr)) {
            $arr = [];
        }
        $arr[] = $this->getKaskoFields();
        $this->kasko_history = json_encode($arr);
    }
}
