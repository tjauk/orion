<?php
class VehicleKm extends Model
{
    protected $table = 'vehicle_kms';

    protected $fillable = [
		'vehicle_id',
		'km'
    ];

    /*
     * Relations
     */
    public function vehicle()
    {
        return $this->belongsTo('Vehicle');
    }


    static function register($vehicle_id,$km)
    {
        if( !empty($vehicle_id) ){
            $last_km = static::whereVehicleId($vehicle_id)->orderBy('km','DESC')->first()->km;
            if( $km > $last_km ){
                static::create(['vehicle_id'=>$vehicle_id,'km'=>$km]);
            }
        }
    }

}
