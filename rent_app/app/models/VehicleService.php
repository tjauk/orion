<?php
class VehicleService extends Model
{
    protected $table = 'vehicle_services';

    protected $fillable = [
        'vehicle_id',
        'type',
        'vehicle_km',
        'from',
        'to',
        'service_id',
        'service_price',
        'parts_price',
        'status',
        'service_status',
        'docs',
        'note'
    ];

    static $statuses = ['0' => "Nije plaćen", '1' => "Plaćen"];

    static function service_status_options()
    {
        return [
            '1'     => "Treba odvesti",
            '2'     => "Traje",
            '3'     => "Završen"
        ];
    }

    static function service_type_options()
    {
        return [
            'Redovan' => 'Redovan',
            'Izvanredni' => 'Izvanredni',
            // 'Pregled'=>'Pregled'
        ];
    }

    /*
     * Relations
     */
    public function vehicle()
    {
        return $this->belongsTo('Vehicle');
    }

    public function service()
    {
        return $this->belongsTo('Contact', 'service_id');
    }

    /*
     * Boot, register events
     */
    static function boot()
    {

        VehicleService::saving(function ($object) {
            //Vehicle::updateKm($object->vehicle_id,$object->vehicle_km);
        });
    }

    /*
     * Options
     */
    static function options()
    {
        $options = ['' => '---'];
        $items = static::all();

        foreach ($items as $item) {
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopeStartsToday($query)
    {
        $query->where(\DB::raw("DATE('from')"), '=', \DB::raw("DATE(NOW())"));
    }

    public function scopeEndsToday($query)
    {
        $query->where(\DB::raw("DATE('to')"), '=', \DB::raw("DATE(NOW())"));
    }

    public function scopeInPeriod($query, $from = null, $to = null)
    {
        if (is_null($from) and is_null($to)) {
            $now = date('Y-m-d');
            $query->whereRaw("DATE('{$now}')>=DATE(`from`) AND ( DATE('{$now}')<=DATE(`to`) OR ISNULL(DATE(`to`)) )");
        } else {

            if (is_null($from)) {
                $from = date('Y-m-d');
            }
            if (is_null($to)) {
                $to = date('Y-m-d');
            }
            $query->whereRaw("(DATE(`from`)<=DATE('{$to}') OR DATE(`to`)>=DATE('{$from}'))");
        }
        //dd($query->toSQL());
    }

    public function scopeNextTime($query, $vehicle)
    {
        if (!is_object($vehicle)) {
            $vehicle = Vehicle::find($vehicle);
        }
        // treba odvesti + na km vece od trenutne km na vozilu
        // treba odvesti + na km vece od trenutne km na vozilu
        $query->whereVehicleId($vehicle->id)
            ->whereServiceStatus(1)
            ->whereRaw("(DATE(`from`)>=CURDATE() or `vehicle_km`>1)");
    }



    /*
     * Getters
     */
    public function getTitleAttribute()
    {
        return $this->vehicle->title . ' (' . \UI::date($this->from) . ' - ' . \UI::date($this->to) . ')';
    }


    /*
     * Setters
     */
    function setFromAttribute($str)
    {
        $val = date('Y-m-d', strtotime($str));
        if (empty($str) or $str == '0000-00-00') {
            $val = null;
        }
        $this->attributes['from'] = $val;
    }
    function setToAttribute($str)
    {
        $val = date('Y-m-d', strtotime($str));
        if (empty($str) or $str == '0000-00-00') {
            $val = null;
        }
        $this->attributes['to'] = $val;
    }
}
