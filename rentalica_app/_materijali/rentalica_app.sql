﻿/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;

DROP TABLE IF EXISTS `audits`;
CREATE TABLE `audits` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` text COLLATE utf8_unicode_ci,
  `message` text COLLATE utf8_unicode_ci,
  `data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `audits` VALUES (1,'2016-03-05 07:51:26','2016-03-05 07:51:26','user.login','User igor.cukac@gmail.com logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (2,'2016-03-05 08:30:47','2016-03-05 08:30:47','user.login','User igor.cukac@gmail.com logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (3,'2016-03-07 06:42:58','2016-03-07 06:42:58','user.login','User igor.cukac@gmail.com logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (4,'2016-03-07 10:59:05','2016-03-07 10:59:05','user.login','User igor.cukac@gmail.com logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (5,'2016-03-07 14:49:48','2016-03-07 14:49:48','user.login','User igor.cukac@gmail.com logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (6,'2016-03-09 15:33:37','2016-03-09 15:33:37','user.login','User igor.cukac@gmail.com logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (7,'2016-03-09 16:46:36','2016-03-09 16:46:36','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (8,'2016-03-10 08:58:03','2016-03-10 08:58:03','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (9,'2016-03-10 08:58:34','2016-03-10 08:58:34','user.logout','User admin logged out','{\"user_id\":1}');
INSERT INTO `audits` VALUES (10,'2016-03-10 08:58:41','2016-03-10 08:58:41','user.login','User igor.cukac@gmail.com logged in','{\"user_id\":2}');
INSERT INTO `audits` VALUES (11,'2016-03-10 09:00:18','2016-03-10 09:00:18','user.logout','User  logged out','{\"user_id\":null}');
INSERT INTO `audits` VALUES (12,'2016-03-10 09:01:45','2016-03-10 09:01:45','user.login','User demo logged in','{\"user_id\":3}');
INSERT INTO `audits` VALUES (13,'2016-03-10 10:40:14','2016-03-10 10:40:14','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (14,'2016-03-10 19:46:16','2016-03-10 19:46:16','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (15,'2016-03-14 13:13:43','2016-03-14 13:13:43','user.login','User demo logged in','{\"user_id\":3}');
INSERT INTO `audits` VALUES (16,'2016-03-14 18:08:59','2016-03-14 18:08:59','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (17,'2016-03-14 18:09:42','2016-03-14 18:09:42','user.logout','User admin logged out','{\"user_id\":1}');
INSERT INTO `audits` VALUES (18,'2016-03-14 18:09:51','2016-03-14 18:09:51','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (19,'2016-03-14 18:13:00','2016-03-14 18:13:00','user.logout','User admin logged out','{\"user_id\":1}');
INSERT INTO `audits` VALUES (20,'2016-03-14 18:13:07','2016-03-14 18:13:07','user.login','User demo logged in','{\"user_id\":3}');
INSERT INTO `audits` VALUES (21,'2016-03-14 19:49:31','2016-03-14 19:49:31','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (22,'2016-03-16 09:18:07','2016-03-16 09:18:07','user.login','User demo logged in','{\"user_id\":3}');
INSERT INTO `audits` VALUES (23,'2016-03-16 09:36:53','2016-03-16 09:36:53','user.login','User demo logged in','{\"user_id\":3}');
INSERT INTO `audits` VALUES (24,'2016-03-16 10:26:18','2016-03-16 10:26:18','user.logout','User demo logged out','{\"user_id\":3}');
INSERT INTO `audits` VALUES (25,'2016-03-16 10:26:20','2016-03-16 10:26:20','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (26,'2016-03-16 10:27:44','2016-03-16 10:27:44','user.logout','User admin logged out','{\"user_id\":1}');
INSERT INTO `audits` VALUES (27,'2016-03-16 10:27:49','2016-03-16 10:27:49','user.login','User demo logged in','{\"user_id\":3}');
INSERT INTO `audits` VALUES (28,'2016-03-16 10:51:20','2016-03-16 10:51:20','user.logout','User demo logged out','{\"user_id\":3}');
INSERT INTO `audits` VALUES (29,'2016-03-16 10:51:21','2016-03-16 10:51:21','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (30,'2016-03-16 11:01:02','2016-03-16 11:01:02','user.logout','User admin logged out','{\"user_id\":1}');
INSERT INTO `audits` VALUES (31,'2016-03-16 11:01:07','2016-03-16 11:01:07','user.login','User demo logged in','{\"user_id\":3}');
INSERT INTO `audits` VALUES (32,'2016-03-16 11:10:53','2016-03-16 11:10:53','user.logout','User demo logged out','{\"user_id\":3}');
INSERT INTO `audits` VALUES (33,'2016-03-16 11:10:55','2016-03-16 11:10:55','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (34,'2016-03-16 11:11:28','2016-03-16 11:11:28','user.logout','User admin logged out','{\"user_id\":1}');
INSERT INTO `audits` VALUES (35,'2016-03-16 11:11:32','2016-03-16 11:11:32','user.login','User demo logged in','{\"user_id\":3}');
INSERT INTO `audits` VALUES (36,'2016-03-16 11:13:08','2016-03-16 11:13:08','user.logout','User demo logged out','{\"user_id\":3}');
INSERT INTO `audits` VALUES (37,'2016-03-16 11:13:12','2016-03-16 11:13:12','user.login','User admin logged in','{\"user_id\":1}');
INSERT INTO `audits` VALUES (38,'2016-03-16 11:23:09','2016-03-16 11:23:09','user.login','User demo logged in','{\"user_id\":3}');
INSERT INTO `audits` VALUES (39,'2016-03-16 11:29:52','2016-03-16 11:29:52','user.logout','User admin logged out','{\"user_id\":1}');
INSERT INTO `audits` VALUES (40,'2016-03-16 11:29:59','2016-03-16 11:29:59','user.login','User demo logged in','{\"user_id\":3}');
DROP TABLE IF EXISTS `bookings`;
CREATE TABLE `bookings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `booked_by` int(10) unsigned NOT NULL,
  `vehicle_id` int(10) unsigned NOT NULL,
  `service_type` tinyint(4) NOT NULL DEFAULT '10',
  `from` datetime DEFAULT NULL,
  `to` datetime DEFAULT NULL,
  `km_start` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `km_end` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `client_id` int(10) unsigned DEFAULT NULL,
  `partner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT '0',
  `price` double(8,2) NOT NULL,
  `partial_amount` double(8,2) NOT NULL,
  `related_doc` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  `paid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `bookings` VALUES (1,'2016-03-16 09:52:02','2016-03-16 11:40:18',3,0,2,10,'2016-03-29 08:00:00','2016-04-06 08:00:00','1680','',2,NULL,0,4567,0,'Račun br. 213-1-1','',1,'');
INSERT INTO `bookings` VALUES (2,'2016-03-16 11:11:10','2016-03-16 11:11:10',1,0,1,10,'2016-03-10 08:00:00','2016-03-10 08:00:00','','',0,NULL,0,0,0,'','',0,'');
DROP TABLE IF EXISTS `checklists`;
CREATE TABLE `checklists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `check_date` date NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `checklists` VALUES (1,'2016-03-16 10:38:11','2016-03-16 10:38:50',3,'2016-03-16',4,'{\"kilometraza_vozila\":\"53000\",\"brisaci\":\"+\",\"kratka_svjetla\":\"-\",\"duga_svjetla\":\"-\",\"zmigavci\":\"-\",\"svjetlo_registracije\":\"+\",\"svjetlo_kocnice\":\"+\",\"svjetlo_rikverc\":\"+\",\"instrument_ploca\":\"+\",\"tekucina_za_stakla\":\"+\",\"tekucina_za_hladenje\":\"+\",\"tekucina_serva\":\"+\",\"tekucina_kocnica\":\"+\",\"gume_tlak_stanje\":\"-\",\"rad_ventilacije_brzine\":\"+\",\"rad_klima_uredaja\":\"+\",\"stanje_sjedala\":\"+\",\"stanje_ut_prostora\":\"+\",\"pp_aparat_ispravnost\":\"+\",\"rezervna_guma\":\"+\",\"dizalica\":\"+\",\"prva_pomoc\":\"+\",\"zarulje\":\"+\",\"prsluk\":\"+\",\"knjizica_vozila\":\"+\",\"polica_auto_odgovornosti\":\"+\",\"zeleni_karton\":\"+\",\"eu_izvjestaj_stete\":\"+\",\"napomena\":\"Gledano nakon udesa\",\"btnSave\":\"\"}');
DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `oib` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_rate` double(8,2) NOT NULL DEFAULT '1.25',
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address_2` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `zip` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `country_id` int(11) NOT NULL DEFAULT '52',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobile` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fax` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `web` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ziro` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bank` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `companies` VALUES (1,'2016-03-02 12:10:06','2016-03-02 12:10:06','','',1.25,'','','','',52,'','','','','','','','','','','');
INSERT INTO `companies` VALUES (2,'2016-03-05 07:51:26','2016-03-09 15:34:56','TIHI KOD d.o.o.','12121212121',1.25,'Mandlova 1','','10040','Zagreb',52,'','','01 7888123','','','','','',';/images/logo-500.png','','');
INSERT INTO `companies` VALUES (3,'2016-03-10 09:01:45','2016-03-10 09:01:45','Demos d.o.o.','1234567890123',1.25,'Ulica BB','','10000','Zagreb',52,'','','','','','','','','','','');
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `groups` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `company` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `company_long` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `oib` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mb` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address_2` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `zip` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `country_id` int(11) NOT NULL DEFAULT '52',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobile` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fax` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `web` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `contacts` VALUES (1,'2016-03-09 15:37:45','2016-03-09 15:37:45',2,'','','','Neka Firma','','','','','','','',52,'','','','','','','','');
INSERT INTO `contacts` VALUES (2,'2016-03-16 09:42:49','2016-03-16 10:16:14',3,'Pravna osoba','','','Moja Tvrtka d.o.o.','','1234567890123','','Ilica 123','','10000','Zagreb',52,'info@rentalica.net','','','','','','','');
INSERT INTO `contacts` VALUES (3,'2016-03-16 10:17:17','2016-03-16 10:17:17',3,'Fizička osoba','Pero','Perić','','','','','Maksimirska 123','','10000','Zagreb',52,'pero.peric@gmail.com','0981234567','','','','Pero Perić','0981234567','');
INSERT INTO `contacts` VALUES (4,'2016-03-16 10:18:47','2016-03-16 10:18:47',3,'Pravna osoba','','','ZSS d.o.o.','','111222333513','','Slavonska 13','','51000','Rijeka',52,'zss@zss.hr','091/1234567','051/123456','051/123457','www.zss.hr','Mladen Horvat','098/1234567','');
INSERT INTO `contacts` VALUES (5,'2016-03-16 10:20:45','2016-03-16 10:20:45',3,'Pravna osoba,Servisni centar','','','Servisi i dijelovi d.o.o.','','123123456456','','Jakuševačka 123','','10000','Zagreb',52,'','095/1234567','01/1234567','','www.servisi-i-dijelovi.hr','Marko Kovačić','095/1234567','Servis radi\r\nKompletno održavanje vozila\r\nLimarija');
DROP TABLE IF EXISTS `contract_templates`;
CREATE TABLE `contract_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `contracts`;
CREATE TABLE `contracts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dated_at` date DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `booking_id` int(11) DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `pdf` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `contracts` VALUES (1,'2016-03-16 11:28:49','2016-03-16 11:28:49',3,'','2016-03-16','',0,4,2,'{\"najmoprimac\":\"Moja Tvrtka d.o.o.\",\"adresa\":\"Ilica 123\",\"oib\":\"1234567890123\",\"registracija_vozila\":\"VW Golf 6 DA-838-SZ\",\"datum_preuzimanja\":\"01.03.2016\",\"hour\":\"08\",\"minute\":\"00\",\"vrijeme_preuzimanja\":\"08:00\",\"datum_povratka\":\"04.03.2016\",\"vrijeme_povratka\":\"08:00\",\"cijena_po_danu\":\"324\",\"ukupno_dana\":\"3\",\"dnevna_kilometraza\":\"120\",\"popust\":\"12\",\"vozac\":\"Mladen Horvat\",\"vozac_adresa\":\"Ninaks 20\",\"vozac_zemlja\":\"Hrvatska\",\"vozac_broj_vozacke\":\"93733\",\"vozac_telefon\":\"00385 99 1122112\",\"dodatni_kilometri\":\"100\",\"dodatni_sat_najma\":\"0\",\"preuzimanje_izvan_rv\":\"75\",\"dostava_vozila_na_adresu\":\"0\",\"napomena\":\"Korisnik nema Hrvatsku voza\\u010dku.\",\"napomena2\":\"\",\"ugovor_napisao\":\"Rentalica\",\"btnSave\":\"\"}','');
DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pri` smallint(6) NOT NULL DEFAULT '0',
  `iso3166` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `iso3166a3` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `countries` VALUES (1,'Afghanistan','af',2,'af','');
INSERT INTO `countries` VALUES (2,'Albania','al',3,'al','');
INSERT INTO `countries` VALUES (3,'Algeria','dz',4,'dz','');
INSERT INTO `countries` VALUES (4,'American Samoa','as',5,'as','');
INSERT INTO `countries` VALUES (5,'Andorra','ad',6,'ad','');
INSERT INTO `countries` VALUES (6,'Angola','ao',7,'ao','');
INSERT INTO `countries` VALUES (7,'Anguilla','ai',8,'ai','');
INSERT INTO `countries` VALUES (8,'Antarctica','aq',9,'aq','');
INSERT INTO `countries` VALUES (9,'Antigua and Barbuda','ag',10,'ag','');
INSERT INTO `countries` VALUES (10,'Argentina','ar',11,'ar','');
INSERT INTO `countries` VALUES (11,'Armenia','am',12,'am','');
INSERT INTO `countries` VALUES (12,'Aruba','aw',13,'aw','');
INSERT INTO `countries` VALUES (13,'Australia','au',14,'au','');
INSERT INTO `countries` VALUES (14,'Austria','at',15,'at','');
INSERT INTO `countries` VALUES (15,'Azerbaijan','az',16,'az','');
INSERT INTO `countries` VALUES (16,'Bahamas','bs',17,'bs','');
INSERT INTO `countries` VALUES (17,'Bahrain','bh',18,'bh','');
INSERT INTO `countries` VALUES (18,'Bangladesh','bd',19,'bd','');
INSERT INTO `countries` VALUES (19,'Barbados','bb',20,'bb','');
INSERT INTO `countries` VALUES (20,'Belarus','by',21,'by','');
INSERT INTO `countries` VALUES (21,'Belgium','be',22,'be','');
INSERT INTO `countries` VALUES (22,'Belize','bz',23,'bz','');
INSERT INTO `countries` VALUES (23,'Benin','bj',24,'bj','');
INSERT INTO `countries` VALUES (24,'Bermuda','bm',25,'bm','');
INSERT INTO `countries` VALUES (25,'Bhutan','bt',26,'bt','');
INSERT INTO `countries` VALUES (26,'Bolivia','bo',27,'bo','');
INSERT INTO `countries` VALUES (27,'Bosnia and Herzegovina','ba',28,'ba','');
INSERT INTO `countries` VALUES (28,'Botswana','bw',29,'bw','');
INSERT INTO `countries` VALUES (29,'Bouvet Island','bv',30,'bv','');
INSERT INTO `countries` VALUES (30,'Brazil','br',31,'br','');
INSERT INTO `countries` VALUES (31,'British Indian Ocean Territory','io',32,'io','');
INSERT INTO `countries` VALUES (32,'Brunei darussalam','bn',33,'bn','');
INSERT INTO `countries` VALUES (33,'Bulgaria','bg',34,'bg','');
INSERT INTO `countries` VALUES (34,'Burkina Faso','bf',35,'bf','');
INSERT INTO `countries` VALUES (35,'Burundi','bi',36,'bi','');
INSERT INTO `countries` VALUES (36,'Cambodia','kh',37,'kh','');
INSERT INTO `countries` VALUES (37,'Cameroon','cm',38,'cm','');
INSERT INTO `countries` VALUES (38,'Canada','ca',39,'ca','');
INSERT INTO `countries` VALUES (39,'Cape Verde','cv',40,'cv','');
INSERT INTO `countries` VALUES (40,'Cayman Island','ky',41,'ky','');
INSERT INTO `countries` VALUES (41,'Central African Republic','cf',42,'cf','');
INSERT INTO `countries` VALUES (42,'Chad','td',43,'td','');
INSERT INTO `countries` VALUES (43,'Chile','cl',44,'cl','');
INSERT INTO `countries` VALUES (44,'China','cn',45,'cn','');
INSERT INTO `countries` VALUES (45,'Christmas Island','cx',46,'cx','');
INSERT INTO `countries` VALUES (46,'Cocos (Keeling) Island','cc',47,'cc','');
INSERT INTO `countries` VALUES (47,'Colombia','co',48,'co','');
INSERT INTO `countries` VALUES (48,'Comoros','km',49,'km','');
INSERT INTO `countries` VALUES (49,'Congo','cg',50,'cg','');
INSERT INTO `countries` VALUES (50,'Cook Island','ck',51,'ck','');
INSERT INTO `countries` VALUES (51,'Costa Rica','cr',52,'cr','');
INSERT INTO `countries` VALUES (52,'Hrvatska (Croatia)','hr',1,'hr','');
INSERT INTO `countries` VALUES (53,'Cuba','cu',54,'cu','');
INSERT INTO `countries` VALUES (54,'Cyprus','cy',55,'cy','');
INSERT INTO `countries` VALUES (55,'Czech Republic','cz',56,'cz','');
INSERT INTO `countries` VALUES (56,'Denmark','dk',57,'dk','');
INSERT INTO `countries` VALUES (57,'Djibouti','dj',58,'dj','');
INSERT INTO `countries` VALUES (58,'Dominica','dm',59,'dm','');
INSERT INTO `countries` VALUES (59,'Dominican Republic','do',60,'do','');
INSERT INTO `countries` VALUES (61,'Ecuador','ec',62,'ec','');
INSERT INTO `countries` VALUES (62,'Egypt','eg',63,'eg','');
INSERT INTO `countries` VALUES (63,'El Salvador','sv',64,'sv','');
INSERT INTO `countries` VALUES (64,'Equatorial Guinea','gq',65,'gq','');
INSERT INTO `countries` VALUES (65,'Eritrea','er',66,'er','');
INSERT INTO `countries` VALUES (66,'Estonia','ee',67,'ee','');
INSERT INTO `countries` VALUES (67,'Ethiopia','et',68,'et','');
INSERT INTO `countries` VALUES (68,'Falkland Islands (Malvinas)','fk',69,'fk','');
INSERT INTO `countries` VALUES (69,'Faroe Islands','fo',70,'fo','');
INSERT INTO `countries` VALUES (70,'Fiji','fj',71,'fj','');
INSERT INTO `countries` VALUES (71,'Finland','fi',72,'fi','');
INSERT INTO `countries` VALUES (72,'France','fr',73,'fr','');
INSERT INTO `countries` VALUES (74,'French Guiana','gf',75,'gf','');
INSERT INTO `countries` VALUES (75,'French Polynesia','pf',76,'pf','');
INSERT INTO `countries` VALUES (76,'French Southern Territories','tf',77,'tf','');
INSERT INTO `countries` VALUES (77,'Gabon','ga',78,'ga','');
INSERT INTO `countries` VALUES (78,'Gambia','gm',79,'gm','');
INSERT INTO `countries` VALUES (79,'Georgia','ge',80,'ge','');
INSERT INTO `countries` VALUES (80,'Germany','de',81,'de','');
INSERT INTO `countries` VALUES (81,'Ghana','gh',82,'gh','');
INSERT INTO `countries` VALUES (82,'Gibraltar','gi',83,'gi','');
INSERT INTO `countries` VALUES (83,'Greece','gr',84,'gr','');
INSERT INTO `countries` VALUES (84,'Greenland','gl',85,'gl','');
INSERT INTO `countries` VALUES (85,'Grenada','gd',86,'gd','');
INSERT INTO `countries` VALUES (86,'Guadeloupe','gp',87,'gp','');
INSERT INTO `countries` VALUES (87,'Guam','gu',88,'gu','');
INSERT INTO `countries` VALUES (88,'Guatemala','gt',89,'gt','');
INSERT INTO `countries` VALUES (89,'Guinea','gn',90,'gn','');
INSERT INTO `countries` VALUES (90,'Guinea Bissau','gw',91,'gw','');
INSERT INTO `countries` VALUES (91,'Guyana','gy',92,'gy','');
INSERT INTO `countries` VALUES (92,'Haiti','ht',93,'ht','');
INSERT INTO `countries` VALUES (93,'Heard Island and Mc Donald Islands','hm',94,'hm','');
INSERT INTO `countries` VALUES (94,'Honduras','hn',95,'hn','');
INSERT INTO `countries` VALUES (95,'Hong Kong','hk',96,'hk','');
INSERT INTO `countries` VALUES (96,'Hungary','hu',97,'hu','');
INSERT INTO `countries` VALUES (97,'Iceland','is',98,'is','');
INSERT INTO `countries` VALUES (98,'India','in',99,'in','');
INSERT INTO `countries` VALUES (99,'Indonesia','id',100,'id','');
INSERT INTO `countries` VALUES (100,'Iran (Islamic republic of)','ir',101,'ir','');
INSERT INTO `countries` VALUES (101,'Iraq','iq',102,'iq','');
INSERT INTO `countries` VALUES (102,'Ireland','ie',103,'ie','');
INSERT INTO `countries` VALUES (103,'Israel','il',104,'il','');
INSERT INTO `countries` VALUES (104,'Italy','it',105,'it','');
INSERT INTO `countries` VALUES (106,'Jamaica','jm',107,'jm','');
INSERT INTO `countries` VALUES (107,'Japan','jp',108,'jp','');
INSERT INTO `countries` VALUES (108,'Jordan','jo',109,'jo','');
INSERT INTO `countries` VALUES (109,'Kazakhstan','kz',110,'kz','');
INSERT INTO `countries` VALUES (110,'Kenya','ke',111,'ke','');
INSERT INTO `countries` VALUES (111,'Kiribati','ki',112,'ki','');
INSERT INTO `countries` VALUES (112,'Korea, Democratic people´s Republic of','kp',113,'kp','');
INSERT INTO `countries` VALUES (113,'Korea, Republic of','kr',114,'kr','');
INSERT INTO `countries` VALUES (114,'Kuwait','kw',115,'kw','');
INSERT INTO `countries` VALUES (115,'Kyrgyzstan','kg',116,'kg','');
INSERT INTO `countries` VALUES (116,'Lao people´s democratic republic','la',117,'la','');
INSERT INTO `countries` VALUES (117,'Latvia','lv',118,'lv','');
INSERT INTO `countries` VALUES (118,'Lebanon','lb',119,'lb','');
INSERT INTO `countries` VALUES (119,'Lesotho','ls',120,'ls','');
INSERT INTO `countries` VALUES (120,'Liberia','lr',121,'lr','');
INSERT INTO `countries` VALUES (121,'Libyan Arab Jamahiriya','ly',122,'ly','');
INSERT INTO `countries` VALUES (122,'Liechtenstein','li',123,'li','');
INSERT INTO `countries` VALUES (123,'Lithuania','lt',124,'lt','');
INSERT INTO `countries` VALUES (124,'Luxembourg','lu',125,'lu','');
INSERT INTO `countries` VALUES (125,'Macau','mo',126,'mo','');
INSERT INTO `countries` VALUES (126,'Madagascar','mg',127,'mg','');
INSERT INTO `countries` VALUES (127,'Malawi','mw',128,'mw','');
INSERT INTO `countries` VALUES (128,'Malaysia','my',129,'my','');
INSERT INTO `countries` VALUES (129,'Maldives','mv',130,'mv','');
INSERT INTO `countries` VALUES (130,'Mali','ml',131,'ml','');
INSERT INTO `countries` VALUES (131,'Malta','mt',132,'mt','');
INSERT INTO `countries` VALUES (132,'Marshall islands','mh',133,'mh','');
INSERT INTO `countries` VALUES (133,'Martinique','mq',134,'mq','');
INSERT INTO `countries` VALUES (134,'Mauritania','mr',135,'mr','');
INSERT INTO `countries` VALUES (135,'Mayotte','yt',136,'yt','');
INSERT INTO `countries` VALUES (136,'Mexico','mx',137,'mx','');
INSERT INTO `countries` VALUES (137,'Micronesia (Federated States of)','fm',138,'fm','');
INSERT INTO `countries` VALUES (138,'Moldova, Republic of','md',139,'md','');
INSERT INTO `countries` VALUES (139,'Monaco','mc',140,'mc','');
INSERT INTO `countries` VALUES (140,'Mongolia','mn',141,'mn','');
INSERT INTO `countries` VALUES (141,'Monserrat','ms',142,'ms','');
INSERT INTO `countries` VALUES (142,'Morocco','ma',143,'ma','');
INSERT INTO `countries` VALUES (143,'Mozambigue','mz',144,'mz','');
INSERT INTO `countries` VALUES (144,'Myanmar','mm',145,'mm','');
INSERT INTO `countries` VALUES (145,'Namibia','na',146,'na','');
INSERT INTO `countries` VALUES (146,'Nauru','nr',147,'nr','');
INSERT INTO `countries` VALUES (147,'Nepal','np',148,'np','');
INSERT INTO `countries` VALUES (148,'Netherlands','nl',149,'nl','');
INSERT INTO `countries` VALUES (149,'Netherlands Antilles','an',150,'an','');
INSERT INTO `countries` VALUES (150,'New Caledonia','nc',151,'nc','');
INSERT INTO `countries` VALUES (151,'New Zealand','nz',152,'nz','');
INSERT INTO `countries` VALUES (152,'Nicaragua','ni',153,'ni','');
INSERT INTO `countries` VALUES (153,'Niger','ne',154,'ne','');
INSERT INTO `countries` VALUES (154,'Nigeria','ng',155,'ng','');
INSERT INTO `countries` VALUES (155,'Niue','nu',156,'nu','');
INSERT INTO `countries` VALUES (156,'Norfolk Islands','nf',157,'nf','');
INSERT INTO `countries` VALUES (157,'Northern Mariana Islands','mp',158,'mp','');
INSERT INTO `countries` VALUES (158,'Norway','no',159,'no','');
INSERT INTO `countries` VALUES (159,'Oman','om',160,'om','');
INSERT INTO `countries` VALUES (160,'Pakistan','pk',161,'pk','');
INSERT INTO `countries` VALUES (161,'Palau','pw',162,'pw','');
INSERT INTO `countries` VALUES (162,'Panama','pa',163,'pa','');
INSERT INTO `countries` VALUES (163,'Papua New Guinea','pg',164,'pg','');
INSERT INTO `countries` VALUES (164,'Paraguay','py',165,'py','');
INSERT INTO `countries` VALUES (165,'Peru','pe',166,'pe','');
INSERT INTO `countries` VALUES (166,'Philippines','ph',167,'ph','');
INSERT INTO `countries` VALUES (167,'Pitcairn','pn',168,'pn','');
INSERT INTO `countries` VALUES (168,'Poland','pl',169,'pl','');
INSERT INTO `countries` VALUES (169,'Portugal','pt',170,'pt','');
INSERT INTO `countries` VALUES (170,'Puerto Rico','pr',171,'pr','');
INSERT INTO `countries` VALUES (171,'Qatar','qa',172,'qa','');
INSERT INTO `countries` VALUES (172,'Reunion','re',173,'re','');
INSERT INTO `countries` VALUES (173,'Romania','ro',174,'ro','');
INSERT INTO `countries` VALUES (174,'Russia Federation','ru',175,'ru','');
INSERT INTO `countries` VALUES (175,'Rwanda','rw',176,'rw','');
INSERT INTO `countries` VALUES (176,'Saint Helena','sh',177,'sh','');
INSERT INTO `countries` VALUES (177,'Saint Kitts and Nevis','kn',178,'kn','');
INSERT INTO `countries` VALUES (178,'Saint Lucia','lc',179,'lc','');
INSERT INTO `countries` VALUES (179,'Saint Pierre et Miquelon','pm',180,'pm','');
INSERT INTO `countries` VALUES (180,'Saint Vincent and the Grenadines','vc',181,'vc','');
INSERT INTO `countries` VALUES (181,'Samoa','ws',182,'ws','');
INSERT INTO `countries` VALUES (182,'San Marino','sm',183,'sm','');
INSERT INTO `countries` VALUES (183,'Sao Tome and Principe','st',184,'st','');
INSERT INTO `countries` VALUES (184,'Saudi Arabia','sa',185,'sa','');
INSERT INTO `countries` VALUES (185,'Senegal','sn',186,'sn','');
INSERT INTO `countries` VALUES (186,'Serbia','rs',187,'rs','');
INSERT INTO `countries` VALUES (187,'Seychelles','sc',188,'sc','');
INSERT INTO `countries` VALUES (188,'Sierra Leone','sl',189,'sl','');
INSERT INTO `countries` VALUES (189,'Singapore','sg',190,'sg','');
INSERT INTO `countries` VALUES (190,'Slovakia','sk',191,'sk','');
INSERT INTO `countries` VALUES (191,'Slovenia','si',192,'si','');
INSERT INTO `countries` VALUES (192,'Solomon Islands','sb',193,'sb','');
INSERT INTO `countries` VALUES (193,'Somalia','so',194,'so','');
INSERT INTO `countries` VALUES (194,'South Africa','za',195,'za','');
INSERT INTO `countries` VALUES (195,'Spain','es',196,'es','');
INSERT INTO `countries` VALUES (196,'Sri Lanka','lk',197,'lk','');
INSERT INTO `countries` VALUES (197,'Sudan','sd',198,'sd','');
INSERT INTO `countries` VALUES (198,'Suriname','sr',199,'sr','');
INSERT INTO `countries` VALUES (199,'Svalbard and Jan Mayen','sj',200,'sj','');
INSERT INTO `countries` VALUES (200,'Swaziland','sz',201,'sz','');
INSERT INTO `countries` VALUES (201,'Sweden','se',202,'se','');
INSERT INTO `countries` VALUES (202,'Switzerland','ch',203,'ch','');
INSERT INTO `countries` VALUES (203,'Syria Arab Republic','sy',204,'sy','');
INSERT INTO `countries` VALUES (204,'Taiwan, Province of China','tw',205,'tw','');
INSERT INTO `countries` VALUES (205,'Tajikistan','tj',206,'tj','');
INSERT INTO `countries` VALUES (206,'Tanzania, United Republic Of','tz',207,'tz','');
INSERT INTO `countries` VALUES (207,'Thailand','th',208,'th','');
INSERT INTO `countries` VALUES (208,'Togo','tg',209,'tg','');
INSERT INTO `countries` VALUES (209,'Tokelau','tk',210,'tk','');
INSERT INTO `countries` VALUES (210,'Tonga','to',211,'to','');
INSERT INTO `countries` VALUES (211,'Trinidad and Tobago','tt',212,'tt','');
INSERT INTO `countries` VALUES (212,'Tunisia','tn',213,'tn','');
INSERT INTO `countries` VALUES (213,'Turkey','tr',214,'tr','');
INSERT INTO `countries` VALUES (214,'Turkmenistan','tm',215,'tm','');
INSERT INTO `countries` VALUES (215,'Turks and Caicos Islands','tc',216,'tc','');
INSERT INTO `countries` VALUES (216,'Tuvalu','tv',217,'tv','');
INSERT INTO `countries` VALUES (217,'Uganda','ug',218,'ug','');
INSERT INTO `countries` VALUES (218,'Ukraine','ua',219,'ua','');
INSERT INTO `countries` VALUES (219,'United Arab Emirates','ae',220,'ae','');
INSERT INTO `countries` VALUES (220,'United Kingdom','gb',221,'gb','');
INSERT INTO `countries` VALUES (221,'United States','us',222,'us','');
INSERT INTO `countries` VALUES (222,'United States Minor Outlying Islands','um',223,'um','');
INSERT INTO `countries` VALUES (223,'Uruguay','uy',224,'uy','');
INSERT INTO `countries` VALUES (224,'Uzbekistan','uz',225,'uz','');
INSERT INTO `countries` VALUES (225,'Vanuatu','vu',226,'vu','');
INSERT INTO `countries` VALUES (226,'Vatican City State (Holy See)','va',227,'va','');
INSERT INTO `countries` VALUES (227,'Venezuela','ve',228,'ve','');
INSERT INTO `countries` VALUES (228,'Vietnam','vn',229,'vn','');
INSERT INTO `countries` VALUES (229,'Virgin Islands (British)','vg',230,'vg','');
INSERT INTO `countries` VALUES (230,'Virgin Islands (U.S.)','vi',231,'vi','');
INSERT INTO `countries` VALUES (231,'Wallis and Futuna Islands','wf',232,'wf','');
INSERT INTO `countries` VALUES (232,'Western Sahara','eh',233,'eh','');
INSERT INTO `countries` VALUES (233,'Yemen','ye',234,'ye','');
INSERT INTO `countries` VALUES (236,'Zambia','zm',237,'zm','');
INSERT INTO `countries` VALUES (237,'Zimbabwe','zw',238,'zw','');
INSERT INTO `countries` VALUES (238,'Aland Islands','ax',240,'ax','');
INSERT INTO `countries` VALUES (239,'Confo, The Democratic Republic of thec','cd',240,'cd','');
INSERT INTO `countries` VALUES (240,'Cote D\'ivoire','ci',240,'ci','');
INSERT INTO `countries` VALUES (241,'Guernsey','gg',240,'gg','');
INSERT INTO `countries` VALUES (243,'Isle of Man','im',240,'im','');
INSERT INTO `countries` VALUES (244,'Jersey','je',240,'je','');
INSERT INTO `countries` VALUES (245,'Macedonia (The Former Yugoslav Republic of)','mk',240,'mk','');
INSERT INTO `countries` VALUES (246,'Mauritius','mu',240,'mu','');
INSERT INTO `countries` VALUES (247,'Montenegro','me',240,'me','');
INSERT INTO `countries` VALUES (248,'Palestinian Territory, Occupied','ps',240,'ps','');
INSERT INTO `countries` VALUES (249,'Saint Barthelemy','bl',240,'bl','');
INSERT INTO `countries` VALUES (250,'Saint Martin','mf',240,'mf','');
INSERT INTO `countries` VALUES (251,'South Georgia and the South Sandwich Islands','gs',240,'gs','');
INSERT INTO `countries` VALUES (252,'Timor-Leste','tl',240,'tl','');
DROP TABLE IF EXISTS `damages`;
CREATE TABLE `damages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `vehicle_id` int(11) NOT NULL DEFAULT '0',
  `vehicle_km` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `date` date DEFAULT NULL,
  `where` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `damages` VALUES (1,'2016-03-16 10:31:20','2016-03-16 10:31:20',3,'Prednja strana',4,'53000','2016-03-16','Novalja','2','Prilikom skretanja na makadamski put,udario u kamen','3','Vozilo prijavljeno kasko osiguranju i popravljeno 20.3.');
DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `uploaded_by` int(10) unsigned NOT NULL DEFAULT '0',
  `shared` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ext` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `size` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `documents` VALUES (1,'2016-03-16 09:21:14','2016-03-16 09:21:14',3,0,'Sken_dokumenata.pdf','pdf',18633,'/files/Sken_dokumenata.pdf');
INSERT INTO `documents` VALUES (2,'2016-03-16 10:03:48','2016-03-16 10:03:48',3,0,'670px-Flick-a-Cigarette-Step-4.jpg','jpg',79804,'/files/670px-Flick-a-Cigarette-Step-4.jpg');
INSERT INTO `documents` VALUES (3,'2016-03-16 10:31:12','2016-03-16 10:31:12',3,0,'271567062986_1.jpg','jpg',53872,'/files/271567062986_1.jpg');
INSERT INTO `documents` VALUES (4,'2016-03-16 11:23:55','2016-03-16 11:23:55',3,0,'SKEN_DOKUMENTA.pdf','pdf',52703,'/files/SKEN_DOKUMENTA.pdf');
INSERT INTO `documents` VALUES (5,'2016-03-16 11:24:56','2016-03-16 11:24:56',3,0,'SKEN_DOKUMENTA.pdf','pdf',52703,'/files/SKEN_DOKUMENTA.pdf');
INSERT INTO `documents` VALUES (6,'2016-03-16 11:25:18','2016-03-16 11:25:18',3,0,'SKEN_DOKUMENTA.pdf','pdf',52703,'/files/SKEN_DOKUMENTA.pdf');
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `migrations_migration_unique` (`migration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `migrations` VALUES ('2015_11_03_071430_create_user_groups_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_04_194404_create_users_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_05_071833_create_password_resets_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_05_072457_create_countries_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_155533_create_vehicles_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_155741_create_vehicle_services_tables.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_161025_create_damages_tables.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_165212_create_bookings_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_08_165314_create_contacts_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_14_084224_create_contract_templates_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_14_084433_create_contracts_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_19_091720_create_translation_tags_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_19_091923_create_translations_table.php',1);
INSERT INTO `migrations` VALUES ('2015_11_29_121909_create_documents_table.php',1);
INSERT INTO `migrations` VALUES ('2015_12_10_091238_create_checklists_table.php',1);
INSERT INTO `migrations` VALUES ('2016_01_19_114804_create_vehicle_history_kms_table.php',1);
INSERT INTO `migrations` VALUES ('2016_02_06_210557_create_audits_table.php',1);
INSERT INTO `migrations` VALUES ('2016_02_27_075343_create_companies_table.php',1);
INSERT INTO `migrations` VALUES ('2016_03_01_045318_add_company_id_to_users.php',1);
INSERT INTO `migrations` VALUES ('2016_03_01_045600_seed_initial_users_and_groups.php',1);
INSERT INTO `migrations` VALUES ('2016_03_02_210349_add_fields_to_company.php',2);
INSERT INTO `migrations` VALUES ('2016_03_16_094913_add_fields_to_bookings.php',3);
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `password_resets_email_unique` (`email`),
  UNIQUE KEY `password_resets_token_unique` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `translation_tags`;
CREATE TABLE `translation_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `scope` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=258 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `translation_tags` VALUES (1,'2016-03-05 07:51:38','2016-03-05 07:51:38','','');
INSERT INTO `translation_tags` VALUES (2,'2016-03-05 07:51:38','2016-03-05 07:51:38','','');
INSERT INTO `translation_tags` VALUES (3,'2016-03-05 07:51:38','2016-03-05 07:51:38','','');
INSERT INTO `translation_tags` VALUES (4,'2016-03-05 07:51:38','2016-03-05 07:51:38','','');
INSERT INTO `translation_tags` VALUES (5,'2016-03-05 07:51:38','2016-03-05 07:51:38','','');
INSERT INTO `translation_tags` VALUES (6,'2016-03-05 07:51:38','2016-03-05 07:51:38','','');
INSERT INTO `translation_tags` VALUES (7,'2016-03-05 07:51:38','2016-03-05 07:51:38','','');
INSERT INTO `translation_tags` VALUES (8,'2016-03-05 07:51:38','2016-03-05 07:51:38','','');
INSERT INTO `translation_tags` VALUES (9,'2016-03-05 07:51:38','2016-03-05 07:51:38','','');
INSERT INTO `translation_tags` VALUES (10,'2016-03-05 07:51:38','2016-03-05 07:51:38','','');
INSERT INTO `translation_tags` VALUES (11,'2016-03-05 07:51:38','2016-03-05 07:51:38','','');
INSERT INTO `translation_tags` VALUES (12,'2016-03-05 07:51:38','2016-03-05 07:51:38','','');
INSERT INTO `translation_tags` VALUES (13,'2016-03-05 07:51:49','2016-03-05 07:51:49','','');
INSERT INTO `translation_tags` VALUES (14,'2016-03-05 07:51:49','2016-03-05 07:51:49','','');
INSERT INTO `translation_tags` VALUES (15,'2016-03-05 07:51:49','2016-03-05 07:51:49','','');
INSERT INTO `translation_tags` VALUES (16,'2016-03-05 07:51:49','2016-03-05 07:51:49','','');
INSERT INTO `translation_tags` VALUES (17,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (18,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (19,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (20,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (21,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (22,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (23,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (24,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (25,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (26,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (27,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (28,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (29,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (30,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (31,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (32,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (33,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (34,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (35,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (36,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (37,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (38,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (39,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (40,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (41,'2016-03-05 07:51:52','2016-03-05 07:51:52','','');
INSERT INTO `translation_tags` VALUES (42,'2016-03-05 07:52:25','2016-03-05 07:52:25','','');
INSERT INTO `translation_tags` VALUES (43,'2016-03-05 07:52:25','2016-03-05 07:52:25','','');
INSERT INTO `translation_tags` VALUES (44,'2016-03-05 07:52:25','2016-03-05 07:52:25','','');
INSERT INTO `translation_tags` VALUES (45,'2016-03-05 07:52:25','2016-03-05 07:52:25','','');
INSERT INTO `translation_tags` VALUES (46,'2016-03-05 07:52:25','2016-03-05 07:52:25','','');
INSERT INTO `translation_tags` VALUES (47,'2016-03-05 07:52:25','2016-03-05 07:52:25','','');
INSERT INTO `translation_tags` VALUES (48,'2016-03-05 07:52:25','2016-03-05 07:52:25','','');
INSERT INTO `translation_tags` VALUES (49,'2016-03-05 07:52:25','2016-03-05 07:52:25','','');
INSERT INTO `translation_tags` VALUES (50,'2016-03-05 07:52:25','2016-03-05 07:52:25','','');
INSERT INTO `translation_tags` VALUES (51,'2016-03-05 07:52:25','2016-03-05 07:52:25','','');
INSERT INTO `translation_tags` VALUES (52,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (53,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (54,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (55,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (56,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (57,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (58,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (59,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (60,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (61,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (62,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (63,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (64,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (65,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (66,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (67,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (68,'2016-03-05 07:52:27','2016-03-05 07:52:27','','');
INSERT INTO `translation_tags` VALUES (69,'2016-03-05 07:52:49','2016-03-05 07:52:49','','');
INSERT INTO `translation_tags` VALUES (70,'2016-03-05 07:52:49','2016-03-05 07:52:49','','');
INSERT INTO `translation_tags` VALUES (71,'2016-03-05 07:52:49','2016-03-05 07:52:49','','');
INSERT INTO `translation_tags` VALUES (72,'2016-03-05 07:52:49','2016-03-05 07:52:49','','');
INSERT INTO `translation_tags` VALUES (73,'2016-03-05 07:52:49','2016-03-05 07:52:49','','');
INSERT INTO `translation_tags` VALUES (74,'2016-03-05 07:52:49','2016-03-05 07:52:49','','');
INSERT INTO `translation_tags` VALUES (75,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (76,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (77,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (78,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (79,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (80,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (81,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (82,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (83,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (84,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (85,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (86,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (87,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (88,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (89,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (90,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (91,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (92,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (93,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (94,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (95,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (96,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (97,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (98,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (99,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (100,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (101,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (102,'2016-03-05 07:52:51','2016-03-05 07:52:51','','');
INSERT INTO `translation_tags` VALUES (103,'2016-03-05 08:31:21','2016-03-05 08:31:21','','');
INSERT INTO `translation_tags` VALUES (104,'2016-03-05 08:31:21','2016-03-05 08:31:21','','');
INSERT INTO `translation_tags` VALUES (105,'2016-03-05 08:31:21','2016-03-05 08:31:21','','');
INSERT INTO `translation_tags` VALUES (106,'2016-03-05 08:31:21','2016-03-05 08:31:21','','');
INSERT INTO `translation_tags` VALUES (107,'2016-03-05 08:31:21','2016-03-05 08:31:21','','');
INSERT INTO `translation_tags` VALUES (108,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (109,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (110,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (111,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (112,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (113,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (114,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (115,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (116,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (117,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (118,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (119,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (120,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (121,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (122,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (123,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (124,'2016-03-05 08:31:24','2016-03-05 08:31:24','','');
INSERT INTO `translation_tags` VALUES (125,'2016-03-07 06:43:04','2016-03-07 06:43:04','','Logotip tvrtke za dokumente');
INSERT INTO `translation_tags` VALUES (126,'2016-03-07 06:43:04','2016-03-07 06:43:04','','Naziv tvrtke');
INSERT INTO `translation_tags` VALUES (127,'2016-03-07 06:43:04','2016-03-07 06:43:04','','OIB');
INSERT INTO `translation_tags` VALUES (128,'2016-03-07 06:43:04','2016-03-07 06:43:04','','Adresa sjedišta');
INSERT INTO `translation_tags` VALUES (129,'2016-03-07 06:43:04','2016-03-07 06:43:04','','Poštanski broj');
INSERT INTO `translation_tags` VALUES (130,'2016-03-07 06:43:04','2016-03-07 06:43:04','','Naziv mjesta');
INSERT INTO `translation_tags` VALUES (131,'2016-03-07 06:43:04','2016-03-07 06:43:04','','Država');
INSERT INTO `translation_tags` VALUES (132,'2016-03-07 06:43:04','2016-03-07 06:43:04','','Kontakt telefon');
INSERT INTO `translation_tags` VALUES (133,'2016-03-07 06:43:04','2016-03-07 06:43:04','','U sustavu PDV-a');
INSERT INTO `translation_tags` VALUES (134,'2016-03-07 06:43:04','2016-03-07 06:43:04','','Žiro račun');
INSERT INTO `translation_tags` VALUES (135,'2016-03-07 06:43:04','2016-03-07 06:43:04','','Kod banke');
INSERT INTO `translation_tags` VALUES (136,'2016-03-07 06:43:04','2016-03-07 06:43:04','','Save');
INSERT INTO `translation_tags` VALUES (137,'2016-03-07 06:43:23','2016-03-07 06:43:23','','Pretraga');
INSERT INTO `translation_tags` VALUES (138,'2016-03-07 06:43:23','2016-03-07 06:43:23','','Vozilo');
INSERT INTO `translation_tags` VALUES (139,'2016-03-07 06:43:23','2016-03-07 06:43:23','','Tip vozila');
INSERT INTO `translation_tags` VALUES (140,'2016-03-07 06:43:23','2016-03-07 06:43:23','','Status plaćanja');
INSERT INTO `translation_tags` VALUES (141,'2016-03-07 06:43:23','2016-03-07 06:43:23','','Od datuma');
INSERT INTO `translation_tags` VALUES (142,'2016-03-07 06:43:23','2016-03-07 06:43:23','','Do datuma');
INSERT INTO `translation_tags` VALUES (143,'2016-03-07 06:43:23','2016-03-07 06:43:23','','Prati datum');
INSERT INTO `translation_tags` VALUES (144,'2016-03-07 06:43:23','2016-03-07 06:43:23','','Vrsta usluge');
INSERT INTO `translation_tags` VALUES (145,'2016-03-07 06:43:23','2016-03-07 06:43:23','','Search');
INSERT INTO `translation_tags` VALUES (146,'2016-03-07 06:43:23','2016-03-07 06:43:23','','Reset filters');
INSERT INTO `translation_tags` VALUES (147,'2016-03-07 06:43:26','2016-03-07 06:43:26','','Početni datum');
INSERT INTO `translation_tags` VALUES (148,'2016-03-07 06:43:26','2016-03-07 06:43:26','','Početno vrijeme');
INSERT INTO `translation_tags` VALUES (149,'2016-03-07 06:43:26','2016-03-07 06:43:26','','Završni datum');
INSERT INTO `translation_tags` VALUES (150,'2016-03-07 06:43:26','2016-03-07 06:43:26','','Završno vrijeme');
INSERT INTO `translation_tags` VALUES (151,'2016-03-07 06:43:26','2016-03-07 06:43:26','','Vezani dokument');
INSERT INTO `translation_tags` VALUES (152,'2016-03-07 06:43:26','2016-03-07 06:43:26','','Klijent');
INSERT INTO `translation_tags` VALUES (153,'2016-03-07 06:43:26','2016-03-07 06:43:26','','Ukupna cijena');
INSERT INTO `translation_tags` VALUES (154,'2016-03-07 06:43:26','2016-03-07 06:43:26','','Uplaćeni iznos');
INSERT INTO `translation_tags` VALUES (155,'2016-03-07 06:43:26','2016-03-07 06:43:26','','Napomena');
INSERT INTO `translation_tags` VALUES (156,'2016-03-07 06:43:26','2016-03-07 06:43:26','','Početna kilometraža');
INSERT INTO `translation_tags` VALUES (157,'2016-03-07 06:43:26','2016-03-07 06:43:26','','Završna kilometraža');
INSERT INTO `translation_tags` VALUES (158,'2016-03-07 06:43:26','2016-03-07 06:43:26','','Dokumenti');
INSERT INTO `translation_tags` VALUES (159,'2016-03-07 06:43:26','2016-03-07 06:43:26','','Status najma');
INSERT INTO `translation_tags` VALUES (160,'2016-03-07 06:43:52','2016-03-07 06:43:52','','Per page');
INSERT INTO `translation_tags` VALUES (161,'2016-03-07 06:43:55','2016-03-07 06:43:55','','Naslov');
INSERT INTO `translation_tags` VALUES (162,'2016-03-07 06:43:55','2016-03-07 06:43:55','','Datum događaja');
INSERT INTO `translation_tags` VALUES (163,'2016-03-07 06:43:55','2016-03-07 06:43:55','','Mjesto događaja');
INSERT INTO `translation_tags` VALUES (164,'2016-03-07 06:43:55','2016-03-07 06:43:55','','Km na vozilu');
INSERT INTO `translation_tags` VALUES (165,'2016-03-07 06:43:55','2016-03-07 06:43:55','','Opis');
INSERT INTO `translation_tags` VALUES (166,'2016-03-07 06:43:55','2016-03-07 06:43:55','','Tko je oštetio vozilo');
INSERT INTO `translation_tags` VALUES (167,'2016-03-07 06:43:55','2016-03-07 06:43:55','','Status');
INSERT INTO `translation_tags` VALUES (168,'2016-03-07 14:50:00','2016-03-07 14:50:00','','Mjesto');
INSERT INTO `translation_tags` VALUES (169,'2016-03-07 14:50:00','2016-03-07 14:50:00','','Po stranici');
INSERT INTO `translation_tags` VALUES (170,'2016-03-07 14:50:02','2016-03-07 14:50:02','','Ime');
INSERT INTO `translation_tags` VALUES (171,'2016-03-07 14:50:02','2016-03-07 14:50:02','','Prezime');
INSERT INTO `translation_tags` VALUES (172,'2016-03-07 14:50:02','2016-03-07 14:50:02','','Tvrtka');
INSERT INTO `translation_tags` VALUES (173,'2016-03-07 14:50:02','2016-03-07 14:50:02','','Adresa');
INSERT INTO `translation_tags` VALUES (174,'2016-03-07 14:50:02','2016-03-07 14:50:02','','Tip kontakta');
INSERT INTO `translation_tags` VALUES (175,'2016-03-07 14:50:02','2016-03-07 14:50:02','','Email');
INSERT INTO `translation_tags` VALUES (176,'2016-03-07 14:50:02','2016-03-07 14:50:02','','Mobilni tel.');
INSERT INTO `translation_tags` VALUES (177,'2016-03-07 14:50:02','2016-03-07 14:50:02','','Tel.');
INSERT INTO `translation_tags` VALUES (178,'2016-03-07 14:50:02','2016-03-07 14:50:02','','Fax');
INSERT INTO `translation_tags` VALUES (179,'2016-03-07 14:50:02','2016-03-07 14:50:02','','Web');
INSERT INTO `translation_tags` VALUES (180,'2016-03-07 14:50:02','2016-03-07 14:50:02','','Mobitel');
INSERT INTO `translation_tags` VALUES (181,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Datum ugovora');
INSERT INTO `translation_tags` VALUES (182,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Ime ili naziv Najmoprimca');
INSERT INTO `translation_tags` VALUES (183,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Tip i registracija vozila');
INSERT INTO `translation_tags` VALUES (184,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Datum preuzimanja');
INSERT INTO `translation_tags` VALUES (185,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Vrijeme preuzimanja');
INSERT INTO `translation_tags` VALUES (186,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Datum povratka');
INSERT INTO `translation_tags` VALUES (187,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Vrijeme povratka');
INSERT INTO `translation_tags` VALUES (188,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Cijena po danu (bez PDV-a)');
INSERT INTO `translation_tags` VALUES (189,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Ukupno dana');
INSERT INTO `translation_tags` VALUES (190,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Dnevna kilometraža (km)');
INSERT INTO `translation_tags` VALUES (191,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Popust (%)');
INSERT INTO `translation_tags` VALUES (192,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Vozač');
INSERT INTO `translation_tags` VALUES (193,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Adresa vozača');
INSERT INTO `translation_tags` VALUES (194,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Zemlja vozača');
INSERT INTO `translation_tags` VALUES (195,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Broj vozačke');
INSERT INTO `translation_tags` VALUES (196,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Broj telefona vozača');
INSERT INTO `translation_tags` VALUES (197,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Dodatni kilometri (km)');
INSERT INTO `translation_tags` VALUES (198,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Dodatni sat najma (kn)');
INSERT INTO `translation_tags` VALUES (199,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Preuzimanje izvan radnog vremena (kn)');
INSERT INTO `translation_tags` VALUES (200,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Dostava vozila na adresu (kn)');
INSERT INTO `translation_tags` VALUES (201,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Dodatna napomena (kod preuzimanja)');
INSERT INTO `translation_tags` VALUES (202,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Dodatna napomena (kod vraćanja)');
INSERT INTO `translation_tags` VALUES (203,'2016-03-07 15:06:32','2016-03-07 15:06:32','','Ugovor napisao');
INSERT INTO `translation_tags` VALUES (204,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Registarska tablica');
INSERT INTO `translation_tags` VALUES (205,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Naziv vozila');
INSERT INTO `translation_tags` VALUES (206,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Marka vozila');
INSERT INTO `translation_tags` VALUES (207,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Model');
INSERT INTO `translation_tags` VALUES (208,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Tip');
INSERT INTO `translation_tags` VALUES (209,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Broj šasije');
INSERT INTO `translation_tags` VALUES (210,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Godina proizvodnje');
INSERT INTO `translation_tags` VALUES (211,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Prešao kilometara');
INSERT INTO `translation_tags` VALUES (212,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Redoviti servis svakih (km)');
INSERT INTO `translation_tags` VALUES (213,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Veličina guma');
INSERT INTO `translation_tags` VALUES (214,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Boja');
INSERT INTO `translation_tags` VALUES (215,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Leasing kuća');
INSERT INTO `translation_tags` VALUES (216,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Rata leasinga');
INSERT INTO `translation_tags` VALUES (217,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Registriran do');
INSERT INTO `translation_tags` VALUES (218,'2016-03-09 15:35:07','2016-03-09 15:35:07','','AO osiguranje vrijedi do');
INSERT INTO `translation_tags` VALUES (219,'2016-03-09 15:35:07','2016-03-09 15:35:07','','AO osg.kuća');
INSERT INTO `translation_tags` VALUES (220,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Kasko vrijedi do');
INSERT INTO `translation_tags` VALUES (221,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Kasko osg.kuća');
INSERT INTO `translation_tags` VALUES (222,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Periodički pregled vrijedi do');
INSERT INTO `translation_tags` VALUES (223,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Protupožarni aparat vrijedi do');
INSERT INTO `translation_tags` VALUES (224,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Prikaži ovo vozilo u rasporedu');
INSERT INTO `translation_tags` VALUES (225,'2016-03-09 15:35:07','2016-03-09 15:35:07','','Boja za prikaz');
INSERT INTO `translation_tags` VALUES (226,'2016-03-09 15:37:46','2016-03-09 15:37:46','','EDIT');
INSERT INTO `translation_tags` VALUES (227,'2016-03-09 15:37:46','2016-03-09 15:37:46','','Are you sure?');
INSERT INTO `translation_tags` VALUES (228,'2016-03-09 15:37:46','2016-03-09 15:37:46','','DELETE');
INSERT INTO `translation_tags` VALUES (229,'2016-03-09 15:37:54','2016-03-09 15:37:54','','Status servisa');
INSERT INTO `translation_tags` VALUES (230,'2016-03-09 15:37:54','2016-03-09 15:37:54','','Tip servisa');
INSERT INTO `translation_tags` VALUES (231,'2016-03-09 16:47:07','2016-03-09 16:47:07','','Usergroup');
INSERT INTO `translation_tags` VALUES (232,'2016-03-09 16:47:16','2016-03-09 16:47:16','','Title');
INSERT INTO `translation_tags` VALUES (233,'2016-03-09 16:47:16','2016-03-09 16:47:16','','Name');
INSERT INTO `translation_tags` VALUES (234,'2016-03-09 16:47:16','2016-03-09 16:47:16','','Permissions');
INSERT INTO `translation_tags` VALUES (235,'2016-03-10 19:50:25','2016-03-10 19:50:25','','Tax Rate');
INSERT INTO `translation_tags` VALUES (236,'2016-03-10 19:50:25','2016-03-10 19:50:25','','Address');
INSERT INTO `translation_tags` VALUES (237,'2016-03-10 19:50:25','2016-03-10 19:50:25','','Address 2');
INSERT INTO `translation_tags` VALUES (238,'2016-03-10 19:50:25','2016-03-10 19:50:25','','Zip');
INSERT INTO `translation_tags` VALUES (239,'2016-03-10 19:50:25','2016-03-10 19:50:25','','City');
INSERT INTO `translation_tags` VALUES (240,'2016-03-10 19:50:25','2016-03-10 19:50:25','','Country Id');
INSERT INTO `translation_tags` VALUES (241,'2016-03-10 19:50:25','2016-03-10 19:50:25','','Mobile');
INSERT INTO `translation_tags` VALUES (242,'2016-03-10 19:50:25','2016-03-10 19:50:25','','Phone');
INSERT INTO `translation_tags` VALUES (243,'2016-03-10 19:50:25','2016-03-10 19:50:25','','Contact Person');
INSERT INTO `translation_tags` VALUES (244,'2016-03-10 19:50:25','2016-03-10 19:50:25','','Contact Phone');
INSERT INTO `translation_tags` VALUES (245,'2016-03-10 19:50:25','2016-03-10 19:50:25','','Note');
INSERT INTO `translation_tags` VALUES (246,'2016-03-10 19:51:54','2016-03-10 19:51:54','','Početni datum servisa');
INSERT INTO `translation_tags` VALUES (247,'2016-03-10 19:51:54','2016-03-10 19:51:54','','Završni datum servisa');
INSERT INTO `translation_tags` VALUES (248,'2016-03-10 19:51:54','2016-03-10 19:51:54','','Servisni centar');
INSERT INTO `translation_tags` VALUES (249,'2016-03-10 19:51:54','2016-03-10 19:51:54','','Cijena servisa');
INSERT INTO `translation_tags` VALUES (250,'2016-03-10 19:51:54','2016-03-10 19:51:54','','Cijena dijelova');
INSERT INTO `translation_tags` VALUES (251,'2016-03-10 19:51:54','2016-03-10 19:51:54','','Odvesti na kilometara');
INSERT INTO `translation_tags` VALUES (252,'2016-03-14 18:09:15','2016-03-14 18:09:15','','First Name');
INSERT INTO `translation_tags` VALUES (253,'2016-03-14 18:09:15','2016-03-14 18:09:15','','Last Name');
INSERT INTO `translation_tags` VALUES (254,'2016-03-14 18:09:15','2016-03-14 18:09:15','','Last seen');
INSERT INTO `translation_tags` VALUES (255,'2016-03-14 18:09:15','2016-03-14 18:09:15','','Username');
INSERT INTO `translation_tags` VALUES (256,'2016-03-14 18:09:15','2016-03-14 18:09:15','','Password');
INSERT INTO `translation_tags` VALUES (257,'2016-03-14 18:09:15','2016-03-14 18:09:15','','Blocked');
DROP TABLE IF EXISTS `translations`;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `translation_tag_id` int(10) unsigned NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `translation` text COLLATE utf8_unicode_ci NOT NULL,
  `auto` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=258 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `translations` VALUES (1,'2016-03-05 07:51:38','2016-03-05 07:51:38',0,'en','',1);
INSERT INTO `translations` VALUES (2,'2016-03-05 07:51:38','2016-03-05 07:51:38',0,'en','',1);
INSERT INTO `translations` VALUES (3,'2016-03-05 07:51:38','2016-03-05 07:51:38',0,'en','',1);
INSERT INTO `translations` VALUES (4,'2016-03-05 07:51:38','2016-03-05 07:51:38',0,'en','',1);
INSERT INTO `translations` VALUES (5,'2016-03-05 07:51:38','2016-03-05 07:51:38',0,'en','',1);
INSERT INTO `translations` VALUES (6,'2016-03-05 07:51:38','2016-03-05 07:51:38',0,'en','',1);
INSERT INTO `translations` VALUES (7,'2016-03-05 07:51:38','2016-03-05 07:51:38',0,'en','',1);
INSERT INTO `translations` VALUES (8,'2016-03-05 07:51:38','2016-03-05 07:51:38',0,'en','',1);
INSERT INTO `translations` VALUES (9,'2016-03-05 07:51:38','2016-03-05 07:51:38',0,'en','',1);
INSERT INTO `translations` VALUES (10,'2016-03-05 07:51:38','2016-03-05 07:51:38',0,'en','',1);
INSERT INTO `translations` VALUES (11,'2016-03-05 07:51:38','2016-03-05 07:51:38',0,'en','',1);
INSERT INTO `translations` VALUES (12,'2016-03-05 07:51:38','2016-03-05 07:51:38',0,'en','',1);
INSERT INTO `translations` VALUES (13,'2016-03-05 07:51:49','2016-03-05 07:51:49',0,'en','',1);
INSERT INTO `translations` VALUES (14,'2016-03-05 07:51:49','2016-03-05 07:51:49',0,'en','',1);
INSERT INTO `translations` VALUES (15,'2016-03-05 07:51:49','2016-03-05 07:51:49',0,'en','',1);
INSERT INTO `translations` VALUES (16,'2016-03-05 07:51:49','2016-03-05 07:51:49',0,'en','',1);
INSERT INTO `translations` VALUES (17,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (18,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (19,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (20,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (21,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (22,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (23,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (24,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (25,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (26,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (27,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (28,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (29,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (30,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (31,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (32,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (33,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (34,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (35,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (36,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (37,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (38,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (39,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (40,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (41,'2016-03-05 07:51:52','2016-03-05 07:51:52',0,'en','',1);
INSERT INTO `translations` VALUES (42,'2016-03-05 07:52:25','2016-03-05 07:52:25',0,'en','',1);
INSERT INTO `translations` VALUES (43,'2016-03-05 07:52:25','2016-03-05 07:52:25',0,'en','',1);
INSERT INTO `translations` VALUES (44,'2016-03-05 07:52:25','2016-03-05 07:52:25',0,'en','',1);
INSERT INTO `translations` VALUES (45,'2016-03-05 07:52:25','2016-03-05 07:52:25',0,'en','',1);
INSERT INTO `translations` VALUES (46,'2016-03-05 07:52:25','2016-03-05 07:52:25',0,'en','',1);
INSERT INTO `translations` VALUES (47,'2016-03-05 07:52:25','2016-03-05 07:52:25',0,'en','',1);
INSERT INTO `translations` VALUES (48,'2016-03-05 07:52:25','2016-03-05 07:52:25',0,'en','',1);
INSERT INTO `translations` VALUES (49,'2016-03-05 07:52:25','2016-03-05 07:52:25',0,'en','',1);
INSERT INTO `translations` VALUES (50,'2016-03-05 07:52:25','2016-03-05 07:52:25',0,'en','',1);
INSERT INTO `translations` VALUES (51,'2016-03-05 07:52:25','2016-03-05 07:52:25',0,'en','',1);
INSERT INTO `translations` VALUES (52,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (53,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (54,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (55,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (56,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (57,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (58,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (59,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (60,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (61,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (62,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (63,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (64,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (65,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (66,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (67,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (68,'2016-03-05 07:52:27','2016-03-05 07:52:27',0,'en','',1);
INSERT INTO `translations` VALUES (69,'2016-03-05 07:52:49','2016-03-05 07:52:49',0,'en','',1);
INSERT INTO `translations` VALUES (70,'2016-03-05 07:52:49','2016-03-05 07:52:49',0,'en','',1);
INSERT INTO `translations` VALUES (71,'2016-03-05 07:52:49','2016-03-05 07:52:49',0,'en','',1);
INSERT INTO `translations` VALUES (72,'2016-03-05 07:52:49','2016-03-05 07:52:49',0,'en','',1);
INSERT INTO `translations` VALUES (73,'2016-03-05 07:52:49','2016-03-05 07:52:49',0,'en','',1);
INSERT INTO `translations` VALUES (74,'2016-03-05 07:52:49','2016-03-05 07:52:49',0,'en','',1);
INSERT INTO `translations` VALUES (75,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (76,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (77,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (78,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (79,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (80,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (81,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (82,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (83,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (84,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (85,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (86,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (87,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (88,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (89,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (90,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (91,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (92,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (93,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (94,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (95,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (96,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (97,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (98,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (99,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (100,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (101,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (102,'2016-03-05 07:52:51','2016-03-05 07:52:51',0,'en','',1);
INSERT INTO `translations` VALUES (103,'2016-03-05 08:31:21','2016-03-05 08:31:21',0,'en','',1);
INSERT INTO `translations` VALUES (104,'2016-03-05 08:31:21','2016-03-05 08:31:21',0,'en','',1);
INSERT INTO `translations` VALUES (105,'2016-03-05 08:31:21','2016-03-05 08:31:21',0,'en','',1);
INSERT INTO `translations` VALUES (106,'2016-03-05 08:31:21','2016-03-05 08:31:21',0,'en','',1);
INSERT INTO `translations` VALUES (107,'2016-03-05 08:31:21','2016-03-05 08:31:21',0,'en','',1);
INSERT INTO `translations` VALUES (108,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (109,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (110,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (111,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (112,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (113,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (114,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (115,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (116,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (117,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (118,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (119,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (120,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (121,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (122,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (123,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (124,'2016-03-05 08:31:24','2016-03-05 08:31:24',0,'en','',1);
INSERT INTO `translations` VALUES (125,'2016-03-07 06:43:04','2016-03-07 06:43:04',125,'hr','Logotip tvrtke za dokumente',1);
INSERT INTO `translations` VALUES (126,'2016-03-07 06:43:04','2016-03-07 06:43:04',126,'hr','Naziv tvrtke',1);
INSERT INTO `translations` VALUES (127,'2016-03-07 06:43:04','2016-03-07 06:43:04',127,'hr','OIB',1);
INSERT INTO `translations` VALUES (128,'2016-03-07 06:43:04','2016-03-07 06:43:04',128,'hr','Adresa sjedišta',1);
INSERT INTO `translations` VALUES (129,'2016-03-07 06:43:04','2016-03-07 06:43:04',129,'hr','Poštanski broj',1);
INSERT INTO `translations` VALUES (130,'2016-03-07 06:43:04','2016-03-07 06:43:04',130,'hr','Naziv mjesta',1);
INSERT INTO `translations` VALUES (131,'2016-03-07 06:43:04','2016-03-07 06:43:04',131,'hr','Država',1);
INSERT INTO `translations` VALUES (132,'2016-03-07 06:43:04','2016-03-07 06:43:04',132,'hr','Kontakt telefon',1);
INSERT INTO `translations` VALUES (133,'2016-03-07 06:43:04','2016-03-07 06:43:04',133,'hr','U sustavu PDV-a',1);
INSERT INTO `translations` VALUES (134,'2016-03-07 06:43:04','2016-03-07 06:43:04',134,'hr','Žiro račun',1);
INSERT INTO `translations` VALUES (135,'2016-03-07 06:43:04','2016-03-07 06:43:04',135,'hr','Kod banke',1);
INSERT INTO `translations` VALUES (136,'2016-03-07 06:43:04','2016-03-07 06:43:04',136,'hr','Save',1);
INSERT INTO `translations` VALUES (137,'2016-03-07 06:43:23','2016-03-07 06:43:23',137,'hr','Pretraga',1);
INSERT INTO `translations` VALUES (138,'2016-03-07 06:43:23','2016-03-07 06:43:23',138,'hr','Vozilo',1);
INSERT INTO `translations` VALUES (139,'2016-03-07 06:43:23','2016-03-07 06:43:23',139,'hr','Tip vozila',1);
INSERT INTO `translations` VALUES (140,'2016-03-07 06:43:23','2016-03-07 06:43:23',140,'hr','Status plaćanja',1);
INSERT INTO `translations` VALUES (141,'2016-03-07 06:43:23','2016-03-07 06:43:23',141,'hr','Od datuma',1);
INSERT INTO `translations` VALUES (142,'2016-03-07 06:43:23','2016-03-07 06:43:23',142,'hr','Do datuma',1);
INSERT INTO `translations` VALUES (143,'2016-03-07 06:43:23','2016-03-07 06:43:23',143,'hr','Prati datum',1);
INSERT INTO `translations` VALUES (144,'2016-03-07 06:43:23','2016-03-07 06:43:23',144,'hr','Vrsta usluge',1);
INSERT INTO `translations` VALUES (145,'2016-03-07 06:43:23','2016-03-07 06:43:23',145,'hr','Search',1);
INSERT INTO `translations` VALUES (146,'2016-03-07 06:43:23','2016-03-07 06:43:23',146,'hr','Reset filters',1);
INSERT INTO `translations` VALUES (147,'2016-03-07 06:43:26','2016-03-07 06:43:26',147,'hr','Početni datum',1);
INSERT INTO `translations` VALUES (148,'2016-03-07 06:43:26','2016-03-07 06:43:26',148,'hr','Početno vrijeme',1);
INSERT INTO `translations` VALUES (149,'2016-03-07 06:43:26','2016-03-07 06:43:26',149,'hr','Završni datum',1);
INSERT INTO `translations` VALUES (150,'2016-03-07 06:43:26','2016-03-07 06:43:26',150,'hr','Završno vrijeme',1);
INSERT INTO `translations` VALUES (151,'2016-03-07 06:43:26','2016-03-07 06:43:26',151,'hr','Vezani dokument',1);
INSERT INTO `translations` VALUES (152,'2016-03-07 06:43:26','2016-03-07 06:43:26',152,'hr','Klijent',1);
INSERT INTO `translations` VALUES (153,'2016-03-07 06:43:26','2016-03-07 06:43:26',153,'hr','Ukupna cijena',1);
INSERT INTO `translations` VALUES (154,'2016-03-07 06:43:26','2016-03-07 06:43:26',154,'hr','Uplaćeni iznos',1);
INSERT INTO `translations` VALUES (155,'2016-03-07 06:43:26','2016-03-07 06:43:26',155,'hr','Napomena',1);
INSERT INTO `translations` VALUES (156,'2016-03-07 06:43:26','2016-03-07 06:43:26',156,'hr','Početna kilometraža',1);
INSERT INTO `translations` VALUES (157,'2016-03-07 06:43:26','2016-03-07 06:43:26',157,'hr','Završna kilometraža',1);
INSERT INTO `translations` VALUES (158,'2016-03-07 06:43:26','2016-03-07 06:43:26',158,'hr','Dokumenti',1);
INSERT INTO `translations` VALUES (159,'2016-03-07 06:43:26','2016-03-07 06:43:26',159,'hr','Status najma',1);
INSERT INTO `translations` VALUES (160,'2016-03-07 06:43:52','2016-03-07 06:43:52',160,'hr','Per page',1);
INSERT INTO `translations` VALUES (161,'2016-03-07 06:43:55','2016-03-07 06:43:55',161,'hr','Naslov',1);
INSERT INTO `translations` VALUES (162,'2016-03-07 06:43:55','2016-03-07 06:43:55',162,'hr','Datum događaja',1);
INSERT INTO `translations` VALUES (163,'2016-03-07 06:43:55','2016-03-07 06:43:55',163,'hr','Mjesto događaja',1);
INSERT INTO `translations` VALUES (164,'2016-03-07 06:43:55','2016-03-07 06:43:55',164,'hr','Km na vozilu',1);
INSERT INTO `translations` VALUES (165,'2016-03-07 06:43:55','2016-03-07 06:43:55',165,'hr','Opis',1);
INSERT INTO `translations` VALUES (166,'2016-03-07 06:43:55','2016-03-07 06:43:55',166,'hr','Tko je oštetio vozilo',1);
INSERT INTO `translations` VALUES (167,'2016-03-07 06:43:55','2016-03-07 06:43:55',167,'hr','Status',1);
INSERT INTO `translations` VALUES (168,'2016-03-07 14:50:00','2016-03-07 14:50:00',168,'hr','Mjesto',1);
INSERT INTO `translations` VALUES (169,'2016-03-07 14:50:00','2016-03-07 14:50:00',169,'hr','Po stranici',1);
INSERT INTO `translations` VALUES (170,'2016-03-07 14:50:02','2016-03-07 14:50:02',170,'hr','Ime',1);
INSERT INTO `translations` VALUES (171,'2016-03-07 14:50:02','2016-03-07 14:50:02',171,'hr','Prezime',1);
INSERT INTO `translations` VALUES (172,'2016-03-07 14:50:02','2016-03-07 14:50:02',172,'hr','Tvrtka',1);
INSERT INTO `translations` VALUES (173,'2016-03-07 14:50:02','2016-03-07 14:50:02',173,'hr','Adresa',1);
INSERT INTO `translations` VALUES (174,'2016-03-07 14:50:02','2016-03-07 14:50:02',174,'hr','Tip kontakta',1);
INSERT INTO `translations` VALUES (175,'2016-03-07 14:50:02','2016-03-07 14:50:02',175,'hr','Email',1);
INSERT INTO `translations` VALUES (176,'2016-03-07 14:50:02','2016-03-07 14:50:02',176,'hr','Mobilni tel.',1);
INSERT INTO `translations` VALUES (177,'2016-03-07 14:50:02','2016-03-07 14:50:02',177,'hr','Tel.',1);
INSERT INTO `translations` VALUES (178,'2016-03-07 14:50:02','2016-03-07 14:50:02',178,'hr','Fax',1);
INSERT INTO `translations` VALUES (179,'2016-03-07 14:50:02','2016-03-07 14:50:02',179,'hr','Web',1);
INSERT INTO `translations` VALUES (180,'2016-03-07 14:50:02','2016-03-07 14:50:02',180,'hr','Mobitel',1);
INSERT INTO `translations` VALUES (181,'2016-03-07 15:06:32','2016-03-07 15:06:32',181,'hr','Datum ugovora',1);
INSERT INTO `translations` VALUES (182,'2016-03-07 15:06:32','2016-03-07 15:06:32',182,'hr','Ime ili naziv Najmoprimca',1);
INSERT INTO `translations` VALUES (183,'2016-03-07 15:06:32','2016-03-07 15:06:32',183,'hr','Tip i registracija vozila',1);
INSERT INTO `translations` VALUES (184,'2016-03-07 15:06:32','2016-03-07 15:06:32',184,'hr','Datum preuzimanja',1);
INSERT INTO `translations` VALUES (185,'2016-03-07 15:06:32','2016-03-07 15:06:32',185,'hr','Vrijeme preuzimanja',1);
INSERT INTO `translations` VALUES (186,'2016-03-07 15:06:32','2016-03-07 15:06:32',186,'hr','Datum povratka',1);
INSERT INTO `translations` VALUES (187,'2016-03-07 15:06:32','2016-03-07 15:06:32',187,'hr','Vrijeme povratka',1);
INSERT INTO `translations` VALUES (188,'2016-03-07 15:06:32','2016-03-07 15:06:32',188,'hr','Cijena po danu (bez PDV-a)',1);
INSERT INTO `translations` VALUES (189,'2016-03-07 15:06:32','2016-03-07 15:06:32',189,'hr','Ukupno dana',1);
INSERT INTO `translations` VALUES (190,'2016-03-07 15:06:32','2016-03-07 15:06:32',190,'hr','Dnevna kilometraža (km)',1);
INSERT INTO `translations` VALUES (191,'2016-03-07 15:06:32','2016-03-07 15:06:32',191,'hr','Popust (%)',1);
INSERT INTO `translations` VALUES (192,'2016-03-07 15:06:32','2016-03-07 15:06:32',192,'hr','Vozač',1);
INSERT INTO `translations` VALUES (193,'2016-03-07 15:06:32','2016-03-07 15:06:32',193,'hr','Adresa vozača',1);
INSERT INTO `translations` VALUES (194,'2016-03-07 15:06:32','2016-03-07 15:06:32',194,'hr','Zemlja vozača',1);
INSERT INTO `translations` VALUES (195,'2016-03-07 15:06:32','2016-03-07 15:06:32',195,'hr','Broj vozačke',1);
INSERT INTO `translations` VALUES (196,'2016-03-07 15:06:32','2016-03-07 15:06:32',196,'hr','Broj telefona vozača',1);
INSERT INTO `translations` VALUES (197,'2016-03-07 15:06:32','2016-03-07 15:06:32',197,'hr','Dodatni kilometri (km)',1);
INSERT INTO `translations` VALUES (198,'2016-03-07 15:06:32','2016-03-07 15:06:32',198,'hr','Dodatni sat najma (kn)',1);
INSERT INTO `translations` VALUES (199,'2016-03-07 15:06:32','2016-03-07 15:06:32',199,'hr','Preuzimanje izvan radnog vremena (kn)',1);
INSERT INTO `translations` VALUES (200,'2016-03-07 15:06:32','2016-03-07 15:06:32',200,'hr','Dostava vozila na adresu (kn)',1);
INSERT INTO `translations` VALUES (201,'2016-03-07 15:06:32','2016-03-07 15:06:32',201,'hr','Dodatna napomena (kod preuzimanja)',1);
INSERT INTO `translations` VALUES (202,'2016-03-07 15:06:32','2016-03-07 15:06:32',202,'hr','Dodatna napomena (kod vraćanja)',1);
INSERT INTO `translations` VALUES (203,'2016-03-07 15:06:32','2016-03-07 15:06:32',203,'hr','Ugovor napisao',1);
INSERT INTO `translations` VALUES (204,'2016-03-09 15:35:07','2016-03-09 15:35:07',204,'hr','Registarska tablica',1);
INSERT INTO `translations` VALUES (205,'2016-03-09 15:35:07','2016-03-09 15:35:07',205,'hr','Naziv vozila',1);
INSERT INTO `translations` VALUES (206,'2016-03-09 15:35:07','2016-03-09 15:35:07',206,'hr','Marka vozila',1);
INSERT INTO `translations` VALUES (207,'2016-03-09 15:35:07','2016-03-09 15:35:07',207,'hr','Model',1);
INSERT INTO `translations` VALUES (208,'2016-03-09 15:35:07','2016-03-09 15:35:07',208,'hr','Tip',1);
INSERT INTO `translations` VALUES (209,'2016-03-09 15:35:07','2016-03-09 15:35:07',209,'hr','Broj šasije',1);
INSERT INTO `translations` VALUES (210,'2016-03-09 15:35:07','2016-03-09 15:35:07',210,'hr','Godina proizvodnje',1);
INSERT INTO `translations` VALUES (211,'2016-03-09 15:35:07','2016-03-09 15:35:07',211,'hr','Prešao kilometara',1);
INSERT INTO `translations` VALUES (212,'2016-03-09 15:35:07','2016-03-09 15:35:07',212,'hr','Redoviti servis svakih (km)',1);
INSERT INTO `translations` VALUES (213,'2016-03-09 15:35:07','2016-03-09 15:35:07',213,'hr','Veličina guma',1);
INSERT INTO `translations` VALUES (214,'2016-03-09 15:35:07','2016-03-09 15:35:07',214,'hr','Boja',1);
INSERT INTO `translations` VALUES (215,'2016-03-09 15:35:07','2016-03-09 15:35:07',215,'hr','Leasing kuća',1);
INSERT INTO `translations` VALUES (216,'2016-03-09 15:35:07','2016-03-09 15:35:07',216,'hr','Rata leasinga',1);
INSERT INTO `translations` VALUES (217,'2016-03-09 15:35:07','2016-03-09 15:35:07',217,'hr','Registriran do',1);
INSERT INTO `translations` VALUES (218,'2016-03-09 15:35:07','2016-03-09 15:35:07',218,'hr','AO osiguranje vrijedi do',1);
INSERT INTO `translations` VALUES (219,'2016-03-09 15:35:07','2016-03-09 15:35:07',219,'hr','AO osg.kuća',1);
INSERT INTO `translations` VALUES (220,'2016-03-09 15:35:07','2016-03-09 15:35:07',220,'hr','Kasko vrijedi do',1);
INSERT INTO `translations` VALUES (221,'2016-03-09 15:35:07','2016-03-09 15:35:07',221,'hr','Kasko osg.kuća',1);
INSERT INTO `translations` VALUES (222,'2016-03-09 15:35:07','2016-03-09 15:35:07',222,'hr','Periodički pregled vrijedi do',1);
INSERT INTO `translations` VALUES (223,'2016-03-09 15:35:07','2016-03-09 15:35:07',223,'hr','Protupožarni aparat vrijedi do',1);
INSERT INTO `translations` VALUES (224,'2016-03-09 15:35:07','2016-03-09 15:35:07',224,'hr','Prikaži ovo vozilo u rasporedu',1);
INSERT INTO `translations` VALUES (225,'2016-03-09 15:35:07','2016-03-09 15:35:07',225,'hr','Boja za prikaz',1);
INSERT INTO `translations` VALUES (226,'2016-03-09 15:37:46','2016-03-09 15:37:46',226,'hr','EDIT',1);
INSERT INTO `translations` VALUES (227,'2016-03-09 15:37:46','2016-03-09 15:37:46',227,'hr','Are you sure?',1);
INSERT INTO `translations` VALUES (228,'2016-03-09 15:37:46','2016-03-09 15:37:46',228,'hr','DELETE',1);
INSERT INTO `translations` VALUES (229,'2016-03-09 15:37:54','2016-03-09 15:37:54',229,'hr','Status servisa',1);
INSERT INTO `translations` VALUES (230,'2016-03-09 15:37:54','2016-03-09 15:37:54',230,'hr','Tip servisa',1);
INSERT INTO `translations` VALUES (231,'2016-03-09 16:47:07','2016-03-09 16:47:07',231,'hr','Usergroup',1);
INSERT INTO `translations` VALUES (232,'2016-03-09 16:47:16','2016-03-09 16:47:16',232,'hr','Title',1);
INSERT INTO `translations` VALUES (233,'2016-03-09 16:47:16','2016-03-09 16:47:16',233,'hr','Name',1);
INSERT INTO `translations` VALUES (234,'2016-03-09 16:47:16','2016-03-09 16:47:16',234,'hr','Permissions',1);
INSERT INTO `translations` VALUES (235,'2016-03-10 19:50:25','2016-03-10 19:50:25',235,'hr','Tax Rate',1);
INSERT INTO `translations` VALUES (236,'2016-03-10 19:50:25','2016-03-10 19:50:25',236,'hr','Address',1);
INSERT INTO `translations` VALUES (237,'2016-03-10 19:50:25','2016-03-10 19:50:25',237,'hr','Address 2',1);
INSERT INTO `translations` VALUES (238,'2016-03-10 19:50:25','2016-03-10 19:50:25',238,'hr','Zip',1);
INSERT INTO `translations` VALUES (239,'2016-03-10 19:50:25','2016-03-10 19:50:25',239,'hr','City',1);
INSERT INTO `translations` VALUES (240,'2016-03-10 19:50:25','2016-03-10 19:50:25',240,'hr','Country Id',1);
INSERT INTO `translations` VALUES (241,'2016-03-10 19:50:25','2016-03-10 19:50:25',241,'hr','Mobile',1);
INSERT INTO `translations` VALUES (242,'2016-03-10 19:50:25','2016-03-10 19:50:25',242,'hr','Phone',1);
INSERT INTO `translations` VALUES (243,'2016-03-10 19:50:25','2016-03-10 19:50:25',243,'hr','Contact Person',1);
INSERT INTO `translations` VALUES (244,'2016-03-10 19:50:25','2016-03-10 19:50:25',244,'hr','Contact Phone',1);
INSERT INTO `translations` VALUES (245,'2016-03-10 19:50:25','2016-03-10 19:50:25',245,'hr','Note',1);
INSERT INTO `translations` VALUES (246,'2016-03-10 19:51:54','2016-03-10 19:51:54',246,'hr','Početni datum servisa',1);
INSERT INTO `translations` VALUES (247,'2016-03-10 19:51:54','2016-03-10 19:51:54',247,'hr','Završni datum servisa',1);
INSERT INTO `translations` VALUES (248,'2016-03-10 19:51:54','2016-03-10 19:51:54',248,'hr','Servisni centar',1);
INSERT INTO `translations` VALUES (249,'2016-03-10 19:51:54','2016-03-10 19:51:54',249,'hr','Cijena servisa',1);
INSERT INTO `translations` VALUES (250,'2016-03-10 19:51:54','2016-03-10 19:51:54',250,'hr','Cijena dijelova',1);
INSERT INTO `translations` VALUES (251,'2016-03-10 19:51:54','2016-03-10 19:51:54',251,'hr','Odvesti na kilometara',1);
INSERT INTO `translations` VALUES (252,'2016-03-14 18:09:15','2016-03-14 18:09:15',252,'hr','First Name',1);
INSERT INTO `translations` VALUES (253,'2016-03-14 18:09:15','2016-03-14 18:09:15',253,'hr','Last Name',1);
INSERT INTO `translations` VALUES (254,'2016-03-14 18:09:15','2016-03-14 18:09:15',254,'hr','Last seen',1);
INSERT INTO `translations` VALUES (255,'2016-03-14 18:09:15','2016-03-14 18:09:15',255,'hr','Username',1);
INSERT INTO `translations` VALUES (256,'2016-03-14 18:09:15','2016-03-14 18:09:15',256,'hr','Password',1);
INSERT INTO `translations` VALUES (257,'2016-03-14 18:09:15','2016-03-14 18:09:15',257,'hr','Blocked',1);
DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE `user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user_groups` VALUES (1,'2016-03-02 12:10:06','2016-03-09 16:47:20','Administrator','admin','access.admin,admin,menu.user,user.create,user.switchto,user.ownpassword,manage.usergroups,menu.dashboard,menu.calendar,menu.booking,menu.contact,menu.vehicle,menu.vehicle-service,menu.damage,menu.checklist,menu.company');
INSERT INTO `user_groups` VALUES (2,'2016-03-02 12:10:06','2016-03-02 12:10:06','Account owner','owner','access.admin,user.ownpassword,menu.dashboard,menu.calendar,menu.booking,menu.contact,menu.vehicle,menu.vehicle-service,menu.damage,menu.checklist');
INSERT INTO `user_groups` VALUES (3,'2016-03-02 12:10:06','2016-03-02 12:10:06','Account user','user','access.admin,user.ownpassword,menu.dashboard,menu.booking,menu.contact,menu.vehicle,menu.checklist');
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `usergroup` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `blocked` tinyint(3) unsigned DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `users` VALUES (1,'2016-03-02 12:10:06','2016-03-16 11:13:12','tihomir.jauk@gmail.com','admin','$2y$10$JV6DhWCN8u.NVFcfBOgb0uYFo9jYWhbiQP5WRGkVj/e/ITb/VtGF.','admin','Administrator','','',0,'2016-03-16 11:13:12',1);
INSERT INTO `users` VALUES (2,'2016-03-05 07:51:26','2016-03-10 08:58:41','igor.cukac@gmail.com','igor.cukac@gmail.com','$2y$10$99Zn5axDZtVi5GuxSo4iWOskuxNoAIjfqYEdqQEvuSNhmY6CoDcB2','owner','TIHI KOD d.o.o.','','',NULL,'2016-03-10 08:58:41',2);
INSERT INTO `users` VALUES (3,'2016-03-10 09:01:45','2016-03-16 11:29:59','demo','demo','$2y$10$vJXXJoG7eP8NJeu1RIW1C.XMDS3UIMM4IXhOrFE2xpjUPFckS2rWC','owner','Demos d.o.o.','','',NULL,'2016-03-16 11:29:59',3);
DROP TABLE IF EXISTS `vehicle_kms`;
CREATE TABLE `vehicle_kms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `vehicle_id` int(10) unsigned NOT NULL,
  `km` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `vehicle_kms` VALUES (1,'2016-03-16 09:40:55','2016-03-16 09:40:55',3,2,1234);
INSERT INTO `vehicle_kms` VALUES (2,'2016-03-16 09:45:44','2016-03-16 09:45:44',3,2,1680);
INSERT INTO `vehicle_kms` VALUES (3,'2016-03-16 10:15:29','2016-03-16 10:15:29',3,2,5000);
INSERT INTO `vehicle_kms` VALUES (4,'2016-03-16 10:31:20','2016-03-16 10:31:20',3,4,53000);
INSERT INTO `vehicle_kms` VALUES (5,'2016-03-16 11:24:22','2016-03-16 11:24:22',3,4,60000);
INSERT INTO `vehicle_kms` VALUES (6,'2016-03-16 11:25:00','2016-03-16 11:25:00',3,3,45000);
DROP TABLE IF EXISTS `vehicle_services`;
CREATE TABLE `vehicle_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `vehicle_km` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Redovan',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `from` date DEFAULT NULL,
  `to` date DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `service_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parts_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `service_status` tinyint(4) NOT NULL DEFAULT '1',
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `vehicle_services` VALUES (1,'2016-03-16 09:53:02','2016-03-16 10:21:10',3,2,'','Redovan','1','2016-03-10','2016-03-16',5,'1000','',3,'','');
INSERT INTO `vehicle_services` VALUES (2,'2016-03-16 10:21:41','2016-03-16 10:21:41',3,3,'50000','Redovan','0',NULL,NULL,5,'','',1,'','');
INSERT INTO `vehicle_services` VALUES (3,'2016-03-16 10:32:18','2016-03-16 10:32:18',3,4,'','Izvanredni','1','2016-03-17','2016-03-20',5,'1600','350',3,'','');
DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `brand` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `licence_plate` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `chassis_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `partner` tinyint(4) NOT NULL DEFAULT '0',
  `year` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tire_size` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'White',
  `leasing` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `leasing_amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `licence_expires` date DEFAULT NULL,
  `pp_expires` date DEFAULT NULL,
  `ppa_expires` date DEFAULT NULL,
  `ao_expires` date DEFAULT NULL,
  `ao_house` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `kasko_expires` date DEFAULT NULL,
  `kasko_house` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `km` int(10) unsigned NOT NULL DEFAULT '0',
  `service_km` int(10) unsigned NOT NULL DEFAULT '50000',
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `show_on_calendar` int(11) NOT NULL DEFAULT '1',
  `color_hex` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#FFFFFF',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `vehicles` VALUES (1,'2016-03-10 08:58:13','2016-03-10 08:58:13',1,'','','','test','','',0,'1990','','','','',NULL,NULL,NULL,NULL,'',NULL,'',0,0,'','',1,'');
INSERT INTO `vehicles` VALUES (2,'2016-03-16 09:40:25','2016-03-16 11:25:24',3,'Renault Master ZG-1234-SD','Renault','Master','ZG-1234-SD','PDG4234564GGHHA234','Teretni',0,'2013','196/65/R15','bijela','OTP Leasing','1750','2016-07-15','2016-08-20','2016-05-31','2016-05-01','Euroherc osiguranje','2016-04-01','Allianz osiguranje',5000,40000,'6','Svi podatci vozila sa skeniranim \r\ndokumentima, leasing ugovorima,\r\nosiguranjima.',1,'#1d3de5');
INSERT INTO `vehicles` VALUES (3,'2016-03-16 10:09:37','2016-03-16 11:25:00',3,'Opel Corsa ZG-007-JB','Opel','Corsa','ZG-007-JB','HJO2342342342','Putnički',0,'2007','195/65/R16','crvena','OTP','1750','2016-04-08','2016-07-23','2016-03-25','2016-05-12','Euroherc osiguranje','2016-06-18','Allianz osiguranje',45000,40000,'5','Svi podatci vozila sa skeniranim \r\ndokumentima, leasing ugovorima,\r\nosiguranjima.',1,'#ea0e0e');
INSERT INTO `vehicles` VALUES (4,'2016-03-16 10:26:10','2016-03-16 11:24:22',3,'VW Golf 6 DA-838-SZ','VW','Golf 6','DA-838-SZ','VV456977WGW4125','Putnički',0,'2012','215/70/R16','bijela','OTP Leasing','2100','2015-11-05','2016-05-21','2016-04-08','2015-10-07','Euroherc osiguranje','2016-04-17','Allianz osiguranje',60000,50000,'4','Svi podatci vozila sa skeniranim \r\ndokumentima, leasing ugovorima,\r\nosiguranjima.',1,'#1907ea');

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
