<?php

return array(

    // rent app
    'menu.dashboard'		=> "Show Dashboard page",
    'menu.calendar'			=> "Show Calendar page",
    'menu.booking'			=> "Show Bookings page",
    'menu.contact'			=> "Show Contacts page",
    'menu.vehicle'			=> "Show Vehicles page",
    'menu.vehicle-service'	=> "Show Vehicle Services page",
    'menu.damage'			=> "Show Vehicle Damages page",
    'menu.checklist'		=> "Show Vehicle Checklists page",
    'menu.company'		=> "Show Companies page",

);
