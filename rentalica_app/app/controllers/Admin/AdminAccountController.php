<?php
class AdminAccountController extends AdminController
{
    public $baseurl = '/admin/account';

    function index()
    {
        return $this->company();
    }

    function company()
    {
        
        Doc::title('Postavke');

        $company = Company::find(Auth::user()->company_id);

        if( Request::post() ){
            $company->fill(Request::post());
            $company->save();
            return redirect($this->baseurl)->msg("Postavke su snimljene");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($company) ){
            $form->fill($company);
        }
        $form->open('settings');

        $form->label('Logotip tvrtke za dokumente')->photos('logo');
        $form->label('Naziv tvrtke')->input('name')->addClass('form-control')->placeholder('Naziv tvrtke');
        $form->label('OIB')->input('oib')->addClass('form-control')->placeholder('OIB');
        $form->label('Adresa sjedišta')->input('address')->addClass('form-control')->placeholder('Adresa sjedišta');
        $form->label('Poštanski broj')->input('zip')->addClass('form-control')->placeholder('Poštanski broj mjesta');
        $form->label('Naziv mjesta')->input('city')->addClass('form-control')->placeholder('Naziv mjesta');
        $form->label('Država')->select('country_id')->options(Country::options())->addClass('form-control')->value(52);
        $form->label('Kontakt telefon')->input('phone')->addClass('form-control')->placeholder('Upišite službeni kontakt telefon');

        $form->label('U sustavu PDV-a')->radioList('tax_rate')->options(['1.25'=>"Da (25%)",'1.0'=>"Ne (0%)"])->addClass('form-control');
        $form->label('Žiro račun')->input('ziro')->addClass('form-control')->placeholder('Upišite žiro račun');
        $form->label('Kod banke')->input('bank')->addClass('form-control')->placeholder('Upišite banku');

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }


}
