<?php
class AdminBookingController extends AdminController
{
    public $model = 'Booking';
    public $baseurl = '/admin/booking';

    function index()
    {
        $model = $this->model;
        
        Doc::title("Pregled najmova");

        $items = Booking::with('Vehicle')->select(DB::raw('
            bookings.*,
            contacts.first_name as clients_first_name,
            contacts.last_name as clients_last_name,
            contacts.company as clients_company,
            vehicles.name as vehicle_name,
            vehicles.type as vehicle_type,
            ABS(DATEDIFF(bookings.from, bookings.to)) as days
        '))
        ->leftJoin('contacts','contacts.id','=','bookings.client_id')
        ->leftJoin('vehicles','vehicles.id','=','bookings.vehicle_id');

        $buttons = Forms::make();
        $buttons->btnAdd('Novi Najam, Prijevoz ili Usluga',$this->baseurl.'/edit');

        $filter = new SearchFilter();
        $filter->perpage = 1000;
        $filter->page = 1;
        $filter->look = 'from';

        $paid_statuses = Booking::paid_options();

        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'from,to,bookings.note,contacts.first_name,contacts.last_name,contacts.company,vehicles.name' );
        }
        // search vehicle
        if( $filter->notEmpty('vehicle_id') ){
            $items = $items->whereVehicleId( $filter->vehicle_id );
        }
        // search paid status
        if( $filter->notEmpty('paid_status') ){
            if( in_array($filter->paid_status, array_keys($paid_statuses) ) ){
                $items = $items->wherePaid( $filter->paid_status );
            }
        }
        // filter partners (Najmodavac)
        /*
        if( $filter->notEmpty('partner') ){
            $items = $items->where( 'bookings.partner', '=', $filter->partner );
        }
        */

        // filter period
        if( in_array($filter->look,['from','to']) ){
            // filter period from
            if( $filter->notEmpty('from') ){
                $items = $items->where( DB::raw('DATE(`'.$filter->look.'`)'),'>=', $filter->from );
            }
            // filter period to
            if( $filter->notEmpty('to') ){
                $items = $items->where( DB::raw('DATE(`'.$filter->look.'`)'),'<=', $filter->to );
            }
        }

        // filter vehicle_type (Vrsta vozila)
        $vehicle_types = [''=>'---'];
        foreach(Vehicle::types() as $key=>$val){
            $vehicle_types[$key]=$val;
        }
        if( $filter->notEmpty('vehicle_type') or $filter->vehicle_type=="0" ){
            $items = $items->where( 'vehicles.type', '=', $vehicle_types[$filter->vehicle_type] );
        }

        // filter service_type (Vrsta usluge)
        if( $filter->notEmpty('service_type') ){
            $items = $items->where( 'bookings.service_type', '=', $filter->service_type );
        }

        

        $paid_options = [''=>'---'];
        foreach($paid_statuses as $key=>$val){
            $paid_options[$key]=$val;
        }
        $paid_label_colors = [
            ''=>'label-primary',
            '1'=>'label-danger',
            '10'=>'label-warning',
            '20'=>'label-success',
            '99'=>'label-primary'
        ];

        $statuses = Booking::status_options();
        $status_colors = [
            0=>'label-danger',
            1=>'label-warning',
            2=>'label-success'
        ];

        $service_types = [''=>"Sve"];
        foreach(Booking::service_type_options() as $key=>$val){
            $service_types[$key] = $val;
        }



        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Pretraga')->search('keywords')->klass('form-control pull-left')->placeholder('po ključnim riječima');
        $form->label('Vozilo')->select('vehicle_id')->options(Vehicle::options());
        $form->label('Tip vozila')->select('vehicle_type')->options($vehicle_types);
        //$form->label('Najmodavac')->select('partner')->options([''=>'---','1'=>"Komet",'2'=>"Flexirent"]);
        $form->label('Status plaćanja')->select('paid_status')->options($paid_options)->klass('form-control');
        $form->label('Od datuma')->datePicker('from');
        $form->label('Do datuma')->datePicker('to');
        $form->label('Prati datum')->select('look')->options(['from'=>"Početni",'to'=>"Završni"]);
        $form->label('Vrsta usluge')->select('service_type')->options($service_types)->klass('form-control');
        //$form->label('Per page')->select('perpage')->values([10,20,50,100,250,500,1000])->klass('form-control');

        $items = $items->orderBy('from','ASC');
        $items = $filter->paginate( $items )->get();

        $table = Table::make()
                ->headings([
                    'Termin',
                    'Vozilo',
                    'Vezani dok.',
                    'Klijent',
                    'Cijena',
                    'Plaćanje',
                    'Napomena',
                    //'Br.dana',
                    //'Cijena/Dan',
                    'Akcije'
                ]);

        $total_price = 0.0;
        $total_days = 0;

        $total_km = 0;
        $total_km_days = 0;

        foreach($items as $item){
            $data = [];
            $data[] = UI::dateTime($item->from).'<br>'.UI::dateTime($item->to);
            
            $title = $item->vehicle->title;//.'<br><span class="label '.$status_colors[$item->status].'">'.strtoupper($statuses[$item->status]).'</span>';
            $km_note = '';
            if( !empty($item->km_start) and !empty($item->km_end) ){
                $km_diff = abs($item->km_start-$item->km_end);
                $km_note ='<br><span style="font-size:13px;color:#777;">Ukupno:&nbsp;'.$km_diff.'&nbsp;km&nbsp;</span>';
                if( $km_diff>0 and (int)$item->days>0 ){
                    $total_km+=$km_diff;
                    $total_km_days+=(int)$item->days;
                    $km_note.='&nbsp;<span style="font-size:13px;color:#777;">(&nbsp;'.round($km_diff/(int)$item->days).' km/dan )</span>';
                }
            }
            // ako je i završio, prikaži po danu?
            $data[] = $title;

            $price = $item->price;
            if( $item->paid==99 ){
                $price = 0.0;
            }

            if( !empty($item->related_doc) ){
                $pt = '<span style="font-size:13px;color:#444;">'.$item->related_doc.'</span>';
            }else{
                $pt = '-';
            }
            $data[] = $pt;
            
            $data[] = $item->client->title;

            $total_price += $price;
            if( (int)$item->days>0 ){
                $price_per_day = $item->price/(int)$item->days;
            }else{
                $orice_per_day = 0;
            }
            if( $item->service_type==10 ){
                $data[] = '<span style="text-align:right;font-weight:bold;">'.Num::price($item->price).'</span><br><span style="font-size:13px;color:#777;">('.(int)$item->days.'&nbsp;dana&nbsp;x&nbsp;'.Num::price($price_per_day).')</span>';
                
            }else{
                $data[] = '<span style="text-align:right;font-weight:bold;">'.Num::price($item->price).'</span>';
            }

            //$data[] = $item->km_start.'-'.$item->km_end;
            $show_amount = '';
            if( $item->paid==10 ){
                $show_amount = '<br><span style="font-size:13px;color:#777;">Uplaćeno:&nbsp;'.Num::price($item->partial_amount).'</span>';
            }
            $data[] = '<span class="label '.$paid_label_colors[$item->paid].'">'.strtoupper($paid_statuses[$item->paid]).'</span>'.$show_amount;
            
            $data[] = $service_types[$item->service_type].' '.$statuses[$item->status].$km_note.'<br>'.$item->note;//.

            //$data[] = $item->days;
            // Zbroji dane samo ako je "Najam" i nije "Bez naplate"
            if( $item->paid!==99 ){// and $item->service_type==10
                $total_days += (int)$item->days;
            }

            //$data[] = Num::price($item->price/$item->days);
            
            // cache this or add With
            $actions = UI::btnEdit($item->id,'booking');
            $actions.= UI::btnDelete($item->id,'booking');
            $data[] = $actions;
            $table->addRow($data);
        }

        if( $total_days>0 ){
            $total_price_per_days = $total_price/$total_days;
        }else{
            $total_price_per_days = 0;
        }

        if( $total_km_days>0 ){
            $total_km_per_days = $total_km/$total_km_days;
        }else{
            $total_km_per_days = 0;
        }

        
        $data = ['&nbsp;','&nbsp;','&nbsp;','&nbsp;'];
        $data[] = b(Num::price($total_price)).'<br><span style="font-size:13px;color:#777;">('.$total_days.' dana x '.Num::price($total_price_per_days).')</span>';
        $data[] = '&nbsp;';
        //$data[] = '<span style="font-size:13px;color:#777;"><b>Ukupno: '.$total_km.' km</b> <br> ( '.round($total_km_per_days).' km/dan)</span>';
        $data[] = '<span style="font-size:13px;color:#777;"><b>Prosjek: '.round($total_km_per_days).' km/dan</b></span>';
        //Ukupno: 406 km  ( 406 km/dan );
        /*
        $data[] = b($total_days);
        if( $total_days==0 ){
            $data[] = b(0);
        }else{
            $data[] = b(Num::price($total_price/$total_days));;
        }
        */
        $data[] = '&nbsp;';
        $table->addRow($data);

        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title("Unos najma");

        $item = $model::firstOrNew(['id'=>$id]);

        if( empty($item->from_time) ){ jsready('$("#from_time").val("08:00");'); }
        if( empty($item->to_time) ){ jsready('$("#to_time").val("08:00");'); }
        if( empty($item->status) ){ jsready('$("#status-0").attr("checked","checked");'); }

        $data = Request::post();
        if( $data ){
            $item->fill($data);
            $item->from_date = $data['from_date'];
            $item->from_time = $data['from_time'];
            $item->to_date = $data['to_date'];
            $item->to_time = $data['to_time'];
            if( empty($item->service_type) ){
                $item->service_type = 10;
            }
            if( $item->client_id<1 ){
                $item->client_id=0;
            }
            $item->save();

            if( isset($data['btnContract']) ){
                return redirect('admin/contract/edit/?booking_id='.$item->id);                
            }
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }else{
            $data = Request::get();
            if( !empty($data) ){
                $item->fill($data);
            }
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('booking');


        //$form->label('Booked By')->hidden('booked_by');
        $form->label('Vozilo')->select('vehicle_id')->options(Vehicle::options());
        $form->label('Početni datum')->datePicker('from_date');
        $form->label('Početno vrijeme')->timePicker('from_time');
        $form->label('Završni datum')->datePicker('to_date');
        $form->label('Završno vrijeme')->timePicker('to_time');
        //$form->label('Preko koje firme')->radioGroup('partner')->options(['1'=>"Komet",'2'=>"Flexirent"]);
        $form->label('Vrsta usluge')->radioGroup('service_type')->options(Booking::service_type_options());
        $form->label('Vezani dokument')->input('related_doc');
        $form->label('Klijent')->select2('client_id')->options(Contact::options());
        
        $form->label('Ukupna cijena')->price('price');
        $form->label('Status plaćanja')->radioGroup('paid')->options(Booking::paid_options());
        $form->div('block-paid-amount');
            $form->label('Uplaćeni iznos')->price('partial_amount');
        $form->closeDiv();
        jsready("
            function displayPaidAmount(){
                var paid = $('#paid-10:checked').val();
                console.log('paid:'+paid);
                if( paid ){
                    $('#block-paid-amount').show();
                }else{
                    $('#block-paid-amount').hide();
                }
            }
            displayPaidAmount();
            $('body').on('click',function(){
                displayPaidAmount();
            });
            $('#from_date').on('change',function(){
                if( $('#to_date').val()=='' ){
                    $('#to_date').val($('#from_date').val());
                }
            });
        ");

        $form->label('Napomena')->text('note');
        $form->label('Početna kilometraža')->number('km_start');
        $form->label('Završna kilometraža')->number('km_end');
        $form->label('Dokumenti')->documents('docs');

        $form->label('Status najma')->radioGroup('status')->options(Booking::status_options());

        $form->btnSave();
        $form->submit('Zapamti i otvori novi ugovor')->name('btnContract')->addClass('btn btn-primary col-md-4 col-sm-6 col-xs-12 ')->removeClass('form-control')->style("float:right;");

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }



    function modal($id=0)
    {
        Doc::view('admin/modal/default');
        
        $model = $this->model;
        
        Doc::title("Unos najma");

        if( $id>0 ){
            $item = $model::firstOrNew(['id'=>$id]);
        }else{
            $item = new $model();
        }

        $data = Request::post();

        if( $data['_method']=='POST' ){
            if( empty($id) ){
                $item = new $model;
            }
            $item->fill($data);
            $item->from_date = $data['from_date'];
            $item->from_time = $data['from_time'];
            $item->to_date = $data['to_date'];
            $item->to_time = $data['to_time'];
            if( empty($item->service_type) ){
                $item->service_type = 10;
            }
            $item->save();
            if( isset($data['btnNext']) ){
                dd($data);
            }
            return $item;
        }else{
            $data = Request::get();
            if( !empty($data) ){
                $item->fill($data);
            }
        }


        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('modal-booking-form')
                ->ajax('/admin/booking/modal/'.$item->id)
                ->onAjaxDone('$("#calendar").fullCalendar("refetchEvents");');

        $form->hidden('_method')->value('POST');
        $form->label('Vozilo')->select('vehicle_id')->options(Vehicle::options())->style("width:100%");
        $form->label('Početni datum')->datePicker('from_date');
        $form->label('Početno vrijeme')->timePicker('from_time');
        $form->label('Završni datum')->datePicker('to_date');
        $form->label('Završno vrijeme')->timePicker('to_time');
        //$form->label('Preko koje firme')->radioGroup('partner')->options(['1'=>"Komet",'2'=>"Flexirent"]);
        $form->label('Vrsta usluge')->radioGroup('service_type')->options(Booking::service_type_options());
        $form->label('Vezani dokument')->input('related_doc');
        $form->label('Klijent')->select2('client_id')->options(Contact::options());

        $form->label('Ukupna cijena')->price('price');
        $form->label('Status plaćanja')->radioGroup('paid')->options(Booking::paid_options());
        $form->div('block-paid-amount');
            $form->label('Uplaćeni iznos')->price('partial_amount');
        $form->closeDiv();
        jsendblock("
            function displayPaidAmount(){
                var paid = $('#paid-10:checked').val();
                console.log('paid:'+paid);
                if( paid ){
                    $('#block-paid-amount').show();
                }else{
                    $('#block-paid-amount').hide();
                }
            }
            displayPaidAmount();
            $('body').on('click',function(){
                displayPaidAmount();
            });
            $('#from_date').on('change',function(){
                if( $('#to_date').val()=='' ){
                    $('#to_date').val($('#from_date').val());
                }
            });
        ");

        jsendblock("
$(document).ready(function(){
    console.log('Modal js ready');
    var hr = {
                applyLabel: 'Odaberi',
                cancelLabel: 'Obriši',
                fromLabel: 'Od',
                toLabel: 'Do',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Ned', 'Pon', 'Uto', 'Sri', 'Čet', 'Pet', 'Sub'],
                monthNames: ['Siječanj', 'Veljača', 'Ožujak', 'Travanj', 'Svibanj', 'Lipanj', 'Srpanj', 'Kolovoz', 'Rujan', 'Listopad', 'Studeni', 'Prosinac'],
                firstDay: 1
            };
    var datepicker_options = {
      showDropdowns: true,
        singleDatePicker: true,
        calendar_style: 'picker_1',
        format: 'DD.MM.YYYY',
        locale: hr
    };

    $('#from_date').daterangepicker(datepicker_options, function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });
    var timepicker_options = {
      template: 'dropdown',
      minuteStep: 5,
        showSeconds: false,
        showMeridian: false,
        explicitMode: false,
        showInputs: true
    };

    $('#from_time').timepicker(timepicker_options);
    var hr = {
                applyLabel: 'Odaberi',
                cancelLabel: 'Obriši',
                fromLabel: 'Od',
                toLabel: 'Do',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Ned', 'Pon', 'Uto', 'Sri', 'Čet', 'Pet', 'Sub'],
                monthNames: ['Siječanj', 'Veljača', 'Ožujak', 'Travanj', 'Svibanj', 'Lipanj', 'Srpanj', 'Kolovoz', 'Rujan', 'Listopad', 'Studeni', 'Prosinac'],
                firstDay: 1
            };
    var datepicker_options = {
      showDropdowns: true,
        singleDatePicker: true,
        calendar_style: 'picker_1',
        format: 'DD.MM.YYYY',
        locale: hr
    };

    $('#to_date').daterangepicker(datepicker_options, function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });

    var timepicker_options = {
          template: 'dropdown',
          minuteStep: 5,
            showSeconds: false,
            showMeridian: false,
            explicitMode: false,
            showInputs: true
        };

    $('#to_time').timepicker(timepicker_options);

    $('#vehicle_id').select2();

    $('#client_id').select2();

});

        ");
        
        $form->label('Napomena')->text('note');

        $form->label('Početna kilometraža')->number('km_start');
        $form->label('Završna kilometraža')->number('km_end');
        $form->label('Dokumenti')->documents('docs');

        $form->label('Status')->radioGroup('status')->options(Booking::status_options());

        $form->btnSave();
        /*
        $form->add(Form::submit('Završi najam i idi na pregled','btnNext')
                        ->addClass('btn btn-primary')
                        ->style("float:right;")
                    );
        */

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }


    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/booking')->with('msg',"{$this->model} #{$item->id} deleted");
    }

    function get($id=0)
    {
        return Booking::find($id);
    }

}
