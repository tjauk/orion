<?php
class AdminCalendarController extends AdminController
{
    public $model = 'Booking';
    public $baseurl = '/admin/calendar';

    function index()
    {
        $model = $this->model;
        
        Doc::title("Raspored");

        return View::make('admin/calendar/full');
    }

    function timeline()
    {
        $model = $this->model;
        
        Doc::title("Raspored");

        return View::make('admin/calendar/timeline');
    }

    function events()
    {
        // GET/POST : start, end, vehicles[]

        $filter = new SearchFilter;
        $filter->fill(Request::all());

        $vehicles = $filter->vehicles;
        if( empty($vehicles) ){
            $vehicles = array();
        }

        // filter only vehicles with show_on_calendar=1 flag
        foreach($vehicles as $index=>$vehicle_id){
            $vehicle = Vehicle::find($vehicle_id);
            if( $vehicle->id == $vehicle->id and $vehicle->show_on_calendar<>1 ){
                unset($vehicles[$index]);
            }
        }
        
        $events = array();

        // bookings
        $bookings = Booking::inPeriod( $filter->start, $filter->end )->get();
        foreach ($bookings as $booking) {
            if( in_array($booking->vehicle_id,$vehicles) ){
                
                $from = UI::dateTime($booking->from);
                $from = str_replace('&nbsp;', ' ', $from);
                
                $to = UI::dateTime($booking->to);
                $to = str_replace('&nbsp;', ' ', $to);

                $event = [
                    'id'        => $booking->id,
                    'vehicle_id'=> $booking->vehicle_id,
                    'title'     => $booking->client->title,//.' ( '.$from.' - '.$to.' )',
                    'model'     => 'Booking',
                    'allDay'    => false,
                    'start'     => $booking->from,
                    'end'       => $booking->to,
                    'description'   => $booking->vehicle->title,
                    'color'     => '#fff',
                    'backgroundColor' => $booking->vehicle->color_hex,
                ];
                $events[] = $event;
            }
        }
        
        // services
        $services = VehicleService::inPeriod( $filter->start, $filter->end )->get();
        foreach ($services as $service) {
            if( in_array($service->vehicle_id,$vehicles) ){
                $event = [
                    'id'        => $service->id,
                    'vehicle_id'=> $service->vehicle_id,
                    'title'     => $service->vehicle->title,
                    'model'     => 'VehicleService',
                    'allDay'    => false,
                    'start'     => $service->from,
                    'end'       => $service->to,
                    'description'   => $booking->type,
                    'color'     => '#fff',
                    'backgroundColor' => '#ca0'
                ];
                $events[] = $event;
            }
        }

        // registracije, ao, kasko, itd
        $vehicles = Vehicle::ppExpires( $filter->start, $filter->end )->whereShowOnCalendar(1)->get();
        foreach ($vehicles as $vehicle) {
            $event = [
                'id'        => $vehicle->id,
                'vehicle_id'=> $vehicle->id,
                'title'     => 'PP '.$vehicle->name,
                'model'     => 'Vehicle',
                'allDay'    => true,
                'start'     => $vehicle->pp_expires,
                'end'       => $vehicle->pp_expires,
                'description'   => 'PP',
                'color'     => '#fff',
                'backgroundColor' => '#DD4B39'
            ];
            $events[] = $event;
        }
        // registracije, ao, kasko, itd
        $vehicles = Vehicle::ppaExpires( $filter->start, $filter->end )->whereShowOnCalendar(1)->get();
        foreach ($vehicles as $vehicle) {
            $event = [
                'id'        => $vehicle->id,
                'vehicle_id'=> $vehicle->id,
                'title'     => 'PP aparat '.$vehicle->name,
                'model'     => 'Vehicle',
                'allDay'    => true,
                'start'     => $vehicle->ppa_expires,
                'end'       => $vehicle->ppa_expires,
                'description'   => 'PP aparat',
                'color'     => '#fff',
                'backgroundColor' => '#DD4B39'
            ];
            $events[] = $event;
        }

        // registracije, ao, kasko, itd
        $vehicles = Vehicle::aoExpires( $filter->start, $filter->end )->whereShowOnCalendar(1)->get();
        foreach ($vehicles as $vehicle) {
            $event = [
                'id'        => $vehicle->id,
                'vehicle_id'=> $vehicle->id,
                'title'     => 'AO osiguranje za '.$vehicle->name,
                'model'     => 'Vehicle',
                'allDay'    => true,
                'start'     => $vehicle->ppa_expires,
                'end'       => $vehicle->ppa_expires,
                'description'   => 'AO osiguranje',
                'color'     => '#fff',
                'backgroundColor' => '#DD4B39'
            ];
            $events[] = $event;
        }

        // registracije, ao, kasko, itd
        $vehicles = Vehicle::kaskoExpires( $filter->start, $filter->end )->whereShowOnCalendar(1)->get();
        foreach ($vehicles as $vehicle) {
            $event = [
                'id'        => $vehicle->id,
                'vehicle_id'=> $vehicle->id,
                'title'     => 'Kasko za '.$vehicle->name,
                'model'     => 'Vehicle',
                'allDay'    => true,
                'start'     => $vehicle->kasko_expires,
                'end'       => $vehicle->kasko_expires,
                'description'   => 'Kasko',
                'color'     => '#fff',
                'backgroundColor' => '#DD4B39'
            ];
            $events[] = $event;
        }

        // registracije, ao, kasko, itd
        $vehicles = Vehicle::licenceExpires( $filter->start, $filter->end )->whereShowOnCalendar(1)->get();
        foreach ($vehicles as $vehicle) {
            $event = [
                'id'        => $vehicle->id,
                'vehicle_id'=> $vehicle->id,
                'title'     => 'Registracija za '.$vehicle->name,
                'model'     => 'Vehicle',
                'allDay'    => true,
                'start'     => $vehicle->licence_expires,
                'end'       => $vehicle->licence_expires,
                'description'   => 'Registracija',
                'color'     => '#fff',
                'backgroundColor' => '#DD4B39'
            ];
            $events[] = $event;
        }

        return $events;
    }

    function bookings()
    {
        return Booking::owned();       
    }

    function vehicles()
    {
        return Vehicle::owned();
    }

    function vehicleservices()
    {
        return VehicleService::owned();
    }

    function damages()
    {
        return Damage::owned();
    }

    function contacts()
    {
        return Contact::owned();
    }

}
