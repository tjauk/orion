<?php
class AdminCompanyController extends AdminController
{
    public $model = 'Company';
    public $baseurl = '/admin/company';

    public function __construct()
    {
        if( !Auth::can('admin') ){
            Auth::logout();
            exit( redirect('/') );
        }
        parent::__construct();
    }


    /**
     * Index page
     *
     * @return View
     */
    function index()
    {
        $model = $this->model;
        
        Doc::title(str_plural($model));

        $items = new $model();

        $buttons = Forms::make();
        $buttons->btnAdd('Add Company',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Search')->search('keywords')->placeholder('Search');
        $form->label('Per page')->select('perpage')->values([10,20,50,100]);

        $table = Table::make()
                ->headings(['Name','Broj vozila','Jed.cijena','Kn/mj','City','Email','Web','Kontakt','Note','Actions']);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $vehicle_total = Vehicle::withoutGlobalScope('owner')->whereCompanyId($item->id)->count();
            $per_vehicle_price = 25.0;
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->name.'</a>';
            $data[] = $vehicle_total;
            $data[] = Num::price($per_vehicle_price);
            $data[] = Num::price($vehicle_total*$per_vehicle_price);
            $data[] = $item->city;
            $data[] = $item->email;
            $data[] = $item->web;
            $data[] = $item->contact_person;
            $data[] = $item->note;
            $actions = UI::btnEdit($item->id,'company');
            $actions.= UI::btnDelete($item->id,'company');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


    /**
     * Edit page
     *
     * @return View
     */
    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title($model);

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('company');
        $form->label('Name')->input('name');
        $form->label('Oib')->input('oib');
        $form->label('Tax Rate')->input('tax_rate');
        $form->label('Address')->input('address');
        $form->label('Address 2')->input('address_2');
        $form->label('Zip')->input('zip');
        $form->label('City')->input('city');
        $form->label('Country Id')->input('country_id');
        $form->label('Email')->input('email');
        $form->label('Mobile')->input('mobile');
        $form->label('Phone')->input('phone');
        $form->label('Fax')->input('fax');
        $form->label('Web')->input('web');
        $form->label('Contact Person')->input('contact_person');
        $form->label('Contact Phone')->input('contact_phone');
        $form->label('Note')->text('note');

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }


    /**
     * Delete
     *
     * @return Response
     */
    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/company')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
