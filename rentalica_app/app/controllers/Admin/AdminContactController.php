<?php
class AdminContactController extends AdminController
{
    public $model = 'Contact';
    public $baseurl = '/admin/contact';

    function index()
    {
        $model = $this->model;
        
        Doc::title("Adresar");

        $items = new Contact();

        $buttons = Forms::make();
        $buttons->btnAdd('Dodaj Kontakt',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'first_name,last_name,groups,company,oib,address,zip,city,email,mobile,phone,fax,web,note' );
        }
        // search city
        if( $filter->notEmpty('city') ){
            $items = $items->whereCity( $filter->city );
        }

        $allcities = new $model();
        $cities = array_merge([''=>"---"],$allcities->groupBy('city')->orderBy('city','ASC')->get()->pluck('city', 'city')->toArray());


        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Pretraga')->search('keywords')->klass('form-control pull-left')->placeholder('po nazivu, oibu i sl.');
        $form->label('Mjesto')->select2('city')->options( $cities );
        $form->label('Po stranici')->select('perpage')->values([10,20,50,100])->klass('form-control');

        $items = $items->orderBy(DB::raw('CONCAT(last_name,company)'),'ASC');

        $items = $filter->paginate( $items )->get();

        $table = Table::make()
                ->headings([
                //'#',
                'Ime / Tvrtka',
                'OIB',
                'Tip',
                'Adresa',
                //'Pbr',
                //'Mjesto',
                //'Država',
                'Kontakti',
                'Kontakt osoba',
                //'Mob.',
                //'Tel.',
                //'Fax',
                //'Web',
                'Napomena',
                'Akcije']);

        foreach($items as $item){
            $data = [];
            //$data[] = $item->id;
            $data[] = '<a href="/admin/contact/edit/'.$item->id.'">'.$item->title.'</a>';
            $data[] = $item->oib;
            $data[] = implode(', ',$item->groups);
            $data[] = $item->address.'<br>'.$item->zip.' '.$item->city;
            //$data[] = $item->zip;
            //$data[] = $item->city;
            //$data[] = $item->country->name;
            $info = [];
            if( !empty($item->email) ){
                $info[] = '<i class="fa fa-envelope"></i>&nbsp;'.$item->email;
                //$info[] = '<i class="fa fa-envelope"></i>&nbsp;'.'<a href="mailto:'.$item->email.'">'.$item->email.'</a>';
            }
            if( !empty($item->mobitel) ){
                $info[] = '<i class="fa fa-mobile"></i>&nbsp;'.$item->mobitel;
            }
            if( !empty($item->phone) ){
                $info[] = '<i class="fa fa-phone"></i>&nbsp;'.$item->phone;
            }
            $data[] = implode('<br>',$info);
            $contact_person = [];
            if( !empty($item->contact_person) ){
                $contact_person[] = $item->contact_person;
            }
            if( !empty($item->contact_phone) ){
                $contact_person[] = '<i class="fa fa-phone"></i>&nbsp;'.$item->contact_phone;
            }
            //'<br>M:&nbsp;'.$item->contact_phone

            $data[] = implode('<br>',$contact_person);
            //$data[] = $item->fax;
            //$data[] = $item->web;
            $data[] = $item->note;
            $actions = UI::btnEdit($item->id,'contact');
            $actions.= UI::btnDelete($item->id,'contact');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title("Kontakt");

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
            Doc::title($item->title);
        }
        $form->open('contact');

        $form->div('.col-md-6');
            $form->panel('Osnovne informacije');
                $form->label('Ime')->input('first_name')->placeholder('Ime');
                $form->label('Prezime')->input('last_name');
                $form->label('Tvrtka')->input('company');
                //$form->label('Tvrtka (dugi naziv)')->input('company_long');
                //$form->label('U sustavu PDV-a')->radiogroup('tax_group')->options([''=>"Nebitno",'no'=>'Izvan sustava PDV-a','yes'=>'U sustavu PDV-a']);
                $form->label('Adresa')->street('address');
                //$form->label('Adresa 2')->street('address_2');
                $form->label('Poštanski broj')->input('zip')->addClass('col-md-3');
                $form->label('Mjesto')->input('city');
                $form->label('Država')->country()->init(52);
                $form->label('OIB')->oib('oib');
                //$form->label('MB')->input('mb');
                $form->label('Tip kontakta')->checkList('groups')->options(Contact::groups());
            $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-6');
            $form->panel('Kontakt informacije');
                $form->label('Email')->email('email');
                $form->label('Mobilni tel.')->mobile('mobile');
                $form->label('Tel.')->phone('phone');
                $form->label('Fax')->fax('fax');
                $form->label('Web')->web('web');
            $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-6');
            $form->panel('Kontakt osoba');
                $form->label('Ime')->input('contact_person');
                $form->label('Mobitel')->phone('contact_phone');
            $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-6');
            $form->panel('Napomena');
                $form->text('note')->autogrow();
            $form->closePanel();
        $form->closeDiv();

/*
        $form->div('.col-md-12');

        $form->close_div();
*/


        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/contact')->with('msg',"{$this->model} #{$item->id} deleted");
    }

    function get($id=0)
    {
        return Contact::find($id);
    }

/*
    function cleanup()
    {
        $contacts = Contact::all();
        foreach($contacts as $contact){
            echo $contact->city.'<br>';
            $contact->city = str_replace(['-HR','-RH'],'',$contact->city);
            list($zip,$city) = explode(' ',$contact->city);
            if( is_numeric($zip) ){
                echo $zip.' --- '.$city.'<br>';
                $contact->zip = $contact->zip.$zip;
                $contact->city = substr($contact->city,strlen($zip)+1);
                echo $contact->zip.' --- '.$contact->city.'<br>';
            }
            $contact->save();
        }
    }

    function duplicates()
    {
        $results = DB::select(DB::raw("SELECT company,id, count( * ) as visak
FROM contacts
WHERE company != ''
GROUP BY company
HAVING count( * ) >1"));
        foreach($results as $res){
            echo "Company:".$res->company." ".$res->visak."<br>";
            $duplicates = DB::select(DB::raw("SELECT id FROM contacts WHERE company='".$res->company."'"));
            $nr=0;
            foreach($duplicates as $dup){
                $nr++;
                if( $nr>1 ){
                    // delete id
                    echo $dup->id.' ';
                    Contact::find($dup->id)->delete();
                }
            }
        }
    }
*/

}
