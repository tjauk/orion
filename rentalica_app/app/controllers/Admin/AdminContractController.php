<?php
class AdminContractController extends AdminController
{
    public $model = 'Contract';
    public $baseurl = '/admin/contract';

    function index()
    {
        $model = $this->model;
        
        Doc::title('Ugovori');

        $items = new Contract;

        $buttons = Forms::make();
        $buttons->btnAdd('Dodaj Ugovor',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 100;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'data' );
        }

        // filter period from
        if( $filter->notEmpty('from') ){
            $items = $items->where( 'dated_at' ,'>=', $filter->from );
        }
        // filter period to
        if( $filter->notEmpty('to') ){
            $items = $items->where( 'dated_at', '<=', $filter->to );
        }

        // search vehicle
        if( $filter->notEmpty('vehicle_id') ){
            $items = $items->whereVehicleId( $filter->vehicle_id );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        
        $form->label('Pretraga')->search('keywords')->placeholder('po ključnim riječima');
        $form->label('Od datuma')->datePicker('from');
        $form->label('Do datuma')->datePicker('to');
        $form->label('Vozilo')->select('vehicle_id')->options(Vehicle::options());
        //$form->label('Po stranici')->select('perpage')->values([10,20,50,100,250]);


        $items = $items->orderBy('dated_at','DESC');
        $items = $filter->paginate( $items )->get();

        $table = Table::make()->autohead();

        foreach($items as $item){
            $data = [];
            $data['Datum'] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.UI::date($item->dated_at).'</a>';
            $data['Klijent'] = $item->client->title;
            $data['Vozilo'] = $item->vehicle->title;
            $actions = UI::btnEdit($item->id,'contract');
            $actions.= UI::btn()->link('/admin/contract/printit/'.$item->id)->title('ISPRINTAJ')->icon('fa-print')->attr('target','_blank');
            $actions.= UI::btnDelete($item->id,'contract');
            $data['Akcije'] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title("Ugovor");

        $item = $model::firstOrNew(['id'=>$id]);

        if( empty($item->dated_at) ){ $item->dated_at = date('Y-m-d',strtotime('now')); }

        $data = Request::post();
        if( $data ){
            $item->fill($data);
            $item->data = $data;
            $item->save();
            if( isset($data['btnPrint']) ){
                return redirect('/contract/printit/'.$item->id);
            }
            return redirect($this->baseurl)->with('msg',"$model {$item->name} saved");
        }else{
            $data = Request::get();
            if( isset($data['booking_id']) ){
                $booking = Booking::find($data['booking_id']);
                $data['client_id'] = $booking->client_id;
                $data['vehicle_id'] = $booking->vehicle_id;
                $data['datum_preuzimanja'] = $booking->from_date;
                $data['vrijeme_preuzimanja'] = $booking->from_time;
                $data['datum_povratka'] = $booking->to_date;
                $data['vrijeme_povratka'] = $booking->to_time;
                $data['najmodavac'] = $booking->partner;
            }
            if( isset($data['client_id']) ){
                $contact = Contact::find($data['client_id']);
                $data['najmoprimac'] = $contact->title;
                $data['adresa'] = $contact->address;
                $data['oib'] = $contact->oib;
            }
            if( isset($data['vehicle_id']) ){
                $vehicle = Vehicle::find($data['vehicle_id']);
                $data['registracija_vozila'] = $vehicle->name;
            }
            if( !empty($data) ){
                $item->fill($data);
                $item->client_id = $data['client_id'];
                $item->data = $data;
            }
        }

        $form = Forms::make()->format('EDIT_VERTICAL');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('contract_form');

        $all = array_merge($item->toArray(),(array)$item->data);
        if( empty($all['ugovor_napisao']) ){
            $all['ugovor_napisao'] = '';
        }
        $form->fill($all);
        //$form->open('checklist_form')->method('POST');

        $opt2 = ['+'=>'Ima','-'=>'Nema'];
        $opt3 = [''=>'<span style="color:#444">Nije gledano</span>&nbsp;&nbsp;','+'=>'<span style="color:#090;font-weight:bold;">OK</span>&nbsp;&nbsp;','-'=>'<span style="color:#a00;font-weight:bold;">Nije OK</span>'];

        if( empty($item->date) ){
            $item->date = date('Y-m-d');
        }

        $form->div('.col-md-4');
        $form->panel('Poveznice');
            $form->hidden('company_id');
            $form->label('Datum ugovora')->datePicker('dated_at');
            //$form->label('Izdaje')->radioGroup('najmodavac')->options(['1'=>"Komet",'2'=>"Flexirent"]);
            //$form->label('Najam')->select2('booking_id')->options(Booking::options());
            $form->hidden('booking_id');
            $form->label('Vozilo')->select('vehicle_id')->options(Vehicle::options());
            $form->label('Klijent')->contact('client_id');
        $form->closePanel();

        $form->panel('Upis');
            foreach(Contract::$meta_labels as $meta_key=>$meta_label){
                $meta_form = Contract::$meta_forms[$meta_key];
                eval('$form->label($meta_label)->'.$meta_form.'($meta_key);');
            }
        $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-8');
        $form->panel('Pregled ugovora');
            $form->div('#preview');
            $form->closeDiv();
        $form->closePanel();
        $form->closeDiv();

        $form->btnSave();
        $form->html('&nbsp;');
        $form->button('<i class="fa fa-print"></i> Isprintaj')->attr('name','btnPrint')->addClass('btn btn-primary')->style('margin-left:10px');

        $form->close();

jsblock("

function previewContract(){
    var url = '/admin/contract/preview';
    var data = $('form').serialize();
    $.ajax({
        url: url,
        data: data,
        method: 'POST',
    }).done(function(data) {
        $('#preview').html(data);
    });
}

// booking_id changed
$('#booking_id').change(function() {
    console.log('Booking changed');
    previewContract();
});

// vehicle_id changed
$('#vehicle_id').change(function(e) {
    console.log(e);
    $.get('/admin/vehicle/get/'+$(this).val(), function(data){
        $('#registracija_vozila').val(data.name);
        previewContract();
    });
});

// client_id changed
$('#client_id').change(function() {
    $.get('/admin/contact/get/'+$(this).val(), function(data){
        $('#najmoprimac').val(data.title);
        $('#adresa').val(data.address);
        $('#oib').val(data.oib);
        previewContract();
    });
});

$('form :input').change(function() {
    // submit form to contract controller preview action
    previewContract();
});
",'ready');

        return View::make('admin/edit/default')->put($form,'form');
    }

    function preview($id=0)
    {
        Doc::none();
        $data = Request::post();
        $data['company_id'] = Auth::user()->company_id;
        return View::make('admin/contract_templates/ugovor1')->put($data);
    }

    function printit($id=0)
    {
        Doc::view('admin/layout/print');
        $item = Contract::find($id);
        $data = array_merge($item->toArray(),(array)$item->data);
        jsready("window.print();");
        return View::make('admin/contract_templates/ugovor1')->put($data);
    }

    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/contract')->with('msg',"{$this->model} #{$item->id} deleted");
    }

}
