<?php

class AdminDashboardController extends AdminController
{

    /**
     * INDEX
     */
    function index()
    {
        Doc::title('Pregled');
        return View::make('admin/dashboard/sample');
    }

    function index2()
    {
    	return View::make('admin/dashboard/sample2');
    }

}
