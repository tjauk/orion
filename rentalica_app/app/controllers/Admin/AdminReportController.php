<?php
class AdminReportController extends AdminController
{

    function vehicles()
    {        
        Doc::title("Pregled po vozilima");

        $vehicles = Vehicle::all();

        $items = new \Illuminate\Support\Collection();
        $items = [];


        foreach($vehicles as $vehicle){
            $item = new Model();
            $item->id = $vehicle->id;
            $item->vehicle = $vehicle;
            $results = DB::select("
                    SELECT
                          *,
                          SUM(days) AS total_days,
                          SUM(price) AS total_price,
                          SUM(km_diff) AS total_km,
                          SUM(price)/SUM(days) AS price_per_day,
                          SUM(km_diff)/SUM(days)AS km_per_day      
                    FROM (
                        SELECT 
                               vehicles.name,
                               bookings.vehicle_id,
                               bookings.from,
                               bookings.to,
                               bookings.km_start,
                               bookings.km_end,
                               abs(datediff(bookings.from, bookings.to)) as days,
                               abs(bookings.km_start-bookings.km_end) as km_diff,
                               bookings.price,
                               bookings.price/abs(datediff(bookings.from, bookings.to)) as price_per_day,
                               abs(bookings.km_start-bookings.km_end)/abs(datediff(bookings.from, bookings.to)) as km_per_day
                        FROM vehicles
                        JOIN bookings ON bookings.vehicle_id=vehicles.id
                        WHERE bookings.vehicle_id=?
                    ) as t
                    ",[$vehicle->id]);
            foreach($results[0] as $key=>$val){
                $item->$key = $val;
            }
            $items[] = $item;
        }
        

        $filter = new SearchFilter;
        $filter->perpage = 50;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            //$items = $items->search( $filter->keywords, 'name' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Pretraga');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('po ključnim riječima');
        $form->label('Po stranici');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');

        $table = Table::make()
                ->headings([    '#',
                                'Vozilo',
                                'Broj dana',
                                'Prihod',
                                'Prihod/dan',
                                'Kilometraža',
                                'Km/dan'
                            ]);

        $items = $filter->paginate( $items );

        foreach($items as $item){
            $data = [];
            $data[] = $item->id;
            $data[] = $item->name;
            $data[] = $item->total_days;
            $data[] = Num::price($item->total_price);
            $data[] = Num::price($item->price_per_day);
            $data[] = round($item->total_km);
            $data[] = Num::price($item->km_per_day);
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }


}
