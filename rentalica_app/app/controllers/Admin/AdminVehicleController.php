<?php
class AdminVehicleController extends AdminController
{
    public $model = 'Vehicle';
    public $baseurl = '/admin/vehicle';

    function index()
    {
        $model = $this->model;
        
        Doc::title("Vozila");

        $items = new Vehicle;

        $buttons = Forms::make();
        $buttons->btnAdd('Dodaj Vozilo',$this->baseurl.'/edit');

        $filter = new SearchFilter;
        $filter->perpage = 20;
        $filter->page = 1;


        // search keywords
        if( $filter->notEmpty('keywords') ){
            $items = $items->search( $filter->keywords, 'name' );
        }

        // search form
        $form = Forms::make($filter)->format('SEARCH_FILTERS');
        $form->label('Pretraga');
        $form->search('keywords')->klass('form-control pull-left')->placeholder('po ključnim riječima');
        $form->label('Po stranici');
        $form->select('perpage')->values([10,20,50,100])->klass('form-control');

        $table = Table::make()
                ->headings([
                            'Naziv',
                            //'Marka',
                            //'Model',
                            'Tip',
                            //'Partner',
                            'Godina',
                            'Boja',
                            'Trenutna Km',
                            'Slj.servis',
                            //'Boja (kod)',
                            //'Broj šasije',
                            //'Leasing',
                            //'Veličina guma',
                            'Registracija',
                            'Per.Pregled',
                            'Protupožarni',
                            'Rata leasinga',
                            'Napomena',
                            'Actions'
                        ]);

        $items = $filter->paginate( $items )->get();

        foreach($items as $item){
            $data = [];
            $data[] = '<a href="'.$this->baseurl.'/edit/'.$item->id.'">'.$item->name.'</a>';
            //$data[] = $item->brand;
            //$data[] = $item->model;
            $data[] = $item->type;
            //$data[] = $item->partnervozila->company;
            $data[] = $item->year;
            $data[] = $item->color;
            $data[] = $item->km;
            $data[] = $item->next_service;
            //$data[] = $item->color_hex;
            //$data[] = $item->chassis_number;
            //$data[] = $item->leasing;
            //$data[] = $item->tire_size;
            $data[] = UI::date($item->licence_expires); // paint in red if expired, paint in orange if close
            $data[] = UI::date($item->pp_expires);
            $data[] = UI::date($item->ppa_expires);
            $data[] = Num::price($item->leasing_amount);
            $data[] = $item->note;
            $actions = UI::btnEdit($item->id,'vehicle');
            $actions.= UI::btnDelete($item->id,'vehicle');
            $data[] = $actions;
            $table->addRow($data);
        }
        $table = $table->render('admin/table/grid');
        
        return View::make('admin/list/default')
                    ->put($this,'controller')
                    ->put($buttons,'buttons')
                    ->put($filter,'filter')
                    ->put($form,'searchform')
                    ->put($table,'table');
    }

    function edit($id=0)
    {
        $model = $this->model;
        
        Doc::title("Vozilo");

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            VehicleKm::register($item->id,$item->km);
            $item->save();
            return redirect($this->baseurl)->with('msg',"Snimljeno vozilo {$item->name}");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }
        $form->open('vehicle');

        $form->div('.col-md-6');
        $form->panel('Osnovne informacije');
            $form->label('Registarska tablica')->input('licence_plate');
            $form->hidden('name');//->label('Naziv vozila')
            $form->label('Marka vozila')->input('brand');
            $form->label('Model')->input('model');
            $form->label('Tip')->radioGroup('type')->values(Vehicle::types());
            $form->label('Broj šasije')->input('chassis_number');
            $form->label('Godina proizvodnje')->select('year')->range(1990,2016);
            $form->label('Prešao kilometara')->number('km');
            $form->label('Redoviti servis svakih (km)')->number('service_km');
            $form->label('Veličina guma')->input('tire_size');
            $form->label('Boja')->input('color');
            //$form->label('Partner')->select2('partner')->options(Contact::options());
            $form->label('Leasing kuća')->input('leasing');
            $form->label('Rata leasinga')->price('leasing_amount')->currency('kn');
        $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-6');
        $form->panel('Dodatne informacije (trenutne osg.kuće za vozila)');
            $form->label('Registriran do')->datePicker('licence_expires');
            $form->label('AO osiguranje vrijedi do')->datePicker('ao_expires');
            $form->label('label.AO osg.kuća')->input('ao_house');
            $form->label('Kasko vrijedi do')->datePicker('kasko_expires');
            $form->label('label.Kasko osg.kuća')->input('kasko_house');
            $form->label('Periodički pregled vrijedi do')->datePicker('pp_expires');
            $form->label('Protupožarni aparat vrijedi do')->datePicker('ppa_expires');
            $form->label('Prikaži ovo vozilo u rasporedu')->radioGroup('show_on_calendar')->options(['1'=>'Da','0'=>'Ne']);
            $form->label('Boja za prikaz')->colorPicker('color_hex');
        $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-12');
        $form->panel('Dokumenti');
            // prvo prebaci docs u notes, pa onda postavi komponente !!!!
            $form->label('Dokumenti')->documents('docs');
            $form->label('Napomena')->text('note')->autogrow();
        $form->closePanel();
        $form->closeDiv();

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }

    function modal($id=0)
    {
        Doc::view('admin/modal/default');
        
        $model = $this->model;
        
        Doc::title("Vozilo");

        $item = $model::firstOrNew(['id'=>$id]);

        if( Request::post() ){
            $item->fill(Request::post());
            $item->save();
            return redirect($this->baseurl)->with('msg',"Snimljeno vozilo {$item->name}");
        }

        $form = Forms::make()->format('EDIT');
        if( !empty($item) ){
            $form->fill($item);
        }

        $form->open('modal-booking-form')
                ->ajax('/admin/vehicle/modal/'.$item->id)
                ->onAjaxDone('$("#calendar").fullCalendar("refetchEvents");');

        $form->div('.col-md-6');
        $form->panel('Osnovne informacije');
            $form->label('Registarska tablica')->input('licence_plate');
            $form->label('Naziv vozila')->input('name');
            $form->label('Marka vozila')->input('brand');
            $form->label('Model')->input('model');
            $form->label('Tip')->radioGroup('type')->values(Vehicle::types());
            $form->label('Broj šasije')->input('chassis_number');
            $form->label('Godina proizvodnje')->select('year')->range(1990,2016);
            $form->label('Prešao kilometara')->number('km');
            $form->label('Redoviti servis svakih (km)')->number('service_km');
            $form->label('Veličina guma')->input('tire_size');
            $form->label('Boja')->input('color');
            //$form->label('Boja (kod)')->colorPicker('color_hex');
            //$form->label('Partner')->select2('partner')->options(Contact::options());
            $form->label('Leasing kuća')->input('leasing');
            $form->label('Rata leasinga')->price('leasing_amount')->currency('kn');
        $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-6');
        $form->panel('Dodatne informacije (trenutne osg.kuće za vozila)');
            $form->label('Registriran do')->datePicker('licence_expires');
            $form->label('AO osiguranje vrijedi do')->datePicker('ao_expires');
            $form->label('AO osg.kuća')->input('ao_house');
            $form->label('Kasko vrijedi do')->datePicker('kasko_expires');
            $form->label('Kasko osg.kuća')->input('kasko_house');
            $form->label('Periodički pregled vrijedi do')->datePicker('pp_expires');
            $form->label('Protupožarni aparat vrijedi do')->datePicker('ppa_expires');
            $form->label('Prikaži ovo vozilo u rasporedu')->radioGroup('show_on_calendar')->options(['1'=>'Da','0'=>'Ne']);
        $form->closePanel();
        $form->closeDiv();

        $form->div('.col-md-12');
        $form->panel('Dokumenti');
            // prvo prebaci docs u notes, pa onda postavi komponente !!!!
            $form->label('Dokumenti')->documents('docs');
            $form->label('Napomena')->text('note')->autogrow();
        $form->closePanel();
        $form->closeDiv();

        $form->btnSave();

        $form->close();
        
        return View::make('admin/edit/default')->put($form,'form');
    }



    function delete($id=0)
    {
        $model = $this->model;
        $item = $model::find($id);
        // !!! Add auth check if user can delete this model
        if( $item->id>0 ){
            $item->delete();
        }
        return redirect('/admin/vehicle')->with('msg',"{$this->model} #{$item->id} deleted");
    }

    function get($id=0)
    {
        return Vehicle::find($id);
    }

}
