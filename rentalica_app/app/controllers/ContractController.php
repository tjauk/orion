<?php
class ContractController extends Controller
{

    function printit($id=0)
    {
        Doc::view('admin/layout/print');
        $item = Contract::find($id);
        $data = array_merge($item->toArray(),(array)$item->data);
        jsready("window.print();");
        return View::make('admin/contract_templates/ugovor1')->put($data);
    }

}
