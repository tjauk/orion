<?php
class UserController extends Controller
{

    /**
     * 
     *
     * @return Response
     */
    public function index()
    {
        return redirect('user/login');
    }

    /**
     * 
     *
     * @return Response
     */
    public function widget()
    {
        //
    }

    /**
     * 
     *
     * @return Response
     */
    public function login()
    {
        return redirect('/admin/auth/login');
        
        $form = Forms::make();
        $form->open('user/login');
        $form->label('Username')->input('username');
        $form->label('Password')->password('password');
        $form->submit('Sign in');
        $form->close();

        return View::make('user/login')->put($form,'form');
    }

    /**
     * 
     *
     * @return Response
     */
    public function postLogin()
    {
        $post = Request::post();
        $credentials = ['username'=>$post['username'],'password'=>$post['password']];
        if( Auth::attempt($credentials) ){
            $intended = Session::get('intended');
            if( !empty($intended) ){
                Session::put('intended',null);
                return redirect($intended);
            }
            return redirect('admin/dashboard')->msg("Welcome to rentalica.net administration");
        }
        return redirect('user/login')->msg("Invalid credentials");
    }

    /**
     * 
     *
     * @return Response
     */
    public function logout()
    {
        Auth::logout();
        return redirect('user/login');
    }

    /**
     * 
     *
     * @return Response
     */
    public function register()
    {
        Doc::none();

        $form = Forms::make();
        $form->open('user/register');

        $form->label('Username')->input('username')->addClass('form-control')->placeholder('Vaša email adresa za pristup');
        $form->label('Password')->password('password')->addClass('form-control')->placeholder('Upišite lozinku');
        $form->label('Repeated password')->password('repeated_password')->addClass('form-control')->placeholder('Ponovite lozinku');
        
        $form->label('Company')->input('company_name')->addClass('form-control')->placeholder('Naziv tvrtke');
        $form->label('OIB')->input('company_oib')->addClass('form-control')->placeholder('OIB');
        $form->label('Address')->input('company_address')->addClass('form-control')->placeholder('Adresa sjedišta');
        $form->label('Zip')->input('company_zip')->addClass('form-control')->placeholder('Poštanski broj mjesta');
        $form->label('City')->input('company_city')->addClass('form-control')->placeholder('Naziv mjesta');
        $form->label('Country')->select('company_country_id')->options(Country::options())->addClass('form-control')->value(52);
        $form->label('Tax')->radioList('company_tax_rate')->options(['1.25'=>"U sustavu PDV-a (25%)",'1.0'=>"Izvan sustava PDV-a (0%)"])->addClass('form-control')->value('1.25');
        $form->label('Telefon')->input('company_phone')->addClass('form-control')->placeholder('Upišite službeni kontakt telefon');
        
        $form->submit('Otvori korisnički račun')->addClass('form-control');
        $form->close();

        return View::make('user/register')->put($form,'form');
    }

    /**
     * 
     *
     * @return Response
     */
    public function postRegister()
    {
        $post = Request::post();
        $credentials = ['username'=>$post['username'],'password'=>$post['password']];

        if( empty($post['username']) ){
            return redirect('/user/register')->msg("Korisničko ime ne smije biti prazno.");
        }
        if( empty($post['password']) ){
            return redirect('/user/register')->msg("Lozinka ne može biti prazna.");
        }

        // check existence
        $user = User::whereUsername($post['username'])->first();
        if( $user->id>0 ){
            // try auth attempt and redirect to account on success
            // or return with error message
            return redirect('/user/register')->msg("Korisnik s takovom email adresom već postoji. Odaberite drugu.");
        }

        if( $post['password']!==$post['repeated_password'] ){
            return redirect('/user/register')->msg("lozinka i ponovljena lozinka nisu iste.");
        }

        // create account owner
        $user = new User();
        $user->username = $post['username'];
        $user->first_name = $post['company_name'];
        $user->email = $post['username'];
        $user->password = $post['password'];
        $user->usergroup = 'owner';
        $user->save();

        // create company
        $company = new Company();
        $company->name = $post['company_name'];
        $company->oib = $post['company_oib'];
        $company->address = $post['company_address'];
        $company->zip = $post['company_zip'];
        $company->city = $post['company_city'];
        $company->country_id = $post['company_country_id'];
        $company->tax_rate = $post['company_tax_rate'];
        $company->phone = $post['company_phone'];
        $company->save();

        $user->company_id = $company->id;
        $user->save();

        if( Auth::attempt($credentials) ){
            return redirect('admin/dashboard')->with('msg',"Registered new user");
        }

        return redirect('user/register')->with('msg',"Invalid credentials");
    }

    /**
     * 
     *
     * @return Response
     */
    public function lostPassword()
    {
        $form = Forms::make();
        $form->open('user/lost-password');
        $form->label('Email')->email('email');
        $form->close();
        return View::make('user/lost-password')->put($form,'form');
    }

    /**
     * 
     *
     * @return Response
     */
    public function postLostPassword()
    {
        $email = Request::post('email');
        $user = User::findByEmail($email);
        if( $user->id>0 ){
            // generate token
            $token = Auth::generateToken();
            // save to password_resets table
            DB::table('password_resets')->insert([
                'email'=>$email,
                'token'=>$token,
                'created_at'=>Carbon::now()
            ]);
            // send mail
            
            // show message
            $msg = "Poslali smo vam mail s uputama kako resetirati svoju lozinku";
            
        }else{
            // redirec wtih message
            return redirect('user/lost-password')->with(['msg'=>"Nismo pronašli korisnik s takvom email adresom"]);
        }
        $form = Forms::make();
        $form->open('user/lost-password');
        $form->label('Email')->email('email');
        $form->close();
        return View::make('user/lost-password')->put($form,'form');
    }

    /**
     * 
     *
     * @return Response
     */
    public function verifyToken()
    {
        $token = Request::get('token');
        if( empty($token) ){

        }
        $user = User::findByToken($token);
        return View::make('user/verify-token');
    }


}
