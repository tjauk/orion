<?php

class CreateVehiclesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->integer('company_id')->unsigned();

            $table->string('name')->default('');
            $table->string('brand')->default('');
            $table->string('model')->default('');
            
            $table->string('licence_plate')->default('');
            $table->string('chassis_number')->default('');
            
            $table->string('type')->default('');
            $table->tinyInteger('partner')->default(0);

            $table->string('year')->default('');
            $table->string('tire_size')->default('');
            
            $table->string('color')->default('White');
            
            $table->string('leasing')->default('');
            $table->string('leasing_amount')->default('');
            
            $table->date('licence_expires')->nullable();
            $table->date('pp_expires')->nullable();
            $table->date('ppa_expires')->nullable();

            $table->date('ao_expires')->nullable();
            $table->string('ao_house')->default('');
            $table->date('kasko_expires')->nullable();
            $table->string('kasko_house')->default('');

            $table->integer('km')->unsigned()->default(0);
            $table->integer('service_km')->unsigned()->default(50000);

            $table->text('docs')->default('');
            $table->text('note')->default('');

            $table->integer('show_on_calendar')->default(1);
            $table->string('color_hex')->default('#FFFFFF');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicles');
    }

}
