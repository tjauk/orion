<?php

class CreateVehicleServicesTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_services', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->integer('company_id')->unsigned();
            
            $table->integer('vehicle_id');
            $table->string('vehicle_km')->default('');
            $table->string('type')->default('Redovan');
            $table->string('status')->default('');
            
            $table->date('from')->nullable();
            $table->date('to')->nullable();

            $table->integer('service_id')->nullable();
            $table->string('service_price')->default('');
            $table->string('parts_price')->default('');
            $table->tinyInteger('service_status')->default(1);

            $table->text('docs')->default('');
            $table->text('note')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicle_services');
    }

}
