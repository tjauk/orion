<?php

class CreateDamagesTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('damages', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->integer('company_id')->unsigned();
            
            $table->string('name')->default('');
            $table->integer('vehicle_id')->default(0);
            $table->string('vehicle_km')->default('');
            
            $table->date('date')->nullable();
            $table->text('where')->default('');
            
            $table->string('status')->default('');
            $table->text('description')->default('');

            $table->text('docs')->default('');
            $table->text('note')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('damages');
    }

}
