<?php

class CreateBookingsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->integer('company_id')->unsigned();
            
            $table->integer('booked_by')->unsigned();
            $table->integer('vehicle_id')->unsigned();

            $table->tinyInteger('service_type')->default(10);
            
            $table->dateTime('from')->nullable();
            $table->dateTime('to')->nullable();
            $table->string('km_start')->default('');
            $table->string('km_end')->default('');
            
            $table->integer('client_id')->unsigned()->nullable();
            $table->string('partner')->nullable();
            
            $table->integer('status')->unsigned()->default(0);
            $table->float('price');
            $table->float('partial_amount');
            $table->string('related_doc')->default('');
            
            $table->text('docs')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookings');
    }

}
