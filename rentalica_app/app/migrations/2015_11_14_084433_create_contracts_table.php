<?php

class CreateContractsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->integer('company_id')->unsigned();

            $table->string('name')->default('');
            $table->date('dated_at')->nullable();
            $table->string('status')->default('');
            
            $table->integer('booking_id')->nullable();
            $table->integer('vehicle_id')->nullable();
            $table->integer('client_id')->nullable();
            
            $table->text('data')->default('');
            $table->text('pdf')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contracts');
    }

}
