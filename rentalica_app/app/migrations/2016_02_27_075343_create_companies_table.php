<?php

class CreateCompaniesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            
            $table->string('name',100)->default('');
            $table->string('oib',20)->default('');
            $table->float('tax_rate')->default(1.25);
            
            $table->string('address',100)->default('');
            $table->string('address_2',100)->default('');
            $table->string('zip',20)->default('');
            $table->string('city',50)->default('');
            $table->integer('country_id')->default(52);

            $table->string('email',100)->default('');
            $table->string('mobile',50)->default('');
            $table->string('phone',50)->default('');
            $table->string('fax',50)->default('');
            $table->string('web',100)->default('');

            $table->string('contact_person')->default('');
            $table->string('contact_phone')->default('');

            $table->text('note')->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }

}
