<?php

class SeedInitialUsersAndGroups extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $company = Company::create([
            'name'  => "Trinium media d.o.o.",
            'oib'   => "83445447856"
        ]);
        User::whereNull('company_id')->update(['company_id'=>$company->id]);

        // 
        $usergroup = UserGroup::findByName('admin');
        $usergroup->permissions = explode(',','access.admin,admin,menu.user,user.create,user.switchto,user.ownpassword,manage.usergroups,menu.dashboard,menu.calendar,menu.booking,menu.contact,menu.vehicle,menu.vehicle-service,menu.damage,menu.checklist,menu.companies');
        $usergroup->save();

        $usergroup = new UserGroup();
        $usergroup->title = 'Account owner';
        $usergroup->name = 'owner';
        $usergroup->permissions = explode(',','access.admin,user.ownpassword,menu.dashboard,menu.calendar,menu.booking,menu.contact,menu.vehicle,menu.vehicle-service,menu.damage,menu.checklist');
        $usergroup->save();

        $usergroup = new UserGroup();
        $usergroup->title = 'Account user';
        $usergroup->name = 'user';
        $usergroup->permissions = explode(',','access.admin,user.ownpassword,menu.dashboard,menu.booking,menu.contact,menu.vehicle,menu.checklist');
        $usergroup->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }

}
