<?php

class AddFieldsToCompany extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function($table)
        {
            $table->string('logo')->default('');
            $table->string('ziro')->default('');
            $table->string('bank')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function($table)
        {
            $table->dropColumn('logo');
            $table->dropColumn('ziro');
            $table->dropColumn('bank');
        });
    }

}
