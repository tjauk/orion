<?php

class AddFieldsToBookings extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function($table)
        {
            $table->tinyInteger('paid')->unsigned()->default(0);
            $table->text('note')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function($table)
        {
            $table->dropColumn('paid');
            $table->dropColumn('note');
        });
    }

}
