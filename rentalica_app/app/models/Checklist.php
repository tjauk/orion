<?php
class Checklist extends Model
{
    protected $table = 'checklists';

    protected $fillable = [
        'check_date',
        'vehicle_id',
        'data'
    ];

    static $nesto = [
        'brisaci','kratka_svjetla','duga_svjetla','zmigavci','svjetlo_registracije','svjetlo_kocnice','svjetlo_rikverc','instrument_ploca',
        'tekucina_za_stakla','tekucina_za_hladenje','tekucina_serva','tekucina_kocnica','gume_tlak_stanje','rad_ventilacije_brzine','rad_klima_uredaja','stanje_sjedala',
        'stanje_ut_prostora','pp_aparat_ispravnost','rezervna_guma','dizalica','prva_pomoc','zarulje','prsluk',
        'knjizica_vozila','polica_auto_odgovornosti','zeleni_karton','eu_izvjestaj_stete'
    ];

    /*
     * Boot
     */
    static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->company_id = Auth::user()->company_id;
        });

        static::addGlobalScope('owner', function($builder) {
            $builder->where('company_id', '=', Auth::user()->company_id);
        });
    }
    
    /*
     * Relations
     */
    public function company()
    {
        return $this->belongsTo('Company');
    }
    
    public function vehicle()
    {
        return $this->belongsTo('Vehicle');
    }


    /*
     * Scopes
     */
    use \Trinium\Traits\ScopeCompanyOwned;
    
    public function scopeChecked($query)
    {
        $query->where('check_date', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */
    public function getDataAttribute()
    {
        return json_decode($this->attributes['data'],true);
    }
    public function unpackData()
    {
        $data = $this->data;
        foreach($data as $key=>$val)
        {
            $this->attributes[$key] = $val;
        }
    }
    
    /*
     * Setters
     */
    public function setDataAttribute( $data=[] )
    {
        foreach($data as $key=>$val){
            if( in_array($key,$this->fillable) or in_array($key,['id','created_at','updated_at']) ){
                unset($data[$key]);
            }
        }
        $this->attributes['data'] = json_encode($data);
    }

    function setCheckDateAttribute($str)
    {
        $val = date('Y-m-d',strtotime($str));
        if( empty($str) or $str=='0000-00-00' ){
            $val = date('Y-m-d');
        }
        $this->attributes['check_date'] = $val;
    }

    // COUNTS
    function countNotChecked()
    {
        $data = $this->data;
        $count = -3; // btnSave
        foreach($data as $d){
            if( $d=="" ){
                $count++;
            }
        }
        return $count;
    }
    function countCheckOK()
    {
        $data = $this->data;
        $count = 0;
        foreach($data as $d){
            if( $d=="+" ){
                $count++;
            }
        }
        return $count;
    }
    function countCheckNotOK()
    {
        $data = $this->data;
        $count = 0;
        foreach($data as $d){
            if( $d=="-" ){
                $count++;
            }
        }
        return $count;
    }
    function countAll()
    {
        return count($this->data)-3;
    }



}
