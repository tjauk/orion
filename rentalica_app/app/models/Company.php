<?php
class Company extends Model
{
    protected $table = 'companies';

    protected $fillable = [
		'name',
		'oib',
		'address',
		'address_2',
		'zip',
		'city',
		'country_id',
		'email',
		'mobile',
		'phone',
		'fax',
		'web',
		'contact_person',
		'contact_phone',
		'note',

        'logo',
        'ziro',
        'bank',
    ];

    /*
     * Relations
     */
    public function country()
    {
        return $this->belongsTo('Country');
    }



    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopeOwned($query)
    {
        $query->where('id','=',\Trinium\Auth::user()->company_id);
    }    

    /*
     * Getters
     */


    /*
     * Setters
     */


}
