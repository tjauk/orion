<?php
class Contract extends Model
{
    protected $table = 'contracts';

    protected $fillable = ['name','dated_at','status','company_id','booking_id','client_id','vehicle_id','data','pdf'];

    static $meta = [
        'najmoprimac',
        'adresa',
        'oib',
        'registracija_vozila',

        'datum_preuzimanja',
        'vrijeme_preuzimanja',
        'datum_povratka',
        'vrijeme_povratka',

        'cijena_po_danu',
        'ukupno_dana',
        'dnevna_kilometraza',
        'popust',

        'vozac',
        'vozac_adresa',
        'vozac_zemlja',
        'vozac_broj_vozacke',
        'vozac_telefon',

        'dodatni_kilometri',
        'dodatni_sat_najma',
        'preuzimanje_izvan_rv',
        'dostava_vozila_na_adresu',

        'napomena',
        'napomena2',
        'ugovor_napisao',
    ];

    static $meta_labels = [
        'najmoprimac'               => "Ime ili naziv Najmoprimca",
        'adresa'                    => "Adresa sjedišta",
        'oib'                       => "OIB",
        'registracija_vozila'       => "Tip i registracija vozila", 
    
        'datum_preuzimanja'         => "Datum preuzimanja", 
        'vrijeme_preuzimanja'       => "Vrijeme preuzimanja", 
        'datum_povratka'            => "Datum povratka", 
        'vrijeme_povratka'          => "Vrijeme povratka", 
    
        'cijena_po_danu'            => "Cijena po danu (bez PDV-a)", 
        'ukupno_dana'               => "Ukupno dana", 
        'dnevna_kilometraza'        => "Dnevna kilometraža (km)", 
        'popust'                    => "Popust (%)", 
    
        'vozac'                     => "Vozač", 
        'vozac_adresa'              => "Adresa vozača", 
        'vozac_zemlja'              => "Zemlja vozača", 
        'vozac_broj_vozacke'        => "Broj vozačke", 
        'vozac_telefon'             => "Broj telefona vozača", 
    
        'dodatni_kilometri'         => "Dodatni kilometri (km)", 
        'dodatni_sat_najma'         => "Dodatni sat najma (kn)", 
        'preuzimanje_izvan_rv'      => "Preuzimanje izvan radnog vremena (kn)", 
        'dostava_vozila_na_adresu'  => "Dostava vozila na adresu (kn)", 

        'napomena'                  => "Dodatna napomena (kod preuzimanja)",
        'napomena2'                 => "Dodatna napomena (kod vraćanja)",
        'ugovor_napisao'            => "Ugovor napisao",
    ];

    static $meta_forms = [
        'najmoprimac'               => "input",
        'adresa'                    => "input",
        'oib'                       => "input",
        'registracija_vozila'       => "input",
    
        'datum_preuzimanja'         => "datePicker",
        'vrijeme_preuzimanja'       => "timePicker",
        'datum_povratka'            => "datePicker",
        'vrijeme_povratka'          => "timePicker",
    
        'cijena_po_danu'            => "price",
        'ukupno_dana'               => "number",
        'dnevna_kilometraza'        => "number",
        'popust'                    => "number",
    
        'vozac'                     => "input",
        'vozac_adresa'              => "input",
        'vozac_zemlja'              => "input",
        'vozac_broj_vozacke'        => "input",
        'vozac_telefon'             => "input",
    
        'dodatni_kilometri'         => "number",
        'dodatni_sat_najma'         => "number",
        'preuzimanje_izvan_rv'      => "input",
        'dostava_vozila_na_adresu'  => "input",

        'napomena'                  => "text",
        'napomena2'                 => "text",
        'ugovor_napisao'            => "input",
    ];

    /*
     * Boot
     */
    static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->company_id = Auth::user()->company_id;
        });

        static::addGlobalScope('owner', function($builder) {
            $builder->where('company_id', '=', Auth::user()->company_id);
        });
    }
    
    /*
     * Relations
     */
    public function company()
    {
        return $this->belongsTo('Company');
    }
    
    public function vehicle()
    {
        return $this->belongsTo('Vehicle');
    }

    public function client()
    {
        return $this->belongsTo('Contact','client_id','id');
    }

    public function booking()
    {
        return $this->belongsTo('Booking');
    }


    /*
     * Options
     */


    /*
     * Scopes
     */
    use \Trinium\Traits\ScopeCompanyOwned;
    
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */

    /*
     * Getters
     */
    public function getDatedAtAttribute()
    {
        if( empty($this->attributes['dated_at']) ){
            return '';
        }
        return date('Y-m-d',strtotime($this->attributes['dated_at']));
    }
    public function getDataAttribute()
    {
        return json_decode($this->attributes['data'],true);
    }
    public function unpackData()
    {
        $data = $this->data;
        foreach($data as $key=>$val)
        {
            $this->attributes[$key] = $val;
        }
    }
    
    /*
     * Setters
     */
    public function setDatedAtAttribute($date)
    {
        $date = date('Y-m-d',strtotime($date));
        $this->attributes['dated_at'] = $date;
    }
    public function setDataAttribute( $data=[] )
    {
        foreach($data as $key=>$val){
            if( in_array($key,$this->fillable) or in_array($key,['id','created_at','updated_at']) ){
                unset($data[$key]);
            }
        }
        $this->attributes['data'] = json_encode($data);
    }

}
