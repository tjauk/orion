<?php
class ContractTemplate extends Model
{
    protected $table = 'contract_templates';

    protected $fillable = ['company_id','name','slug','template'];

    /*
     * Relations
     */
    public function company()
    {
        return $this->belongsTo('Company');
    }

    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */


}
