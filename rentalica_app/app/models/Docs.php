<?php
class Docs extends Model
{
    protected $table = 'documents';

    protected $fillable = [];

    /*
     * Boot
     */
    static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->company_id = Auth::user()->company_id;
        });

        static::addGlobalScope('owner', function($builder) {
            $builder->where('company_id', '=', Auth::user()->company_id);
        });
    }

    /*
     * Relations
     */
    public function company()
    {
        return $this->belongsTo('Company');
    }

    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->name;
        };

        return $options;
    }


    /*
     * Scopes
     */


    /*
     * Getters
     */


}
