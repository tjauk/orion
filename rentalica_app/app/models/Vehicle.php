<?php
class Vehicle extends Model
{
    protected $table = 'vehicles';

    protected $fillable = [
        'name',
        'brand',
        'model',
        'type',
        'partner',
        'year',
        'km',
        'service_km',
        'color',
        'color_hex',
        'chassis_number',
        'leasing',
        'leasing_amount',
        'tire_size',
        'pp_expires',
        'ppa_expires',
        'ao_expires',
        'ao_house',
        'kasko_expires',
        'kasko_house',
        'licence_plate',
        'licence_expires',
        'show_on_calendar',
        'docs',
        'note'
    ];

    static function types()
    {
        return ['Teretni','Putnički','Partnerski teretni','Partnerski putnički'];
    }

    /*
     * Boot
     */
    static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->company_id = Auth::user()->company_id;
        });

        static::addGlobalScope('owner', function($builder) {
            $builder->where('company_id', '=', Auth::user()->company_id);
        });
    }

    /*
     * Relations
     */
    public function company()
    {
        return $this->belongsTo('Company');
    }

    public function partnervozila()
    {
        return $this->belongsTo('Contact','partner');
    }
    
    public function next_service_temp(){
        // prvo pronalazi servis sa upisanim pocetnim datumom i statusom treba ići,
        // nakon toga nađi servise s treba ici i kilometražom većom od trenutne na vozilu
        // return VehicelService models (this should be scope)
        return null;
    }

    /*
     * Options
     */
    static function options()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->title;
        };

        return $options;
    }
    static function options_with_km()
    {
        $options = [''=>'---'];
        $items = static::all();

        foreach($items as $item){
            $options[$item->id] = $item->title.' ( '.$item->km.' km )';
        };

        return $options;
    }


    /*
     * Scopes
     */
    use \Trinium\Traits\ScopeCompanyOwned;

    public function scopePublished($query)
    {
        $query->where('published_at', '<', \DB::raw('NOW()'));
    }


    /*
     * Getters
     */
    public function getNameAttribute()
    {
        if( empty($this->attributes['name']) ){
            return implode(' ',array_filter([$this->brand,$this->model,$this->licence_plate]));
        }
        return $this->attributes['name'];//.' ('.$this->brand.' '.$this->model.', '.$this->color.')'
    }
    public function getTitleAttribute()
    {
        return $this->name;
    }

    // default color
    public function getColorHexAttribute()
    {
        $color = $this->attributes['color_hex'];
        if( empty($color) ){
            return '#f80';
        }
        return $color;
    }

    /*
     * Setters
     */
    function setPpExpiresAttribute($str)
    {
        $val = date('Y-m-d',strtotime($str));
        if( empty($str) or $str=='0000-00-00' ){
            $val = null;
        }
        $this->attributes['pp_expires'] = $val;
    }
    function setPpaExpiresAttribute($str)
    {
        $val = date('Y-m-d',strtotime($str));
        if( empty($str) or $str=='0000-00-00' ){
            $val = null;
        }
        $this->attributes['ppa_expires'] = $val;
    }
    function setAoExpiresAttribute($str)
    {
        $val = date('Y-m-d',strtotime($str));
        if( empty($str) or $str=='0000-00-00' ){
            $val = null;
        }
        $this->attributes['ao_expires'] = $val;
    }
    function setKaskoExpiresAttribute($str)
    {
        $val = date('Y-m-d',strtotime($str));
        if( empty($str) or $str=='0000-00-00' ){
            $val = null;
        }
        $this->attributes['kasko_expires'] = $val;
    }
    function setLicenceExpiresAttribute($str)
    {
        $val = date('Y-m-d',strtotime($str));
        if( empty($str) or $str=='0000-00-00' ){
            $val = null;
        }
        $this->attributes['licence_expires'] = $val;
    }


    /*
     * Update Km if greater then prev value
     */
    static function updateKm($vehicle_id,$km){
        if( $vehicle_id>0 ){
            $vehicle = Vehicle::find($vehicle_id);
            if( $vehicle ){
                if( $km>$vehicle->km ){
                    $vehicle->km = $km;
                    $vehicle->save();

                }
            }
            VehicleKm::register($vehicle->id,$km);
        }
    }


    /*
     * Reporting
     */
    public function getStats(){
       
    }



    /*
     * Expirations between dates
     */
    public function scopeExpiresBetween($query,$from=null,$to=null,$field='licence_expires')
    {
        if( is_null($from) ){
            $from = date('Y-m-d');
        }
        if( is_null($to) ){
            $to = $from;
        }
        $query->whereRaw("( DATE(`$field`)>=DATE('{$from}') AND DATE(`$field`)<=DATE('{$to}') )");
    }

    public function scopePpExpires($query,$from=null,$to=null)
    {
        return $this->scopeExpiresBetween($query,$from,$to,'pp_expires');
    }

    public function scopePpaExpires($query,$from=null,$to=null)
    {
        return $this->scopeExpiresBetween($query,$from,$to,'ppa_expires');
    }

    public function scopeAoExpires($query,$from=null,$to=null)
    {
        return $this->scopeExpiresBetween($query,$from,$to,'ao_expires');
    }

    public function scopeKaskoExpires($query,$from=null,$to=null)
    {
        return $this->scopeExpiresBetween($query,$from,$to,'kasko_expires');
    }

    public function scopeLicenceExpires($query,$from=null,$to=null)
    {
        return $this->scopeExpiresBetween($query,$from,$to,'licence_expires');
    }
    

    /*
     * Future expirations of licence, kasko, pp, ppa
     */
    static function itemsExpiringSoon($days=15)
    {
        $items = DB::select("
            SELECT * FROM
            (
            (
            SELECT id,name,pp_expires as datum,'Periodički pregled' as item
            FROM vehicles
            WHERE pp_expires BETWEEN CURDATE() AND DATE_ADD(CURDATE(),INTERVAL $days DAY)
            )
            UNION
            (
            SELECT id,name,ppa_expires as datum,'Protupožarni aparat' as item
            FROM vehicles
            WHERE ppa_expires BETWEEN CURDATE() AND DATE_ADD(CURDATE(),INTERVAL $days DAY)
            )
            UNION
            (
            SELECT id,name,licence_expires as datum,'Registracija' as item
            FROM vehicles
            WHERE licence_expires BETWEEN CURDATE() AND DATE_ADD(CURDATE(),INTERVAL $days DAY)
            )
            UNION
            (
            SELECT id,name,kasko_expires as datum,'Kasko' as item
            FROM vehicles
            WHERE kasko_expires BETWEEN CURDATE() AND DATE_ADD(CURDATE(),INTERVAL $days DAY)
            )
            ) as t
            ORDER BY datum ASC"
        );
        return $items;
    }
    


}
