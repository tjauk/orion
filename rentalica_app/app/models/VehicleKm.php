<?php
class VehicleKm extends Model
{
    protected $table = 'vehicle_kms';

    protected $fillable = [
        'company_id',
		'vehicle_id',
		'km'
    ];

    /*
     * Relations
     */
    public function company()
    {
        return $this->belongsTo('Company');
    }

    public function vehicle()
    {
        return $this->belongsTo('Vehicle');
    }


    static function register($vehicle_id=null,$km=0)
    {
        $company_id = Auth::user()->company_id;
        if( !is_null($vehicle_id) and $km>0 ){
            $last_km = static::whereVehicleId($vehicle_id)->orderBy('km','DESC')->first()->km;
            if( $km > $last_km ){
                static::create([
                    'company_id'=>$company_id,
                    'vehicle_id'=>$vehicle_id,
                    'km'=>$km
                ]);
            }
        }
    }

}
