<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rentalica_wp');

/** MySQL database username */
define('DB_USER', 'wps');

/** MySQL database password */
define('DB_PASSWORD', 'WPdbAdmin1234');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'SP/H|B6i-dgdQu%E_+Of4mCPXt8l$|&#WuX~[q|1T|!:,8)Y6FKG=BJ|xd{EE;r<');
define('SECURE_AUTH_KEY',  '_ghH[YA?2-M,+Xpu2(*KXW~i+9n1S{0wFl;};L0_~),vojU&?|E+{H}7^;dtGjS<');
define('LOGGED_IN_KEY',    '%w6Q7W@F`Rm^Jr-QgiaMz)zm^cm8|.++825|?e* {ap {*;O1[ f++-?O&%<[LM$');
define('NONCE_KEY',        'bF-9{!j)`^@wmhuG-FTEVxz8~IRP nHujPJ*.#0QT-JDsqB`%T+-fDwC#0-SBF~1');
define('AUTH_SALT',        'BFY-6n:ej$-oE_*]ZAV]t?f81^==HHLXs^,pAIq8Hb@N#Y{F+>OFl1U)GMUo0X(v');
define('SECURE_AUTH_SALT', '6dhD54W9f;EXe&T}QioJh-Ec_-q`gUdcd~r[E^c<Y#}b<:UAc583rHlb<7z!$|@{');
define('LOGGED_IN_SALT',   'r;]?8:_`rP<h7xc1^>@G)@F$N7ez:r=x9}o[^dW@+K]++Y/x8IJ0A+yr+9%fq$1>');
define('NONCE_SALT',       'fYT/eAvr9`$rqgZdL+(ej61,Y<[=[o|*&;+v4ezYi]Wm@k|D+kM(TtnH`<W@Zp{4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'tm_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
