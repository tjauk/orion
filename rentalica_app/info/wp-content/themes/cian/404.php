<?php
/**
 * The 404 for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package _tk
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<link href="<?php echo vp_option('vpt_option.fav_ico'); ?>" rel="shortcut icon" type="image/x-icon">
<link href="<?php echo vp_option('vpt_option.fav_ico'); ?>" rel="icon" type="image/x-icon">
<?php wp_head(); ?>

<style type="text/css">
	.big-text-error{
		font-weight: bold;
		font-size: 150px;
		color: #00cccc;
		padding-top: 100px;
	}
	.text-error{
		margin-top: 60px;
	}
</style>

</head>

<body <?php body_class(); ?>>
	<?php do_action( 'before' );	
	
	require_once 'sections/navbar.php';

	get_template_part( 'no-results2', 'index' );

get_footer(); ?>