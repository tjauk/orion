<?php

//add_action('admin_init', 'remove_editor');
//add_action('add_meta_boxes','remove_my_page_metaboxes');


return array(
	'id'          => 'vp_meta_feature',
	'types'       => array('page'),
	'title'       => __('Features', 'vp_textdomain'),
	'priority'    => 'high',
	'include_template' => array('page-features.php'),
	'template'    => array(
	
		array(
			'type'      => 'group',
			'repeating' => false,

			'name'      => 'feature_sep_group',
			'title'     => __('Section title and details', 'vp_textdomain'),
			'fields'    => array(
				array(
					'type' => 'select',
					'name' => 'animation',
					'label' => __('Select the animation effect for the section', 'vp_textdomain'),
					'items' => array(
						array(
							'value' => '',
							'label' => __('None', 'vp_textdomain'),
						),
						array(
							'value' => 'bounceIn',
							'label' => __('bounceIn', 'vp_textdomain'),
						),
					    array(
					    	'value' => 'bounceInDown',
					    	'label' => __('bounceInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInLeft',
					    	'label' => __('bounceInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInRight',
					    	'label' => __('bounceInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInUp',
					    	'label' => __('bounceInUp', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeIn',
					    	'label' => __('fadeIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInDown',
					    	'label' => __('fadeInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInDownBig',
					    	'label' => __('fadeInDownBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInLeft',
					    	'label' => __('fadeInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInLeftBig',
					    	'label' => __('fadeInLeftBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInRight',
					    	'label' => __('fadeInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInRightBig',
					    	'label' => __('fadeInRightBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInUp',
					    	'label' => __('fadeInUp', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInUpBig',
					    	'label' => __('fadeInUpBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flip',
					    	'label' => __('flip', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flipInX',
					    	'label' => __('flipInX', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flipInY',
					    	'label' => __('flipInY', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'lightSpeedIn',
					    	'label' => __('lightSpeedIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateIn',
					    	'label' => __('rotateIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInDownLeft',
					    	'label' => __('rotateInDownLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInDownRight',
					    	'label' => __('rotateInDownRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInUpLeft',
					    	'label' => __('rotateInUpLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInUpRight',
					    	'label' => __('rotateInUpRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rollIn',
					    	'label' => __('rollIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomIn',
					    	'label' => __('zoomIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInDown',
					    	'label' => __('zoomInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInLeft',
					    	'label' => __('zoomInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInRight',
					    	'label' => __('zoomInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInUp',
					    	'label' => __('zoomInUp', 'vp_textdomain'),
					    ),
					),
					'default' => array(
						'',
					),
				),
				array(
					'type' => 'textbox',
					'name' => 'name_section',
					'label' => __('Section Name', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
				),
				array(
					'type' => 'textbox',
					'name' => 'title',
					'label' => __('Title', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',					
				),
				array(
					'type' => 'textarea',
					'name' => 'text_intro',
					'label' => __('Text Intro', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
				),
				array(
					'type' => 'upload',
					'name' => 'feature_image_left',
					'label' => __('Upload image left', 'vp_textdomain'),
					'description' => __('Recommend: 375 x 500', 'vp_textdomain'),
				),
				array(
					'type' => 'upload',
					'name' => 'feature_image_center',
					'label' => __('Upload image center', 'vp_textdomain'),
					'description' => __('Recommend: 500 x 1000', 'vp_textdomain'),
				),
				array(
					'type' => 'upload',
					'name' => 'feature_image_right',
					'label' => __('Upload image right', 'vp_textdomain'),
					'description' => __('Recommend: 375 x 500', 'vp_textdomain'),
				),				
			),
		),	
	
		array(
			'type'      => 'group',
			'repeating' => true,
			'length'    => 1,
			'name'      => 'feature_group',
			'title'     => __('Features', 'vp_textdomain'),
			'fields'    => array(
				array(
					'type' => 'fontawesome',
					'name' => 'fa_1',
					'label' => __('Feature Icon', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => array(
					'',
					),
				),
				array(
					'type' => 'textbox',
					'name' => 'title',
					'label' => __('Title', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
				),
				array(
					'type' => 'textarea',
					'name' => 'text',
					'label' => __('Text', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
				),					
			),
		),
		
	),
);