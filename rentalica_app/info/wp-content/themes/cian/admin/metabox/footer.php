<?php

//add_action('admin_init', 'remove_editor');
//add_action('add_meta_boxes','remove_my_page_metaboxes');


return array(
	'id'          => 'vp_meta_footer',
	'types'       => array('page'),
	'title'       => __('Footer Social Icon', 'vp_textdomain'),
	'priority'    => 'high',
	'include_template' => array('page-footer.php'),
	'template'    => array(	
		array(
			'type'      => 'group',
			'repeating' => false,

			'name'      => 'footer_sep_group',
			'title'     => __('Section details', 'vp_textdomain'),
			'fields'    => array(
				array(
					'type' => 'select',
					'name' => 'animation',
					'label' => __('Select the animation effect for the section', 'vp_textdomain'),
					'items' => array(
						array(
							'value' => '',
							'label' => __('None', 'vp_textdomain'),
						),
						array(
							'value' => 'bounceIn',
							'label' => __('bounceIn', 'vp_textdomain'),
						),
					    array(
					    	'value' => 'bounceInDown',
					    	'label' => __('bounceInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInLeft',
					    	'label' => __('bounceInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInRight',
					    	'label' => __('bounceInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInUp',
					    	'label' => __('bounceInUp', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeIn',
					    	'label' => __('fadeIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInDown',
					    	'label' => __('fadeInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInDownBig',
					    	'label' => __('fadeInDownBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInLeft',
					    	'label' => __('fadeInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInLeftBig',
					    	'label' => __('fadeInLeftBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInRight',
					    	'label' => __('fadeInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInRightBig',
					    	'label' => __('fadeInRightBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInUp',
					    	'label' => __('fadeInUp', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInUpBig',
					    	'label' => __('fadeInUpBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flip',
					    	'label' => __('flip', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flipInX',
					    	'label' => __('flipInX', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flipInY',
					    	'label' => __('flipInY', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'lightSpeedIn',
					    	'label' => __('lightSpeedIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateIn',
					    	'label' => __('rotateIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInDownLeft',
					    	'label' => __('rotateInDownLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInDownRight',
					    	'label' => __('rotateInDownRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInUpLeft',
					    	'label' => __('rotateInUpLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInUpRight',
					    	'label' => __('rotateInUpRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rollIn',
					    	'label' => __('rollIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomIn',
					    	'label' => __('zoomIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInDown',
					    	'label' => __('zoomInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInLeft',
					    	'label' => __('zoomInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInRight',
					    	'label' => __('zoomInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInUp',
					    	'label' => __('zoomInUp', 'vp_textdomain'),
					    ),
					),
					'default' => array(
						'',
					),
				),
				array(
					'type' => 'wpeditor',
					'name' => 'copyright',
					'label' => __('Footer Copyright text', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'use_external_plugins' => '1',
					'disabled_externals_plugins' => '',
					'disabled_internals_plugins' => '',
					'default' => '',
				),
			),
		),	

		array(
			'type'      => 'group',
			'repeating' => true,
			'length'    => 1,
			'name'      => 'social_icon_group',
			'title'     => __('Social Icon', 'vp_textdomain'),
			'fields'    => array(
				array(
					'type' => 'fontawesome',
					'name' => 'fa_1',
					'label' => __('Social Icon', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => array(
					'',
					),
				),
				array(
					'type' => 'textbox',
					'name' => 'tb_11',
					'label' => __('Link', 'vp_textdomain'),
					'description' => __('Valid url is required.', 'vp_textdomain'),
					'default' => '',
					'validation' => 'url',
				),				
			),
		),		
	),
);