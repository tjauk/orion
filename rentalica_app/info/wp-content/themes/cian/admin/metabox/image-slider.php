<?php

//add_action('admin_init', 'remove_editor');
//add_action('add_meta_boxes','remove_my_page_metaboxes');


return array(
	'id'          => 'vp_meta_image_slider',
	'types'       => array('page'),
	'title'       => __('Image Slider', 'vp_textdomain'),
	'priority'    => 'high',
	'include_template' => array('page-image-slider.php'),
	'template'    => array(	
		
		array(
			'type'      => 'group',
			'repeating' => false,
			'length'    => 1,
			'name'      => 'image_slider_sep_group',
			'title'     => __('Section title and details', 'vp_textdomain'),
			'fields'    => array(
				array(
					'type' => 'textbox',
					'name' => 'slide_interval',
					'label' => __('Length between transitions', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '3000',
				),
				array(
					'type' => 'select',
					'name' => 'transition',
					'label' => __('Select the transition effect', 'vp_textdomain'),
					'items' => array(
						array(
					    	'value' => '0',
					        'label' => __('None', 'vp_textdomain'),
						),
						array(
					    	'value' => '1',
					        'label' => __('Fade', 'vp_textdomain'),
						),
						array(
					    	'value' => '2',
					        'label' => __('Slide Top', 'vp_textdomain'),
						),
						array(
					    	'value' => '3',
					        'label' => __('Slide Right', 'vp_textdomain'),
						),
						array(
					    	'value' => '4',
					        'label' => __('Slide Bottom', 'vp_textdomain'),
						),
						array(
					    	'value' => '5',
					        'label' => __('Slide Left', 'vp_textdomain'),
						),
						array(
					    	'value' => '6',
					        'label' => __('Carousel Right', 'vp_textdomain'),
						),
						array(
					    	'value' => '7',
					        'label' => __('Carousel Left', 'vp_textdomain'),
						),
					),
					'default' => array(
					 	'1',
					),
				),	
			),
		),

		array(
			'type'      => 'group',
			'repeating' => true,
			'length'    => 1,
			'name'      => 'image_slider',
			'title'     => __('Image Slider', 'vp_textdomain'),
			'fields'    => array(
				array(
					'type' => 'upload',
					'name' => 'image',
					'label' => __('Upload', 'vp_textdomain'),
					'description' => __('Recommend: 1200 x 800', 'vp_textdomain'),
				),							
			),
		),
	),
);