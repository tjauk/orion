<?php

return array(
	'title' => __('&nbsp;', 'vp_textdomain'),
	'logo' => get_template_directory_uri() . '/resources/images/logo.png',
	'menus' => array(
		array(
			'title' => __('General', 'vp_textdomain'),
			'name' => 'general',
			'icon' => 'font-awesome:fa-gears',
			'controls' => array(
				array(
					'type' => 'toggle',
					'name' => 'loader_enable',					
					'label' => __('Enable/Disable', 'vp_textdomain'),
					'description' => __('Enable or disable the loader', 'vp_textdomain'),
					'default' => '1',
				),
				array(
					'type' => 'toggle',
					'name' => 'animation_enable',					
					'label' => __('Enable/Disable', 'vp_textdomain'),
					'description' => __('Enable or disable the animations in the website', 'vp_textdomain'),
					'default' => '1',
				),
				array(
					'type' => 'radiobutton',
					'name' => 'logo_type',
					'label' => __('Logo', 'vp_textdomain'),
					'items' => array(
						array(
							'value' => 'logo_img',
							'label' => __('Image', 'vp_textdomain'),
						),
						array(
							'value' => 'logo_txt',
							'label' => __('Text', 'vp_textdomain'),
						),				
					),
					'default' => array(
						'logo_txt',
					),
				),			
				array(
					'type' => 'upload',
					'name' => 'logo_m',
					'label' => __('Logo (Menu)', 'vp_textdomain'),
					'description' => __('Upload your logo', 'vp_textdomain'),
					'dependency' => array(
						'field' => 'logo_type',
						'function' => 'vp_logo_image',
					),					
				),	
				
				array(
					'type' => 'upload',
					'name' => 'logo_f',
					'label' => __('Logo (Footer)', 'vp_textdomain'),
					'description' => __('Upload your logo', 'vp_textdomain'),
					'dependency' => array(
						'field' => 'logo_type',
						'function' => 'vp_logo_image',
					),					
				),	
				
				// LOGO TEXT (MENU) *****
				array(
					'type' => 'section',
					'title' => __('Logo Text (Menu)', 'vp_textdomain'),
					'name' => 'logo_txt',
					'dependency' => array(
						'field' => 'logo_type',
						'function' => 'vp_logo_text',
					),						
					'fields' => array(
						array(
							'type' => 'html',
							'name' => 'logo_txt_m_preview',
							'binding' => array(
								'field'    => 'logo_txt_m_font_face,logo_txt_m_font_style,logo_txt_m_font_weight,logo_txt_m_font_size,logo_txt_m_font_color',
								'function' => 'vp_logo_preview_menu',
							),
						),
						array(
							'type' => 'textbox',
							'name' => 'logo_txt_m_title',
							'label' => __('Logo', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'validation' => '',
							'default' => 'Cian',
						),						
						array(
							'type' => 'select',
							'name' => 'logo_txt_m_font_face',
							'label' => __('Logo Font Face', 'vp_textdomain'),
							'description' => __('<b>Default:</b> Pacifico', 'vp_textdomain'),
							'items' => array(
								'data' => array(
									array(
										'source' => 'function',
										'value' => 'vp_get_gwf_family',
									),
								),
							),
							'default' => array(
								'Pacifico',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'logo_txt_m_font_style',
							'label' => __('Logo Font Style', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'logo_txt_m_font_face',
										'value' => 'vp_get_gwf_style',
									),
								),
							),
							'default' => array(
								'{{first}}',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'logo_txt_m_font_weight',
							'label' => __('Logo Font Weight', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'logo_txt_m_font_face',
										'value' => 'vp_get_gwf_weight',
									),
								),
							),
							'default' => array(
								'{{first}}',
							),
						),
						array(
							'type'    => 'slider',
							'name'    => 'logo_txt_m_font_size',
							'label'   => __('Logo Font Size (px)', 'vp_textdomain'),
							'description' => __('<b>Default:</b> 33px', 'vp_textdomain'),
							'min'     => '8',
							'max'     => '96',
							'default' => '33',
							'step'    => '1',
						),
						array(
							'type' => 'color',
							'name' => 'logo_txt_m_font_color',
							'label' => __('Color', 'vp_textdomain'),
							'description' => __('<b>Default:</b> #4E565C', 'vp_textdomain'),
							'default' => '#4E565C',
							'format' => 'hex',
						),
					),
				),
				
				// LOGO TEXT (FOOTER) *****
				array(
					'type' => 'section',
					'title' => __('Logo Text (Footer)', 'vp_textdomain'),
					'name' => 'logo_txt_f',
					'dependency' => array(
						'field' => 'logo_type',
						'function' => 'vp_logo_text',
					),						
					'fields' => array(
						array(
							'type' => 'html',
							'name' => 'logo_txt_f_preview',
							'binding' => array(
								'field'    => 'logo_txt_f_font_face,logo_txt_f_font_style,logo_txt_f_font_weight,logo_txt_f_font_size,logo_txt_f_font_color',
								'function' => 'vp_logo_preview_footer',
							),
						),
						array(
							'type' => 'textbox',
							'name' => 'logo_txt_f_title',
							'label' => __('Logo', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'validation' => '',
							'default' => 'Cian',
						),						
						array(
							'type' => 'select',
							'name' => 'logo_txt_f_font_face',
							'label' => __('Logo Font Face', 'vp_textdomain'),
							'description' => __('<b>Default:</b> Pacifico', 'vp_textdomain'),
							'items' => array(
								'data' => array(
									array(
										'source' => 'function',
										'value' => 'vp_get_gwf_family',
									),
								),
							),
							'default' => array(
								'Pacifico',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'logo_txt_f_font_style',
							'label' => __('Logo Font Style', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'logo_txt_f_font_face',
										'value' => 'vp_get_gwf_style',
									),
								),
							),
							'default' => array(
								'{{first}}',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'logo_txt_f_font_weight',
							'label' => __('Logo Font Weight', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'logo_txt_f_font_face',
										'value' => 'vp_get_gwf_weight',
									),
								),
							),
							'default' => array(
								'{{first}}',
							),
						),
						array(
							'type'    => 'slider',
							'name'    => 'logo_txt_f_font_size',
							'label'   => __('Logo Font Size (px)', 'vp_textdomain'),
							'description' => __('<b>Default:</b> 38px', 'vp_textdomain'),
							'min'     => '8',
							'max'     => '96',
							'default' => '38',
							'step'    => '1',
						),
						array(
							'type' => 'color',
							'name' => 'logo_txt_f_font_color',
							'label' => __('Color', 'vp_textdomain'),
							'description' => __('<b>Default:</b> #00cccc', 'vp_textdomain'),
							'default' => '#00cccc',
							'format' => 'hex',
						),
					),
				),								
				
				array(
					'type' => 'upload',
					'name' => 'fav_ico',
					'label' => __('Favicon', 'vp_textdomain'),
					'description' => __('Upload', 'vp_textdomain'),				
				),				

				array(
					'type' => 'section',
					'title' => __('Site Color Schema', 'vp_textdomain'),
					'name' => 'site_color_schema',						
					'fields' => array(
				
						array(
							'type' => 'color',
							'name' => 'primary_color',
							'label' => __('Primary color', 'vp_textdomain'),
							'description' => __('<b>Default:</b> #4E565C', 'vp_textdomain'),
							'default' => '#4E565C',
							'format' => 'hex',
						),				
						array(
							'type' => 'color',
							'name' => 'secondary_color',
							'label' => __('Secondary color', 'vp_textdomain'),
							'description' => __('<b>Default:</b> #818A90', 'vp_textdomain'),
							'default' => '#818A90',
							'format' => 'hex',
						),
						array(
							'type' => 'color',
							'name' => 'secondary_color2',
							'label' => __('Highlight color', 'vp_textdomain'),
							'description' => __('<b>Default:</b> #00cccc', 'vp_textdomain'),
							'default' => '#00cccc',
							'format' => 'hex',
						),
					),
				),
				
				array(
					'type' => 'section',
					'title' => __('Cookies message', 'vp_textdomain'),
					'name' => 'cookies_message',						
					'fields' => array(
						array(
							'type' => 'toggle',
							'name' => 'cookies_message_enable',					
							'label' => __('Enable/Disable', 'vp_textdomain'),
							'description' => __('Enable or disable the cookies message', 'vp_textdomain'),
							'default' => '0',
						),
						array(
							'type' => 'select',
							'name' => 'cookies_message_animation',
							'label' => __('Select the animation effect for the cookies message', 'vp_textdomain'),
							'items' => array(
								array(
									'value' => '',
									'label' => __('None', 'vp_textdomain'),
								),
								array(
									'value' => 'bounceIn',
									'label' => __('bounceIn', 'vp_textdomain'),
								),
							    array(
							    	'value' => 'bounceInDown',
							    	'label' => __('bounceInDown', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'bounceInLeft',
							    	'label' => __('bounceInLeft', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'bounceInRight',
							    	'label' => __('bounceInRight', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'bounceInUp',
							    	'label' => __('bounceInUp', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'fadeIn',
							    	'label' => __('fadeIn', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'fadeInDown',
							    	'label' => __('fadeInDown', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'fadeInDownBig',
							    	'label' => __('fadeInDownBig', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'fadeInLeft',
							    	'label' => __('fadeInLeft', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'fadeInLeftBig',
							    	'label' => __('fadeInLeftBig', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'fadeInRight',
							    	'label' => __('fadeInRight', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'fadeInRightBig',
							    	'label' => __('fadeInRightBig', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'fadeInUp',
							    	'label' => __('fadeInUp', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'fadeInUpBig',
							    	'label' => __('fadeInUpBig', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'flip',
							    	'label' => __('flip', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'flipInX',
							    	'label' => __('flipInX', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'flipInY',
							    	'label' => __('flipInY', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'lightSpeedIn',
							    	'label' => __('lightSpeedIn', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'rotateIn',
							    	'label' => __('rotateIn', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'rotateInDownLeft',
							    	'label' => __('rotateInDownLeft', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'rotateInDownRight',
							    	'label' => __('rotateInDownRight', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'rotateInUpLeft',
							    	'label' => __('rotateInUpLeft', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'rotateInUpRight',
							    	'label' => __('rotateInUpRight', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'rollIn',
							    	'label' => __('rollIn', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'zoomIn',
							    	'label' => __('zoomIn', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'zoomInDown',
							    	'label' => __('zoomInDown', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'zoomInLeft',
							    	'label' => __('zoomInLeft', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'zoomInRight',
							    	'label' => __('zoomInRight', 'vp_textdomain'),
							    ),
							    array(
							    	'value' => 'zoomInUp',
							    	'label' => __('zoomInUp', 'vp_textdomain'),
							    ),
							),
							'default' => array(
								'',
							),
							'dependency' => array(
								'field' => 'cookies_message_enable',
								'function' => 'vp_dep_boolean',
							),
						),
						
						array(
							'type' => 'textbox',
							'name' => 'cookies_message_title',
							'label' => __('Cookies Message Title', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'default' => '',
							'dependency' => array(
								'field' => 'cookies_message_enable',
								'function' => 'vp_dep_boolean',
							),						
						),
						array(
							'type' => 'textarea',
							'name' => 'cookies_message_text',
							'label' => __('Cookies Message Text', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'default' => '',
							'dependency' => array(
								'field' => 'cookies_message_enable',
								'function' => 'vp_dep_boolean',
							),					
						),
					),
				),
				
			),
		),
		
		array(
			'title' => __('Typography', 'vp_textdomain'),
			'name' => 'typography',
			'icon' => 'font-awesome:fa-font',
			'controls' => array(
				
				// SLIDER TITLE *****
				array(
					'type' => 'section',
					'title' => __('Slider title', 'vp_textdomain'),
					'name' => 'slider_txt',
					'fields' => array(
						array(
							'type' => 'html',
							'name' => 'slider_title_font_preview',
							'binding' => array(
								'field'    => 'slider_title_font_face,slider_title_font_style,slider_title_font_weight,slider_title_font_size,slider_title_line_height',
								'function' => 'vp_font_preview',
							),
						),
						array(
							'type' => 'select',
							'name' => 'slider_title_font_face',
							'label' => __('Slider Title Font Face', 'vp_textdomain'),
							'description' => __('<b>Default:</b> Lato', 'vp_textdomain'),
							'items' => array(
								'data' => array(
									array(
										'source' => 'function',
										'value' => 'vp_get_gwf_family',
									),
								),
							),
							'default' => array(
								'Lato',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'slider_title_font_style',
							'label' => __('Slider Title Font Style', 'vp_textdomain'),							
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'slider_title_font_face',
										'value' => 'vp_get_gwf_style',
									),
								),
							),
							'default' => array(
								'normal',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'slider_title_font_weight',
							'label' => __('Slider Title Font Weight', 'vp_textdomain'),						
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'slider_title_font_face',
										'value' => 'vp_get_gwf_weight',
									),
								),
							),
							'default' => array(
								'normal',
							),
						),
						array(
							'type'    => 'slider',
							'name'    => 'slider_title_font_size',
							'label'   => __('Slider Title Font Size', 'vp_textdomain'),
							'description' => __('<b>Default:</b> 50px', 'vp_textdomain'),
							'min'     => '8',
							'max'     => '96',
							'default' => '50',
							'step'    => '1',
						),
						array(
							'type'    => 'slider',
							'name'    => 'slider_title_line_height',
							'label'   => __('Slider Title Line Height (px)', 'vp_textdomain'),
							'description' => __('<b>Default:</b> 65px', 'vp_textdomain'),
							'min'     => '0',
							'max'     => '96',
							'default' => '65',
							'step'    => '1',
						),
					),
				),
				
				// H2 *****
				array(
					'type' => 'section',
					'title' => __('Heading H2', 'vp_textdomain'),
					'name' => 'h2_txt',
					'fields' => array(
						array(
							'type' => 'html',
							'name' => 'h2_font_preview',
							'binding' => array(
								'field'    => 'h2_font_face,h2_font_style,h2_font_weight,h2_font_size,h2_line_height',
								'function' => 'vp_font_preview',
							),
						),
						array(
							'type' => 'select',
							'name' => 'h2_font_face',
							'label' => __('Heading H2 Font Face', 'vp_textdomain'),
							'description' => __('<b>Default:</b> Lato', 'vp_textdomain'),
							'items' => array(
								'data' => array(
									array(
										'source' => 'function',
										'value' => 'vp_get_gwf_family',
									),
								),
							),
							'default' => array(
								'Lato',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'h2_font_style',
							'label' => __('Heading H2 Font Style', 'vp_textdomain'),							
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'h2_font_face',
										'value' => 'vp_get_gwf_style',
									),
								),
							),
							'default' => array(
								'normal',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'h2_font_weight',
							'label' => __('Heading H2 Font Weight', 'vp_textdomain'),						
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'h2_font_face',
										'value' => 'vp_get_gwf_weight',
									),
								),
							),
							'default' => array(
								'900',
							),
						),
						array(
							'type'    => 'slider',
							'name'    => 'h2_font_size',
							'label'   => __('Heading H2 Font Size', 'vp_textdomain'),
							'description' => __('<b>Default:</b> 30px', 'vp_textdomain'),
							'min'     => '8',
							'max'     => '96',
							'default' => '30',
							'step'    => '1',
						),
						array(
							'type'    => 'slider',
							'name'    => 'h2_line_height',
							'label'   => __('Heading H2 Line Height (px)', 'vp_textdomain'),
							'description' => __('<b>Default:</b> 36px', 'vp_textdomain'),
							'min'     => '0',
							'max'     => '96',
							'default' => '36',
							'step'    => '1',
						),
					),
				),
				
				// H2 (STRONG) *****
				array(
					'type' => 'section',
					'title' => __('Heading H2 - strong text', 'vp_textdomain'),
					'name' => 'h2_strong_txt',
					'fields' => array(
						array(
							'type' => 'html',
							'name' => 'h2_strong_font_preview',
							'binding' => array(
								'field'    => 'h2_strong_font_face,h2_strong_font_style,h2_strong_font_weight',
								'function' => 'vp_font_strong_preview',
							),
						),
						array(
							'type' => 'select',
							'name' => 'h2_strong_font_face',
							'label' => __('Heading H2 (strong) Font Face', 'vp_textdomain'),
							'description' => __('<b>Default:</b> Pacifico', 'vp_textdomain'),
							'items' => array(
								'data' => array(
									array(
										'source' => 'function',
										'value' => 'vp_get_gwf_family',
									),
								),
							),
							'default' => array(
								'Pacifico',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'h2_strong_font_style',
							'label' => __('Heading H2 (strong) Font Style', 'vp_textdomain'),							
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'h2_font_face',
										'value' => 'vp_get_gwf_style',
									),
								),
							),
							'default' => array(
								'normal',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'h2_strong_font_weight',
							'label' => __('Heading H2 (strong) Font Weight', 'vp_textdomain'),						
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'h2_font_face',
										'value' => 'vp_get_gwf_weight',
									),
								),
							),
							'default' => array(
								'900',
							),
						),
					),
				),
				
				// H2 TEXT *****
				array(
					'type' => 'section',
					'title' => __('Section Text Intro', 'vp_textdomain'),
					'name' => 'h2_p_txt',
					'fields' => array(
						array(
							'type' => 'html',
							'name' => 'h2_p_font_preview',
							'binding' => array(
								'field'    => 'h2_p_font_face,h2_p_font_style,h2_p_font_weight,h2_p_font_size,h2_p_line_height',
								'function' => 'vp_font_preview',
							),
						),
						array(
							'type' => 'select',
							'name' => 'h2_p_font_face',
							'label' => __('Section Text Intro Font Face', 'vp_textdomain'),
							'description' => __('<b>Default:</b> Lato', 'vp_textdomain'),
							'items' => array(
								'data' => array(
									array(
										'source' => 'function',
										'value' => 'vp_get_gwf_family',
									),
								),
							),
							'default' => array(
								'Lato',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'h2_p_font_style',
							'label' => __('Section Text Intro Font Style', 'vp_textdomain'),							
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'h2_p_font_face',
										'value' => 'vp_get_gwf_style',
									),
								),
							),
							'default' => array(
								'normal',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'h2_p_font_weight',
							'label' => __('Section Text Intro Font Weight', 'vp_textdomain'),						
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'h2_p_font_face',
										'value' => 'vp_get_gwf_weight',
									),
								),
							),
							'default' => array(
								'300',
							),
						),
						array(
							'type'    => 'slider',
							'name'    => 'h2_p_font_size',
							'label'   => __('Section Text Intro Font Size', 'vp_textdomain'),
							'description' => __('<b>Default:</b> 18px', 'vp_textdomain'),
							'min'     => '8',
							'max'     => '96',
							'default' => '18',
							'step'    => '1',
						),
						array(
							'type'    => 'slider',
							'name'    => 'h2_p_line_height',
							'label'   => __('Section Text Intro Line Height (px)', 'vp_textdomain'),
							'description' => __('<b>Default:</b> 27px', 'vp_textdomain'),
							'min'     => '0',
							'max'     => '96',
							'default' => '27',
							'step'    => '1',
						),
					),
				),
				
				// H3 *****
				array(
					'type' => 'section',
					'title' => __('Heading H3', 'vp_textdomain'),
					'name' => 'h3_txt',
					'fields' => array(
						array(
							'type' => 'html',
							'name' => 'h3_font_preview',
							'binding' => array(
								'field'    => 'h3_font_face,h3_font_style,h3_font_weight,h3_font_size,h3_line_height',
								'function' => 'vp_font_preview',
							),
						),
						array(
							'type' => 'select',
							'name' => 'h3_font_face',
							'label' => __('Heading H3', 'vp_textdomain'),
							'description' => __('<b>Default:</b> Lato', 'vp_textdomain'),
							'items' => array(
								'data' => array(
									array(
										'source' => 'function',
										'value' => 'vp_get_gwf_family',
									),
								),
							),
							'default' => array(
								'Lato',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'h3_font_style',
							'label' => __('Heading H3', 'vp_textdomain'),
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'h3_font_face',
										'value' => 'vp_get_gwf_style',
									),
								),
							),
							'default' => array(
								'normal',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'h3_font_weight',
							'label' => __('Heading H3', 'vp_textdomain'),						
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'h3_font_face',
										'value' => 'vp_get_gwf_weight',
									),
								),
							),
							'default' => array(
								'300',
							),
						),
						array(
							'type'    => 'slider',
							'name'    => 'h3_font_size',
							'label'   => __('Heading H3', 'vp_textdomain'),
							'description' => __('<b>Default:</b> 26px', 'vp_textdomain'),
							'min'     => '8',
							'max'     => '96',
							'default' => '26',
							'step'    => '1',
						),
						array(
							'type'    => 'slider',
							'name'    => 'h3_line_height',
							'label'   => __('Heading H3 (px)', 'vp_textdomain'),
							'description' => __('<b>Default:</b> 35px', 'vp_textdomain'),
							'min'     => '0',
							'max'     => '96',
							'default' => '35',
							'step'    => '1',
						),
					),
				),					

				// H4 *****
				array(
					'type' => 'section',
					'title' => __('Heading H4', 'vp_textdomain'),
					'name' => 'h4_txt',
					'fields' => array(
						array(
							'type' => 'html',
							'name' => 'h4_font_preview',
							'binding' => array(
								'field'    => 'h4_font_face,h4_font_style,h4_font_weight,h4_font_size,h4_line_height',
								'function' => 'vp_font_preview',
							),
						),
						array(
							'type' => 'select',
							'name' => 'h4_font_face',
							'label' => __('Heading H4 Font Face', 'vp_textdomain'),
							'description' => __('<b>Default:</b> Lato', 'vp_textdomain'),
							'items' => array(
								'data' => array(
									array(
										'source' => 'function',
										'value' => 'vp_get_gwf_family',
									),
								),
							),
							'default' => array(
								'Lato',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'h4_font_style',
							'label' => __('Heading H4 Font Style', 'vp_textdomain'),							
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'h4_font_face',
										'value' => 'vp_get_gwf_style',
									),
								),
							),
							'default' => array(
								'normal',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'h4_font_weight',
							'label' => __('Heading H4 Font Weight', 'vp_textdomain'),							
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'h4_font_face',
										'value' => 'vp_get_gwf_weight',
									),
								),
							),
							'default' => array(
								'normal',
							),
						),
						array(
							'type'    => 'slider',
							'name'    => 'h4_font_size',
							'label'   => __('Heading H4 Font Size', 'vp_textdomain'),
							'description' => __('<b>Default:</b> 18px', 'vp_textdomain'),
							'min'     => '8',
							'max'     => '96',
							'default' => '18',
							'step'    => '1',
						),
						array(
							'type'    => 'slider',
							'name'    => 'h4_line_height',
							'label'   => __('Heading H4 Line Height (px)', 'vp_textdomain'),
							'description' => __('<b>Default:</b> 18px', 'vp_textdomain'),
							'min'     => '0',
							'max'     => '96',
							'default' => '18',
							'step'    => '1',
						),
					),
				),
				
				// BODY P *****
				array(
					'type' => 'section',
					'title' => __('Body text (p)', 'vp_textdomain'),
					'name' => 'p_txt',
					'fields' => array(
						array(
							'type' => 'html',
							'name' => 'p_font_preview',
							'binding' => array(
								'field'    => 'p_font_face,p_font_style,p_font_weight,p_font_size,p_line_height',
								'function' => 'vp_font_preview',
							),
						),
						array(
							'type' => 'select',
							'name' => 'p_font_face',
							'label' => __('Body Font Face', 'vp_textdomain'),
							'description' => __('<b>Default:</b> Lato', 'vp_textdomain'),
							'items' => array(
								'data' => array(
									array(
										'source' => 'function',
										'value' => 'vp_get_gwf_family',
									),
								),
							),
							'default' => array(
								'Lato',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'p_font_style',
							'label' => __('Body Font Style', 'vp_textdomain'),							
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'p_font_face',
										'value' => 'vp_get_gwf_style',
									),
								),
							),
							'default' => array(
								'normal',
							),
						),
						array(
							'type' => 'radiobutton',
							'name' => 'p_font_weight',
							'label' => __('Body Font Weight', 'vp_textdomain'),							
							'items' => array(
								'data' => array(
									array(
										'source' => 'binding',
										'field' => 'p_font_face',
										'value' => 'vp_get_gwf_weight',
									),
								),
							),
							'default' => array(
								'300',
							),
						),
						array(
							'type'    => 'slider',
							'name'    => 'p_font_size',
							'label'   => __('Body Font Size', 'vp_textdomain'),
							'description' => __('<b>Default:</b> 15px', 'vp_textdomain'),
							'min'     => '8',
							'max'     => '96',
							'default' => '15',
							'step'    => '1',
						),
						array(
							'type'    => 'slider',
							'name'    => 'p_line_height',
							'label'   => __('Body Line Height (px)', 'vp_textdomain'),
							'description' => __('<b>Default:</b> 22px', 'vp_textdomain'),
							'min'     => '0',
							'max'     => '96',
							'default' => '22',
							'step'    => '1',
						),
					),
				),				
	
				
			),
		),	
	
		array(
			'title' => __('Sections', 'vp_textdomain'),
			'name' => 'sections',
			'icon' => 'font-awesome:fa-folder-o',
			'controls' => array(
			
				 array(
					'type' => 'notebox',
					'name' => 'nb_1',
					'label' => __('Please select a page for the sections.', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'status' => 'info',
				),			
			
				array(
					'type' => 'select',
					'name' => 'header_section_page',
					'label' => __('Header', 'vp_textdomain'),
					'items' => array(
						'data' => array(
							array(
								'source' => 'function',
								'value' => 'vp_get_pages_header',
							),
						),
					),
				),					
				array(
					'type' => 'select',
					'name' => 'feature_section_page',
					'label' => __('Features', 'vp_textdomain'),
					'items' => array(
						'data' => array(
							array(
								'source' => 'function',
								'value' => 'vp_get_pages_features',
							),
						),
					),
				),		
				array(
					'type' => 'select',
					'name' => 'spotlight_1_section_page',
					'label' => __('Spotlight 1', 'vp_textdomain'),
					'items' => array(
						'data' => array(
							array(
								'source' => 'function',
								'value' => 'vp_get_pages_spotlight_1',
							),
						),
					),
				),
				array(
					'type' => 'select',
					'name' => 'spotlight_2_section_page',
					'label' => __('Spotlight 2', 'vp_textdomain'),
					'items' => array(
						'data' => array(
							array(
								'source' => 'function',
								'value' => 'vp_get_pages_spotlight_2',
							),
						),
					),
				),
				array(
					'type' => 'select',
					'name' => 'clients_section_page',
					'label' => __('Clients', 'vp_textdomain'),					
					'items' => array(
						'data' => array(
							array(
								'source' => 'function',
								'value' => 'vp_get_pages_clients',
							),
						),
					),
				),				
				array(
					'type' => 'select',
					'name' => 'prices_section_page',
					'label' => __('Prices', 'vp_textdomain'),					
					'items' => array(
						'data' => array(
							array(
								'source' => 'function',
								'value' => 'vp_get_pages_prices',
							),
						),
					),
				),								
				array(
					'type' => 'select',
					'name' => 'footer_secion_page',
					'label' => __('Footer', 'vp_textdomain'),					
					'items' => array(
						'data' => array(
							array(
								'source' => 'function',
								'value' => 'vp_get_pages_footer',
							),
						),
					),
				),
			),
		),
		
		array(
			'title' => __('Background', 'vp_textdomain'),
			'name' => 'background',
			'icon' => 'font-awesome:fa-desktop',
			'controls' => array(
				
				array(
					'type' => 'section',
					'title' => __('IMAGE SLIDER BACKGROUND', 'vp_textdomain'),
					'name' => 'section_image_background',							
					'fields' => array(
						 array(
							'type' => 'notebox',
							'name' => 'nb_2',
							'label' => __('If you choose the image slider background, you have to create a new page with the template \'Image Slider\' to upload the images you want.', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'status' => 'info',
						),
						array(
							'type' => 'select',
							'name' => 'image_slider_section_page',
							'label' => __('Choose the page', 'vp_textdomain'),					
							'items' => array(
								'data' => array(
									array(
										'source' => 'function',
										'value' => 'vp_get_pages_image_slider',
									),
								),
							),
						),						
					),
				),
				
				array(
					'type' => 'section',
					'title' => __('VIDEO OWN BACKGROUND', 'vp_textdomain'),
					'name' => 'section_video_own',						
					'fields' => array(
						array(
							'type' => 'upload',
							'name' => 'video_own',
							'label' => __('Video', 'vp_textdomain'),
							'description' => __('Upload your video', 'vp_textdomain'),					
						),
						array(
							'type' => 'toggle',
							'name' => 'video_own_sound',					
							'label' => __('Enable/Disable', 'vp_textdomain'),
							'description' => __('Enable or disable the video sound', 'vp_textdomain'),
							'default' => '1',
						),				
					),
				),
				
				array(
					'type' => 'section',
					'title' => __('VIDEO FROM YOUTUBE', 'vp_textdomain'),
					'name' => 'section_video_youtube',							
					'fields' => array(
						array(
							'type' => 'textbox',
							'name' => 'video_youtube',
							'label' => __('Video from Youtube', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'default' => '',						
						),
						array(
							'type' => 'toggle',
							'name' => 'video_youtube_sound',					
							'label' => __('Enable/Disable', 'vp_textdomain'),
							'description' => __('Enable or disable the video sound', 'vp_textdomain'),
							'default' => '1',
						),
						array(
					        'type' => 'select',
					        'name' => 'video_youtube_quality',
					        'label' => __('Select the quality', 'vp_textdomain'),
					        'items' => array(
					            array(
					                'value' => 'small',
					                'label' => __('Small', 'vp_textdomain'),
					            ),
					            array(
					                'value' => 'medium',
					                'label' => __('Medium', 'vp_textdomain'),
					            ),
					            array(
					                'value' => 'large',
					                'label' => __('Large', 'vp_textdomain'),
					            ),
					            array(
					                'value' => 'hd720',
					                'label' => __('hd720', 'vp_textdomain'),
					            ),
					            array(
					                'value' => 'hd1080',
					                'label' => __('hd1080', 'vp_textdomain'),
					            ),
					            array(
					                'value' => 'highres',
					                'label' => __('highres', 'vp_textdomain'),
					            ),
					        ),
					        'default' => array(
					            'small',
					        ),
					    ),					
					),
				),
				
				array(
					'type' => 'section',
					'title' => __('VIDEO FROM VIMEO', 'vp_textdomain'),
					'name' => 'section_video_vimeo',						
					'fields' => array(
						array(
							'type' => 'textbox',
							'name' => 'video_vimeo',
							'label' => __('Video ID from Vimeo', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'default' => '',						
						),
						array(
							'type' => 'toggle',
							'name' => 'video_vimeo_sound',					
							'label' => __('Enable/Disable', 'vp_textdomain'),
							'description' => __('Enable or disable the video sound', 'vp_textdomain'),
							'default' => '1',
						),				
					),
				),
				
				array(
					'type' => 'section',
					'title' => __('IMAGE REPLACEMENT', 'vp_textdomain'),
					'name' => 'section_image_replacement',							
					'fields' => array(
						array(
							'type' => 'notebox',
							'name' => 'nb_3',
							'label' => __('With the video background there are a lot of problems in mobile and tablet devices. In this cases, the template can change the video by an image. If you want this feature, you have to enable the next option and upload the image replacement.', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'status' => 'info',
						),	
						array(
							'type' => 'toggle',
							'name' => 'video_image_replacement_enable',					
							'label' => __('Enable/Disable', 'vp_textdomain'),
							'description' => __('Enable or disable if you want to use an image replacement', 'vp_textdomain'),
							'default' => '1',
						),
						array(
							'type' => 'upload',
							'name' => 'video_image_replacement',
							'label' => __('Image replacement', 'vp_textdomain'),
							'description' => __('Upload the image replacement', 'vp_textdomain'),					
						),
					),
				),
				array(
					'type' => 'section',
					'title' => __('IMAGES PARALLAX', 'vp_textdomain'),
					'name' => 'section_image_parallax',							
					'fields' => array(
						array(
							'type' => 'upload',
							'name' => 'parallax_image_homepage',
							'label' => __('Homepage Image', 'vp_textdomain'),
							'description' => __('Upload the image for the homepage section', 'vp_textdomain'),					
						),
						array(
							'type' => 'upload',
							'name' => 'parallax_image_subscription',
							'label' => __('Subscription Image', 'vp_textdomain'),
							'description' => __('Upload the image for the subcription section', 'vp_textdomain'),					
						),
						array(
							'type' => 'upload',
							'name' => 'parallax_image_clients',
							'label' => __('Clients Image', 'vp_textdomain'),
							'description' => __('Upload the image for the clients section', 'vp_textdomain'),					
						),
						array(
							'type' => 'upload',
							'name' => 'parallax_image_twitter',
							'label' => __('Twitter Image', 'vp_textdomain'),
							'description' => __('Upload the image for the twitter section', 'vp_textdomain'),					
						),
					),
				),
			),
		),
		
		array(
			'title' => __('Newsletter', 'vp_textdomain'),
			'name' => 'subscription',
			'icon' => 'font-awesome:fa-envelope-o',
			'controls' => array(
				array(
					'type' => 'toggle',
					'name' => 'subs_enable',					
					'label' => __('Enable/Disable', 'vp_textdomain'),
					'description' => __('Mailchimp Subscription', 'vp_textdomain'),
					'default' => '1',
				),
				array(
					'type' => 'select',
					'name' => 'subscription_animation',
					'label' => __('Select the animation effect for the section', 'vp_textdomain'),
					'items' => array(
						array(
							'value' => '',
							'label' => __('None', 'vp_textdomain'),
						),
						array(
							'value' => 'bounceIn',
							'label' => __('bounceIn', 'vp_textdomain'),
						),
					    array(
					    	'value' => 'bounceInDown',
					    	'label' => __('bounceInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInLeft',
					    	'label' => __('bounceInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInRight',
					    	'label' => __('bounceInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInUp',
					    	'label' => __('bounceInUp', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeIn',
					    	'label' => __('fadeIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInDown',
					    	'label' => __('fadeInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInDownBig',
					    	'label' => __('fadeInDownBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInLeft',
					    	'label' => __('fadeInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInLeftBig',
					    	'label' => __('fadeInLeftBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInRight',
					    	'label' => __('fadeInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInRightBig',
					    	'label' => __('fadeInRightBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInUp',
					    	'label' => __('fadeInUp', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInUpBig',
					    	'label' => __('fadeInUpBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flip',
					    	'label' => __('flip', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flipInX',
					    	'label' => __('flipInX', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flipInY',
					    	'label' => __('flipInY', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'lightSpeedIn',
					    	'label' => __('lightSpeedIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateIn',
					    	'label' => __('rotateIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInDownLeft',
					    	'label' => __('rotateInDownLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInDownRight',
					    	'label' => __('rotateInDownRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInUpLeft',
					    	'label' => __('rotateInUpLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInUpRight',
					    	'label' => __('rotateInUpRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rollIn',
					    	'label' => __('rollIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomIn',
					    	'label' => __('zoomIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInDown',
					    	'label' => __('zoomInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInLeft',
					    	'label' => __('zoomInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInRight',
					    	'label' => __('zoomInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInUp',
					    	'label' => __('zoomInUp', 'vp_textdomain'),
					    ),
					),
					'default' => array(
						'',
					),
					'dependency' => array(
						'field' => 'subs_enable',
						'function' => 'vp_dep_boolean',
					),
				),
				array(
					'type' => 'textbox',
					'name' => 'subs_name_section',
					'label' => __('Section Name', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
					'dependency' => array(
						'field' => 'subs_enable',
						'function' => 'vp_dep_boolean',
					),
				),
				array(
					'type' => 'textbox',
					'name' => 'subs_title',
					'label' => __('Section Title', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
					'dependency' => array(
						'field' => 'subs_enable',
						'function' => 'vp_dep_boolean',
					),						
				),
				array(
					'type' => 'textarea',
					'name' => 'subs_text_intro',
					'label' => __('Section Text Intro', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
					'dependency' => array(
						'field' => 'subs_enable',
						'function' => 'vp_dep_boolean',
					),						
				),
				array(
					'type' => 'section',
					'title' => __('MAILCHIMP CODE', 'vp_textdomain'),
					'name' => 'section_mailchimp',
					'dependency' => array(
						'field' => 'subs_enable',
						'function' => 'vp_dep_boolean',
					),						
					'fields' => array(
						array(
							'type' => 'notebox',
							'name' => 'nb_4',
							'label' => __('Mailchimp configuration', 'vp_textdomain'),
							'description' => __('You have to get the values url form, u and id, from your mailchimp signup form. If you don\'t know how to get these values, follow <a href="http://kb.mailchimp.com/lists/signup-forms/host-your-own-signup-forms">this tutorial</a>. The values are got in the step 8.', 'vp_textdomain'),
							'status' => 'info',
						),						
						array(
							'type' => 'textbox',
							'name' => 'your_urlForm',
							'label' => __('Type the variable urlForm', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'default' => '',
						),
						array(
							'type' => 'textbox',
							'name' => 'your_u',
							'label' => __('Type the variable u', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'default' => '',
						),
						array(
							'type' => 'textbox',
							'name' => 'your_id',
							'label' => __('Type the variable id', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'default' => '',
						),
					),
				),				
			),
		),
		
		array(
			'title' => __('Portfolio', 'vp_textdomain'),
			'name' => 'portfolio',
			'icon' => 'font-awesome:fa-th',
			'controls' => array(
				array(
					'type' => 'toggle',
					'name' => 'portfolio_enable',					
					'label' => __('Enable/Disable', 'vp_textdomain'),
					'description' => __('Portfolio Section', 'vp_textdomain'),
					'default' => '1',
				),
				array(
					'type' => 'select',
					'name' => 'portfolio_animation',
					'label' => __('Select the animation effect for the section', 'vp_textdomain'),
					'items' => array(
						array(
							'value' => '',
							'label' => __('None', 'vp_textdomain'),
						),
						array(
							'value' => 'bounceIn',
							'label' => __('bounceIn', 'vp_textdomain'),
						),
					    array(
					    	'value' => 'bounceInDown',
					    	'label' => __('bounceInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInLeft',
					    	'label' => __('bounceInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInRight',
					    	'label' => __('bounceInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInUp',
					    	'label' => __('bounceInUp', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeIn',
					    	'label' => __('fadeIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInDown',
					    	'label' => __('fadeInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInDownBig',
					    	'label' => __('fadeInDownBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInLeft',
					    	'label' => __('fadeInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInLeftBig',
					    	'label' => __('fadeInLeftBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInRight',
					    	'label' => __('fadeInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInRightBig',
					    	'label' => __('fadeInRightBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInUp',
					    	'label' => __('fadeInUp', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInUpBig',
					    	'label' => __('fadeInUpBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flip',
					    	'label' => __('flip', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flipInX',
					    	'label' => __('flipInX', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flipInY',
					    	'label' => __('flipInY', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'lightSpeedIn',
					    	'label' => __('lightSpeedIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateIn',
					    	'label' => __('rotateIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInDownLeft',
					    	'label' => __('rotateInDownLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInDownRight',
					    	'label' => __('rotateInDownRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInUpLeft',
					    	'label' => __('rotateInUpLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInUpRight',
					    	'label' => __('rotateInUpRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rollIn',
					    	'label' => __('rollIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomIn',
					    	'label' => __('zoomIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInDown',
					    	'label' => __('zoomInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInLeft',
					    	'label' => __('zoomInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInRight',
					    	'label' => __('zoomInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInUp',
					    	'label' => __('zoomInUp', 'vp_textdomain'),
					    ),
					),
					'default' => array(
						'',
					),
				),
				array(
					'type' => 'select',
					'name' => 'portfolio_style',
					'label' => __('Select the portfolio\'s style', 'vp_textdomain'),
					'items' => array(
						array(
							'value' => 'grid',
							'label' => __('Grid', 'vp_textdomain'),
						),
						array(
					    	'value' => 'expanded_grid',
					    	'label' => __('Expanded Grid', 'vp_textdomain'),
					    ),
					),
					'default' => array(
						'grid',
					),
				),
				array(
					'type' => 'textbox',
					'name' => 'portfolio_name_section',
					'label' => __('Section Name', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
					'dependency' => array(
						'field' => 'portfolio_enable',
						'function' => 'vp_dep_boolean',
					),
				),
				array(
					'type' => 'textbox',
					'name' => 'portfolio_title',
					'label' => __('Section Title', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
					'dependency' => array(
						'field' => 'portfolio_enable',
						'function' => 'vp_dep_boolean',
					),						
				),
				array(
					'type' => 'textarea',
					'name' => 'portfolio_text_intro',
					'label' => __('Section Text Intro', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
					'dependency' => array(
						'field' => 'portfolio_enable',
						'function' => 'vp_dep_boolean',
					),						
				),		
			),
		),

		array(
			'title' => __('Blog', 'vp_textdomain'),
			'name' => 'blog',
			'icon' => 'font-awesome:fa-comments',
			'controls' => array(
				array(
					'type' => 'toggle',
					'name' => 'blog_enable',					
					'label' => __('Enable/Disable', 'vp_textdomain'),
					'description' => __('Blog Section', 'vp_textdomain'),
					'default' => '1',
				),
				array(
					'type'    => 'slider',
					'name'    => 'blog_number_posts',
					'label'   => __('Number posts', 'vp_textdomain'),
					'description' => __('<b>Default:</b> 3', 'vp_textdomain'),
					'min'     => '1',
					'max'     => '9',
					'default' => '3',
					'step'    => '1',
				),
				array(
					'type' => 'select',
					'name' => 'blog_animation',
					'label' => __('Select the animation effect for the section', 'vp_textdomain'),
					'items' => array(
						array(
							'value' => '',
							'label' => __('None', 'vp_textdomain'),
						),
						array(
							'value' => 'bounceIn',
							'label' => __('bounceIn', 'vp_textdomain'),
						),
					    array(
					    	'value' => 'bounceInDown',
					    	'label' => __('bounceInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInLeft',
					    	'label' => __('bounceInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInRight',
					    	'label' => __('bounceInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInUp',
					    	'label' => __('bounceInUp', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeIn',
					    	'label' => __('fadeIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInDown',
					    	'label' => __('fadeInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInDownBig',
					    	'label' => __('fadeInDownBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInLeft',
					    	'label' => __('fadeInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInLeftBig',
					    	'label' => __('fadeInLeftBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInRight',
					    	'label' => __('fadeInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInRightBig',
					    	'label' => __('fadeInRightBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInUp',
					    	'label' => __('fadeInUp', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInUpBig',
					    	'label' => __('fadeInUpBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flip',
					    	'label' => __('flip', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flipInX',
					    	'label' => __('flipInX', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flipInY',
					    	'label' => __('flipInY', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'lightSpeedIn',
					    	'label' => __('lightSpeedIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateIn',
					    	'label' => __('rotateIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInDownLeft',
					    	'label' => __('rotateInDownLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInDownRight',
					    	'label' => __('rotateInDownRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInUpLeft',
					    	'label' => __('rotateInUpLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInUpRight',
					    	'label' => __('rotateInUpRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rollIn',
					    	'label' => __('rollIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomIn',
					    	'label' => __('zoomIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInDown',
					    	'label' => __('zoomInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInLeft',
					    	'label' => __('zoomInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInRight',
					    	'label' => __('zoomInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInUp',
					    	'label' => __('zoomInUp', 'vp_textdomain'),
					    ),
					),
					'default' => array(
						'',
					),
				),
				array(
					'type' => 'textbox',
					'name' => 'blog_name_section',
					'label' => __('Section Name', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
					'dependency' => array(
						'field' => 'blog_enable',
						'function' => 'vp_dep_boolean',
					),
				),
				array(
					'type' => 'textbox',
					'name' => 'blog_title',
					'label' => __('Section Title', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
					'dependency' => array(
						'field' => 'blog_enable',
						'function' => 'vp_dep_boolean',
					),						
				),
				array(
					'type' => 'textarea',
					'name' => 'blog_text_intro',
					'label' => __('Section Text Intro', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
					'dependency' => array(
						'field' => 'blog_enable',
						'function' => 'vp_dep_boolean',
					),						
				),
				array(
					'type' => 'section',
					'title' => __('MORE POSTS BUTTON', 'vp_textdomain'),
					'name' => 'section_more_posts_button',
					'dependency' => array(
						'field' => 'blog_enable',
						'function' => 'vp_dep_boolean',
					),						
					'fields' => array(
						array(
							'type' => 'toggle',
							'name' => 'more_posts_button_enable',					
							'label' => __('More posts button', 'vp_textdomain'),
							'description' => __('Enable/Disable', 'vp_textdomain'),
							'default' => '1',
						),
						array(
							'type' => 'textbox',
							'name' => 'more_posts_button_text',
							'label' => __('Text', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'default' => '',
						),
						array(
							'type' => 'textbox',
							'name' => 'more_posts_button_link',
							'label' => __('Link', 'vp_textdomain'),
							'description' => __('', 'vp_textdomain'),
							'default' => '',
						),
					),
				),	
				array(
					'type' => 'section',
					'title' => __('SHARE BUTTONS', 'vp_textdomain'),
					'name' => 'section_buttons_share',
					'dependency' => array(
						'field' => 'blog_enable',
						'function' => 'vp_dep_boolean',
					),						
					'fields' => array(
						array(
							'type' => 'toggle',
							'name' => 'twitter_share_enable',					
							'label' => __('Twitter button', 'vp_textdomain'),
							'description' => __('Enable/Disable', 'vp_textdomain'),
							'default' => '1',
						),
						array(
							'type' => 'toggle',
							'name' => 'facebook_share_enable',					
							'label' => __('Facebook button', 'vp_textdomain'),
							'description' => __('Enable/Disable', 'vp_textdomain'),
							'default' => '1',
						),
						array(
							'type' => 'toggle',
							'name' => 'google_share_enable',					
							'label' => __('Google Plus button', 'vp_textdomain'),
							'description' => __('Enable/Disable', 'vp_textdomain'),
							'default' => '1',
						),
					),
				),			
			),
		),
		
		
		array(
			'title' => __('Twitter', 'vp_textdomain'),
			'name' => 'twitter',
			'icon' => 'font-awesome:fa-twitter',
			'controls' => array(
				array(
					'type' => 'toggle',
					'name' => 'twitter_enable',					
					'label' => __('Enable/Disable', 'vp_textdomain'),
					'description' => __('Twitter', 'vp_textdomain'),
					'default' => '1',
				),
				array(
					'type' => 'select',
					'name' => 'twitter_animation',
					'label' => __('Select the animation effect for the section', 'vp_textdomain'),
					'items' => array(
						array(
							'value' => '',
							'label' => __('None', 'vp_textdomain'),
						),
						array(
							'value' => 'bounceIn',
							'label' => __('bounceIn', 'vp_textdomain'),
						),
					    array(
					    	'value' => 'bounceInDown',
					    	'label' => __('bounceInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInLeft',
					    	'label' => __('bounceInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInRight',
					    	'label' => __('bounceInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInUp',
					    	'label' => __('bounceInUp', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeIn',
					    	'label' => __('fadeIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInDown',
					    	'label' => __('fadeInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInDownBig',
					    	'label' => __('fadeInDownBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInLeft',
					    	'label' => __('fadeInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInLeftBig',
					    	'label' => __('fadeInLeftBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInRight',
					    	'label' => __('fadeInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInRightBig',
					    	'label' => __('fadeInRightBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInUp',
					    	'label' => __('fadeInUp', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInUpBig',
					    	'label' => __('fadeInUpBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flip',
					    	'label' => __('flip', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flipInX',
					    	'label' => __('flipInX', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flipInY',
					    	'label' => __('flipInY', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'lightSpeedIn',
					    	'label' => __('lightSpeedIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateIn',
					    	'label' => __('rotateIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInDownLeft',
					    	'label' => __('rotateInDownLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInDownRight',
					    	'label' => __('rotateInDownRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInUpLeft',
					    	'label' => __('rotateInUpLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInUpRight',
					    	'label' => __('rotateInUpRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rollIn',
					    	'label' => __('rollIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomIn',
					    	'label' => __('zoomIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInDown',
					    	'label' => __('zoomInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInLeft',
					    	'label' => __('zoomInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInRight',
					    	'label' => __('zoomInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInUp',
					    	'label' => __('zoomInUp', 'vp_textdomain'),
					    ),
					),
					'default' => array(
						'',
					),
				),
				array(
					'type' => 'textbox',
					'name' => 'twitter_shortcode',
					'label' => __('Twitter Rotate Shortcode', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
					'dependency' => array(
							'field' => 'twitter_enable',
							'function' => 'vp_dep_boolean',
					),
				),		
			),
		),
		
		array(
			'title' => __('Contact form', 'vp_textdomain'),
			'name' => 'contact',
			'icon' => 'font-awesome:fa-mail-reply-all',
			'controls' => array(
				array(
					'type' => 'toggle',
					'name' => 'contact_enable',					
					'label' => __('Enable/Disable', 'vp_textdomain'),
					'description' => __('Contact form', 'vp_textdomain'),
					'default' => '1',
				),
				array(
					'type' => 'select',
					'name' => 'contact_animation',
					'label' => __('Select the animation effect for the section', 'vp_textdomain'),
					'items' => array(
						array(
							'value' => '',
							'label' => __('None', 'vp_textdomain'),
						),
						array(
							'value' => 'bounceIn',
							'label' => __('bounceIn', 'vp_textdomain'),
						),
					    array(
					    	'value' => 'bounceInDown',
					    	'label' => __('bounceInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInLeft',
					    	'label' => __('bounceInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInRight',
					    	'label' => __('bounceInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'bounceInUp',
					    	'label' => __('bounceInUp', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeIn',
					    	'label' => __('fadeIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInDown',
					    	'label' => __('fadeInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInDownBig',
					    	'label' => __('fadeInDownBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInLeft',
					    	'label' => __('fadeInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInLeftBig',
					    	'label' => __('fadeInLeftBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInRight',
					    	'label' => __('fadeInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInRightBig',
					    	'label' => __('fadeInRightBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInUp',
					    	'label' => __('fadeInUp', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'fadeInUpBig',
					    	'label' => __('fadeInUpBig', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flip',
					    	'label' => __('flip', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flipInX',
					    	'label' => __('flipInX', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'flipInY',
					    	'label' => __('flipInY', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'lightSpeedIn',
					    	'label' => __('lightSpeedIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateIn',
					    	'label' => __('rotateIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInDownLeft',
					    	'label' => __('rotateInDownLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInDownRight',
					    	'label' => __('rotateInDownRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInUpLeft',
					    	'label' => __('rotateInUpLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rotateInUpRight',
					    	'label' => __('rotateInUpRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'rollIn',
					    	'label' => __('rollIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomIn',
					    	'label' => __('zoomIn', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInDown',
					    	'label' => __('zoomInDown', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInLeft',
					    	'label' => __('zoomInLeft', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInRight',
					    	'label' => __('zoomInRight', 'vp_textdomain'),
					    ),
					    array(
					    	'value' => 'zoomInUp',
					    	'label' => __('zoomInUp', 'vp_textdomain'),
					    ),
					),
					'default' => array(
						'',
					),
				),
				array(
					'type' => 'textbox',
					'name' => 'contact_name_section',
					'label' => __('Section Name', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
					'dependency' => array(
						'field' => 'contact_enable',
						'function' => 'vp_dep_boolean',
					),
				),
				array(
					'type' => 'textbox',
					'name' => 'contact_title',
					'label' => __('Section Title', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
					'dependency' => array(
						'field' => 'contact_enable',
						'function' => 'vp_dep_boolean',
					),
				),					
				array(
					'type' => 'textarea',
					'name' => 'contact_text_intro',
					'label' => __('Section Text Intro', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
					'dependency' => array(
						'field' => 'contact_enable',
						'function' => 'vp_dep_boolean',
					),						
				),											
				array(
					'type' => 'textbox',
					'name' => 'cf7_shortcode',
					'label' => __('Contact Form 7 Shortcode', 'vp_textdomain'),
					'description' => __('', 'vp_textdomain'),
					'default' => '',
					'dependency' => array(
						'field' => 'contact_enable',
						'function' => 'vp_dep_boolean',
					),
				),
			),
		),
		
		
		array(
			'title' => __('Custom Scripts', 'vp_textdomain'),
			'name' => 'menu_scripts',
			'icon' => 'font-awesome:fa-code',
			'controls' => array(
				array(
					'type' => 'section',
					'title' => __('Document Scripts', 'vp_textdomain'),
					'fields' => array(
						array(
							'type' => 'codeeditor',
							'name' => 'ce_css',
							'label' => __('Custom CSS', 'vp_textdomain'),
							'description' => __('Write your custom css here.', 'vp_textdomain'),
							'theme' => 'eclipse',
							'mode' => 'css',
						),
						array(
							'type' => 'codeeditor',
							'name' => 'ce_js',
							'label' => __('Custom JS', 'vp_textdomain'),
							'description' => __('Write your custom js here.', 'vp_textdomain'),
							'theme' => 'twilight',
							'mode' => 'javascript',
						),						
					),
				),	
			),
		),
	),
);