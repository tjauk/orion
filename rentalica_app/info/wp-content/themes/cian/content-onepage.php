<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package _tk
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="clearfix"><br/><br/></div>
	<div class="container">
		<div class="row">
			<div class="entry-content">
				<?php the_content(); ?>
				<?php		
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', '_tk' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- .entry-content -->
			<?php edit_post_link( __( 'Edit Page', '_tk' ), '<footer class="entry-meta entry-meta-onepage animate" data-animate="fadeIn"><span class="edit-link edit-link-onepage">', '</span></footer>' ); ?>
		</div>
	</div>
</article><!-- #post-## -->
