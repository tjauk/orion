<?php
/**
 * @package _tk
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<?php
	
	$format = get_post_format( $post->ID );

	switch ($format) {
		
		case 'gallery': ?> 
		
		<?php $images = get_post_meta( get_the_ID(), '_format_gallery_images', true ); ?>
		<?php if($images) : ?>
			<div class="post-image">
				<ul class="bxslider">
				<?php foreach($images as $image) : ?>
				
					<?php $the_image = wp_get_attachment_image_src( $image, 'full' ); ?> 
					<?php $the_caption = get_post_field('post_excerpt', $image); ?>
					<li><img src="<?php echo $the_image[0]; ?>" <?php if($the_caption) : ?>title="<?php echo $the_caption; ?>"<?php endif; ?> /></li>
					
				<?php endforeach; ?>
				</ul>
			</div>
		<?php endif;
		
			break;
		
		case 'video': ?>
	
		<div class="post-image">
			<?php $video = get_post_meta( get_the_ID(), '_format_video_embed', true ); ?>
			<?php if(wp_oembed_get( $video )) : ?>
				<?php echo wp_oembed_get($video); ?>
			<?php else : ?>
				<?php echo $video; ?>
			<?php endif; ?>
		</div>
		
			<?php 
			break;
		
		case 'audio': ?>
	
		<div class="post-image audio">
			<?php $audio = get_post_meta( get_the_ID(), '_format_audio_embed', true ); ?>
			<?php if(wp_oembed_get( $audio )) : ?>
				<?php echo wp_oembed_get($audio); ?>
			<?php else : ?>
				<?php echo $audio; ?>
			<?php endif; ?>
		</div>
	
			<?php 
			break;
		
		default: ?>
		
		<?php if(has_post_thumbnail()) : ?>
			<div class="post-image">
				<a href="<?php echo get_permalink() ?>"><?php the_post_thumbnail(); ?></a>
			</div>
		<?php endif; ?>
		
	<?php } ?>
	
		
	<div class="clearfix"></div>

	<div class="post-content text-edit">
		<h3 class="page-title"><?php the_title(); ?></h3>

	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .post-content -->

	<footer class="entry-meta">
		<?php
			_cian_share_post();
			
			_tk_posted_on();
		
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', '_tk' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', '_tk' ) );

			if ( ! _tk_categorized_blog() ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( '<span class="categories-meta">Categories: %2$s</span><br/><br/>', '_tk' );
				} 

			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list ) {
					$meta_text = __( '<span class="categories-meta"><strong>Category:</strong> %1$s</span> <span class="tags-meta"><strong>Tags:</strong> %2$s</span><br/><br/>', '_tk' );
				} else {
					$meta_text = __( '<span class="categories-meta"><strong>Category:</strong> %1$s</span><br/><br/>', '_tk' );
				}

			} // end check for categories on this blog

			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink(),
				the_title_attribute( 'echo=0' )
			);
		?>

		<?php edit_post_link( __( 'Edit post', '_tk' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->
<hr>
<div class="clearfix"></div>