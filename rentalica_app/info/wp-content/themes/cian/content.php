<?php
/**
 * @package _tk
 */
?>


<?php // Styling Tip! 

// Want to wrap for example the post content in blog listings with a thin outline in Bootstrap style?
// Just add the class "panel" to the article tag here that starts below. 
// Simply replace post_class() with post_class('panel') and check your site!   
// Remember to do this for all content templates you want to have this, 
// for example content-single.php for the post single view. ?>

<article class="col-md-4 col-sm-4" id="post-<?php the_ID(); ?>">

 	<?php 
	if (vp_option('vpt_option.animation_enable') == 1) {
		echo '<div class="blog-post animate" data-animate="'.vp_option('vpt_option.blog_animation').'">';	
	} else {
		echo '<div class="blog-post">';	
	}
	echo'
		<div class="blog-post-thumb">
		'.get_the_post_thumbnail( get_the_ID(), 'medium' ).'	
		</div>
		<div class="blog-post-content">
			<h3><a href="'.get_the_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3>
			<p>';
			$excerpt = wp_trim_excerpt(get_the_content()); // $excerpt contains the excerpt of the concerned post
        	$excerpt=substr($excerpt,0,150)."...";
			echo $excerpt;
			echo '</p>
			<div class="blog-post-footer">
				<ul>
					<li><p><span class="fa fa-calendar"></span>'.get_the_date("j F Y").'</p></li>';
					$number_comments = wp_count_comments(get_the_ID());
					echo'<li><p><span class="fa fa-comments-o"></span>'.$number_comments->total_comments.'</p></li>
				</ul>
				<div class="blog-post-plus">
					<a href="'.get_the_permalink().'" class="fa fa-plus-circle" ></a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>';
	?>
        									
</article><!-- #post-## -->
