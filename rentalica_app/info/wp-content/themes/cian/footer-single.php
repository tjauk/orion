<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _tk
 */

	wp_footer();

	
	if ( is_page_template("page-background-image-slider.php") ){ ?>
		<script type="text/javascript">
			<?php require(get_template_directory().'/sections/images.php'); ?>
		</script>	
	<?php }
	
	if ( is_page_template("page-background-video-own.php") ){ ?>
		<script type="text/javascript">
			<?php 
			if(vp_option('vpt_option.video_image_replacement_enable') == 1){
				require(get_template_directory().'/sections/video.php');
			} else {
				require(get_template_directory().'/sections/video_no_image_replacemet.php');
			}?>
		</script>	
	<?php }
	
	if ( is_page_template("page-background-video-youtube.php") ){ ?>
		<?php
		if ( vp_option('vpt_option.video_youtube_sound') == '1' ){
			$sound = 'false';
		} else {
			$sound = 'true';
		}
		?>
		<a id="bgndVideo" data-property="{videoURL:'<?php echo esc_url(vp_option('vpt_option.video_youtube')) ?>',containment:'body',autoPlay:true, mute:<?php echo esc_js($sound)?>, startAt:0, opacity:1, ratio:'4/3', addRaster:true, quality:'<?php echo vp_option('vpt_option.video_youtube_quality'); ?>'}">My video</a>
		<script type="text/javascript">
			<?php 
			if(vp_option('vpt_option.video_image_replacement_enable') == '1'){
				require(get_template_directory().'/sections/video_youtube.php');
			} else {
				require(get_template_directory().'/sections/video_youtube_no_image_replacemet.php');
			}?>
		</script>	
	<?php }
	
	
	if ( is_page_template("page-background-video-vimeo.php") ){ ?>
		<div id="fullscreen-vimeo"></div>
		<script type="text/javascript">
			<?php 
			if(vp_option('vpt_option.video_image_replacement_enable') == 1){
				require(get_template_directory().'/sections/video_vimeo.php');
			} else {
				require(get_template_directory().'/sections/video_vimeo_no_image_replacemet.php');
			}
			require(get_template_directory().'/sections/vimeo_script.php');
			?>
		</script>	
	<?php }
	
	
	if ( is_page_template("page-background-parallax.php") ){ ?>
		<script type="text/javascript">
			<?php require(get_template_directory().'/sections/parallax.php'); ?>
		</script>	
	<?php } 

	
	$loader_enable = vp_option('vpt_option.loader_enable');
	if ($loader_enable == 1){
	?>
		<script type="text/javascript">
		jQuery(window).load(function() {
			"use strict";
		    /* ==============================================
		    PRELOADER
		    =============================================== */
		    var preloaderDelay = 800;
		    var preloaderFadeOutTime = 1000;

		    function hidePreloader() {
		        var loadingAnimation = jQuery('#loading-animation');
		        var preloader = jQuery('.main');

		        loadingAnimation.fadeOut();
		        preloader.delay(preloaderDelay).fadeOut(preloaderFadeOutTime, function() {
		        	jQuery('.animate').waypoint(function() {
		        	     var animation = jQuery(this).attr("data-animate");
		        	     jQuery(this).addClass(animation);
		        	     jQuery(this).addClass('animated');
		        	}, { offset: '80%' }); 
		         });
		     
		    }
		    
		    hidePreloader();
		    
		});
		</script>
	<?php 
	} else {
	?>
		<script type="text/javascript">
		jQuery(window).load(function() {
			jQuery('.animate').waypoint(function() {
       	     var animation = jQuery(this).attr("data-animate");
       	     jQuery(this).addClass(animation);
       	     jQuery(this).addClass('animated');
       	}, { offset: '80%' }); 
		});
		</script>
	<?php 	
	}
	?>
	
	
	<script type="text/javascript">
		<?php echo vp_option('vpt_option.ce_js'); 
		
		if ( vp_option('vpt_option.subs_enable') == 1){
		?>
			var urlForm = '<?php echo vp_option('vpt_option.your_urlForm')?>';
			var u = '<?php echo vp_option('vpt_option.your_u')?>';
			var id = '<?php echo vp_option('vpt_option.your_id')?>';
		    jQuery('#mc-form').ajaxChimp({
		    	url: urlForm+'?u='+u+'&amp;id='+id
			});
		<?php }
		
		if ( vp_option('vpt_option.cookies_message_enable') == 1){
		?>
			jQuery(".close-cookies").click(function() {
				jQuery("#cookies-message").fadeOut();
			});

		<?php } ?>

		jQuery('.navbar-nav > li.scrollTo > a').bind('click', function(e) {
		    e.preventDefault();
		    var target = this.hash;
		    jQuery.scrollTo(target, 1250, {
		    	easing: 'swing',
		    	axis: 'y',
		    	offset: -45
		    });
		});

		jQuery('.navbar-brand').bind('click', function(e) {
		    e.preventDefault();
		    var target = this.hash;
		    jQuery.scrollTo(0, 1250, {
		    	easing: 'swing',
		    	axis: 'y'
		    });
		});
		
		jQuery(function() {
			var bar = jQuery('nav');
			var top = bar.css('top');
			var ww = jQuery(window).width();
			var navigationHeight = -jQuery('.collapse').height();
			var mobileTop = Math.floor(navigationHeight - 60);
			
			jQuery(window).scroll(function() {
					if(jQuery(this).scrollTop() > 310) {
							bar.stop().animate({top : '0'}, 300);
					} else {
							if(ww < 768) {
									bar.stop().animate({top : mobileTop}, 600);
							} else {
									bar.stop().animate({top : top}, 300);
							}
					}  
			});
		});
		
		jQuery(document).ready(function(){	
			
		<?php 
		$sectionPageID = vp_option('vpt_option.clients_section_page');	
		if ($sectionPageID != ""){ ?>
			
			/* ==============================================
		    TESTIMONIALS SLIDER
		    =============================================== */
			jQuery("#testimonials-slides").owlCarousel({
				navigation : true, // Show next and prev buttons
				slideSpeed : 600,
				paginationSpeed : 800,
				singleItem:true,
				items : 1,
				navigationText : ["<span class='testimonial-icon fa fa-chevron-circle-left'></span>", "<span class='testimonial-icon fa fa-chevron-circle-right' ></span>"],
				pagination : false
			});
			
		<?php 
		}
		
		$sectionPageID = vp_option('vpt_option.portfolio_enable');	
		if ( vp_option('vpt_option.portfolio_enable') == 1 ){ 
			switch ( vp_option('vpt_option.portfolio_style') ) {
				case 'grid':
			?>
					/* ==============================================
				    /* GALLERY - GRID
					================================================== */
					jQuery('#Grid').mixitup();
					jQuery('.portfolio-popup').magnificPopup({
						type: 'image',
						removalDelay: 500, //delay removal by X to allow out-animation
						callbacks: {
						beforeOpen: function() {
							   this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
							   this.st.mainClass = 'mfp-zoom-in';
							}
						},
						closeOnContentClick: true,
						fixedContentPos: false
					});
					jQuery('.portfolio-video').magnificPopup({
						disableOn: 700,
				        type: 'iframe',
				        mainClass: 'mfp-fade',
				        removalDelay: 160,
				        preloader: false,
				        fixedContentPos: false
					});
					jQuery('.portfolio-gallery').magnificPopup({
						type: 'image',
						removalDelay: 500, //delay removal by X to allow out-animation
						callbacks: {
						beforeOpen: function() {
							   this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
							   this.st.mainClass = 'mfp-zoom-in';
							}
						},
						gallery: {
							enabled: true 
						},
						closeOnContentClick: true,
						fixedContentPos: false
					});
				<?php 
				break;
				case 'expanded_grid':
				?>
					/* ==============================================
				    /* GALLERY - STYLE 2
					================================================== */
						jQuery('#og-grid').mixitup();
						Grid.init();
				<?php 
				break;
				default:
					echo '';
			}
		}
		
		if ( function_exists('icl_object_id') ) { 
		?>
			if ( jQuery('.submenu-languages').length ) {
				jQuery('.menu-item-language a:first').append(' <span class="caret"></span>');
			}
			jQuery('.menu-item-language a:first').toggle(function(ev) {
	        	ev.stopPropagation();
	        	jQuery('.submenu-languages').show();
	        }, function() {
	        	jQuery('.submenu-languages').hide();
	        });
	        jQuery('html').click(function() {
	        	jQuery('.submenu-languages').hide();
	        });
		<?php 
		}
		?>
		
		});
		
	</script>
	

</body>
</html>
