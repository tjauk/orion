<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _tk
 */

	wp_footer();
	
	$content = get_the_content();
	$string_gallery = "add_gallery";
	$string_newsletter = "subscription";
	$result_gallery = strpos($content, $string_gallery);
	$result_newsletter = strpos($content, $string_newsletter);
			
	$loader_enable = vp_option('vpt_option.loader_enable');
	if ($loader_enable == 1){
	?>
		<script type="text/javascript">
		jQuery(window).load(function() {
			"use strict";
		    /* ==============================================
		    PRELOADER
		    =============================================== */
		    var preloaderDelay = 800;
		    var preloaderFadeOutTime = 1000;

		    function hidePreloader() {
		        var loadingAnimation = jQuery('#loading-animation');
		        var preloader = jQuery('.main');

		        loadingAnimation.fadeOut();
		        preloader.delay(preloaderDelay).fadeOut(preloaderFadeOutTime, function() {
		        	jQuery('.animate').waypoint(function() {
		        	     var animation = jQuery(this).attr("data-animate");
		        	     jQuery(this).addClass(animation);
		        	     jQuery(this).addClass('animated');
		        	}, { offset: '80%' }); 
		         });
		     
		    }
		    
		    hidePreloader();
		    
		});
		</script>
	<?php 
	} else {
	?>
		<script type="text/javascript">
		jQuery(window).load(function() {
			jQuery('.animate').waypoint(function() {
       	     var animation = jQuery(this).attr("data-animate");
       	     jQuery(this).addClass(animation);
       	     jQuery(this).addClass('animated');
       	}, { offset: '80%' }); 
		});
		</script>
	<?php 	
	}
	?>
	
	<script type="text/javascript">
		
		jQuery(document).ready(function(){

		<?php
		$sectionPageID = vp_option('vpt_option.subs_enable');	
		if ( (vp_option('vpt_option.subs_enable') == 1) && ($result_newsletter !== FALSE)){
		?>

			var urlForm = '<?php echo vp_option('vpt_option.your_urlForm')?>';
			var u = '<?php echo vp_option('vpt_option.your_u')?>';
			var id = '<?php echo vp_option('vpt_option.your_id')?>';
		    jQuery('#mc-form').ajaxChimp({
		    	url: urlForm+'?u='+u+'&amp;id='+id
			});
		
		<?php  
		}
		?>
						
		<?php
		$sectionPageID = vp_option('vpt_option.portfolio_enable');	
		if (($sectionPageID != "") && ($result_gallery !== FALSE)){
		?>
		
			/* ==============================================
		    /* GALLERY
			================================================== */
			jQuery('#Grid').mixitup();

			jQuery('.portfolio-popup').magnificPopup({
				type: 'image',
				removalDelay: 500, //delay removal by X to allow out-animation
				callbacks: {
				beforeOpen: function() {
					   this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
					   this.st.mainClass = 'mfp-zoom-in';
					}
				},
				closeOnContentClick: true,
				fixedContentPos: false
			});

			jQuery('.portfolio-video').magnificPopup({
				disableOn: 700,
		        type: 'iframe',
		        mainClass: 'mfp-fade',
		        removalDelay: 160,
		        preloader: false,
		        fixedContentPos: false
			});
			jQuery('.portfolio-gallery').magnificPopup({
				type: 'image',
				removalDelay: 500, //delay removal by X to allow out-animation
				callbacks: {
				beforeOpen: function() {
					   this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
					   this.st.mainClass = 'mfp-zoom-in';
					}
				},
				gallery: {
					enabled: true 
				},
				closeOnContentClick: true,
				fixedContentPos: false
			});
			
			jQuery('.gallery-images > div ').each( function() { jQuery(this).hoverdir(); } );

		<?php 
		}
		
		if ( function_exists('icl_object_id') ) {
		?>
			if ( jQuery('.submenu-languages').length ) {
				jQuery('.menu-item-language a:first').append(' <span class="caret"></span>');
			}
			jQuery('.menu-item-language a:first').toggle(function(ev) {
	        	ev.stopPropagation();
	        	jQuery('.submenu-languages').show();
	        }, function() {
	        	jQuery('.submenu-languages').hide();
	        });
	        jQuery('html').click(function() {
	        	jQuery('.submenu-languages').hide();
	        });
		<?php 
		}
		?>

		});
		
		<?php echo vp_option('vpt_option.ce_js'); ?>
	</script>
	
	
</body>
</html>	
	
