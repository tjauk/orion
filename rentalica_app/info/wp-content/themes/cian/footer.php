<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _tk
 */
?>
			</div> <!-- END ROW -->
		</div> <!-- END CONTAINER -->	
	</section> <!-- END BLOG SECTION -->

<?php

	$tmpl_mb0  = get_template_directory() . '/admin/metabox/footer.php';
	$mb0 = new VP_Metabox($tmpl_mb0);

	$sectionPageID = vp_option('vpt_option.footer_secion_page');
?>

	<footer id="footer-blog">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h1 class="logo">
					<?php							
						if (vp_option('vpt_option.logo_type') == 'logo_img'){
							echo '<img src="'.esc_url(vp_option('vpt_option.logo_f')).'" alt="logo">';
						}
						else if (vp_option('vpt_option.logo_type') == 'logo_txt'){
							echo vp_option('vpt_option.logo_txt_f_title');
						}
					?>	
					</h1>
					<p class="text-copyright"><?php echo vp_metabox('vp_meta_footer.footer_sep_group.0.copyright', false, $sectionPageID) ?></p>
				</div>
				<div class="col-md-6">
					<ul class="footer-social">
						<?php 
							$social_icons = vp_metabox('vp_meta_footer.social_icon_group', false, $sectionPageID);
							if (is_array($social_icons)){
								foreach ($social_icons as $social_icon) {				
									echo 	'<li>
												<a href="'.esc_url($social_icon['tb_11']) .'">
													<div class="fa '.esc_attr($social_icon['fa_1']).'"></div>
												</a>
											</li>';
								}
							}
						?>
					</ul>
				</div>
			</div>
		</div>
	</footer> <!-- END FOOTER SECTIONS -->

	<?php 
	$loader_enable = vp_option('vpt_option.loader_enable');
	if ($loader_enable == 1){
	?>
		<script type="text/javascript">
		jQuery(window).load(function() {
			"use strict";
		    /* ==============================================
		    PRELOADER
		    =============================================== */
		    var preloaderDelay = 800;
		    var preloaderFadeOutTime = 1000;

		    function hidePreloader() {
		        var loadingAnimation = jQuery('#loading-animation');
		        var preloader = jQuery('.main');

		        loadingAnimation.fadeOut();
		        preloader.delay(preloaderDelay).fadeOut(preloaderFadeOutTime, function() {
		        	jQuery('.animate').waypoint(function() {
		        	     var animation = jQuery(this).attr("data-animate");
		        	     jQuery(this).addClass(animation);
		        	     jQuery(this).addClass('animated');
		        	}, { offset: '80%' }); 
		         });
		     
		    }
		    
		    hidePreloader();
		    
		});
		</script>
	<?php 
	} else {
	?>
		<script type="text/javascript">
		jQuery(window).load(function() {
			jQuery('.animate').waypoint(function() {
       	     var animation = jQuery(this).attr("data-animate");
       	     jQuery(this).addClass(animation);
       	     jQuery(this).addClass('animated');
       	}, { offset: '80%' }); 
		});
		</script>
	<?php 	
	}
	
	if ( function_exists('icl_object_id') ) { 
	?>
		<script type="text/javascript">
		if ( jQuery('.submenu-languages').length ) {
			jQuery('.menu-item-language a:first').append(' <span class="caret"></span>');
		}
		jQuery('.menu-item-language a:first').toggle(function(ev) {
        	ev.stopPropagation();
        	jQuery('.submenu-languages').show();
        }, function() {
        	jQuery('.submenu-languages').hide();
        });
        jQuery('html').click(function() {
        	jQuery('.submenu-languages').hide();
        });
	   	</script>
	<?php 
	}
	?>
	<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery(".bxslider").bxSlider({
        	adaptiveHeight: true,
        	mode: "fade",
        	pager: !1,
        	captions: false
   		});
	});
	</script>

<script>
<?php echo vp_option('vpt_option.ce_js'); ?>
</script>

<?php wp_footer(); ?>
</body>
</html>