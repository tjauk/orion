<?php
/**
 * _tk functions and definitions
 *
 * @package _tk
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 750; /* pixels */

if ( ! function_exists( '_tk_setup' ) ) :
/**
 * Set up theme defaults and register support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function _tk_setup() {
    global $cap, $content_width;

    // This theme styles the visual editor with editor-style.css to match the theme style.
    add_editor_style();

    if ( function_exists( 'add_theme_support' ) ) {

		/**
		 * Add default posts and comments RSS feed links to head
		*/
		add_theme_support( 'automatic-feed-links' );
		
		/**
		 * Enable support for Post Thumbnails on posts and pages
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		*/
		add_theme_support( 'post-thumbnails' );
		
		/**
		 * Setup the WordPress core custom background feature.
		*/
		add_theme_support( 'custom-background', apply_filters( '_tk_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
			) ) 
		);
		
		// Post formats
		add_theme_support( 'post-formats', array( 'gallery', 'video', 'audio' ) );
	
    }

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on _tk, use a find and replace
	 * to change '_tk' to the name of your theme in all the template files
	*/
	load_theme_textdomain( '_tk', get_template_directory() . '/languages' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	*/ 
    register_nav_menus( array(
        'primary'  => __( 'Main menu', '_tk' ),
    ) );

}
endif; // _tk_setup
add_action( 'after_setup_theme', '_tk_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function _tk_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', '_tk' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', '_tk_widgets_init' );


/**
 * Enqueue scripts and styles
 */
function _tk_scripts() {

    // load bootstrap css
    wp_enqueue_style( '_tk-bootstrap', get_template_directory_uri() . '/includes/resources/bootstrap/css/bootstrap.css' );

    // load _tk styles
    wp_enqueue_style( '_tk-style', get_stylesheet_uri() );

    // load bootstrap js
    wp_enqueue_script('_tk-bootstrapjs', get_template_directory_uri().'/includes/resources/bootstrap/js/bootstrap.js', array('jquery') );

    // load bootstrap wp js
    wp_enqueue_script( '_tk-bootstrapwp', get_template_directory_uri() . '/includes/js/bootstrap-wp.js', array('jquery') );

    wp_enqueue_script( '_tk-skip-link-focus-fix', get_template_directory_uri() . '/includes/js/skip-link-focus-fix.js', array(), '20130115', true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

    if ( is_singular() && wp_attachment_is_image() ) {
        wp_enqueue_script( '_tk-keyboard-image-navigation', get_template_directory_uri() . '/includes/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
    }

}
add_action( 'wp_enqueue_scripts', '_tk_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/includes/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/includes/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/includes/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/includes/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/includes/jetpack.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/includes/bootstrap-wp-navwalker.php';


/*
	VAFPRESS
 */

/**
 * Load Languages
 */
add_action('after_setup_theme', 'vp_tb_load_textdomain');

function vp_tb_load_textdomain()
{
	load_theme_textdomain('vp_textdomain', get_template_directory() . '/lang/');
}

/**
 * Include Vafpress Framework
 */
require_once 'vafpress-framework/bootstrap.php';

/**
 * Include Custom Data Sources
 */
require_once 'admin/data_sources.php';

/**
 * Load options, metaboxes, and shortcode generator array templates.
 */

// options
	$tmpl_opt  = get_template_directory() . '/admin/option/option.php';

// metaboxes 
	
	$tmpl_mb8  = get_template_directory() . '/admin/metabox/image-slider.php';
	$mb8 = new VP_Metabox($tmpl_mb8);
	
	$tmpl_mb7  = get_template_directory() . '/admin/metabox/spotlight-1.php';
	$mb7 = new VP_Metabox($tmpl_mb7); 
	
    $tmpl_mb6  = get_template_directory() . '/admin/metabox/header.php';
	$mb6 = new VP_Metabox($tmpl_mb6); 		

    $tmpl_mb5  = get_template_directory() . '/admin/metabox/features.php';
	$mb5 = new VP_Metabox($tmpl_mb5); 
  	
    $tmpl_mb4  = get_template_directory() . '/admin/metabox/spotlight-2.php';
	$mb4 = new VP_Metabox($tmpl_mb4);		
	
    $tmpl_mb3  = get_template_directory() . '/admin/metabox/clients.php';
	$mb3 = new VP_Metabox($tmpl_mb3);		;		

    $tmpl_mb2  = get_template_directory() . '/admin/metabox/prices.php';
	$mb2 = new VP_Metabox($tmpl_mb2);		
  
    $tmpl_mb1  = get_template_directory() . '/admin/metabox/footer.php';
	$mb1 = new VP_Metabox($tmpl_mb1);	
  
  
/**
 * Create instance of Options
 */
$theme_options = new VP_Option(array(
	'is_dev_mode'           => false,                                  // dev mode, default to false
	'option_key'            => 'vpt_option',                           // options key in db, required
	'page_slug'             => 'vpt_option',                           // options page slug, required
	'template'              => $tmpl_opt,                              // template file path or array, required
	'menu_page'             => 'themes.php',                           // parent menu slug or supply `array` (can contains 'icon_url' & 'position') for top level menu
	'use_auto_group_naming' => true,                                   // default to true
	'use_util_menu'         => true,                                   // default to true, shows utility menu
	'minimum_role'          => 'edit_theme_options',                   // default to 'edit_theme_options'
	'layout'                => 'fixed',                                // fluid or fixed, default to fixed
	'page_title'            => __( 'Theme Options', 'vp_textdomain' ), // page title
	'menu_label'            => __( 'Theme Options', 'vp_textdomain' ), // menu label
));

/* Sections */
require_once 'sections/header.php';
require_once 'sections/features.php';
require_once 'sections/cookies_message.php';
require_once 'sections/subscription.php';
require_once 'sections/spotlight-1.php';
require_once 'sections/spotlight-2.php';
require_once 'sections/portfolio.php';
require_once 'sections/clients.php';
require_once 'sections/prices.php';
require_once 'sections/blog.php';
require_once 'sections/twitter.php';
require_once 'sections/contact.php';


/*
 * EOF
 */
// embeding theme css and js

function cian_enqueue_style() {
	wp_enqueue_style( '_twl-fontawesome-icons-css', get_template_directory_uri() .'/resources/lib/font-awesome/css/font-awesome.min.css' ); 
	require_once 'resources/lib/Mobile-Detect/Mobile_Detect.php';
	$detect = new Mobile_Detect;	
	$msie = strpos($_SERVER["HTTP_USER_AGENT"], 'MSIE') ? true : false;
	if ( !$detect->isMobile() and !$msie) {
		wp_enqueue_style( '_twl-uikit', get_template_directory_uri() .'/resources/lib/uikit/css/uikit.min.css' );
	}
	$loader_enable = vp_option('vpt_option.loader_enable');
	if ( !$detect->isMobile() && !$msie && $loader_enable == 1){
		wp_enqueue_style( '_twl-preloader-css', get_template_directory_uri() . '/resources/css/preloader.css' );
	}
	wp_enqueue_style( '_twl-bxslider', get_template_directory_uri() . '/resources/css/jquery.bxslider.css' );
	wp_enqueue_style( '_twl-css', get_template_directory_uri() . '/resources/css/style.css' );
	wp_enqueue_style( '_twl-responsive', get_template_directory_uri() . '/resources/css/responsive.css' );
	wp_enqueue_style('dynamic-css', admin_url('admin-ajax.php').'?action=dynamic_css');
	if ( !$detect->isMobile() && !$msie && (vp_option('vpt_option.animation_enable') == 1) ) {
		wp_enqueue_style( '_twl-animate-css', get_template_directory_uri() . '/resources/css/animate.css' );	
	}
	if(is_page())
	{ 
		global $wp_query;
        $template_name = get_post_meta( $wp_query->post->ID, '_wp_page_template', true );
			if($template_name == 'page-background-image-slider.php'){
	   			wp_enqueue_style( '_supersized_css_01', get_template_directory_uri() . '/resources/lib/supersized/css/supersized.css' );	
			}
			if($template_name == 'page-background-video-own.php'){
	   			wp_enqueue_style( '_video_own_css_01', get_template_directory_uri() . '/resources/lib/bigvideo/css/bigvideo.css' );
			}
   	}
   	$portfolio_enable = vp_option('vpt_option.portfolio_enable');
	if ( ($portfolio_enable == 1) && is_page() && ( vp_option('vpt_option.portfolio_style') == 'grid') )
	{			
		wp_enqueue_style( '_cian_portfolio_css_01', get_template_directory_uri() .'/resources/lib/magnific-popup/magnific-popup.css' );
	}
	$sectionPageID = vp_option('vpt_option.clients_section_page');
	if ( ($sectionPageID != "") && is_page())
	{			
		wp_enqueue_style( '_cian_carousel_css_01', get_template_directory_uri() . '/resources/lib/owl-carousel/owl.carousel.css' );
	}
}

function cian_enqueue_script() {
	require_once 'resources/lib/Mobile-Detect/Mobile_Detect.php';
	$detect = new Mobile_Detect;	
	$msie = strpos($_SERVER["HTTP_USER_AGENT"], 'MSIE') ? true : false;
	if ( !$detect->isMobile() and !$msie) {
		wp_enqueue_script( '_twl-uikit-js', get_template_directory_uri() .'/resources/lib/uikit/js/uikit.min.js', array(), '1.0.0', true );
	}	
	wp_enqueue_script( '_twl-easing-js', get_template_directory_uri() .'/resources/js/jquery.easing.1.3.js', array(), '1.0.0', true );
	wp_enqueue_script( '_twl-scrollTo-js', get_template_directory_uri() .'/resources/js/jquery.scrollTo.min.js', array(), '1.0.0', true );	
	wp_enqueue_script( '_twl-waypoints-js', get_template_directory_uri() .'/resources/js/waypoints.min.js', array(), '1.0.0', true );
	wp_enqueue_script( '_twl-modernizr-js', get_template_directory_uri() .'/resources/js/modernizr.js', array(), '1.0.0', true );
	wp_enqueue_script( '_bxslider', get_template_directory_uri() .'/resources/js/jquery.bxslider.min.js', array(), '1.0.0', true );
	wp_enqueue_script( '_twl-footer-js', get_template_directory_uri() .'/resources/js/custom.js', array(), '1.0.0', true );
	if(is_page())
	{ 
		global $wp_query;
        $template_name = get_post_meta( $wp_query->post->ID, '_wp_page_template', true );
			if($template_name == 'page-background-image-slider.php'){
	   			wp_enqueue_script( '_supersized_js_01', get_template_directory_uri() .'/resources/lib/supersized/js/supersized.3.2.7.min.js', array(), '1.0.0', true );
			}
			if($template_name == 'page-background-video-own.php'){
	   			wp_enqueue_script( '_video_own_js_01', get_template_directory_uri() .'/resources/lib/bigvideo/js/jquery-ui-1.8.22.custom.min.js', array(), '1.0.0', true );
	   			wp_enqueue_script( '_video_own_js_02', get_template_directory_uri() .'/resources/lib/bigvideo/js/jquery.imagesloaded.min.js', array(), '1.0.0', true );
	   			wp_enqueue_script( '_video_own_js_03', get_template_directory_uri() .'/resources/lib/bigvideo/js/video.js', array(), '1.0.0', true );
	   			wp_enqueue_script( '_video_own_js_04', get_template_directory_uri() .'/resources/lib/bigvideo/js/bigvideo.js', array(), '1.0.0', true );
			}
			if($template_name == 'page-background-video-youtube.php'){
	   			wp_enqueue_script( '_video_youtube_js_01', get_template_directory_uri() .'/resources/js/jquery.mb.YTPlayer.js', array(), '1.0.0', true );
			}
			if($template_name == 'page-background-parallax.php'){
	   			wp_enqueue_script( '_parallax_js_01', get_template_directory_uri() .'/resources/lib/parallax/jquery.parallax.min.js', array(), '1.0.0', true );
			}
   	}
	$portfolio_enable = vp_option('vpt_option.portfolio_enable');
	if ( ($portfolio_enable == 1) && is_page())
	{
		switch ( vp_option('vpt_option.portfolio_style') ) {
			case 'grid':
				wp_enqueue_script( '_cian_portfolio_js_01', get_template_directory_uri() .'/resources/lib/magnific-popup/jquery.magnific-popup.min.js', array(), '1.0.0', true );
				wp_enqueue_script( '_cian_portfolio_js_03', get_template_directory_uri() .'/resources/lib/mixitup/jquery.mixitup.min.js', array(), '1.0.0', true );
			break;
			case 'expanded_grid':
				wp_enqueue_script( '_cian_portfolio_js_03', get_template_directory_uri() .'/resources/lib/mixitup/jquery.mixitup.min.js', array(), '1.0.0', true );
				wp_enqueue_script( '_cian_portfolio_js_04', get_template_directory_uri() .'/resources/js/grid.js', array(), '1.0.0', true );
			break;
			default:
				echo '';	
		}
	}
	$subs_enable = vp_option('vpt_option.subs_enable');
	if ( ($subs_enable == 1) && is_page())
	{			
		wp_enqueue_script( '_cian_ajaxchimp_js_01', get_template_directory_uri() .'/resources/lib/ajaxchimp/jquery.ajaxchimp.js', array(), '1.0.0', true );
	}
	$sectionPageID = vp_option('vpt_option.clients_section_page');
	if ( ($sectionPageID != "") && is_page())
	{			
		wp_enqueue_script( '_cian_carousel_js_01', get_template_directory_uri() .'/resources/lib/owl-carousel/owl.carousel.min.js', array(), '1.0.0', true );
	}
}

add_action( 'wp_enqueue_scripts', 'cian_enqueue_style' );
add_action( 'wp_enqueue_scripts', 'cian_enqueue_script' );


// color code hex to rgb
function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}

function dynaminc_css() {
  require(get_template_directory().'/sections/custom_css_js.php');
  exit;
}
add_action('wp_ajax_dynamic_css', 'dynaminc_css');
add_action('wp_ajax_nopriv_dynamic_css', 'dynaminc_css');


function _cian_share_post(){
	$title = urlencode( get_the_title() );
	$url = urlencode( get_permalink() );
	
	if ( (vp_option('vpt_option.twitter_share_enable') == 1) || (vp_option('vpt_option.facebook_share_enable') == 1) || (vp_option('vpt_option.google_share_enable') == 1) )
	{
		echo '<div class="share-post"><strong>Share:</strong><span class="space-buttons"></span>';
		if (vp_option('vpt_option.twitter_share_enable') == 1)
		{
			echo '<a class="share-twitter" rel="external nofollow" href="http://twitter.com/intent/tweet/?text='.$title.'&url='.$url.'" title="" target="_blank"><span class="fa fa-twitter"></span></a>';
		}
		if (vp_option('vpt_option.facebook_share_enable') == 1)
		{
			echo '<a class="share-facebook" rel="external nofollow" href="http://www.facebook.com/sharer/sharer.php?s=100&p[url]='.$url.'&p[title]='.$title.'" title="" target="_blank"><span class="fa fa-facebook"></span></a>';
		}
		if (vp_option('vpt_option.google_share_enable') == 1)
		{
			echo '<a class="share-google-plus" rel="external nofollow" href="https://plus.google.com/share?url='.$url.'" title="" target="_blank"><span class="fa fa-google-plus"></span></a>';
		}
		echo '</div>';
	}	
	
}


// embeding google font
// add the font
$font_face = vp_option('vpt_option.logo_txt_m_font_face');
$font_weight = vp_option('vpt_option.logo_txt_m_font_style');
$font_style = vp_option('vpt_option.logo_txt_m_font_weight');
VP_Site_GoogleWebFont::instance()->add($font_face, $font_weight, $font_style);

$font_face = vp_option('vpt_option.logo_txt_f_font_face');
$font_weight = vp_option('vpt_option.logo_txt_f_font_style');
$font_style = vp_option('vpt_option.logo_txt_f_font_weight');
VP_Site_GoogleWebFont::instance()->add($font_face, $font_weight, $font_style);

$font_face = vp_option('vpt_option.slider_title_font_face');
$font_weight = vp_option('vpt_option.slider_title_font_style');
$font_style = vp_option('vpt_option.slider_title_font_weight');
VP_Site_GoogleWebFont::instance()->add($font_face, $font_weight, $font_style);

$font_face = vp_option('vpt_option.h2_font_face');
$font_weight = vp_option('vpt_option.h2_font_weight');
$font_style = vp_option('vpt_option.h2_font_style');
VP_Site_GoogleWebFont::instance()->add($font_face, $font_weight, $font_style);

$font_face = vp_option('vpt_option.h2_strong_font_face');
$font_weight = vp_option('vpt_option.h2_strong_font_style');
$font_style = vp_option('vpt_option.h2_strong_font_weight');
VP_Site_GoogleWebFont::instance()->add($font_face, $font_weight, $font_style);

$font_face = vp_option('vpt_option.h2_p_font_face');
$font_weight = vp_option('vpt_option.h2_p_font_style');
$font_style = vp_option('vpt_option.h2_p_font_weight');
VP_Site_GoogleWebFont::instance()->add($font_face, $font_weight, $font_style);

$font_face = vp_option('vpt_option.h3_font_face');
$font_weight = vp_option('vpt_option.h3_font_weight');
$font_style = vp_option('vpt_option.h3_font_style');
VP_Site_GoogleWebFont::instance()->add($font_face, $font_weight, $font_style);

$font_face = vp_option('vpt_option.h4_font_face');
$font_weight = vp_option('vpt_option.h4_font_weight');
$font_style = vp_option('vpt_option.h4_font_style');
VP_Site_GoogleWebFont::instance()->add($font_face, $font_weight, $font_style);

$font_face = vp_option('vpt_option.p_font_face');
$font_weight = vp_option('vpt_option.p_font_weight');
$font_style = vp_option('vpt_option.p_font_style');
VP_Site_GoogleWebFont::instance()->add($font_face, $font_weight, $font_style);


function get_carousel( $atts ){
   $carousel = '';
   
   // get attibutes and set defaults
        extract(shortcode_atts(array(
                'ids' => '0'
       ), $atts));
    // Display info 
    
    $ids_count = explode(",", $ids);
    $carouselLenght = count($ids_count);
    
   	$carousel .= '
   	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
	';
   	
   	for ( $i = 0 ; $i < $carouselLenght ; $i++ ) {
   		if( $i == 0){
   			$carousel .= '<li data-target="#myCarousel" data-slide-to="'.$i.'" class="active"></li>';	
   		} else {
   			$carousel .= '<li data-target="#myCarousel" data-slide-to="'.$i.'"></li>';
   		}
   	}
   	
	$carousel .= '
		</ol>
		<div class="carousel-inner">
	';
	
	for ( $i = 0 ; $i < $carouselLenght ; $i++ ) {
   		if( $i == 0){
   			$carousel .= '<div class="item active"><img src="http://placehold.it/848x450" alt="Chania" /></div>';	
   		} else {
   			$carousel .= '<div class="item"><img src="http://placehold.it/848x450" alt="Chania" /></div>';
   		}
   	}

	$carousel .= '	
		</div>
	</div>
    ';
    
    return $carousel;
    
}

//add our shortcode carousel
add_shortcode('carousel', 'get_carousel');
//add_action( 'init', 'register_shortcodes');

// embed font function
function mytheme_embed_fonts(){
	// you can directly enqueue and register
	VP_Site_GoogleWebFont::instance()->register_and_enqueue();	
}
add_action('wp_enqueue_scripts', 'mytheme_embed_fonts');

require_once dirname( __FILE__ ) . '/resources/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */

define( 'cian_plugins_dir', get_template_directory_uri().'/plugins/');

function my_theme_register_required_plugins() {

	/**
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(
		array(
			'name'     => 'Contact Form 7',
			'slug'     => 'contact-form-7',
			'required' => true,
		),
		array(
			'name'     => 'Rotating Tweets',
			'slug'     => 'rotatingtweets',
			'required' => true,
		),
		array(
			'name' 		=> 'Portfolio Post Type',
			'slug' 		=> 'portfolio-post-type',
			'source'   	=> get_stylesheet_directory() . '/plugins/portfolio-post-type.zip', // The plugin source
			'required' 	=> true,
		),
		array(
			'name'     				=> 'Vafpress Post Formats UI', // The plugin name
			'slug'     				=> 'vafpress-post-formats-ui-develop', // The plugin slug (typically the folder name)
			'source'   				=> get_stylesheet_directory() . '/plugins/vafpress-post-formats-ui-develop.zip', // The plugin source
			'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		)
	);

	// Change this to your theme text domain, used for internationalising strings
	$theme_text_domain = 'tgmpa';

	/**
	 * Array of configuration settings. Amend each line as needed.
	 * If you want the default strings to be available under your own theme domain,
	 * leave the strings uncommented.
	 * Some of the strings are added into a sprintf, so see the comments at the
	 * end of each line for what each argument will be.
	 */
	$config = array(
		'domain'       		=> $theme_text_domain,         	// Text domain - likely want to be the same as your theme.
		'default_path' 		=> '',                         	// Default absolute path to pre-packaged plugins
		'parent_menu_slug' 	=> 'themes.php', 				// Default parent menu slug
		'parent_url_slug' 	=> 'themes.php', 				// Default parent URL slug
		'menu'         		=> 'install-required-plugins', 	// Menu slug
		'has_notices'      	=> true,                       	// Show admin notices or not
		'is_automatic'    	=> false,					   	// Automatically activate plugins after installation or not
		'message' 			=> '',							// Message to output right before the plugins table
		'strings'      		=> array(
			'page_title'                       			=> __( 'Install Required Plugins', $theme_text_domain ),
			'menu_title'                       			=> __( 'Install Plugins', $theme_text_domain ),
			'installing'                       			=> __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name
			'oops'                             			=> __( 'Something went wrong with the plugin API.', $theme_text_domain ),
			'notice_can_install_required'     			=> _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
			'notice_can_install_recommended'			=> _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_install'  					=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
			'notice_can_activate_required'    			=> _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
			'notice_can_activate_recommended'			=> _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_activate' 					=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
			'notice_ask_to_update' 						=> _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_update' 						=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
			'install_link' 					  			=> _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
			'activate_link' 				  			=> _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
			'return'                           			=> __( 'Return to Required Plugins Installer', $theme_text_domain ),
			'plugin_activated'                 			=> __( 'Plugin activated successfully.', $theme_text_domain ),
			'complete' 									=> __( 'All plugins installed and activated successfully. %s', $theme_text_domain ), // %1$s = dashboard link
			'nag_type'									=> 'updated' // Determines admin notice type - can only be 'updated' or 'error'
		)
	);

	tgmpa( $plugins, $config );

}