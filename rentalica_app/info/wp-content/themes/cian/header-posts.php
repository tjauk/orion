<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package _tk
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>

<link href="<?php echo esc_url(vp_option('vpt_option.fav_ico')) ?>" rel="shortcut icon" type="image/x-icon">
<link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri() .'/blog.css') ?>" type="text/css" media="screen" />

<?php
wp_head(); 
?>

</head>

<body <?php body_class(); ?>>
	<?php do_action( 'before' );

	require_once 'resources/lib/Mobile-Detect/Mobile_Detect.php';
	$detect = new Mobile_Detect;	
	$msie = strpos($_SERVER["HTTP_USER_AGENT"], 'MSIE') ? true : false;
	
	$loader_enable = vp_option('vpt_option.loader_enable');
	if (!$detect->isMobile() && !$msie && $loader_enable == 1){
		require_once 'sections/preloader.php';
	}
	
	require_once 'sections/navbar.php';
?>

<section id="posts">
		<div class="container">
			<div class="row">
				<div class="heading col-md-8 col-md-offset-2">
					<span class="section-name"><?php echo vp_option('vpt_option.blog_name_section')?></span>
					<h2><?php echo vp_option('vpt_option.blog_title')?></h2>
					<span class="line"></span>
				</div>
			</div>
			<div class="row">
					<div class="col-md-12">
					
					