<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package _tk
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>

<link href="<?php echo esc_url(vp_option('vpt_option.fav_ico')) ?>" rel="shortcut icon" type="image/x-icon">

<?php 
if ( is_page_template("page-background-parallax.php") ){ ?>
	<style type="text/css">
		.parallax-homepage, .parallax-subscription, .parallax-clients, .parallax-twitter{
			background-size: cover !important;
			background-position: 100% center;
		}
		.parallax-homepage{
			background: url(<?php echo esc_url(vp_option('vpt_option.parallax_image_homepage')) ?>) fixed;
		}
		.parallax-subscription{
			background: url(<?php echo esc_url(vp_option('vpt_option.parallax_image_subscription')) ?>) fixed;
		}
		.parallax-clients{
			background: url(<?php echo esc_url(vp_option('vpt_option.parallax_image_clients')) ?>) fixed;
		}
		.parallax-twitter{
			background: url(<?php echo esc_url(vp_option('vpt_option.parallax_image_twitter')) ?>) fixed;
		}
	</style>
<?php }
	
if ( !is_page_template("page-onepage.php") && !is_page_template("page-blog.php") ){ ?>
<style type="text/css">
	nav.navbar-fixed-top{
		top: -60px;
	}
</style>
<?php 
}

wp_head(); 
?>

</head>

<body <?php body_class(); ?>>

	<?php do_action( 'before' ); 
