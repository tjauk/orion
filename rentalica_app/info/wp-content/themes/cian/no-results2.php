<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _tk
 */
?>

<section class="error-section">

	<div class="col-md-8 col-md-offset-2 big-text-error">404</div>
	<div class="clearfix"></div>
	<div class="col-md-8 col-md-offset-2 text-error">
		<h4>Sorry but the page you are looking for does not exist.</h4>
	</div><!-- .page-content -->
	
</section><!-- .no-results -->
