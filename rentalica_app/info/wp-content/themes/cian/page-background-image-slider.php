<?php
/**
 template name: Background Image Slider

 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _tk
 */

get_header('single');

		require_once 'resources/lib/Mobile-Detect/Mobile_Detect.php';
		$detect = new Mobile_Detect;	
		$msie = strpos($_SERVER["HTTP_USER_AGENT"], 'MSIE') ? true : false;

		$loader_enable = vp_option('vpt_option.loader_enable');
		
		if (!$detect->isMobile() && !$msie && $loader_enable == 1){
			require_once 'sections/preloader.php';
		}

		require_once 'sections/navbar.php'; 
				
		$sectionPageID = vp_option('vpt_option.header_section_page');
		
		if ($sectionPageID != ""){
			sec_header();
		}
		
		$cookies_enable = vp_option('vpt_option.cookies_message_enable');
		
		if ($cookies_enable == 1){
			sec_cookies_message();
		}
		
		$sectionPageID = vp_option('vpt_option.feature_section_page');
		
		if ($sectionPageID != ""){
			sec_features();
		}
		
		$sectionPageID = vp_option('vpt_option.spotlight_1_section_page');
		
		if ($sectionPageID != ""){
			sec_spotlight_1();
		}
		
		$subs_enable = vp_option('vpt_option.subs_enable');
		
		if ($subs_enable == 1){
			sec_subscription();
		}
		
		$portfolio_enable = vp_option('vpt_option.portfolio_enable');
		
		if ($portfolio_enable == 1){			
			sec_portfolio();
		}
		
		$sectionPageID = vp_option('vpt_option.spotlight_2_section_page');
		
		if ($sectionPageID != ""){
			sec_spotlight_2();
		}		
		
		$sectionPageID = vp_option('vpt_option.clients_section_page');
		
		if ($sectionPageID != ""){
			sec_clients();
		}
		
		$blog_enable = vp_option('vpt_option.blog_enable');
		
		if ($blog_enable == 1){			
			sec_blog();
		}
		
		$sectionPageID = vp_option('vpt_option.prices_section_page');
		
		if ($sectionPageID != ""){
			sec_prices();
		}

		$twitter_enable = vp_option('vpt_option.twitter_enable');
		
		if ($twitter_enable == 1){			
			sec_twitter();
		}

		$contact_enable = vp_option('vpt_option.contact_enable');
		
		if ($contact_enable == 1){			
			sec_contact();
		}		
		
		$sectionPageID = vp_option('vpt_option.footer_secion_page');
		if ($sectionPageID != ""){
			require_once 'sections/footer.php';
		}
	
get_footer('single'); 

?>	
	