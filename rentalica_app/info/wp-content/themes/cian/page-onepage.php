<?php
/**
 template name: One Page

 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _tk
 */
 
get_header('single'); 

	$loader_enable = vp_option('vpt_option.loader_enable');
	if ($loader_enable == 1){
		require_once 'sections/preloader.php';
	}
	
	require_once 'sections/navbar.php';

	while ( have_posts() ) : the_post();
		get_template_part( 'content', 'onepage' ); 
	endwhile; // end of the loop. 
	
	$sectionPageID = vp_option('vpt_option.footer_secion_page');
	if ($sectionPageID != ""){
		require_once 'sections/footer.php';
	}
	
get_footer('single2');

?>
