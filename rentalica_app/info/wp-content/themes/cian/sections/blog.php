<?php

add_shortcode('blog', 'sec_blog');  

function sec_blog(){

?>

	<!-- ==============================================
	BLOG
	=============================================== -->
	<section id="blog">
		<div class="container">
			<div class="row">
				<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
				<div class="heading col-md-8 col-md-offset-2 animate" data-animate="<?php echo vp_option('vpt_option.blog_animation');?>">
				<?php } else { ?>
				<div class="heading col-md-8 col-md-offset-2">
				<?php }?>
					<span class="section-name"><?php echo vp_option('vpt_option.blog_name_section')?></span>
					<h2><?php echo vp_option('vpt_option.blog_title')?></h2>
					<span class="line"></span>
					<p><?php echo vp_option('vpt_option.blog_text_intro')?></p>
				</div>
			</div>
			<div class="row">
				<!-- FIRST POST -->
				<?php
					$args = array( 
						'numberposts' => vp_option('vpt_option.blog_number_posts'),
						'post_status' => 'publish',
						'suppress_filters' => false
					);
					$recent_posts = wp_get_recent_posts( $args );
					foreach( $recent_posts as $recent ){
						echo '
						<div class="col-md-4 col-sm-4">';
							if (vp_option('vpt_option.animation_enable') == 1) {
								echo '<div class="blog-post animate" data-animate="'.vp_option('vpt_option.blog_animation').'">';	
							} else {
								echo '<div class="blog-post">';	
							}
							echo'
								<div class="blog-post-thumb">
								'.get_the_post_thumbnail( $recent["ID"], 'medium' ).'	
								</div>
								<div class="blog-post-content">
									<h3><a href="'.esc_url(get_permalink($recent["ID"])).'" title="'.esc_attr($recent["post_title"]).'">'.$recent["post_title"].'</a></h3>
									<p>';
									$excerpt = wp_trim_excerpt($recent['post_content']); // $excerpt contains the excerpt of the concerned post
        							$excerpt=substr($excerpt,0,150)."...";
									echo $excerpt;
									echo '</p>
									<div class="blog-post-footer">
										<ul>
											<li><p><span class="fa fa-calendar"></span>'.get_the_date("j F Y",$recent["ID"]).'</p></li>';
											$number_comments = wp_count_comments($recent["ID"]);
									   echo'<li><p><span class="fa fa-comments-o"></span>'.$number_comments->total_comments.'</p></li>
										</ul>
										<div class="blog-post-plus">
											<a href="'.esc_url(get_permalink($recent["ID"])).'" class="fa fa-plus-circle" ></a>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div> ';
				}
				
				if (vp_option('vpt_option.animation_enable') == 1) {
					echo '<div class="col-md-12 animate" data-animate="'.vp_option('vpt_option.blog_animation').'">';	
				} else {
					echo '<div class="col-md-12">';	
				}
				if ( vp_option('vpt_option.more_posts_button_enable') == true) {?>
					<a class="btn btn-default spotlight-link" href="<?php echo vp_option('vpt_option.more_posts_button_link') ?>"><?php echo vp_option('vpt_option.more_posts_button_text') ?></a>
				<?php } ?>
				</div>
				
			</div> <!-- END ROW -->
		</div> <!-- END CONTAINER -->
	</section> <!-- END BLOG SECTION -->
		
		
<?php 

}

?>