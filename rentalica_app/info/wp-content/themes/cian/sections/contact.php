<?php 

add_shortcode('contact', 'sec_contact');  

function sec_contact(){

?>
	
	<section id="contact">
		<div class="container">
			<div class="row">
				<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
				<div class="heading col-md-8 col-md-offset-2 animate" data-animate="<?php echo vp_option('vpt_option.contact_animation'); ?>">
				<?php } else { ?>
				<div class="heading col-md-8 col-md-offset-2">
				<?php }?>
					<span class="section-name"><?php echo vp_option('vpt_option.contact_name_section');?></span>
					<h2><?php echo vp_option('vpt_option.contact_title');?></h2>
					<span class="line"></span>
					<p><?php echo vp_option('vpt_option.contact_text_intro');?></p>
				</div>
			</div>
							
				<div class="row">
					<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
					<div class="col-md-10 col-md-offset-1 animate" data-animate="<?php echo vp_option('vpt_option.contact_animation'); ?>">	
					<?php } else { ?>
					<div class="col-md-10 col-md-offset-1">	
					<?php }?>	
						<?php 
							echo do_shortcode(vp_option('vpt_option.cf7_shortcode'));
						?>
					</div>
				</div>
			
		</div> <!-- END CONTAINER -->	
	</section> <!-- END CONTACT SECTION -->


<?php

}

?>