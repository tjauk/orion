<?php

add_shortcode('cookies_message', 'sec_cookies_message');  

function sec_cookies_message(){

?>
	
	<!-- ==============================================
	COOKIES MESSAGE
	=============================================== -->
    <?php if (vp_option('vpt_option.animation_enable') == 1) {?>
    <div id="cookies-message" class="animate" data-animate="<?php echo vp_option('vpt_option.cookies_message_animation'); ?>" >
	<?php } else { ?>
    <div id="cookies-message" >
	<?php }?>
    	<button class="close-cookies" title="Close cookies"><span class="fa fa-times"></span></button>
    	<p class="title-cookies"><?php echo vp_option('vpt_option.cookies_message_title'); ?></p>
    	<p><?php echo vp_option('vpt_option.cookies_message_text'); ?></p>
    </div>
	
<?php 

}

?>