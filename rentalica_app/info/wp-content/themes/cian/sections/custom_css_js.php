<?php 
header('Content-type: text/css');
?>

<?php
$rgb = hex2rgb(vp_option('vpt_option.primary_color'));
?>

body, p {
	font-family: '<?php echo vp_option('vpt_option.p_font_face');?>', sans-serif;
	font-style:<?php echo vp_option('vpt_option.p_font_style');?>;
	font-weight:<?php echo vp_option('vpt_option.p_font_weight');?>;
	font-size:<?php echo vp_option('vpt_option.p_font_size');?>px;
	line-height:<?php echo vp_option('vpt_option.p_line_height');?>px;
	color: <?php echo vp_option('vpt_option.secondary_color');?>;
}

a,
a:hover,
a:focus,
a:active,
em { 
	color: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

hr {
	border-color: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.color-hover,
.color-hover-2,
.color-hover-3{
	background-color: <?php echo "rgba(".$rgb[0].", ".$rgb[1].", ".$rgb[2].", 0.65)"; ?>;
}

.poster-image {
	background: url('<?php echo vp_option('vpt_option.video_image_replacement'); ?>');
}

h1 {
	font-family: '<?php echo vp_option('vpt_option.slider_title_font_face');?>', sans-serif;
	font-style:<?php echo vp_option('vpt_option.slider_title_font_style');?>;
	font-weight:<?php echo vp_option('vpt_option.slider_title_font_weight');?>;
	font-size:<?php echo (vp_option('vpt_option.slider_title_font_size')/2);?>px;
	line-height:<?php echo vp_option('vpt_option.slider_title_line_height');?>px;
}

h2 {
	font-family: '<?php echo vp_option('vpt_option.h2_font_face');?>', sans-serif;
	font-style:<?php echo vp_option('vpt_option.h2_font_style');?>;
	font-weight:<?php echo vp_option('vpt_option.h2_font_weight');?>;
	font-size:<?php echo vp_option('vpt_option.h2_font_size');?>px;
	line-height:<?php echo vp_option('vpt_option.h2_line_height');?>px;
	color:<?php echo vp_option('vpt_option.primary_color');?>;
}

h2 em{
	font-family: '<?php echo vp_option('vpt_option.h2_strong_font_face');?>', sans-serif;
	font-style:<?php echo vp_option('vpt_option.h2_strong_font_style');?>;
	font-weight:<?php echo vp_option('vpt_option.h2_strong_font_weight');?>;
}

h3 {
	font-family: '<?php echo vp_option('vpt_option.h3_font_face');?>', sans-serif;
	font-style:<?php echo vp_option('vpt_option.h3_font_style');?>;
	font-weight:<?php echo vp_option('vpt_option.h3_font_weight');?>;
	font-size:<?php echo vp_option('vpt_option.h3_font_size');?>px;
	line-height:<?php echo vp_option('vpt_option.h3_line_height');?>px;
	color:<?php echo vp_option('vpt_option.primary_color');?>;
}

h4 {
	font-family: '<?php echo vp_option('vpt_option.h4_font_face');?>', sans-serif;
	font-style:<?php echo vp_option('vpt_option.h4_font_style');?>;
	font-weight:<?php echo vp_option('vpt_option.h4_font_weight');?>;
	font-size:<?php echo vp_option('vpt_option.h4_font_size');?>px;
	line-height:<?php echo vp_option('vpt_option.h4_line_height');?>px;
	color:<?php echo vp_option('vpt_option.primary_color');?>;
}

.heading p { 
	font-family: '<?php echo vp_option('vpt_option.h2_p_font_face');?>', sans-serif;
	font-style:<?php echo vp_option('vpt_option.h2_p_font_style');?>;
	font-weight:<?php echo vp_option('vpt_option.h2_p_font_weight');?>;
	font-size:<?php echo vp_option('vpt_option.h2_p_font_size');?>px;
	line-height:<?php echo vp_option('vpt_option.h2_p_line_height');?>px;
	color: <?php echo vp_option('vpt_option.secondary_color');?>;
}

.heading span.line {
	border-bottom: 2px solid <?php echo vp_option('vpt_option.secondary_color2');?>;
}

ul.nav li a {
	color: <?php echo vp_option('vpt_option.primary_color');?>;
	font-size: <?php echo (vp_option('vpt_option.p_font_size')*0.8);?>px;
}

ul.nav li a::before, 
ul.nav li a::after,
ul.nav li a:hover::before, 
ul.nav li a:hover::after,
ul.nav li a:focus::before, 
ul.nav li a:focus::after,
ul.nav li.active a::before, 
ul.nav li.active a::after,
ul.nav li.active a,
ul.nav li.active a:hover,
ul.nav li ul.dropdown-menu li a:hover,
ul.nav li a.dropdown-toggle:hover,
ul.nav li a:hover,
.nav .open > a, .nav .open > a:hover, .nav .open > a:focus{
	color: <?php echo vp_option('vpt_option.secondary_color2');?>;
}	

#cookies-message p{
	font-family: '<?php echo vp_option('vpt_option.p_font_face');?>', sans-serif;
	font-style:<?php echo vp_option('vpt_option.p_font_style');?>;
	font-weight:<?php echo vp_option('vpt_option.p_font_weight');?>;
	font-size:<?php echo vp_option('vpt_option.p_font_size');?>px;
	line-height:<?php echo (vp_option('vpt_option.p_line_height')*0.8);?>px;
}

nav .logo,
.intro-brand, 
footer .logo { 
	color:<?php echo vp_option('vpt_option.logo_txt_m_font_color');?>;
	font-family: '<?php echo vp_option('vpt_option.logo_txt_m_font_face');?>';
	font-style: <?php echo vp_option('vpt_option.logo_txt_m_font_style');?>;
}

nav .logo { 
	font-size: <?php echo vp_option('vpt_option.logo_txt_m_font_size');?>px;
}

nav .logo:hover { 
	color:<?php echo vp_option('vpt_option.secondary_color2');?>;
}

.navbar-toggle span,
.navbar-toggle span,
.navbar-toggle span{
	background-color:<?php echo vp_option('vpt_option.primary_color');?>;
}

.navbar-toggle:hover span,
.navbar-toggle:focus span,
.navbar-toggle:active span{
	background-color:<?php echo vp_option('vpt_option.secondary_color2');?>;
}

footer h1.logo { 
	font-family: '<?php echo vp_option('vpt_option.logo_txt_f_font_face');?>', sans-serif;
	font-style:<?php echo vp_option('vpt_option.logo_txt_f_font_style');?>;
	font-weight:<?php echo vp_option('vpt_option.logo_txt_f_font_weight');?>;
	font-size:<?php echo vp_option('vpt_option.logo_txt_f_font_size');?>px;
	color:<?php echo vp_option('vpt_option.logo_txt_f_font_color');?>;
	line-height:50px;
}

.intro .btn-lg,
.intro .learn{
	font-size:<?php echo vp_option('vpt_option.p_font_size');?>px;
}

.intro .btn-lg:hover,
.intro .learn{
	border-color: <?php echo vp_option('vpt_option.secondary_color2');?>; 
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.intro .learn:hover{
	color: <?php echo vp_option('vpt_option.secondary_color2');?> !important;
}

.hi-icon,
.hi-icon-effectb .hi-icon {
	color:<?php echo vp_option('vpt_option.primary_color');?>;
}

.hi-icon-effect .hi-icon {
	box-shadow: 0 0 0 1px <?php echo vp_option('vpt_option.primary_color');?>;
}

.hi-icon-effect .hi-icon:after {
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.features-desc:hover h4 {
	color: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.spotlight-link{
	border: 1px solid <?php echo vp_option('vpt_option.primary_color');?>;
	color: <?php echo vp_option('vpt_option.primary_color');?> !important;
	font-size:<?php echo vp_option('vpt_option.p_font_size');?>px;
}

.spotlight-link:hover{
	border-color: <?php echo vp_option('vpt_option.secondary_color2');?>;
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
	color: #fff !important;
} 

article #newsletter span.section-name,
article #newsletter h2,
article #newsletter p {
	color: <?php echo vp_option('vpt_option.primary_color');?> !important;
}

.subs-input,
.subs-input:hover,
.subs-input:focus,
.subs-input:active {
	font-family: '<?php echo vp_option('vpt_option.p_font_face');?>', sans-serif;
	font-size:<?php echo vp_option('vpt_option.p_font_size');?>px;
}

.subs-submit{
	color: <?php echo vp_option('vpt_option.primary_color');?>;
	font-family: '<?php echo vp_option('vpt_option.p_font_face');?>', sans-serif;
	font-size:<?php echo vp_option('vpt_option.p_font_size');?>px;
}

article .subs-submit{
	border: 1px solid <?php echo vp_option('vpt_option.primary_color');?>;
}

article .subs-submit:hover,
article .subs-submit:focus,
article .subs-submit:active {
	border: 1px solid <?php echo vp_option('vpt_option.secondary_color2');?>;
}

article .subs-input,
article .subs-input:hover,
article .subs-input:focus,
article .subs-input:active {
	border-color: <?php echo vp_option('vpt_option.secondary_color');?>;
	color: <?php echo vp_option('vpt_option.primary_color');?>;
}

.subs-submit:hover,
.subs-submit:focus,
.subs-submit:active {
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
	font-size:<?php echo vp_option('vpt_option.p_font_size');?>px;
}

.success-message,
.valid{
	color: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

#gallery .custom-nav .filter{
	color: <?php echo vp_option('vpt_option.secondary_color');?>;
	border: 1px solid <?php echo vp_option('vpt_option.secondary_color');?>; 
}

#gallery .custom-nav .active,
#gallery .custom-nav .filter:hover{
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
	border: 1px solid <?php echo vp_option('vpt_option.secondary_color2');?>;
}

a.effect-chico:hover img{
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

a.effect-chico h2{
	font-size:<?php echo (vp_option('vpt_option.h2_font_size')*0.8);?>px;
}

<?php
$rgb1 = hex2rgb(vp_option('vpt_option.secondary_color2'));
?>

.gallery-images a,
.og-grid li > a {
	background: <?php echo "rgba(".$rgb1[0].", ".$rgb1[1].", ".$rgb1[2].", 0.3)"; ?>;
}

.gallery-images a:hover,
.og-grid li > a:hover {
	background: <?php echo "rgba(".$rgb1[0].", ".$rgb1[1].", ".$rgb1[2].", 0.8)"; ?>;
}

.og-expander {
	background: <?php echo vp_option('vpt_option.primary_color');?>;
}

.og-details a:hover {
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
	border: 1px solid <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.hi-icon-2 {
	color: <?php echo vp_option('vpt_option.secondary_color');?>;
}

.spotlight-features li:hover .hi-icon-2 {
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
	box-shadow: 0 0 0 2px <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.spotlight-features li:hover p strong {
	color: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

#comments .icon-title,
.circle-price {
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.buy-now a{
	border: 1px solid <?php echo vp_option('vpt_option.secondary_color');?>; 
	color: <?php echo vp_option('vpt_option.secondary_color');?> !important;
}

.buy-now a:hover{
	border-color: <?php echo vp_option('vpt_option.secondary_color2');?>; 
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

article h3 a,
.blog-post-content h3 a{
	color: <?php echo vp_option('vpt_option.primary_color');?>;
}

article h3 a:hover,
.blog-post-content h3 a:hover{
	color: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.blog-post-plus a{
	color: <?php echo vp_option('vpt_option.secondary_color');?>;
}

.blog-post-plus a:hover{
	color: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.post .thumbnail:hover{
	border-color: <?php echo vp_option('vpt_option.secondary_color2');?>;
}
	
.mask-overlay{
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.post-pagination a,
.post-content a.more-link,
nav.paging-navigation a,
.image-navigation a{
	color: <?php echo vp_option('vpt_option.primary_color');?>;
}

.post-pagination a:hover,
.post-content a:hover.more-link,
nav.paging-navigation a:hover,
.image-navigation a:hover{
	color: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.post-content .page-links a{
	background: <?php echo vp_option('vpt_option.secondary_color');?>;
}

.post-content .page-links a:hover{
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

a.more:hover,
a.more-posts:hover{
	border-color: <?php echo vp_option('vpt_option.secondary_color2');?>;
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.icon-twitter,
.rotatingtweets .rotatingtweet p a{
	color: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

form.wpcf7-form input,
form.wpcf7-form textarea {
	border: 1px solid <?php echo vp_option('vpt_option.secondary_color');?>;
	color: <?php echo vp_option('vpt_option.secondary_color');?>;
}

.button-send{
	background: <?php echo vp_option('vpt_option.primary_color');?> !important;
	font-family: '<?php echo vp_option('vpt_option.p_font_face');?>', sans-serif;
	font-size:<?php echo vp_option('vpt_option.p_font_size');?>px;
}

.button-send:hover,
.button-send:focus,
.button-send:active{
	background: <?php echo vp_option('vpt_option.secondary_color2');?> !important;
}

.footer-content .back-top:hover{
	background-color: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

ul.footer-social a:hover { 
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

#post h3 {
	border-left: 2px solid <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.search-results #post h3 a,
.archive #post h3 a{
	color: <?php echo vp_option('vpt_option.primary_color');?> !important;
}

.search-results #post h3 a:hover,
.archive #post h3 a:hover{
	color: <?php echo vp_option('vpt_option.secondary_color2');?> !important;
}

.entry-meta{
	color: <?php echo vp_option('vpt_option.secondary_color');?>;
}

.entry-meta .edit-link a{
	color: <?php echo vp_option('vpt_option.secondary_color');?>;
	border: 1px solid <?php echo vp_option('vpt_option.secondary_color');?>;
}

.entry-meta .edit-link a:hover{
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
	border: 1px solid <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.comment-respond input,
.comment-respond textarea {
	border: 1px solid <?php echo vp_option('vpt_option.secondary_color');?>;
	font-size: <?php echo vp_option('vpt_option.p_font_size');?>px;
}

.post-content .post-tags span {
	font-size: <?php echo (vp_option('vpt_option.p_font_size')*1.5);?>px;
}

.text-edit blockquote p{
	font-size: <?php echo (vp_option('vpt_option.p_font_size')*1.2);?>px;
	line-height: <?php echo (vp_option('vpt_option.p_line_height')*1.5);?>px;
}

.comment-list .comment-meta{
	color: <?php echo vp_option('vpt_option.primary_color');?>;
}

.comment-list .comment-meta .comment-reply-link{
	color: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.form-submit .btn-primary{
	background: <?php echo vp_option('vpt_option.primary_color');?>;
	font-size: <?php echo vp_option('vpt_option.p_font_size');?>px;
}

.form-submit .btn-primary:hover,
.form-submit .btn-primary:focus,
.form-submit .btn-primary:active{
	background-color: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

#footer-blog p{ 
	font-size: <?php echo vp_option('vpt_option.p_font_size');?>px;
	font-weight: <?php echo vp_option('vpt_option.p_font_weight');?>;
	line-height: <?php echo vp_option('vpt_option.p_line_height');?>px;
}

#footer-blog ul.footer-social a { 
	color: <?php echo vp_option('vpt_option.primary_color');?>;
}

#footer-blog ul.footer-social a:hover { 
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.search-form .search-field{
	font-size: <?php echo vp_option('vpt_option.p_font_size');?>px;
}

.sidebar .widget_categories ul li a{
	color: <?php echo vp_option('vpt_option.secondary_color');?>;
}

.sidebar .widget_categories ul li:hover,
.sidebar .widget_categories ul li:hover a{
	color: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.sidebar .widget_tag_cloud .tagcloud a{
	font-size: <?php echo vp_option('vpt_option.p_font_size');?>px !important;
	border: 1px solid <?php echo vp_option('vpt_option.secondary_color');?>; 
	color: <?php echo vp_option('vpt_option.secondary_color');?>;
}

.sidebar .widget_tag_cloud .tagcloud a:hover{
	border-color: <?php echo vp_option('vpt_option.secondary_color2');?>;
	background: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

.sidebar .widget_rotatingtweets_widget .rotatingtweets .rotatingtweet p{
	color: <?php echo vp_option('vpt_option.primary_color');?>;
}

.sidebar .widget select{
	color: <?php echo vp_option('vpt_option.primary_color');?>;
	font-weight: <?php echo vp_option('vpt_option.p_font_weight');?> !important;
	line-height: <?php echo vp_option('vpt_option.p_line_height');?>px;
}

.search-form .button-search:hover {
	background-color: <?php echo vp_option('vpt_option.secondary_color2');?>;
}

@media (max-width: 1024px) {
	body, p {
		font-size:<?php echo (vp_option('vpt_option.p_font_size')*0.8);?>px;
		line-height:<?php echo (vp_option('vpt_option.p_line_height')*0.8);?>px;
	}
	h1 {
		font-size:<?php echo (vp_option('vpt_option.slider_title_font_size')*0.8);?>px;
		line-height:<?php echo (vp_option('vpt_option.slider_title_line_height')*0.8);?>px;
	}
	h2 {
		font-size:<?php echo (vp_option('vpt_option.h2_font_size')*0.8);?>px;
		line-height:<?php echo (vp_option('vpt_option.h2_line_height')*0.8);?>px;
	}
	h2 em{
		font-weight:<?php echo (vp_option('vpt_option.h2_strong_font_weight')*0.8);?>;
	}
	h3 {
		font-size:<?php echo (vp_option('vpt_option.h3_font_size')*0.8);?>px;
		line-height:<?php echo (vp_option('vpt_option.h3_line_height')*0.8);?>px;
	}
	h4 {
		font-size:<?php echo (vp_option('vpt_option.h4_font_size')*0.8);?>px;
		line-height:<?php echo (vp_option('vpt_option.h4_line_height')*0.8);?>px;
	}
	.heading p { 
		font-size:<?php echo (vp_option('vpt_option.h2_p_font_size')*0.8);?>px;
		line-height:<?php echo (vp_option('vpt_option.h2_p_line_height')*0.8);?>px;
	}
	.intro .btn-lg,
	.intro .learn{
		font-size:<?php echo (vp_option('vpt_option.p_font_size')*0.8);?>px;
	}
	nav .logo { 
		font-size: <?php echo (vp_option('vpt_option.logo_txt_m_font_size')*0.8);?>px;
	}
	footer h1.logo { 
		font-size:<?php echo (vp_option('vpt_option.logo_txt_f_font_size')*0.8);?>px;
		line-height:50px;
	}
	.spotlight-link{
		font-size:<?php echo (vp_option('vpt_option.p_font_size')*0.8);?>px;
	}
	.subs-input,
	.subs-input:hover,
	.subs-input:focus,
	.subs-input:active,
	.subs-submit,
	.subs-submit:hover,
	.subs-submit:focus,
	.subs-submit:active,
	.button-send,
	.button-send:hover,
	.button-send:focus,
	.button-send:active {
		font-size:<?php echo (vp_option('vpt_option.p_font_size')*0.8);?>px;
	}
	.comment-respond input,
	.comment-respond textarea {
		font-size: <?php echo (vp_option('vpt_option.p_font_size')*0.8);?>px;
	}
	.post-content .post-tags span {
		font-size: <?php echo (vp_option('vpt_option.p_font_size')*1.2);?>px;
	}
	.text-edit blockquote p{
		font-size: <?php echo (vp_option('vpt_option.p_font_size')*0.95);?>px;
		line-height: <?php echo (vp_option('vpt_option.p_line_height'));?>px;
	}
	.form-submit .btn-primary{
		font-size:<?php echo (vp_option('vpt_option.p_font_size')*0.8);?>px;
	}
	#footer-blog p{ 
		font-size: <?php echo (vp_option('vpt_option.p_font_size')*0.8);?>px;
		font-weight: <?php echo vp_option('vpt_option.p_font_weight');?>;
		line-height: <?php echo (vp_option('vpt_option.p_line_height')*0.8);?>px;
	}
	.search-form .search-field{
		font-size: <?php echo (vp_option('vpt_option.p_font_size')*0.8);?>px;
	}
	.sidebar{
		border-top: 1px dotted <?php echo vp_option('vpt_option.secondary_color2');?>;
	}
}

@media (max-width:992px) {
	ul.nav li a {
		font-size: <?php echo (vp_option('vpt_option.p_font_size')*0.7);?>px;
	}
}

@media (max-width: 480px) {
	h1 {
		font-size:<?php echo (vp_option('vpt_option.slider_title_font_size')*0.65);?>px;
		line-height:<?php echo (vp_option('vpt_option.slider_title_line_height')*0.65);?>px;
	}
	h3 {
		font-size:<?php echo (vp_option('vpt_option.h3_font_size')*0.7);?>px;
		line-height:<?php echo (vp_option('vpt_option.h3_line_height')*0.7);?>px;
	}
}


<?php echo vp_option('vpt_option.ce_css'); ?>