<?php

add_shortcode('features', 'sec_features');  

function sec_features(){
	
	$sectionPageID = vp_option('vpt_option.feature_section_page');
	if(function_exists('icl_object_id' )) {
    	$sectionPageID = icl_object_id(vp_option('vpt_option.feature_section_page' ),'page' ,true);
    }
?>
	
	<!-- ==============================================
	FEATURES
	=============================================== -->
	<section id="features">
		<div class="container">
			<div class="row">
				<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
				<div class="heading col-md-8 col-md-offset-2 animate" data-animate="<?php echo vp_metabox('vp_meta_feature.feature_sep_group.0.animation', false, $sectionPageID); ?>">
				<?php } else { ?>
				<div class="heading col-md-8 col-md-offset-2">
				<?php }?>
					<span class="section-name"><?php echo vp_metabox('vp_meta_feature.feature_sep_group.0.name_section', false, $sectionPageID); ?></span>
					<h2><?php echo vp_metabox('vp_meta_feature.feature_sep_group.0.title', false, $sectionPageID); ?></h2>
					<span class="line"></span>
					<p><?php echo vp_metabox('vp_meta_feature.feature_sep_group.0.text_intro', false, $sectionPageID); ?></p>
				</div>
			</div>
			<div class="row">	
				<!-- FEATURES LEFT SIDE -->
				<div class="features-wrapper col-md-2 col-sm-3" >
					<?php 
					$features =  vp_metabox('vp_meta_feature.feature_group', false, $sectionPageID);		
					$i = 1;
					if (is_array($features)){
						foreach ($features as $feature){
					
							if ($i%2 != 0){
								if (vp_option('vpt_option.animation_enable') == 1) {
									echo '<div class="features-desc col-md-12 col-sm-12 col-xs-6 animate" data-animate="'.vp_metabox('vp_meta_feature.feature_sep_group.0.animation', false, $sectionPageID).'">';
								} else {
									echo '<div class="features-desc col-md-12 col-sm-12 col-xs-6">';
								}	
								echo'
										<div class="hi-icon-wrap hi-icon-effect hi-icon-effectb">
											<div class="hi-icon fa '.esc_attr($feature['fa_1']).'"></div>
										</div>
										<h4>'.$feature['title'] .'</h4>
										<p>'.$feature['text'] .'</p>
									</div>';
							}
							$i++;
						}
					} ?>
				</div>
				<!-- END FEATURES LEFT SIDE -->
				
				<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
				<!-- FEATURES IMAGES -->						
				<figure class="col-md-8 col-sm-6 features-images">
					<img class="feat1 hidden-sm hidden-xs animate" data-animate="<?php echo vp_metabox('vp_meta_feature.feature_sep_group.0.animation', false, $sectionPageID); ?>" src="<?php echo esc_url(vp_metabox('vp_meta_feature.feature_sep_group.0.feature_image_left', false, $sectionPageID)) ?>" alt="" title="">
					<img class="feat2 hidden-sm hidden-xs animate" data-animate="<?php echo vp_metabox('vp_meta_feature.feature_sep_group.0.animation', false, $sectionPageID); ?>" src="<?php echo esc_url(vp_metabox('vp_meta_feature.feature_sep_group.0.feature_image_right', false, $sectionPageID)) ?>" alt="" title="">
					<img class="feat3 animate" data-animate="<?php echo vp_metabox('vp_meta_feature.feature_sep_group.0.animation', false, $sectionPageID); ?>" src="<?php echo esc_url(vp_metabox('vp_meta_feature.feature_sep_group.0.feature_image_center', false, $sectionPageID)) ?>" alt="" title="">
				</figure>
				<!-- END FEATURES IMAGES -->
				<?php } else { ?>
				<!-- FEATURES IMAGES -->						
				<figure class="col-md-8 col-sm-6 features-images">
					<img class="feat1 hidden-sm hidden-xs" src="<?php echo esc_url(vp_metabox('vp_meta_feature.feature_sep_group.0.feature_image_left', false, $sectionPageID)) ?>" alt="" title="">
					<img class="feat2 hidden-sm hidden-xs" src="<?php echo esc_url(vp_metabox('vp_meta_feature.feature_sep_group.0.feature_image_right', false, $sectionPageID)) ?>" alt="" title="">
					<img class="feat3" src="<?php echo esc_url(vp_metabox('vp_meta_feature.feature_sep_group.0.feature_image_center', false, $sectionPageID)) ?>" alt="" title="">
				</figure>
				<!-- END FEATURES IMAGES -->
				<?php }?>
				
				<!-- FEATURES RIGTH SIDE -->
				<div class="features-wrapper col-md-2 col-sm-3" >
					<?php 
					$features =  vp_metabox('vp_meta_feature.feature_group', false, $sectionPageID);		
					$i = 1;
					if (isset($features)){
						foreach ($features as $feature){
					
							if ($i%2 == 0){
								if (vp_option('vpt_option.animation_enable') == 1) {
									echo'<div class="features-desc col-md-12 col-sm-12 col-xs-6 animate" data-animate="'.vp_metabox('vp_meta_feature.feature_sep_group.0.animation', false, $sectionPageID).'">';
								} else {
									echo'<div class="features-desc col-md-12 col-sm-12 col-xs-6">';
								}
								echo'
										<div class="hi-icon-wrap hi-icon-effect hi-icon-effectb">
											<div class="hi-icon fa '.esc_attr($feature['fa_1']).'"></div>
										</div>
										<h4>'.$feature['title'] .'</h4>
										<p>'.$feature['text'] .'</p>
									</div>';
							}
							$i++;
						}
					} ?>
				</div>
				<!-- END FEATURES RIGHT SIDE -->
	
			</div> <!-- END ROW -->
		</div> <!-- END CONTAINER -->	
	</section> <!-- END FEATURES SECTION -->
	
<?php 

}

?>