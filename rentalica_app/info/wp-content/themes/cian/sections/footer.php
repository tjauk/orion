
	<footer>
		<div class="container">
			<div class="row footer-content">
				<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
				<div class="col-md-10 col-md-offset-1 animate" data-animate="<?php echo vp_metabox('vp_meta_footer.footer_sep_group.0.animation', false, $sectionPageID); ?>">
				<?php } else { ?>
				<div class="col-md-10 col-md-offset-1">
				<?php }?>
					<hr>
					<a id="toTop" class="back-top fa fa-angle-up" href="#top" title=""></a>
					<h1 class="logo">
						<?php
							if (vp_option('vpt_option.logo_type') == 'logo_img'){
								echo '<img src="'.vp_option('vpt_option.logo_f').'" alt="logo">';
							}
							else if (vp_option('vpt_option.logo_type') == 'logo_txt'){
								echo vp_option('vpt_option.logo_txt_f_title');
							}
						?>
					</h1>
					<p><?php echo vp_metabox('vp_meta_footer.footer_sep_group.0.copyright', false, $sectionPageID); ?></p>
					<ul class="footer-social">
						<?php 
						
							$social_icons =  vp_metabox('vp_meta_footer.social_icon_group', false, $sectionPageID);

							if (is_array($social_icons)) {
								foreach ($social_icons as $social_icon){				
									echo 	'<li>
												<a href="'.esc_url($social_icon['tb_11']).'">
													<div class="fa '.esc_attr($social_icon['fa_1']).'"></div>
												</a>
											</li>';
								}
							}
							
						?>
					</ul>
				</div>
			</div>
		</div>
	</footer> <!-- END FOOTER SECTIONS -->