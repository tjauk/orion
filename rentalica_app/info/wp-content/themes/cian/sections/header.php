<?php 

add_shortcode('header', 'sec_header');  

function sec_header(){

	$sectionPageID = vp_option('vpt_option.header_section_page');
	if(function_exists('icl_object_id' )) {
    	$sectionPageID = icl_object_id(vp_option('vpt_option.header_section_page' ),'page' ,true);
    }
	
?>
	
	<section id="homepage">
		<div class="container">
			<div class="row">
			<?php if( vp_metabox('vp_meta_header.header_sep_group.0.button_image', false, $sectionPageID) == 1){
				if (vp_option('vpt_option.animation_enable') == 1) {
				echo '<div class="intro col-md-5 animate" data-animate="'.vp_metabox('vp_meta_header.header_sep_group.0.animation', false, $sectionPageID).'">';
				} else {
				echo '<div class="intro col-md-5">';
				}
			}else{
				if (vp_option('vpt_option.animation_enable') == 1) {
				echo'<div class="intro col-md-8 col-md-offset-2 video-version animate" data-animate="'.vp_metabox('vp_meta_header.header_sep_group.0.animation', false, $sectionPageID).'">';
				} else {
				echo'<div class="intro col-md-8 col-md-offset-2 video-version"';
				}
			} ?>
					<h1><?php echo vp_metabox('vp_meta_header.header_sep_group.0.title', false, $sectionPageID); ?></h1>
					<h3><?php echo vp_metabox('vp_meta_header.header_sep_group.0.text_intro', false, $sectionPageID); ?></h3>
					<?php 
					$buttons_header =  vp_metabox('vp_meta_header.header_group', false, $sectionPageID);
						foreach ($buttons_header as $button_header){				
							echo'<a class="btn btn-default '.esc_attr($button_header['style']).'" href="'.esc_url($button_header['link']).'">'.$button_header['text'].'</a>';
						}
					?>
				</div> <!-- end homepage intro -->
				<?php if( vp_metabox('vp_meta_header.header_sep_group.0.button_image', false, $sectionPageID) == 1){
				if (vp_option('vpt_option.animation_enable') == 1) {?>
				<div class="col-md-6 col-md-offset-1 animate" data-animate="<?php echo vp_metabox('vp_meta_header.header_sep_group.0.animation', false, $sectionPageID) ?>">
				<?php } else { ?>
				<div class="col-md-6 col-md-offset-1">
				<?php }?>
					<img class="img-responsive mockup-slider" src="<?php echo esc_url(vp_metabox('vp_meta_header.header_sep_group.0.header_image', false, $sectionPageID)) ?>" alt="" title="">
				</div>
				<?php } ?>
			</div>
		</div>
		<a class="scroll-down" href="#features" title=""><img src="<?php echo esc_url(get_template_directory_uri().'/resources/images/scroll-down.png') ?>" alt="" /></a>
	</section> <!-- END SLIDER -->
	
<?php 

}

?>