<?php
$sectionPageID = vp_option('vpt_option.image_slider_section_page');
echo '
jQuery(function($){
	"use strict";
	$.supersized({ ';

		$images =  vp_metabox('vp_meta_image_slider.image_slider', false, $sectionPageID);
		$c = count($images);
		if ($c == 1){
			echo'
				slideshow               :   0, ';
		}else{
			echo'
				slideshow               :   1, ';
		}
		
		echo'
		autoplay				:	1,			
		start_slide             :   1,			
		stop_loop				:	0,			
		random					: 	0, ';
		
		if ( (vp_metabox('vp_meta_image_slider.image_slider_sep_group.0.slide_interval', false, $sectionPageID)) == '' ){
			echo 'slide_interval          :   3000,';
		} else {
			echo 'slide_interval          :   '.vp_metabox('vp_meta_image_slider.image_slider_sep_group.0.slide_interval', false, $sectionPageID).',';
		}
		
		if ( (vp_metabox('vp_meta_image_slider.image_slider_sep_group.0.transition', false, $sectionPageID)) == '' ){
			echo 'transition          :   0,';
		} else {
			echo 'transition          :   '.vp_metabox('vp_meta_image_slider.image_slider_sep_group.0.transition', false, $sectionPageID).',';
		}
		
		echo'
		transition_speed		:	1500,		
		new_window				:	1,			
		pause_hover             :   0,			
		keyboard_nav            :   1,			
		performance				:	1,			
		image_protect			:	1,								   
		
		min_width		        :   0,			
		min_height		        :   0,			
		vertical_center         :   1,			
		horizontal_center       :   1,			
		fit_always				:	0,			
		fit_portrait         	:   1,			
		fit_landscape			:   0,			

		slide_links				:	\'blank\',	
		thumb_links				:	1,			
		thumbnail_navigation    :   0,			
		slides 					:  	[';
								
								$images =  vp_metabox('vp_meta_image_slider.image_slider', false, $sectionPageID);
								$c = count($images);
								$i=1;
								if (is_array($images)){
									foreach ($images as $image){
										if($i != $c){
											echo '{image : \''.esc_url($image['image']).'\'},';
										}else{
											echo '{image : \''.esc_url($image['image']).'\'}';
										}	
										$i++;			
									}
								}

								echo'],
										
		progress_bar			:	1,								
		mouse_scrub				:	0
			
	});
});';

?>