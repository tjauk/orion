    <!-- ==============================================
	NAVIGATION BAR
	=============================================== -->
	<nav class="navbar navbar-fixed-top" role="navigation" <?php if (is_user_logged_in()){ echo 'style="top:32px;"';}?>>
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
				    <span class="icon-bar"></span>
				    <span class="icon-bar"></span>
				    <span class="icon-bar"></span>
				</button>			
				<?php
				if (vp_option('vpt_option.logo_type') == 'logo_img'){
					echo'
					<a class="navbar-brand logo logo-image-header" href="'.esc_url( home_url( '/' ) ).'" title="'.esc_attr( get_bloginfo( 'name', 'display' ) ).'" rel="home">	
						<img src="'.esc_url(vp_option('vpt_option.logo_m')).'" alt="logo">
					</a>';
				}
				else if (vp_option('vpt_option.logo_type') == 'logo_txt'){
					echo '<a class="navbar-brand logo" href="'.esc_url( home_url( '/' ) ).'" title="'.esc_attr( get_bloginfo( 'name', 'display' ) ).'" rel="home">';	
					echo vp_option('vpt_option.logo_txt_m_title').'</a>';
				}
				?>					
				
				
			</div>
						
			<div class="collapse navbar-collapse">
				
				<?php 
					wp_nav_menu(
						array(
							'theme_location' => 'primary',
							'container_class' => 'collapse navbar-collapse navbar-responsive-collapse navbar-right',
							'menu_class' => 'nav navbar-nav',
							'fallback_cb' => '',
							'menu_id' => 'main-menu',
							'walker' => new wp_bootstrap_navwalker()
						)
					); 						
				?>
				
				</ul>
			</div> 
		</div>
	</nav>
    