<?php

add_shortcode('add_gallery', 'sec_portfolio');  

function sec_portfolio(){

if (class_exists('Portfolio_Post_Type')){ 

?>
	
	<!-- ==============================================
	GALLERY
	=============================================== -->
	<section id="gallery">
		<div class="container">
			<div class="row">
				<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
				<div class="heading col-md-8 col-md-offset-2 animate" data-animate="<?php echo vp_option('vpt_option.portfolio_animation');?>">
				<?php } else { ?>
				<div class="heading col-md-8 col-md-offset-2">
				<?php }?>
					<span class="section-name"><?php echo vp_option('vpt_option.portfolio_name_section');?></span>
					<h2><?php echo vp_option('vpt_option.portfolio_title');?></h2>
					<span class="line"></span>
					<p><?php echo vp_option('vpt_option.portfolio_text_intro');?></p>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-12">
					<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
					<div class="btn-group custom-nav animate" data-animate="<?php echo vp_option('vpt_option.portfolio_animation');?>">
					<?php } else { ?>
					<div class="btn-group custom-nav">
					<?php }?>
						<ul class="nav">
							<li class="filter active" data-filter="all">All</li>
							<?php
							$taxonomy = 'portfolio_category';
							$terms = get_terms($taxonomy); // Get all terms of a taxonomy
							if ( $terms && !is_wp_error( $terms ) ) :
							foreach ( $terms as $term ) { 
							?>										
								<li class="filter" data-filter="<?php echo strtolower(preg_replace('/\s+/', '-', $term->name)); ?>">
									<?php echo $term->name; ?>
								</li>
							<?php 
								} 
							?>									
							<?php endif;?>
						</ul>
					</div>
				</div>	
			</div>
			
			<?php 
			switch ( vp_option('vpt_option.portfolio_style') ) {
				case 'grid': 
				?>
			    	<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
			    	<div id="Grid" class="row gallery-images animate" data-animate="<?php echo vp_option('vpt_option.portfolio_animation');?>">	
					<?php } else { ?>
			    	<div id="Grid" class="row gallery-images">	
					<?php }?>
						<!-- GALLERY IMAGES -->
						<?php
							global $wpdb;
							$query="SELECT * FROM $wpdb->posts WHERE post_type = '%s' AND post_status = '%s'";
		     				$results = $wpdb->get_results($wpdb->prepare($query,'portfolio','publish'));
							
							if ($results)
							{
								foreach ( $results as $post )
								{ 
									$terms = get_the_terms( $post->ID, 'portfolio_category' ); 
									$large_image =  wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'fullsize', false, '' ); 
									$large_image = $large_image[0];
								?>	
								<div class="col-xs-6 col-md-3 image-container mix <?php foreach ($terms as $term) { echo strtolower(preg_replace('/\s+/', '-', $term->name)). ' '; } ?>" >
									<?php 
									$format = get_post_format( $post->ID );
									switch ( $format ){
										case "video":
											$class = 'portfolio-video';
											$content = get_post_meta( $post->ID, '_format_video_embed', true ) ;
											break;
										case "gallery":
											$class = 'portfolio-gallery';
											$content = esc_url($large_image);
											break;
										default:
											$class = 'portfolio-popup';
											$content = esc_url($large_image);
											break;
									}
									?>
									<a href="<?php echo $content; ?>" class="effect-chico <?php echo $class; ?>" title="<?php echo esc_attr($post->post_content) ?>">
										<?php echo get_the_post_thumbnail($post->ID) ?>	
								    	<div class="image-content">
								    		<h2><?php echo $post->post_title ?></h2>
								    		<?php 
								    		$excerpt = wp_trim_excerpt($post->post_content); // $excerpt contains the excerpt of the concerned post
        									$excerpt = substr($excerpt,0,50)."...";
											echo '<p>'.$excerpt.'</p>';
								    		?>
								    	</div>
									</a>
								</div>
								<?php 
								} // foreach 
							} // if	
						?>										
						<!-- END GALLERY IMAGES -->
					</div> <!-- END ROW -->
			    
			    <?php
			    break;
			    case 'expanded_grid':
			    ?>
			    
			    	<div class="row gallery-images-expanded-grid">	
			    		<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
						<ul id="og-grid" class="og-grid animate" data-animate="<?php echo vp_option('vpt_option.portfolio_animation');?>">
						<?php } else { ?>
						<ul id="og-grid" class="og-grid">
						<?php }?>
						<?php 
						global $wpdb;
						$query="SELECT * FROM $wpdb->posts WHERE post_type = '%s' AND post_status = '%s'";
		     			$results = $wpdb->get_results($wpdb->prepare($query,'portfolio','publish'));
							
						if ($results)
						{
							foreach ( $results as $post )
							{ 
								$terms = get_the_terms( $post->ID, 'portfolio_category' ); 
								$large_image =  wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'fullsize', false, '' ); 
								$large_image = $large_image[0];
								?>	
								<li class="image-container mix <?php foreach ($terms as $term) { echo strtolower(preg_replace('/\s+/', '-', $term->name)). ' '; } ?>" >
									<a href="<?php echo esc_attr($post->post_excerpt) ?>" class="effect-chico" data-largesrc="<?php echo esc_url($large_image) ?>" data-title="<?php echo esc_attr($post->post_title) ?>" data-description="<?php echo esc_attr($post->post_content) ?>">
										<?php echo get_the_post_thumbnail($post->ID) ?>
										<div class="image-content">
								    		<h2><?php echo $post->post_title ?></h2>
								    		<?php 
								    		$excerpt = wp_trim_excerpt($post->post_content); // $excerpt contains the excerpt of the concerned post
        									$excerpt = substr($excerpt,0,50)."...";
											echo '<p>'.$excerpt.'</p>';
								    		?>
								    	</div>
									</a>
								</li>
								<?php 
								} // foreach 
							} // if	
						?>	
						</ul> <!-- END GALLERY IMAGES -->
					</div> <!-- END ROW -->
			    
			    <?php
			    break;
			    default:
			    	echo '';
			}
			?>
			
		</div> <!-- END CONTAINER -->	
	</section> <!-- END GALLERY SECTION -->
	
<?php 

}

}
?>