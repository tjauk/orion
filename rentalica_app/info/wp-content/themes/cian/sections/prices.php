<?php 

add_shortcode('prices', 'sec_prices');  

function sec_prices(){

	$sectionPageID = vp_option('vpt_option.prices_section_page');
	if(function_exists('icl_object_id' )) {
    	$sectionPageID = icl_object_id(vp_option('vpt_option.prices_section_page' ),'page' ,true);
    }
	
?>	
	<!-- ==============================================
	PRICES
	=============================================== -->
	<section id="prices">
		<div class="container">
		
			<div class="row">
				<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
				<div class="heading col-md-8 col-md-offset-2 animate" data-animate="<?php echo vp_metabox('vp_meta_prices.prices_sep_group.0.animation', false, $sectionPageID); ?>">
				<?php } else { ?>
				<div class="heading col-md-8 col-md-offset-2">
				<?php }?>
					<span class="section-name"><?php echo vp_metabox('vp_meta_prices.prices_sep_group.0.name_section', false, $sectionPageID) ?></span>
					<h2><?php echo vp_metabox('vp_meta_prices.prices_sep_group.0.title', false, $sectionPageID) ?></h2>
					<span class="line"></span>
					<p><?php echo vp_metabox('vp_meta_prices.prices_sep_group.0.text_intro', false, $sectionPageID) ?></p>
				</div>
			</div>
			
			<div class="row pricing-tables">
				
				<?php
				
					$prices =  vp_metabox('vp_meta_prices.prices_group', false, $sectionPageID);
					
					$i = count($prices);
					$c = count($prices);
										
					if ($c == 1) $col = "col-md-12";
					if ($c == 2) $col = "col-md-6 col-sm-6";
					if ($c == 3) $col = "col-md-4 col-sm-6";
					if ($c >= 4) $col = "col-md-3 col-sm-6";
					if (is_array($prices)){
						foreach ($prices as $price){			
							echo '
							<div class="'.esc_attr($col).' animate" data-animate="'.vp_metabox('vp_meta_prices.prices_sep_group.0.animation', false, $sectionPageID).'">
						        <!-- Pricing Table -->
						        <div class="plan">
						       		<div class="plan-heading">
							            <h3>'.$price['title'].'</h3>
							            <p class="subtitle-price">'.$price['subtitle'].'</p>
							            <div class="circle-price">
							              <span>'.$price['price'].'</span>
							            </div>
						        	</div>
						       		'.$price['details'];
									if ( $price['button_link_enable'] == true ){
						          	echo '<div class="buy-now">
						          		  <a class="btn btn-default btn-sm" href="'.esc_url($price['button_link_url']).'">';
						          			echo $price['button_link_text'];
						          	echo '</a> </div>';
						          	}
						    	echo' </div> <!-- End Pricing Table -->
							</div>
				    		';
							$i++;	
						}
					}					
				?>
				
			</div> <!-- END ROW -->
			
		</div> <!-- END CONTAINER -->	
	</section> <!-- END PRICES SECTION -->			

<?php 

}

?>