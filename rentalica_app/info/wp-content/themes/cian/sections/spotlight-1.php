<?php

add_shortcode('spotlight_1', 'sec_spotlight_1');  

function sec_spotlight_1(){
	
	$sectionPageID = vp_option('vpt_option.spotlight_1_section_page');
	if(function_exists('icl_object_id' )) {
    	$sectionPageID = icl_object_id(vp_option('vpt_option.spotlight_1_section_page' ),'page' ,true);
    }
	
?>
	<!-- ==============================================
	SPOTLIGHT 1
	=============================================== -->
	<section id="spotlight1">
		<div class="container">
			<div class="row">
				<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
				<div class="col-md-3 col-sm-5 col-md-offset-1 animate" data-animate="<?php echo vp_metabox('vp_meta_spotlight_1.spotlight_1_sep_group.0.animation', false, $sectionPageID); ?>">
				<?php } else { ?>
				<div class="col-md-3 col-sm-5 col-md-offset-1">
				<?php }?>
					<h3><?php echo vp_metabox('vp_meta_spotlight_1.spotlight_1_sep_group.0.title', false, $sectionPageID); ?></h3>
					<p><?php echo vp_metabox('vp_meta_spotlight_1.spotlight_1_sep_group.0.text', false, $sectionPageID); ?></p>
					<?php if ( vp_metabox('vp_meta_spotlight_1.spotlight_1_sep_group.0.button_link_enable', false, $sectionPageID) == true) {?>
						<a class="btn btn-default spotlight-link" href="<?php echo esc_url(vp_metabox('vp_meta_spotlight_1.spotlight_1_sep_group.0.button_link_url', false, $sectionPageID)) ?>"><?php echo vp_metabox('vp_meta_spotlight_1.spotlight_1_sep_group.0.button_link_text', false, $sectionPageID); ?></a>
					<?php } ?>
				</div> <!-- end spotlight description -->
				<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
				<figure class="col-md-7 col-sm-7 animate" data-animate="<?php echo vp_metabox('vp_meta_spotlight_1.spotlight_1_sep_group.0.animation', false, $sectionPageID); ?>" >
				<?php } else { ?>
				<figure class="col-md-7 col-sm-7" >
				<?php }?>
					<img class="img-responsive spot1-img" src="<?php echo esc_url(vp_metabox('vp_meta_spotlight_1.spotlight_1_sep_group.0.spotlight_1_image', false, $sectionPageID)) ?>" alt="" title="">
				</figure>
			</div> <!-- END ROW -->					
		</div> <!-- END CONTAINER -->
	</section> <!-- END SPOTLIGHT1 SECTION -->

	
<?php 

}

?>