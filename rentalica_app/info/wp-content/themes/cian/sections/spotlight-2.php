<?php

add_shortcode('spotlight_2', 'sec_spotlight_2');  

function sec_spotlight_2(){
	
	$sectionPageID = vp_option('vpt_option.spotlight_2_section_page');
	if(function_exists('icl_object_id' )) {
    	$sectionPageID = icl_object_id(vp_option('vpt_option.spotlight_2_section_page' ),'page' ,true);
    }
	
?>
	<!-- ==============================================
	SPOTLIGHT 2
	=============================================== -->
	<section id="spotlight2">
		<div class="container">
			<div class="row">
				<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
				<figure class="col-md-6 col-sm-6 hidden-xs animate" data-animate="<?php echo vp_metabox('vp_meta_spotlight_2.spotlight_2_sep_group.0.animation', false, $sectionPageID); ?>">
				<?php } else { ?>
				<figure class="col-md-6 col-sm-6 hidden-xs">
				<?php }?>
					<img class="img-responsive spot2-img" src="<?php echo esc_url(vp_metabox('vp_meta_spotlight_2.spotlight_2_sep_group.0.spotlight_2_image', false, $sectionPageID)) ?>" alt="" title="">
				</figure>
			<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
			<div class="col-md-6 col-sm-6 animate" data-animate="<?php echo vp_metabox('vp_meta_spotlight_2.spotlight_2_sep_group.0.animation', false, $sectionPageID); ?>">
			<?php } else { ?>
			<div class="col-md-6 col-sm-6">
			<?php }?>
					<h3><?php echo vp_metabox('vp_meta_spotlight_2.spotlight_2_sep_group.0.title', false, $sectionPageID); ?></h3>
	
				<ul class="spotlight-features">
				
				<?php
				
					$features_spotlight_2 =  vp_metabox('vp_meta_spotlight_2.spotlight_2_group', false, $sectionPageID);
					if (is_array($features_spotlight_2)){
						foreach ($features_spotlight_2 as $feature){			
								
							echo '
							<li>
								<div class="hi-icon-2-wrap hi-icon-2-effect">
									<div class="hi-icon-2 fa '.esc_attr($feature['fa_1']).'"></div>
								</div>
								<p>
								<strong>'.$feature['title'].'</strong><br/>
								'.$feature['text'].'
								</p>
							</li>';
	
						}
					}
										
				?>
				
				</ul>
				<?php if ( vp_metabox('vp_meta_spotlight_2.spotlight_2_sep_group.0.button_link_enable', false, $sectionPageID) == true) {?>
					<a class="btn btn-default spotlight-link" href="<?php echo esc_url(vp_metabox('vp_meta_spotlight_2.spotlight_2_sep_group.0.button_link_url', false, $sectionPageID)) ?>"><?php echo vp_metabox('vp_meta_spotlight_2.spotlight_2_sep_group.0.button_link_text', false, $sectionPageID); ?></a>
				<?php } ?>
				</div> <!-- end spotlight description -->
			</div> <!-- END ROW -->					
		</div> <!-- END CONTAINER -->
	</section> <!-- END SPOTLIGHT2 SECTION -->
	
<?php 

}

?>