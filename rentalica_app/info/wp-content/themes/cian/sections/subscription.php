<?php

add_shortcode('subscription', 'sec_subscription');  

function sec_subscription(){

?>

	<!-- ==============================================
	SUBSCRIPTION
	=============================================== -->
		
	<section id="newsletter">
		<div class="container">
			<div class="row">
				<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
				<div class="heading col-md-8 col-md-offset-2 animate" data-animate="<?php echo vp_option('vpt_option.subscription_animation'); ?>">
				<?php } else { ?>
				<div class="heading col-md-8 col-md-offset-2" >
				<?php }?>
					<span class="section-name"><?php echo vp_option('vpt_option.subs_name_section'); ?></span>
					<h2><?php echo vp_option('vpt_option.subs_title'); ?></h2>
					<span class="line"></span>
					<p><?php echo vp_option('vpt_option.subs_text_intro'); ?></p>
				</div>
			</div>
			<div class="row">
				<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
				<div class="col-md-8 col-md-offset-2 subscribe animate" data-animate="<?php echo vp_option('vpt_option.subscription_animation'); ?>">
				<?php } else { ?>
				<div class="col-md-8 col-md-offset-2 subscribe">
				<?php }?>
			    	<div id="mc_embed_signup">
						<form id="mc-form" class="form-inline">
							<div class="row">
								<div class="col-md-8 col-md-offset-1 col-sm-12">
						        	<input id="mc-email" type="email" placeholder="Type your email..." class="form-control subs-input">
						        </div>
						        <div class="col-md-1 col-sm-12">
						        	<button type="submit" class="btn btn-default subs-submit">send</button>
						        </div>
					        </div>
					        <div class="row">
						        <label for="mc-email"></label>
						    </div>
					    </form>
					</div>
			    </div>							
			</div> <!-- END ROW -->			
		</div> <!-- END CONTAINER -->
	</section> <!-- END NEWSLETTER SECTION -->	
	
<?php 

}

?>