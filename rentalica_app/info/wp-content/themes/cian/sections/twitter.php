<?php 

add_shortcode('twitter', 'sec_twitter');  

function sec_twitter(){

?>

	<!-- ==============================================
	TWITTER
	=============================================== -->
	
	<section id="twitter">
		<?php if (vp_option('vpt_option.animation_enable') == 1) {?>
		<div class="container animate" data-animate="<?php echo vp_option('vpt_option.twitter_animation')?>">
		<?php } else { ?>
		<div class="container">
		<?php }?>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="icon-twitter fa fa-twitter" ></div>
					<?php echo do_shortcode(vp_option('vpt_option.twitter_shortcode')); ?>
			    </div>							
			</div> <!-- END ROW -->			
		</div> <!-- END CONTAINER -->
	</section> <!-- END TWITTER SECTION -->
	
<?php

}

?>