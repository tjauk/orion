<?php
$detect = new Mobile_Detect;	
$msie = strpos($_SERVER["HTTP_USER_AGENT"], 'MSIE') ? true : false;
if ( !$detect->isMobile() and !$msie) {
	echo'
		(function($) {
		  	"use strict";
		    var BV = new $.BigVideo({doLoop: true});
			BV.init();';
			if ( vp_option('vpt_option.video_own_sound') == '1') {
				echo 'BV.show(\''.esc_attr(vp_option('vpt_option.video_own')).'\');';
			} else {
				echo 'BV.show(\''.esc_attr(vp_option('vpt_option.video_own')).'\',{ambient:true});';
			}
	echo'})(jQuery);';
} else {
	echo 'jQuery(\'#bgimg\').addClass(\'poster-image\');';
}

?>