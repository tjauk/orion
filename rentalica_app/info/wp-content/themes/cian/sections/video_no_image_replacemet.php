<?php

echo'
	(function($) {
	  	"use strict";
	    var BV = new $.BigVideo({doLoop: true});
		BV.init();';
		if ( vp_option('vpt_option.video_own_sound') == 0) {
			echo 'BV.show(\''.esc_attr(vp_option('vpt_option.video_own')).'\);';
		} else {
			echo 'BV.show(\''.esc_attr(vp_option('vpt_option.video_own')).'\,{ambient:true});';
	}
echo'})(jQuery);';

?>