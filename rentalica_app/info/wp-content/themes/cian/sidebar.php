<?php
/**
 * The sidebar containing the main widget area
 *
 * @package _tk
 */
?>
	
	</div><!-- close .main-content-inner -->

	<div class="sidebar col-md-3">

		<div class="sidebar-padder">

			<?php do_action( 'before_sidebar' ); ?>
			
			<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>
	
			<?php endif; ?>
			
		</div><!-- close .sidebar-padder -->
