@asset "jquery"
@asset "moment"
@asset "fullcalendar"

<!-- used in modals -->
@asset "jquery-form"
@asset "daterangepicker"
@asset "timepicker"
@asset "select2"
@asset "dropzone"

<style>
.fc-content {
    padding: 1px;
}
.fc-day-grid {
    height: 100% !important;
}
</style>

<!-- Main content -->
<section class="content">
  <div class="row">
    
    <div class="col-md-2">
      
      <div class="box">
        <div class="box-body">
          <a class="btn btn-primary" href="{{ROOTURL}}/admin/booking/edit"><i class="fa fa-plus"></i> Otvori novi najam</a>
        </div>
      </div>

      <div class="box box-solid">
        <div class="box-header with-border">
          <h4 class="box-title">Vozila</h4>
        </div>
        <div class="box-header with-border">
          <label class="label-md">Označi</label>
          <a class="btn-md vehicles-select-all" style="cursor:pointer;">Sva vozila</a>
          ili
          <a class="btn-md vehicles-select-none" style="cursor:pointer;">Ništa</a>
        </div>
        <div class="box-body">
          <!-- the events -->
          <div id="external-events">
            <?php
            $vehicles = Vehicle::whereShowOnCalendar(1)->get();
            foreach($vehicles as $vehicle){
            ?>
            <div class="external-event bg-default" style="cursor:pointer;">
                <label for="v{{vehicle.id}}" style="cursor:pointer;">
                    <span style="background-color:{{vehicle.color_hex}};padding:4px 1px 1px 4px;">
                        {{Form:checkBox('v'.$vehicle->id)->attr('data-id',$vehicle->id)->attr('checked','checked')->addClass('checked-vehicles')}}
                    </span>
                    &nbsp;&nbsp;
                    {{vehicle.name}}
                </label>
            </div>
            <?php }; ?>

            <!--
            <div class="checkbox">
              <label for="drop-remove">
                <input type="checkbox" id="drop-remove">
                remove after drop
              </label>
            </div>
            -->
          </div>
        </div><!-- /.box-body -->
      </div><!-- /. box -->

<?php /* ?>
      <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Create Event</h3>
        </div>
        <div class="box-body">
          <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
            <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
            <ul class="fc-color-picker" id="color-chooser">
              <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
              <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
              <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
              <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
              <li><a class="text-yellow" href="#"><i class="fa fa-square"></i></a></li>
              <li><a class="text-orange" href="#"><i class="fa fa-square"></i></a></li>
              <li><a class="text-green" href="#"><i class="fa fa-square"></i></a></li>
              <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
              <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
              <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
              <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
              <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
              <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
            </ul>
          </div><!-- /btn-group -->
          <div class="input-group">
            <input id="new-event" type="text" class="form-control" placeholder="Event Title">
            <div class="input-group-btn">
              <button id="add-new-event" type="button" class="btn btn-primary btn-flat">Add</button>
            </div><!-- /btn-group -->
          </div><!-- /input-group -->
        </div>
      </div>
<?php */ ?>

    </div><!-- /.col -->

    <div class="col-md-10">
      <div class="box box-primary">
        <div class="box-body no-padding">
          <!-- THE CALENDAR -->
          <div id="calendar"></div>
        </div><!-- /.box-body -->
      </div><!-- /. box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->

<script>

$(function () {

    /* initialize the external events
     -----------------------------------------------------------------*/
    function ini_events(ele) {
        ele.each(function () {

            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
            };

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);

            // make the event draggable using jQuery UI
            /*
            $(this).draggable({
                zIndex: 1070,
                revert: true, // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });
            */

        });
    }
    ini_events($('#external-events div.external-event'));

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date();
    var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear();

    $('#calendar').fullCalendar({
        lang: 'hr',
        nextDayThreshold: "00:00:00",
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },

        // Events API endpoint
        events: {
            url: '/admin/calendar/events',
            type: 'POST',
            data: function(){
                var vehicles = [];
                $( '.checked-vehicles:checked' ).each(function( index ) {
                    vehicles.push($(this).data('id'));
                });
                return {vehicles:vehicles};
            },
            error: function() {
                alert('there was an error while fetching events!');
            }
        },

        editable: false,
        droppable: false, // this allows things to be dropped onto the calendar !!!

        drop: function (date, allDay) { // this function is called when something is dropped

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            copiedEventObject.backgroundColor = $(this).css("background-color");
            copiedEventObject.borderColor = $(this).css("border-color");

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }

        },

        eventClick: function(calEvent, jsEvent, view) {

            console.log('Event: ' + calEvent.title);
            console.log('Model: ' + calEvent.model);
            console.log('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
            console.log('View: ' + view.name);

            // change the border color just for fun
            $(this).css('border-color', 'white');

            // open modal
            $('#ajaxModal').remove();
            
            var $this = $(this);
            if( calEvent.model == 'Booking' ){
                var $remote = '/admin/booking/modal/'+calEvent.id;
            }else if( calEvent.model == 'VehicleService' ){
                var $remote = '/admin/vehicleservice/modal/'+calEvent.id;
            }else if( calEvent.model == 'Vehicle' ){
                var $remote = '/admin/vehicle/modal/'+calEvent.id;
            } 

            var $modal = $('<div class="modal fade" id="ajaxModal"></div>');
            $('body').append($modal);
            $modal.modal({backdrop: true, keyboard: true, show: false});
            $modal.load($remote,function(){
                console.log('modal load');
                $('.modal-body').addClass('md-effect-1');
                $('#ajaxModal').modal('show');
                // do something fancy
                
            });

            $.fn.modal.Constructor.prototype.enforceFocus = function() {};
            
            $('#ajaxModal').on('show.bs.modal', function (e) {
              console.log('modal show');
              $('.modal-body').addClass('md-effect-1').addClass('md-show');
            });
            /*
            $(document).on('#ajaxModal .modal-body', 'click', function(e){
              $('#ajaxModal').modal('close');
            });
            */

        },

        resizable: false,
        eventResize: function(event, delta, revertFunc) {

            //alert(event.title + " end is now " + event.end.format());

            if (!confirm("is this okay?")) {
                revertFunc();
            }

        }

    });

    // initial
    $('.checked-vehicles').prop('checked',false);

    $(document).on('change','.checked-vehicles',function(event){
        $('#calendar').fullCalendar('refetchEvents');
        console.log('.checked-vehicles');
    });

    $(document).on('click','.vehicles-select-all',function(event){
        event.preventDefault();
        $('.checked-vehicles').prop('checked',true);
        $('#calendar').fullCalendar('refetchEvents');
    });
    
    $(document).on('click','.vehicles-select-none',function(event){
        event.preventDefault();
        $('.checked-vehicles').prop('checked',false);
        $('#calendar').fullCalendar('refetchEvents');
    });

    $('#calendar').fullCalendar('refetchEvents');



    /* ADDING EVENTS */
    var currColor = "#3c8dbc"; //Red by default
    //Color chooser button
    var colorChooser = $("#color-chooser-btn");
    $("#color-chooser > li > a").click(function (e) {
        e.preventDefault();
        //Save color
        currColor = $(this).css("color");
        //Add color effect to button
        $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
    });

    $("#add-new-event").click(function (e) {
        e.preventDefault();
        //Get value and make sure it is not null
        var val = $("#new-event").val();
        if (val.length == 0) {
          return;
        }

        //Create events
        var event = $("<div />");
        event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
        event.html(val);
        $('#external-events').prepend(event);

        //Add draggable funtionality
        ini_events(event);

        //Remove event from text input
        $("#new-event").val("");
    });
});
</script>
