@asset "jquery"
@asset "moment"
@asset "fullcalendar"

<!-- used in modals -->
@asset "jquery-form"
@asset "daterangepicker"
@asset "timepicker"
@asset "select2"
@asset "dropzone"

<style>
.fc-content {
    padding: 1px;
}
.fc-day-grid {
    height: 100% !important;
}
</style>
<style>
.grid {
  background-color:#ffffff;
  -webkit-box-shadow:0 1px 3px rgba(0,0,0,0.25);
  -moz-box-shadow:0 1px 3px rgba(0,0,0,0.25);
  box-shadow:0 1px 3px rgba(0,0,0,0.25);
  margin-bottom:10px;
}
.grid .selected { background-color:#ffeeaa; }
.grid .selected:hover { background-color:#efefcc; }
.grid {
  padding:2px;
  border:solid 1px #eeeeee;
}
.grid table { border-collapse:collapse; }
.grid th strong { color:#ffffff; }
.grid thead {
  /*background:#93bc0c url(images/topmenu-bg.jpg) repeat-x;*/
  height:29px;
  padding-left:12px;
  padding-right:12px;
  color:#ffffff;
  text-align:left;
  border-left:1px solid #b6d59a;
  border-bottom:solid 2px #374850;
}
.grid thead tr th {
  /*font-size:12px;*/
  font-weight:bold;
  color:#cccccc;
  background-color:#252525;
  padding-right:16px;
  border-right: solid 1px #fff;
}
.grid thead tr th:last {
    border:none;
}
.grid tr { height:30px; }
.grid td {
  padding-left:11px;
  padding-right:11px;
  border-left:1px solid #e8e8e8;
  border-bottom:1px solid #dfdfdf;
}
.grid td.first, th.first { border-left:0px; }
.grid tr { background:#f8f8f8; }
.grid tr .alt { background:#efefef; }
.grid tbody tr:hover { background-color:#e7efff; }
.grid a {
  color:#336699;
  font-weight:bold;
}
.grid .catcur:hover {
  cursor:pointer;
  text-decoration:underline;
}
.grid a.btn {
    color:#eee;
    padding:2px 5px;
    border-radius:0 !important;
    font-weight: normal;
}
.grid a.btn:hover { color:#fff; }

</style>




<style type="text/css">
.event-holder {
    display: block;
    position: relative;
    top: 0;
    left: 15px;
    
    width: 500px;
    height: 20px;
    
    overflow: hidden;

    color: #fff;
    background-color: #f00;
    border: none;
}

</style>
<!-- Main content -->
<section class="content">
  <div class="row">
    <?php
    $vehicles = vehicle::all();
    $show_days = 7;
    $today = date('Y-m-d');
    ?>
    <div class="grid">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Vozila</th>
                <?php foreach(range(1,$show_days) as $day_nr){ ?>
                <th><?php echo date('Y-m-d D',strtotime("$today +$day_nr days")); ?></th>
                <?php }; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach($vehicles as $vehicle){ ?>
            <tr id="vehicle-row-{{vehicle.id}}">
                <td style="position:relative;">{{vehicle.id}} {{vehicle.name}}</td>
                <td class="first"></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php }; ?>
        </tbody>
    </table>
    </div>

  </div>
</section><!-- /.content -->

<script>

$(function () {
    function get_events(){
        $.ajax({
            url: '/admin/calendar/events',
            type: 'POST',
            dataType: 'json',
            data: {'start':'2016-01-26','end':'2016-02-02','vehicles':[4,5,6,7,8,9,11,12,13,14,15,16]},
        })
        .done(function(data) {
            console.log("success");
            $.each(data, function(index, obj) {
                 console.log(obj);
                 var cell = $('#vehicle-row-'+obj.vehicle_id+' td.first');
                 $(cell).append('<div id="'+obj.model+'-'+obj.id+'" class="event-holder model'+obj.model+'">'+obj.title+'</div>');
            });

        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
        
    }
    get_events();
});
</script>
