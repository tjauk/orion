<?php
$contact = Contact::find($najmoprimac);
$company = Company::find($company_id);
$pdv = floatval($company->tax_rate);
?>
<style>
table.predlozak {
	border-top: solid 1px #333;
	border-left: solid 1px #333;
	border-right: solid 1px #333;
}
table.predlozak tr td {
	border-left: solid 1px #777;
	border-right: solid 1px #777;
	padding-left: 2px;
}
table.nutarnja {
	border: none;
}
table.nutarnja tr {
	border: none;
}
table.nutarnja tr td {
	border: none;
}
.farban, .farban td {
	background-color: #eee !important;
	-webkit-print-color-adjust: exact; 
}
td.text-right {
	padding-right: 4px;
}
.crtadolje {
	border: solid 3px #333;
}
</style>

<?php if( !empty($company->logo) ){ ?>
	<img style="height:60px" src="{{company->logo}}" />
<?php }; ?>

<p>
{{company->name}} , OIB: {{company->oib}} , {{company->address}}, {{company->zip}} {{company->city}}, {{company->country->name}} / {{company->phone}} 
</p>



<h1>Ugovor o najmu vozila</h1>

<table class="predlozak" style="width:100%">
	<tr class="farban">
		<td style="font-weight: bold;width:25%;">KORISNIK NAJMA</td>
		<td style="font-weight: bold;width:20%;">VOZAČ 1</td>
		<td style="font-weight: bold;width:20%;">VOZAČ 2</td>
		<td style="font-weight: bold;width:35%;">VOZILO I TERMIN NAJMA</td>
	</tr>
	<tr>
		<td style="font-weight: bold;">Ime ili naziv</td>
		<td style="font-weight: bold;">Ime i prezime</td>
		<td style="font-weight: bold;">Ime i prezime</td>
		<td style="font-weight: bold;">Vozilo</td>
	</tr>
	<tr>
		<td>{{najmoprimac}}</td>
		<td>{{vozac}}</td>
		<td></td>
		<td>{{registracija_vozila}}</td>
	</tr>
	<tr>
		<td style="font-weight: bold;">Adresa sjedišta</td>
		<td style="font-weight: bold;">Adresa</td>
		<td style="font-weight: bold;">Adresa</td>
		<td style="font-weight: bold;">Poslovnica:</td>
	</tr>
	<tr>
		<td>{{adresa}}</td>
		<td>{{vozac_adresa}}</td>
		<td></td>
		<td>{{company->address}}, {{company->zip}} {{company->city}}</td>
	</tr>
	<tr>
		<td style="font-weight: bold;">Oib broj:</td>
		<td style="font-weight: bold;">Broj vozačke dozvole:</td>
		<td style="font-weight: bold;">Broj vozačke dozvole:</td>
		<td style="font-weight: bold;">Početak najma (datum i sat)</td>
	</tr>
	<tr>
		<td>{{oib}}</td>
		<td>{{vozac_broj_vozacke}}</td>
		<td></td>
		<td>{{datum_preuzimanja}} {{vrijeme_preuzimanja}}</td>
	</tr>
	<tr>
		<td style="font-weight: bold;">Telefon:</td>
		<td style="font-weight: bold;">Telefon:</td>
		<td style="font-weight: bold;">Telefon:</td>
		<td style="font-weight: bold;">Kraj najma (datum i sat)</td>
	</tr>
	<tr>
		<td>{{vozac_telefon}}</td>
		<td>{{vozac_telefon}}</td>
		<td></td>
		<td>{{datum_povratka}} {{vrijeme_povratka}}</td>
	</tr>
</table>

<table class="predlozak" style="width:100%">
	<tr class="farban">
		<td style="width:65%"><b>STANJE VOZILA I OPREME NA POČETKU I KRAJU NAJMA:</b></td>
		<td><b>OBAČUN NAJMA</b></td>
	</tr>
	<tr>
		<td style="vertical-align: center;text-align: center;"><img style="width:320px;height:180px" src="/theme/images/slika-vozila.png" /></td>
		<td style="vertical-align: top;">
			<table class="nutarnja" style="width:100%">
				<tr>
					<td style="width:50%">Dnevna cijena</td>
					<td style="width:30%" class="text-right">{{Num:price($cijena_po_danu)}} </td>
					<td style="width:20%">kn</td>
				</tr>
				<tr>
					<td>Dnevna kilometraža</td>
					<td class="text-right">{{dnevna_kilometraza}} </td>
					<td>km/dan</td>
				</tr>
				<tr>
					<td>Trajanje najma</td>
					<td class="text-right">{{ukupno_dana}} </td>
					<td>dan(a)</td>
				</tr>
				<tr>
					<td>Ukupno kilometara</td>
					<td class="text-right">
						<?php $ukupno_kilometara = $ukupno_dana * $dnevna_kilometraza; ?>
						{{ukupno_kilometara}} 
					</td>
					<td>km</td>
				</tr>
				<tr>
					<td>CDW - kasko</td>
					<td class="text-right">0 </td>
					<td>kn</td>
				</tr>
				<tr>
					<td>TP - osiguranje</td>
					<td class="text-right">0 </td>
					<td>kn</td>
				</tr>
				<tr>
					<td>AO - osiguranje</td>
					<td class="text-right">0 </td>
					<td>kn</td>
				</tr>
				<tr>
					<td>Čišćenje vozila</td>
					<td class="text-right">0 </td>
					<td>kn</td>
				</tr>
				<tr>
					<td>Zeleni karton</td>
					<td class="text-right">0 </td>
					<td>kn</td>
				</tr>
				<tr>
					<td>Dodatni vozač</td>
					<td class="text-right">0 </td>
					<td>kn</td>
				</tr>
				<tr>
					<td colspan="3"><small>NAPOMENA: iznad uključene kilometraže je naplata 0,40kn/km</small></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<table style="width:100%;border-bottom: solid 1px #777;" class="predlozak">
	<tr>
		<td style="width:65%;vertical-align: top;">
			
			<table class="nutarnja" style="width:100%">
				<tr>
					<td><b>Vozilo na početku najma:</b></td>
					<td><b>Početna kilometraža:</b></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="2"><b>Popis oštećenja na vozilu:</b></td>
					<td><b>Važeća oprema:</b></td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>o Sigurnosni trokut</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>o Sigurnosni prsluk</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>o Kutija prve pomoći</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>o Protupožarni aparat</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>o Rezervni kotač</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>o Dizalica vozila</td>
				</tr>

				<tr>
					<td colspan="2"><b>Dodatna napomena (ako postoji:)</b></td>
					<td><b>Pregledao:</b></td>
				</tr>
				<tr>
					<td colspan="2">{{napomena}}</td>
					<td>&nbsp;</td>
				</tr>
				<tr style="border-bottom: solid 1px #777;">
					<td  colspan="2">&nbsp;</td>
					<td class="crtadolje"></td>
				</tr>

				<tr>
					<td><b>Vozilo na kraju najma:</b></td>
					<td><b>Završna kilometraža:</b></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="2"><b>Popis novonastalih oštećenja na vozilu:</b></td>
					<td><b>Važeća oprema:</b></td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>o Sigurnosni trokut</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>o Sigurnosni prsluk</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>o Kutija prve pomoći</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>o Protupožarni aparat</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>o Rezervni kotač</td>
				</tr>
				<tr>
					<td style="border-bottom: dashed 1px #777;" colspan="2">Rb.</td>
					<td>o Dizalica vozila</td>
				</tr>

				<tr>
					<td colspan="3"><b>Dodatna napomena (ako postoji):</b></td>
				</tr>
				<tr>
					<td colspan="3">{{napomena2}}&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr style="border-bottom: solid 1px #777;">
					<td colspan="3"></td>
				</tr>
				<tr>
					<td colspan="3"><small>Potpis osobe koja je u ime najmoprimaca vratila vozilo</small></td>
				</tr>

			</table>

		</td>
		<td style="vertical-align: top;">
			
			<table class="nutarnja" style="width:100%">
				<tr>
					<td colspan="3"><b>DODACI NAJMA:</b></td>
				</tr>
				<tr>
					<td colspan="3">(ako je iznos 0,00kn usluga nije ugovorena)</td>
				</tr>
				
				<tr>
					<td>Dodatni kilometri</td>
					<td class="text-right">{{intval($dodatni_kilometri)}}</td>
					<td>km</td>
				</tr>
				<tr>
					<td>Ukupno kilometri</td>
					<td class="text-right"><?php echo Num::price($dodatni_kilometri*0.4);?> </td>
					<td>kn</td>
				</tr>
				<tr>
					<td>Dodatni sat najma</td>
					<td class="text-right">{{Num:price($dodatni_sat_najma)}} </td>
					<td>kn</td>
				</tr>
				<tr>
					<td>Rad izvan R.V.</td>
					<td class="text-right">{{Num:price($preuzimanje_izvan_rv)}} </td>
					<td>kn</td>
				</tr>
				<tr>
					<td>Dostava vozila</td>
					<td class="text-right">{{Num:price($dostava_vozila_na_adresu)}} </td>
					<td>kn</td>
				</tr>
				<tr>
					<td><b>Ukupno dodatci</b></td>
					<td class="text-right"><?php
					$ukupno_dodatci = $dodatni_kilometri*0.4;
					$ukupno_dodatci+= $dodatni_sat_najma;
					$ukupno_dodatci+= $preuzimanje_izvan_rv;
					$ukupno_dodatci+= $dostava_vozila_na_adresu;
					echo $ukupno_dodatci;
					?> </td>
					<td>kn</td>
				</tr>

				<tr>
					<td colspan="3"><b>OBRAČUN NAJMA SA DODACIMA:</b></td>
				</tr>
				<tr>
					<td style="width:50%;">Najam</td>
					<td style="width:30%;"class="text-right">
						<?php $cijena_najma = (int)$ukupno_dana * (float)$cijena_po_danu; ?>
						{{Num:price($cijena_najma)}}
					</td>
					<td style="width:20%;">kn</td>
				</tr>
				<tr>
					<td>Popust</td>
					<td class="text-right"><?php echo (int)$popust;?></td>
					<td>%</td>
				</tr>
				<tr>
					<td>Popust u kn</td>
					<td class="text-right">
					<?php $popust_u_kn = $cijena_najma * ((int)$popust/100); ?>
					{{Num:price($popust_u_kn)}}
					</td>
					<td>kn</td>
				</tr>
				<tr>
					<td>Ukupno najam</td>
					<td class="text-right">
						<?php $ukupno_najam = $cijena_najma - $popust_u_kn; ?>
						{{Num:price($ukupno_najam)}}
					</td>
					<td>kn</td>
				</tr>
				<tr>
					<td>Dodatci ukupno</td>
					<td class="text-right">{{Num:price($ukupno_dodatci)}}</td>
					<td>kn</td>
				</tr>
				<?php
				$ukupno_sve = $ukupno_najam + $ukupno_dodatci;
				$ukupno_bez_pdva = $ukupno_sve;
				$ukupno_sa_pdvom = $ukupno_bez_pdva * $pdv;
				$samo_pdv = $ukupno_sa_pdvom - $ukupno_bez_pdva;

				$ugovorena_kilometraza = $ukupno_kilometara + $dodatni_kilometri;
				?>
				<tr>
					<td>Ukupno bez PDV-a</td>
					<td class="text-right">{{Num:price($ukupno_bez_pdva)}}</td>
					<td>kn</td>
				</tr>
				<tr>
					<td>PDV (<?php echo ($pdv-1)*100;?>%)</td>
					<td class="text-right">{{Num:price($samo_pdv)}}</td>
					<td>kn</td>
				</tr>
				<tr>
					<td><b>Ukupan iznos:</b></td>
					<td class="text-right"><b>{{Num:price($ukupno_sa_pdvom)}}</b></td>
					<td>kn</td>
				</tr>
				<tr>
					<td><b>Ugovorena kilometraža:</b></td>
					<td class="text-right"><b>{{intval($ugovorena_kilometraza)}}</b></td>
					<td>km</td>
				</tr>

				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3"><b>Ugovor sastavio:</b> {{ugovor_napisao}}</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr style="border-bottom: solid 1px #777;">
					<td colspan="3">&nbsp;</td>
				</tr>

				<tr>
					<td><b>U ime najmoprimca:<b></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3" style="font-size:6px">Osoba izjavljuje da je ovlaštena od korisnika najma za preuzimanje vozila.</td>
				</tr>
<!--
				<tr>
					<td colspan="3">Vozilo pregledao i preuzeo na kraju najma:</td>
				</tr>
				<tr>
					<td colspan="3">MP i potpis</td>
				</tr>
				-->
				
			</table>

		</td>
	</tr>
</table>

<p>
<br>
<b>Prilog ugovora su:</b>
knjižica vozila, polica auto odgovornosti, knjižica periodičkog pregleda, uvjeti najma na poleđini<br>
<em>UGOVOR JE SASTAVLJEN <?php echo UI::date($dated_at);?> U 2 (DVA) ISTOVJETNA PRIMJERKA OD KOJIH SVAKA STRANA ZADRŽAVA 1 (JEDAN) PRIMJERAK UGOVORA</em>
</p>


