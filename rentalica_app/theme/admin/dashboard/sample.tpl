<!-- Morris chart -->
@css "morris/morris.css"
<!-- Morris.js charts -->
@jsend "https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"
@jsend "morris/morris.min.js"
<?php /*
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
@jsend "pages/dashboard.js"
*/ ?>
  <!-- Content Header (Page header) -->
  <!--
        <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
  -->
  
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{Booking:startsToday()->withoutOtherServices()->count()}}</h3>
                  <p>Danas počinje</p>
                </div>
                <div class="icon">
                  <i class="fa fa-truck"></i>
                </div>
                <a href="/admin/booking?keywords=&vehicle_id=&partner=&paid_status=&from=<?php echo date('Y-m-d');?>&to=<?php echo date('Y-m-d');?>&look=from&perpage=1000" class="small-box-footer">pogledaj sve <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3>{{Booking:endsToday()->withoutOtherServices()->count()}}</h3>
                  <p>Danas završava</p>
                </div>
                <div class="icon">
                  <i class="fa fa-truck"></i>
                </div>
                <a href="/admin/booking?keywords=&vehicle_id=&partner=&paid_status=&from=<?php echo date('Y-m-d');?>&to=<?php echo date('Y-m-d');?>&look=to&perpage=1000" class="small-box-footer">pogledaj sve <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3>{{VehicleService:inPeriod()->count()}}</h3>
                  <p>Danas na servisu</p>
                </div>
                <div class="icon">
                  <i class="fa fa-wrench"></i>
                </div>
                <a href="/admin/vehicle-service" class="small-box-footer">pogledaj sve <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div>
<?php /*
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>65</h3>
                  <p>Tren</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">pogledaj sve <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
*/ ?>


<?php
$start_date = date('Y-m-d');
$week_later = date('Y-m-d',strtotime('now +7days'));
?>
          <div class="row">
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{Booking:startsThisWeek()->withoutOtherServices()->count()}}</h3>
                  <p>Počinje unutar 7 dana</p>
                </div>
                <div class="icon">
                  <i class="fa fa-truck"></i>
                </div>
                <a href="/admin/booking?keywords=&vehicle_id=&partner=&paid_status=&from=<?php echo $start_date;?>&to=<?php echo $week_later;?>&look=from&perpage=1000" class="small-box-footer">pogledaj sve <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3>{{Booking:endsThisWeek()->withoutOtherServices()->count()}}</h3>
                  <p>Završava unutar 7 dana</p>
                </div>
                <div class="icon">
                  <i class="fa fa-truck"></i>
                </div>
                <a href="/admin/booking?keywords=&vehicle_id=&partner=&paid_status=&from=<?php echo $start_date;?>&to=<?php echo $week_later;?>&look=to&perpage=1000" class="small-box-footer">pogledaj sve <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->
          

          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
              <!-- Custom tabs (Charts with tabs)-->
              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->

                <ul class="nav nav-tabs pull-right">
                  <li class="active"><a href="#revenue-chart" data-toggle="tab">Graf</a></li>
                  <!--<li><a href="#sales-chart" data-toggle="tab">Donut</a></li>-->
                  <li class="pull-left header"><i class="fa fa-inbox"></i> Prihodi </li>
                </ul>

                <div class="alert alert-info">
                  <p>
                    <i class="icon fa fa-info"></i> Prikaz od početnog dana najma po mjesecima bez obzira na status plaćanja.
                  </p>
                </div>

                <div class="tab-content no-padding">
                  <!-- Morris chart - Sales -->
                  <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>
                  <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
                </div>
              </div><!-- /.nav-tabs-custom -->



              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->

                <ul class="nav nav-tabs pull-right">
                  <li><a href="#expires-in-30" data-toggle="tab">30 dana</a></li>
                  <li class="active"><a href="#expires-in-15" data-toggle="tab">15 dana</a></li>
                  <!--<li><a href="#sales-chart" data-toggle="tab">Donut</a></li>-->
                  <li class="pull-left header"><i class="fa fa-bus"></i> Prikaz isteka registracija, kaska, periodičkih pregleda i protupožarnih aparata. </li>
                </ul>

                <div class="alert alert-info">
                  <p>
                    <i class="icon fa fa-info"></i>
                    Kada se napravi nova registracija, AO i protupožarnih aparata pod vozila obavezno unijeti datum isteka.
                  </p>
                </div>

                <div class="tab-content">
                  <div class="active tab-pane table-responsive" id="expires-in-15">
                    <?php
                    $days = 15;
                    $items = Vehicle::itemsExpiringSoon($days);
                    $table = Table::make()->headings(['Datum','Ističe','Vozilo']);
                    foreach($items as $item){
                        $data = [];
                        $data[] = UI::date($item->datum);
                        $data[] = $item->item;
                        $data[] = '<a href="/admin/vehicle/edit/'.$item->id.'">'.$item->name.'</a>';
                        $table->addRow($data);
                    }
                    $table->out('admin/table/grid');
                    ?>
                  </div>
                  <div class="tab-pane table-responsive" id="expires-in-30">
                    <?php
                    $days = 30;
                    $items = Vehicle::itemsExpiringSoon($days);
                    $table = Table::make()->headings(['Datum','Ističe','Vozilo']);
                    foreach($items as $item){
                        $data = [];
                        $data[] = UI::date($item->datum);
                        $data[] = $item->item;
                        $data[] = '<a href="/admin/vehicle/edit/'.$item->id.'">'.$item->name.'</a>';
                        $table->addRow($data);
                    }
                    $table->out('admin/table/grid');
                    ?>
                  </div>
                </div>

              </div><!-- /.nav-tabs-custom -->



              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->

                <ul class="nav nav-tabs pull-right">
                  <li class="active"><a href="#servis-1" data-toggle="tab">Po vozilima</a></li>
                  <li class="pull-left header"><i class="fa fa-bus"></i> Prikaz redovitog servisa, zadnjeg servisnog pregleda i najranijeg sljedećeg servisa </li>
                </ul>

                <div class="alert alert-info">
                  <p>
                    <i class="icon fa fa-info"></i>
                    <b>Redovite servise</b> raditi svakih 40000 km, a <b>preglede</b> svakih 10000 km.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                    <b>Redoviti servis</b> obavezno unijeti 3000km prije isteka u rubrici servisi.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                    <b>Preglede</b> obavezno unijeti 1000km prije isteka u rubrici servisi.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                    <b>Izvanredne servise</b> unijeti prema naputku servisnog centra.
                  </p>
                </div>

                <div class="tab-content">
                  <div class="active tab-pane  table-responsive" id="servis-1">
                    <?php
                    $vehicles = Vehicle::all();
                    $table = Table::make()
                                  ->headings([
                                              'Vozilo i trenutna kilometraža',
                                              'Zadnji redoviti servis',
                                              'Zadnji servisni pregled',
                                              'Sljedeći servis',
                                              'Svi servisi za vozilo'
                                            ]);
                    foreach($vehicles as $item){
                        $data = [];
                        
                        $data[] = '<a href="/admin/vehicle-service?vehicle_id='.$item->id.'">'.$item->name.'</a><br>'.'<span style="text-align:right;font-weight:bold;">'.$item->km.' km</span>';
                        
                        $last_regular = VehicleService::whereVehicleId($item->id)
                                                      ->whereType('Redovan')
                                                      ->whereServiceStatus(3) // finished
                                                      ->orderBy('updated_at')
                                                      ->first();
                        $out = [];
                        if( !empty($last_regular->to) ){ $out[] = UI::date($last_regular->to); }
                        if( !empty($last_regular->vehicle_km) ){ $out[] = $last_regular->vehicle_km.' km + '.($item->km-$last_regular->vehicle_km).' km'; }
                        $data[] = implode('<br>',$out);
                        
                        $last_check = VehicleService::whereVehicleId($item->id)
                                                      ->whereType('Pregled')
                                                      ->whereServiceStatus(3) // finished
                                                      ->orderBy('updated_at')
                                                      ->first();
                        $out = [];
                        if( !empty($last_check->to) ){ $out[] = UI::date($last_check->to); }
                        if( !empty($last_check->vehicle_km) ){ $out[] = $last_check->vehicle_km.' km + '.($item->km-$last_check->vehicle_km).' km'; }
                        $data[] = implode('<br>',$out);
                        
                        $next_services = VehicleService::nextTime($item)->get();
                        $next = [];
                        foreach ($next_services as $service) {
                            if( !empty($service->vehicle_km) ){
                                $next[] = $service->type.' na '.$service->vehicle_km.' km <span style="font-weight:bold;">(za '.($service->vehicle_km-$item->km).' km)</span>';
                            }else{
                                $next[] = UI::date($service->from).' - '.$service->type;
                            }
                        }
                        $data[] = implode('<br>',$next);

                        $data[] = '<a class="btn btn-xs btn-primary" href="/admin/vehicle-service?vehicle_id='.$item->id.'">Pogledaj</a>';

                        $table->addRow($data);
                    }
                    $table->out('admin/table/grid');
                    ?>
                  </div>
                </div>
              </div><!-- /.nav-tabs-custom -->





            </section><!-- /.Left col -->


          </div><!-- /.row (main row) -->

        </section><!-- /.content -->

<?php
$currentYear = date('Y');
$months = range(0,15);
$data = [];
foreach($months as $month){
    $month_start= date('Y-m-d',strtotime($currentYear.'-01-01 +'.$month.'month'));
    $month_end  = date('Y-m-t',strtotime($month_start));
    
    $items = Booking::inMonth($month,$currentYear)->get();

    $total_price1 = 0.0;
    $total_price2 = 0.0;
    foreach ($items as $item) {
        $price = $item->price;
        if( $item->paid==99 ){
            $price = 0.0;
        }
        if( $item->partner==1 ){
            $total_price1+=$price;
        }
        if( $item->partner==2 ){
            $total_price2+=$price;
        }
    }

    $data[] = '{y: "'.date('Y-m',strtotime($month_start)).'", item1: '.$total_price1.'}';
}
?>

<script>
$(document).ready(function(){

  /* Morris.js Charts */
  // Prihodi

  var area = new Morris.Area({
    element: 'revenue-chart',
    resize: true,
    behaveLikeLine: true,
    data: [<?php echo implode(',',$data);?>],
    xkey: 'y',
    ykeys: ['item1'],
    labels: ['<?php echo Company::owned()->first()->name;?>'],
    lineColors: ['#a0d0e0', '#3c8dbc'],
    hideHover: 'auto'
  });

  
  /*
  var line = new Morris.Line({
    element: 'line-chart',
    resize: true,
    data: [
      {y: '2011 Q1', item1: 2666},
      {y: '2011 Q2', item1: 2778},
      {y: '2011 Q3', item1: 4912},
      {y: '2011 Q4', item1: 3767},
      {y: '2012 Q1', item1: 6810},
      {y: '2012 Q2', item1: 5670},
      {y: '2012 Q3', item1: 4820},
      {y: '2012 Q4', item1: 15073},
      {y: '2013 Q1', item1: 10687},
      {y: '2013 Q2', item1: 8432}
    ],
    xkey: 'y',
    ykeys: ['item1'],
    labels: ['Item 1'],
    lineColors: ['#efefef'],
    lineWidth: 2,
    hideHover: 'auto',
    gridTextColor: "#fff",
    gridStrokeWidth: 0.4,
    pointSize: 4,
    pointStrokeColors: ["#efefef"],
    gridLineColor: "#efefef",
    gridTextFamily: "Open Sans",
    gridTextSize: 10
  });
  */

  //Donut Chart
  /*
  var donut = new Morris.Donut({
    element: 'sales-chart',
    resize: true,
    colors: ["#3c8dbc", "#f56954", "#00a65a"],
    data: [
      {label: "Download Sales", value: 12},
      {label: "In-Store Sales", value: 30},
      {label: "Mail-Order Sales", value: 20}
    ],
    hideHover: 'auto'
  });
  */

  //Fix for charts under tabs
  $('.box ul.nav a').on('shown.bs.tab', function () {
    area.redraw();
    donut.redraw();
    line.redraw();
  });

});
</script>