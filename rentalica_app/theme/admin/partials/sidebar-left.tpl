<!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">GLAVNI IZBORNIK</li>
            
            @if Auth:can('menu.dashboard')
            <li>
              <a href="/admin/dashboard">
                <i class="fa fa-dashboard"></i> <span>Pregled</span>
              </a>
            </li>
            @end

            @if Auth:can('menu.calendar')
            <li>
              <a href="/admin/calendar">
                <i class="fa fa-calendar"></i> <span>Raspored</span>
              </a>
            </li>
            @end

            @if Auth:can('menu.booking')
            <li>
              <a href="/admin/booking">
                <i class="fa fa-truck"></i> <span>Najam</span>
                <small class="label pull-right bg-green">{{Booking:owned()->inPeriod()->count()}}</small>
              </a>
            </li>
            @end

            @if Auth:can('menu.contact')
            <li>
              <a href="/admin/contact">
                <i class="fa fa-user-plus"></i> <span>Imenik</span>
                <small class="label pull-right bg-gray">{{Contact:owned()->count()}}</small>
              </a>
            </li>
            @end

            <li>
              <a href="/admin/contract">
                <i class="fa fa-file-text-o"></i> <span>Ugovori</span>
              </a>
            </li>
            
            @if Auth:can('menu.vehicle-service')
            <li>
              <a href="/admin/vehicle-service">
                <i class="fa fa-wrench"></i> <span>Servisi</span>
                <small class="label pull-right bg-yellow">{{VehicleService:owned()->inPeriod()->count()}}</small>
              </a>
            </li>
            @end

            @if Auth:can('menu.damage')
            <li>
              <a href="/admin/damage">
                <i class="fa fa-bullhorn"></i> <span>Oštećenja</span>
                <small class="label pull-right bg-red">{{Damage:owned()->whereStatus(1)->count()}}</small>
              </a>
            </li>
            @end

            @if Auth:can('menu.vehicle')
            <li>
              <a href="/admin/vehicle">
                <i class="fa fa-bus"></i> <span>Vozila</span>
                <small class="label pull-right bg-blue">{{Vehicle:owned()->count()}}</small>
              </a>
            </li>
            @end

            @if Auth:can('menu.checklist')
            <li>
              <a href="/admin/checklist">
                <i class="fa fa-bus"></i> <span>Provjera vozila</span>
              </a>
            </li>
            @end

            @if Auth:can('menu.company')
            <li>
              <a href="/admin/company">
                <i class="fa fa-briefcase"></i> <span>Tvrtke</span>
                <small class="label pull-right bg-green">{{Company:count()-1}}</small>
              </a>
            </li>
            @end

            <li>
              <a href="/admin/account">
                <i class="fa fa-cog"></i> <span>Postavke</span>
              </a>
            </li>

<?php /*
            <li>
              <a href="/admin/report/vehicles">
                <i class="fa fa-bus"></i> <span>Report test</span>
              </a>
            </li>



            <li>
              <a href="/admin/task">
                <i class="fa fa-check-square-o"></i> <span>Zadaci</span>
                <!--<small class="label pull-right bg-yellow">12</small>-->
              </a>
            </li>
*/?>

            @if Auth:can('menu.user')
            <li class="treeview">
              <a href="/admin/user">
                <i class="fa fa-user"></i> <span>Korisnici</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                @if Auth:can('menu.user')
                  <li class="active"><a href="/admin/user"><i class="fa fa-circle-o"></i> Korisnici</a></li>
                @end
                @if Auth:can('manage.usergroups')
                  <li><a href="/admin/usergroup"><i class="fa fa-circle-o"></i> Korisničke grupe</a></li>
                @end
              </ul>
            </li>
            @end
<!--
            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
-->
          </ul>