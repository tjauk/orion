<h1>Prijava</h1>
{{Session:get('msg')}}
{{form..Open}}

<label>Vaša email adresa</label>
{{form..username}}<br>

<label>Vaša lozinka</label>
{{form..password}}<br>

<br>
{{form..btnSubmit}}

{{form..Close}}

<a href="{{"/user/register"|href}}">Otvori korisnički račun</a>
<br>
<a href="{{"/user/lost-password"|href}}">Izgubljena lozinka?</a>
