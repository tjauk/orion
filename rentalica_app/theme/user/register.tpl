<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{doc.title}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/theme/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/theme/css/AdminLTE.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">

{{form..Open}}

    <div class="login-box" style="width: 640px;margin-top:25px">
      <div class="login-logo">
        <a href="{{ROOTURL}}"><b>{{doc.title}}</b></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">

          @if Session:has('msg')
            <div class="col-xs-12">
              <div class="alert alert-warning alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{Session:get('msg')}}
              </div>
            </div>
          @end

			<h2>Otvaranje korisničkog računa</h2>

			<div class="form-group has-feedback">
				{{form..username}}
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				<script>
				window.onload = function() {
				    document.getElementById("username").focus();
				};
				</script>
			</div>

			<div class="form-group has-feedback">
				{{form..password}}
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>

			<div class="form-group has-feedback">
				{{form..repeated_password}}
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>

			<h2>Upišite osnovne podatke o vašoj tvrtki</h2>

			<div class="form-group has-feedback">
				{{form..company_name}}
			</div>

			<div class="form-group has-feedback">
				{{form..company_oib}}
			</div>

			<div class="form-group has-feedback">
				{{form..company_address}}
			</div>

			<div class="form-group has-feedback">
				{{form..company_zip}}
			</div>

			<div class="form-group has-feedback">
				{{form..company_city}}
			</div>

			<div class="form-group has-feedback">
				{{form..company_country_id}}
			</div>

			<div class="form-group has-feedback">
				{{form..company_phone}}
			</div>

			<div class="form-group has-feedback">
				<label>Da li ste u sustavu PDV-a</label>
				{{form..company_tax_rate}}
			</div>

{{form..btnSubmit..addClass('btn-primary btn-block btn-flat')}}


      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

{{form..Close}}

    <!-- jQuery 2.1.4 -->
    <script src="/theme/js/jquery/jquery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="/theme/js/bootstrap.min.js"></script>

  </body>
</html>
