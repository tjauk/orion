#!/bin/bash
#Linode Ubuntu 16.04 LTS

#os update
sudo apt-get update --yes
sudo apt-get install nginx --yes
sudo ufw allow 'Nginx HTTP'
sudo ufw status

#mysql
sudo apt-get install mysql-server --yes

#php
sudo add-apt-repository ppa:ondrej/php
sudo apt-get install php7.1 php7.1-common php-fpm
sudo apt-get install php7.1-cli php7.1-curl php7.1-dom php7.1-xml php7.1-zip php7.1-gd php7.1-mysql php7.1-mbstring php7.1-gd --yes
sudo apt-get install php7.1-bcmath

#redis
sudo apt-get install redis-server --yes

#misc
sudo apt-get install imagemagick --yes
sudo apt-get install fail2ban --yes
sudo apt-get install curl --yes
sudo apt-get install git --yes
sudo apt-get install strace --yes

#composer
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

#OPTIONAL if you have valid domain set
#sudo apt-get install sendmail-bin sendmail --yes

#orion
mkdir /var/www/orion
cd /var/www/orion/
git clone https://tjauk@bitbucket.org/tjauk/orion.git .
git config credential.helper store
git remote set-url --push origin no_push

#framework
mkdir /var/www/orion/framework
cd /var/www/orion/framework
git clone https://tjauk@bitbucket.org/tjauk/orion-framework.git .
git config credential.helper store
git remote set-url --push origin no_push

#composer
cd /var/www/orion/
composer update


#phantomjs
sudo apt-get update
sudo apt-get install build-essential chrpath libssl-dev libxft-dev --yes

sudo apt-get install libfreetype6 libfreetype6-dev
sudo apt-get install libfontconfig1 libfontconfig1-dev

cd ~
export PHANTOM_JS="phantomjs-2.1.1-linux-x86_64"
wget https://bitbucket.org/ariya/phantomjs/downloads/$PHANTOM_JS.tar.bz2
sudo tar xvjf $PHANTOM_JS.tar.bz2

sudo mv $PHANTOM_JS /usr/local/share
sudo ln -sf /usr/local/share/$PHANTOM_JS/bin/phantomjs /usr/local/bin

#damp
mkdir /var/www/damp
cd /var/www/damp/
git clone https://tjauk@bitbucket.org/tjauk/damp.git .
git config credential.helper store
git remote set-url --push origin no_push

cp example.env .env

#copy aliases to root profile
#
#


#done
