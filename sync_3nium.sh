#!/bin/bash

cd /var/www/orion/

# set all folders
declare -a folders=(events_croatia events_france events_poland)

# loop through folders and execute
for folder in "${folders[@]}"
do
	cd $folder
	echo ""
	echo $folder
	php -f orion events:sync
	php -f orion events:covers
	php -f orion ai:run
	cd ..
done
