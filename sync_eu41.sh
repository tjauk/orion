#!/bin/bash

cd /var/www/orion/

# set all folders
declare -a folders=(events_austria events_belgium events_czech events_denmark events_finland events_gb events_germany events_greece events_ireland events_italy events_luxembourg events_malta events_netherlands events_norway events_poland events_portugal events_spain events_switzerland events_southafrica events_sweden)

# loop through folders and execute
for folder in "${folders[@]}"
do
	cd $folder
	echo ""
	echo $folder
	php -f orion events:sync
	php -f orion events:covers
	# php -f orion ai:run
	cd ..
done
