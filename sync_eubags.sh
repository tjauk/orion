#!/bin/bash

cd /var/www/orion/

# set all folders
# events_uk removed
declare -a folders=( events_germany )

# loop through folders and execute
for folder in "${folders[@]}"
do
	cd $folder
	echo ""
	echo $folder
	php -f orion events:sync
	php -f orion events:covers
#	php -f orion ai:run
	cd ..
done

# run germany without ai process
#cd events_germany
#echo ""
#echo "events_germany"
#php -f orion events:sync
#cd ..
