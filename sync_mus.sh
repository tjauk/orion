#!/bin/bash

cd /var/www/orion/

# set all folders
declare -a folders=(events_australia events_colombia events_japan events_newzealand events_mexico events_singapore)

# loop through folders and execute
for folder in "${folders[@]}"
do
	cd $folder
	echo ""
	echo $folder
	php -f orion events:sync
	php -f orion events:covers
	php -f orion ai:run
	cd ..
done

# run usa without ai process
#cd events_usa
#echo ""
#echo "events_usa"
#php -f orion events:sync
#cd ..

# run canada without ai process
cd events_canada
echo ""
echo "events_canada"
php -f orion events:sync
php -f orion events:covers
cd ..

# run brazil without ai process
cd events_brazil
echo ""
echo "events_brazil"
php -f orion events:sync
php -f orion events:covers
cd ..