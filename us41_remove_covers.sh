#!/bin/bash

# set all folders
declare -a folders=(events_usa)

# loop through folders and execute
for folder in "${folders[@]}"
do
	rm -r /var/www/orion/$folder/images/covers/
done

echo "Removed covers on US41 server"
